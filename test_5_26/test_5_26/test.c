#include <stdio.h>


//struct point
//{
//	int x;
//	int y;
//}p1;		//定义变量的同时定义变量p1
//struct point p2; //定义结构体变量p2
//
////初始化：定义变量的同时赋初值
//struct point p3 = { 1,2 };
//
//struct Student //类型声明
//{
//	char name[15];
//	int age;
//};
//
//struct Student s = { "zhangsan",20 };//初始化
//
//struct Node
//{
//	int date;
//	struct point p;
//	struct Node* next;
//}n1 = { 10,{4,5},NULL };	//结构体嵌套初始化
//
//struct Node n2 = { 20,{100,98},NULL };//结构体嵌套初始化



//struct Stu
//{
//	char name[20];
//	int age;
//};
//
////函数定义
//void print(struct Stu* ps)
//{
//	//方法一，解引用实现，过于冗杂
//	printf("name=%s age=%d\n", (*ps).name, (*ps).age);
//	//方法二，直接对指针变量操作，达到目的
//	printf("name=%s age=%d\n", ps->name, ps->age);
//}
//
//int main()
//{
//	struct Stu s = { "zhangsan",20 };
//	//封装打印函数
//	print(&s);//结构体地址传参
//	return 0;
//}

////结构体类型定义
//struct S
//{
//	int date[1000];
//	int num;
//};
//
//struct S s = { {1,2,3,4},1000 };//初始化
//
////结构体传参
//void print1(struct S s)
//{
//	printf("sum=%d\n", s.num);
//}
//
////结构体变量传参
//void print2(struct S* ps)
//{
//	printf("sum=%d\n", ps->num);
//}
//
//int main()
//{	//封装两个打印函数
//	print1(s);//传结构体
//	print2(&s);//传地址
//	return 0;
//}




//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = 10 - i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



