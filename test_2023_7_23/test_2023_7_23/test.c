#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};


//enum Day//星期
//{
//	Mon,
//	Tues,
//	Wed,
//	Thur,
//	Fri,
//	Sat,
//	Sun
//};
//enum Sex//性别
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//enum Color//颜色
//{
//	RED,
//	GREEN,
//	BLIE
//};

//enum Color
//{
//	RED=1,
//	GREEN=2,
//	BLUE=4
//};
//
//int main()
//{
//	enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异
//	clr = 5;
//
//	return 0;
//}


#include <stdio.h>
#include <assert.h>
//模拟实现方法一
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//模拟实现方法二：指针-指针
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	char* start = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str - start;
//}

//模拟实现方法三：递归
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	if (*str == '\0')
//		return 0;
//	else
//		return 1 + my_strlen(str + 1);
//}
//int main()
//{
//	size_t sz = my_strlen("abc");
//	printf("%d\n", sz);
//	return 0;
//}


//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello world";
//	char arr2[] = "xxxxxx";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}



//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	//1.找到目标空间'\0'
//	while (*dest)
//	{
//		dest++;
//	}
//	//2. 追加字符
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}



//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	int ret = my_strcmp("bbq", "bcq");
//	printf("%d\n", ret);
//	return 0;
//}



//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "xxx";
//	//strncpy(arr1, arr2, 5);
//	strncat(arr1, arr2, 5);
//	return 0;
//}


//int main()
//{
//	int ret = strncmp("abced", "abc", 4);
//	return 0;
//}


#include <string.h>
//
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	char* cp = str1;
//	char* s1 = cp;
//	char* s2 = str2;
//
//	if (*s2 == '\0')
//		return NULL;
//	while (*cp)
//	{
//		//开始匹配
//		s1 = cp;
//		s2 = str2;
//		while(*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//			return cp;
//		cp++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abcdefabcdef";
//	char arr2[] = "def";
//	char* ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//		printf("%s\n", ret);
//	else
//		printf("NULL");
//	return 0;
//}





//int main()
//{
//	char arr1[] = "zhenyuWang@yeah.net";
//	char copy[30];
//	strcpy(copy, arr1);
//
//	char arr2[] = "@.";
//	char* ret = NULL;
//	for (ret = strtok(copy, arr2); ret != NULL; ret = strtok(NULL, arr2))
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}





//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d: %s\n", i,strerror(i));
//	}
//	return 0;
//}



//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	//arr1中的内容拷贝到arr2
//	memcpy(arr2, arr1, 40);
//	for (int i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}



//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	my_memcpy(arr2, arr1, 20);
//	//my_memcpy(arr1 + 2, arr1, 20);
//	for (int i = 0; i < 20; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}


//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest > src)//从后向前拷贝
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	else//从前向后拷贝
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1 + 2, arr1, 20);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	return 0;
//}




//int main()
//{
//	int arr1[] = { 1,2,1,4,5,6 };
//	int arr2[] = { 1,2,257 };
//	int ret = memcmp(arr1, arr2, 10);
//	printf("%d\n", ret);
//	return 0;
//}

#include <ctype.h>
int main()
{
	char str[] = "AFJffjjnn";
	char* cp = str;
	while (*cp)
	{
		if (islower(*cp))
		{
			*cp=toupper(*cp);
		}
		cp++;
	}
	printf("%s\n", str);
	return 0;
}