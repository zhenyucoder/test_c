#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"


void SLInit(SL* ps)
{
	assert(ps);

	ps->a = (SLDateType*)malloc(sizeof(SLDateType) * 4);
	if (ps->a == NULL)
	{
		perror("malloc");
		exit(-1);
	}

	ps->size = 0;
	ps->capacity = 4;
}


void SLDestory(SL* ps)
{
	assert(ps);

	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SLPrint(SL* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}


void SLCheakCapacity(SL* ps)
{
	assert(ps);
	//需要扩容
	if (ps->size == ps->capacity)
	{
		SLDateType* tmp = realloc(ps->a, ps->capacity * sizeof(SLDateType)*2);
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}

		ps->a = tmp;
		ps->capacity *= 2;
	}
}

void SLPushBack(SL* ps, SLDateType x)
{
	assert(ps);
	SLCheakCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;
}

void SLPophBack(SL* ps)
{
	assert(ps);
	assert(ps->size > 0);
	ps->size--;
}


void SLPushFront(SL* ps, SLDateType x)
{
	SLCheakCapacity(ps);

	//挪动数据
	int end = ps->size - 1;
	while (end >= 0)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[0] = x;
	ps->size++;
}

void SLPopFront(SL* ps)
{
	assert(ps->size > 0);
	int begin = 1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;
}
