#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDateType;
typedef struct SeqList
{
	SLDateType* a;
	int size;//存储数据大小
	int capacity;//空间大小
}SL;


//管理数据 --- 增删查改
void SLInit(SL* ps);
void SLDestory(SL* ps);
void SLCheakCapacity(SL* ps);
void SLPrint(SL* ps);



//头插头删 尾插尾删
void SLPushBack(SL* ps, SLDateType x);
void SLPophBack(SL* ps);
void SLPushFront(SL* ps, SLDateType x);
void SLPopFront(SL* ps);

