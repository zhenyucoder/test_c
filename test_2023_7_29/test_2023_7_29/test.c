#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"

//TestSeqList()
//{
//	SL s;
//	SLInit(&s);
//	SLPushBack(&s, 1);
//	SLPushBack(&s, 2);
//	SLPushBack(&s, 3);
//	SLPushBack(&s, 4);
//	SLPushBack(&s, 5);
//	SLPushBack(&s, 6);
//	SLPushBack(&s, 6);
//	SLPushBack(&s, 7);
//	SLPophBack(&s);
//	SLPophBack(&s);
//	SLPophBack(&s);
//	SLPrint(&s);
//}


TestSeqList2()
{
	SL s;
	SLInit(&s);	
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushFront(&s, 10); 
	SLPushFront(&s, 11); 
	SLPushFront(&s, 12); 
	SLPrint(&s);

}

int main()
{
	SL s;
	SLInit(&s);

	//TestSeqList();
	TestSeqList2();
	return 0;
}