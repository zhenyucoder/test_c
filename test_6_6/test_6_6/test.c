//int removeDuplicates(int* nums, int numsSize) {
//
//    int left = 0;
//    for (int right = 1; right < numsSize; right++)
//    {
//        if (nums[right] != nums[left])
//        {
//            nums[++left] = nums[right];
//        }
//    }
//    return left + 1;
//
//}
//代码优化
//int removeDuplicates(int* nums, int numsSize) {
//    if (nums == NULL)
//        return 0;
//    int left = 0;
//    for (int right = 1; right < numsSize; right++)
//    {
//        if (nums[right] != nums[left])
//        {   //如果相邻元素，则不需要交换，做指针向后移移位
//            if ((right - left) > 1)
//            {
//                nums[++left] = nums[right];
//            }
//            else
//            {
//                left++;
//            }
//        }
//    }
//    return left + 1;
//}




//int maxProfit(int* prices, int pricesSize) {
//    if (prices == NULL || pricesSize < 2)
//        return 0;
//    //动态规划
//    // int dp[pricesSize][2];
//    // //初始条件
//    // dp[0][0]=0;
//    // dp[0][1]=-prices[0];
//
//    // for(int i=1; i<pricesSize; i++)
//    // {
//    //     //递归方程
//    //     dp[i][0]=fmax(dp[i-1][0],dp[i-1][1]+prices[i]);
//    //     dp[i][1]=fmax(dp[i-1][1],dp[i-1][0]-prices[i]);
//    // }
//    // return dp[pricesSize-1][0];
//
//
//    // //贪心算法
//    // int total=0;
//    // int index=0;
//    // while(index<pricesSize)
//    // { 
//    //     //如果股票跌，一直找。直到找到上涨为止
//    //     while(index<pricesSize-1 && prices[index]<prices[index+1])
//    //         index++;
//    //     int min=prices[index];
//    //     //如果股票上涨一直找，直到找到下跌为止
//    //     while(index<pricesSize-1 && prices[index+1]>prices[index])
//    //         index++;
//    //     total=prices[index]-min;
//    // }
//    // return total;
//
//    //  优化
//    int total = 0;
//    int index = 0;
//    while (index < pricesSize)
//    {
//        total += fmax(0, prices[index + 1] - prices[index]);
//    }
//    return total;
//}




//void rotate(int* nums, int numsSize, int k) {
//    int tmp[numsSize];
//    for (int i = 0; i < numsSize; i++)
//    {
//        tmp[i] = nums[i];
//    }
//    for (int i = 0; i < numsSize; i++)
//    {
//        nums[(i + k) % numsSize] = tmp[i];
//    }
//
//}

