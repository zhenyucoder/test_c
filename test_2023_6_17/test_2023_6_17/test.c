//int comp(int* a, int* b)
//{
//    return *a - *b;
//}
//
//int** threeSum(int* nums, int numsSize, int* returnSize, int** returnColumnSizes)
//{
//    qsort(nums, numsSize, sizeof(int), comp);
//
//    int** res = malloc(sizeof(int*) * 18000);
//    int count = 0;
//
//    for (int i = 0; i < numsSize - 2; i++)
//    {
//        int left = i + 1;
//        int right = numsSize - 1;
//
//        if (i >= 1 && nums[i] == nums[i - 1])
//        {
//            continue;
//        }
//       
//        else if (nums[i] > 0)
//        {
//            continue;
//        }
//        else
//            while (left < right)
//            {
//                int sum = nums[i] + nums[left] + nums[right];
//
//                if (sum > 0)
//                {
//                    right--;
//                }
//                else if (sum < 0)
//                {
//                    left++;
//                }
//
//                else if (sum == 0)
//                {
//                    res[count] = malloc(sizeof(int) * 3);
//                    res[count][0] = nums[i];
//                    res[count][1] = nums[left];
//                    res[count][2] = nums[right];
//                    count++;
//
//                    while (left < right && nums[left] == nums[left + 1])
//                    {
//                        left++;
//                    }
//                    while (left < right && nums[right] == nums[right - 1])
//                    {
//                        right--;
//                    }
//
//                    left++;
//                    right--;
//                }
//            }
//    }
//
//    *returnSize = count;
//    *returnColumnSizes = malloc(sizeof(int) * count);
//    for (int i = 0; i < count; i++)
//    {
//        returnColumnSizes[0][i] = 3;
//    }
//
//    return res;
//}





//
//void setZeroes(int** matrix, int row, int* matrixColSize)
//{
//    int col = *matrixColSize;
//    int flag_col0 = 0;
//    for (int i = 0; i < row; i++)
//    {
//        if (!matrix[i][0])    
//            flag_col0 = 1;
//        for (int j = 1; j < col; j++)
//        {
//            if (!matrix[i][j]) 
//                matrix[i][0] = matrix[0][j] = 0;
//        }
//    }
//
//    for (int i = row - 1; i >= 0; i--)
//    {
//        for (int j = 1; j < col; j++)
//        {
//            if (!matrix[i][0] || !matrix[0][j])
//                matrix[i][j] = 0;
//        }
//        if (flag_col0)
//            matrix[i][0] = 0;
//    }
//
//}




int cmp(const void* a, const void* b)
{
    return *(char*)a - *(char*)b;
}


typedef struct {
    char* sortedStr;
    int index;
    UT_hash_handle hh;
} str_index_struct;

char*** groupAnagrams(char** strs, int strsSize, int* returnSize,
    int** returnColumnSizes)
{
   
    char*** result = (char***)malloc(sizeof(char**) * strsSize);
    *returnSize = 0;
    *returnColumnSizes = (int*)malloc(sizeof(int) * strsSize);

    str_index_struct* table = NULL;

    for (int i = 0; i < strsSize; ++i) {
        //首先对字符串里的字符进行排序
        char* sortedStr = (char*)malloc(sizeof(char) * (strlen(strs[i]) + 1));
        strcpy(sortedStr, strs[i]);
        qsort(sortedStr, strlen(strs[i]), sizeof(char), cmp);

        //排序后的字符串是否在散列表里
        str_index_struct* s;
        HASH_FIND(hh, table, sortedStr, strlen(sortedStr), s);

        if (s == NULL) {
            //不在散列表里，加入新的键值——排序后的字符串和二级指针组末尾位置
            s = (str_index_struct*)malloc(sizeof(str_index_struct));
            s->sortedStr = sortedStr;

            //同时要把新的字符串插入结果中
            result[*returnSize] = (char**)malloc(sizeof(char*) * 5);
            (*returnColumnSizes)[*returnSize] = 0;
            result[*returnSize][((*returnColumnSizes)[*returnSize])++] = strs[i];

            s->index = (*returnSize)++;
            HASH_ADD_KEYPTR(hh, table, s->sortedStr, strlen(s->sortedStr), s);
        }

        else
        {
            result[s->index][((*returnColumnSizes)[s->index])++] = strs[i];

        }
           
    }

    return result;
}

。