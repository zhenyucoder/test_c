#include <iostream>
using namespace std;

//int main()
//{
//	int a = 10;
//	int* b = &a;
//	int& c = a;
//
//	++(*b);
//	c++;//引用的底层还是指针
//
//	cout << "c=" << c << endl;
//	int d = 1000;
//	c = d;//赋值
//	cout << "d=" << d << endl;
//	return 0;
//}


//int Add(int x, int y)
//{
//	int c = x + y;
//	return c;
//}
//inline int add(int x, int y)
//{
//	int c = x + y;
//	return c;
//}
//
////#define Add(x, y) x + y;----printf("%d\n",Add(1,2));
////#define ADD(x, y) x+y------printf("%d\n",Add(1,2)*3);
////#define ADD(x, y) (x+y)----printf("%d\n",Add(a|b,a&v));
//#define add(x,y) ((x)+(y))//宏，没有函数栈帧创建,
//
//int main()
//{
//	int C = Add(1, 2);
//	int c = add(3, 4);
//	cout << "C=" << C << endl;
//	cout << "c=" << c << endl;
//	return 0;
//}


//
//#include "Func.h"
//int main()
//{
//	//f(1);//内联函数不要声明和定义分离
//	fx();
//	return 0;
//}

//void Test()
//{
//    int a = 10;
//    //引用类型必须和引用实体是同种类型的
//    int& ra = a;//<====定义引用类型
//    printf("%p\n", &a);
//    printf("%p\n", &ra);
//}
//int main()
//{
//    Test();
//    return 0;
//}

//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 2);
//	Add(3, 4);
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}

//#include <time.h>
//struct A { int a[10000]; };
//A a;
//// 值返回
//A TestFunc1() { return a; }
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestReturnByRefOrValue();
//	return 0;
//}



int main()
{
	int a = 10;
	int& ra = a;
	ra = 20;
	int* pa = &a;
	*pa = 20;
	return 0;
}