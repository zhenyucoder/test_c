#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

//int main() {
//    int year, month, day;
//    while (~scanf("%d %d %d", &year, &month, &day))
//    {
//        int arr[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//        int sum = day;
//        int n = month;
//        while (--n)
//        {
//            sum += arr[n];
//        }
//        //闰年二月已上+1
//        if (month > 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//            sum++;
//        //输出
//        printf("%d", sum);
//    }
//    return 0;
//}



//int* printNumbers(int n, int* returnSize) {
//    *returnSize = pow(10, n) - 1;
//    int* a = (int*)malloc(*returnSize * sizeof(int));
//    for (int i = 0; i < *returnSize; i++)
//    {
//        //a[i++]=i+1;
//        *(a + i) = i + 1;
//    }
//    return a;
//}



//BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
//{
//	if (root == NULL)
//		return NULL;
//	if (root->data == x)
//		return root;
//	BTNode* ret1 = BinaryTreeFind(root->left, x);
//	if (ret1)
//		return ret1;
//	BTNode* ret2 = BinaryTreeFind(root->right, x);
//	if (ret2)
//		return ret2;
//	return NULL;
//}


//void BinaryTreeDestory(BTNode* root)
//{
//	if (root == NULL)
//		return;
//	BinaryTreeDestory(root->left);
//	BinaryTreeDestory(root->right);
//	free(root);
//}


//int main() {
//    int n;
//    while (~scanf("%d", &n))
//    {
//        int count1 = 0, count2 = 0, tmp;
//        float sum = 0;
//        for (int i = 0; i < n; i++)
//        {
//            scanf("%d", &tmp);
//            if (tmp < 0)
//                count1++;//负数个数
//            else if (tmp > 0)
//            {
//                count2++;
//                sum += tmp;
//            }
//        }
//        printf("%d ", count1);
//        if (count2)
//            printf("%.1f", sum / count2);
//        else
//            printf("0.0");
//    }
//    return 0;
//}



//int i;
//void prt()
//{
//	for (i = 5; i < 8; i++)
//		printf("%c", '*');
//	printf("\t");
//} 
//int main()
//{
//	for (i = 5; i <= 8; i++)
//		prt();
//	return 0;
//}


//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//    *returnSize = 2;
//    int* arr = (int*)calloc(numsSize + 1, sizeof(int));
//    int* ret = (int*)calloc(*returnSize, sizeof(int));//返回数组
//    int old_sum = 0, cur_sum = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (arr[nums[i]] == 1)
//            ret[0] = nums[i];
//
//        arr[nums[i]] = 1;
//        old_sum += nums[i];//err
//        cur_sum += i + 1;//true
//    }
//    ret[1] = ret[0] + cur_sum - old_sum;
//    free(arr);
//    return ret;
//}


//int main() {
//    int n;
//    while (~scanf("%d", &n))
//    {
//        char password[101] = { 0 };
//        int upper = 0, lower = 0, digital = 0;
//        for (int i = 0; i < n; i++)
//        {
//            scanf("%s", password);
//            if (strlen(password) < 8)//长度小于9
//            {
//                printf("NO\n");
//                continue;
//            }
//            if (password[0] >= '0' && password[0] <= '9')//以数字开头
//            {
//                printf("NO\n");
//                continue;
//            }
//
//            char* cur = password;
//            while (*cur != '\0')//统计各种字符串
//            {
//                if (*cur >= 'a' && *cur <= 'z')
//                    lower++;
//                if (*cur >= 'A' && *cur <= 'Z')
//                    upper++;
//                if (*cur >= '0' && *cur <= '9')
//                    digital++;
//
//                cur++;
//            }
//            //三种字符至少出现两种
//            if ((lower > 0) + (lower > 0) + (digital) < 2)
//            {
//                printf("NO\n");
//                continue;
//            }
//            printf("YES\n");
//        }
//    }
//    return 0;
//}


//int main()
//{
//	printf("%d\n", printf("1\n"));
//	return 0;
//}



//int minArray(int* numbers, int numbersSize) {
//    int left = 0, right = numbersSize - 1, mid;
//    if (numbers[left] < numbers[right])
//        return  numbers[left];
//    while (left < right)
//    {
//        mid = left + (right - left) / 2;
//        if (numbers[mid] > numbers[right])
//            left = mid + 1;
//        else if (numbers[mid] < numbers[right])
//            right = mid;
//        else
//            right--;
//    }
//    return numbers[right];
//}



//int findMin(int* nums, int numsSize) {
//    int left = 0, right = numsSize - 1, mid;
//    while (left < right)
//    {
//        mid = left + (right - left) / 2;
//        if (nums[mid] > nums[right])
//            left = mid + 1;
//        else
//            right = mid;
//    }
//    return nums[left];
//}




//int Find(int* nums, int numsLen, int k, int flag)
//{
//    int left = 0, right = numsLen - 1, mid;
//    while (left < right)
//    {
//        mid = left + (right - left) / 2;
//        if (nums[mid] < k)
//            left = mid + 1;
//        else if (nums[mid] > k)
//            right = mid - 1;
//        else
//        {
//            //重复数字处理
//            if (flag == -1)
//            {
//                //返回最右下标
//                if (nums[mid + 1] != k)
//                    return mid;
//                else
//                    left = mid + 1;
//            }
//            else
//            {
//                //返回最左下标
//                if (nums[mid - 1] != k)
//                    return mid;
//                else
//                    right = mid - 1;
//            }
//
//        }
//    }
//    if (nums[left] == k)
//        return left;
//
//    return -1;
//}
//
//int GetNumberOfK(int* nums, int numsLen, int k) {
//    if (numsLen == 0)
//        return 0;
//    int Left_GetNumberOfK = Find(nums, numsLen, k, 0);
//    int Right_GetNumberOfK = Find(nums, numsLen, k, -1);//5
//    if (Right_GetNumberOfK == -1 && Left_GetNumberOfK == -1)//没有找到k的数据
//        return 0;
//    return Right_GetNumberOfK - Left_GetNumberOfK + 1;
//}
//
//int main()
//{
//    int nums[] = { 1,2,3,  3,3,  3,4,5 };
//    GetNumberOfK(nums, 8, 3);
//    return 0;
//}


//int Positive(int* nums, int numsLen, double k)
//{
//    int left = 0, right = numsLen - 1, mid;
//    while (left <= right)
//    {
//        mid = left + (right - left) / 2;
//        if (nums[mid] < k)
//            left = mid + 1;
//        else if (nums[mid] > k)
//            right = mid - 1;
//    }
//    return left;
//}
//int GetNumberOfK(int* nums, int numsLen, int k) {
//    return Positive(nums, numsLen, k + 0.5) - Positive(nums, numsLen, k - 0.5);
//}



//int dominantIndex(int* nums, int numsSize) {
//    if (numsSize == 1)
//        return -1;
//    int max = nums[0], sec = nums[1], index = 0;
//    if (nums[0] < nums[1])
//    {
//        max = nums[1];
//        sec = nums[0];
//        index = 1;
//    }
//    for (int i = 2; i < numsSize; i++)
//    {
//        int tmp = 0;
//        if (nums[i] > sec)
//        {
//            if (nums[i] > max)
//            {
//                tmp = max;
//                max = nums[i];
//                sec = tmp;
//
//                index = i;
//            }
//            else
//            {
//                sec = nums[i];
//            }
//        }
//    }
//
//    //判断
//    return  max >= sec*2 ? index: -1;
//}


//int compar(const void* p1, const void* p2)
//{
//    return *(int*)p1 - *(int*)p2;
//}
//int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
//    qsort(nums1, nums1Size, sizeof(int), compar);
//    qsort(nums2, nums2Size, sizeof(int), compar);
//    int idx1 = 0, idx2 = 0;
//    *returnSize = 0;
//    int* ret = (int*)malloc(sizeof(int) * nums1Size);
//    while (idx1 < nums1Size && idx2 < nums2Size)
//    {
//        if (nums1[idx1] < nums2[idx2])
//            idx1++;
//        else if (nums1[idx1] > nums2[idx2])
//            idx2++;
//        else
//        {
//            //保证元素唯一性
//            if (*returnSize == 0 || nums1[idx1] != ret[*returnSize - 1])
//            {
//                ret[(*returnSize)++] = nums1[idx1];
//            }
//            idx1++;
//            idx2++;
//        }
//    }
//    return ret;
//}



//int main() {
//    char str[3] = { 0 };
//    while (gets(str))
//    {
//        int len = strlen(str);
//        for (int i = 0; i < len - 1; i++)
//        {
//            for (int j = 0; j < len - 1 - i; j++)
//            {
//                if (str[j] > str[j + 1])
//                {
//                    char tmp = str[j];
//                    str[j] = str[j + 1];
//                    str[j + 1] = tmp;
//                }
//            }
//        }
//        printf("%s\n", str);
//    }
//    return 0;
//}




//int pivotIndex(int* nums, int numsSize) {
//    int total = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        total += nums[i];
//    }
//    int LSum = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (LSum * 2 + nums[i] == total)
//            return i;
//        LSum += nums[i];
//    }
//    return -1;
//}



//int compar(const void* p1, const void* p2)
//{
//    return *(char*)p1 - *(char*)p2;
//}
//
//int main() {
//    char str[501] = { 0 };
//    while (gets(str))
//    {
//        int len = strlen(str);
//        qsort(str, len, sizeof(char), compar);//升序
//
//        int count = 1;
//        for (int i = 1; i < len; i++)
//        {
//            if (str[i] != str[i - 1])
//                count++;
//        }
//        printf("%d\n", count);
//    }
//    return 0;
//}



//int compar(const void* p1, const void* p2)
//{
//    return *(int*)p1 - *(int*)p2;
//}
//
//int majorityElement(int* nums, int numsSize) {
//    qsort(nums, numsSize, sizeof(int), compar);
//    return nums[numsSize / 2];
//
//}


