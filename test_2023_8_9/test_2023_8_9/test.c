#include <stdio.h>
#include <stdlib.h>




//int* reversePrint(struct ListNode* head, int* returnSize) {
//    struct ListNode* cur = head;
//    int count = 0;
//    while (cur)
//    {
//        count++;
//        cur = cur->next;
//    }
//    *returnSize = count;
//    int* ans = (int*)malloc(count * sizeof(int));
//    cur = head;
//    while (cur)
//    {
//        ans[--count] = cur->val;
//        cur = cur->next;
//    }
//    return ans;
//}



//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* newnode = NULL, * cur = head;
//    while (cur)
//    {
//		  //头插
//        struct ListNode* next = cur->next;
//        cur->next = newnode;
//        newnode = cur;
//        cur = next;
//    }
//    return newnode;
//}

//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* prev = NULL, * cur = head;
//    if (cur == NULL || cur->next == NULL)
//    {
//        return cur;
//    }
//
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//
//        //链接方向反向
//        cur->next = prev;
//
//        prev = cur;
//        cur = next;
//        if (cur != NULL)
//            next = cur->next;
//    }
//    return prev;
//}



//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    if (list1 == NULL)
//        return list2;
//    if (list2 == NULL)
//        return list1;
//    struct ListNode* head = NULL, * tail = NULL;
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            if (tail == NULL)
//                tail = head = list1;
//            else
//            {
//                tail->next = list1;
//                tail = tail->next;
//            }
//            list1 = list1->next;
//        }
//        else
//        {
//            if (tail == NULL)
//                tail = head = list2;
//            else
//            {
//                tail->next = list2;
//                tail = tail->next;
//            }
//            list2 = list2->next;
//        }
//    }
//
//    if (list1)
//        tail->next = list1;
//    if (list2)
//        tail->next = list2;
//    return head;
//}



//struct ListNode* FindFirstCommonNode(struct ListNode* pHead1, struct ListNode* pHead2) {
//    if (pHead1 == NULL || pHead2 == NULL)
//    {
//        return NULL;
//    }
//    struct ListNode* cur1 = pHead1, * cur2 = pHead2;
//    int len1 = 1, len2 = 1;
//    while (cur1->next)
//    {
//        len1++;
//        cur1 = cur1->next;
//    }
//    while (cur2->next)
//    {
//        len2++;
//        cur2 = cur2->next;
//    }
//
//    //不相交
//    if (cur1 != cur2)
//        return NULL;
//
//    int gap = abs(len1 - len2);
//    struct ListNode* longNode = pHead1, * shortNode = pHead2;
//    if (len1 < len2)
//    {
//        longNode = pHead2;
//        shortNode = pHead1;
//    }
//
//    //长链表先走差距步
//    while (gap--)
//    {
//        longNode = longNode->next;
//    }
//
//    //同时走
//    while (longNode != shortNode)
//    {
//        longNode = longNode->next;
//        shortNode = shortNode->next;
//    }
//    return longNode;
//}



//struct ListNode* EntryNodeOfLoop(struct ListNode* pHead) {
//    struct ListNode* fast = pHead, * slow = pHead;
//    while (fast && fast->next)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//        if (fast == slow)//相遇
//        {
//            struct ListNode* meet = fast;
//            while (pHead != meet)
//            {
//                pHead = pHead->next;
//                meet = meet->next;
//            }
//            return meet;
//        }
//    }
//
//    //没有环
//    return NULL;
//
//}



struct ListNode* FindKthToTail(struct ListNode* pHead, int k) {
    struct ListNode* fast = pHead, * slow = pHead;
    while (k--)
    {
        if (fast == NULL)
            return NULL;
        fast = fast->next;
    }

    while (fast)
    {
        fast = fast->next;
        slow = slow->next;
    }
    return slow;
}



