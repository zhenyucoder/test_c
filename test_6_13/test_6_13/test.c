#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//递归
//int fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return fib(n - 1) + fib(n - 2);
//}
//迭代
//int fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = fib(n);
//	printf("%d\n", ret);
//	return 0;
//}




//void swap(char* a, char* b) 
//{
//    int tmp = *a;
//    *a = *b;
//    *b = tmp;
//}
//void reverseString(char* s, int sSize) 
//{
//    int left, right;
//    for (left = 0, right = sSize - 1; left < right; ++left, --right) 
//    {
//        swap(s + left, s + right);
//    }
//}




//int reverse(int x) 
//{
//    int n = pow(2, 31) - 1;
//    int cnt = 0;
//    while (x != 0) 
//    {
//        if (abs(cnt) > n / 10) 
//            return 0;
//        cnt = x % 10 + cnt * 10;
//        x /= 10;
//    }
//    return cnt;
//}




//int firstUniqChar(char* s) 
//{
//	char p;
//	int l = strlen(s);
//	int i = 0;
//
//	for (i = 0; i < l; i++)
//	{
//		p = s[i];//等下变回去
//		s[i] = '0';//不返回自己
//		if (strchr(s, p) == NULL)
//			return i;
//		s[i] = p;
//
//	}
//	return -1;
//}
//



//int cmp(const void* _a, const void* _b) 
//{
//    char a = *(char*)_a, b = *(char*)_b;
//    return a - b;
//}
//
//bool isAnagram(char* s, char* t) 
//{
//    int len_s = strlen(s), len_t = strlen(t);
//    if (len_s != len_t) 
//    {
//
//        return false;
//    }
//    qsort(s, len_s, sizeof(char), cmp);
//    qsort(t, len_t, sizeof(char), cmp);
//    return strcmp(s, t) == 0;
//}





//class Solution 
//{
//    public boolean isPalindrome(String s) 
//    {
//        s = s.toLowerCase();
//        int first = 0, last = s.length() - 1;
//
//        while (first < last) 
//        {
//            while (first < last && !Character.isLetterOrDigit(s.charAt(first))) 
//            {
//                first++;
//            }
//            while (first < last && !Character.isLetterOrDigit(s.charAt(last))) 
//            {
//                last--;
//            }
//
//            if (s.charAt(first) != s.charAt(last)) 
//            {
//                return false;
//            }
//            first++;
//            last--;
//        }
//        return true;
//    }
//}
//




int strStr(char* haystack, char* needle) 
{
    int i = 0, j = 0;
    if (strlen(needle) > strlen(haystack))
    {
        return -1;
    }
    else
    {
        for (i = 0; i <= strlen(haystack) - strlen(needle); ++i)
        {
            for (j = 0; j < strlen(needle); ++j)
            {
                if (haystack[i + j] != needle[j])
                {
                    break;
                }
            }
            if (j == strlen(needle))
            {
                return i;
            }
        }
    }
    return -1;
}