#pragma once

#include <iostream>
#include <memory>s
#include <unordered_map>

// 先描述
class Session
{
private:
    std::string username;
    std::string passwd;
    int status;
    uint64_t login_time;
    //...
public:
    Session(/* args */) {};
    ~Session() {};
};

using session_ptr = std::unique_ptr<Session>;

class SessionManager
{
public:
    SessionManager() {}
    ~SessionManager() {}
    // 返回值就是sessionid
    std::string AddSession(std::string username, std::string passwd)
    {
        //
    }
    bool DelSession(std::string &sessionid)
    {
    }
    void ModSession(std::string &sessionid, session_ptr session)
    {
    }
    session_ptr SearchSession(std::string &sessionid)
    {
    }

private:
    // 在组织
    // std::string: sessionid
    // session_ptr: 用户登录信息
    std::unordered_map<std::string, session_ptr> sessions;
};