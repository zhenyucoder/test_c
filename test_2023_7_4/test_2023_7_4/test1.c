//bool  containsDuplicate(int* nums, int numsSize) {
//    int i = 0;
//    int tmp = 0;
//    //冒泡排序，变成升序
//    for (i = 0; i < numsSize - 1; i++)
//    {
//        for (int j = 0; j < numsSize - 1 - i; j++)
//        {
//            if (nums[j] > nums[j + 1])
//            {
//                tmp = nums[j];
//                nums[j] = nums[j + 1];
//                nums[j + 1] = tmp;
//            }
//        }
//    }
//    //判断是否有元素相同
//    for (i = 0; i < numsSize - 1; i++)
//    {
//        if (nums[i] == nums[i + 1])
//            return true;
//    }
//    return false;
//}

//优化
//int compare(const void* a, const void* b)
//{
//    return *(int*)a - *(int*)b;
//}
//bool containsDuplicate(int* nums, int numsSize) {
//    qsort(nums, numsSize, sizeof(int), compare);
//    for (int i = 0; i < numsSize - 1; i++)
//    {
//        if (nums[i] == nums[i + 1])
//            return true;
//    }
//    return false;
//}





//
//int singleNumber(int* nums, int numsSize) {
//    int result = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        result ^= nums[i];
//    }
//    return result;
//}



//int cmp(const void* a, const void* b)
//{
//    return *(int*)a - *(int*)b;
//}
//
//int* intersect(int* nums1, int nums1Size, int* nums2, int nums2Size,
//    int* returnSize) {
//    qsort(nums1, nums1Size, sizeof(int), cmp);
//    qsort(nums2, nums2Size, sizeof(int), cmp);
//    *returnSize = 0;
//    int* intersection = (int*)malloc(sizeof(int) * fmin(nums1Size, nums2Size));
//    int index1 = 0, index2 = 0;
//    while (index1 < nums1Size && index2 < nums2Size)
//    {
//        if (nums1[index1] < nums2[index2]) 
//        {
//            index1++;
//        }
//        else if (nums1[index1] > nums2[index2])
//        {
//            index2++;
//        }
//        else 
//        {
//            intersection[(*returnSize)++] = nums1[index1];
//            index1++;
//            index2++;
//        }
//    }
//    return intersection;
//}




int* plusOne(int* digits, int digitsSize, int* returnSize) {
    for (int i = digitsSize - 1; i >= 0; i--) {
        if (digits[i] != 9) 
        {   //检测数字非9，则进行加一，并及时返回
            digits[i]++;
            break;
        }
        digits[i] = 0;  //当数字为9时，则将它置零（除了数字全为9）
    }
    if (digits[0] == 0) 
    {  //当数字全为9时（这里由于经过上面的循环，首位应该变为0，即99变为00）
        int* temp;    //创建新数组
        int Size = digitsSize + 1; 
        int k = Size - 1;
        temp = (int*)malloc(Size * sizeof(int));  
        while ((k) != 0) 
        {
            temp[k] = 0;
            k--;
        }
        temp[k] = 1;  //首位置一，其它置零
        return temp;
    }
    *returnSize = digitsSize;
    return digits;
}