//struct ListNode* deleteNode(struct ListNode* head, int val) {
//    if (head == NULL)
//        return NULL;
//
//    struct ListNode* cur = head, * newhead = head;
//    while (cur)
//    {
//        if (cur->val == val)
//        {
//            if (cur == head)
//                newhead = cur->next;
//            else
//            {
//                struct ListNode* prev = head;
//                while (prev->next != cur)
//                {
//                    prev = prev->next;
//                }
//                prev->next = cur->next;
//            }
//            break;
//        }
//
//        cur = cur->next;
//    }
//    return newhead;
//}


//struct ListNode* deleteNode(struct ListNode* head, int val) {
//    //哨兵位plist
//    struct ListNode* plist = (struct ListNode*)malloc(sizeof(struct ListNode));
//    plist->next = head;
//
//    struct ListNode* cur = head, * prev = plist;
//    while (cur)
//    {
//        if (cur->val == val)
//        {
//            prev->next = cur->next;
//            break;
//        }
//
//        cur = cur->next;
//        prev = prev->next;
//    }
//    return plist->next;
//}



//struct ListNode* deleteDuplication(struct ListNode* pHead) {
//    if (pHead == NULL || pHead->next == NULL)
//        return pHead;
//
//    struct ListNode* prev = NULL, * newpHead = NULL;
//    struct ListNode* cur = pHead, * next1 = pHead->next;
//
//    int flag = 0;
//    while (next1)
//    {
//        if (cur->val == next1->val)
//        {
//            next1 = next1->next;
//            flag = 1;
//            continue;
//        }
//
//        if (flag)
//        {
//            flag = 0;
//            cur->next = next1;
//            cur = cur->next;
//            next1 = next1->next;
//            continue;
//        }
//
//        if (newpHead == NULL)
//        {
//            newpHead = prev = cur;
//        }
//        else
//        {
//            prev->next = cur;
//            prev = prev->next;
//        }
//        cur->next = next1;
//        cur = cur->next;
//        next1 = next1->next;
//    }
//
//    if (cur->next == NULL)
//    {
//        if (newpHead == NULL)
//            newpHead = cur;
//        else
//            prev->next = cur;
//    }
//    else
//    {
//        if (prev)
//        {
//            prev->next = NULL;
//        }
//    }
//    return newpHead;
//}



//struct ListNode* deleteDuplication(struct ListNode* pHead) {
//    if (pHead == NULL)
//        return NULL;
//    struct ListNode* SentinelPosition = (struct ListNode*)malloc(sizeof(struct ListNode));
//    SentinelPosition->next = pHead;
//    struct ListNode* cur = SentinelPosition;
//    while (cur->next != NULL && cur->next->next != NULL)
//    {
//        //遇到两个节点值相同的
//        if (cur->next->val == cur->next->next->val)
//        {
//            int tmp = cur->next->val;
//            while (cur->next != NULL && cur->next->val == tmp)
//            {
//                cur->next = cur->next->next;
//            }
//        }
//        else
//        {
//            cur = cur->next;
//        }
//    }
//    return SentinelPosition->next;
//}



//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* newhead = NULL, * cur = head;
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//        cur->next = newhead;
//        newhead = cur;
//        cur = next;
//    }
//    return newhead;
//}



//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* fast = head, * slow = head;
//    while (fast != NULL && fast->next != NULL)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//    }
//    return slow;
//}


struct Node* copyRandomList(struct Node* head) {
    struct Node* cur = head;
    while (cur)
    {
        struct Node* next = cur->next;
        struct Node* copy = (struct Node*)malloc(sizeof(struct Node));
        copy->val = cur->val;
        //链接
        cur->next = copy;
        copy->next = next;


        cur = next;
    }


    cur = head;
    while (cur)
    {
        struct Node* copy = cur->next;
        if (cur->random == NULL)
            copy->random = NULL;
        else
            copy->random = cur->random->next;


        cur = copy->next;
    }


    cur = head;
    struct Node* copyhead = NULL, * copytail = NULL;
    while (cur)
    {
        struct Node* copy = cur->next;
        struct Node* next = copy->next;
        //copy节点尾插到新节点
        if (copytail == NULL)
        {
            copyhead = copytail = copy;
        }
        else
        {
            copytail->next = copy;
            copytail = copytail->next;
        }


        //恢复原链表
        cur->next = next;
        cur = next;
    }
    return copyhead;
}