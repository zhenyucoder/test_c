//class Solution {
//public:
//    int findNumberOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> len(n, 1), count(n, 1);
//        int maxlen = 1, maxcount = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (nums[i] > nums[j])
//                {
//                    if (len[j] + 1 == len[i])
//                        count[i] += count[j];
//                    else if (len[j] + 1 > len[i])
//                        len[i] = len[j] + 1, count[i] = count[j];
//                }
//            }
//            if (len[i] == maxlen)
//                maxcount += count[i];
//            else if (len[i] > maxlen)
//                maxlen = len[i], maxcount = count[i];
//        }
//        return maxcount;
//    }
//};


//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size(), ret = 1;
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (pairs[i][0] > pairs[j][1])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                    break;
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        unordered_map<int, int> hash;//arr[i]��dp[i]��ӳ��
//        hash[arr[0]] = 1;
//        int ret = 0;
//        for (int i = 1; i < arr.size(); i++)
//        {
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            ret = max(ret, hash[arr[i]]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        hash[nums[0]] = 0;
//        int n = nums.size(), ret = 2;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        for (int i = 1; i < n - 1; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                int k = 2 * nums[i] - nums[j];
//                if (hash.count(k))
//                {
//                    dp[i][j] = dp[hash[k]][i] + 1;
//                    ret = max(ret, dp[i][j]);
//                }
//            }
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        unordered_map<long long, vector<long long>> hash;
//        for (long long i = 0; i < nums.size(); i++)
//            hash[nums[i]].push_back(i);
//        int n = nums.size(), ret = 0;
//        vector<vector<long long>> dp(n, vector<long long>(n));
//        for (int j = 2; j < n; j++)
//        {
//            for (int i = 1; i < j; i++)
//            {
//                long long target = (long long)2 * nums[i] - nums[j];
//                if (hash.count(target))
//                {
//                    for (auto k : hash[target])
//                    {
//                        if (k < i)
//                            dp[i][j] += dp[k][i] + 1;
//                    }
//                }
//                ret += dp[i][j];
//            }
//        }
//        return ret;
//    }
//};