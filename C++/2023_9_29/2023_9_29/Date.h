#pragma once

#include<iostream>
using namespace std;

class Date
{
public:
	int GetMonthDay(int year, int month);
	Date(int year = 1, int month = 1, int day = 1);
	Date(const Date& d);
	// 赋值运算符重载
	Date& operator=(const Date& d);
	~Date();

	//日期、天数
	Date& operator+=(int day);
	Date operator+(int day);
	Date operator-(int day);
	Date& operator-=(int day);
	int operator-(const Date& d);

	//运算符重载
	bool operator>(const Date& d);
	bool operator==(const Date& d);
	bool operator >= (const Date& d);
	bool operator < (const Date& d);
	bool operator <= (const Date& d);
	bool operator != (const Date& d);
private:
	int _year;
	int _month;
	int _day;
};