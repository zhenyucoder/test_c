#include "Date.h"

void TestDate1()
{
	Date d1;
	Date d2(2023, 9, 29);
	int monthret = d2.GetMonthDay(2000, 2);

	Date d3(d2);
	Date& d4 = d3 += 3;
}

void TestDate2()
{
	Date d1(2023, 10, 1);
	Date ret1 = d1 + 4;
	Date ret2 = d1 - 30;
	Date d2 = d1 + 3;
}

void TestDate3()
{
	Date d1(2023, 9, 29);
	Date d2(2023, 10, 4);
	Date d3(d2);
	cout << (d1 > d2) << endl;
	cout << (d1 == d2) << endl;
	cout << (d3 == d2) << endl;
}
int main()
{
	TestDate3();
	return 0;
}