//#include <iostream>
//#include <vector>
//using namespace std;
//
//int V, n, ret = 0x3f3f3f3f;
//
//void dfs(vector<int>& arr, int PrevSum, int pos)
//{
//    if (PrevSum <= V && pos <= n)
//        ret = min(ret, V - PrevSum);
//    for (int i = pos; i < n; i++)
//    {
//        if (PrevSum + arr[i] <= V)
//            dfs(arr, PrevSum + arr[i], i + 1);
//    }
//}
//
//int main()
//{
//    cin >> V >> n;
//    vector<int> arr(n);
//    for (int i = 0; i < n; i++)
//        cin >> arr[i];
//    dfs(arr, 0, 0);
//    cout << ret << endl;
//    return 0;
//}



//#include <iostream>
//#include <string>
//using namespace std;
//
//int Diff(string str1, string str2)
//{
//    int ret = 0;
//    for (int i = 0; i < str2.size(); i++)
//    {
//        if (str1[i] != str2[i]) ret++;
//    }
//    return ret;
//}
//int main()
//{
//    string A, B;
//    cin >> B >> A;
//    int ans = 100;
//    for (int i = 0, base = A.size(), len = B.size();
//        i + len <= base; i++)
//    {
//        ans = min(ans, Diff(A.substr(i, len), B));
//    }
//    cout << ans << endl;
//    return 0;
//}



/*class Solution
{
public:
	bool IsBalanced_Solution(TreeNode* pRoot)
	{
		return dfs(pRoot) != -1;
	}
	int dfs(TreeNode* root) 
	{
		if (root == nullptr) return 0;
		int left = dfs(root->left);
		if (left == -1) return -1;
		int right = dfs(root->right);
		if (right == -1) return -1;
		return abs(left - right) <= 1 ? max(left, right) + 1 : -1;
	}
}*/;


using namespace std;
int n;
string s;
int main()
{
	cin >> n >> s;
	int sum[2] = { 0 };
	for (auto ch : s)
	{
		sum[ch - '0']++;
	}

	int left = 0, right = 0, ret = 0, half = n / 2;
	int count[2] = { 0 }; 
	while (right < n - 1)
	{
		count[s[right] - '0']++;
		while (right - left + 1 > half)
		{
			count[s[left++] - '0']--;
		}
		if (right - left + 1 == half)
		{
			if (count[0] * 2 == sum[0] && count[1] * 2 == sum[1])
			{
				ret += 2;
			}
		}
		right++;
	}

	cout << ret << endl;

	return 0;
}