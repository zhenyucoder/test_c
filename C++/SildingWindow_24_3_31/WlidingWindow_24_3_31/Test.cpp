//class Solution {
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;//统计数子频率
//        for (int left = 0, right = 0; right < nums.size(); right++)
//        {
//            hash[nums[right]]++;//进窗口
//            if (right - left > k)//出窗口
//            {
//                hash[nums[left++]]--;
//            }
//            //更新结果
//            if (hash[nums[right]] > 1)
//                return true;
//        }
//        return false;
//    }
//}


//class Solution {
//public:
//    int findLHS(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int ret = 0, n = nums.size();
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            //出窗口
//            while (nums[right] - nums[left] > 1)
//                left++;
//            //更新结果
//            if (nums[right] != nums[left])
//                ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int findLHS(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        for (auto x : nums)//统计nums中各数字频率
//            hash[x]++;
//        int ret = 0;
//        for (auto x : nums)
//            if (hash[x + 1])
//                ret = max(ret, hash[x] + hash[x + 1]);
//        return ret;
//    }
//};


//class Solution {
//public:
//    double findMaxAverage(vector<int>& nums, int k) {
//        int ret = INT_MIN, n = nums.size();
//        for (int left = 0, right = 0, sum = 0; right < n; right++)
//        {
//            sum += nums[right];//进窗口
//            if (right - left + 1 > k)
//                sum -= nums[left++];//出窗口
//            //更新结果
//            if (right - left + 1 == k)
//                ret = max(ret, sum);
//        }
//        return (double)ret / k;
//    }
//};


//class Solution {
//public:
//    unordered_map<int, bool> hash;//统计是否存在
//    bool findTarget(TreeNode* root, int k) {
//        if (root == nullptr)
//            return false;
//        if (hash[k - root->val])
//            return true;
//        hash[root->val] = true;
//        return findTarget(root->left, k) || findTarget(root->right, k);
//    }
//};


//class Solution {
//public:
//    int maximumLengthSubstring(string s) {
//        int ret = 2, n = s.size();
//        //unordered_map<char, int> hash;//统计频率
//        int hash[26] = { 0 };
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            //进窗口
//            hash[s[right] - 'a']++;
//            while (hash[s[right] - 'a'] > 2)//出窗口
//            {
//                hash[s[left++] - 'a']--;
//            }
//            //更新结果
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int countGoodSubstrings(string s) {
//        int ret = 0, n = s.size();
//        int hash[26] = { 0 };
//        int count = 0;//统计有效字符个数
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            //进窗口, 维护count
//            if (hash[s[right] - 'a']++ == 0)
//                count++;
//            if (right - left + 1 == 3)
//            {
//                if (count == 3)//更新结果
//                    ret++;
//                //出窗口
//                if (--hash[s[left++] - 'a'] == 0)
//                    count--;
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minimumDifference(vector<int>& nums, int k) {
//        sort(nums.begin(), nums.end());
//        int ret = INT_MAX;
//        for (int left = 0, right = k - 1; right < nums.size(); right++)
//        {
//            ret = min(ret, nums[right] - nums[left++]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int divisorSubstrings(int num, int k) {
//        long long N = pow(10, k);
//        long long cur = num, ret = 0;
//        while (cur * 10 >= N)
//        {
//            long long tmp = cur % N;
//            if (tmp && num % tmp == 0)
//                ret++;
//            cur /= 10;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minimumRecolors(string blocks, int k) {
//        int ret = INT_MAX, n = blocks.size();
//        for (int left = 0, right = 0, count = 0; right < n; right++)
//        {
//            if (blocks[right] == 'W')//进窗口
//                count++;
//            int gap = right - left + 1;
//            if (gap > k)//出窗口
//                if (blocks[left++] == 'W')
//                    count--, gap--;
//            //更新结果
//            if (gap == k)
//                ret = min(ret, count);
//        }
//        return ret;
//    }
//};



