#include <iostream>
using namespace std;
#include<vector>
#include<list>
#include<deque> 
#include "stack.h"
#include "queue.h"
#include "priority_queue.h"
#include <time.h>

void test_stack()
{
	achieveStack::stack<int> st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);

	while (!st.empty())
	{
		cout << st.top() << " ";
		st.pop();
	}
	cout << endl;
}

template<class T>
class Less
{
public:
	bool operator()(const T& x, const T& y)
	{
		return x < y;
	}
};

template<class T>
class Greater
{
public:
	bool operator()(const T& x, const T& y)
	{
		return x > y;
	}
};

void test_queue()
{
	srand((unsigned)time(0));
	achieveQueue::queue<int> q;
	for (int i = 0; i < 10; i++)
	{
		q.push(rand());
	}

	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
}

void test_priority_queue()
{
	achievePriority_queue::priority_queue<int,vector<int>, Greater<int>> pq;
	pq.push(1);
	pq.push(5);
	pq.push(3);
	pq.push(2);
	pq.push(6);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
}

int main()
{
	//test_stack();
	//test_queue();
	test_priority_queue();
	return 0;
}


