//class Solution {
//public:
//    bool vis[310][310] = { 0 };
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int m, n;
//    int numIslands(vector<vector<char>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == '1' && !vis[i][j])
//                {
//                    ret++;
//                    vis[i][j] = true;
//                    dfs(grid, i, j);
//                }
//            }
//        }
//        return ret;
//    }
//
//    void dfs(vector<vector<char>>& grid, int i, int j)
//    {
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !vis[x][y])
//            {
//                vis[x][y] = true;
//                dfs(grid, x, y);
//            }
//        }
//    }
//};



//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int m, n, ret = 0;
//    bool vis[60][60] = { 0 };
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    vis[i][j] = true;
//                    ret = max(ret, dfs(grid, i, j, 1));
//                }
//            }
//        }
//        return ret;
//    }
//
//    int dfs(vector<vector<int>>& grid, int i, int j, int area)
//    {
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//            {
//                vis[x][y] = true;
//                area += dfs(grid, x, y, 1);
//            }
//        }
//        return area;
//    }
//};



//class Solution {
//public:
//    int m, n;
//    bool vis[210][210];
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    void solve(vector<vector<char>>& board) {
//        m = board.size(), n = board[0].size();
//        // 1.将所有与边界相连'O'区域对应vis[][]修改为true，标记
//        for (int i = 0; i < m; i++)
//        {
//            if (board[i][0] == 'O' && !vis[i][0])
//                dfs(board, i, 0);
//            if (board[i][n - 1] == 'O' && !vis[i][n - 1])
//                dfs(board, i, n - 1);
//        }
//
//        for (int j = 0; j < n; j++)
//        {
//            if (board[0][j] == 'O' && !vis[0][j])
//                dfs(board, 0, j);
//            if (board[m - 1][j] == 'O' && !vis[m - 1][j])
//                dfs(board, m - 1, j);
//        }
//
//        // 所有vis为false的'O'为待修改被围绕区域
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == 'O' && !vis[i][j])
//                    board[i][j] = 'X';
//            }
//        }
//        return;
//    }
//
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O' && !vis[x][y])
//                dfs(board, x, y);
//        }
//    }
//};




//class Solution {
//public:
//    int m, n;
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
//        m = heights.size(), n = heights[0].size();
//        vector<vector<bool>> atl(m, vector<bool>(n)), pac = atl;
//        for (int i = 0; i < m; i++)
//        {
//            dfs(heights, i, 0, pac);
//            dfs(heights, i, n - 1, atl);
//        }
//
//        for (int j = 0; j < n; j++)
//        {
//            dfs(heights, 0, j, pac);
//            dfs(heights, m - 1, j, atl);
//        }
//
//        vector<vector<int>> ret;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (pac[i][j] && atl[i][j])
//                    ret.push_back({ i, j });
//            }
//        }
//        return ret;
//    }
//
//    void dfs(vector<vector<int>>& heights, int i, int j, vector<vector<bool>>& vis)
//    {
//        if (vis[i][j]) return;
//        vis[i][j] = true;
//
//        for (int k = 0; k < 4; k++)
//        {
//            int x = dx[k] + i, y = dy[k] + j;
//            if (x >= 0 && x < m && y >= 0 && y < n && vis[x][y] == false && heights[x][y] >= heights[i][j])
//                dfs(heights, x, y, vis);
//        }
//    }
//};



