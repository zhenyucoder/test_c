//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int len = 1, begin = 0;
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = j - i > 1 ? dp[i + 1][j - 1] : true;
//                if (dp[i][j] && j - i + 1 > len)
//                    begin = i, len = j - i + 1;
//            }
//        return s.substr(begin, len);
//
//    }
//};




//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int ret = 0;
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = j - i > 1 ? dp[i + 1][j - 1] : true;
//                if (dp[i][j])
//                    ret++;
//            }
//        return ret;
//    }
//};




//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        unordered_map<string, int> hash;
//        for (auto& str : wordDict)
//            hash[str]++;
//        int n = s.size();
//        vector<bool> dp(n + 1);
//        dp[0] = true;//初始化
//        s = ' ' + s;//原始字符串下标统一加1
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = i; j >= 1; j--)
//            {
//                if (dp[j - 1] && hash.count(s.substr(j, i - j + 1)))
//                {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }
//        return dp[n];
//    }
//};




class Solution {
public:
    bool checkPartitioning(string s) {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));
        for (int i = n - 1; i >= 0; i--)//填dp表
        {
            for (int j = i; j < n; j++)
                if (s[i] == s[j])
                    dp[i][j] = j - i > 1 ? dp[i + 1][j - 1] : true;
        }
        //遍历判断可分割为3个非空回文子字符串
        for (int i = 1; i < n - 1; i++)
            for (int j = i; j < n - 1; j++)
                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1])
                    return true;
        return false;

    }
};