////bool issort(const string& str1, const string& str2)//字符是否按字典序排序
////{
////    int n1 = str1.size(), n2 = str2.size(), i = 0;
////    for (i = 0; i < n1 && i < n2; i++)
////    {
////        if (str1[i] > str2[i])
////            return false;
////        else if (str1[i] < str2[i])
////            return true;
////    }
////    if (n2 == i && str1[n2] >= str2[n2])
////        return false;
////    else
////        return true;
////}
//
//int main() {
//    int n;
//    while (cin >> n)
//    {
//        vector<string> v(n);//输入数据
//        for (auto& str : v)
//        {
//            cin >> str;
//        }
//
//        //排序方式
//        bool symbolLex = true, symbolLen = true;//两者排序方式变量
//        for (int i = 1; i < v.size(); ++i)
//        {
//            if (v[i - 1].size() >= v[i].size())
//                symbolLen = false;
//            if (v[i - 1] >= v[i])
//                symbolLex = false;
//        }
//
//        //对应方式
//        if (symbolLex && symbolLen)
//            cout << "both" << endl;
//        else if (!symbolLex && symbolLen)
//            cout << "lengths" << endl;
//        else if (symbolLex && !symbolLen)
//            cout << "lexicographically" << endl;
//        else
//            cout << "none" << endl;
//
//        return 0;
//    }
//}


//int main() {
//    int A = 0, B = 0;
//    while (cin >> A >> B)
//    {
//        int ans = 2;
//        while (1)
//        {
//            if ((ans % A == 0) && (ans % B == 0))
//                break;
//            ++ans;
//        }
//        cout << ans << endl;
//    }
//    return 0;
//}


//int gcd(int x, int y)
//{
//    int c;
//    while (c = x % y)
//    {
//        x = y;
//        y = c;
//    }
//    return y;
//}
//
//int main() {
//    int A = 0, B = 0;
//    cin >> A >> B;
//    cout << A * B / gcd(A, B) << endl;
//}


#include <iostream>
using namespace std;

//class A
//{
//public:
//	static const int b = 4;
//	const static int c = 1;//const修饰的静态变量可以在声明时初始化
//	static int a;
//};
//int A::a = 2;
//int main()
//{
//	A A1;
//	cout << A1.a << endl;
//	cout << A1.b << endl;
//	cout << A::a << endl;
//	return 0;
//}


//class UnusualAdd {
//public:
//    int addAB(int x, int y) {
//        while (y)
//        {
//            int z = (x & y) << 1;//两数相加进位数
//            x = x ^ y;//无进制相加
//            y = z;
//        }
//        return x;
//    }
//};

//int addAB(int A, int B) {
//    if ((A & B) == 0)
//        return A ^ B;
//    else
//    {
//        int a = A ^ B;
//        int b = (A & B) << 1;
//        return addAB(a, b);
//    }
//}
//
//int main()
//{
//    int x, y;
//    while(cin>>x>>y)
//        cout<<addAB(x, y)<<endl;
//}
//class UnusualAdd {
//public:
//    int addAB(int A, int B) {
//        if (A & B)
//            return addAB(A ^ B, (A & B) << 1);
//        else
//            return A ^ B;
//    }
//};


//int pathNum(int x, int y)
//{
//    if (x > 1 && y > 1)
//        return pathNum(x - 1, y) + pathNum(x, y - 1);
//    else if (((x >= 1) && (y == 1)) || ((x == 1) && (y >= 1)))
//        return x + y;
//    else
//        return 0;
//}
//int main() {
//    int n, m;
//    while (cin >> n >> m)
//    {
//        cout << pathNum(n, m) << endl;
//    }
//}


int main()
{
	const int a = 10;
	int* p = (int*)&a;
	*p = 20;
	cout << "a=" << a << " " << "*p = " << *p << endl;
	return 0;
}