//class Solution {
//public:
//    int findNumberOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> len(n, 1), count(n, 1);
//        int maxlen = 1, maxcount = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (nums[i] > nums[j])
//                {
//                    if (len[j] + 1 == len[i])
//                        count[i] += count[j];
//                    else if (len[j] + 1 > len[i])
//                        len[i] = len[j] + 1, count[i] = count[j];
//                }
//            }
//            if (len[i] == maxlen)
//                maxcount += count[i];
//            else if (len[i] > maxlen)
//                maxlen = len[i], maxcount = count[i];
//        }
//        return maxcount;
//    }
//};




//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size(), ret = 1;
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (pairs[i][0] > pairs[j][1])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};




class Solution {
public:
    int longestSubsequence(vector<int>& arr, int difference) {
        int n = arr.size(), ret = 1;
        vector<int> dp(n, 1);
        unordered_map<int, int> hash; hash[arr[0]]++;
        for (int i = 1; i < n; i++)
        {
            hash[arr[i]] = hash[arr[i] - difference] + 1;
            ret = max(ret, hash[arr[i]]);
        }
        return ret;
    }
};