#pragma once

#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

namespace initateString
{	
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		const char* c_str()
		{
			return _str;
		}

		//new char[1]而不使用new char,是因为前者是分配一个字符数组，可以通过下标快速访问和修改数据；而后者则是一个字符空间
		/*string()
			:_str(new char[1]{'\0'})
			, _size(0)
			, _capacity(0)
		{
			_str[0] = '\0';
		}*/

		//权限放大，直接new
		/*string(const char* str)
			:_str(str)
			,_size(strlen(str))
			,_capacity(_size+1)
		{}*/
		string(const char* str="")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new  char[_size + 1];
			strcpy(_str, str);
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_capacity = _size = 0;
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}
		
		size_t capacity() const
		{
			return _capacity;
		}

		size_t size() const
		{
			return _size;
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n+1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}

		void push_bach(char ch)
		{
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			strcpy(_str + _size, str);
			_size += len;
		}

		string& operator+=(char ch)
		{
			push_bach(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}

			size_t end = _size+1;//包括'\0'
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			_str[pos] = ch;
			_size++;
		}

		void insert(size_t pos, const char* str)
		{

		}

		void erase(size_t pos, size_t len = npos)
		{

		}

		bool operator<(const string& s)
		{
			return strcmp(_str, s._str) < 0;
		}

		bool operator==(const string& s)
		{
			return strcmp(_str, s._str) == 0;
		}

		bool operator<=(const string& s)
		{
			return *this < s || *this == s;
		}

		bool operator>(const string& s)
		{
			return !(*this <= s);
		}

		bool operator>=(const string& s)
		{
			return !(*this < s);
		}

		bool operator!=(const string& s)
		{
			return !(*this == s);
		}



	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		//const 静态整型特例
		//const static size_t npos = -1;

		const static size_t npos;
	};
	const size_t string::npos = -1;

	string& operator<<(ostream& out, const string& str)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			out << str[i];
		}
		return out;
	}
	void testString1()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		string s2;
		cout << s2.c_str() << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;

		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void testString2()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		s1.push_bach('@');
		cout << s1.c_str() << endl;

		s1 += '!';
		s1 += "123456789";
		cout << s1.c_str() << endl;
		s1.append("hello Linux");
		cout << s1.c_str() << endl;

	}

	void testString3()
	{
		string s1("hello world");
		string s2("hello world");
		cout << (s1 < s1) << endl;
		cout << (s1 == s1) << endl;

		s1[0] = '0';
		cout << (s1 < s2) << endl;
		cout << (s1 > s2) << endl;
	}
};



