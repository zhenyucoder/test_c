#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <algorithm>      
#include <utility>
using namespace std;


//int main()
//{
//	vector<int> v{ 1,4,2,6,7,29 };
//	set<int> s;
//	s.insert(v.begin(), v.end());//插入迭代区间的值
//	for (auto num : s)//打印s中的所有元素
//	{
//		cout << num << ":";
//	}
//	cout << endl;
//
//	pair<set<int>::iterator, bool> pr = s.insert(2);//插入一个已存在的值
//	cout << *pr.first << "->" << pr.second << endl;//返回值pair的两个参数结果
//
//	pr = s.insert(5);//插入一个不存在的值
//	cout << *pr.first << "->" << pr.second << endl;//返回值pair的两个参数结果
//	return 0;
//}

//int main()
//{
//	vector<int> v{ 1,4,2,6,7,29 };
//	set<int> s;
//	s.insert(v.begin(), v.end());//插入迭代区间的值
//
//	cout << "删除前数据:";
//	for (auto num : s)
//	{
//		cout << num << "->";
//	}
//	cout << endl;
//
//	s.erase(++s.begin());//删除迭代器对应的值, 即第二个值
//	cout << "迭代器删除第二个值数据:";
//	for (auto num : s)
//	{
//		cout << num << "->";
//	}
//	cout << endl;
//
//	s.erase(29);
//	cout << "删除29:";
//	for (auto num : s)
//	{
//		cout << num << "->";
//	}
//	cout << endl;
//
//	s.erase(++s.begin(), --s.end());
//	cout << "删除除首位外的其他元素";
//	for (auto num : s)
//	{
//		cout << num << "->";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	vector<int> v{ 1,4,2,6,7,29 };
//	set<int> s;
//	s.insert(v.begin(), v.end());
//
//	set<int>::iterator it = s.find(4);
//	if (it != s.end())
//		cout << "数据存在" << endl;
//	else
//		cout << "数据不存在" << endl;
//
//	it = s.find(10);
//	if (it != s.end())
//		cout << "数据存在" << endl;
//	else
//		cout << "数据不存在" << endl;
//	return 0;
//}



//class Solution {//暴力解法，超时
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int ret = INT_MAX, n = nums.size();
//        for (int start = 0; start < n; start++)
//        {//记录一段连续区间的开始
//            int sum = 0;
//            for (int end = start; end < n; end++)
//            {
//                sum += nums[end];
//                if (sum >= target)
//                {
//                    ret = min(ret, end - start + 1);
//                    break;
//                }
//            }
//        }
//        return ret == INT_MAX ? 0 : ret;
//    }
//};

//class Solution {//滑动窗口
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size(), len = INT_MAX, sum = 0;
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            sum += nums[right];//进窗口
//            while (sum >= target)//判断
//            {
//                //更新结果
//                len = min(len, right - left + 1);
//                sum -= nums[left++];//出窗口
//            }
//        }
//        return len == INT_MAX ? 0 : len;
//    }
//};

//法3：利用前缀和+二分查找
//class Solution {
//public:
//    int findTarget(vector<int>& v, int target, int left, int right)//二分查找目标值
//    {
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (v[mid] < target)
//            {
//                left = mid + 1;
//            }
//            else
//            {
//                right = mid;
//            }
//        }
//        return left;
//    }
//
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size(), len = INT_MAX;
//        vector<int> dp(n + 1);//前缀和
//        for (int i = 1; i <= n; i++)
//            dp[i] = dp[i - 1] + nums[i - 1];
//        if (dp[n] < target)//所有数据和都小于目标值，不存在结果
//            return 0;
//        //存在结果，查找
//        for (int i = 1; i <= n; i++)
//        {
//            int _target = target + dp[i - 1];
//            if (dp[n] < _target)
//                continue;
//            int rightbase = findTarget(dp, _target, i, n);
//            len = min(len, rightbase - i + 1);
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int len = 0, n = s.size();
//        char hash[256] = { 0 };
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            hash[s[right]]++;//进窗口
//            while (left <= right && hash[s[right]] == 2)//判断
//            {
//                //出窗口
//                hash[s[left++]]--;
//            }
//            len = max(len, right - left + 1);
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int findMaxConsecutiveOnes(vector<int>& nums) {
//        int Maxcount = 0, count = 0;
//        for (auto num : nums)
//        {
//            if (num == 1)
//                count++;
//            else
//            {
//                Maxcount = max(Maxcount, count);
//                count = 0;
//            }
//        }
//        return max(count, Maxcount);
//    }
//};

// class Solution {
// public:
//     int findMaxConsecutiveOnes(vector<int>& nums) {
//         int ret =0, n=nums.size();
//         for(int start=0, end=0; end<n; end++)
//         {
//           if(nums[end] == 1)
//           {
//             start=end;
//             while(end<n && nums[end]==1)
//             end++;
//             //更新结果
//             ret = max(ret, end-start);
//           }
//         }
//         return ret;
//     }
// };


//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int Maxcount = 0, count = 0, n = nums.size();
//        for (int left = 0, right = 0, zero = 0; right < n; right++)
//        {
//            if (nums[right] == 0)
//                zero++;//进窗口
//            while (zero > k)//判断
//            {
//                if (nums[left++] == 0)
//                    zero--;//出窗口
//            }
//            //更新结果
//            count = max(count, right - left + 1);
//        }
//        return count;
//    }
//};
//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int count = 0, n = nums.size();
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            if (nums[right] == 0)
//                --k;
//            while (k < 0)
//            {
//                if (nums[left++] == 0)
//                    k++;
//            }
//            count = max(count, right - left + 1);
//        }
//        return count;
//    }
//};


      

//int main() {
//	int myints[] = { 10,20,30,30,20,10,10,20 };
//	std::vector<int> v(myints, myints + 8);           // 10 20 30 30 20 10 10 20
//
//	std::sort(v.begin(), v.end());                // 10 10 10 20 20 20 30 30
//
//	std::vector<int>::iterator low, up;
//	low = std::lower_bound(v.begin(), v.end(), 15); 
//	cout << *low << endl;
//	low = std::lower_bound(v.begin(), v.end(), 20);
//	cout << *low << endl;
//	low = std::lower_bound(v.begin(), v.end(), 25); // >=val
//	cout << *low << endl;
//	//up = std::upper_bound(v.begin(), v.end(), 20); //                   ^
//	//std::cout << "lower_bound at position " << (low - v.begin()) << '\n';
//	//std::cout << "upper_bound at position " << (up - v.begin()) << '\n';
//
//	return 0;
//}


//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        for (int i = 1; i <= n; i++)//前缀和求[0, i-1]中0的个数
//        {
//            dp[i] = dp[i - 1] + (1 - nums[i - 1]);
//        }
//
//        int ans = 0;
//        for (int right = 0; right < n; right++)
//        {
//            int left = lower_bound(dp.begin(), dp.end(), dp[right + 1] - k) - dp.begin();
//            ans = max(ans, right - left + 1);
//        }
//        return ans;
//    }
//};

//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        long long sum = 0, n = nums.size(), len = 0;
//        for (auto num : nums)
//            sum += num;
//        long long target = sum - x;//将原问题转化为求连续区间和为target的最大区间
//        if (target < 0)
//            return -1;
//        else if (target == 0)
//            return n;
//        sum = 0;
//        for (long long left = 0, right = 0; right < n; right++)
//        {
//            sum += nums[right];//进窗口
//            while (left <= right && sum >= target)//判断
//            {
//                if (sum == target)//更新结果
//                    len = max(len, right - left + 1);
//                sum -= nums[left++];//出窗口
//            }
//        }
//        return len == 0 ? -1 : n - len;
//    }
//};
//int main()
//{
//    vector<int> v{ 8828,9581,49,9818,9974,9869,9991,10000,10000,10000,9999,9993,9904,8819,1231,6309 };
//    minOperations(v, 134365);
//    return 0;
//}


//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        int hash[100001] = { 0 };
//        int n = fruits.size(), kind = 0, ret = 0;
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            if (hash[fruits[right]] == 0)
//                kind++;//维护水果种类
//            hash[fruits[right]]++;//进窗口
//            while (kind > 2)//判断
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)
//                    kind--;//出窗口
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool check(int hash1[], int hash2[])
//    {
//        for (int i = 0; i < 26; i++)
//            if (hash1[i] != hash2[i])
//                return false;
//        return true;
//    }
//    vector<int> findAnagrams(string s, string p) {
//        vector<int> ret;
//        if (s.size() < p.size())
//            return ret;
//        int hash2[26] = { 0 }, hash1[26] = { 0 };
//        for (auto ch : p)
//        {
//            hash2[ch - 'a']++;
//        }
//
//        int n1 = s.size(), n2 = p.size();
//        for (int left = 0, right = 0; right < n1; right++)
//        {
//            hash1[s[right] - 'a']++;
//            if (right - left + 1 > n2)
//                hash1[s[left++] - 'a']--;
//            if (right - left + 1 == n2)
//                if (check(hash1, hash2))
//                    ret.push_back(left);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        vector<int> ret;
//        if (s.size() < p.size())
//            return ret;
//        int hash1[26] = { 0 }, hash2[26] = { 0 };
//        for (auto ch : p)
//        {
//            hash2[ch - 'a']++;
//        }
//
//        int n1 = s.size(), n2 = p.size();
//        for (int left = 0, right = 0, count = 0; right < n1; right++)
//        {
//            if (++hash1[s[right] - 'a'] <= hash2[s[right] - 'a'])
//                count++;//统计有效字符个数
//            if (right - left + 1 > n2)//出窗口
//            {
//                char out = s[left++];
//                if (hash1[out - 'a']-- <= hash2[out - 'a'])
//                {
//                    count--;
//                }
//
//            }
//            if (count == n2)
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};


