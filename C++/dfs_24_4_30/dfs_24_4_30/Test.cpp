//class Solution {
//public:
//    vector<string> ret;
//    string path;
//    vector<string> letterCasePermutation(string s) {
//        dfs(s, 0);
//        return ret;
//    }
//
//    void dfs(string& s, int pos)
//    {
//        if (s.size() == pos)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//
//        path.push_back(s[pos]);
//        dfs(s, pos + 1);
//        path.pop_back();//恢复现场
//
//        if (s[pos] >= 'a' && s[pos] <= 'z')
//        {
//            path.push_back(toupper(s[pos]));
//            dfs(s, pos + 1);
//            path.pop_back();//恢复现场
//        }
//
//        if (s[pos] >= 'A' && s[pos] <= 'Z')
//        {
//            path.push_back(tolower(s[pos]));
//            dfs(s, pos + 1);
//            path.pop_back();//恢复现场
//        }
//    }
//};



//class Solution {
//public:
//    int ret = 0, end = 0;
//    bool chech[17] = { 0 };
//    int countArrangement(int n) {
//        end = n;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int pos)
//    {
//        if (pos > end)
//        {
//            ret++;
//            return;
//        }
//
//        for (int i = 1; i <= end; i++)
//        {
//            if (chech[i] == false && (i % pos == 0 || pos % i == 0))
//            {
//                chech[i] = true;
//                dfs(pos + 1);
//                //恢复现场
//                chech[i] = false;
//            }
//        }
//    }
//};





#include <iostream>
#include <string>
#include <vector>
using namespace std;


vector<vector<string>> ret;
vector<string> path;

bool isExist(int x, int y, int n)
{
    //[x][y]所在列是否已经存在皇后
    for (int i = x - 1; i >= 0; i--)
        if (path[i][y] == 'Q')
            return true;
    //[x][y]所在主对角线是否已经存在皇后
    for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--)
        if (path[i][j] == 'Q')
            return true;
    //[x][y]所在副对角线是否已经存在皇后
    for (int i = x - 1, j = y + 1; i >= 0 && j < n; i--, j++)
        if (path[i][j] == 'Q')
            return true;
    //[x][y]位置可以存在放后
    return false;
}
void dfs(int pos, int n)
{
    if (pos == n)
    {
        ret.push_back(path);
        return;
    }

    string s(n, '.');
    for (int i = 0; i < n; i++)
    {
        if (isExist(pos, i, n) == false)
        {
            s[i] = 'Q'; path.push_back(s);
            dfs(pos + 1, n);
            //恢复现场
            s[i] = '.'; path.pop_back();
        }
    }
}



vector<vector<string>> solveNQueens(int n) {
    dfs(0, n);
    return ret;
}



int main()
{
    int n;
    cin >> n;
    solveNQueens(n);
    return 0;
}