#include <iostream>
#include <string>
using namespace std;
//
//int main()
//{
//	//string str1;
//	//getline(cin, str1, 'c');//只有string中有
//	//cout << str1 << endl;
//	printf("%3.5s\n", "dwjdewfefijef");
//	return 0;
//}


//class Solution {
//public:
//    bool isUnique(string astr) {
//        if (astr.size() > 26)
//            return false;
//        char hash[26];//哈希表，存储小写字母
//        for (auto ch : astr)
//        {
//            hash[ch - 'a']++;
//        }
//        for (int i = 0; i < 26; i++)
//        {
//            if (hash[i] > 1)//字母出现次数大于1
//                return false;
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    bool isUnique(string astr) {
//        if (astr.size() > 26)
//            return 0;
//        //位图
//        int bitMap = 0;
//        for (auto ch : astr)
//        {
//            //字符对应比特位
//            int n = ch - 'a';
//            //判断是否字母以及出现过
//            if ((bitMap >> n) & 1)
//                return false;
//            //将比特位改为1
//            bitMap ^= (1 << n);
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //数学，求和差值
//        int n = nums.size();
//        int sum = (n + 1) * n / 2;
//        int total = 0;
//        for (auto e : nums)
//        {
//            total += e;
//        }
//        return sum - total;
//    }
//}; 
//
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //排序
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < nums.size(); i++)
//        {
//            //查找缺少值
//            if (i != nums[i])
//                return i;
//        }
//        return nums.size();
//    }
//};
#include <vector>

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        //排序
//        int n = nums.size();
//        int hash[10000] = { 0 };//哈希表//初始化,必须是确定参数
//        for (int i = 0; i < n; i++)
//        {
//            hash[nums[i]]++;
//        }
//        //查找确实数字
//        for (int i = 0; i < n + 1; i++)
//        {
//            if (hash[i] == 0)
//                return i;
//        }
//        return -1;
//    }
//};
//int main()
//{
//    vector<int> v = { 3,0,1 };
//    missingNumber(v);
//    return 0;
//}

//按位异或
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0, n = nums.size();
//        for (auto e : nums)
//        {
//            ret ^= e;
//        }
//        for (int i = 0; i <= n; i++)
//        {
//            ret ^= i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int getSum(int a, int b) {
//        while (b != 0)
//        {
//            int c = a ^ b;
//            b = (a & b) << 1;
//            a = c;
//        }
//        return a;
//    }
//};

//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            int sum = 0;
//            for (auto num : nums)
//            {
//                if ((num >> i) & 1 == 1)
//                    sum++;
//            }
//            if (sum % 3 == 1)
//                ret |= 1 << i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int sum = 0;
//        for (auto num : nums)
//        {
//            sum ^= num;
//        }
//        //找到第一位非0二进制位
//        int i = 0;
//        while (1)//查找第一个非0比特位下标
//        {
//            if ((sum >> i) & 1)
//                break;
//            else
//                i++;
//        }
//
//        //依第i个比特位1/0，将所有数字分为两部分
//        int type1 = 0, type2 = 0;
//        for (auto num : nums)
//        {
//            if ((num >> i) & 1 == 1)
//                type1 ^= num;
//            else
//                type2 ^= num;
//        }
//        return { type1, type2 };
//    }
//};



//class Solution {
//public:
//    vector<int> missingTwo(vector<int>& nums) {
//        int n = nums.size(), ret = 0;
//        for (auto num : nums)
//        {
//            ret ^= num;
//        }
//        for (int i = 1; i <= n + 2; i++)
//        {
//            ret ^= i;
//        }
//
//        //查找两个数中比特位不同的那位
//        int diff = 0;
//        while (1)
//        {
//            if ((ret >> diff) & 1 == 1)
//                break;
//            else
//                diff++;
//        }
//
//        //根据diff不同，将数分为两部分异或
//        int a = 0, b = 0;
//        for (auto num : nums)
//        {
//            if ((num >> diff) & 1 == 1)
//                a ^= num;
//            else
//                b ^= num;
//        }
//
//        for (int i = 1; i <= n + 2; i++)
//        {
//            if ((i >> diff) & 1 == 1)
//                a ^= i;
//            else
//                b ^= i;
//
//        }
//        return { a, b };
//    }
//};


