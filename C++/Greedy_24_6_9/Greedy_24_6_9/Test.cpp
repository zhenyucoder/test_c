//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0)  return 0;
//        vector<int> ret;
//        ret.push_back(nums[0]);
//
//        for (int i = 1; i < nums.size(); i++)
//        {
//            if (nums[i] > ret.back())
//            {
//                ret.push_back(nums[i]);
//            }
//            else
//            {
//                //二分查找待插入位置
//                int left = 0, right = ret.size();
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2;
//                    if (ret[mid] < nums[i])
//                        left = mid + 1;
//                    else
//                        right = mid;
//                }
//                ret[right] = nums[i];
//            }
//        }
//        return ret.size();
//    }
//}



//class Solution {
//public:
//    bool increasingTriplet(vector<int>& nums) {
//        if (nums.size() < 3) return false;
//        vector<int> ret;
//        ret.push_back(nums[0]);
//
//        int n = nums.size();
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] > ret.back())
//            {
//                ret.push_back(nums[i]);
//            }
//            else
//            {
//                int left = 0, right = ret.size() - 1;
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2;
//                    if (nums[i] > ret[mid])
//                        left = mid + 1;
//                    else
//                        right = mid;
//                }
//                ret[left] = nums[i];
//            }
//            if (ret.size() == 3)
//                return true;
//        }
//        return false;
//    }
//};





//class Solution {
//public:
//    bool increasingTriplet(vector<int>& nums) {
//        if (nums.size() < 3) return false;
//        int a = INT_MAX, b = INT_MAX;
//
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            int x = nums[i], j = 0;
//            if (x > b)
//                return true;
//            else if (x > a)
//                b = x;
//            else
//                a = x;
//        }
//        return false;
//    }
//};




//class Solution {
//public:
//    int findLengthOfLCIS(vector<int>& nums) {
//        int len = 0, n = nums.size();
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            if (right + 1 < n && nums[right + 1] > nums[right])
//                continue;
//            else
//            {
//                len = max(len, right - left + 1);
//                left = right + 1;
//            }
//        }
//        return len;
//    }
//};




//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int prev = INT_MAX, ret = 0;
//        for (auto price : prices)
//        {
//            if (price > prev)
//                ret = max(ret, price - prev);
//            prev = min(prev, price);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int ret = 0;
//        for (int left = 0, right = 0, n = prices.size(); right < n; right++)
//        {
//            while (right + 1 < n && prices[right + 1] >= prices[right])
//                right++;
//            ret += prices[right] - prices[left];
//            left = right + 1;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        int  size = 0, prev = INT_MAX;
//        for (auto x : nums)//统计负数个数
//            if (x < 0) size++, prev = min(prev, -x);
//            else prev = min(prev, x);
//
//        sort(nums.begin(), nums.end());
//        int n = min(k, size);
//        for (int i = 0; i < n; i++)
//            nums[i] = -nums[i];
//
//        int ret = 0;
//        for (auto x : nums)
//            ret += x;
//        if (size < k && (k - size) % 2)
//            ret -= 2 * prev;
//        return ret;
//    }
//};



class Solution {
public:
    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
        int n = names.size();
        vector<int> index(n);
        for (int i = 0; i < n; i++)
            index[i] = i;

        //对下标进行排序
        sort(index.begin(), index.end(), [&](int i, int j)
            {
                return heights[i] > heights[j];
            });

        vector<string> ret(n);
        for (int i = 0; i < n; i++)
            ret[i] = names[index[i]];
        return ret;
    }
};