//class Solution {
//public:
//    string tree2str(TreeNode* root) {
//        string ret;
//        if (root == nullptr) return ret;
//        ret += to_string(root->val);
//        if (root->left != nullptr)
//        {
//            ret += '(';
//            ret += tree2str(root->left);
//            ret += ')';
//        }
//        if (root->right != nullptr)
//        {
//            if (root->left == nullptr) ret += "()";
//            ret += '(';
//            ret += tree2str(root->right);
//            ret += ')';
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    bool check[16];
//    int ret;
//    int countArrangement(int n) {
//        dfs(n, 1);
//        return ret;
//    }
//
//    void dfs(int n, int pos)
//    {
//        if (pos > n)
//        {
//            ret++;
//            return;
//        }
//
//        for (int i = 1; i <= n; i++)
//        {
//            if (check[i] == false && (i % pos == 0 || pos % i == 0))
//            {
//                check[i] = true;
//                dfs(n, pos + 1);
//                check[i] = false;
//            }
//        }
//    }
//};


class Solution {
public:
    bool row[9][10];
    bool col[9][10];
    bool grid[3][3][10];
    void solveSudoku(vector<vector<char>>& board) {
        //保存初始数独信息
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (board[i][j] != '.')
                {
                    int num = board[i][j] - '0';
                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
                }
        dfs(board);
    }

    bool dfs(vector<vector<char>>& board)
    {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] == '.')
                {
                    for (int k = 1; k <= 9; k++)
                    {
                        if (!row[i][k] && !col[j][k] && !grid[i / 3][j / 3][k])
                        {
                            row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = true;
                            board[i][j] = '0' + k;
                            if (dfs(board) == false)
                            {
                                row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = false;
                                board[i][j] = '.';
                                continue;
                            }
                            break;
                        }

                    }
                    if (board[i][j] == '.')
                        return false;
                }
            }
        }
        return true;
    }
};