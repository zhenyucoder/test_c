//class Solution {
//public:
//    bool isUnivalTree(TreeNode* root) {
//        if (root == nullptr)
//            return true;
//        if (root->left && root->val != root->left->val)
//            return false;
//        if (root->right && root->val != root->right->val)
//            return false;
//        return isUnivalTree(root->left) && isUnivalTree(root->right);
//    }
//};



//class Solution {
//public:
//    bool isSameTree(TreeNode* p, TreeNode* q) {
//        if (p == nullptr && q == nullptr) return true;
//        if (p == nullptr || q == nullptr) return false;
//        if (p->val != q->val) return false;
//
//        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//    }
//};



//class Solution {
//public:
//    bool _isSymmetric(TreeNode* Leftroot, TreeNode* Rightroot)
//    {
//        if (Leftroot == nullptr && Rightroot == nullptr) return true;
//        if (Leftroot == nullptr || Rightroot == nullptr) return false;
//        if (Leftroot->val != Rightroot->val) return false;
//        return _isSymmetric(Leftroot->left, Rightroot->right)
//            && _isSymmetric(Leftroot->right, Rightroot->left);
//    }
//
//    bool isSymmetric(TreeNode* root) {
//        if (root == nullptr) return true;
//        return _isSymmetric(root->left, root->right);
//    }
//};




//class Solution {
//public:
//    vector<int> ret;
//    vector<int> preorderTraversal(TreeNode* root) {
//        dfs(root);
//        return ret;
//    }
//
//    void dfs(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        ret.push_back(root->val);
//        dfs(root->left);
//        dfs(root->right);
//    }
//};



//class Solution {
//public:
//    bool isSame(TreeNode* root, TreeNode* targetRoot)
//    {
//        if (root == nullptr && targetRoot == nullptr) return true;
//        if (root == nullptr || targetRoot == nullptr) return false;
//        if (root->val != targetRoot->val) return false;
//        return isSame(root->left, targetRoot->left)
//            && isSame(root->right, targetRoot->right);
//    }
//
//    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
//        if (root == nullptr) return false;
//        if (root->val == subRoot->val && isSame(root, subRoot))
//            return true;
//        if (isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot))
//            return true;
//        return false;
//    }
//};


struct TreeNode {
    char val;
    TreeNode* left;
    TreeNode* right;

    TreeNode(char v) :val(v), left(nullptr), right(nullptr) { }
    ~TreeNode() {
        delete left;
        delete right;
    }
    void InorderTraversal() {
        if (left)
            left->InorderTraversal();
        printf("%c ", val);
        if (right)
            right->InorderTraversal();
    }
};

TreeNode* CreateTreeFromString(const string& str) {
    if (str[0] == '#')
        return nullptr;
    int len = str.size();
    stack<TreeNode*> st;
    TreeNode* root = new TreeNode(str[0]);
    st.push(root);
    int i = 1;
    while (i < len) {
        while (i < len && str[i] != '#') {
            TreeNode* node = new TreeNode(str[i]);
            TreeNode* parent = st.top();
            parent->left = node;
            st.push(node);
            ++i;
        }
        ++i;
        while (i < len && str[i] == '#') {
            st.pop();
            ++i;
        }
        if (i < len) {
            TreeNode* node = new TreeNode(str[i]);
            TreeNode* parent = st.top();
            parent->right = node;
            st.pop();
            st.push(node);
            ++i;
        }
    }
    return root;
}

int main() {
    string preorder_str;
    while (cin >> preorder_str) {
        TreeNode* root = CreateTreeFromString(preorder_str);
        if (root != nullptr)
            root->InorderTraversal();
        printf("\n");
    }

    return 0;
}