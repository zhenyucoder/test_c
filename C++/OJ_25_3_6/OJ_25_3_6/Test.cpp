//struct TreeNode* invertTree(struct TreeNode* root) {
//    if (root == NULL)
//        return NULL;
//    struct TreeNode* left = invertTree(root->left);
//    struct TreeNode* right = invertTree(root->right);
//    root->left = right;
//    root->right = left;
//    return root;
//}

class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> ret;
        ret.push_back({ 1 });
        if (numRows == 1) return ret;
        ret.push_back({ 1, 1 });
        if (numRows == 2) return ret;

        for (int num = 2; num < numRows; num++)
        {
            vector<int> ans(num + 1);
            ans[0] = ans[num] = 1;
            for (int i = 1; i < num; i++)
                ans[i] = ret[num - 1][i - 1] + ret[num - 1][i];
            ret.push_back(ans);
        }
        return ret;
    }
};