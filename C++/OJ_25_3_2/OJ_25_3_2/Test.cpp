//class RecentCounter 
//{
//    queue<int> q;
//public:
//    RecentCounter() 
//    {}
//
//    int ping(int t)
//    {
//        q.push(t);
//        while (q.front() < t - 3000) 
//        {
//            q.pop();
//        }
//        return q.size();
//    }
//};
//



class Solution {
public:
    bool vis[310][310] = { 0 };
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    int m, n;
    int numIslands(vector<vector<char>>& grid) {
        m = grid.size(), n = grid[0].size();
        int ret = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j] == '1' && !vis[i][j])
                {
                    ret++;
                    vis[i][j] = true;
                    dfs(grid, i, j);
                }
            }
        }
        return ret;
    }

    void dfs(vector<vector<char>>& grid, int i, int j)
    {
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !vis[x][y])
            {
                vis[x][y] = true;
                dfs(grid, x, y);
            }
        }
    }
};