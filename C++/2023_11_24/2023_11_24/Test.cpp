//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto e : nums)
//        {
//            ret ^= e;
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> vv;
//        vv.resize(numRows);
//        for (size_t i = 0; i < vv.size(); i++)
//        {
//            vv[i].resize(i + 1, 0);
//            vv[i][0] = vv[i][i] = 1;
//        }
//
//        for (size_t i = 0; i < vv.size(); i++)
//        {
//            for (size_t j = 0; j < vv[i].size(); j++)
//            {
//                if (vv[i][j] == 0)
//                {
//                    vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
//                }
//            }
//        }
//
//        return vv;
//    }
//};


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0)
//            return 0;
//
//        int slow = 0, fast = 1;
//        while (fast < n)
//        {
//            if (nums[slow] != nums[fast])
//                nums[++slow] = nums[fast];
//
//            fast++;
//        }
//        return slow + 1;
//    }
//};


//class Solution {
//    const char* numsStrArr[10] = { "", "","abc", "def", "ghi", "jkl", "mno", "pqrs",
//                                "tuv", "wxyz" };
//public:
//    void Combinations(string digits, int i, string CombineStr, vector<string>& v)
//    {
//        if (i == digits.size())
//        {
//            v.push_back(CombineStr);
//            return;
//        }
//
//        int num = digits[i] - '0';
//        string str = numsStrArr[num];
//        for (auto ch : str)
//        {
//            Combinations(digits, i + 1, CombineStr + ch, v);
//        }
//    }
//    vector<string> letterCombinations(string digits) {
//        vector<string> v;
//        if (digits.size() == 0)
//            return v;
//        string str;
//        Combinations(digits, 0, str, v);
//        return v;
//    }
//};


class Solution {

public:
    int singleNumber(vector<int>& nums) {
        int ans = 0;
        for (int i = 0; i < 32; i++)
        {
            int total = 0;
            for (auto num : nums)
            {
                total += (num >> i) & 1;
            }

            if (total % 3)
                ans |= (1 << i);
        }
        return ans;
    }
};