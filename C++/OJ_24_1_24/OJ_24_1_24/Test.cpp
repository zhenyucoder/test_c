//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        if (nums.size() == 1) return (nums[0] - 1) * (nums[0] - 1);
//        int first = INT_MIN, sec = INT_MIN;
//        for (auto x : nums)
//        {
//            if (x > first) sec = first, first = x;
//            else if (x > sec) sec = x;
//        }
//        return (first - 1) * (sec - 1);
//    }
//};



//class Solution {
//public:
//    int arr[26] = { 0 };
//    int characterReplacement(string s, int k) {
//        int maxcount = 0, ret = 0;
//        for (int left = 0, right = 0; right < s.size(); right++)
//        {
//            maxcount = max(maxcount, ++arr[s[right] - 'A']);
//            if (right - left + 1 > maxcount + k)
//            {
//                --arr[s[left++] - 'A'];
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};




