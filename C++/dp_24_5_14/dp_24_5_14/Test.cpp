//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};




//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};




//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1) return nums[0];
//        return max(_rob(nums, 1, n - 1), _rob(nums, 2, n));
//    }
//
//    int _rob(vector<int>& nums, int begin, int end)
//    {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = begin; i <= end; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[end], g[end]);
//    }
//};




//class Solution {
//public:
//    int deleteAndEarn(vector<int>& nums) {
//        int arr[20001] = { 0 };
//        for (auto x : nums)
//            arr[x] += x;
//        int f[20001] = { 0 }, g[20001] = { 0 };
//        for (int i = 1; i <= 20000; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[20000], g[20000]);
//    }
//};




//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};




//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(3, -INF));
//        auto g = f;
//        //初始化
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//            for (int j = 0; j <= 2; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        //返回结果
//        int ret = -INF;
//        for (int j = 0; j <= 2; j++)
//            ret = max(ret, g[n - 1][j]);
//        return ret;
//    }
//};




class Solution {
public:
    const int KNF = 0x3f3f3f3f;
    int maxProfit(int k, vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> f(n, vector<int>(k + 1, -KNF));
        auto g = f;
        //初始化
        f[0][0] = -prices[0], g[0][0] = 0;
        for (int i = 1; i < n; i++)
            for (int j = 0; j <= k; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)
                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }

        int ret = -KNF;
        for (int j = 0; j <= k; j++)
            ret = max(ret, g[n - 1][j]);
        return ret;
    }
};