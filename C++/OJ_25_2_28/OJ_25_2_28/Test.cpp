//class Solution {
//public:
//    int countStudents(vector<int>& students, vector<int>& sandwiches) 
//    {
//        int s1 = accumulate(students.begin(), students.end(), 0);
//        int s0 = students.size() - s1;
//        for (int i = 0; i < sandwiches.size(); i++) 
//        {
//            if (sandwiches[i] == 0 && s0 > 0)
//            {
//                s0--;
//            }
//            else if (sandwiches[i] == 1 && s1 > 0)
//            {
//                s1--;
//            }
//            else {
//                break;
//            }
//        }
//        return s0 + s1;
//    }
//};



class LRUCache {
public:
    LRUCache(int capacity) :_capacity(capacity)
    {}

    int get(int key) {
        auto iter = _hash.find(key);
        if (iter == _hash.end()) return -1;
        else
        {
            // 更新使用节点在list中的位置
            _list.splice(_list.begin(), _list, iter->second);
            return iter->second->second;
        }
    }

    void put(int key, int value) {
        auto iter = _hash.find(key);
        if (iter != _hash.end()) //更新数据
        {
            _list.splice(_list.begin(), _list, iter->second);
            iter->second->second = value;
        }
        else
        { // 插入数据
            if (_capacity == _hash.size()) // 达到容量上线，删除最久未使用数据
            {
                _hash.erase(_list.back().first);
                _list.pop_back();
            }
            _list.push_front({ key, value });
            _hash.insert({ key, _list.begin() });
        }
    }
private:
    unordered_map<int, list<pair<int, int>>::iterator> _hash;
    list<pair<int, int>> _list;
    int _capacity;
};

