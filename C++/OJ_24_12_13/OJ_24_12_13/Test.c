//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size(), ret = 0;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                ret = max(ret, dp[i][j]);
//            }
//        }
//        return ret;
//    }
//};


class Solution {
public:
    int divisorSubstrings(int num, int k) {
        long long N = pow(10, k), cur = num, ret = 0;
        while (cur * 10 >= N)
        {
            int tmp = cur % N;
            cur /= 10;
            if (tmp && num % tmp == 0) ret++;
        }
        return ret;
    }
};