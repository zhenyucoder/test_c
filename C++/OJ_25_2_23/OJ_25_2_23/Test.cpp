//class MyCircularQueue {
//public:
//    MyCircularQueue(int k) :_arr(k + 1), _capacity(k + 1)
//    {}
//
//    bool enQueue(int value) {
//        if (isFull()) return false;
//        _arr[_windex] = value;
//        _windex = (_windex + 1) % _capacity;
//        return true;
//    }
//
//    bool deQueue() {
//        if (isEmpty()) return false;
//        _rindex = (_rindex + 1) % _capacity;
//        return true;
//    }
//
//    int Front() {
//        return isEmpty() ? -1 : _arr[_rindex];
//    }
//
//    int Rear() {
//        return isEmpty() ? -1 : _arr[(_windex + _capacity - 1) % _capacity];
//    }
//
//    bool isEmpty() {
//        return _windex == _rindex;
//    }
//
//    bool isFull() {
//        return ((_windex + 1) % _capacity) == _rindex;
//    }
//private:
//    vector<int> _arr;
//    int _capacity;
//    int _windex = 0;
//    int _rindex = 0;
//};




//class Solution {
//public:
//    int firstMissingPositive(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; ++i)
//        {
//            while (nums[i] > 0 && nums[i] <= n && nums[nums[i] - 1] != nums[i])
//                swap(nums[nums[i] - 1], nums[i]);
//        }
//        for (int i = 0; i < n; ++i)
//        {
//            if (nums[i] != i + 1)
//            {
//                return i + 1;
//            }
//        }
//        return n + 1;
//    }
//};





//class CQueue {
//public:
//    CQueue() {
//
//    }
//
//    void appendTail(int value) {
//        pushst.push(value);
//    }
//
//    int deleteHead() {
//        if (popst.empty())
//        {
//            while (!pushst.empty())
//            {
//                popst.push(pushst.top());
//                pushst.pop();
//            }
//        }
//
//        int ret = -1;
//        if (!popst.empty())
//        {
//            ret = popst.top();
//            popst.pop();
//        }
//        return ret;
//    }
//private:
//    stack<int> pushst;
//    stack<int> popst;
//};




class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ret;
        if (root == nullptr) return ret;
        bool flag = true; // �Ƿ���Ҫ��ת
        queue<TreeNode*> q;
        q.push(root);
        while (!q.empty())
        {
            vector<int> cur;
            int levelsize = q.size();
            flag = !flag;
            for (int i = 0; i < levelsize; i++)
            {
                TreeNode* front = q.front();
                cur.push_back(front->val);
                q.pop();
                if (front->left) q.push(front->left);
                if (front->right) q.push(front->right);
            }
            if (flag) reverse(cur.begin(), cur.end());
            ret.push_back(cur);
        }
        return ret;
    }
};
