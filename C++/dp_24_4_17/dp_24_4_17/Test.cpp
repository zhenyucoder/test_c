//#include <iostream>
//#include <vector>
//#include <string.h>
//using namespace std;
//
//const int N = 1010;
//int n, V, v[N], w[N];
//int dp[N][N];
//
//int main()
//{
//    cin >> n >> V;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= V; j++)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (v[i] <= j)
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//        }
//    cout << dp[n][V] << endl;
//
//    memset(dp, 0, sizeof dp);
//    //��ʼ��
//    for (int i = 1; i <= V; i++)
//        dp[0][i] = -1;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= V; j++)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (v[i] <= j && dp[i - 1][j - v[i]] != -1)
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//        }
//    cout << (dp[n][V] == -1 ? 0 : dp[n][V]) << endl;
//    return 0;
//}



//const int N = 1010;
//int n, V, v[N], w[N];
//int dp[N];
//
//int main()
//{
//    cin >> n >> V;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//        for (int j = V; j >= v[i]; j--)
//        {
//            dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    cout << dp[V] << endl;
//
//    memset(dp, 0, sizeof dp);
//    //��ʼ��
//    for (int i = 1; i <= V; i++)
//        dp[i] = -1;
//    for (int i = 1; i <= n; i++)
//        for (int j = N; j >= v[i]; j--)
//        {
//            if (dp[j - v[i]] != -1)
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//    return 0;
//}




//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int sum = 0;
//        for (auto num : nums)
//            sum += num;
//        if (sum % 2) return false;
//        sum /= 2;
//
//        int n = nums.size();
//        vector<vector<bool>> dp(n + 1, vector<bool>(sum + 1));
//        for (int i = 0; i <= n; i++)
//            dp[i][0] = true;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= sum; j++)
//            {
//                dp[i][j] = dp[i - 1][j] || (j >= nums[i - 1] ? dp[i - 1][j - nums[i - 1]] : false);
//            }
//        return dp[n][sum];
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int sum = 0;
//        for (auto num : nums)
//            sum += num;
//        if (sum % 2) return false;
//        sum /= 2;
//
//        int n = nums.size();
//        vector<bool> dp(sum + 1);
//        dp[0] = true;
//        for (int i = 1; i <= n; i++)
//            for (int j = sum; j >= nums[i - 1]; j--)
//            {
//                dp[j] = dp[j] || dp[j - nums[i - 1]];
//            }
//        return dp[sum];
//    }
//};



//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        int aim = (sum + target) / 2, n = nums.size();
//        if (aim < 0 || (sum + target) % 2) return 0;
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; i++)
//            for (int j = 0; j <= aim; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                    dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        return dp[n][aim];
//    }
//};


class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        int sum = 0;
        for (auto x : nums)
            sum += x;
        int aim = (sum + target) / 2, n = nums.size();
        if (aim < 0 || (sum + target) % 2) return 0;
        vector<long long> dp(n + 1);
        dp[0] = 1;
        for (int i = 1; i <= n; i++)
            for (int j = aim; j >= nums[i - 1]; j--)
                dp[j] += dp[j - nums[i - 1]];
        return dp[aim];
    }
};