//class Solution
//{
//public:
//	int maxProfit(vector<int>& prices)
//	{
//		int ret = 0; 
//		for (int i = 0, prevMin = INT_MAX; i < prices.size(); i++)
//		{
//			ret = max(ret, prices[i] - prevMin); 
//			prevMin = min(prevMin, prices[i]); 
//		}
//		return ret;
//	}
//};




//class Solution
//{
//public:
//	int maxProfit(vector<int>& p)
//	{
//		int ret = 0, n = p.size();
//		for (int i = 0; i < n; i++)
//		{
//			int j = i;
//			while (j + 1 < n && p[j + 1] > p[j]) j++; 
//			ret += p[j] - p[i];
//			i = j;
//		}
//		return ret;
//	}
//};
// 
//class Solution
//{
//public:
//	int maxProfit(vector<int>& prices)
//	{
//		int ret = 0;
//		for (int i = 1; i < prices.size(); i++)
//		{
//			if (prices[i] > prices[i - 1])
//				ret += prices[i] - prices[i - 1];
//		}
//		return ret;
//	}
//};





//class Solution
//{
//public:
//	int largestSumAfterKNegations(vector<int>& nums, int k)
//	{
//		int m = 0, minElem = INT_MAX, n = nums.size();
//		for (auto x : nums)
//		{
//			if (x < 0) m++;
//			minElem = min(minElem, abs(x)); 
//		}
//		int ret = 0;
//		if (m > k)
//		{
//			sort(nums.begin(), nums.end());
//			for (int i = 0; i < k; i++) 
//			{
//				ret += -nums[i];
//			}
//			for (int i = k; i < n; i++) 
//			{
//				ret += nums[i];
//			}
//		}
//		else
//		{
//			for (auto x : nums) ret += abs(x);
//			if((k - m) % 2) 
//			{
//				ret -= minElem * 2;
//			}
//		}
//		return ret;
//	}
//};




class Solution
{
public:
	vector<string> sortPeople(vector<string>& names, vector<int>& heights)
	{
		int n = names.size();
		vector<int> index(n);
		for (int i = 0; i < n; i++) index[i] = i;
		sort(index.begin(), index.end(), [&](int i, int j)
			{
				return heights[i] > heights[j];
			});
		vector<string> ret;
		for (int i : index)
		{
			ret.push_back(names[i]);
		}
		return ret;
	}
};