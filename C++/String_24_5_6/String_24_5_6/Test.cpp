//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        string ret = strs[0];
//        for (auto& str : strs)
//            ret = Samestr(ret, str);
//        return ret;
//    }
//    string Samestr(string& str1, string& str2)
//    {
//        if (str1.size() == 0 || str2.size() == 0)
//            return "";
//        string ret;
//        for (int i = 0; i < str1.size() && i < str2.size(); i++)
//        {
//            if (str1[i] == str2[i])
//                ret += str1[i];
//            else
//                break;
//        }
//        return ret;
//    }
//};

//优化1
//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        string ret;
//        for (int i = 0; i < strs[0].size(); i++)
//        {
//            char flag = strs[0][i];
//            for (int j = 0; j < strs.size(); j++)
//            {
//                if (strs[j].size() <= i || strs[j][i] != flag)
//                    return ret;
//            }
//            ret += flag;
//        }
//        return ret;
//    }
//};

//优化2
//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        return merge(strs, 0, strs.size() - 1);
//    }
//
//    string merge(vector<string>& strs, int left, int right)
//    {
//        if (left == right) return strs[left];
//        if (left > right)
//            return "";
//        int mid = left + (right - left) / 2;
//        string str1 = merge(strs, left, mid);
//        string str2 = merge(strs, mid + 1, right);
//        string ret;
//        for (int i = 0; i < str1.size() && str2.size(); i++)
//        {
//            if (str1[i] == str2[i])
//                ret += str1[i];
//            else
//                break;
//        }
//        return ret;
//    }
//};



//class Solution {//解法一:dp
//public:
//    string longestPalindrome(string s) {
//        int begin = 0, len = 0,  n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    dp[i][j] = j - i > 1 ? dp[i + 1][j - 1] : true;
//                }
//                if(dp[i][j] && j - i> len)
//						begin = i, len = j - i + 1;
//            }
//        return s.substr(begin, len);
//    }
//};

//解法二:中心扩展算法
//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int begin = 0, len = 0, n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            //判断是否存在奇数次回文串
//            int left = i, right = i;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                if (right - left + 1 > len)
//                    begin = left, len = right - left + 1;
//                left--, right++;
//            }
//
//            //判断是否存在偶数次回文串
//            left = i, right = i + 1;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                if (right - left + 1 > len)
//                    begin = left, len = right - left + 1;
//                left--, right++;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};





//class Solution {
//public:
//    string addBinary(string a, string b) {
//        reverse(a.begin(), a.end());
//        reverse(b.begin(), b.end());
//        string ret;
//        int tmp = 0, i = 0, n1 = a.size(), n2 = b.size();
//        while (i < n1 || i < n2 || tmp)
//        {
//            if (i < n1) tmp += a[i] - '0';
//            if (i < n2) tmp += b[i] - '0';
//            ret += to_string(tmp % 2);
//            tmp /= 2;
//            i++;
//        }
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        int cur1 = a.size() - 1, cur2 = b.size() - 1, tmp = 0;
//        string ret;
//        while (cur1 >= 0 || cur2 >= 0 || tmp)
//        {
//            if (cur1 >= 0) tmp += a[cur1--] - '0';
//            if (cur2 >= 0) tmp += b[cur2--] - '0';
//            ret += tmp % 2 + '0';
//            tmp /= 2;
//        }
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};



