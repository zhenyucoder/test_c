//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int ret = INT_MAX;
//        for (int left = 0, right = 0, sum = 0; right < nums.size(); right++)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                ret = min(ret, right - left + 1);
//                sum -= nums[left++];
//            }
//        }
//        return (ret == INT_MAX) ? 0 : ret;
//    }
//};




class Solution {
public:
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    bool vis[60][60];
    int m, n, target;
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
        m = image.size(), n = image[0].size(), target = image[sr][sc];
        if (target == color) return image;
        dfs(image, sr, sc, color);
        return image;
    }

    void dfs(vector<vector<int>>& image, int sr, int sc, int color)
    {
        if (vis[sr][sc]) return;
        image[sr][sc] = color, vis[sr][sc] = true;
        for (int i = 0; i < 4; i++)
        {
            int x = sr + dx[i], y = sc + dy[i];
            if (x >= 0 && x < m && y >= 0 && y < n && vis[x][y] == false && image[x][y] == target)
                dfs(image, x, y, color);
        }
    }
};