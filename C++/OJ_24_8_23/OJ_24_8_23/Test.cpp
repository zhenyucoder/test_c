//#include <iostream>
//using namespace std;
//const int N = 1e6 + 10;
//typedef long long LL;
//LL n, m;
//LL row[N], col[N];
//int main()
//{
//	scanf("%ld %ld", &n, &m);
//	LL arr[n][m];
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			scanf("%ld", &arr[i][j]);
//			row[i] += arr[i][j];
//			col[j] += arr[i][j];
//		}
//	}
//
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			printf("%ld ", row[i] + col[j] - arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}




//#include <iostream>
//using namespace std;
//typedef long long LL;
//const int N = 1e5 + 10;
//LL n, k;
//LL h[N], s[N];
//int main()
//{
//	cin >> n >> k;
//	for (int i = 1; i <= n; i++) cin >> h[i];
//	for (int i = 1; i <= n; i++) cin >> s[i];
//
//	LL left = 0, right = 0;
//	LL hSum = 0, sSum = 0, hMax = 0, sMin = 0, begin = 0;
//
//	while (right <= n)
//	{
//		hSum += h[right];
//		sSum += s[right];
//		while (right - left + 1 > k)
//		{
//			hSum -= h[left];
//			sSum -= s[left];
//			left++;
//		}
//		if (right - left + 1 == k)
//		{
//			if (hSum > hMax)
//			{
//				begin = left;
//				hMax = hSum;
//				sMin = sSum;
//			}
//			else if (hSum == hMax && sSum < sMin)
//			{
//				begin = left;
//				hMax = hSum;
//				sMin = sSum;
//			}
//		}
//		right++;
//	}
//
//	cout << begin << endl;
//
//	return 0;
//}




//class Solution
//{
//public:
//	string compressString(string param)
//	{
//		string ret;
//		int left = 0, right = 0, n = param.size();
//		while (left < n)
//		{
//			while (right + 1 < n && param[right] == param[right + 1]) right++;
//			int len = right - left + 1;
//			ret += param[left];
//			if (len > 1)
//			{
//				ret += to_string(len);
//			}
//			left = right + 1;
//			right = left;
//		}
//		return ret;
//	}
//};



//#include <iostream>
//#include <algorithm>
//using namespace std;
//const int N = 2e5 + 10;
//typedef pair<int, int> PII; // <��ȣ����>
//PII arr[N];
//int n, k;
//int main()
//{
//	cin >> n >> k;
//	for (int i = 0; i < n; i++) cin >> arr[i].first;
//	for (int i = 0; i < n; i++) cin >> arr[i].second;
//
//	sort(arr, arr + n, [&](const PII& a, const PII& b)
//		{
//			if (a.second != b.second) return a.second > b.second;
//			else return a.first < b.first;
//		});
//
//	long long s = 0, t = 0;
//	for (int i = 0; i < k; i++)
//	{
//		s += arr[i].first;
//		t += arr[i].second;
//	}
//
//	cout << s << " " << t << endl;
//
//	return 0;
//}


class Solution
{
int dp[1010] = { 0 };
public:
	int knapsack(int V, int n, vector<vector<int> >& vw)
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = V; j >= vw[i][0]; j--)
			{
				dp[j] = max(dp[j], dp[j - vw[i][0]] + vw[i][1]);
			}
		}
		return dp[V];
	}
};