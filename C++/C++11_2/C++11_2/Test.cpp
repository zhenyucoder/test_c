#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

//class Test
//{
//public:
//	Test(string s, int x = 1)
//	{
//
//	}
//private:
//	string s;
//	int x;
//};
//
//
//class Time
//{
//public:
//	//Time(int hour = 1, int minute = 1, int second = 1)
//	//{
//	//	cout << "Time()" << endl;
//	//	_hour = hour;
//	//	_minute = minute;
//	//	_second = second;
//	//}
//
//private:
//	int _hour =10;
//	int _minute = 9;
//	int _second = 8;
//	Test t;
//};
//
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << _month << _day << endl;
//	}
//private:
//	// 基本类型(内置类型)
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//	// 自定义类型
//	Time _t;//调用对应的默认构造函数
//};
//
//int main()
//{
//	Date d;
//	return 0;
//}



//namespace Mystring
//{
//	class string
//	{
//	public:
//		typedef char* iterator;
//		iterator begin()
//		{
//			return _str;
//		}
//
//		iterator end()
//		{
//			return _str + _size;
//		}
//
//		string(const char* str = "")
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			cout << "string(char* str) -- 构造" << endl;
//
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//
//		// s1.swap(s2)
//		void swap(string& s)
//		{
//			::swap(_str, s._str);
//			::swap(_size, s._size);
//			::swap(_capacity, s._capacity);
//		}
//
//		// 拷贝构造
//		string(const string& s)
//		{
//			cout << "string(const string& s) -- 深拷贝" << endl;
//
//			/*string tmp(s._str);
//			swap(tmp);*/
//			_str = new char[s._capacity + 1];
//			strcpy(_str, s._str);
//		}
//
//		// 移动构造
//		string(string&& s)
//		{
//			cout << "string(string&& s) -- 移动拷贝" << endl;
//
//			swap(s);
//		}
//
//		// 赋值重载
//		string& operator=(const string& s)
//		{
//			cout << "string& operator=(const string& s) -- 深拷贝" << endl;
//			/*string tmp(s);
//			swap(tmp);*/
//			if (this != &s)
//			{
//				char* tmp = new char[s._capacity + 1];
//				strcpy(tmp, s._str);
//
//				delete[] _str;
//				_str = tmp;
//				_size = s._size;
//				_capacity = s._capacity;
//			}
//
//			return *this;
//		}
//
//		// 移动赋值
//		string& operator=(string&& s)
//		{
//			cout << "string& operator=(string&& s)-- 移动赋值" << endl;
//
//			swap(s);
//			return *this;
//		}
//
//		~string()
//		{
//			delete[] _str;
//			_str = nullptr;
//		}
//
//		char& operator[](size_t pos)
//		{
//			assert(pos < _size);
//			return _str[pos];
//		}
//
//		void reserve(size_t n)
//		{
//			if (n > _capacity)
//			{
//				char* tmp = new char[n + 1];
//				strcpy(tmp, _str);
//				delete[] _str;
//				_str = tmp;
//
//				_capacity = n;
//			}
//		}
//
//		void push_back(char ch)
//		{
//			if (_size >= _capacity)
//			{
//				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//				reserve(newcapacity);
//			}
//
//			_str[_size] = ch;
//			++_size;
//			_str[_size] = '\0';
//		}
//
//		string& operator+=(char ch)
//		{
//			push_back(ch);
//			return *this;
//		}
//
//		const char* c_str() const
//		{
//			return _str;
//		}
//	private:
//		char* _str = nullptr;
//		size_t _size = 0;
//		size_t _capacity = 0; // 不包含最后做标识的\0
//	};
//};
//
//
//int main()
//{
//	Mystring::string s;
//	s = "11111111";
//	cout << endl;
//	
//	Mystring::string s1;
//	cout << endl;
//
//	s1 = s;
//	cout << endl;
//
//	Mystring::string s2("12343");
//	return 0;
//}


//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//// 函数模板：万能引用
//template<typename T>
//void PerfectForward(T&& t)
//{
//	//Fun(t);
//	// 期望保持实参的属性呢！
//	// 完美转发
//	Fun(forward<T>(t));
//}
//
//
//
//int main()
//{
//	int x = 1;
//	PerfectForward(x);
//	PerfectForward(123);
//	return 0;
//}




