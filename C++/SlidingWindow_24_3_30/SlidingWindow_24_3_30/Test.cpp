//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        int n1 = s.size(), n2 = words.size();
//        unordered_map<string, int> hash2;
//        for (auto& str : words)//统计words中，子字符串出现次数
//            hash2[str]++;
//
//        vector<int> ret;
//        int slen = words[0].size();
//        for (int i = 0; i < slen; i++)//执行slen滑动窗口次数
//        {
//            unordered_map<string, int> hash1;//维护窗口内子字符串的次数
//            //滑动窗口，判断是否存在串联字串
//            for (int left = i, right = i, count = 0; right + slen <= n1; right += slen)
//            {
//                //进窗口
//                string in = s.substr(right, slen);
//                if (hash2.count(in) && ++hash1[in] <= hash2[in])
//                    count++;
//                //出窗口
//                if (right - left + 1 > n2 * slen)
//                {
//                    string out = s.substr(left, slen);
//                    if (hash2.count(out) && hash1[out]-- <= hash2[out])
//                        count--;
//                    left += slen;
//                }
//                //更新结果
//                if (count == n2)
//                    ret.push_back(left);
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//	string minWindow(string s, string t) {
//		string ret;
//		int n1 = s.size(), n2 = t.size();
//		unordered_map<char, int> hash2;//统计t中字符频率
//		for (auto ch : t)
//			hash2[ch]++;
//
//		unordered_map<char, int> hash1;//统计s中有效字符的频率
//		for (int left = 0, right = 0, count = 0; right < n1; right++)
//		{
//			//进窗口 + 维护count
//			char in = s[right];
//			if (hash2.count(in) && ++hash1[in] <= hash2[in])
//				count++;
//			while (count == n2)
//			{
//				//更新结果
//				if (ret.size() == 0 || right - left + 1 < ret.size())
//					ret = s.substr(left, right - left + 1);
//				//出窗口 + 维护count
//				char out = s[left];
//				if (hash2.count(out) && hash1[out]-- <= hash2[out])
//					count--;
//				left++;
//			}
//		}
//		return ret;
//	}
//};