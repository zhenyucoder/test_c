//int q, a, b, c;
//int main() {
//    cin >> q;
//    while (q--) {
//        int ret = 0;
//        cin >> a >> b >> c;
//        int count = min(a, min(b, c));
//        ret += 2 * count;
//        b -= count;
//        if (b > 1)
//        {
//            ret += b - 1;
//        }
//
//        cout << ret << endl;
//    }
//
//    return 0;
//}


//class Solution {
//public:
//    int m, n;
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {
//        m = mat.size(), n = mat[0].size();
//        vector<vector<int>> dist(m, vector<int>(n, -1));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (mat[i][j] == 0)
//                    dist[i][j] = 0, q.push({ i, j });
//            }
//        }
//
//        while (!q.empty())
//        {
//            auto [a, b] = q.front(); q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = dx[k] + a, y = dy[k] + b;
//                if (x >= 0 && x < m && y >= 0 && y < n && dist[x][y] == -1)
//                    dist[x][y] = dist[a][b] + 1, q.push({ x, y });
//            }
//        }
//        return dist;
//    }
//};




//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int m, n;
//    int numEnclaves(vector<vector<int>>& grid) {
//        queue<pair<int, int>> q;
//        m = grid.size(), n = grid[0].size();
//        vector<vector<bool>> dist(m, vector<bool>(n));
//        for (int i = 0; i < m; i++)
//        {
//            if (grid[i][0] == 1)
//                dist[i][0] = true, q.push({ i, 0 });
//            if (grid[i][n - 1] == 1)
//                dist[i][n - 1] = true, q.push({ i, n - 1 });
//        }
//
//        for (int j = 0; j < n; j++)
//        {
//            if (grid[0][j] == 1)
//                dist[0][j] = true, q.push({ 0, j });
//            if (grid[m - 1][j] == 1)
//                dist[m - 1][j] = true, q.push({ m - 1, j });
//        }
//
//        while (!q.empty())
//        {
//            auto [a, b] = q.front(); q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = dx[k] + a, y = dy[k] + b;
//                if (x >= 0 && x < m && y >= 0 & y < n && !dist[x][y] && grid[x][y] == 1)
//                    dist[x][y] = true, q.push({ x, y });
//            }
//        }
//
//        int ret = 0;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//                if (grid[i][j] == 1 && !dist[i][j])
//                    ret++;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    vector<vector<int>> highestPeak(vector<vector<int>>& isWater) {
//        int m = isWater.size(), n = isWater[0].size();
//        vector<vector<int>> dist(m, vector<int>(n, -1));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (isWater[i][j] == 1)
//                    dist[i][j] = 0, q.push({ i, j });
//            }
//        }
//
//        while (q.size())
//        {
//            auto [a, b] = q.front(); q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = dx[k] + a, y = dy[k] + b;
//                if (x >= 0 && x < m && y >= 0 && y < n && dist[x][y] == -1)
//                    dist[x][y] = dist[a][b] + 1, q.push({ x,y });
//            }
//        }
//        return dist;
//    }
//};





//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int maxDistance(vector<vector<int>>& grid) {
//        int m = grid.size(), n = grid[0].size();
//        queue<pair<int, int>> q;
//        vector<vector<int>> dist(m, vector<int>(n, -1));
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j])
//                {
//                    dist[i][j] = 0;
//                    q.push({ i, j });
//                }
//            }
//
//
//        int ret = -1;
//        while (q.size())
//        {
//            auto [a, b] = q.front(); q.pop();
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && dist[x][y] == -1)
//                {
//                    dist[x][y] = dist[a][b] + 1;
//                    q.push({ x, y });
//                    ret = max(ret, dist[x][y]);
//                }
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int rotApple(vector<vector<int> >& grid) {
//        int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//        int m = grid.size(), n = grid[0].size();
//        bool vis[1001][1001];
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 2)
//                    vis[i][j] = true, q.push({ i, j });
//            }
//        }
//
//        int step = -1;
//        while (q.size())
//        {
//            step++;
//            int levelsize = q.size();
//            while (levelsize--)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; k++)
//                {
//                    int x = dx[k] + a, y = dy[k] + b;
//                    if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] == 1)
//                        vis[x][y] = true, q.push({ x, y });
//                }
//            }
//        }
//
//
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                    return -1;
//            }
//        }
//        return step;
//    }
//};




//class Solution {
//public:
//    string solve(string s, string t) {
//        string ret;
//        int len1 = s.size(), len2 = t.size(), carry = 0;
//        reverse(s.begin(), s.end());
//        reverse(t.begin(), t.end());
//        for (int i = 0; i < len1 || i < len2; i++)
//        {
//            int x = (i >= len1) ? 0 : (s[i] - '0');
//            int y = (i >= len2) ? 0 : (t[i] - '0');
//            int bit = x + y + carry;
//            ret += '0' + bit % 10;
//            carry = bit / 10;
//        }
//        if (carry) ret += '1';
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};



//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head)
//    {
//        ListNode* newhead = new ListNode(0), * cur = head;
//        while (cur)
//        {
//            ListNode* temp = cur;
//            cur = cur->next;
//
//            temp->next = newhead->next;
//            newhead->next = temp;
//        }
//
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//
//    ListNode* addInList(ListNode* head1, ListNode* head2) {
//        ListNode* cur1 = ReverseList(head1);
//        ListNode* cur2 = ReverseList(head2);
//        ListNode* newhead = new ListNode(0), * cur = newhead;
//        int bit = 0;
//        while (cur1 || cur2 || bit)
//        {
//            if (cur1) bit += cur1->val, cur1 = cur1->next;
//            if (cur2) bit += cur2->val, cur2 = cur2->next;
//            cur = cur->next = new ListNode(bit % 10);
//            bit /= 10;
//        }
//
//        cur = newhead->next;
//        delete newhead;
//        return ReverseList(cur);
//        return cur1;
//    }
//};



class Solution
{
public:
	string solve(string s, string t)
	{
		reverse(s.begin(), s.end());
		reverse(t.begin(), t.end());
		int m = s.size(), n = t.size();
		vector<int> tmp(m + n);

		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				tmp[i + j] += (s[i] - '0') * (t[j] - '0');
			}
		}

		int c = 0;
		string ret;
		for (auto x : tmp)
		{
			c += x;
			ret += c % 10 + '0';
			c /= 10;
		}
		while (c)
		{
			ret += c % 10 + '0';
			c /= 10;
		}

		while (ret.size() > 1 && ret.back() == '0') ret.pop_back();

		reverse(ret.begin(), ret.end());
		return ret;
	}
};