//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0)
//            return 0;
//        else if (n == 1 || n == 2)
//            return 1;
//        //创建dp表
//        vector<int> dp(n + 1);
//        //初始化
//        dp[0] = 0, dp[1] = dp[2] = 1;
//        //填表
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int waysToStep(int n) {
//        //特殊处理
//        if (n == 1)
//            return 1;
//        else if (n == 2)
//            return 2;
//        else if (n == 3)
//            return 4;
//        const int DEL = 1000000007;
//        vector<int> dp(n + 1);//创建dp表，多开一个空间，让下标对应
//        //初始化
//        dp[1] = 1, dp[2] = 2, dp[3] = 4;
//        //填表
//        for (int i = 4; i <= n; i++)
//            dp[i] = (dp[i - 1] + (dp[i - 2] + dp[i - 3]) % DEL) % DEL;
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        if (cost.size() <= 1)
//            return 0;
//        //创建dp表
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        //填表
//        for (int i = 2; i <= n; i++)
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        //创建dp表
//        vector<int> dp(n);
//        //初始化
//        dp[0] = s[0] != '0';
//        if (n == 1)
//            return dp[0];
//        if (s[0] != '0' && s[1] != '0')
//            dp[1]++;
//        int tmp = (s[0] - '0') * 10 + s[1] - '0';
//        if (tmp >= 10 && tmp <= 26)
//            dp[1]++;
//        //填表
//        for (int i = 2; i < n; i++)
//        {
//            if (s[i] != '0')
//                dp[i] += dp[i - 1];
//            int tmp = (s[i - 1] - '0') * 10 + s[i] - '0';
//            if (tmp >= 10 && tmp <= 26)
//                dp[i] += dp[i - 2];
//        }
//        return dp[n - 1];
//    }
//};



