//class Solution {
//public:
//    vector<int> getRow(int rowIndex) {
//        if (rowIndex == 0) return { 1 };
//        if (rowIndex == 1) return { 1, 1 };
//        vector<int> arr(rowIndex + 1);
//        arr[0] = arr[1] = 1;
//        for (int i = 2; i <= rowIndex; i++) // ����
//        {
//            arr[i] = 1;
//            for (int j = i - 1; j >= 1; j--)
//                arr[j] = arr[j] + arr[j - 1];
//        }
//        return arr;
//    }
//};



//class Solution {
//public:
//    int hash1[128] = { 0 }, hash2[128] = { 0 };
//    vector<int> findAnagrams(string s, string p) {
//        vector<int> ret;
//        for (auto ch : p)
//            hash2[ch]++;
//        int len = p.size();
//        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//        {
//            char in = s[right];
//            hash1[in]++;
//            if (hash2[in] && hash1[in] <= hash2[in])
//                count++;
//            if (right - left + 1 > len)
//            {
//                char out = s[left++];
//                hash1[out]--;
//                if (hash2[out] && hash1[out] < hash2[out])
//                    count--;
//            }
//            if (count == len)
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};




//class MyCircularQueue {
//public:
//    MyCircularQueue(int k) {
//        arr = vector<int>(k + 1);
//        front = rear = 0;
//        capacity = k;
//    }
//
//    bool enQueue(int value) {
//        if (isFull()) return false;
//        arr[rear++] = value;
//        rear %= (capacity + 1);
//        return true;
//    }
//
//    bool deQueue() {
//        if (isEmpty()) return false;
//        front++;
//        front %= (capacity + 1);
//        return true;
//    }
//
//    int Front() {
//        return isEmpty() ? -1 : arr[front];
//    }
//
//    int Rear() {
//        return isEmpty() ? -1 : arr[(rear + capacity) % (capacity + 1)];
//    }
//
//    bool isEmpty() {
//        return rear == front;
//    }
//
//    bool isFull() {
//        return (rear + 1) % (capacity + 1) == front;
//    }
//    vector<int> arr;
//    int front;
//    int rear;
//    int capacity;
//};





//class Solution {
//public:
//    string removeDuplicates(string s) {
//        string ret;
//        for (auto ch : s)
//        {
//            if (ret.size() && ret.back() == ch) ret.pop_back();
//            else ret.push_back(ch);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        string ans1, ans2;
//        for (auto ch : s)
//        {
//            if (ch == '#')
//            {
//                if (ans1.size()) ans1.pop_back();
//            }
//            else
//                ans1.push_back(ch);
//        }
//
//        for (auto ch : t)
//        {
//            if (ch == '#')
//            {
//                if (ans2.size()) ans2.pop_back();
//            }
//            else
//                ans2.push_back(ch);
//        }
//        return ans1 == ans2;
//    }
//};





