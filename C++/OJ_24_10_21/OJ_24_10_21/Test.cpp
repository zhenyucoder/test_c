//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (j + nums[j] >= i)
//                    dp[i] = min(dp[i], dp[j] + 1);
//            }
//        }
//        return dp[n - 1];
//    }
//};




//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        if (nums.size() < 2) return 0;
//        int n = nums.size(), count = 0;
//        for (int left = 0, right = 0, maxpos = 0; right < n - 1; left++)
//        {
//            maxpos = max(maxpos, left + nums[left]);
//            if (left == right)
//            {
//                left = right;
//                right = maxpos;
//                count++;
//                maxpos = 0;
//            }
//        }
//        return count;
//    }
//};




//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        int left = 0, right = 0, maxpos = 0, n = nums.size();
//        while (left <= right)
//        {
//            if (right >= n - 1) return true;
//            while (left <= right) // 遍历当前层，更新下一层最大区间
//                maxpos = max(maxpos, left + nums[left++]);
//            left = right + 1,
//                right = maxpos;
//            maxpos = 0;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (gas[i] - cost[i] >= 0)
//            {
//                int resp = 0;
//                int step = 0;
//                for (; step < n; step++)
//                {
//                    int index = (i + step) % n;
//                    resp += gas[index] - cost[index];
//                    if (resp < 0) break;
//                }
//                if (resp >= 0) return i;
//                i = step + i;
//            }
//        }
//        return -1;
//    }
//};



//class Solution {
//public:
//    bool check(string str)
//    {
//        for (int i = 1; i < str.size(); i++)
//            if (str[i] < str[i - 1]) return false;
//        return true;
//    }
//    int monotoneIncreasingDigits(int n) {
//        for (int i = n; i >= 0; i--)
//        {
//            if (check(to_string(i))) return i;
//        }
//        return 0;
//    }
//};


class Solution {
public:
    int monotoneIncreasingDigits(int n) {
        string str = to_string(n);
        int i = 0, m = str.size();
        while (i + 1 < m && str[i + 1] > str[i])
            i++;
        if (i + 1 == m) return n;
        while (i - 1 >= 0 && str[i] == str[i - 1]) i--;
        str[i]--;
        for (int j = i + 1; j < m; j++)
            str[j] = '9';
        return stoi(str);
    }
};

