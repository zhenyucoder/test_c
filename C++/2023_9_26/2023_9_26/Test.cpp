#include <iostream>
#include <assert.h>
using namespace std;


//struct Stack
//{
//	//成员函数
//	void Init(int x=4)
//	{
//		if (x == 0)
//		{
//
//			a = nullptr;
//			capacity = top = 0;
//		}
//		else
//		{
//			a = (int*)malloc(sizeof(int) * x);
//			top = 0;
//			capacity = x;
//		}
//	}
//
//	void Push(int x)
//	{
//		a[top] = x;
//		top++;
//	}
//
//	int Top()
//	{
//		return a[top - 1];
//	}
//
//	void Pop()
//	{
//		top--;
//	}
//
//	void print()
//	{
//		for (int i = top; i > 0; i--)
//		{
//			printf("%d ", Top());
//			Pop();
//		}
//	}
//	//..... 
//	
//	//成员变量
//	int* a;
//	int top;
//	int capacity;
//};
//
//int main()
//{
//	Stack st;
//	st.Init(4);
//	st.Push(1);
//	st.Push(2);
//	st.Push(3);
//	st.Push(4);
//	st.print();
//	return 0;
//}





//class Stack//class默认权限私有
//{
//public:
//	// 成员函数
//	void Init(int newcapacity = 4)
//	{
//		if (newcapacity == 0)
//		{
//			a = nullptr;
//			top = newcapacity = 0;
//		}
//		else
//		{
//			a = (int*)malloc(sizeof(int) * newcapacity);
//			top = 0;
//			capacity = newcapacity;
//		}
//	}
//
//	void Push(int x)
//	{
//		if (top == capacity)
//		{
//			size_t newcapacity = capacity == 0 ? 4 : capacity * 2;
//			int* tmp = (int*)realloc(a, sizeof(int) * newcapacity);
//			if (tmp == nullptr)
//			{
//				printf("malloc fail");
//				exit(-1);
//			}
//
//			a = tmp;
//			capacity = newcapacity;
//		}
//
//		a[top++] = x;
//	}
//
//	int Top()
//	{
//		return a[top - 1];
//	}
//
//	void Destroy()
//	{
//		a = nullptr;
//		free(a);
//		top = capacity = 0;
//	}
//
//private:
//	// 成员变量
//	int* a;
//	int top;
//	int capacity;
//};
//
//int main()
//{
//	Stack st;
//	st.Init(5);
//	st.Push(2);
//	st.Push(3);
//	st.Push(4);
//	st.Push(5);
//
//	st.Destroy();
//	return 0;
//}




//class Date
//{
//public:
//	void Init(int year = 2023, int month = 9, int day = 26)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year<<"/" << _month<<"/"<<_day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//int  main()
//{
//	Date d1;
//	d1.Init(2000, 12, 2);
//	d1.Print();
//	cout << endl;
//
//
//	Date d2;
//	d2.Init();
//	d2.Print();
//
//	cout << sizeof(d1) << endl;
//	cout << sizeof(Date) << endl;
//
//	return  0;
//}



//class A1
//{
//public:
//	void func();
//private:
//	int a;
//	char c;
//};
//
////类中仅有成员函数
//class A2
//{
//public:
//	void func();//成员函数存在公共代码区
//};
//
////空类
//class A3
//{
//
//};
//
//int main()
//{
//	cout << sizeof(A1) << endl;
//	// 分配1个字节，不存储数据，占位，仅表示对象存在过
//	cout << sizeof(A2) << endl;
//	cout << sizeof(A3) << endl;
//	return 0;
//}



//class A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
////private:
//	int _a;
//};
//
//int main()
//{
//	A* p = nullptr;
//	p->Print();//p传this指针，存到寄存器
//	p->_a;//由于编译器的各种优化，p仅仅指向a，没有进行任何操作，不会生成任何指令
//	return 0;
//}


//class date
//{
//public:
//	// this在实参和形参位置不能显示写
//	// 但是在类里面可以显示的用
//	void init(int year, int month, int day)
//	{
//		cout << this << endl;
//
//		//this->_year = year;
//		//this->_month = month;
//		//this->_day = day;
//
//		_year = year;
//		_month = month;
//		_day = day;
//		this->print();//通过this去调用
//	}
//
//	void print()
//	{
//		cout << this << endl;
//
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//
//private:
//	int _year;   // 声明
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	date d1;
//	d1.init(2023, 7, 20);
//	d1.print();
//
//	/*date d2;
//	d2.init(2023, 7, 21);
//	d2.print();*/
//
//	return 0;
//}





//class Date
//{
//public:
//	/*Date()
//	{
//		cout << "Date()" << endl;
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}*/
//
//	Date(int year=1, int month=1, int day=1)
//	{
//		cout << "Date()" << endl;
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	/*void Init(int year = 2023, int month = 9, int day = 26)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	void Print()
//	{
//		cout << _year<<"/" << _month<<"/"<<_day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1;
//	//data d1();//err 和函数声明产生歧义
//	d1.Print();
//
//	Date d2(2023, 9, 26);
//	d2.Print();
//
//	Date d3(2023, 9);
//	d3.Print();
//	return 0;
//}




//class Stack//class默认权限私有
//{
//public:
//	// 成员函数
//	/*Stack()
//	{
//		a = nullptr;
//		top = capacity = 0;
//	}*/
//	
//	Stack(int  n = 4)
//	{
//		if (n == 0)
//		{
//			a = nullptr;
//			top = capacity = 0;
//		}
//		else
//		{
//			int* tmp = (int*)malloc(sizeof(int) * n);
//			if (tmp == nullptr)
//			{
//				cout << "malloc fail" << endl;
//				exit(-1);
//			}
//			a = tmp;
//			top = 0;
//			capacity = n;
//		}
//	}
//
//	void Push(int x)
//	{
//		if (top == capacity)
//		{
//			size_t newcapacity = capacity == 0 ? 4 : capacity * 2;
//			int* tmp = (int*)realloc(a, sizeof(int) * newcapacity);
//			if (tmp == nullptr)
//			{
//				cout << "malloc fail" << endl;
//				exit(-1);
//			}
//
//			if (a == tmp)
//			{
//				cout << "原地" << endl;
//			}
//			else
//			{
//				cout << "异地" << endl;
//			}
//
//			a = tmp;
//			capacity = newcapacity;
//		}
//
//		a[top++] = x;
//	}
//
//	int Top()
//	{
//		return a[top - 1];
//	}
//
//	void Destroy()
//	{
//		free(a);
//		a = nullptr;
//		top = capacity = 0;
//	}
//
//	bool Empty()
//	{
//		return top == 0;
//	}
//
//	void Pop()
//	{
//		assert(top > 0);
//		top--;
//	}
//
//private:
//	// 成员变量
//	int* a;
//	int top;
//	int capacity;
//};
//
//
//int main()
//{
//	Stack st1;
//	st1.Push(1);
//	st1.Push(2);
//	st1.Push(3);
//	st1.Push(4);
//	st1.Push(5);
//	st1.Push(6);
//	while (!st1.Empty())
//	{
//		cout << st1.Top() << " ";
//		st1.Pop();
//	}
//	cout << endl;
//
//	Stack st2(1000);
//	for (int i = 0; i < 1000; i++)
//	{
//		st2.Push(i + 1);
//	}
//
//	while (!st2.Empty())
//	{
//		cout << st2.Top() << " ";
//		st2.Pop();
//	}
//	cout << endl;
//	return 0;
//}



class Stack//class默认权限私有
{
public:
	
	Stack(int  n = 4)
	{
		cout << "Stack(int  n = 4)" << endl;
		if (n == 0)
		{
			a = nullptr;
			top = capacity = 0;
		}
		else
		{
			int* tmp = (int*)malloc(sizeof(int) * n);
			if (tmp == nullptr)
			{
				cout << "malloc fail" << endl;
				exit(-1);
			}
			a = tmp;
			top = 0;
			capacity = n;
		}
	}

	void Push(int x)
	{
		if (top == capacity)
		{
			size_t newcapacity = capacity == 0 ? 4 : capacity * 2;
			int* tmp = (int*)realloc(a, sizeof(int) * newcapacity);
			if (tmp == nullptr)
			{
				cout << "malloc fail" << endl;
				exit(-1);
			}

			if (a == tmp)
			{
				cout << "原地" << endl;
			}
			else
			{
				cout << "异地" << endl;
			}

			a = tmp;
			capacity = newcapacity;
		}

		a[top++] = x;
	}

	int Top()
	{
		return a[top - 1];
	}

	void Destroy()
	{
		free(a);
		a = nullptr;
		top = capacity = 0;
	}

	bool Empty()
	{
		return top == 0;
	}

	void Pop()
	{
		assert(top > 0);
		top--;
	}

private:
	// 成员变量
	int* a;
	int top;
	int capacity;
};

//两栈实现队列
class MyQueue
{
private:
	Stack _pushst;
	Stack _popst;
};

class Date
{
public:
	
	Date(int year, int month=1, int day=1)
	{
		cout << "Date()" << endl;
		_year = year;
		_month = month;
		_day = day;
	}

	Date( )
	{
		cout << "Date()" << endl;
		_year = 12234241;
		_day = 2;
	}

	void Print()
	{
		cout << _year<<"/" << _month<<"/"<<_day << endl;
	}

private:
	int _year=2023;
	int _month=9;
	int _day=26;
};

int main()
{
	Date d1;
	d1.Print();

	MyQueue q;
	return 0;
}
