//class Solution
//{
//public:
//	vector<int> searchRange(vector<int>& nums, int target)
//	{
//		if (nums.size() == 0) return { -1, -1 };
//
//		int begin = 0;
//		int left = 0, right = nums.size() - 1;
//		while (left < right)
//		{
//			int mid = left + (right - left) / 2;
//			if (nums[mid] < target) left = mid + 1;
//			else right = mid;
//		}
//		if (nums[left] != target) return { -1, -1 };
//		else begin = left;
//		left = 0, right = nums.size() - 1;
//		while (left < right)
//		{
//			int mid = left + (right - left + 1) / 2; 
//			if (nums[mid] <= target) left = mid;
//			else right = mid - 1;
//		}
//		return { begin, right };
//	}
//};




//class Solution
//{
//public:
//	int searchInsert(vector<int>& nums, int target)
//	{
//		int left = 0, right = nums.size() - 1;
//		while (left < right)
//		{
//			int mid = left + (right - left) / 2;
//			if (nums[mid] < target) left = mid + 1;
//			else right = mid;
//		}
//		if (nums[left] < target) return right + 1;
//		return right;
//	}
//};





//class Solution
//{
//public:
//	int mySqrt(int x)
//	{
//		if (x < 1) return 0;
//		int left = 1, right = x;
//		while (left < right)
//		{
//			long long mid = left + (right - left + 1) 
//			if (mid * mid <= x) left = mid;
//			else right = mid - 1;
//		}
//		return left;
//	}
//}��




//class Solution
//{
//public:
//	int peakIndexInMountainArray(vector<int>& arr)
//	{
//		int left = 1, right = arr.size() - 2;
//		while (left < right)
//		{
//			int mid = left + (right - left + 1) / 2;
//			if (arr[mid] > arr[mid - 1]) left = mid;
//			else right = mid - 1;
//		}
//		return left;
//	}
//};


class Solution
{
public:
	int findMin(vector<int>& nums)
	{
		int left = 0, right = nums.size() - 1;
		int x = nums[right]; 
		while (left < right)
		{
			int mid = left + (right - left) / 2;
			if (nums[mid] > x) left = mid + 1;
			else right = mid;
		}
		return nums[left];
	}
}��