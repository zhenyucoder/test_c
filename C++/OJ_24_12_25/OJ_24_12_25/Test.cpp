//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        unordered_map<string, vector<string>> hash;
//        for (auto& str : strs)
//        {
//            string key = str;
//            sort(key.begin(), key.end());
//            hash[key].push_back(str);
//        }
//
//
//        vector<vector<string>> ret;
//        for (auto& [a, b] : hash)
//        {
//            ret.push_back(b);
//        }
//        return ret;
//    }
//};


class Solution {
public:
    #define LL long long
    int numberOf2sInRange(int n) {
        LL weight = 1;
        int res = 0;
        while (weight <= n) {
            int cur = n / weight % 10;
            if (cur > 2) {
                res += n / weight / 10 * weight + weight;
            }
            else if (cur < 2) {
                res += n / weight / 10 * weight;
            }
            else {
                res += n / weight / 10 * weight + n % weight + 1;
            }
            weight *= 10;
        }
        return res;
    }
};