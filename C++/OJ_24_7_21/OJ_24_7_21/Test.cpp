//class Solution {
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        unordered_map<int, int> hash;
//        int n = arr.size();
//        for (int i = 0; i < n; i++)
//            hash[arr[i]] = i;
//
//        int ret = 2;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        for (int i = 2; i < n; i++)
//        {
//            for (int j = i - 1; j >= 1; j--)
//            {
//                int x = arr[i] - arr[j];
//                if (hash.count(x) && hash[x] < j)
//                {
//                    dp[j][i] = max(dp[j][i], dp[hash[x]][j] + 1);
//                    ret = max(ret, dp[j][i]);
//                }
//            }
//        }
//        return ret == 2 ? 0 : ret;
//    }
//};




//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] : true;
//                }
//            }
//        }
//        int ret = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (dp[i][j])
//                    ret++;
//            }
//        }
//        return ret;
//    }
//};





//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size(), maxlen = 0, pos = 0;
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] : true;
//                if (dp[i][j] && (j - i + 1) > maxlen)
//                {
//                    maxlen = j - i + 1;
//                    pos = i;
//                }
//            }
//        }
//        cout << pos << " " << maxlen << endl;
//        return s.substr(pos, maxlen);
//    }
//};




//class Solution {
//public:
//    bool checkPartitioning(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] : true;
//            }
//        }
//
//        for (int i = 1; i < n - 1; i++)
//            for (int j = i; j < n - 1; j++)
//                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1])
//                    return true;
//        return false;
//    }
//};




//class Solution {
//public:
//    int minCut(string s) {
//        int n = s.size();
//        vector<vector<bool>> info(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    info[i][j] = (i + 1 < j) ? info[i + 1][j - 1] : true;
//            }
//        }
//
//        vector<int> dp(n, 0x3f3f3f3f);
//        for (int i = 0; i < n; i++)
//        {
//            if (!info[0][i])
//            {
//                for (int j = i; j > 0; j--)
//                {
//                    if (info[j][i])
//                        dp[i] = min(dp[i], dp[j - 1] + 1);
//                }
//            }
//            else
//            {
//                dp[i] = 0;
//            }
//        }
//        return dp[n - 1];
//    }
//};





//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = (i + 1 < j) ? dp[i + 1][j - 1] + 2 : (j - i + 1);
//                else
//                    dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};




//class Solution {
//public:
//    int minInsertions(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n, INT_MAX));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : 0;
//                else
//                    dp[i][j] = min(dp[i + 1][j], dp[i][j - 1]) + 1;
//            }
//        }
//        return dp[0][n - 1];
//    }
//};




//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int m = text1.size(), n = text2.size();
//        text1 = ' ' + text1, text2 = ' ' + text2;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (text1[i] == text2[j])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};




//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[m][n];
//    }
//};




