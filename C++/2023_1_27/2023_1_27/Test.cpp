#include <iostream>
using namespace std;

//struct A
//{
//	int a : 19;
//	int c : 11;
//	int d : 5;
//	char e;
//	int g : 23;
//	int w : 20;
//	char y;
//};
//int main()
//{
//	cout << sizeof(A) << endl;
//	return 0;
//}


//int main() {
//    string s;
//    getline(cin, s);
//    reverse(s.begin(), s.end());
//
//    for (int left = 0, right = 0; right <= s.size(); right++)
//    {
//        if (s[right] == ' ' || right == s.size())
//        {
//            reverse(s.begin() + left, s.begin() + right);//反转单词
//            left = right + 1;
//        }
//    }
//    cout << s << endl;
//}


//int main() {
//    string s;
//    getline(cin, s);
//    reverse(s.begin(), s.end());//翻转字符串
//    auto start = s.begin();
//    while (start != s.end())
//    {
//        auto end = start;
//        while (end != s.end() && *end != ' ')
//            end++;
//        reverse(start, end);//反转单个单词
//
//        if (end != s.end())
//            start = end + 1;
//        else
//            start = end;
//    }
//    cout << s << endl;
//}

//int main() {
//    string s1, s2;
//    cin >> s1;
//    while (cin >> s2)
//        s1 = s2 + " " + s1;//将每次读入的字符放加到开头
//    cout << s1 << endl;
//}


#include <vector>
//int main()
//{
//	int n;
//	cin >> n;
//	// 注意这里多给了一个值，是处理越界的情况的比较，具体参考上面的解题思路
//	vector<int> a;
//	a.resize(n + 1);
//	a[n] = 0;
//	//读入数组
//	int i = 0;
//	for (i = 0; i < n; ++i)
//		cin >> a[i];
//	i = 0;
//	int count = 0;
//	while (i < n)
//	{
//		// 非递减子序列
//		if (a[i] < a[i + 1])
//		{
//			while (i < n && a[i] <= a[i + 1])
//				i++;
//			count++;
//			i++;
//		}
//		else if (a[i] == a[i + 1])
//		{
//			i++;
//		}
//		else // 非递增子序列
//		{
//			while (i < n && a[i] >= a[i + 1])
//				i++;
//			count++;
//			i++;
//		}
//	}
//	cout << count << endl;
//	return 0;
//}

//int main() {
//	int n;
//	while (cin >> n)
//	{
//		vector<int> nums;
//		nums.resize(n + 1);//多开一个处理边界情况
//		for (int i = 0; i < n; i++)//输入数据
//		{
//			cin >> nums[i];
//		}
//		int count = 0, i = 0;
//		while (i < n)
//		{
//			if (i + 1 < n && nums[i] < nums[i + 1])//非递减
//			{
//				while (i + 1 < n && nums[i] <= nums[i + 1])
//					i++;
//				count++;
//				i++;
//			}
//			else if (i + 1 < n && nums[i] > nums[i + 1])//非递增
//			{
//				while (i + 1 < n && nums[i] >= nums[i + 1])
//					i++;
//				count++;
//				i++;
//			}
//			else
//			{
//				while (i + 1 < n && nums[i] == nums[i + 1])
//					i++;
//			}
//		}
//		cout << count << endl;
//	}
//}

#include <string>

//int main() {
//    string str;
//    getline(cin, str);
//    string target, tmpstr;
//    for (int i = 0; i <= str.size(); i++)
//    {
//        if (str[i] >= '0' && str[i] <= '9')
//            tmpstr += str[i];
//        else
//        {
//            if (target.size() < tmpstr.size())
//                target = tmpstr;
//            else
//            tmpstr.clear();
//        }
//    }
//    cout << target << endl;
//}
//int MoreThanHalfNum_Solution(vector<int>& numbers) {
//    if (numbers.empty())
//        return 0;
//    int hash[10001] = { 0 };
//    for (auto num : numbers)
//    {
//        hash[num]++;
//    }
//
//    int level = numbers.size() / 2;
//    for (auto num : numbers)
//    {
//        if (hash[num] > level)
//            return num;
//    }
//    return 0;
//}


//int MoreThanHalfNum_Solution(vector<int>& numbers) {
//    int cond = -1;//但返回众数
//    int cnt = 0;//众数次数
//    for (int i = 0; i < numbers.size(); i++)//得到众数
//    {
//        if (cnt == 0)
//        {
//            cond = numbers[i];
//            cnt++;
//        }
//        else
//        {
//            if (cond == numbers[i])
//                cnt++;
//            else
//                cnt--;
//        }
//    }
//    cout << cond << endl;
//    //统计众数出现次数
//    cnt = 0;
//    for (auto num : numbers)
//    {
//        if (num == cond)
//            cnt++;
//    }
//
//    return (cnt > numbers.size() / 2) ? cond : 0;
//}
//
//int main()
//{
//    vector<int> v{ 1,2,3,2,2,2,5,4,2 };
//    cout<<MoreThanHalfNum_Solution(v)<<endl;
//    return 0;
//}


//int main() {
//    int a, b, c, d;
//    while (cin >> a >> b >> c >> d)
//    {
//        //2倍A,B,B
//        int dA = a + c;
//        int dB = b + d;
//        int dC = d - b;
//        if ((dA >= 0 && dB >= 0 && dC >= 0)
//            && (dA % 2 == 0 && dB % 2 == 0 && dC % 2 == 0))
//            cout << dA / 2 << " " << dB / 2 << " " << dC / 2 << endl;
//        else
//            cout << "No" << endl;
//    }
//}


int main() {
    int M = 0, N = 0;//M转化为N进制数
    cin >> M >> N;
    string target, table = "0123456789ABCDEF";
    bool flag = false;//标记是否为负数
    if (M < 0)
    {
        flag == true;
        M = 0 - M;
    }

    while (M)
    {
        target += table[M % N];
        M /= N;
    }
    if (target == "")
        target += table[0];
    if (flag)
        target += '-';
    reverse(target.begin(), target.end());
    cout << target << endl;
}