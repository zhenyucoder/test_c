//bool isPal(string str, int left, int right)
//{
//	while (left < right)
//	{
//		if (str[left++] != str[right--])
//			return false;
//	}
//	return true;
//}
//
//int main()
//{
//	string str;
//	cin >> str;
//	//特殊处理
//	bool flag = true;//判断字符串中字符是否全部相等
//	for (int left = 0, right = 0; right < n; right++)
//		if (str[left] != str[right])
//		{
//			flag = false;
//			break;
//		}
//	if (flag)
//	{
//		cout << 0 << endl;
//	}
//	else
//	{
//		if (isPal(str, 0, str.size()))
//			cout << str.size() - 1 << endl;
//		else
//			cout << str.size() << endl;
//	}
//	return 0;
//}



//int arr[100001] = { 0 };
//
//int Comdiv(int x, int y)
//{
//	if (y == 0)
//		return x;
//	else
//		return Comdiv(y, x % y);
//}
//
//int main()
//{
//	int n, force;
//	cin >> n >> force;
//	for (int i = 0; i < n; i++)
//		cin >> arr[i];
//	for (int i = 0; i < n; i++)
//	{
//		if (force >= arr[i])
//			force += arr[i];
//		else
//			force += Comdiv(force, arr[i]);
//	}
//	return 0;
//}



//int main()
//{
//	int n, m;
//	cin >> n >> m;
//	vector<vector<int>> v(n, vector<int>(m));
//	for (int i = 0; i < n; i++)
//		for (int j = 0; j < m; j++)
//			cin >> v[i][j];
//	vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//	for (int i = 1; i <= n; i++)
//		for (int j = 1; j <= m; j++)
//			dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//	cout << dp[n][m] << endl;
//	return 0;
//}



bool hahvSame(vector<string>& sv, int left, int right)
{
	int hash[26] = { 0 };
	for (auto ch : sv[left])
		hash[ch - 'a']++;
	for (auto ch : sv[right])
		if (hash[ch - 'a'])
			return true;
	return false;
}

int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n; cin >> n;
		vector<string> sv(n);
		for (int i = 0; i < n; i++)
			cin >> sv[i];
		bool flag = true;
		for (int left = 0, right = n - 1; left < right; left--, right++)
		{
			if (haveSame(sv, left, right) == false)
			{
				flag = false;
				break;
			}
		}
		if (flag)
			cout << "Yes" << endl;
		else
			cout << "No" << endl;
	}
	return 0;
 }