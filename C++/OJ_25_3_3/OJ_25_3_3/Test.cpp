//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == nullptr || p == root || q == root) return root;
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        if (left != nullptr && right != nullptr) return root;
//        return left ? left : right;
//    }
//};




class Solution {
public:
    vector<string> findRelativeRanks(vector<int>& score) {
        int sz = score.size();
        vector<int> hash(sz);
        for (int i = 0; i < sz; i++)
            hash[i] = i;
        sort(hash.begin(), hash.end(), [&score](const int& x, const int& y) {
            return score[x] > score[y];
            });
        vector<string> ret(sz);
        for (int i = 0; i < sz; i++)
        {
            if (i == 0) ret[hash[i]] = "Gold Medal";
            else if (i == 1) ret[hash[i]] = "Silver Medal";
            else if (i == 2) ret[hash[i]] = "Bronze Medal";
            else ret[hash[i]] = to_string(i + 1);
        }
        return ret;
    }
};