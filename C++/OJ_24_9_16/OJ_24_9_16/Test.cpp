//#include <iostream>
//#include <string>
//#include <cmath>
//using namespace std;
//
//
//bool isprime(long long x)
//{
//    for (int i = 2; i <= sqrt(x); i++)
//    {
//        if (x % i == 0) return false;
//    }
//    return true;
//}
//
//long long change(string str)
//{
//    for (int i = str.size() - 2; i >= 0; i--)
//        str += str[i];
//    return  stol(str);
//}
//
//int main()
//{
//    string str;
//    cin >> str;
//    bool ret = isprime(change(str));
//    cout << (ret ? "prime" : "noprime") << endl;
//    return 0;
//}




//int main()
//{
//    string str;
//    cin >> str;
//    int ret = 0, count = 0; // 统计出现字符种类
//    int hash[26] = { 0 }; // 字符出现次数
//    for (int left = 0, right = 0, count = 0; right < str.size(); right++)
//    {
//        int in = str[right] - 'a';
//        if (hash[in]++ == 0)
//            count++;
//        while (count > 2)
//        {
//            int out = str[left++] - 'a';
//            if (--hash[out] == 0)
//                count--;
//        }
//        ret = max(ret, right - left + 1);
//    }
//    cout << ret << endl;
//    return 0;
//}




class Solution {
public:
	vector<string> ret; 
	string path; 
	bool vis[11] = { 0 }; 
	int n;
	string s;
	vector<string> Permutation(string str)
	{
		n = str.size();
		sort(str.begin(), str.end());
		s = str;

		dfs(0);
		return ret;
	}
	void dfs(int pos)
	{
		if (pos == n)
		{
			ret.push_back(path);
			return;
		}
		// 填 pos 位置
		for (int i = 0; i < n; i++)
		{
			if (!vis[i])
			{
				if (i > 0 && s[i] == s[i - 1] && !vis[i - 1]) continue;
				path.push_back(s[i]);
				vis[i] = true;
				dfs(pos + 1);
				// 恢复现场
				vis[i] = false;
				path.pop_back();
			}
		}
	}
};