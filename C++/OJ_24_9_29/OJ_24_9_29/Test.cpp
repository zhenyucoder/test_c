//#include <iostream>
//using namespace std;
//const int N = 1010;
//int n;
//int arr[N], f[N], g[N];
//int main()
//{
//	cin >> n;
//	for (int i = 1; i <= n; i++) cin >> arr[i];
//	for (int i = 1; i <= n; i++)
//	{
//		f[i] = 1;
//		for (int j = 1; j < i; j++)
//		{
//			if (arr[j] < arr[i])
//			{
//				f[i] = max(f[i], f[j] + 1);
//			}
//		}
//	}
//	for (int i = n; i >= 1; i--)
//	{
//		g[i] = 1;
//		for (int j = i + 1; j <= n; j++)
//		{
//			if (arr[i] > arr[j])
//			{
//				g[i] = max(g[i], g[j] + 1);
//			}
//		}
//	}
//	int len = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		len = max(len, f[i] + g[i] - 1);
//	}
//	cout << n - len << endl;
//	return 0;
//}



#include <iostream>
using namespace std;
const int N = 2010;
int n;
int arr[N];
int bfs()
{
	int left = 1, right = 1;
	int ret = 0;
	while (left <= right)
	{
		ret++;
		int r = right;
		for (int i = left; i <= right; i++)
		{
			r = max(r, arr[i] + i);
			if (r >= n)
			{
				return ret;
			}
		}
		left = right + 1;
		right = r;
	}
	return -1;
}
int main()
{
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> arr[i];

	cout << bfs() << endl;

	return 0;
}