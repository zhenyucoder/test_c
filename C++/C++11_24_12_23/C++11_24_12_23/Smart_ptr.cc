#include <iostream>
#include <mutex>
#include <string>
#include <functional>
using namespace std;


template <class T>
class Shared_ptr
{
public:
	Shared_ptr(T * ptr = nullptr)
		:_ptr(ptr), _Pcount(new std::atomic<int>(1))
	{}

	Shared_ptr(const Shared_ptr<T>& sp)
		:_ptr(sp._ptr), _Pcount(sp._Pcount)
	{
		(*_Pcount)++;
	}

	template <class D>
	Shared_ptr(const Shared_ptr<T>& sp, D del)
		:_ptr(sp._ptr), _Pcount(sp._Pcount), _del(del)
	{
		(*_Pcount)++;
	}
	Shared_ptr<T>& operator=(const Shared_ptr<T>& sp)
	{
		if (_ptr != sp._ptr) // 防止直接或间接和自己赋值，指向同一块空间
		{
			release(); // 在赋值前，将自身引用计数--
			_ptr = sp._ptr;
			_Pcount = sp._Pcount;
			(*_Pcount)++;
			return *this;
		}
	}

	int use_count() const { return _Pcount->load(); }
	T* get() const  { return _ptr; } // const 防止权限放大
	T* operator->() { return _ptr; }
	T& operator*() { return *_ptr; }


	~Shared_ptr()
	{
		release();
	}
private:
	void release()
	{
		if (--(*_Pcount) == 0)
		{
			_del(_ptr);
			delete _Pcount;
		}
	}
private:
	T* _ptr;
	std::atomic<int>* _Pcount; // 不能设置为静态（static int count）
	std::function<void(T*)> _del = [](T* ptr) { delete ptr; };
};



// 不参与Shared_ptr的引用计数，不支持RAII。
// 但如果Shared_ptr指向的空间释放，可能会导致野指针。
// 所以实际Weak_ptr会有一个use_count观察Shared_ptr引用计数
template <class T>
class Weak_ptr
{
public:
	Weak_ptr() :_ptr(nullptr)
	{}
	Weak_ptr(const Shared_ptr<T>& sp)
		:_ptr(sp.get())
	{}

	Weak_ptr<T>& operator=(const Shared_ptr<T>& sp)
	{
		_ptr = sp.get();
		return *this;
	}

	T& operator*() { return *_ptr; }
	T* operator->() { return _ptr; }
private:
	T* _ptr;
};


struct ListNode
{
	int _data;
	Weak_ptr<ListNode> _prev;
	Weak_ptr<ListNode> _next;
};

//int main()
//{
//	Shared_ptr<ListNode> n1 = new ListNode;
//	Shared_ptr<ListNode> n2 = new ListNode;
//	cout << n1.use_count() << endl;
//	cout << n2.use_count() << endl;
//
//	n1->_next = n2;
//	n2->_prev = n1;
//
//	Shared_ptr<ListNode> n3(new ListNode[10], [](ListNode* ptr) {delete[] ptr; });
//
//	cout << n1.use_count() << endl;
//	cout << n2.use_count() << endl;
//
//
//	return 0;
//}



class CopyBan
{
public:
private:
	CopyBan(const CopyBan&) = delete;
	CopyBan& operator=(const CopyBan&) = delete;
};


// 析构函数私有，提供一个静态函数进行释放资源
//class HeapOnly
//{
//public:
//	static void Destory(HeapOnly *ptr)
//	{
//		delete ptr;
//	}
//private:
//	HeapOnly(const HeapOnly&) = delete;
//	HeapOnly& operator=(const HeapOnly&) = delete;
//	~HeapOnly();
//};

// 将构造函数私有，提供一个静态函数获取对象
class HeapOnly
{
public:
	static HeapOnly* CreateObj()
	{
		return new HeapOnly;
	}
private:
	HeapOnly(const HeapOnly&) = delete;  // HeapOnly* ptr = HeapOnly::CreateObj();在栈上创建对象
	//HeapOnly& operator=(const HeapOnly&) = delete;
	HeapOnly();
};

class StackOnly
{
public:
	static StackOnly CreateObj()
	{
		return StackOnly();
	}
private:
	// 禁掉operator new可以把下面用new 调用拷贝构造申请对象给禁掉
	// StackOnly obj = StackOnly::CreateObj();
	// StackOnly* ptr3 = new StackOnly(obj);
	void* operator new(size_t size) = delete;
	void operator delete(void* p) = delete;
	StackOnly();
};

//class Singleton
//{
//public:
//	static Singleton* GetInstance()
//	{
//		return &m_instance;
//	}
//private:
//	Singleton() {};
//	Singleton( Singleton const &) = delete;
//	Singleton& operator=( Singleton const &) = delete;
//	static Singleton m_instance;
//};
//Singleton Singleton::m_instance;
//
//
//class Singleton
//{
//public:
//	static Singleton& GetInstance()
//	{
//		if (m_instance == nullptr)
//		{
//			unique_lock<std::mutex> lock(_mutex);
//			if (m_instance == nullptr)
//			{
//				m_instance = new Singleton;
//			}
//		}
//		return *m_instance;
//	}
//private:
//	Singleton() {};
//	Singleton( Singleton const &) = delete;
//	Singleton& operator=( Singleton const &) = delete;
//	static Singleton* m_instance;
//	static std::mutex _mutex;
//};
//Singleton* Singleton::m_instance = nullptr;
//std::mutex Singleton::_mutex;


class singleton
{
public:
	static singleton& getinstance()
	{
		static singleton sl; // 懒汉模式，局部静态变量是在第一次调用时创建初始化
		return sl;
	}
private:
	singleton() {};
	singleton( singleton const &) = delete;
	singleton& operator=( singleton const &) = delete;
};



int main()
{/*
	Singleton::GetInstance()->_x++;
	Singleton::GetInstance()->_x++;
	Singleton::GetInstance()->_x++;
	Singleton::GetInstance()->_x++;
	std::cout << Singleton::GetInstance()->_x << std::endl;*/
	return 0;
}