//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= n; i++)
//            for (int j = 0; j <= amount; j++)
//                if (j >= coins[i - 1])
//                    dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//        return dp[amount] == 0x3f3f3f3f ? -1 : dp[amount];
//    }
//};




//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, 0);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = coins[i - 1]; j <= amount; j++)
//            {
//                if (dp[j - coins[i - 1]] != -1)
//                    dp[j] += dp[j - coins[i - 1]];
//            }
//        }
//        return dp[amount];
//    }
//};




//class Solution {
//public:
//    int numSquares(int n) {
//        int x = sqrt(n);
//        vector<int> dp(n + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= x; i++)
//        {
//            for (int j = i * i; j <= n; j++)
//            {
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//            }
//        }
//        return dp[n] >= 0x3f3f3f3f ? 0 : dp[n];
//    }
//};



class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n) {
        int len = strs.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
        for (int i = 1; i <= len; i++)
        {
            int a = 0, b = 0;
            for (auto ch : strs[i - 1])
                if (ch == '0') a++;
                else b++;
            for (int j = m; j >= a; j--)
                for (int k = n; k >= b; k--)
                {
                    dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
                }
        }
        return dp[m][n];
    }
};