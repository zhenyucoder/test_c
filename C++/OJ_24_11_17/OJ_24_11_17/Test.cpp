//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool grid[3][3][10];
//    void solveSudoku(vector<vector<char>>& board) {
//        //保存初始数独信息
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//        dfs(board);
//    }
//
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int k = 1; k <= 9; k++)
//                    {
//                        if (!row[i][k] && !col[j][k] && !grid[i / 3][j / 3][k])
//                        {
//                            row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = true;
//                            board[i][j] = '0' + k;
//                            if (dfs(board) == false)
//                            {
//                                row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = false;
//                                board[i][j] = '.';
//                                continue;
//                            }
//                            break;
//                        }
//
//                    }
//                    if (board[i][j] == '.')
//                        return false;
//                }
//            }
//        }
//        return true;
//    }
//};


class Solution {
public:
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    bool vis[60][60];
    int m, n, target;
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
        m = image.size(), n = image[0].size(), target = image[sr][sc];
        if (target == color) return image;
        dfs(image, sr, sc, color);
        return image;
    }

    void dfs(vector<vector<int>>& image, int sr, int sc, int color)
    {
        if (vis[sr][sc]) return;
        image[sr][sc] = color, vis[sr][sc] = true;
        for (int i = 0; i < 4; i++)
        {
            int x = sr + dx[i], y = sc + dy[i];
            if (x >= 0 && x < m && y >= 0 && y < n && vis[x][y] == false && image[x][y] == target)
                dfs(image, x, y, color);
        }
    }
};