//class Solution {
//public:
//    vector<int> tmp;
//    vector<int> sortArray(vector<int>& nums) {
//        tmp.resize(nums.size());
//        mergesort(nums, 0, nums.size() - 1);
//        return nums;
//    }
//
//    void mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right)   return;
//        int mid = (left + right) >> 1;//left+right不会发生溢出
//        //排序两个子数组
//        //[left, mid] [mid + 1， right]
//        mergesort(nums, left, mid);
//        mergesort(nums, mid + 1, right);
//
//        //合并两个区间
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            tmp[i++] = nums[cur1] >= nums[cur2] ? nums[cur2++] : nums[cur1++];
//        }
//
//        //处理两个区间中剩余未排序元素
//        while (cur1 <= mid)
//            tmp[i++] = nums[cur1++];
//        while (cur2 <= right)
//            tmp[i++] = nums[cur2++];
//
//        //将排序好的数据拷贝回原数组
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i];
//    }
//};



//class Solution {
//public:
//    int tmp[50001];
//    int reversePairs(vector<int>& record) {
//        return mergesort(record, 0, record.size() - 1);
//    }
//
//    int mergesort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//        int ret = 0;
//        int mid = (left + right) >> 1;
//        //分别统计[left, mid][mid+1, right]各自区间的逆序对
//        ret += mergesort(record, left, mid);
//        ret += mergesort(record, mid + 1, right);
//        //统计[left, mid][mid+1, right]区间各取一数构成的逆序对
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] <= record[cur2])
//            {
//                tmp[i++] = record[cur1++];
//            }
//            else
//            {
//                ret += mid - cur1 + 1;
//                tmp[i++] = record[cur2++];
//            }
//        }
//
//        //将[left, mid][mid+1, right]中剩余元素排序
//        while (cur1 <= mid)
//            tmp[i++] = record[cur1++];
//        while (cur2 <= right)
//            tmp[i++] = record[cur2++];
//        //将tmp中排好的数据拷回record
//        for (i = left; i <= right; i++)
//            record[i] = tmp[i];
//        return ret;
//    }
//};



//class Solution {
//public:
//    int tmp[50001];
//    int reversePairs(vector<int>& record) {
//        return mergesort(record, 0, record.size() - 1);
//    }
//
//    int mergesort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//        int ret = 0;
//        int mid = (left + right) >> 1;
//        //分别统计[left, mid][mid+1, right]各自区间的逆序对
//        ret += mergesort(record, left, mid);
//        ret += mergesort(record, mid + 1, right);
//        //统计[left, mid][mid+1, right]区间各取一数构成的逆序对
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] > record[cur2])
//            {
//                ret += right - cur2 + 1;
//                tmp[i++] = record[cur1++];
//            }
//            else
//            {
//                tmp[i++] = record[cur2++];
//            }
//        }
//
//        //将[left, mid][mid+1, right]中剩余元素排序
//        while (cur1 <= mid)
//            tmp[i++] = record[cur1++];
//        while (cur2 <= right)
//            tmp[i++] = record[cur2++];
//        //将tmp中排好的数据拷回record
//        for (i = left; i <= right; i++)
//            record[i] = tmp[i];
//        return ret;
//    }
//};


class Solution
{
	vector<int> ret;
	vector<int> index; //nums中当前元素的原始下标
	int tmpNums[500010];
	int tmpIndex[500010];
public:
	vector<int> countSmaller(vector<int>& nums)
	{
		int n = nums.size();
		ret.resize(n);
		index.resize(n);
		//初始化index 数组
		for (int i = 0; i < n; i++)
			index[i] = i;
		mergeSort(nums, 0, n - 1);
		return ret;
	}

	void mergeSort(vector<int>& nums, int left, int right)
	{
		if (left >= right) return;
		int mid = (left + right) >> 1;
		mergeSort(nums, left, mid);
		mergeSort(nums, mid + 1, right);
		int cur1 = left, cur2 = mid + 1, i = 0;
		while (cur1 <= mid && cur2 <= right) 
		{
			if (nums[cur1] <= nums[cur2])
			{
				tmpNums[i] = nums[cur2];
				tmpIndex[i++] = index[cur2++];
			}
			else
			{
				ret[index[cur1]] += right - cur2 + 1; 
				tmpNums[i] = nums[cur1];
				tmpIndex[i++] = index[cur1++];
			}
		}
		//处理剩下的排序过程
		while (cur1 <= mid)
		{
			tmpNums[i] = nums[cur1];
			tmpIndex[i++] = index[cur1++];
		}
		while (cur2 <= right)
		{
			tmpNums[i] = nums[cur2];
			tmpIndex[i++] = index[cur2++];
		}
		for (int j = left; j <= right; j++)
		{
			nums[j] = tmpNums[j - left];index[j] = tmpIndex[j - left];
		}
	}
};