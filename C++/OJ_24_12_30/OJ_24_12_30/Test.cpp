//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        unordered_map<string, vector<string>> hash;
//        for (auto str : strs)
//        {
//            string key = str;
//            sort(key.begin(), key.end());
//            hash[key].push_back(str);
//        }
//
//        vector<vector<string>> ret;
//        for (auto& [a, b] : hash)
//            ret.push_back(b);
//        return ret;
//    }
//};




class Solution {
public:
    string printBin(double num) 
    {
        string res = "0.";
        while (res.size() <= 32 && num != 0) 
        {
            num *= 2;
            int digit = num;
            res.push_back(digit + '0');
            num -= digit;
        }
        return res.size() <= 32 ? res : "ERROR";
    }
};
