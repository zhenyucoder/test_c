//class Solution {
//public:
//    ListNode* reverseBetween(ListNode* head, int left, int right) {
//        if (right <= 1) return head;
//        ListNode* newhead = new ListNode, * prev = newhead;
//        newhead->next = head;
//        for (int i = 0; i < left - 1; i++)
//        {
//            prev = prev->next;
//        }
//
//        ListNode* cur = prev->next, * tail = cur;
//
//        for (int i = left; i <= right && cur; i++)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev->next;
//            prev->next = cur;
//            cur = next;
//        }
//
//        tail->next = cur;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};





//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        int tmp = 0;
//        ListNode* newhead = new ListNode, * prev = newhead;
//        while (l1 || l2 || tmp)
//        {
//            if (l1)
//            {
//                tmp += l1->val;
//                l1 = l1->next;
//            }
//
//            if (l2)
//            {
//                tmp += l2->val;
//                l2 = l2->next;
//            }
//
//            ListNode* node = new ListNode(tmp % 10);
//            prev->next = node;
//            prev = prev->next;
//            tmp /= 10;
//        }
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};


class Solution {
public:
    string printBin(double num) {
        string ret = "0.";
        while (num != 0 && ret.size() <= 32)
        {
            num *= 2;
            int bit = num;
            ret.push_back(num + '0');
            num -= bit;
        }
        return ret.size() > 32 ? "ERROR" : ret;
    }
};