#include <iostream>
#include <string>
using namespace std;


//class CopyBan
//{
//public:
//	CopyBan()
//	{}
//	~CopyBan()
//	{}
//public:
//	CopyBan(const CopyBan&) = delete;
//	CopyBan& operator=(const CopyBan&) = delete;
//};


// 析构函数私有化 -- 没法很好的释放资源
// 构造函数私有化
//class HeapOnly
//{
//public:
//
//private:
//	~HeapOnly()
//	{ 
//		cout << "~HeapOnly()" << endl;
//	}
//};
//
//int main()
//{
//	//HeapOnly hp1;
//	//static HeapOnly hp2;
//	HeapOnly* ptr = new HeapOnly;
//	return 0;
}

//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObj()
//	{
//		return new HeapOnly;
//	}
//	HeapOnly(const HeapOnly&) = delete;
//
//	~HeapOnly()
//	{
//		cout << "~HeapOnly()" << endl;
//	}
//private:
//	HeapOnly()
//	{
//		cout << "HeapOnly()" << endl;
//	}
//};
//
//int main()
//{
//	//HeapOnly hp1;
//	//static HeapOnly hp2;
//	//HeapOnly* ptr = new HeapOnly; // 也不行了！！
//	HeapOnly* ptr = HeapOnly::CreateObj();
//	delete ptr;
//	//HeapOnly p(*ptr);
//	return 0;
//}



// 不能将拷贝禁掉，复制CreateObj创建对象需要传值返回
//class StackOnly
//{
//public:
//	void* operator new(size_t size) = delete;
//	void operator delete(void* ptr) = delete;
//	static StackOnly CreateObj()
//	{
//		return StackOnly();
//	}
//
//private:
//	StackOnly()
//	{
//		cout << "StackOnly()" << endl;
//	}
//};
//
//int main()
//{
//	StackOnly s = StackOnly::CreateObj(); 
//	//StackOnly* ptr = new StackOnly(s);
//	return 0;
//}


//class NonInherit
//{
//public:
//	static NonInherit GetInstance()
//	{
//		return NonInherit();
//	}
//private:
//	NonInherit()
//	{}
//};
//
//class NonInherit final;
//{
//};



