#pragma once 
#include <iostream>
#include <utility> 
using namespace std;

template <class K, size_t M>
struct BTreeNode
{
	K _keys[M];
	BTreeNode<K, M>* _pSub[M + 1];
	BTreeNode<K, M>* _pParent;
	size_t _n; // 节点有效数据个数

	BTreeNode()
		:_n(0), _pParent(nullptr)
	{
		for (size_t i = 0; i < M; i++)
		{
			_keys[i] = K();
			_pSub[i] = nullptr;
		}
		_pSub[M] = nullptr;
	}
};


template <class K, size_t M>
class BTree
{
	typedef BTreeNode<K, M> Node;
private:
	void InsertKey(Node* node, K& key, Node* subs)
	{
		int end = node->_n - 1;
		for (; end >= 0; end--)
		{
			if (node->_keys[end] > key)
			{
				node->_keys[end + 1] = node->_keys[end];
				node->_pSub[end + 2] = node->_pSub[end + 1];
			}
			else
				break;
		}
		node->_keys[end + 1] = key;
		node->_pSub[end + 2] = subs;
		if (subs)
			subs->_pParent = node;
		node->_n++;
	}
public:
	pair<Node*, int> FindKey(const K& key)
	{
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			// 在当前节点中查找
			size_t i = 0;
			while(i < cur->_n)
			{
				if (cur->_keys[i] > key)
					break;
				else if (cur->_keys[i] < key)
					i++;
				else
					return make_pair(cur, i);
			}

			// 下一层节点查找
			parent = cur;
			cur = cur->_pSub[i];
		}
		return make_pair(parent, -1); // 没有查找到， 返回数据插入节点
	}

	bool Insert(const K& key)
	{
		if (_root == nullptr)
		{
			_root = new Node;
			_root->_keys[0] = key;
			_root->_n++;
			return  true;
		}

		
		pair<Node*, int> ret = FindKey(key);
		if (ret.second >= 0)
			return false;

		// 循环每次往cur插入 newkey和child
		K newkey = key;
		Node* child = nullptr;
		Node* parent = ret.first;
		while (true)
		{
			InsertKey(parent, newkey, child);
			if (parent->_n < M)
			{
				break;
			}
			else
			{ // 节点已满，开始分裂

				// 1. 将 [mid + 1, M - 1]相关数据拷贝给兄弟
				Node* brother = new Node;
				size_t mid = M / 2;
				int j = 0;
				size_t i = mid + 1;
				for (; i <= M - 1; i++)
				{
					brother->_keys[j] = parent->_keys[i];
					brother->_pSub[j] = parent->_pSub[i];
					if (parent->_pSub[i])
						parent->_pSub[i]->_pParent = brother;
					j++;

					//将数据拷贝完后清0，方便调试
					parent->_pSub[i] = nullptr;
					parent->_keys[i] = K();
				}
				brother->_pSub[j] = parent->_pSub[i];
				if (parent->_pSub[i])
					parent->_pSub[i]->_pParent = brother;
				parent->_pSub[i] = nullptr; // 清理数据

				// 2. 更新节个数 清理数据
				brother->_n = j;
				parent->_n -= (j + 1);
				K midkey = parent->_keys[mid];
				parent->_keys[mid] = K();

				// 3. 将brother合并到parent->parent，并检查是否需要进一步分裂
				if (parent->_pParent == nullptr) // 根节点
				{
					_root = new Node;
					_root->_n = 1;
					_root->_keys[0] = midkey;
					_root->_pSub[0] = parent;
					_root->_pSub[1] = brother;
					parent->_pParent = _root;
					brother->_pParent = _root;
					break;
				}
				else
				{
					newkey = midkey;
					child = brother;
					parent = parent->_pParent;
				}
			}
		}
		return true;
	}

	void InOrder()
	{
		_InOrder(_root);
	}
private:
	void _InOrder(Node* _root)
	{
		if (_root == nullptr)
			return;
		size_t i = 0;
		for (; i < _root->_n; i++)
		{
			_InOrder(_root->_pSub[i]);
			std::cout << _root->_keys[i] << " ";
		}
		_InOrder(_root->_pSub[i]);
	}
private:
	Node* _root = nullptr;
};