#include "BTree.hpp"

void TestBTree()
{
	BTree<int, 3> bt;
	int a[] = { 53, 139, 75, 49, 145, 36, 101 };
	for (auto e : a)
	{
		if (e == 101)
		{
			int a = 1;
		}
		bt.Insert(e);
	}
	bt.Inorder();
}

int main()
{
	TestBTree();
	return 0;
}