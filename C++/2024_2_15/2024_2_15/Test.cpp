//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int m = dungeon.size(), n = dungeon[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        //初始化 
//        dp[m][n - 1] = dp[m - 1][n] = 1;
//        for (int i = m - 1; i >= 0; i--)
//            for (int j = n - 1; j >= 0; j--)
//            {
//                dp[i][j] = min(dp[i][j + 1], dp[i + 1][j]) - dungeon[i][j];
//                dp[i][j] = max(dp[i][j], 1);
//            }
//        return dp[0][0];
//    }
//};



//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        if (nums.size() == 0)
//            return 0;
//        int n = nums.size();
//        vector<int>  f(n);
//        auto g = f;
//        //f:从开始到下标i的最大预约集合，包含nums[i]
//        //g:从开始到下标i的最大预约集合，不包含nums[i]
//
//        //初始化
//        f[0] = nums[0], g[0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};



//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n);//记录到某一位置时的最高金额，并且包含nums[i]
//        auto g = f;//记录到某一位置时的最高金额，并且不包含nums[i]
//
//        //初始化
//        f[0] = nums[0], g[0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};


//class Solution {
//public:
//    int _rob(vector<int>& v, int begin, int end)
//    {
//        if (begin > v.size() || begin > end) return 0;
//        vector<int> f(v.size());//记录从begin到某一位置时的最高金额，并且包含nums[i]
//        auto g = f;///记录从begin到某一位置时的最高金额，并且不包含nums[i]
//        //初始化
//        f[begin] = v[begin], g[begin] = 0;
//        //填表
//        for (int i = begin + 1; i <= end; i++)
//        {
//            f[i] = g[i - 1] + v[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[end], g[end]);
//    }
//    int rob(vector<int>& nums) {
//        //分类讨论是否偷首房子，环形问题转化为两个线性打家劫舍问题
//        int n = nums.size();
//        return max(nums[0] + _rob(nums, 2, n - 2), _rob(nums, 1, n - 1));
//    }
//};




//class Solution {
//public:
//    int deleteAndEarn(vector<int>& nums) {
//        const int N = 10001;
//        int arr[N] = { 0 };
//        for (auto x : nums)
//            arr[x] += x;
//
//        vector<int> f(N);
//        auto g = f;
//        for (int i = 1; i < N; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[N - 1], g[N - 1]);
//    }
//};




//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};


//
//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        //dp[i][0]:第i天处于“买入”状态的最大利润， 
//        //dp[i][1]:第i天处于“可交易”状态的最大利润， dp[i][2]:第i天处于“冷冻期”状态的最大利润
//        vector<vector<int>> dp(n, vector<int>(3));//创建dp表
//        //初始化
//        dp[0][0] = -prices[0];
//        //填表
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        cout << dp[n - 1][1] << ":" << dp[n - 1][2] << endl;
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};



class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        int n = prices.size();
        vector<int> f(n);
        auto g = f;
        //初始化
        f[0] = -prices[0], g[0] = 0;
        for (int i = 1; i < n; i++)
        {
            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
        }
        return g[n - 1];
    }
};