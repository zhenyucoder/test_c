#include <iostream>
using namespace std;


//class A
//{
//public:
//	void func()
//	{}
//protected:
//	int _a;
//};
//
//class B : public A
//{
//public:
//	void f()
//	{
//		func();
//		_a++;
//	}
//protected:
//	int _b;
//};
//
//class Person
//{
//public:
//	virtual A* BuyTicket()
//	{
//		cout << "买票-全价" << endl;
//		return nullptr;
//	}
//
//	virtual ~Person()
//	{
//		cout << "~Person" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual B* BuyTicket()
//	{
//		cout << "买票-半价" << endl;
//		return nullptr;
//	}
//
//	virtual ~Student()
//	{
//		cout << "~Student" << endl;
//	}
//};
//
////void func(Person& p)
////{
////	p.BuyTicket();
////}
////
////int main()
////{
////	Person p;
////	Student s;
////	func(p);
////	func(s);
////	return 0;
////}
//
//int main()
//{
//	Person* p = new Person;
//	delete p;
//	p = new Student;
//	delete p;
//	return 0;
//}


//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	void func(int val = 0) override { std::cout << "B->" << val << std::endl; }
//};
//
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	p->func();//普通调用
//
//	return 0;
//}



//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};


//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//
//class Benz :public Car
//{
//public:
//	// 纯虚函数
//	// 1、间接强制去派生类去重写
//	// 2、抽象类-不能实例化出对象
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//
//class BMW :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "BMW-操控" << endl;
//	}
//};
//
//int main()
//{
//	//Car c;
//	Benz bz;
//	BMW bm;
//	return 0;
//}


//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Func2()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Func3()" << endl;
//	}
//private:
//	int _b = 1;
//	int _a = 0;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//	virtual void Func4()
//	{
//		cout << "Func4()" << endl;
//	}
//private:
//	int _c;
//};
//
//int main()
//{
//	Base b;
//	Derive d;
//	cout << sizeof(b) << endl;
//	cout << sizeof(d) << endl;
//
//	return 0;
//}


class Person {
public:
	 void BuyTicket() { cout << "买票-全价" << endl; }
};

class Student : public Person {
public:
	virtual void BuyTicket() { cout << "买票-半价" << endl; }
};

void Func(Person* p)
{
	p->BuyTicket();
}

int main()
{
	//Person p;
	//Func(p);
	Func(new Student);
	return 0;
}