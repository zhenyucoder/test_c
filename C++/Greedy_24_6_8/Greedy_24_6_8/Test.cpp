//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        int fiveSize = 0, tenSize = 0;
//        for (auto bill : bills)
//        {
//            if (bill == 5)
//                fiveSize++;
//            else if (bill == 10)
//            {
//                if (fiveSize > 0)
//                    --fiveSize, tenSize++;
//                else
//                    return false;
//            }
//            else
//            {
//                if (fiveSize > 0 && tenSize > 0)
//                    fiveSize--, tenSize--;
//                else if (fiveSize >= 3)
//                    fiveSize -= 3;
//                else
//                    return false;
//            }
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        double sum = 0.0;
//        priority_queue<double> pq;
//        for (auto num : nums)
//        {
//            pq.push(num);
//            sum += num;
//        }
//        sum /= 2.0;
//
//        double target = 0.0;
//        int count = 0;
//        while (target < sum)
//        {
//            count++;
//            double top = pq.top(); pq.pop();
//            top /= 2.0;
//            target += top;
//            pq.push(top);
//        }
//        return count;
//    }
//};



//class Solution {
//public:
//    string largestNumber(vector<int>& nums) {
//        vector<string> strs;
//        for (auto num : nums)
//            strs.push_back(to_string(num));
//        sort(strs.begin(), strs.end(), [](const string& s1, const string& s2) {return s1 + s2 > s2 + s1; });
//        string ret;
//        for (int i = 0; i < strs.size(); i++)
//        {
//            ret += strs[i];
//        }
//        return ret[0] == '0' ? "0" : ret;
//    }
//};





//class Solution {
//public:
//    int left = 0;//�����������
//    int wiggleMaxLength(vector<int>& nums) {
//        int len = 0, n = nums.size();
//        if (n < 2) return n;
//        for (int i = 0; i < n - 1; i++)
//        {
//            int right = nums[i + 1] - nums[i];
//            if (right == 0)
//                continue;
//            if (right * left <= 0)
//                len++;
//            left = right;
//        }
//        return len + 1;
//    }
//};




class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int n = nums.size();
        vector<int> memo(n);
        int ret = 0;
        for (int i = 0; i < n; i++)
            ret = max(ret, dfs(nums, i, memo));
        return ret;
    }

    int dfs(vector<int>& nums, int pos, vector<int>& memo)
    {
        if (memo[pos] != 0)
            return memo[pos];

        int ret = 1;
        for (int i = pos + 1; i < nums.size(); i++)
        {
            if (nums[i] > nums[pos])
                ret = max(ret, dfs(nums, i, memo) + 1);
        }
        memo[pos] = ret;
        return memo[pos];
    }
};