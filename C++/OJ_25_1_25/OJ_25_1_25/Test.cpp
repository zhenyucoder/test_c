//class Solution {
//public:
//    int hash1[256] = { 0 }, hash2[256] = { 0 };
//    string minWindow(string s, string t) {
//        for (auto ch : t)
//            hash2[ch]++;
//        int n = t.size(), begin = -1, len = INT_MAX;
//        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//        {
//            char in = s[right];
//            if (++hash1[in] <= hash2[in])
//                count++;
//            while (count >= n)
//            {
//                if (right - left + 1 < len)
//                    begin = left, len = right - left + 1;
//                char out = s[left++];
//                --hash1[out];
//                if (hash2[out] && hash1[out] < hash2[out])
//                    count--;
//            }
//        }
//        return (len == INT_MAX) ? "" : s.substr(begin, len);
//    }
//};



class Solution {
public:
    bool isletter(char ch)
    {
        if (ch >= 'a' && ch <= 'z') return true;
        if (ch >= 'A' && ch <= 'Z')  return true;
        return false;
    }
    string reverseOnlyLetters(string s) {
        for (int left = 0, right = s.size() - 1; left < right; left++, right--)
        {
            while (left < right && !isletter(s[left]))
                left++;
            while (left < right && !isletter(s[right]))
                right--;
            swap(s[left], s[right]);
        }
        return s;
    }
};