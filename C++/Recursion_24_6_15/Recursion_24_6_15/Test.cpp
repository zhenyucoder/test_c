//class Solution {
//public:
//    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
//        if (A.size() == 0) return;
//        _hanota(A, B, C, A.size());
//        return;
//    }
//
//    void _hanota(vector<int>& A, vector<int>& B, vector<int>& C, int num)
//    {
//        if (num == 1)
//        {
//            C.push_back(A.back());
//            A.pop_back();
//            return;
//        }
//
//        _hanota(A, C, B, num - 1);
//        C.push_back(A.back());
//        A.pop_back();
//        _hanota(B, A, C, num - 1);
//        return;
//    }
//};





//class Solution {
//public:
//    ListNode* newhead = new ListNode(), * prev = newhead;
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr)
//        {
//            prev->next = list2;
//            prev = newhead->next; delete newhead;
//            return prev;
//        }
//        if (list2 == nullptr)
//        {
//            prev->next = list1;
//            prev = newhead->next; delete newhead;
//            return prev;
//        }
//
//        if (list1->val < list2->val)
//        {
//            prev->next = list1, prev = prev->next;
//            list1 = list1->next;
//            return mergeTwoLists(list1, list2);
//        }
//        else
//        {
//            prev->next = list2, prev = prev->next;
//            list2 = list2->next;
//            return mergeTwoLists(list1, list2);
//        }
//    }
//};




//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr || list2 == nullptr)
//            return list1 == nullptr ? list2 : list1;
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};




//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr || list2 == nullptr)
//            return list1 == nullptr ? list2 : list1;
//        ListNode* newnead = new ListNode(), * prev = newnead;
//        while (list1 && list2)
//        {
//            if (list1->val < list2->val)
//                prev->next = list1, prev = prev->next, list1 = list1->next;
//            else
//                prev->next = list2, prev = prev->next, list2 = list2->next;
//        }
//        prev->next = list1 == nullptr ? list2 : list1;
//        //����
//        prev = newnead->next;
//        delete newnead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newhead = new ListNode, * cur = head;
//        while (cur)
//        {
//            ListNode* tmp = cur;
//            cur = cur->next;
//            //ͷ��
//            tmp->next = newhead->next;
//            newhead->next = tmp;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};



class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        return dfs(head);
    }

    ListNode* dfs(ListNode* head)
    {
        if (head == nullptr || head->next == nullptr)
            return head;
        ListNode* ret = dfs(head->next);
        head->next->next = head;
        head->next = nullptr;
        return ret;
    }

};