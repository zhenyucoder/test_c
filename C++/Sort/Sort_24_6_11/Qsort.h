#pragma once
#include <iostream>
#include <stack>
#include <vector>
using namespace std;


//template <class T>
//void BubbleSort(vector<T>& arr)
//{
//	int n = arr.size();
//	for (int i = 0; i < n - 1; i++)
//	{
//		int flag = true;//标记是否已经有序
//		for (int j = 0; j < n - 1 - i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				swap(arr[j], arr[j + 1]);
//				flag = false;
//			}
//		}
//		if (flag) break;
//	}
//}


//---------------------------------------------------
// 快排3个版本， 递归
// 
//template <class T>
//int middle(vector<T>& arr, int left, int right)//用于优化
//{
//	int mid = left + (right - left) / 2;
//	if (arr[left] < arr[mid])
//	{	
//		if (arr[mid] < arr[right])
//			return mid;
//		else
//			return arr[left] > arr[right] ? left : right;
//	}
//	else
//	{
//		if (arr[mid] > arr[right])
//			return mid;
//		else
//			return arr[left] < arr[right] ? left : right;
//	}
//}

//template <class T>
//int PartSort(vector<T>& arr, int left, int right);
//
////快排
//template <class T>
//void QuickSort(vector<T>& arr, int begin, int end)//[begin, end]
//{
//	if (begin >= end) return;
//	// [begin, keyi] <= arr[keyi] << [keyi + 1, end]
//	int keyi = PartSort(arr, begin, end);
//	QuickSort(arr, begin, keyi - 1);
//	QuickSort(arr, keyi + 1, end);
//}


// 快速排序hoare版本
//template <class T>
//int PartSort(vector<T>& arr, int left, int right)//[left, right]
//{
//	int mid = middle(arr, left, right);
//	swap(arr[left], arr[mid]);
//	int keyi = left;//基准值下标
//	while (left < right)
//	{
//		while (right > left && arr[right] >= arr[keyi])
//			right--;
//		while (left < right && arr[left] <= arr[keyi])
//			left++;
//		swap(arr[left], arr[right]);
//	}
//	swap(arr[left], arr[keyi]);
//	return left;
//}



// 快速排序挖坑法
//template <class T>
//int PartSort(vector<T> &arr, int left, int right)
//{
//	int mid = middle(arr, left, right);
//	swap(arr[left], arr[mid]);
//	int key = arr[left], hole = left;
//	while (left < right)
//	{
//		while (left < right && arr[right] >= key)
//			right--;
//		arr[hole] = arr[right], hole = right;
//		while (left < right && arr[left] <= key)
//			left++;
//		arr[hole] = arr[left], hole = left;
//	}
//	arr[hole] = key;
//	return hole;
//}

// 快速排序前后指针法
//template <class t>
//int partsort(vector<t>& arr, int left, int right)
//{
//	int mid = middle(arr, left, right);
//	swap(arr[left], arr[mid]);
//	int keyi = left, prev = left, cur = left + 1;
//	while (cur <= right)
//	{
//		if (arr[cur] < arr[keyi] && ++prev != cur)
//			swap(arr[prev], arr[cur]);
//		cur++;
//	}
//	swap(arr[keyi], arr[prev]);
//	return prev;
//}


//---------------------------------------------------
// 快排 非递归

//template <class T>
//void QuickSortNonR(vector<T>& arr, int left, int right)
//{
//	stack<T> st;
//	st.push(left), st.push(right);
//	while (!st.empty())
//	{
//		int right = st.top(); st.pop();
//		int left = st.top(); st.pop();
//		int keyi = partsort(arr, left, right);
//		// [left, keyi - 1] keyi [keyi + 1, right]
//		if (left < keyi - 1)
//		{
//			st.push(left); st.push(keyi - 1);
//		}
//		if (keyi + 1 < right)
//		{
//			st.push(keyi + 1); st.push(right);
//		}
//	}
//}
//
//int main()
//{
//	vector<int> v{ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
//	QuickSortNonR(v, 0, v.size() - 1);
//	for (auto x : v)
//		cout << x << " ";
//	cout << endl;
//	return 0;
//}


//----------------------------------------------
// 快排优化为0(N) , oj

//class Solution {
//public:
//    int findKthLargest(vector<int>& nums, int k) {
//        srand(time(NULL));
//        return topsort(nums, 0, nums.size() - 1, k);
//    }
//
//    int topsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l == r)
//            return nums[l];
//        int x = getRandom(nums, l, r);
//        int left = l - 1, right = r + 1, i = l;
//        while (i < right)
//        {
//            if (nums[i] < x)
//                swap(nums[++left], nums[i++]);
//            else if (nums[i] == x)
//                i++;
//            else
//                swap(nums[--right], nums[i]);
//        }
//        //判断更新区间
//        if ((r - right + 1) >= k)
//            return topsort(nums, right, r, k);
//        else if (r - left >= k)
//            return x;
//        else
//            return topsort(nums, l, left, k - (r - left));
//    }
//
//    //获取[l, r]区间中的随机值
//    int getRandom(vector<int>& nums, int l, int r)
//    {
//        return nums[rand() % (r - l + 1) + l];
//    }
//};




class Solution {
public:
    vector<int> smallestK(vector<int>& arr, int k) {
        srand(time(NULL));
        qsort(arr, 0, arr.size() - 1, k);
        return { arr.begin(), arr.begin() + k };
    }

    void qsort(vector<int>& arr, int l, int r, int k)
    {
        if (l >= r) return;
        int key = getRandom(arr, l, r);
        int left = l - 1, right = r + 1, i = l;
        while (i < right)
        {
            if (arr[i] < key)
                swap(arr[++left], arr[i++]);
            else if (arr[i] == key)
                i++;
            else
                swap(arr[--right], arr[i]);
        }

        //分类讨论
        int a = left - l + 1, b = right - left - 1;
        if (a > k)
            qsort(arr, l, left, k);
        else if (k <= a + b)
            return;
        else
            qsort(arr, right, r, k - a - b);

    }

    int getRandom(vector<int>& arr, int l, int r)
    {
        return arr[rand() % (r - l + 1) + l];
    }
};