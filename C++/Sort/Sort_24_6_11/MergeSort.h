#pragma once
#include <iostream>
#include <vector>
#include <string>
using namespace std;

//-------------------------------------------------
// 递归版本
//void InsertSort(vector<int>& a, int begin, int end)//排升序
//{
//	for (int i = begin; i < end; i++)
//	{
//		int end = i;
//		//tmp记录待插入元素，因为插入数据时需要挪动数据，会被覆盖
//		int tmp = a[end + 1];
//		//[0,end]有序，将tmp插入到合适位置
//		while (end >= 0)
//		{
//			if (a[end] > tmp)
//			{
//				a[end + 1] = a[end];
//				end--;
//			}
//			else
//			{
//				break;
//			}
//		}
//		a[end + 1] = tmp;
//	}
//}

void _MergeSort(vector<int>& arr, int begin, int end, vector<int>& tmp)
{
	if (begin >= end) return;

	//优化 减少递归次数
	//if (end - begin + 1 < 10)
	//{
	//	int* arraybegin = &arr[begin];
	//	InsertSort(arr, begin, end);
	//	return;
	//}

	int mid = begin + (end - begin) / 2;
	_MergeSort(arr, begin, mid, tmp);
	_MergeSort(arr, mid + 1, end, tmp);

	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int i = begin;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
			tmp[i++] = arr[begin1++];
		else
			tmp[i++] = arr[begin2++];
	}

	while (begin1 <= end1)
		tmp[i++] = arr[begin1++];
	while (begin2 <= end2)
		tmp[i++] = arr[begin2++];
	for (int i = begin; i <= end; i++)
		arr[i] = tmp[i];
}


void MergeSort(vector<int>& arr, int n)
{
	vector<int> tmp(n);
	_MergeSort(arr, 0, n - 1, tmp);
}

//------------------------------------------------------
//非递归

void MergeSortNonR(vector<int>& arr, int n)
{
	vector<int> tmp(arr.begin(), arr.end());
	int gap = 1;
	while (gap < n)
	{
		int j = 0;
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//修正待排序区间
			if (end1 > n - 1)
			{
				end1 = n - 1;
				begin2 = n, end2 = n - 1;//无效区间
			}
			else if (begin2 > n - 1)
			{
				begin2 = n, end2 = n - 1;//无效区间
			}
			else if (end2 > n - 1)
			{
				end2 = n - 1;
			}

			//开始排序
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2])
					tmp[j++] = arr[begin1++];
				else
					tmp[j++] = arr[begin2++];
			}

			while (begin1 <= end1)
				tmp[j++] = arr[begin1++];
			while (begin2 <= end2)
				tmp[j++] = arr[begin2++];

		}

		arr = tmp;
		gap *= 2;
	}
}