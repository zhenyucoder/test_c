#pragma once
#include <iostream>
#include <vector>
using namespace std;


//---------------------------------------------
// ѡ������
void SelectSort(vector<int>& arr, int n)
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int maxPos = begin, minPos = begin;
		for (int i = begin; i <= end; i++)
		{
			if (arr[i] > arr[maxPos]) maxPos = i;
			if (arr[i] < arr[minPos]) minPos = i;
		}
		swap(arr[begin], arr[minPos]);
		if (begin == maxPos)
		{
			maxPos = minPos;
		}
		swap(arr[end], arr[maxPos]);
		begin++, end--;
	}
}


//--------------------------------------------
// ������
void AdjustDown(vector<int>& arr, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && arr[child] < arr[child + 1])
			child++;
		if (arr[child] > arr[parent])
		{
			swap(arr[child], arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(vector<int>& arr, int n)
{
	// ���� - �����
	for (int i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(arr, n, i);
	}

	int end = n - 1;
	while (end > 0)
	{
		swap(arr[0], arr[end]);
		AdjustDown(arr, end, 0);
		--end;
	}
}
