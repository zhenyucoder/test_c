#pragma once
#include <iostream>
#include <vector>
using namespace std;



void CountSort(vector<int>& arr, int n)
{
	//查找最大数和最小数
	int minNum = arr[0], maxNum = arr[0];
	for (auto x : arr)
		if (x > maxNum) maxNum = x;
		else if (x < minNum) minNum = x;

	//统计出现次数
	vector<int> Count(maxNum - minNum + 1);
	for (auto x : arr)
		Count[x - minNum]++;

	//将tmp中的数据有序填回arr
	int j = 0;
	for (int i = 0; i < Count.size(); i++)
	{
		while (Count[i]--)
			arr[j++] = minNum + i;
	}
}