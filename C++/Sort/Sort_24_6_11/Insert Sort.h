#pragma once
#include <iostream>
#include <vector>
using namespace std;

//-------------------------------------------
// 直接插入排序
void InsertSort(vector<int>& arr, int n)
{
	for (int i = 0; i < n - 1; i++)//固定待插入元素
	{
		int tmp = arr[i + 1];//待插入元素
		int end = i;
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
				break;
		}
		arr[end + 1] = tmp;//插入到合适位置
	}
}


//------------------------------------------------
// 希尔排序
void ShellSort(vector<int>& arr, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;//+ 1保证最后一次gap为1
		for (int i = 0; i < n - gap; i++)//多组并排
		{
			int tmp = arr[i + gap];
			int end = i;
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			//查找到待查入的合适位置下标
			arr[end + gap] = tmp;
		}
	}
}