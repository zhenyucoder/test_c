//class Solution
//{
//	int memo[201][201];
//public:
//	int getMoneyAmount(int n)
//	{
//		return dfs(1, n);
//	}
//	int dfs(int left, int right)
//	{
//		if (left >= right) return 0;
//		if (memo[left][right] != 0) return memo[left][right];
//		int ret = INT_MAX;
//		for (int head = left; head <= right; head++) // ѡ��ͷ���
//		{
//			int x = dfs(left, head - 1);
//			int y = dfs(head + 1, right);
//			ret = min(ret, head + max(x, y));
//		}
//		memo[left][right] = ret;
//		return ret;
//	}
//};



class Solution
{
	int m, n;
	int dx[4] = { 0, 0, 1, -1 };
	int dy[4] = { 1, -1, 0, 0 };
	int memo[201][201];
public:
	int longestIncreasingPath(vector<vector<int>>& matrix)
	{
		int ret = 0;
		m = matrix.size(), n = matrix[0].size();
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
			{
				ret = max(ret, dfs(matrix, i, j));
			}

		return ret;
	}
	int dfs(vector<vector<int>>& matrix, int i, int j)
	{
		if (memo[i][j] != 0) return memo[i][j];
		int ret = 1;
		for (int k = 0; k < 4; k++)
		{
			int x = i + dx[k], y = j + dy[k];
			if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i]
				[j])
			{
				ret = max(ret, dfs(matrix, x, y) + 1);
			}
		}
		memo[i][j] = ret;
		return ret;
	}
};