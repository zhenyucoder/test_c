//class Solution {
//public:
//    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
//        _hanota(A, B, C, A.size());
//    }
//    void _hanota(vector<int>& A, vector<int>& B, vector<int>& C, int num)
//    {
//        if (num == 1)
//        {
//            C.push_back(A.back());
//            A.pop_back();
//            return;
//        }
//        //先将A中前num-1个圆盘借助C移到B
//        _hanota(A, C, B, num - 1);
//        //将A剩余的最后一个元素移到C柱子
//        C.push_back(A.back());
//        A.pop_back();
//        //将B上的num-1个圆盘借助A移到C柱子上
//        _hanota(B, A, C, num - 1);
//    }
//};



//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr)
//            return list2;
//        if (list2 == nullptr)
//            return list1;
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};


//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr)
//            return list2;
//        if (list2 == nullptr)
//            return list1;
//        ListNode* newhead = nullptr, * tail = nullptr;
//        ListNode* cur1 = list1, * cur2 = list2;
//        while (cur1 && cur2)
//        {
//            if (cur1->val < cur2->val)
//            {
//                if (tail == nullptr)
//                {
//                    tail = newhead = cur1;
//                }
//                else
//                {
//                    tail->next = cur1;
//                    tail = tail->next;
//                }
//                cur1 = cur1->next;
//            }
//            else
//            {
//                if (tail == nullptr)
//                {
//                    tail = newhead = cur2;
//                }
//                else
//                {
//                    tail->next = cur2;
//                    tail = tail->next;
//                }
//                cur2 = cur2->next;
//            }
//            cout << tail->val << " ";
//        }
//        if (cur1)
//            tail->next = cur1;
//        if (cur2)
//            tail->next = cur2;
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* newhead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//}

//
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        //迭代版本，本质上即使链表头插
//        ListNode* newhead = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = newhead;
//            newhead = cur;
//            cur = next;
//        }
//        return newhead;
//    }
//};


//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* newhead = head->next;
//        head->next = swapPairs(newhead->next);
//        newhead->next = head;
//        return newhead;
//    }
//};

//class Solution {
//public:
//    double myPow(double x, long long n) {
//        return n < 0 ? 1.0 / _myPow(x, -n) : _myPow(x, n);
//    }
//    double _myPow(double x, long long n)
//    {
//        if (n == 0)
//            return 1.0;
//        double ans = _myPow(x, n / 2);
//        return n % 2 == 1 ? ans * ans * x : ans * ans;
//    }
//};


//class Solution {
//public:
//    bool evaluateTree(TreeNode* root) {
//        if (root->left == nullptr || root->right == nullptr)
//            return root->val;
//        bool BLeft = evaluateTree(root->left);
//        bool BRight = evaluateTree(root->right);
//        return root->val == 2 ? BLeft | BRight : BLeft & BRight;
//    }
//};


class Solution {
public:
    int sumNumbers(TreeNode* root) {
        return _sumNumbers(root, 0);
    }

    int _sumNumbers(TreeNode* root, int prev)
    {
        int sum = 0, val = prev * 10 + root->val;
        if (root->left == nullptr && root->right == nullptr)
            return val;
        if (root->left)
            sum += _sumNumbers(root->left, val);
        if (root->right)
            sum += _sumNumbers(root->right, val);
        return sum;
    }
};