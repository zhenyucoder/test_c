//class Solution
//{
//public:
//	int dx[4] = { 0, 0, 1, -1 };
//	int dy[4] = { 1, -1, 0, 0 };
//	vector<vector<int> > flipChess(vector<vector<int> >& A, vector<vector<int>
//	>& f)
//	{
//		for (auto& v : f)
//		{
//			int a = v[0] - 1, b = v[1] - 1;
//			for (int i = 0; i < 4; i++)
//			{
//				int x = a + dx[i], y = b + dy[i];
//				if (x >= 0 && x < 4 && y >= 0 && y < 4)
//				{
//					A[x][y] ^= 1;
//				}
//			}
//		}
//		return A;
//	}
//};




#include <iostream>
using namespace std;
const int N = 2010;
int n;
int arr[N];
int bfs()
{
	int left = 1, right = 1;
	int ret = 0;
	while (left <= right)
	{
		ret++;
		int r = right;
		for (int i = left; i <= right; i++)
		{
			r = max(r, arr[i] + i);
			if (r >= n)
			{
				return ret;
			}
		}
		left = right + 1;
		right = r;
	}
	return -1;
}
int main()
{
	cin >> n;
	for (int i = 1; i <= n; i++) cin >> arr[i];

	cout << bfs() << endl;

	return 0;
}