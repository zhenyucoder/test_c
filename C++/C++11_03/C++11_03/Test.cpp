#define  _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <initializer_list>

using namespace std;

//template<class T>
//int Add(const T& x, const T& y)
//{
//	/*T tmp = x;
//	x = y;
//	y = tmp;*/
//	return x + y;
//}

//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int x = 10; int y = 20; double d = 10.3;
//	int sum = Add(x, d);
//	cout << sum<< endl;
//	//Add(x, (int)d);
//	/*sum = Add<int>(y, d);
//	cout << sum << endl;*/
//	return 0;
//}





//namespace mystring
//{
//	class string
//	{
//	public:
//		string(const char* str = "")//默认构造函数
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			cout << "string(const char* str = "") ----- 构造函数" << endl;
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//
//		//拷贝构造函数
//		string(const string& s)
//		{
//			string tmp(s._str);
//			swap(tmp);
//			cout << "string(const string& s) ---- 拷贝构造函数 深拷贝" << endl;
//		}
//
//		//移动构造函数
//		string(string&& s)
//		{
//			swap(s);
//			cout << "string(string&& s) ---- 移动构造函数  移动语义" << endl;
//		}
//
//		//拷贝赋值函数
//		string& operator=(const string& s)
//		{
//			string tmp(s);
//			swap(tmp);
//			cout << "string& operator=(const string& s) ---- 拷贝赋值函数 深拷贝" << endl;
//			return *this;
//		}
//
//		//移动赋值函数
//		string& operator=(string&& s)
//		{
//			swap(s);
//			cout << "string& operator=(string&& s) ---- 移动语义" << endl;
//			return s;
//		}
//		void swap(string& s)
//		{
//			std::swap(_str, s._str);
//			std::swap(_size, s._size);
//			std::swap(_capacity, s._capacity);
//		}
//	private:
//		char* _str = nullptr;
//		size_t _size = 0;
//		size_t _capacity = 0;
//	};
//};
//
//int main()
//{
//	mystring::string s("123456");
//	cout << endl;
//
//	mystring::string s1 = s;
//	cout << endl;
//
//	mystring::string s2(move(s1));
//
//	//mystring::string s1, s2;
//	//s1 = s;
//	//cout << endl;
//
//	//s2 = move(s);
//	//cout << endl;
//	return 0;
//}




void Fun(int& x) { cout << "左值引用" << endl; }
void Fun(const int& x) { cout << "const 左值引用" << endl; }
void Fun(int&& x) { cout << "右值引用" << endl; }
void Fun(const int&& x) { cout << "const 右值引用" << endl; }
// std::forward<T>(t)在传参的过程中保持了t的原生类型属性。
template<typename T>
void PerfectForward(T&& t)
{
	Fun(std::forward<T>(t));
}
int main()
{
	PerfectForward(10);
	int a;
	PerfectForward(a);
	PerfectForward(std::move(a)); 
	const int b = 8;
	PerfectForward(b);
	PerfectForward(std::move(b)); 
	return 0;
}



//int main()
//{
//	int x = 2;
//	double d = 4.2;
//	cout << typeid(x).name() << "     " << typeid(d).name() << endl;
//	//error typeid(x).name()获取到的信息是一个字符串
//	//typeid(x).name() ret; 
//
//	decltype(x * d) ret;
//	cout << typeid(ret).name() << endl;
//	return 0;
//}



//struct Date
//{
//	int year;
//	int monday;
//	int day;
//};
//
//int main()
//{
//	//int arr[5]{ 1, 2 ,3, 4, 5 };//省略等号
//	//int i{ 1 };//初始化列表初始化内置类型，等价于int i = 1;或int i = {1};
//	//Date d{ 2024, 4 ,14 };//省略等号
//
//
//	////C++中，初始化列表使用于new表达
//	////本质上是用花括号中的值生成一个initializer_list对象，在调用对于的构造函数
//	//int* p = new int[5]{ 1, 2, 3 };
//
//	//string s{ 'a', 'b', 'c' };
//	vector<int> v{ 1, 2, 3, 4, 5 };
//	list<int> l{ 1, 2, 3, 4, 5 };
//	map<string, string> m{
//		{"排序", "sort"},
//		{"快排", "qsort"},
//		{"水果", "fruits"}
//	};
//
//	vector<int> v1;
//	v1 = { 1, 3 ,5 ,7, 9 };
//	list<int> l1;
//	l1 = { 1 ,3 ,5, 7, 9 };
//	return 0;
//}


//namespace Myvector
//{
//	template<class T>
//	class vector
//	{
//		typedef T* iterator;
//	pubilc:
//		vector<T>(initializer_list<T> l)
//		{
//			_start = new T[l.size()];
//			_finish = _start + l.size();
//			_endofstorage = _finish;
//
//			iterator vit = _start;
//			typename initializer_list<T>::iterator lit = l.begin();
//			while (lit != l.end())
//			{
//				*vit++ = *lit++;
//			}
//		};
//
//		vector<T>& operator=(initializer_list<T> l)
//		{
//			vector<T> tmp(l);
//			std::swap(_start, tmp._start);
//			std::swap(_finish, tmp._finish);
//			std::swap(_endofstorage, tmp._endofstorage);
//			return *this;
//		}
//
//	priceate:
//		iterator _start = nullptr;
//		iterator _finish = nullptr;
//		iterator _endofstorage = nullptr;
//	};
//}


//int main()
//{
//	//a、b、p、*p都是左值
//	int* p = new int(0);
//	int a = 10;
//	const int b = 12;
//
//	//rp、ra、rb、rval都是左值引用
//	int*& rp = p;
//	int& ra = a;
//	const int& rb = b;
//	int& rval = *p;
//	return 0;
//}

//int Add(const int x, const int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	//10、10 + 20、Add函数返回值都是右值，无法取地址
//	//ra、rb、rc都是右值引用
//	int&& ra = 10;
//	int&& rb = 10 + 20;
//	int&& rc = Add(1, 2);
//	ra = 20;
//	return 0;
//}


#include <utility>


//int main()
//{
//	//int a = 10;
//
//	//int& ra = a;
//	////int& rb = 10;//error,普通左值引用不能引用右值
//	//const int& rc = a;
//	//const int& rd = 10;
//
//	int a = 10;
//	const int&& rb = 10;
//
//	//int&& ra = 1;//error, 右值引用无法引用左值
//	//error，move本身不会修改左值属性
//	//move(a);
//	//int&& ra = a;
//
//	int&& ra = move(a);//move后的返回值是一个右值
//	return 0;
//}


