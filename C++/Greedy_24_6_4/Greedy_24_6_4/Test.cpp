﻿//class Solution
//{
//public:
//	vector<int> diStringMatch(string s)
//	{
//		int left = 0, right = s.size(); // ⽤ left，right 标记最⼩值和最⼤值
//		vector<int> ret;
//		for (auto ch : s)
//		{
//			if (ch == 'I') ret.push_back(left++);
//			else ret.push_back(right--);
//		}
//		ret.push_back(left); // 把最后⼀个数放进去
//		return ret;
//	}
//};



//class Solution
//{
//public:
//	int findContentChildren(vector<int>& g, vector<int>& s)
//	{
//		sort(g.begin(), g.end());
//		sort(s.begin(), s.end());
//		int ret = 0, n = s.size();
//		for (int i = 0, j = 0; i < g.size() && j < n; i++, j++)
//		{
//			while (j < n && s[j] < g[i]) j++;
//			if (j < n) ret++;
//		}
//		return ret;
//	}
//};




//class Solution
//{
//public:
//	string optimalDivision(vector<int>& nums)
//	{
//		int n = nums.size();
//		if (n == 1)
//		{
//			return to_string(nums[0]);
//		}
//		if (n == 2)
//		{
//			return to_string(nums[0]) + "/" + to_string(nums[1]);
//		}
//		string ret = to_string(nums[0]) + "/(" + to_string(nums[1]);
//		for (int i = 2; i < n; i++)
//		{
//			ret += "/" + to_string(nums[i]);
//		}
//		ret += ")";
//		return ret;
//	}
//};




class Solution
{
public:
	int jump(vector<int>& nums)
	{
		int left = 0, right = 0, maxPos = 0, ret = 0, n = nums.size();
		while (left <= right) 
		{
			if (maxPos >= n - 1) 
			{
				return ret;
			}
			// 遍历当成层，更新下⼀层的最右端点
			for (int i = left; i <= right; i++)
			{
				maxPos = max(maxPos, nums[i] + i);
			}
			left = right + 1;
			right = maxPos;
			ret++;
		}
		return -1;
	}
};