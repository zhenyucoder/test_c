//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int sum = 0, ret = 0x3f3f3f3f;
//        for (int left = 0, right = 0, n = nums.size(); right < n; right++)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                ret = min(ret, right - left + 1);
//                sum -= nums[left++];
//            }
//        }
//        return ret == 0x3f3f3f3f ? 0 : ret;
//    }
//};



//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[128] = { 0 }, ret = 0, n = s.size();
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            char in = s[right];
//            hash[in]++;
//            while (hash[in] > 1)
//            {
//                hash[s[left]]--;
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int n = nums.size(), ret = 0;
//        for (int left = 0, right = 0, zero = 0; right < n; right++)
//        {
//            if (nums[right] == 0) zero++;
//
//            // 更新区间左边界
//            while (zero > k)
//            {
//                if (nums[left++] == 0)
//                    zero--;
//            }
//            // 更新结构
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int target = 0;
//        for (auto x : nums)
//            target += x;
//        target -= x;
//        if (target < 0) return -1;
//        // 统计和恰好为target的最长子区间
//        int left = 0, right = 0, n = nums.size(), sum = 0, maxlen = INT_MIN;
//        while (right < n)
//        {
//            sum += nums[right];
//            while (sum > target)
//            {
//                sum -= nums[left++];
//            }
//            if (target == sum)
//                maxlen = max(maxlen, right - left + 1);
//            right++;
//        }
//        return maxlen == INT_MIN ? -1 : n - maxlen;
//    }
//};




//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        unordered_map<int, int> hash;
//        int ret = 0;
//        for (int left = 0, right = 0, n = fruits.size(); right < n; right++)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)
//                    hash.erase(fruits[left]);
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};





//class Solution {
//public:
//    string minWindow(string s, string t) {
//        unordered_map<char, int> hash1, hash2;
//        for (auto ch : t)
//            hash2[ch]++;
//        int begin = -1, minlen = INT_MAX, n = s.size(), m = t.size();
//        for (int left = 0, right = 0, count = 0; right < n; right++)
//        {
//            // 进窗口
//            char in = s[right];
//            hash1[in]++;
//            if (hash2[in] && hash1[in] <= hash2[in])
//                count++;
//            while (count == m)
//            {
//                // 更新结果
//                if (right - left + 1 < minlen)
//                    begin = left, minlen = right - left + 1;
//                // 更新区间
//                char out = s[left++];
//                hash1[out]--;
//                if (hash2[out] && hash1[out] < hash2[out])
//                    count--;
//            }
//        }
//        return (begin == -1) ? "" : s.substr(begin, minlen);
//    }
//};



