//class Solution {
//public:
//    vector<string> ret;
//    string path;
//    int left = 0, right = 0;
//    vector<string> generateParenthesis(int n) {
//        dfs(n);
//        return ret;
//    }
//
//    void dfs(int n)
//    {
//        if (right == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        if (left < n)
//        {
//            path.push_back('(');
//            left++;
//            dfs(n);
//            path.pop_back();
//            left--;
//        }
//
//        if (right < left)
//        {
//            path.push_back(')');
//            right++;
//            dfs(n);
//            path.pop_back();
//            right--;
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n, k;
//    vector<vector<int>> combine(int _n, int _k) {
//        n = _n, k = _k;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int pos)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = pos; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};





//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n, k;
//    vector<vector<int>> combine(int _n, int _k) {
//        n = _n, k = _k;
//        dfs(1);
//        return ret;
//    }
//
//    void dfs(int pos)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (pos > n) return;
//        path.push_back(pos);
//        dfs(pos + 1);
//        path.pop_back();
//        dfs(pos + 1);
//    }
//};



//class Solution {
//public:
//    int aim, prevSum, ret, n;
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        sum += target;
//        if (sum % 2) return 0;
//        aim = sum / 2, n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == n)
//        {
//            if (prevSum == aim)
//                ret++;
//            return;
//        }
//        // pos位置选
//        prevSum += nums[pos];
//        dfs(nums, pos + 1);
//        prevSum -= nums[pos];
//
//        // pos位置不选
//        dfs(nums, pos + 1);
//    }
//};




//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        int aim = sum + target;
//        if (aim % 2 || aim < 0) return 0;
//        aim /= 2;
//
//        int n = nums.size();
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1, 0));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= aim; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1])
//                    dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][aim];
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int Prev, n;
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        n = candidates.size();
//        dfs(candidates, 0, target);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int pos, int target)
//    {
//        if (Prev >= target || pos >= n)
//        {
//            if (Prev == target) ret.push_back(path);
//            return;
//        }
//
//        // pos位置选 1~n
//        int x = candidates[pos];
//        for (int i = 1; Prev + x * i <= target; i++)
//        {
//            for (int cur = i; cur; cur--)
//                path.push_back(x);
//            Prev += x * i;
//            dfs(candidates, pos + 1, target);
//            Prev -= x * i;
//            for (int cur = i; cur; cur--)
//                path.pop_back();
//        }
//
//        // pos位置不选
//        dfs(candidates, pos + 1, target);
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int Prev;
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        dfs(candidates, 0, target);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int start, int target)
//    {
//        if (Prev > target) return;
//        if (Prev == target)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = start; i < candidates.size(); i++)
//        {
//            Prev += candidates[i];
//            path.push_back(candidates[i]);
//            dfs(candidates, i, target);
//            path.pop_back();
//            Prev -= candidates[i];
//        }
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        int n = candidates.size();
//        vector<vector<int>> dp(n + 1, vector<int>(target + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= target; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                int x = candidates[i - 1];
//                if (j >= x)
//                {
//                    dp[i][j] += dp[i - 1][j - x] + dp[i][j - x];
//                }
//            }
//        }
//        return dp[n][target];
//    }
//};




//class Solution {
//public:
//    vector<string> ret;
//    string path;
//    int n;
//    vector<string> letterCasePermutation(string s) {
//        n = s.size();
//        dfs(s, 0);
//        return ret;
//    }
//
//    void dfs(string& s, int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        char ch = s[pos];
//        path.push_back(ch);
//        dfs(s, pos + 1);
//        path.pop_back();
//
//        if (ch >= 'a' && ch <= 'z')
//        {
//            path.push_back(toupper(ch));
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//
//        if (ch >= 'A' && ch <= 'Z')
//        {
//            path.push_back(tolower(ch));
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//};




//class Solution {
//public:
//    bool check[16];
//    int ret;
//    int countArrangement(int n) {
//        dfs(n, 1);
//        return ret;
//    }
//
//    void dfs(int n, int pos)
//    {
//        if (pos > n)
//        {
//            ret++;
//            return;
//        }
//
//        for (int i = 1; i <= n; i++)
//        {
//            if (check[i] == false && (i % pos == 0 || pos % i == 0))
//            {
//                check[i] = true;
//                dfs(n, pos + 1);
//                check[i] = false;
//            }
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<string>> ret;
//    vector<string> path;
//    bool CheckCol[10], CheckDig1[20], CheckDig2[20];
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//            path[i].append(n, '.');
//        dfs(n, 0);
//        return ret;
//    }
//
//    void dfs(int n, int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            if (CheckCol[i] == false && CheckDig1[pos - i + n] == false && CheckDig2[pos + i] == false)
//            {
//                CheckCol[i] = CheckDig1[pos - i + n] = CheckDig2[pos + i] = true;
//                path[pos][i] = 'Q';
//                dfs(n, pos + 1);
//                path[pos][i] = '.';
//                CheckCol[i] = CheckDig1[pos - i + n] = CheckDig2[pos + i] = false;
//            }
//        }
//    }
//};




class Solution {
public:
    bool CheckRow[9][10], CheckCol[9][10], CheckBlock[3][3][10];
    bool isValidSudoku(vector<vector<char>>& board) {
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (board[i][j] != '.')
                {
                    int x = board[i][j] - '0';
                    if (CheckBlock[i / 3][j / 3][x] || CheckRow[i][x] || CheckCol[j][x])
                        return false;
                    CheckBlock[i / 3][j / 3][x] = CheckRow[i][x] = CheckCol[j][x] = true;
                }
            }
        }
        return true;
    }
};