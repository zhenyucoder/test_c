#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <string>
#include <assert.h>

//class String
//{
//public:
//	//String()
//	//	:_str(new char[1]{ '\0' })
//	//	, _capacity(0)
//	//	, _size(0)
//	//{ }
//
//	String(const char* str = "")
//		:_size(strlen(str))
//		,_capacity(_size)
//	{
//		_str = new char[_size+1];
//		strcpy(_str, str);
//	}
//
//
//	/*String(const String& s)
//		:_str(new char[s._size + 1])
//	{
//		strcpy(_str, s._str);
//		_size = s._size;
//		_capacity = s._capacity;
//	}*/
//
//	//�ִ�д��
//	void swap(String& s)
//	{
//		std::swap(_str, s._str);
//		std::swap(_size, s._size);
//		std::swap(_capacity, s._capacity);
//	}
//
//	String(const String& s)
//		:_str(nullptr)
//		,_size(0)
//		,_capacity(0)
//	{
//		String tmp(s._str);
//		swap(tmp);
//	}
//
//	/*String& operator=(const String& s)
//	{
//		if (this != &s)
//		{
//			_str = new char[s._size + 1];
//			strcpy(_str, s._str);
//			_size = s._size;
//			_capacity = s._capacity;
//		}
//		return *this;
//	}*/
//
//	String& operator=(const String& s)
//	{
//		if (this != &s)
//		{
//			String tmp(s);
//			swap(tmp);
//		}
//		return *this;
//	}
//
//	~String()
//	{
//		delete[] _str;
//		_size = _capacity = 0;
//	}
//	const char* c_str()const
//	{
//		return _str;
//	}
//
//private:
//	char* _str;
//	int _size;
//	int _capacity;
//};

//int main()
//{
//	//String s1("hello Linux");
//	///*String s2;
//	//cout << s2.c_str() << endl;*/
//	//String s2(s1);
//	//cout << s1.c_str() << endl;
//	///*String s3 = s1;
//	//cout << s3.c_str() << endl;*/
//
//
//	String s1("hello world");
//	cout << s1.c_str() << endl;
//
//	String s2(s1);
//	cout << s1.c_str() << endl;
//
//	String s3;
//	s3 = "1234567890";
//	cout << s3.c_str() << endl;
//	return 0;
//}

class String
{

public:

	String(const char* str = "")
	{
		if (nullptr == str)
		{
			assert(false);
			return;
		}

		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}

	void swap(String& s)
	{
		std::swap(_str, s._str);
	}

	String(const String& s)
		:_str(nullptr)
	{
		String tmp(s._str);
		swap(tmp);
	}

	String& operator=(String tmp)
	{
		swap(tmp);
		return *this;
	}

	~String()
	{
		delete[] _str;
	}

	const char* c_str()const
	{
		return _str;
	}
private:
	char* _str;

};

int main()
{
	string s1("1234567890");
	cout << s1.c_str() << endl;

	string s2 = s1;
	cout << s2.c_str() << endl;

	string s3;
	s3 = "234567";
	cout << s3.c_str() << endl;
	s3 = s1;
	cout << s3.c_str() << endl;

	return 0;
}