#include <iostream>
#include <string>
using namespace std;


//C++98私有化父类的构造函数，实现一个不能被继承的类
//class A 
//{
//public:
////private:
//	A()
//	{}
//
//private:
//	int _a;
//	int _b;
//};
//
//class B : public A
//{
//public:
//	B()
//		:A()
//	{}
//};
//
//int main()
//{
//	B bb;
//
//	return 0;
//}


//C++11 新增final，修饰父类，直接不能被继承
//class A  final
//{
//public:
//	A()
//	{}
//
//private:
//	int _a;
//	int _b;
//};
//
//class B : public A
//{
//public:
//};
//
//int main()
//{
//	B bb;
//
//	return 0;
//}



//class Person
//{
//public:
//	Person()
//	{
//		++_count;
//	}
//	static int _count;
//};
//
//int Person::_count = 0;
//
//class Student : public Person
//{
//
//};
//
//int main()
//{
//	//static静态变量在整个继承中只有一份，类似于函数，只继承了使用权
//	Person p1;
//	Person p2;
//	cout << Person::_count << endl;
//	Student s1;
//	cout << Person::_count << endl;
//	Student s2;
//	cout << Person::_count << endl;
//
//	return 0;
//}


//class Person
//{
//public:
//	string _name;
//};
//
//class Student : public Person
//{
//protected:
////public:
//	int _num;//学号
//};
//
//class Teacher : public Person
//{
//protected:
//	int _id;//编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//int main()
//{
//	Assistant a1;
//	//二义性
//	a1.Student::_name = "张三";
//	a1.Teacher::_name = "李四";
//	return 0;
//}


//class Person
//{
//public:
//	string _name;
//};
//
//class Student : virtual public Person
//{
//protected:
//	//public:
//	int _num;//学号
//};
//
//class Teacher : virtual public Person
//{
//protected:
//	int _id;//编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//int main()
//{
//	Assistant a1;
//	a1._name = "王五";
//	a1.Student::_name = "张三";
//	a1.Teacher::_name = "李四";
//	return 0;
//}


//class A
//{
//public:
//	int _a;
//};
//
////class B : public A
//class B : virtual public A
//{ 
//public:
//	int _b;
//};
//
////class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	D d;
//	d.C::_a = 1;
//	d.B::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	cout << sizeof(d) << endl;
//	return 0;
//}



//class A
//{
//public:
//	void func()
//	{}
//protected:
//	int _a;
//};
//
//class B : public A
//{
//public:
//	void f()
//	{
//		func();
//		_a++;
//	}
//protected:
//	int _b;
//};
//
//// 组合
//class C
//{
//public:
//	void func()
//	{}
//protected:
//	int _c;
//};
//
//class D
//{
//public:
//	void f()
//	{
//		_c.func();
//		//_c._a++;
//	}
//protected:
//	C _c;
//	int _d;
//};
//
//int main()
//{
//	cout << sizeof(D) << endl;
//	cout << sizeof(B) << endl;
//
//	D dd;
//	//dd.func();
//
//	B bb;
//	bb.func();
//
//	return 0;
//}



//using namespace std;
//class A {
//public:
//	A(const char* s) { cout << s << endl; }
//	~A() {}
//};
//
////class B :virtual public A
//class B : public A
//{
//public:
//	B(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
//};
//
//class C : public A
//{
//public:
//	C(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
//};
//
//class D : public C, public B
//{
//public:
//	//D(const char* s1, const char* s2, const char* s3, const char* s4) 
//	//	:B(s1, s2)
//	//	, C(s1, s3)
//	//	, A(s1)
//	//{
//	//	cout << s4 << endl;
//	//}
//
//	D(const char* s1, const char* s2, const char* s3, const char* s4)
//		:B(s1, s2)
//		, C(s1, s3)
//	{
//		cout << s4 << endl;
//	}
//};
//
//int main() {
//	D* p = new D("class A", "class B", "class C", "class D");
//	delete p;
//	return 0;
//}

class Base1 { public:  int _b1; };
class Base2 { public:  int _b2; };
class Derive : public Base1, public Base2 { public: int _d; };

int main() {
	Derive d;
	Base1* p1 = &d;
	Base2* p2 = &d;
	Derive* p3 = &d;
	return 0;
}