#include <iostream>
#include <vector>
#include <string>
using namespace std;

void test_vector1()
{
	vector<int> v1;
	vector<int> v2(10, 0);
	for (auto x : v2)
	{
		cout << x << ' ';
	}
	cout << endl;

	vector<int> v3(v2.begin() + 1, v2.end() - 1);
	for (size_t i = 0; i < v2.size() - 2; i++)
	{
		cout << v3[i] << ' ';
	}
	cout << endl;

	string s1("hello world");
	vector<int> v4(s1.begin(), s1.end());
	for (size_t i = 0; i < v4.size(); i++)
	{
		cout << v4[i] << ' ';
	}
	cout << endl;

	vector<int> v5(v4);
	vector<int>::iterator it = v5.begin();
	while (it != v5.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}


void test_vector2()
{
	vector<int> v;
	v.reserve(100);
	size_t sz = v.capacity();
	for(size_t i = 0; i < 100; i++)
	{
		v.push_back(0);
		if (sz != v.capacity())
		{
			cout << "扩容:" << v.capacity() << endl;
			sz = v.capacity();
		}

	}
}

void test_vector3()
{
	vector<int> v;
	//v.reserve(100);//err 由于reserve导致size并没有改变， 在加上operator[]中直接断言i<size，会导致后序程序报错崩溃
	v.resize(100);

	for (size_t i = 0; i < 100; i++)
	{
		v[i] = i;
	}

	for (auto e : v)
	{
		cout << e << ' ';
	}
	cout << endl;
}


void test_vector4()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	for (auto e : v)
	{
		cout << e << ' ';
	}
	cout << endl;

	v.insert(v.begin(), 0);
	for (auto e : v)
	{
		cout << e << ' ';
	}
	cout << endl;

	auto it = find(v.begin(), v.end(), 3);
	if (it != v.end())
	{
		v.erase(it);
	}

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.shrink_to_fit();

	cout << v.size() << endl;
	cout << v.capacity() << endl;
}

int main()
{
	test_vector4();
	return 0;
}