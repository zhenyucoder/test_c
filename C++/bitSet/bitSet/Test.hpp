#pragma once
#include <iostream>
#include <vector>
#include <assert.h>

namespace simulate
{
	template <size_t N>
	class bitset
	{
	public:
		bitset()
		{
			_bits.resize(N / 32 + 1);
		}
		void set(size_t x)
		{
			assert(x <= N);
			size_t i = x / 32;
			size_t j = x % 32;
			_bits[i] |= (1 << j);
		}
		void reset(size_t x)
		{
			assert(x <= N);
			size_t i = x / 32;
			size_t j = x % 32;
			_bits[i] &= ~(1 << j);
		}
		bool test(size_t x)
		{
			assert(x <= N);
			size_t i = x / 32;
			size_t j = x % 32;
			return _bits[i] & (1 << j);
		}
	private:
		std::vector<int> _bits;
	};

	template <size_t N>
	class two_bitset1
	{
	public:
		void set(size_t x)
		{
			bool ret1 = _bits1(x), ret2 = _bits2(x);
			if (ret1 == false && ret2 == false) // 00 -> 01
			{
				_bits2.set(x);
			}
			else if (ret1 == false && ret2 == true) // 01->10
			{
				_bits1.set(x);
				_bits2.reset(x);
			}
		}

		bool test(size_t x) // 只出现一次
		{
			bool ret1 = _bits1(x), ret2 = _bits2(x);
			return !ret1 && ret2;
		}
	private:
		bitset<N> _bits1;
		bitset<N> _bits2;
	};

	template <size_t N>
	class two_bitset2
	{
	public:
		void set(size_t x)
		{
			bool ret1 = _bits1(x), ret2 = _bits2(x);
			if (ret1 == false && ret2 == false) // 00 -> 01
			{
				_bits2.set(x);
			}
			else if (ret1 == false && ret2 == true) // 01->10
			{
				_bits1.set(x);
				_bits2.reset(x);
			}
			else if (ret1 == false && ret2 == true) // 10->11
			{
				_bits2.set(x);
			}
		}

		bool test(size_t x) // 不超过两次
		{
			if (x > N) return false;
			bool ret1 = _bits1(x), ret2 = _bits2(x);
			if((ret1 == false && ret2 == true) || (ret1 == true && ret2 == false))
				return true;
			return false;
		}
	private:
		bitset<N> _bits1;
		bitset<N> _bits2;
	};


	

	struct HashFuncBKDR
	{
		size_t operator()(std::string& str)
		{
			size_t hash = 0;
			for(auto ch : str)
			{
				hash = hash * 131 + ch;        
			}
			return hash;
		}
	};

	struct HashFuncAP
	{
		size_t operator()(std::string& str)
		{
			size_t hash = 0;
			for (long i = 0; i < str.size(); i++)
			{
				if ((i & 1) == 0)
				{
					hash ^= ((hash << 7) ^ str[i] ^ (hash >> 3));
				}
				else
				{
					hash ^= (~((hash << 11) ^ str[i] ^ (hash >> 5)));
				}
			}
			return hash;
		}
	};

	struct HashFuncDJB
	{
		size_t operator()(std::string& str)
		{
			size_t hash = 5381;
			for (auto ch : str)
			{
				hash += (hash << 5) + ch;
			}
			return hash;
		}
	};

	template<size_t N, class K = std::string, class Hash1 = HashFuncBKDR, class Hash2 = HashFuncAP, class Hash3 = HashFuncDJB>
	class BloomFilter
	{
	public:
		//BloomFilter()
		//{
		//	_bits.resize(N);
		//}

		void set(const K& key)
		{
			size_t hash1 = HashFuncBKDR()(key) % N;
			_bits.set(hash1);
			size_t hash2 = HashFuncAP()(key) % N;
			_bits.set(hash2);
			size_t hash3 = HashFuncDJB()(key) % N;
			_bits.set(hash3);
		}
		//void reset(const K& key)
		//{
		//}
		bool test(const K& key)
		{
			size_t hash1 = HashFuncBKDR()(key) % N;
			if (_bits.test(hash1) == false) return false;
			size_t hash2 = HashFuncAP()(key) % N;
			if (_bits.test(hash2) == false) return false;
			size_t hash3 = HashFuncDJB()(key) % N;
			if (_bits.test(hash3) == false) return false;
			return true;
		}
	private:
		bitset<N> _bits;
	};
}