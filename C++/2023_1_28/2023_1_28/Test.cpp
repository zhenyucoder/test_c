//#include <iostream>
//using namespace std;
//
//bool istargetstr(const string& s)//判断是否为回文字符串
//{
//    size_t begin = 0, end = s.size() - 1;
//    while (begin < end)
//    {
//        if (s[begin] != s[end])
//            return false;
//        ++begin, --end;
//    }
//    return true;
//}
//int main() {
//    string strA, strB;
//    getline(cin, strA);
//    getline(cin, strB);
//
//    int total = 0;
//    for (int i = 0; i <= strA.size(); i++)
//    {
//        string str = strA;
//        str.insert(i, strB);
//        if (istargetstr(str))
//            total++;
//    }
//    cout << total << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//#include <string>
//
//int main()
//{
//	string s1 = "1234", s2 = "789";//前插
//	s1.insert(4, s2);
//	cout << s1 << endl;
//	return 0;
//}


//#include <iostream>
//#include<string>
//#include<algorithm>
//using namespace std;
//int main()
//{
//	string s, table = "0123456789ABCDEF";
//	int m, n;
//	cin >> m >> n;
//	bool flag = false;
//	// 如果是负数，则转成正数，并标记一下
//	if (m < 0)
//	{
//		m = 0 - m;
//		flag = true;
//	}
//	// 按进制换算成对应的字符添加到s
//	while (m)
//	{
//		s += table[m % n];
//		m /= n;
//	}
//	if (flag)
//		s +='-';
//	reverse(s.begin(), s.end());
//	cout << s << endl;
//	return 0;
//}


#include <iostream>
#include<vector>
using namespace std;
int GetMax(int a, int b) //得到两个数的最大值
{
	return (a) > (b) ? (a) : (b);
}
int main()
{
	int size;
	cin >> size;
	vector<int> nums(size);
	for (size_t i = 0; i < size; ++i)
		cin >> nums[i];
	int Sum = nums[0]; //临时最大值
	int MAX = nums[0]; //比较之后的最大值
	for (int i = 1; i < size; i++)
	{
		Sum = GetMax(Sum + nums[i], nums[i]); //状态方程if (Sum >= MAX)
		MAX = Sum;
	}
	cout << MAX << endl;
	return 0;
}