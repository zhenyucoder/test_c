//class Solution {
//public:
//    vector<vector<string>> ret;
//    vector<string> path;
//    bool checkCol[10], checkDig1[20], checkDig2[20];//标记每列、主副对角线是否存在皇后信息
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//            path[i].append(n, '.');
//        dfs(0, n);
//        return ret;
//    }
//
//    void dfs(int pos, int n)
//    {
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            if (checkCol[i] == false && checkDig1[pos - i + n] == false && checkDig2[pos + i] == false)
//            {
//                path[pos][i] = 'Q';
//                checkCol[i] = true, checkDig1[pos - i + n] = true, checkDig2[pos + i] = true;
//                dfs(pos + 1, n);
//                //恢复现场
//                path[pos][i] = '.';
//                checkCol[i] = false, checkDig1[pos - i + n] = false, checkDig2[pos + i] = false;
//            }
//        }
//    }
//};




//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool grid[3][3][10];
//    bool isValidSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    //判断是否有效
//                    if (row[i][num] || col[j][num] || grid[i / 3][j / 3][num])
//                        return false;
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//        return true;
//    }
//};




//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool grid[3][3][10];
//    void solveSudoku(vector<vector<char>>& board) {
//        //保存初始数独信息
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//        dfs(board);
//    }
//
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int num = 1; num <= 9; num++)
//                        if (!row[i][num] && !col[j][num] && !grid[i / 3][j / 3][num])
//                        {
//                            board[i][j] = num + '0';
//                            row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                            bool flag = dfs(board);
//                            if (flag == false)
//                            {//恢复现场
//                                board[i][j] = '.';
//                                row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = false;
//                                continue;
//                            }
//                            break;
//                        }
//                }
//                //判断当前位置能否成功填入数字
//                if (board[i][j] == '.')
//                    return false;
//            }
//        return true;
//
//    }
//};



//class Solution {
//public:
//    vector<vector<bool>> isuse;
//    int m = 0, n = 0;
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        isuse.resize(m);
//        for (int i = 0; i < m; i++)
//            isuse[i].resize(n);
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (board[i][j] == word[0] && dfs(board, i, j, word, 0))
//                    return true;
//        return false;
//    }
//
//    bool dfs(vector<vector<char>>& board, int x, int y, string word, int pos)
//    {
//        if (pos == word.size() - 1)
//            return true;
//        isuse[x][y] = true;
//        if ((x - 1 >= 0) && !isuse[x - 1][y] && board[x - 1][y] == word[pos + 1])
//            if (dfs(board, x - 1, y, word, pos + 1))
//                return true;
//        if ((x + 1 < m) && !isuse[x + 1][y] && board[x + 1][y] == word[pos + 1])
//            if (dfs(board, x + 1, y, word, pos + 1))
//                return true;
//        if ((y - 1 >= 0) && !isuse[x][y - 1] && board[x][y - 1] == word[pos + 1])
//            if (dfs(board, x, y - 1, word, pos + 1))
//                return true;
//        if ((y + 1 < n) && !isuse[x][y + 1] && board[x][y + 1] == word[pos + 1])
//            if (dfs(board, x, y + 1, word, pos + 1))
//                return true;
//        //恢复现场
//        isuse[x][y] = false;
//        return false;
//    }
//};


//class Solution {
//public:
//    bool isuse[7][7];
//    int m = 0, n = 0;
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (board[i][j] == word[0])
//                {
//                    isuse[i][j] = true;
//                    if (dfs(board, i, j, word, 1))
//                        return true;
//                    isuse[i][j] = false;//恢复现场
//                }
//        return false;
//    }
//
//    int dx[4] = { 0, 0, -1, 1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    bool dfs(vector<vector<char>>& board, int i, int j, string& word, int pos)
//    {
//        if (pos == word.size())
//            return true;
//        //向量的方式， 定义上下左右四个位置
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k];
//            int y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !isuse[x][y] && board[x][y] == word[pos])
//            {
//                isuse[x][y] = true;
//                if (dfs(board, x, y, word, pos + 1))
//                    return true;
//                //恢复现场
//                isuse[x][y] = false;
//            }
//        }
//        return false;
//    }
//};




//class Solution {
//public:
//    bool vis[16][16];
//    int ret;
//    int m, n;
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (grid[i][j] != 0)
//                {
//                    vis[i][j] = true;
//                    dfs(grid, grid[i][j], i, j);
//                    vis[i][j] = false;
//                }
//        return ret;
//    }
//
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    void dfs(vector<vector<int>>& grid, int sum, int i, int j)
//    {
//        ret = max(ret, sum);
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k];
//            int y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y])
//            {
//                vis[x][y] = true;
//                dfs(grid, sum + grid[x][y], x, y);
//                //恢复现场
//                vis[x][y] = false;
//            }
//        }
//
//    }
//};



class Solution {
public:
    bool vis[21][21];
    int ret, step, m, n;
    int dx[4] = { 0, 0, 1, -1 }, dy[4] = { 1, -1, 0, 0 };
    int uniquePathsIII(vector<vector<int>>& grid) {
        int bx, by;//记录开始位置
        m = grid.size(), n = grid[0].size();
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (grid[i][j] == 0)
                    step++;
                else if (grid[i][j] == 1)
                    bx = i, by = j;
        step += 2, vis[bx][by] = true;
        dfs(grid, bx, by, 1);
        return ret;
    }

    void dfs(vector<vector<int>>& grid, int i, int j, int count)
    {
        if (grid[i][j] == 2)
        {
            //判断是否合法路径
            if (count == step)
                ret++;
            return;
        }
        for (int k = 0; k < 4; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] != -1)
            {
                vis[x][y] = true;
                dfs(grid, x, y, count + 1);
                //恢复现场
                vis[x][y] = false;
            }
        }
    }
};