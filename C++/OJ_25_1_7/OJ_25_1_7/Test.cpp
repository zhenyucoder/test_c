//class Solution {
//public:
//    bool canConstruct(string ransomNote, string magazine) {
//        unordered_map<char, int> hash1, hash2;
//        for (auto ch : magazine)
//            hash1[ch]++;
//        for (auto ch : ransomNote)
//        {
//            if (!hash1.count(ch) || ++hash2[ch] > hash1[ch])
//                return false;
//        }
//        return true;
//    }
//};




class Solution {
public:
    int bulbSwitch(int n)
    {
        return sqrt(n + 0.5);
    }
};