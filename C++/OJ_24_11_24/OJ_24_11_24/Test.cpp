//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        vector<int> tmp(arr.begin(), arr.end());
//        int n = tmp.size();
//        for (int cur1 = 0, cur2 = 0; cur1 < n && cur2 < n; cur1++, cur2++)
//        {
//            arr[cur1] = tmp[cur2];
//            if (tmp[cur2] == 0 && ++cur1 < n)
//                arr[cur1] = 0;
//        }
//    }
//};


//class Solution
//{
//public:
//	void sortColors(vector<int>& nums)
//	{
//		int n = nums.size();
//		int left = -1, right = n, i = 0;
//		while (i < right)
//		{
//			if (nums[i] == 0) swap(nums[++left], nums[i++]);
//			else if (nums[i] == 1) i++;
//			else swap(nums[--right], nums[i]);
//		}
//	}
//};




//class Solution
//{
//public:
//	vector<int> sortArray(vector<int>& nums)
//	{
//		srand(time(NULL)); 
//		qsort(nums, 0, nums.size() - 1);
//		return nums;
//	}
//	void qsort(vector<int>& nums, int l, int r)
//	{
//		if (l >= r) return;
//		int key = getRandom(nums, l, r);
//		int i = l, left = l - 1, right = r + 1;
//		while (i < right)
//		{
//			if (nums[i] < key) swap(nums[++left], nums[i++]);
//			else if (nums[i] == key) i++;
//			else swap(nums[--right], nums[i]);
//		}
//		qsort(nums, l, left);
//		qsort(nums, right, r);
//	}
//	int getRandom(vector<int>& nums, int left, int right)
//	{
//		int r = rand();
//		return nums[r % (right - left + 1) + left];
//	}
//};




class Solution
{
public:
	int findKthLargest(vector<int>& nums, int k)
	{
		srand(time(NULL));
		return qsort(nums, 0, nums.size() - 1, k);
	}
	int qsort(vector<int>& nums, int l, int r, int k)
	{
		if (l == r) return nums[l];
		// 1. 随机选择基准元素
		int key = getRandom(nums, l, r);
		// 2. 根据基准元素将数组分三块
		int left = l - 1, right = r + 1, i = l;
		while (i < right)
		{
			if (nums[i] < key) swap(nums[++left], nums[i++]);
			else if (nums[i] == key) i++;
			else swap(nums[--right], nums[i]);
		}
		// 3. 分情况讨论
		int c = r - right + 1, b = right - left - 1;
		if (c >= k) return qsort(nums, right, r, k);
		else if (b + c >= k) return key;
		else return qsort(nums, l, left, k - b - c);
	}
	int getRandom(vector<int>& nums, int left, int right)
	{
		return nums[rand() % (right - left + 1) + left];
	}
};