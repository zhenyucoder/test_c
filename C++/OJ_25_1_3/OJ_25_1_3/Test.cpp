//struct ListNode* rotateRight(struct ListNode* head, int k) {
//    if (k == 0 || head == NULL || head->next == NULL)
//        return head;
//
//    struct ListNode* dummy = (struct ListNode*)malloc(sizeof(struct ListNode));
//    dummy->next = head;
//
//    int length = 1;
//    struct ListNode* iter = head;
//    while (iter->next)
//    {
//        length++;
//        iter = iter->next;
//    }
//    k %= length;
//    if (k == 0)
//        return head;
//
//    struct ListNode* slow = dummy, * fast = head;
//    while (k--)
//    {
//        fast = fast->next;
//    }
//
//    while (fast)
//    {
//        slow = slow->next;
//        fast = fast->next;
//    }
//
//    dummy->next = slow->next;
//    slow->next = NULL;
//    iter->next = head;
//    return dummy->next;
//}




class Solution {
public:
    Node* copyRandomList(Node* head) {
        if (head == nullptr) return nullptr;
        Node* cur = head;
        while (cur)
        {
            Node* tmp = cur->next;

            Node* copy = new Node(cur->val);
            cur->next = copy;
            copy->next = tmp;

            cur = tmp;
        }

        // 处理所有新节点的random指针
        cur = head;
        while (cur)
        {
            if (cur->random)
                cur->next->random = cur->random->next;
            cur = cur->next->next;
        }

        // 恢复原链表，提取新链表
        cur = head;
        Node* newhead = new Node(0), * prev = newhead;
        while (cur)
        {
            Node* next = cur->next->next;
            Node* Cnext = cur->next;

            cur->next = next;
            cur = cur->next;

            prev->next = Cnext;
            prev = Cnext;
        }
        prev->next = nullptr;
        prev = newhead->next;
        delete newhead;
        return prev;
    }
};
