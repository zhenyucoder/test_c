//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        int five = 0, ten = 0;
//        for (auto x : bills)
//        {
//            if (x == 5)
//                five++;
//            else if (x == 10)
//            {
//                if (five == 0) return false;
//                five--, ten++;
//            }
//            else
//            {
//                if (five != 0 && ten != 0)//小贪心
//                    five--, ten--;
//                else if (five >= 3)
//                    five -= 3;
//                else
//                    return false;
//            }
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        priority_queue<double> pq;
//        double sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//            pq.push(x);
//        }
//        double target = sum / 2; int ret = 0; sum = 0;
//        while (sum < target)
//        {
//            double x = pq.top() / 2;
//            pq.pop();
//
//            sum += x, ret++;
//            pq.push(x);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    string largestNumber(vector<int>& nums) {
//        vector<string> v;
//        for (auto x : nums)
//            v.push_back(to_string(x));
//        sort(v.begin(), v.end(), [](const string& a, const string& b)
//            {
//                return a + b > b + a;
//            });
//
//        //特殊处理
//        if (v[0][0] == '0') return "0";
//        string str;
//        for (auto s : v)
//            str += s;
//        return str;
//    }
//};



