//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        const int INF = -0x3f3f3f3f;
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(3, INF)), g = f;
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j <= 2; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        }
//
//        int ret = INF;
//        for (int i = 0; i <= 2; i++)
//            ret = max(ret, g[n - 1][i]);
//        return ret;
//    }
//};

//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= nums.size(); i++)
//        {
//            f[i] = max(f[i - 1], g[i - 1]);
//            g[i] = f[i - 1] + nums[i - 1];
//        }
//        return max(f[n], g[n]);
//    }
//};


//class Solution {
//public:
//    int _rob(vector<int>& nums, int begin, int end)
//    {
//        vector<int> f(end + 1), g(end + 1);
//        for (int i = begin; i <= end; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[end], g[end]);
//    }
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        return max(_rob(nums, 2, n), _rob(nums, 3, n - 1) + nums[0]);
//    }
//};



//class Solution {
//public:
//    int arr[10010] = { 0 }, top = 0;
//    int deleteAndEarn(vector<int>& nums) {
//        for (auto x : nums)
//        {
//            arr[x] += x;
//            top = max(top, x);
//        }
//
//        vector<int> f(top + 1), g(top + 1);
//        for (int i = 1; i <= top; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[top], g[top]);
//    }
//};



//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};



//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]); // 第i天结束，处于买入状态
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]); // 第i天结束，处于卖出状态
//            dp[i][2] = dp[i - 1][0] + prices[i]; // 第i天结束，处于冷冻期
//        }
//        return max(dp[n - 1][0], max(dp[n - 1][1], dp[n - 1][2]));
//    }
//};


class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        int n = prices.size();
        vector<int> f(n), g(n);
        f[0] = -prices[0];
        for (int i = 1; i < n; i++)
        {
            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
        }
        return g[n - 1];
    }
};