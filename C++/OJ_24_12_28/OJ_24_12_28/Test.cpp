//class Solution {
//public:
//    bool isSubsequence(string s, string t) {
//        int m = s.size(), n = t.size();
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        for (int j = 0; j <= n; j++)
//            dp[0][j] = true;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                dp[i][j] = (s[i - 1] == t[j - 1]) ? dp[i - 1][j - 1] : dp[i][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};



class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        int low = 0, high = numbers.size() - 1;
        while (low < high)
        {
            int sum = numbers[low] + numbers[high];
            if (sum == target) 
            {
                return { low + 1, high + 1 };
            }
            else if (sum < target)
            {
                ++low;
            }
            else 
            {
                --high;
            }
        }
        return { -1, -1 };
    }
};