//class Solution
//{
//public:
//	int lengthOfLIS(vector<int>& nums)
//	{
//		// 动态规划
//		int n = nums.size();
//		vector<int> dp(n, 1);
//		int ret = 0;
//		for (int i = n - 1; i >= 0; i--)
//		{
//			for (int j = i + 1; j < n; j++)
//			{
//				if (nums[j] > nums[i])
//				{
//					dp[i] = max(dp[i], dp[j] + 1);
//				}
//			}
//			ret = max(ret, dp[i]);
//		}
//		return ret;
//		// 记忆化搜索
//		// 
//		// vector<int> memo(n);
//		// int ret = 0;
//		// for(int i = 0; i < n; i++)
//		// ret = max(ret, dfs(i, nums, memo));
//		// return ret;
//	}
//
//	int dfs(int pos, vector<int>& nums, vector<int>& memo)
//	{
//		if (memo[pos] != 0) return memo[pos];
//		int ret = 1;
//		for (int i = pos + 1; i < nums.size(); i++)
//		{
//			if (nums[i] > nums[pos])
//			{
//				ret = max(ret, dfs(i, nums, memo) + 1);
//			}
//		}
//		memo[pos] = ret;
//		return ret;
//	}
//};





class Solution
{
	int memo[201][201];
public:
	int getMoneyAmount(int n)
	{
		return dfs(1, n);
	}
	int dfs(int left, int right)
	{
		if (left >= right) return 0;
		if (memo[left][right] != 0) return memo[left][right];
		int ret = INT_MAX;
		for (int head = left; head <= right; head++)
		{
			int x = dfs(left, head - 1);
			int y = dfs(head + 1, right);
			ret = min(ret, head + max(x, y));
		}
		memo[left][right] = ret;
		return ret;
	}
}