//int m, n;
//int choose[11] = { 0 };
//int sum = 0, count = 0;
//
//void dfs(int pos)
//{
//	if (pos == n + 1 || sum > m)
//		return;
//	if (sum == n)
//	{
//		for(int i = 0; i < count; i++)
//			cout << choose[i] << " ";
//		cout << endl;
//		return;
//	}
//	sum += pos; choose[count++] = pos;
//	dfs(pos + 1);
//	//恢复现场
//	sum -= pos; count--;
//	dfs(pos + 1);
//}
//int main()
//{
//	cin >> m >> n;
//	dfs(1);
//	return 0;
//}



//int main()
//{
//	string str1, str2;
//	cin >> str1 >> str2;
//	int m = str1.size(), n = str2.size();
//	vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//  for(int i = 1; i <= m; i++)//初始化
//		dp[i][0] = i;
//	for(int j = 1; j <= n; j++)
//		dp[0][j] = j;
// 
//	for (int i = 1; i <= m; i++)//填表
//	{
//		for (int j = 1; j <= n; j++)
//		{
//			if (str[i - 1] == str[j - 1])
//				dp[i][j] = dp[i - 1][j - 1];
//			else
//			dp[i][j] = min(dp[i - 1][j] + 1, min(dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1);
//		}
//	}
//	cout << dp[m][n] << endl;
//	return 0;
//}



//class Solution {
//public:
//    int countSubstrings(string s) {
//        int ret = 0, n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = j - i < 2 ? true : dp[i + 1][j - 1];
//                if (dp[i][j]) ret++;
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size(), ret = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < 2; j++)
//            {
//                int left = i, right = j + i;
//                //cout << left << ":" << right << endl;
//                while (left >= 0 && right < n)
//                {
//                    if (s[left] == s[right])
//                        ret++, left--, right++;
//                    else
//                        break;
//                }
//            }
//        }
//        return ret;
//    }
//};






//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int len = 0, begin = 0, n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = j - i < 2 ? true : dp[i + 1][j - 1];
//                if (dp[i][j] && j - i + 1 > len)
//                    begin = i, len = j - i + 1;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};





//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size(), len = 0, begin = 0;
//        for (int i = 0; i < n; i++)
//        {
//            //回文串为奇数
//            int left = i, right = i;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] != s[right])
//                    break;
//                if (right - left + 1 > len)
//                    begin = left, len = right - left + 1;
//                left--, right++;
//            }
//
//            //回文串为偶数
//            left = i, right = i + 1;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] != s[right])
//                    break;
//                if (right - left + 1 > len)
//                    begin = left, len = right - left + 1;
//                left--, right++;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};



//class Solution {
//public:
//    bool checkPartitioning(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)//保存子窜回文信息
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = j - i < 2 ? true : dp[i + 1][j - 1];
//            }
//        }
//
//        for (int i = 1; i < n - 1; i++)
//            for (int j = i; j < n - 1; j++)
//                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1])
//                    return true;
//        return false;
//    }
//};





//
//class Solution {
//public:
//    int minCut(string s) {
//        int n = s.size();
//        vector<vector<bool>> info(n, vector<bool>(n));//字串回文信息
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//                if (s[i] == s[j])
//                    info[i][j] = j - i < 2 ? true : info[i + 1][j - 1];
//
//        vector<int> dp(n, 0x3f3f3f3f);
//        for (int j = 0; j < n; j++)
//        {
//            if (info[0][j] == false)
//            {
//                for (int i = j; i > 0; i--)
//                {
//                    if (info[i][j])
//                        dp[j] = min(dp[j], dp[i - 1] + 1);
//                }
//            }
//            else
//                dp[j] = 0;
//        }
//        return dp[n - 1];
//    }
//};




