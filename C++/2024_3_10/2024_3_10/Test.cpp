//class Solution {
//public:
//    void sortColors(vector<int>& nums) {
//        int left = -1, right = nums.size();
//        for (int i = 0; i < right; )
//        {
//            int x = nums[i];
//            if (x == 0)
//                swap(nums[++left], nums[i++]);
//            else if (x == 1)
//                i++;
//            else
//                swap(nums[--right], nums[i]);
//        }
//    }
//};



//class Solution {
//public:
//    vector<int> sortArray(vector<int>& nums) {
//        srand(time(NULL));
//        qsort(nums, 0, nums.size() - 1);//快排
//        return nums;
//    }
//
//    void qsort(vector<int>& nums, int l, int r)
//    {
//        if (l >= r) return;
//        int x = getRandom(nums, l, r);//获取[l, r]中的随机值
//        int i = l, left = l - 1, right = r + 1;
//        while (i < right)
//        {
//            if (nums[i] < x)
//                swap(nums[++left], nums[i++]);
//            else if (nums[i] == x)
//                i++;
//            else
//                swap(nums[--right], nums[i]);
//        }
//        //[l, left] [left + 1, right - 1] [right , r]
//        qsort(nums, l, left);
//        qsort(nums, right, r);
//    }
//
//    int getRandom(vector<int>& nums, int l, int r)
//    {
//        int x = rand();
//        return nums[x % (r - l + 1) + l];
//    }
//};




class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        srand(time(NULL));
        return topsort(nums, 0, nums.size() - 1, k);
    }

    int topsort(vector<int>& nums, int l, int r, int k)
    {
        if (l == r)
            return nums[l];
        int x = getRandom(nums, l, r);
        int left = l - 1, right = r + 1, i = l;
        while (i < right)
        {
            if (nums[i] < x)
                swap(nums[++left], nums[i++]);
            else if (nums[i] == x)
                i++;
            else
                swap(nums[--right], nums[i]);
        }
        //判断更新区间
        if ((r - right + 1) >= k)
            return topsort(nums, right, r, k);
        else if (r - left >= k)
            return x;
        else
            return topsort(nums, l, left, k - (r - l));
    }

    //获取[l, r]区间中的随机值
    int getRandom(vector<int>& nums, int l, int r)
    {
        return nums[rand() % (r - l + 1) + l];
    }
};