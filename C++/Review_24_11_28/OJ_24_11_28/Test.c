#include <stdio.h>
#include <assert.h>
#include <string.h>

void* my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}

void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	if (dest < src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}


const char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);
	char* ret = str1, * s1 = str1, * s2 = str2;
	if (*str2 == '\0') return NULL;
	while (ret)
	{
		s1 = ret, s2 = str2;
		while (*s1 && *s2 && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
			return ret;
		ret++;
	}
	return NULL;
}

void* my_strlen(const char* str)
{
	assert(str);
	const char* start = str, * end = str;
	while (*end != '\0')
		end++;
	return end - start;
}

char* my_strcpy(char* dest, const char* src)
{
	assert(dest && src);
	char* ret = dest;
	while(*dest++ = *src++)
	{ }
	return ret;
}

char* my_strcat(char* dest, const char* src) // dest剩余空间必须能容纳下src
{
	assert(dest && src);
	char* ret = dest;
	// 查找dest中'\0'
	while (*dest)
		dest++;
	// 开始追加
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}

//int main()
//{
//	char dest[30] = "gygdywgdyw";
//	char* src = "dywg";
//	//char* ret = my_memcpy(dest, src, 6);
//	//char* ret = my_memmove(dest, src, 6);
//	//char* ret = my_strstr(dest, src);
//	//char * ret = my_strcpy(dest, src);
//	char* ret = my_strcat(dest, src);
//	printf("%s\n", ret);
//
//	//printf("%d\n", my_strlen(src));
//	return 0;
//}


//#define OFFSETOF(type, member) ((size_t)&((type*)0)->member)
//
//int main()
//{
//	struct Person
//	{
//		int age;
//		double salary;
//		char name[50];
//	};
//
//	//printf("%d\n", (size_t)&(((struct Person*)0)->salary));
//	printf("%d\n", OFFSETOF(struct Person, salary));
//
//	return 0;
//}


int cheak_sys()
{
	union Un
	{
		int i;
		char c;
	}un = { .i = 1 };
	return un.c;
}

int main()
{
	int ret = cheak_sys();
	if (ret == 1)
		printf("小端\n");
	else
		printf("大端\n");
	return 0;
}

