//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size(), ret = -0x3f3f3f3f;
//        vector<int> dp(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int sum = 0, n = nums.size();
//        for (auto x : nums)
//            sum += x;
//
//        vector<int> f(n + 1), g = f;
//        int fMin = 0x3f3f3f3f, gmax = -0x3f3f3f3f;
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = min(nums[i - 1], f[i - 1] + nums[i - 1]);
//            fMin = min(fMin, f[i]);
//            g[i] = max(nums[i - 1], g[i - 1] + nums[i - 1]);
//            gmax = max(gmax, g[i]);
//        }
//        return sum == fMin ? gmax : max(gmax, sum - fMin);
//    }
//};


//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size(), ret = -0x3f3f3f3f;
//        vector<int> f(n + 1, 1), g = f;
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = max(f[i - 1] * nums[i - 1], nums[i - 1]);
//                g[i] = min(g[i - 1] * nums[i - 1], nums[i - 1]);
//            }
//            else
//            {
//                f[i] = max(g[i - 1] * nums[i - 1], nums[i - 1]);
//                g[i] = min(f[i - 1] * nums[i - 1], nums[i - 1]);
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size(), ret = 0;
//        vector<int> f(n + 1, 0), g = f;
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = (g[i - 1] > 0) ? g[i - 1] + 1 : 0;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                g[i] = f[i - 1] + 1;
//                f[i] = (g[i - 1] > 0) ? g[i - 1] + 1 : 0;
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        int ret = 0;
//        for (int i = 2; i < n; i++)
//        {
//            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2])
//                dp[i] = dp[i - 1] + 1;
//            ret += dp[i];
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size(), ret = 1;
//        vector<int> f(n, 1), g(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            if (arr[i] > arr[i - 1])
//                f[i] = g[i - 1] + 1;
//            else if (arr[i] < arr[i - 1])
//                g[i] = f[i - 1] + 1;
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};



class Solution {
public:
    int findSubstringInWraproundString(string s) {
        int n = s.size();
        vector<int> dp(n, 1);
        for (int i = 1; i < n; i++)
            if ((s[i] - s[i - 1] == 1) || (s[i - 1] == 'z' && s[i] == 'a'))
                dp[i] += dp[i - 1];

        //去重
        int hash[26] = { 0 };
        for (int i = 0; i < n; i++)
            hash[s[i] - 'a'] = max(dp[i], hash[s[i] - 'a']);

        //统计结果
        int ret = 0;
        for (auto x : hash)
            ret += x;
        return ret;
    }
};