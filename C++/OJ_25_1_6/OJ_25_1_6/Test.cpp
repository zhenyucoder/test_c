//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0x3f3f3f3f));
//        dp[1][0] = 0;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//        return dp[m][n];
//    }
//};



class Solution {
public:
    bool wordBreak(string s, vector<string>& wordDict) {
        unordered_map<string, bool> hash;
        for (auto& str : wordDict)
        {
            hash[str] = true;
        }
        int n = s.size();
        s = ' ' + s;
        vector<bool> dp(n + 1);
        dp[0] = true;
        for (int i = 1; i <= n; i++)
        {
            for (int j = i; j >= 1; j--)
            {
                if (hash[s.substr(j, i - j + 1)] == true && dp[j - 1])
                {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[n];
    }
};