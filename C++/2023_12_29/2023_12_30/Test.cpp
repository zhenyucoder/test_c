#include <iostream>
using namespace std;
#include <array>
#include <vector>

//静态数组
//#define N 10
//
//// 非类型模板参数 -- 只支持整形
//template <class T, size_t N>
//class stack
//{
//private:
//	T _a[N];
//};
////
//
//
//int main()
//{
//	stack<int, 10> st1;
//	stack<int, 100> st2;
//	stack<double, 100> st3;
//	//int n;
//	//cin >> n;
//	//stack<int, n>;//err,N必须是整型
//
//	int a[10];
//	array<int, 10> a1;//本质上想代替C语言数组
//	a[12] = 1;
//	//a1[15] = 1;
//
//	//vector<int> v(10, 0);//bug
//	return 0;
//}


template<class T1, class T2>
class Data
{
public:
	Data()
	{
		cout << "Data<T1, T2>" << endl; 
	}
private:
	T1 _d1;
	T2 _d2;
};

//全特化
template<>
class Data<int, double>
{
public:
	Data()
	{
		cout << "Data<int, double>" << endl;
	}
};

//偏特化
template<class T>
class Data<double,T>
{
public:
	Data()
	{
		cout << "Data<T, double>" << endl;
	}
};

//偏特化
template<class T1,class T2>
class Data<T1*, T2*>
{
public:
	Data()
	{
		cout << "Data<T1*, T2*>" << endl;
	}
};

//偏特化
template<class T1, class T2>
class Data<T1&, T2&>
{
public:
	Data()
	{
		cout << "Data<T1&, T2&>" << endl;
	}
};

int main()
{
	Data<int, int> d1;

	Data<int*, int*> dd1;
	Data<int&, int&> dd2;

	//Data<int, double> d2;
	//Data<double, double> d3;
	return 0;
}


//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{}
//
//	bool operator<(const Date& d) const
//	{
//		return (_year < d._year) ||
//			(_year == d._year && _month < d._month) ||
//			(_year == d._year && _month == d._month && _day < d._day);
//	}
//
//	bool operator>(const Date& d) const
//	{
//		return (_year > d._year) ||
//			(_year == d._year && _month > d._month) ||
//			(_year == d._year && _month == d._month && _day > d._day);
//	}
//
//	friend ostream& operator<<(ostream& _cout, const Date& d);
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//ostream& operator<<(ostream& _cout, const Date& d)
//{
//	_cout << d._year << "-" << d._month << "-" << d._day;
//	return _cout;
//}
//
//template<class T>
//bool Less(const T& left, const T& right)
//{
//	return left < right;
//}
//
//template<>
//bool Less<Date*>(Date* const & left, Date* const & right)//很怪，函数模板建议不要用特化
//{
//	return *left < *right;
//}
//
//template<class T>
//bool Less(T* left, T* right)
//{
//	return *left < *right;
//}
//
//int main()
//{
//	Date d1(2023, 12, 28);
//	Date d2(2023, 12, 29);
//
//	Date* d3 = new Date(2023, 12, 28);
//	Date* d4 = new Date(2023, 12, 29);
//	cout << Less(d3, d4) << endl;
//	return 0;
//}

