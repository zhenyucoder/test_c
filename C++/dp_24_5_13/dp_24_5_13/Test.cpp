//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0) return 0;
//        if (n == 1 || n == 2) return 1;
//        vector<int> dp(n + 1);
//        dp[1] = dp[2] = 1;
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        return dp[n];
//    }
//};



//class Solution {
//public:
//    const int MOD = 1e9 + 7;
//    int waysToStep(int n) {
//        if (n == 1) return 1;
//        if (n == 2) return 2;
//        if (n == 3) return 4;
//        vector<int> dp(n + 1);
//        dp[1] = 1, dp[2] = 2, dp[3] = 4;
//        for (int i = 4; i <= n; i++)
//        {
//            dp[i] = ((dp[i - 1] + dp[i - 2]) % MOD + dp[i - 3]) % MOD;
//        }
//        return dp[n];
//    }
//};




//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        if (n == 0 || n == 1) return 0;
//        vector<int> dp(n + 1);
//        for (int i = 2; i <= n; i++)
//        {
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        }
//        return dp[n];
//    }
//};




//class Solution {
//public:
//    int numDecodings(string s) {
//        if (s[0] == '0') return 0;//特殊处理,前导0
//        int n = s.size();
//        vector<int> dp(n);
//        dp[0] = 1;
//        if (n == 1) return dp[0];
//        if (s[1] != '0')
//            dp[1]++;
//        int tmp = (s[0] - '0') * 10 + s[1] - '0';
//        if (tmp >= 10 && tmp <= 26) dp[1]++;
//        for (int i = 2; i < n; i++)
//        {
//            if (s[i] != '0') dp[i] += dp[i - 1];
//            int tmp = s[i] - '0' + (s[i - 1] - '0') * 10;
//            if (tmp >= 10 && tmp <= 26) dp[i] += dp[i - 2];
//        }
//        return dp[n - 1];
//    }
//};




//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        if (m == 1 || n == 1) return 1;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        dp[0][1] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};




//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        long long ret = 1;
//        for (int x = 1, y = n; x < m; x++, y++)
//            ret = ret * y / x;
//        return ret;
//    }
//};




//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int m = obstacleGrid.size(), n = obstacleGrid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        dp[0][1] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = obstacleGrid[i - 1][j - 1] == 1 ? 0 : dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};





//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int m = frame.size(), n = frame[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + +frame[i - 1][j - 1];
//        return dp[m][n];
//    }
//};




//class Solution {
//    const int INF = 0x3f3f3f3f;
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int m = matrix.size(), n = matrix[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 2, INF));
//        //初始化第一行，防止对后续填表结果造成影响
//        for (int j = 0; j < n + 2; j++)
//            dp[0][j] = 0;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j - 1], min(dp[i - 1][j], dp[i - 1][j + 1])) + matrix[i - 1][j - 1];
//        int ret = INF;
//        for (int j = 1; j <= n; j++)
//            ret = min(ret, dp[m][j]);
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        dp[1][0] = dp[0][1] = 0;// 初始化
//    //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//        return dp[m][n];
//    }
//};





class Solution {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon) {
        int m = dungeon.size(), n = dungeon[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0x3f3f3f3f));
        //初始化
        dp[m][n - 1] = dp[m - 1][n] = 1;
        for (int i = m - 1; i >= 0; i--)
            for (int j = n - 1; j >= 0; j--)
                dp[i][j] = max(1, min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j]);
        return dp[0][0];
    }
};