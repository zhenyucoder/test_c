//class Solution {
//public:
//    int memo[31];
//    int fib(int n) {
//        memo[0] = 0, memo[1] = 1;
//        dfs(n);
//        return memo[n];
//    }
//
//    void dfs(int n)
//    {
//        if (n < 2) return;
//        if (memo[n - 2] == 0) dfs(n - 2);
//        if (memo[n - 1] == 0) dfs(n - 1);
//        memo[n] = memo[n - 1] + memo[n - 2];
//    }
//};



//class Solution {
//    int memo[201][201];
//public:
//    int getMoneyAmount(int n) {
//        return dfs(1, n);
//    }
//    int dfs(int left, int right)
//    {
//        if (left >= right) return 0;
//        if (memo[left][right] != 0) return memo[left][right];
//        int ret = INT_MAX;
//        for (int head = left; head <= right; head++)
//        {
//            int l = dfs(left, head - 1);
//            int r = dfs(head + 1, right);
//            ret = min(ret, max(l, r) + head);
//        }
//        memo[left][right] = ret;
//        return ret;
//    }
//};


class Solution {
public:
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    int m, n;
    bool vis[201][201];
    int memo[201][201];
    int longestIncreasingPath(vector<vector<int>>& matrix) {
        m = matrix.size(), n = matrix[0].size();
        int ret = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                vis[i][j] = true;
                ret = max(ret, dfs(matrix, i, j));
                vis[i][j] = false;
            }
        }
        return ret;
    }

    int dfs(vector<vector<int>>& matrix, int i, int j)
    {
        if (memo[i][j] != 0) return memo[i][j];
        int ret = 1;
        for (int k = 0; k < 4; k++)
        {
            int x = dx[k] + i, y = dy[k] + j;
            if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j] && !vis[x][y])
            {
                vis[x][y] = true;
                ret = max(ret, dfs(matrix, x, y) + 1);
                vis[x][y] = false;
            }
        }
        memo[i][j] = ret;
        return ret;
    }
};