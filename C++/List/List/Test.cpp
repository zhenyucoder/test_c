#include <iostream>
#include<algorithm>
#include <list>
#include <vector>
using namespace std;

void test_list1()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.reverse();
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list2()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	//lt.push_back(2);
	lt.push_back(4);
	lt.push_back(2);
	lt.push_back(4);
	lt.push_back(6);

	lt.unique();//数据必循是连续的
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;


	//排序
	lt.sort();//默认升序
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	// < less ; >greater
	less<int> ls;
	lt.sort(ls);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	greater<int> gt;
	lt.sort(gt);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.unique();//数据必循是连续的
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

}

void test_list3()
{
	srand((size_t)time(0));
	const int N = 1000000;
	list<int> st1;
	vector<int> v;

	for (int i = 0; i < N; i++)
	{
		auto e = rand();
		st1.push_back(e);
		v.push_back(e);
	}

	int begin1 = clock();
	/*st1.sort();*/
	//将数据拷贝到vector
	vector<int> v1(st1.begin(), st1.end());
	sort(v1.begin(), v1.end());
	//数据拷贝回去
	st1.assign(v1.begin(), v1.end());
	int end1 = clock();

	int begin2 = clock();
	sort(v.begin(), v.end());
	int end2 = clock();

	printf("list sort:%d\n", end1 - begin1);
	printf("vector sort : %d\n", end2 - begin2);
}

void test_list4()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(3);
	lt.push_back(5);
	lt.push_back(3);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.remove(3);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test_list4();
	return 0;
}