//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size(); if (n == 0) return 0;
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};



//class Solution {
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        //初始化
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee);
//        }
//        return dp[n - 1][1];
//    }
//};



//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(3, -INF));
//        auto g = f;
//        //初始化
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//            for (int j = 0; j <= 2; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        //返回结果
//        int ret = -INF;
//        for (int j = 0; j <= 2; j++)
//            ret = max(ret, g[n - 1][j]);
//        return ret;
//    }
//};



class Solution {
public:
    const int KNF = 0x3f3f3f3f;
    int maxProfit(int k, vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> f(n, vector<int>(k + 1, -KNF));
        auto g = f;
        //初始化
        f[0][0] = -prices[0], g[0][0] = 0;
        for (int i = 1; i < n; i++)
            for (int j = 0; j <= k; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)
                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }

        int ret = -KNF;
        for (int j = 0; j <= k; j++)
            ret = max(ret, g[n - 1][j]);
        return ret;
    }
};