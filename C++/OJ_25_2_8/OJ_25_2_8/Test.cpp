//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        hash[0] = 1;
//        int ret = 0, prevSum = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            prevSum += nums[i];
//            int aim = prevSum - k;
//            if (hash.count(aim))
//                ret += hash[aim];
//            hash[prevSum]++;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> spiralOrder(vector<vector<int>>& matrix) {
//        int rows = matrix.size(), cols = matrix[0].size();
//        int top = 0, low = rows - 1, left = 0, right = cols - 1;
//        vector<int> ret(rows * cols);
//        int pos = 0;
//        while (top <= low && left <= right)
//        {
//            for (int j = left; j <= right; j++)
//                ret[pos++] = matrix[top][j];
//            for (int i = top + 1; i <= low; i++)
//                ret[pos++] = matrix[i][right];
//            if (left < right && top < low)
//            {
//                for (int j = right - 1; j >= left; j--)
//                    ret[pos++] = matrix[low][j];
//                for (int i = low - 1; i > top; i--)
//                    ret[pos++] = matrix[i][left];
//            }
//            left++, right--, top++, low--;
//        }
//        return ret;
//    }
//};