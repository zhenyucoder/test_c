//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        unordered_map<int, int> hash;
//        hash[nums[0]] = 0;
//        for (int i = 1; i < nums.size(); i++)
//        {
//            int x = target - nums[i];
//            if (hash.count(x))
//                return { hash[x], i };
//            hash[nums[i]] = i;
//        }
//        return {};
//    }
//};




//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* newhead = new ListNode, * prev = newhead;
//        int sum = 0; // 统计当前两链表节点和
//        while (l1 || l2 || sum)
//        {
//            if (l1)
//                sum += l1->val, l1 = l1->next;
//            if (l2)
//                sum += l2->val, l2 = l2->next;
//            ListNode* node = new ListNode(sum % 10);
//            sum /= 10;
//
//            prev->next = node;
//            prev = node;
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[256] = { 0 };
//        int maxlen = 0;
//        for (int left = 0, right = 0, n = s.size(); right < n; right++)
//        {
//            // 进窗口
//            char in = s[right];
//            hash[in]++;
//            while (hash[in] > 1) // 判断 出窗口
//            {
//                char out = s[left++];
//                hash[out]--;
//            }
//            // 更新结果
//            maxlen = max(maxlen, right - left + 1);
//        }
//        return maxlen;
//    }
//};



class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size();
        bool dp[1010][1010] = { 0 };
        for (int i = n - 1; i >= 0; i--)
            for (int j = i; j < n; j++)
                if (s[i] == s[j])
                    dp[i][j] = (j - i + 1 <= 2) ? true : dp[i + 1][j - 1];

        int pos = 0, len = 1;
        for (int i = 0; i < n; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                if (dp[i][j] && (j - i + 1) > len)
                {
                    len = j - i + 1;
                    pos = i;
                }
            }
        }
        return s.substr(pos, len);
    }
};