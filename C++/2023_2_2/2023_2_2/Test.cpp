class compare
{
public:
    bool operator()(const pair<string, int>& p1, const pair<string, int>& p2)
    {
        return (p1.second > p2.second) || (p1.second == p2.second && p1.first < p2.first);
    }
};
//
//int main() {
//    string str;
//    map<string, int> m;
//    while (getline(cin, str))
//    {
//        string tmp;
//        for (size_t i = 0; i < str.size(); i++)//分割单词，并统计次数
//        {
//            if (str[i] == ' ' || str[i] == '.')
//            {
//                m[tmp]++;
//                tmp = "";
//            }
//            else
//            {
//                tmp += tolower(str[i]);
//            }
//        }
//
//        //排序，次数优先，首字母小优先
//        vector<pair<string, int>> v(m.begin(), m.end());
//        sort(v.begin(), v.end(), compare());
//        for (auto pr : v)
//        {
//            cout << pr.first << ":" << pr.second << endl;
//        }
//    }
//    return 0;
//}


//class Solution {
//public:
//    class compare
//    {
//    public:
//        bool operator()(const pair<string, int> m1, const pair<string, int> m2)
//        {
//            return (m1.second > m2.second) || (m1.second == m2.second && m1.first < m2.first);
//        }
//    };
//
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        map<string, int> topWords;
//        for (auto str : words)
//        {
//            topWords[str]++;
//        }
//        vector<pair<string, int>> v(topWords.begin(), topWords.end());
//        sort(v.begin(), v.end(), compare());
//        vector<string> ret;
//        for (int i = 0; i < k; i++)
//        {
//            ret.push_back(v[i].first);
//        }
//        return ret;
//    }
//};


class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        //去重 + 排序
        set<int> s1(nums1.begin(), nums1.end());
        set<int> s2(nums2.begin(), nums2.end());
        for (auto num : s1)
        {
            cout << num << "->";
        }
        cout << endl;
        for (auto num : s2)
        {
            cout << num << "->";
        }
        cout << endl;

        vector<int> ret;
        set<int>::iterator begin1 = s1.begin(), begin2 = s2.begin();
        for (; begin1 != s1.end() && begin2 != s2.end();)
        {
            if (*begin1 == *begin2)
            {
                ret.push_back(*begin1);
                ++begin1;
                ++begin2;
            }
            else if (*begin1 > *begin2)
            {
                ++begin2;
            }
            else
            {
                ++begin1;
            }
        }
        return ret;
    }
};