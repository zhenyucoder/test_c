#include <iostream>
#include <string>
using namespace std;


void test_string()
{
	string s1("hello world");
	s1.insert(0, 1, 'x');
	cout << s1 << endl;

	s1.insert(0, "123456789");
	cout << s1 << endl;

	s1.insert(s1.end() - 1, '1');
	cout << s1 << endl;

}

void test_string1()
{
	string s1("hello xxxxx");
	cout << s1.size() << endl;

	s1.erase(5, 2);
	cout << s1 << endl;

	s1.erase(s1.size() - 4);
	cout << s1 << endl;

	s1.erase(s1.begin() + 1);
	cout << s1 << endl;

	s1.erase(s1.begin()+1, s1.end()-1);
	cout << s1 << endl;

	//s1.erase(s1.begin(), s1.end());
	//cout << s1 << endl;

	string s2("hello world!");
	s2.replace(5, 3, "x");
	cout << s2 << endl;

	string s3("The quick brown fox jumps over a lazy dog.");
	string s4;
	for (auto ch : s3)
	{
		if (ch == ' ')
			s4 += "20%";
		else
			s4 += ch;
	}
	cout << s4 << endl;

	cout << endl;
	cout << s3 << endl;

	cout << endl;
	cout << endl;
	swap(s3, s4);
	/*cout << s3.c_str() << endl;
	cout << s4.c_str() << endl;*/
	cout << s3 << endl;

	s3.swap(s4);
	cout << s3 << endl;
	cout << s4 << endl;

	printf("%p\n", s2.c_str());
}

void test_string2()
{
	string s1("test.cpp.tar.zip");
	size_t i = s1.find(".");
	cout << s1.substr(i+1,7) << endl;

	size_t i1 = s1.rfind('.');
	cout << s1.substr(i1+1) << endl;

	//协议、域名、资源名
	string s3("https://legacy.cplusplus.com/reference/string/string/rfind/");
	//string s3("ftp://www.baidu.com/?tn=65081411_1_oem_dg");

	i1 = s3.find(':');
	if (i1 != string::npos)
		cout << s3.substr(0, i1) << endl;
	else
		cout << "没找到i1" << endl;

	size_t i2 = s3.find('/',i1+3);
	if (i2 != string::npos)
		cout << s3.substr(i1+3, i2 - (i1 + 1))<<endl;
	else
		cout << "没找到i2" << endl;

	cout << s3.substr(i2 + 1) << endl;
	
}

void test_string3()
{
	/*std::string str("Please, replace the vowels in this sentence by asterisks.");
	std::size_t found = str.find_first_not_of("abc");
	while (found != std::string::npos)
	{
		str[found] = '*';
		found = str.find_first_not_of("abcdefg", found + 1);
	}

	std::cout << str << '\n';*/

	std::string str("Please, replace the vowels in this sentence by asterisks.");
	std::size_t found = str.find_first_of("abcd");
	while (found != std::string::npos)
	{
		str[found] = '*';
		found = str.find_first_of("abcd", found + 1);
	}

	std::cout << str << '\n';

}
int main()
{
	test_string3();
	return 0;
}