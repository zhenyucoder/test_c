//class Solution {
//public:
//    bool isValid(string s) {
//        stack<char> st;
//        for (auto ch : s)
//        {
//            if (ch == '(' || ch == '{' || ch == '[')
//                st.push(ch);
//            else
//            {
//                if (!st.empty() && ((st.top() == '(' && ch == ')')
//                    || (st.top() == '[' && ch == ']')
//                    || (st.top() == '{' && ch == '}')))
//                    st.pop();
//                else return false;
//            }
//        }
//        return st.empty();
//    }
//};


//class Solution {
//public:
//    int hash[128];
//    int lengthOfLongestSubstring(string s) {
//        int ret = 0;
//        for (int left = 0, right = 0, n = s.size(); right < n; right++)
//        {
//            char in = s[right];
//            hash[in]++;
//            while (hash[in] > 1)
//            {
//                char out = s[left++];
//                hash[out]--;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int hash1[128] = { 0 }, hash2[128] = { 0 };
//    string minWindow(string s, string t) {
//        for (auto ch : t)
//            hash2[ch]++;
//        int m = t.size();
//        int pos = 0, len = INT_MAX;
//        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//        {
//            char in = s[right];
//            ++hash1[in];
//            if (hash2[in] && hash1[in] <= hash2[in])
//                count++;
//            while (count >= m)
//            {
//                if (right - left + 1 < len)
//                {
//                    pos = left;
//                    len = right - left + 1;
//                }
//                char out = s[left++];
//                hash1[out]--;
//                if (hash2[out] && hash1[out] < hash2[out])
//                    count--;
//            }
//        }
//        return len == INT_MAX ? "" : s.substr(pos, len);
//    }
//};



//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* newhead = new ListNode(), * prev = newhead;
//        int tmp = 0;
//        while (l1 || l2 || tmp)
//        {
//            if (l1)
//            {
//                tmp += l1->val;
//                l1 = l1->next;
//            }
//
//            if (l2)
//            {
//                tmp += l2->val;
//                l2 = l2->next;
//            }
//
//            ListNode* node = new ListNode(tmp % 10);
//            tmp /= 10;
//            prev->next = node;
//            prev = node;
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* next = head->next;
//        head->next = swapPairs(next->next);
//        next->next = head;
//        return next;
//    }
//};




//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = new ListNode(), * prev = newhead;
//        ListNode* cur = head;
//        while (cur && cur->next)
//        {
//            ListNode* next = cur->next, * nnext = next->next;
//            prev->next = next;
//            next->next = cur;
//            prev = cur;
//
//            cur = nnext;
//        }
//        prev->next = cur;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = ReverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return;
//        ListNode* slow = head, * fast = head, * prev = nullptr;
//        while (fast && fast->next)
//        {
//            prev = slow;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        prev->next = nullptr;
//
//        ListNode* cur1 = head;
//        ListNode* cur2 = ReverseList(slow);
//        ListNode* newhead = new ListNode();
//        prev = newhead;
//        while (cur1 && cur2)
//        {
//            ListNode* next1 = cur1->next, * next2 = cur2->next;
//            // 连接
//            prev->next = cur1;
//            cur1->next = cur2;
//
//            // 更新节点
//            prev = cur2;
//            cur1 = next1;
//            cur2 = next2;
//        }
//
//        prev = newhead->next;
//        delete newhead;
//    }
//};



//class Solution {
//public:
//    class cmp
//    {
//    public:
//        bool operator()(ListNode* l1, ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        ListNode* newhead = new ListNode(), * prev = newhead;
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        for (auto& list : lists)
//            if (list)
//                heap.push(list);
//        while (!heap.empty())
//        {
//            ListNode* top = heap.top();
//            heap.pop();
//            if (top->next)
//                heap.push(top->next);
//            prev->next = top;
//            prev = prev->next;
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        int count = 0;
        ListNode* cur = head;
        while (cur)
        {
            count++;
            cur = cur->next;
        }
        count /= k; // 待逆序组数

        cur = head;
        ListNode* newhead = new ListNode(), * prev = newhead, * _prev = cur;
        for (int i = 0; i < count; i++)
        {
            _prev = cur;
            for (int j = 0; j < k; j++)
            {
                ListNode* node = cur;
                cur = cur->next;
                // 头插node
                node->next = prev->next;
                prev->next = node;
            }
            // 更新节点
            prev = _prev;
        }
        _prev->next = cur;

        prev = newhead->next;
        delete newhead;
        return prev;
    }
};