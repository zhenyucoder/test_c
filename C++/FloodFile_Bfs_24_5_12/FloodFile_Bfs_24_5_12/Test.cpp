//class Solution {
//    typedef pair<int, int> PII;
//    int dx[4] = { 1, -1, 0, 0 };
//    int dy[4] = { 0, 0, 1, -1 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int m = image.size(), n = image[0].size();
//        int prev = image[sr][sc];//标记待修改元素值
//        if (prev == color) return image;//边界情况
//        queue<PII> q;
//        q.push({ sr, sc });
//        while (q.size())
//        {
//            auto& [a, b] = q.front();
//            q.pop();
//            image[a][b] = color;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == prev)
//                    q.push({ x, y });
//            }
//        }
//        return image;
//    }
//};




//class Solution {
//public:
//    bool vis[301][301] = { 0 };
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int m = 0, n = 0;
//    int numIslands(vector<vector<char>>& grid) {
//        int ret = 0;
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == '1' && !vis[i][j])
//                {
//                    bfs(grid, i, j);
//                    ret++;
//                }
//            }
//        return ret;
//    }
//
//    void bfs(vector<vector<char>>& grid, int i, int j)
//    {
//        queue<pair<int, int>> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            //vis[a][b] = true;
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//        }
//    }
//};




//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    bool vis[51][51] = { 0 };
//    int m, n;
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (grid[i][j] == 1 && !vis[i][j])
//                    ret = max(ret, bfs(grid, i, j));
//        return ret;
//    }
//
//    int bfs(vector<vector<int>>& grid, int i, int j)
//    {
//        //cout <<i <<"->" << j << endl;
//        int ret = 1;
//        vis[i][j] = true;
//        queue<pair<int, int>> q;
//        q.push({ i, j });
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    ret++;
//                    vis[x][y] = true;
//                }
//            }
//        }
//        return ret;
//    }
//};




class Solution {
public:
    bool vis[201][201] = { 0 };
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    int m, n;
    void solve(vector<vector<char>>& board) {
        m = board.size();
        n = board[0].size();
        //处理边界范围，标记所有边界'0'
        for (int i = 0; i < m; i++)//标记第一列、最后一列
        {
            if (board[i][0] == 'O' && !vis[i][0]) bfs(board, i, 0);
            if (board[i][n - 1] == 'O' && !vis[i][n - 1]) bfs(board, i, n - 1);
        }
        for (int j = 0; j < n; j++)//标记第一行、最后一行
        {
            if (board[0][j] == 'O' && !vis[0][j]) bfs(board, 0, j);
            if (board[m - 1][j] == 'O' && !vis[m - 1][j]) bfs(board, m - 1, j);
        }

        //将其他未标记的'0'改为'X'
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (board[i][j] == 'O' && !vis[i][j])
                    board[i][j] = 'X';
    }

    void bfs(vector<vector<char>>& board, int i, int j)
    {
        queue<pair<int, int>> q;
        vis[i][j] = true;
        q.push({ i, j });
        while (q.size())
        {
            auto [a, b] = q.front();
            q.pop();
            for (int k = 0; k < 4; k++)
            {
                int x = a + dx[k], y = b + dy[k];
                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O' && !vis[x][y])
                {
                    q.push({ x, y });
                    vis[x][y] = true;
                }
            }
        }
    }
};