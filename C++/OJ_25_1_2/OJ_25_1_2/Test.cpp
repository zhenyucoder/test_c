//class Solution {
//public:
//    int ListLen(ListNode* head)
//    {
//        ListNode* cur = head;
//        int len = 0;
//        while (cur)
//        {
//            len++;
//            cur = cur->next;
//        }
//        return len;
//    }
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        ListNode* newhead = new ListNode, * prev = newhead;
//        ListNode* cur = head;
//        int gap = ListLen(head) / k;
//        std::cout << gap;
//        for (int i = 0; i < gap; i++) // ��תk��
//        {
//            ListNode* end = cur;
//            int count = k;
//            while (count--)
//            {
//                ListNode* next = cur->next;
//                cur->next = prev->next;
//                prev->next = cur;
//                cur = next;
//            }
//            prev = end;
//        }
//        prev->next = cur;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    ListNode* partition(ListNode* head, int x) {
//        ListNode* lesshead = new ListNode, * lessPrev = lesshead;
//        ListNode* greaterhead = new ListNode, * greaterPrev = greaterhead;
//        ListNode* cur = head;
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                lessPrev->next = cur;
//                lessPrev = lessPrev->next;
//            }
//            else
//            {
//                greaterPrev->next = cur;
//                greaterPrev = greaterPrev->next;
//            }
//            cur = cur->next;
//        }
//        greaterPrev->next = nullptr;
//        lessPrev->next = greaterhead->next;
//        lessPrev = lesshead->next;
//        delete lesshead;
//        delete greaterhead;
//        return lessPrev;
//    }
//};




//class Solution {
//public:
//    void flatten(TreeNode* root) {
//        if (root == nullptr) return;
//        TreeNode* newhead = new TreeNode, * prev = newhead;
//        stack<TreeNode*> st;
//        st.push(root);
//        while (!st.empty())
//        {
//            TreeNode* cur = st.top();
//            st.pop();
//            s
//                if (cur->right) st.push(cur->right);
//            if (cur->left) st.push(cur->left);
//
//            cur->left = nullptr;
//            prev->right = cur;
//            prev = prev->right;
//
//        }
//        prev = newhead->right;
//        delete newhead;
//        return;
//    }
//};



//class Solution {
//public:
//    bool isSameTree(TreeNode* p, TreeNode* q) {
//        if (p == nullptr && q == nullptr) return true;
//        if (p == nullptr || q == nullptr) return false;
//        if (p->val != q->val) return false;
//        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//    }
//};




//class Solution {
//public:
//    Node* connect(Node* root) {
//        if (root == nullptr) return root;
//        Node* prev = nullptr;
//        queue<Node*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int levelsize = q.size();
//            for (int i = 0; i < levelsize; i++)
//            {
//                Node* cur = q.front();
//                q.pop();
//                if (cur->left) q.push(cur->left);
//                if (cur->right) q.push(cur->right);
//                if (prev == nullptr) prev = cur;
//                else
//                {
//                    prev->next = cur;
//                    prev = prev->next;
//                }
//            }
//            prev->next = nullptr;
//            prev = nullptr;
//        }
//        return root;
//    }
//};





//class Solution {
//public:
//    void _connect(Node*& prev, Node*& cur, Node*& start)
//    {
//        if (prev)
//            prev->next = cur;
//        if (!start)
//            start = cur;
//        prev = cur;
//    }
//
//    Node* connect(Node* root) {
//        if (root == nullptr) return root;
//        Node* start = root;
//        while (start)
//        {
//            Node* prev = nullptr, * nextStart = nullptr;
//            Node* cur = start;
//            while (cur)
//            {
//                if (cur->left) _connect(prev, cur->left, nextStart);
//                if (cur->right) _connect(prev, cur->right, nextStart);
//                cur = cur->next;
//            }
//            start = nextStart;
//        }
//        return root;
//    }
//};




class LRUCache {
public:
    LRUCache(int capacity) {
        _capacity = capacity;
    }

    int get(int key) {
        auto it = _hash.find(key);
        if (it == _hash.end()) return -1;
        auto kv = it->second;
        _dList.remove(kv);
        _dList.push_front(kv);
        return kv.second;
    }

    void put(int key, int value) {
        auto it = _hash.find(key);
        if (it != _hash.end()) _dList.remove(it->second);
        _hash[key] = make_pair(key, value);
        _dList.push_front(make_pair(key, value));
        if (_hash.size() > _capacity)
        {
            auto popValue = _dList.back();
            _dList.pop_back();
            _hash.erase(popValue.first);
        }
    }
private:
private:
    unordered_map<int, pair<int, int>> _hash;
    int _capacity;
    list<pair<int, int>> _dList;
};
