//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        int ret = 0;
//        hash[0] = -1;
//        for (int i = 0, sum = 0; i < nums.size(); i++)
//        {
//            sum += (nums[i] == 0 ? -1 : 1);
//            if (hash.count(sum))
//                ret = max(ret, i - hash[sum]);
//            else
//                hash[sum] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> merge(vector<vector<int>>& intervals) {
//        vector<vector<int>> ret;
//        sort(intervals.begin(), intervals.end());
//        int n = intervals.size();
//        for (int i = 0; i < n;)
//        {
//            int begin = intervals[i][0], end = intervals[i][1], cur = i;
//            while (cur + 1 < n && end >= intervals[cur + 1][0])
//            {
//                begin = min(begin, intervals[cur + 1][0]);
//                end = max(end, intervals[cur + 1][1]);
//                cur++;
//            }
//            ret.push_back({ begin, end });
//            i = cur + 1;
//        }
//        return ret;
//    }
//};


//
//class Solution {
//public:
//    vector<vector<int>> ret;
//    bool vis[12] = { 0 };
//    vector<int> path;
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (vis[i] == false)
//            {
//                vis[i] = true;
//                path.push_back(nums[i]);
//                dfs(nums, pos + 1);
//                path.pop_back();
//                vis[i] = false;
//
//                // ȥ���ظ�
//                while (i + 1 < nums.size() && nums[i + 1] == nums[i])
//                    i++;
//            }
//        }
//    }
//};





