#include "Func.h"


void TestDate1()
{
	Date d1(2023,9,30);
	Date d2;

	d1.Print();
	d2.Print();

	Date d3(2023, 3, 123);
	d3.Print();

	Date d4(d3);
	d4.Print();
}


void TestDate2()
{
	Date d1(2023, 9, 30);
	// 拷贝构造，一个已经存在的对象去初始化另一个要创建对象
	Date d2(d1);

	d1.Print();
	d2.Print();

	Date d3(2023, 8, 13);
	// 赋值，两个已经存在对象进行拷贝
	//d1 = d3;  // d1.operator=(d3)
	d1 = d2 = d3;
	d1.Print();
	d2.Print();
	d3.Print();

	d1 = d1;

	int i, j;
	i = j = 10;
	cout << (j = 10) << endl;
}


void TestDate3()
{
	Date d1;
	 d1 += 61;
	d1.Print();
	Date d3 = d1 + 31;// 拷贝构造
	d3.Print();
}

void TestDate4()
{
	Date d1(2023, 9, 30);
	d1 -= 2000;
	d1.Print();
	d1.Print();

	Date d2(2023, 9, 30);
	Date ret = d2 - 2000;
	ret.Print();

	Date d3(2023,9,30);
	d3 += (-2000);
	d3.Print();
	d3 -= (-3000);
	d3.Print();
}


void TestDate5()
{
	Date d1(2023, 9, 30);
	Date tmp=d1++;
	tmp.Print();
	d1.Print();
	++d1;
	d1.Print();
}


void TestDate6()
{
	Date d1(2023, 9, 30);
	Date d2(2003, 1, 17);
	cout << (d1 - d2) << endl;

	const Date d3(2003, 1, 17);
	d3.Print();
}

int main()
{
	TestDate6();

	return 0;
}