//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> ret(n);
//        ret[0] = 1;
//        for (int i = 1; i < n; i++) // 前缀积
//            ret[i] = ret[i - 1] * nums[i - 1];
//        int rBase = 1;
//        for (int i = n - 1; i >= 0; i--)// 后缀积
//        {
//            ret[i] = rBase * ret[i];
//            rBase *= nums[i]; // 后缀积维护
//        }
//        return ret;
//    }
//};





//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int  n = nums.size();
//        int ret = INT_MIN;
//        vector<int> dp(n + 1, 0);
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(dp[i - 1], 0) + nums[i - 1];
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};





//class Solution {
//public:
//    int maxArea(vector<int>& height) {
//        int ret = 0, n = height.size();
//        for (int left = 0, right = n - 1, prevMax = 0; left < right; )
//        {
//            while (left < right & height[left] <= prevMax)
//                left++;
//            while (left < right && height[right] <= prevMax)
//                right--;
//            prevMax = min(height[left], height[right]);
//            ret = max(ret, prevMax * (right - left));
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    bool check[10] = { 0 };
//    vector<int> path;
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        dfs(nums);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        unordered_map<int, int> hash;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && ++hash[nums[i]] == 1)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums);
//                // 恢复现场
//                check[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//};



//class Solution {
//public:
//    int hash[256] = { 0 };
//    int numJewelsInStones(string jewels, string stones) {
//        for (auto ch : stones)
//            hash[ch]++;
//        int ret = 0;
//        for (auto ch : jewels)
//            ret += hash[ch];
//        return ret;
//    }
//};



class Solution {
public:
    int  BitSum(int x)
    {
        int ret = 0;
        while (x)
        {
            int bit = x % 10;
            ret += bit * bit;
            x /= 10;
        }
        return ret;
    }
    bool isHappy(int n) {
        if (n == 1) return true;
        int slow = n, fast = BitSum(n);
        while (slow != fast)
        {
            if (slow == 1 || fast == 1) return true;
            slow = BitSum(slow);
            fast = BitSum(BitSum(fast));
        }
        return false;
    }
};