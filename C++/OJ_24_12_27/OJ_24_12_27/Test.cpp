class Solution {
public:
    vector<int> singleNumber(vector<int>& nums) {
        int ans = 0;
        for (auto x : nums)
            ans ^= x;
        int i = 0;
        while (1)
        {
            if ((ans >> i) & 1)
                break;
            else i++;
        }

        int a = ans;
        for (auto x : nums)
        {
            if ((x >> i) & 1)
                a ^= x;
        }
        return { a, ans ^ a };
    }
};