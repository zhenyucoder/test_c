#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string.h>

using namespace std;
//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template<class T>
//void PerfectForward(T&& t)
//{
//	Fun(forward<T>(t));
//}
//
//int main()
//{
//	int a = 11;
//	PerfectForward(a);//左值
//	PerfectForward(10);//右值
//
//	const int b = 10;
//	PerfectForward(b);//const左值
//	PerfectForward(move(b));//const 右值
//	return 0;
//}



//namespace mystring
//{
//	class string
//	{
//	public:
//		string(const char* str = "")//默认构造函数
//			:_size(strlen(str))
//			, _capacity(_size)
//		{
//			cout << "string(const char* str = "") ----- 构造函数" << endl;
//			_str = new char[_capacity + 1];
//			strcpy(_str, str);
//		}
//
//		//拷贝构造函数
//		string(const string& s)
//		{
//			string tmp(s._str);
//			swap(tmp);
//			cout << "string(const string& s) ---- 拷贝构造函数 深拷贝" << endl;
//		}
//
//		//移动构造函数
//		string(string&& s)
//		{
//			swap(s);
//			cout << "string(string&& s) ---- 移动构造函数  移动语义" << endl;
//		}
//
//		//拷贝赋值函数
//		string& operator=(const string& s)
//		{
//			string tmp(s);
//			swap(tmp);
//			cout << "string& operator=(const string& s) ---- 拷贝赋值函数 深拷贝" << endl;
//			return *this;
//		}
//
//		//移动赋值函数
//		string& operator=(string&& s)
//		{
//			swap(s);
//			cout << "string& operator=(string&& s) ---- 移动语义" << endl;
//			return s;
//		}
//		void swap(string& s)
//		{
//			std::swap(_str, s._str);
//			std::swap(_size, s._size);
//			std::swap(_capacity, s._capacity);
//		}
//	private:
//		char* _str = nullptr;
//		size_t _size = 0;
//		size_t _capacity = 0;
//	};
//};
//
//template<class T>
//struct ListNode
//{
//	ListNode* _next = nullptr;
//	ListNode* _prev = nullptr;
//	T _data;
//};
//
//
//template<class T>
//class List
//{
//	typedef ListNode<T> Node;
//public:
//	List()
//	{
//		_head = new Node;
//		_head->_next = _head;
//		_head->_prev = _head;
//	}
//	void PushBack(T&& x)
//	{
//		//Insert(_head, x);
//		Insert(_head, std::forward<T>(x));
//	}
//	void PushFront(T&& x)
//	{
//		//Insert(_head->_next, x);
//		Insert(_head->_next, std::forward<T>(x));
//	}
//	void Insert(Node* pos, T&& x)
//	{
//		Node* prev = pos->_prev;
//		Node* newnode = new Node;
//		newnode->_data = std::forward<T>(x); // 关键位置
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//
//	void Insert(Node* pos, const T& x)
//	{
//		Node* prev = pos->_prev;
//		Node* newnode = new Node;
//		newnode->_data = x; // 关键位置
//		// prev newnode pos
//		prev->_next = newnode;
//		newnode->_prev = prev;
//		newnode->_next = pos;
//		pos->_prev = newnode;
//	}
//private:
//	Node* _head = nullptr;
//};
//
//
//int main()
//{
//	List<mystring::string> lt;
//	cout << endl;
//	lt.PushBack("11111111");
//	return 0;
//}


//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p)
//		:_name(p._name)
//		, _age(p._age)
//	{}
//	Person(Person&& p) = default;
//private:
//	mystring::string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	cout << endl;
//
//	Person s2 = s1;
//	cout << endl;
//
//	Person s3 = std::move(s1);
//	cout << endl;
//	return 0;
//}



//template<class ...Args>
//void ShowList(Args... args)
//{
//	cout << sizeof...(args) << endl;//计算参数包参数个数
//}


//递归终止函数
//void _ShowList()
//{
//	cout << endl;
//}
//
////展开函数
//template<class T, class... Args>
//void _ShowList(const T& value, Args... args)
//{
//	cout << value << " ";
//	_ShowList(args...);
//}
//
//template<class... Args>
//void ShowList(Args... args)
//{
//	_ShowList(args...);
//}


template <class T>
int PrintArg(T&& t)
{
	cout << t << " ";
	return 0;
}


template<class... Args>
void ShowList(Args... args)
{
	/*int arr[] = { (PrintArg(args), 0)... };*/
	int arr[] = { PrintArg(args)... };
	cout << endl;
}

int main()
{
	/*ShowList(1);
	ShowList(1, 2);
	ShowList(1.0, 3.0);*/

	ShowList(1, 'a', "string", 5.3);
	return 0;
}