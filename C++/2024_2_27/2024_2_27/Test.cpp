//class Solution {
//public:
//    int tribonacci(int n) {
//        //特殊处理
//        if (n == 0) return 0;
//        if (n == 1 || n == 2) return 1;
//        vector<int> dp(n + 1);//创建dp表
//        //初始化
//        dp[1] = 1, dp[2] = 1;
//        //从左往右填表
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    const int MOD = 1e9 + 7;
//    int waysToStep(int n) {
//        if (n == 1) return 1;
//        else if (n == 2) return 2;
//        else if (n == 3) return 4;
//
//        vector<int> dp(n + 1);
//        //初始化
//        dp[1] = 1, dp[2] = 2, dp[3] = 4;
//        //填表
//        for (int i = 4; i <= n; i++)
//            dp[i] = ((dp[i - 1] + dp[i - 2]) % MOD + dp[i - 3]) % MOD;
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        if (n == 0 || n == 1) return 0;//特殊处理
//        vector<int> dp(n + 1);
//        //填表
//        for (int i = 2; i <= n; i++)
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n);
//        dp[0] = s[0] != '0';
//        //处理边界情况
//        if (n == 1) return dp[0];
//
//        if (s[0] != '0' && s[1] != '0') dp[1]++;
//        int tmp = (s[0] - '0') * 10 + s[1] - '0';
//        if (tmp >= 10 && tmp <= 26) dp[1]++;
//        //填表
//        for (int i = 2; i < n; i++)
//        {
//            if (s[i] != '0')
//                dp[i] += dp[i - 1];
//            int tmp = (s[i - 1] - '0') * 10 + s[i] - '0';
//            if (tmp >= 10 && tmp <= 26)
//                dp[i] += dp[i - 2];
//        }
//        return dp[n - 1];
//    }
//};



//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //初始化
//        dp[1][0] = 1;
//        //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int m = obstacleGrid.size(), n = obstacleGrid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //初始化
//        dp[0][1] = 1;
//        //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                if (obstacleGrid[i - 1][j - 1] == 1)
//                    dp[i][j] = 0;
//                else
//                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int m = frame.size(), n = frame[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + frame[i - 1][j - 1];
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int n = matrix.size();
//        vector<vector<int>> dp(n + 1, vector<int>(n + 2, INT_MAX));
//        //初始化
//        for (int i = 0; i < n + 2; i++)
//            dp[0][i] = 0;
//        //填表
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j - 1], min(dp[i - 1][j], dp[i - 1][j + 1])) + matrix[i - 1][j - 1];
//        int ret = INT_MAX;
//        //查看dp表最一列最小值
//        for (int j = 1; j <= n; j++)
//            ret = min(ret, dp[n][j]);
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int m = grid.size(), n = grid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        dp[1][0] = dp[0][1] = 0;// 初始化
//    //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//        return dp[m][n];
//    }
//};


