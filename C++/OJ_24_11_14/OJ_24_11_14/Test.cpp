//class Solution {
//public:
//    int ret = 0, aim = 0;
//    int findTargetSumWays(vector<int>& nums, int target) { 
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        if ((sum + target) % 2) return 0;
//        aim = (sum + target) / 2;
//        dfs(nums, 0, 0);;
//        return ret;
//    }
//    void dfs(vector<int>& nums, int prevSum, int pos)
//    {
//        if (prevSum == aim)
//            ret++;
//        if (prevSum > aim || pos >= nums.size())
//            return;
//        for (int i = pos; i < nums.size(); i++)
//        {
//            dfs(nums, prevSum + nums[i], i + 1);
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int aim;
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        aim = target;
//        dfs(candidates, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& candidates, int prevSum, int pos)
//    {
//        if (prevSum == aim)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (prevSum > aim || pos >= candidates.size()) return;
//        for (int i = pos; i < candidates.size(); i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, prevSum + candidates[i], i);
//            path.pop_back();
//        }
//    }
//};




//class Solution {
//public:
//    vector<string> ret;
//    string path;
//    vector<string> letterCasePermutation(string s) {
//        dfs(s, 0);
//        return ret;
//    }
//
//    char change(char ch)
//    {
//        if (ch >= 'a' && ch <= 'z')
//            ch -= 32;
//        else
//            ch += 32;
//        return ch;
//    }
//
//    void dfs(string& s, int pos)
//    {
//        if (pos == s.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        char ch = s[pos];
//        path.push_back(ch);
//        dfs(s, pos + 1);
//        path.pop_back();
//
//        if (ch < '0' || ch > '9')
//        {
//            char tmp = change(ch);
//            path.push_back(tmp);
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//};




//class Solution {
//public:
//    bool check[20] = { 0 };
//    int ret = 0;
//    int countArrangement(int n) {
//        dfs(n, 1);
//        return ret;
//    }
//
//    void dfs(int n, int pos)
//    {
//        if (pos > n)
//        {
//            ret++;
//            return;
//        }
//        for (int val = 1; val <= n; val++) // 在pos位置填充可能的val
//        {
//            if (!check[val] && ((val % pos == 0) || (pos % val == 0)))
//            {
//                check[val] = true;
//                dfs(n, pos + 1);
//                check[val] = false;
//            }
//        }
//    }
//};


