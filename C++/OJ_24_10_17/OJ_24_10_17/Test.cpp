//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size(), ret = 0;
//        int left = 0, right = 0;
//        while (left < n)
//        {
//            while (right + 1 < n && prices[right + 1] > prices[right])
//                right++;
//            ret += prices[right] - prices[left];
//            left = right = right + 1;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size(), ret = 0;
//        for (int i = 0; i < n - 1; i++)
//        {
//            if (i + 1 < n && prices[i + 1] > prices[i])
//                ret += prices[i + 1] - prices[i];
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        int n = nums.size(), neg = 0;
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] < 0 && k > neg)
//                nums[i] = -nums[i], neg++;
//            else
//                break;
//        }
//        if (k > neg && (k - neg) % 2)
//        {
//            sort(nums.begin(), nums.end());
//            nums[0] = -nums[0];
//        }
//        int ret = 0;
//        for (auto x : nums)
//            ret += x;
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
//        int n = heights.size();
//        vector<int> index(n);
//        for (int i = 0; i < n; i++)
//            index[i] = i;
//        sort(index.begin(), index.end(), [&](int i, int j) {
//            return heights[i] > heights[j];
//            });
//        vector<string> ret(n);
//        for (int i = 0; i < n; i++)
//            ret[i] = names[index[i]];
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size();
//        vector<int> index(n), ret(n);
//        for (int i = 0; i < n; i++)//��ʼ��
//            index[i] = i;
//
//        sort(nums1.begin(), nums1.end());
//        sort(index.begin(), index.end(), [&](int i, int j)
//            {
//                return nums2[i] < nums2[j];
//            });
//
//        int left = 0, right = n - 1;
//        for (auto x : nums1)
//        {
//            if (x > nums2[index[left]])
//                ret[index[left++]] = x;
//            else
//                ret[index[right--]] = x;
//        }
//        return ret;
//    }
//};



