#pragma once


#include <iostream>
#include <string>
#include <assert.h>

using namespace std;

namespace initateString
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin() const
		{
			return _str;
		}
		const_iterator end() const
		{
			return _str + _size;
		}
		const char* c_str()
		{
			return _str;
		}

		//new char[1]而不使用new char,是因为前者是分配一个字符数组，可以通过下标快速访问和修改数据；而后者则是一个字符空间
		/*string()
			:_str(new char[1]{'\0'})
			, _size(0)
			, _capacity(0)
		{
			_str[0] = '\0';
		}*/

		//权限放大，直接new
		/*string(const char* str)
			:_str(str)
			,_size(strlen(str))
			,_capacity(_size+1)
		{}*/
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new  char[_size + 1];
			strcpy(_str, str);
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_capacity = _size = 0;
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}

		size_t capacity() const
		{
			return _capacity;
		}

		size_t size() const
		{
			return _size;
		}

		//string(const string& s)
		//{
		//	_str = new char[s._capacity + 1];
		//	strcpy(_str, s._str);
		//	_size = s._size;
		//	_capacity = s._capacity;
		//}


		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		//现代写法
		string(const string& s)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			string tmp(s._str);
			swap(tmp);
		}

	/*	string(const string& s)
		{
			string tmp(s._str);
			swap(_str, tmp._str);
			swap(_size, tmp._size);
			swap(_capacity, tmp._capacity);

		}*/

		//string& operator=(const string& s)
		//{
		//	if (this != &s)
		//	{
		//		char* tmp = new char[s._capacity + 1];
		//		strcpy(tmp, s._str);
		//		delete[] _str;
		//		_str = tmp;
		//		_size = s._size;
		//		_capacity = s._capacity;
		//	}
		//	return *this;
		//}
		
		//现代写法 -1 
		/*string& operator=(const string& s)
		{
			if (this != &s)
			{
				string tmp(s);
				swap(tmp);
			}
			return *this;
		}*/

		// -2
		string& operator=(string& s)
		{
			swap(s);
			return *this;
		}


		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}

		void resize(size_t n, char ch = '\0')
		{
			
			if (n <= _size)
			{
				_str[n] = '\0';
				_size = n;
			}
			else
			{
				reserve(n);
				while (_size < n)
				{
					_str[_size] = ch;
					_size++;
				}
				_str[_size] = '\0';
			}
		}

		void push_bach(char ch)
		{
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			strcpy(_str + _size, str);
			_size += len;
		}

		size_t find(char ch, size_t pos = 0)
		{
			for (size_t begin = pos; begin < _size; begin++)
			{
				if (_str[begin] == ch)
				{
					return begin;
				}
			}

			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			const char* p = strstr(_str, str);
			if (p)
			{
				return p - _str;
			}
			else
			{
				return npos;
			}
		}

		string substr(size_t pos = 0, size_t len = npos) const
		{
			string s;
			size_t end = pos + len;
			if (len == npos || pos + len >= _size)
			{
				len = _size - pos;
				end = _size;
			}

			s.reserve(len);
			for (size_t begin = pos; begin < end; begin++)
			{
				s += _str[begin];
			}
			s._str[end] = '\0';
			s._size = len;
			return s;
		}

		 
		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}

			size_t end = _size + 1;//包括'\0'
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			_str[pos] = ch;
			_size++;
		}

		void insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size+len > _capacity)
			{
				reserve(_size+len);
			}

			//第一种挪动数据
			/*int end = _size;
			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				end--;
			}*/

			//第二种
			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end + len - 1] = _str[end - 1];
				end--;
			}
			strncpy(_str+pos, str, len);
			_size += len;
		}

		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			if (len==npos || pos + len >= _capacity)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				size_t begin = pos + len;
				while (begin <= _size)
				{
					_str[begin - len] = _str[begin];
					begin++;
				}
				_size -= len;
			}
		}

		string& operator+=(char ch)
		{
			push_bach(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		bool operator<(const string& s)
		{
			return strcmp(_str, s._str) < 0;
		}

		bool operator==(const string& s)
		{
			return strcmp(_str, s._str) == 0;
		}

		bool operator<=(const string& s)
		{
			return *this < s || *this == s;
		}

		bool operator>(const string& s)
		{
			return !(*this <= s);
		}

		bool operator>=(const string& s)
		{
			return !(*this < s);
		}

		bool operator!=(const string& s)
		{
			return !(*this == s);
		}

		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		//const 静态整型特例
		//const static size_t npos = -1;

		const static size_t npos;
	};
	const size_t string::npos = -1;

	ostream& operator<<(ostream& out, const string& s)
	{
		/*for (size_t i = 0; i < s.size(); i++)
		{
			out << s[i];
		}*/

		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}

	istream& operator>>(istream& in, string& s)
	{
		s.clear();

		char buff[129];
		size_t i = 0;

		char ch;
		ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 128)
			{
				buff[i] = '\0';
				s += buff;
				i = 0;
			}
			ch = in.get();
		}

		if (i != 0)
		{
			buff[i] = '\0';
			s += buff;
		}
		return in;
	}


	void testString1()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		string s2;
		cout << s2.c_str() << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;

		for (auto& ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void testString2()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		s1.push_bach('@');
		cout << s1.c_str() << endl;

		s1 += '!';
		s1 += "123456789";
		cout << s1.c_str() << endl;
		s1.append("hello Linux");
		cout << s1.c_str() << endl;

	}

	void testString3()
	{
		string s1("hello world");
		string s2("hello world");
		cout << (s1 < s1) << endl;
		cout << (s1 == s1) << endl;

		/*s1[0] = '0';
		cout << (s1 < s2) << endl;
		cout << (s1 > s2) << endl;*/

		s1.insert(0, '1');
		s1.insert(1, '9');
		cout << s1.c_str() << endl;

		s2.insert(0, "123456789");
		cout << s2.c_str() << endl;

		s2.erase(0, 3);
		cout << s2.c_str() << endl;
		s2.erase(10, 3);
		cout << s2.c_str() << endl;
		s2.erase(7);
		cout << s2.c_str() << endl;
	}

	void testString4()
	{
		string s1("hello world");
		cout << s1 << endl;

		cin >> s1;
		cout << s1 << endl;

		//char c1, c2;
		//cin >> c1 >> c2;
		//cout << c1 << c2 << endl;
	}

	void testString5()
	{
		string s1("hello world");
		s1.resize(5, 'x');
		cout << s1 << endl;
		s1.resize(20, 'A');
		cout << s1 << endl;

		string s2("hello Linux 111 hello C++!!");
		cout << (s2.find('C')) << endl;
		cout << (s2.find("Linux")) << endl;

		string s = s2.substr(0, 13);
		cout << s << endl;
 	}

	void testString6()
	{
		string s1("hello Linux 111 hello C++!!");
		string s2 = s1;
		cout << s1 << endl;
		cout << s2 << endl;

		/*string s3("xxxxxxxxxxxxxxxxxxxxxxx");
		s1 = s3;
		cout << s1 << endl;*/
	}

	void testString7()
	{
		string s1("xxxxxxxxxxxxxxxxxxxxxxxxxxx");
		string s2(s1);
		cout << (void*)s1.c_str() << endl;
		cout << (void*)s2.c_str() << endl;
	}
};