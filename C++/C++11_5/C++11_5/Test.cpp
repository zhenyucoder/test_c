#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <functional>
using namespace std;

//struct Goods
//{
//	string _name;//名字
//	double _price;//价格
//	int _evaluate; // 评价
//	Goods(const char* str, double price, int evaluate)
//		:_name(str)
//		, _price(price)
//		, _evaluate(evaluate)
//	{}
//};

//struct ComparePriceless
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._price < g2._price;
//	}
//};
//
//struct ComparePricegreater
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._price > g2._price;
//	}
//};
//struct CompareEvaluateless
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._evaluate < g2._evaluate;
//	}
//}; 
//struct CompareEvaluategreater
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._evaluate > g2._evaluate;
//	}
//};
//
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	/*sort(v.begin(), v.end(), ComparePriceless());
//	sort(v.begin(), v.end(), ComparePricegreater());
//	sort(v.begin(), v.end(), CompareEvaluateless());
//	sort(v.begin(), v.end(), CompareEvaluategreater());*/
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) ->bool { return g1._price < g2._price; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) { return g1._price > g2._price; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) { return g1._evaluate < g2._evaluate; });
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) { return g1._evaluate < g2._evaluate; });
//
//
//
//	return 0;
//}



//int main()
//{
//	int x = 10, y = 32, sum = 18;
//	//auto f = [&x, &y]()mutable {
//	//	int tmp = x;
//	//	x = y;
//	//	y = tmp;
//	//	cout << &x << ":" << &y << endl << endl;
//	//};
//	//cout << x << ":" << y << endl;
//	//cout << &x << ":" << &y << endl << endl;
//
//	//f();
//	////lambda本质会转化成一个类，<class_uuid>，本质上是一个匿名函数对象
//	//cout << typeid(f).name() << endl;
//	//cout << x << ":" << y << endl;
//
//	//auto f = [=, &sum](int mul) mutable { sum = x * mul + y; x = sum;  cout << x << endl; return sum; };
//	auto S = [=] {return x + y + sum; };
//
//	auto f1 = [&, x](int c) mutable {
//		x++; sum += c * x + y; 
//		cout << &x << ":" << &sum << endl << endl;
//	};
//	//cout << f(3) << endl;
//	//cout << S() << endl;
//	cout << x << ":" << sum << endl; 
//	cout << &x << ":" << &sum << endl << endl;
//	f1(2); 
//	cout << x << ":" << sum << endl;
//	cout << &x << ":" << &sum << endl << endl;
//	return 0;
//}



//struct Date
//{
//	Date()
//	{
//		//auto f = [=] {return year; };
//		auto f = [=] {return x; };//只能捕捉全局遍历，父作用域（包含this指针）
//		cout << f() << endl;
//	}
//
//	int year = 2024;
//	int monday = 4;
//	int day = 21;
//};
//
//int main()
//{
//	static int x = 10;
//	int sum = 10;
//	Date d;
//	return 0;
//}



//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//
//
//int main()
//{
//	double rate = 0.91;
//	Rate r1(rate);
//	r1(10000000.43, 2);;
//
//	auto f = [=](double money, int year) {return money * rate * year; };//底层就是仿函数，生成一个类和对应的operator()
//	f(10000000.43, 2);
//
//	return 0;
//}




//void swap_fun(int& x, int& y)
//{
//	int tmp = x;
//	x = y;
//	y = tmp;
//}
//
//struct Swap
//{
//	void operator()(int& x, int& y)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	}
//};
//
//
//
//int main()
//{
//	int x = 10, y = 20;
//	auto swaplambda = [](int& x, int& y) {
//		int tmp = x;
//		x = y;
//		y = tmp; 
//	};
//
//	function<void(int&, int&)> func1 = swap_fun;
//	func1(x, y);
//	cout << x << ":" << y << endl;
//
//	function<void(int&, int&)> func2 = Swap();
//	func2(x, y);
//	cout << x << ":" << y << endl;
//
//	function<void(int&, int&)> func3 = swaplambda;
//	func3(x, y);
//	cout << x << ":" << y << endl << endl;
//
//	map<string, function<void(int&, int&)>> m = {
//		{"函数指针", swap_fun},
//		{"仿函数", Swap()},
//		{"lambda", func3}
//	};
//
//	m["函数指针"](x, y);
//	cout << x << ":" << y << endl;
//	m["仿函数"](x, y);
//	cout << x << ":" << y << endl;
//	m["lambda"](x, y);
//	cout << x << ":" << y << endl;
//	return 0;
//}


//class Solution {
//public:
//    int evalRPN(vector<string>& tokens) {
//        map<string, function<int(int, int)>> m = {
//            {"+", [](int x, int y) {return x + y; }},
//            {"-", [](int x, int y) {return x - y; }},
//            {"*", [](int x, int y) {return x * y; }},
//            {"/", [](int x, int y) {return x / y; }},
//        };
//
//        stack<int> s;
//        for (auto& str : tokens)
//        {
//            if (m.count(str))
//            {
//                int x = s.top();
//                s.pop();
//                int y = s.top();
//                s.pop();
//                s.push(m[str](y, x));
//            }
//            else
//            {
//                s.push(stoi(str));
//            }
//        }
//        return s.top();
//    }
//};



//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//
//
//int main()
//{
//	function<int(int, int)> f = Plus::plusi;
//	cout << f(10, 32) << endl;
//
//	Plus p;
//	function<double(Plus*, double, double)> f1 = &Plus::plusd;//成员函数取地址比较特殊，需要加&
//	cout << f1(&p, 3.2, 3.1) << endl;
//	//cout << f1(&Plus(), 3.2, 3.1) << endl; 不能用匿名对象，匿名对象是一个临时遍历右值
//
//	//function<double(const Plus&, double, double)> f2 = &Plus::plusd;//error！
//	function<double(Plus, double, double)> f2 = &Plus::plusd;
//	cout << f2(Plus(), 4.23, 32.1) << endl;//编译器特殊处理
//	return 0;
//}





class Mul
{
public:
	int mul(int a, int b)
	{
		return a * b;
	}
};

int sub(int a, int b)
{
	return a - b;
}

int main()
{
	/*function<int(int, int)> f1 = bind(sub, placeholders::_1, placeholders::_2);
	cout << f1(10, 5) << endl;
	function<int(int, int)> f2 = bind(sub, placeholders::_2, placeholders::_1);
	cout << f2(20, 10) << endl;

	function<int(int)> f3 = bind(sub, 20, placeholders::_1);
	cout << f3(5) << endl;
	function<int(int)> f4 = bind(sub, placeholders::_1, 20);
	cout << f4(5) << endl;*/

	//function<int(int, int)> f = bind(&Mul::mul, Mul(), placeholders::_1, placeholders::_2);//编译器特殊处理，Mul()等价于指针
	Mul m;
	auto f = bind(&Mul::mul, &m, placeholders::_1, placeholders::_2);
	cout << typeid(f).name() << endl;
	cout << f(3, 5) << endl;
	return 0;
}