// struct ListNode {
//    int val;
//    ListNode* next;
//    ListNode() : val(0), next(nullptr) {}
//    ListNode(int x) : val(x), next(nullptr) {}
//    ListNode(int x, ListNode* next) : val(x), next(next) {}
//    
//};
//
//
//
// class Solution {
// public:
//     ListNode* FindMiddleNode(ListNode* head)
//     {
//         ListNode* fast = head, * slow = head, * prev = slow;
//         while (fast && fast->next)
//         {
//             prev = slow;
//             fast = fast->next->next;
//             slow = slow->next;
//         }
//         prev->next = nullptr;
//         return slow;
//     }
//
//     ListNode* reverseList(ListNode* head)
//     {
//         ListNode* newhead = new ListNode;//增加虚拟节点
//         ListNode* cur = head;
//         while (cur)
//         {
//             ListNode* tmp = cur;
//             cur = cur->next;
//             //头插
//             tmp->next = newhead->next;
//             newhead->next = tmp;
//         }
//         cur = newhead->next;
//         delete newhead;
//         return cur;
//     }
//
//     void CombineList(ListNode*& head1, ListNode*& head2)
//     {
//         ListNode* newhead = new ListNode, * cur = newhead;//增加虚拟节点
//         ListNode* cur1 = head1, * cur2 = head2;
//         while (cur1 || cur2)
//         {
//             if (cur1)
//             {
//                 cur->next = cur1;
//                 cur = cur->next;
//                 cur1 = cur1->next;
//             }
//             if (cur2)
//             {
//                 cur->next = cur2;
//                 cur = cur->next;
//                 cur2 = cur2->next;
//             }
//         }
//         delete newhead;
//     }
//
//     void reorderList(ListNode* head) {
//         if (head->next == nullptr)
//             return;
//         //查找最中间节点
//         ListNode* MiddleNode = FindMiddleNode(head);
//         //反转MiddleNode后续节点
//         ListNode* cur2 = reverseList(MiddleNode);
//         // while(tmp1)
//         // {
//         //     cout<<tmp1->val<<":";
//         //     tmp1 = tmp1->next;
//         // }
//         // cout<<endl;
//         // while(tmp2)
//         // {
//         //     cout<<tmp2->val<<":";
//         //     tmp2 = tmp2->next;
//         // }
//         // cout<<endl;
//         //合并双链表
//         CombineList(head, cur2);
//     }
// };
//
//int main()
//{
//    int arr[] = { 1,2,3,4 };
//    ListNode* newhead = new ListNode, *cur = newhead;
//    for (auto x : arr)
//    {
//        ListNode* node = new ListNode(x);
//        cur->next = node;
//        cur = cur->next;
//    }
//    cur = newhead->next;
//    delete newhead;
//    reorderList(cur);
//    return 0;
//}



class Solution {
public:
    struct cmp
    {
        bool operator()(const ListNode* l1, const ListNode* l2)
        {
            return l1->val > l2->val;
        }
    };
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        //创建小根堆
        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
        //所有头节点进入小根堆
        for (auto l : lists)
            if (l)
                heap.push(l);
        //合并k个升序链表
        ListNode* newnode = new ListNode, * prev = newnode;
        while (!heap.empty())
        {
            auto tmp = heap.top();
            heap.pop();
            //链接
            prev->next = tmp;
            prev = prev->next;
            //新增
            if (tmp->next)
                heap.push(tmp->next);
        }
        prev = newnode->next;
        delete newnode;
        return prev;
    }
};