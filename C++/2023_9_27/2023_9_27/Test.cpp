#include <iostream>
#include <assert.h>
using namespace std;

//class date
//{
//public:
//	date(int year=1, int month=2, int day=23)
//	{
//		cout << this << endl;
//
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	date(const date& d)//像栈一样的会多次释放，
//	{
//		cout << "date(date& d)" << endl;
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//
//	void print()
//	{
//		cout << this << endl;
//
//		cout << _year << "/" << _month << "/" << _day << endl;
//	}
//
//	~date()
//	{
//		cout << "~date()" << endl;;
//	}
//private:
//	int _year=1;   
//	int _month=1;
//	int _day=1;
//};
//
//
//class Stack
//{
//public:
//
//	Stack(int  n = 4)
//	{
//		cout << "Stack(int  n = 4)" << endl;
//		if (n == 0)
//		{
//			_a = nullptr;
//			_top = _capacity = 0;
//		}
//		else
//		{
//			int* tmp = (int*)malloc(sizeof(int) * n);
//			if (tmp == nullptr)
//			{
//				cout << "malloc fail" << endl;
//				exit(-1);
//			}
//			_a = tmp;
//			_top = 0;
//			_capacity = n;
//		}
//	}
//
//	Stack(const Stack& s)
//	{
//		int* _a = (int*)malloc(sizeof(int) * s._capacity);
//		if (_a == nullptr)
//		{
//			cout << "malloc fail" << endl;
//			exit(-1);
//		}
//
//		memcpy(_a, s._a, sizeof(int) * s._capacity);
//		_top = s._top;
//		_capacity = s._capacity;
//
//		
//	}
//
//	void Push(int x)
//	{
//		if (_top == _capacity)
//		{
//			size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
//			int* tmp = (int*)realloc(_a, sizeof(int) * newcapacity);
//			if (tmp == nullptr)
//			{
//				cout << "malloc fail" << endl;
//				exit(-1);
//			}
//
//			if (_a == tmp)
//			{
//				cout << "原地" << endl;
//			}
//			else
//			{
//				cout << "异地" << endl;
//			}
//
//			_a = tmp;
//			_capacity = newcapacity;
//		}
//
//		_a[_top++] = x;
//	}
//
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//
////private:
//	// 成员变量
//	int*_a;
//	int _top;
//	int _capacity;
//};
//
//
//void func1(date d1)
//{
//	d1.print();
//}
//
////void func2(Stack s)//对_a多次释放，崩溃
////{
////	s.Push(1);
////	s.Push(2);
////	s.Push(3);
////}
//
//void func2(Stack& s) //引用可以解决问题，但形参修改会改变实参
//{
//	s.Push(1);
//	s.Push(2);
//	s.Push(3);
//}
//
//
//int main()
//{
//	date d1(1, 1, 1);
//	func1(d1);
//
//	Stack st1(4);
//	func2(st1);
//
//	date d2(d1);
//	d2.print();
//	/*date d2 = d1;*///也是拷贝构造
//
//	Stack st2(st1);
//	return 0;
//}




class date
{
public:
	date(int year=1, int month=2, int day=23)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	void print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}

	//d1<d2
	//d1.operator<d2
	bool operator<(const date& d2)
	{
		if (_year < d2._year)
		{
			return true;
		}
		else if (_year == d2._year && _month < d2._month)
		{
			return true;
		}
		else if (_year == d2._year && _month == d2._month && _day < d2._day)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool operator==(const date& d2)
	{
		return _year == d2._year && _month == d2._month && _day == d2._day;
	}

	bool operator<=(const date& d2)
	{
		return (*this) < d2 && *this == d2;
	}

	bool operator>(const date& d2)
	{
		return !(*this <= d2);
	}

	bool operator>=(const date& d2)
	{
		return !(*this < d2);
	}

	bool operator!=(const date& d2)
	{
		return !(*this == d2);
	}

	int GetMonthDay(int year, int month)
	{
		int monthArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) && (year % 400) == 0))
		{
			return 29;
		}
		return monthArray[month];
	}

	date& operator+=(int day)
	{
		_day += day;
		while (day > GetMonthDay(_year, _month))
		{
			day -= GetMonthDay(_year, _month);
			_month += 1;
			if (_month == 13)
			{
				_year += 1;
				_month = 1;
			}
		}
		return *this;//出作用域*this还存在，使用引用返回。值返回，不仅需要拷贝数据，还要调用拷贝构造
	}

	date operator+(int day)
	{
		date tmp(*this);
		tmp += day;
		return tmp;
		//tmp._day += day;
		//while (tmp._day > GetMonthDay(_year, _month))
		//{
		//	tmp._day -= GetMonthDay(_year, _month);
		//	tmp._month += 1;
		//	if (tmp._month == 13)
		//	{
		//		tmp._year += 1;
		//		tmp._month = 1;
		//	}
		//}
		//return tmp;//出了作用域引用不在了
	}

private:
	int _year=1;   
	int _month=1;
	int _day=1;
};

//bool Dateless(const date& d1, const date& d2)//传值传参需要拷贝数据，还需要拷贝构造
//运算符重载
//bool operator<(const date& d1, const date& d2)
//{
//	if (d1._year < d2._year)
//	{
//		return true;
//	}
//	else if (d1._year == d2._year && d1._month < d2._month)
//	{
//		return true;
//	}
//	else if (d1._year == d2._year && d1._month == d2._month && d1._day < d2._day)
//	{
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}

int main()
{
	date d1(2023, 1, 23);
	date d2(2023, 5, 6);
	cout << (d1 < d2) << endl;
	cout << (d1.operator<(d2)) << endl;

	//date& ret = d1 += 2;//拷贝构造
	//ret.print();

	date ret = d1 +2;//拷贝构造
	ret.print();
	return 0;
}