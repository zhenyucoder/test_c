//int main() {
//    int n, q;
//    while (cin >> n >> q)
//    {
//        int arr[n];
//        for (int i = 0; i < n; i++)
//            cin >> arr[i];
//        vector<long long> dp(n + 1);
//        for (int i = 1; i <= n; i++)
//            dp[i] = dp[i - 1] + arr[i - 1];
//        while (q--)
//        {
//            int l = 0, r = 0;
//            cin >> l >> r;
//            cout << (dp[r] - dp[l - 1]) << endl;
//        }
//        return 0;
//    }
//}


//int main() {
//    int n = 0, m = 0, q = 0;
//    while (cin >> n >> m >> q)
//    {
//        //输入数据
//        vector<vector<long long>> arr(n + 1, vector<long long>(m + 1));
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= m; j++)
//                cin >> arr[i][j];
//        //创建二维前缀和
//        vector<vector<long long>> dp(n + 1, vector<long long>(m + 1));
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= m; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + arr[i][j];
//        //q次输入询问下标，和结果
//        while (q--)
//        {
//            int x1, y1, x2, y2;
//            cin >> x1 >> y1 >> x2 >> y2;
//            cout << dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
//        }
//    }
//    return 0;
//}



//class Solution {
//public:
//    int pivotIndex(vector<int>& nums) {
//        int n = nums.size();
//        //f[i]:[1, i -1]的和； g[i]:[i + 1, n]的和
//        vector<int> f(n), g(n);
//        for (int i = 1; i < n; i++)
//            f[i] = f[i - 1] + nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            g[i] = g[i + 1] + nums[i + 1];
//        //查找是否存在中心下标
//        for (int i = 0; i < n; i++)
//            if (f[i] == g[i])
//                return i;
//        //不存在
//        return -1;
//
//    }
//};


//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        //f[i]:[]
//        vector<int> f(n), g(n);
//        //初始化
//        f[0] = g[n - 1] = 1;
//        for (int i = 1; i < n; i++)
//            f[i] = f[i - 1] * nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            g[i] = g[i + 1] * nums[i + 1];
//        //除原数组自身的其他元素乘机
//        vector<int> ret(n);
//        for (int i = 0; i < n; i++)
//            ret[i] = f[i] * g[i];
//        return ret;
//    }
//};



//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;//统计前缀和各值出现次数
//        int n = nums.size();
//        hash[0] = 1;//特殊处理：区间和为k
//        int ret = 0, sum = 0;
//        for (auto num : nums)
//        {
//            sum += num;
//            if (hash.count(sum - k))
//                ret += hash[sum - k];//统计个数
//            hash[sum]++;//入哈希
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int subarraysDivByK(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        hash[0 % k] = 1;
//        int ret = 0, sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;//统计前缀和
//            int r = (sum % k + k) % k;//余数纠正
//            if (hash.count(r))
//                ret += hash[r];//统计次数
//            hash[r]++;//进入哈希
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        hash[0] = -1;//默认有一个前缀和为0情况
//        int ret = 0, sum = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            sum += nums[i] == 0 ? -1 : 1;
//            if (hash.count(sum))
//                ret = max(ret, i - hash[sum]);
//            else
//                hash[sum] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
//        int m = mat.size(), n = mat[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));//二维前缀和数组
//        //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + mat[i - 1][j - 1];
//        vector<vector<int>> ret(m, vector<int>(n));
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//            {
//                //代求前缀和的对角线下标
//                int x1 = max(0, i - k) + 1, y1 = max(0, j - k) + 1;
//                int x2 = min(m - 1, i + k) + 1, y2 = min(n - 1, j + k) + 1;
//                //计算两下表对应区域的前缀和
//                ret[i][j] = dp[x2][y2] - dp[x2][y1 - 1] - dp[x1 - 1][y2] + dp[x1 - 1][y1 - 1];
//            }
//        return ret;
//    }
//};


//class Solution {
//public:
//    string modifyString(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == '?')//替换
//            {
//                for (char ch = 'a'; ch <= 'z'; ch++)
//                {
//                    if ((i == 0 || ch != s[i - 1]) && (i == n - 1 || ch != s[i + 1]))
//                    {
//                        s[i] = ch;//发生替换
//                        break;
//                    }
//                }
//            }
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& timeSeries, int duration) {
//        int n = timeSeries.size(), ret = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (i + 1 < n && timeSeries[i + 1] - timeSeries[i] < duration)
//                ret += timeSeries[i + 1] - timeSeries[i];
//            else
//                ret += duration;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    string convert(string s, int numRows) {
//        if (s.size() == 1 || numRows == 1)
//            return s;
//        string ret;
//        int n = s.size(), d = 2 * (numRows - 1);
//        cout << d << endl;
//        for (int i = 0; i < n; i += d)//处理第一行
//            ret += s[i];
//        for (int i = 1; i < numRows - 1; i++)//处理中间的n - 2行
//        {//依次处理每一行
//            for (int j = i, k = d - j; j < n || k < n; j += d, k += d)
//            {
//                if (j < n)
//                    ret += s[j];
//                if (k < n)
//                    ret += s[k];
//            }
//        }
//        for (int i = numRows - 1; i < n; i += d)//最后一行
//            ret += s[i];
//        return ret;
//    }
//};


//class Solution {
//public:
//    string countAndSay(int n) {
//        string ret = "1";
//        for (int i = 1; i < n; i++)//解释n-1次即可
//        {
//            string tmp;
//            int len = ret.size();
//            for (int left = 0, right = 0; left < len && right < len; )
//            {
//                while (right < len && ret[left] == ret[right])
//                    right++;
//                tmp += to_string(right - left) + ret[left];
//                left = right;
//            }
//            ret = tmp;
//        }
//        return ret;
//    }
//}


class Solution {
public:
    int minNumberOfFrogs(string croakOfFrogs) {
        string t = "croak";
        int n = t.size();
        vector<int> hash(n);//模拟哈希表
        unordered_map<char, int> index;//映射字符对应哈希中的下标
        for (int i = 0; i < n; i++)
        {
            index[t[i]] = i;
        }

        for (auto ch : croakOfFrogs)
        {
            if (ch == 'c')
            {
                if (hash[n - 1] != 0)
                    hash[n - 1]--;
                hash[0]++;
            }
            else
            {
                if (hash[index[ch] - 1] != 0)
                {
                    hash[index[ch] - 1]--;
                    hash[index[ch]]++;
                }
                else
                {
                    return -1;//错误
                }
            }
        }
        for (int i = 0; i < n - 1; i++)
            if (hash[i])
                return -1;
        return hash[n - 1];
    }
};