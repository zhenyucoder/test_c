//class Solution {
//public:
//    int lastStoneWeight(vector<int>& stones) {
//        priority_queue<int, vector<int>> pq(stones.begin(), stones.end());
//        while (pq.size() > 1)
//        {
//            int first = pq.top(); pq.pop();
//            int second = pq.top(); pq.pop();
//            if (second != first)
//                pq.push(first - second);
//        }
//        return pq.size() ? pq.top() : 0;
//    }
//};



//class KthLargest {
//    priority_queue<int, vector<int>, std::greater<int>> pq;
//    int _k;
//public:
//    KthLargest(int k, vector<int>& nums) {
//        _k = k;
//        for (auto x : nums)
//        {
//            pq.push(x);
//            if (pq.size() > _k)
//                pq.pop();
//        }
//    }
//
//    int add(int val) {
//        pq.push(val);
//        if (pq.size() > _k)
//            pq.pop();
//        return pq.top();
//    }
//};




//class Solution {
//    typedef pair<string, int> PSI;
//    struct Cmp
//    {
//        bool operator()(const PSI& p1, const PSI& p2)
//        {
//            if (p1.second == p2.second)
//                return p1.first < p2.first;
//            else
//                return p1.second > p2.second;
//        }
//    };
//
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        //统计单词频率
//        unordered_map<string, int> hash;
//        for (auto str : words)
//            hash[str]++;
//        //topk
//        priority_queue<PSI, vector<PSI>, Cmp> pq;
//        for (auto psi : hash)
//        {
//            pq.push(psi);
//            if (pq.size() > k)
//                pq.pop();
//        }
//        //输出结果
//        vector<string> ret(k);
//        int i = k - 1;
//        while (pq.size())
//        {
//            ret[i--] = pq.top().first;
//            pq.pop();
//        }
//        return ret;
//    }
//};




class MedianFinder {
public:
    priority_queue<int> left;
    priority_queue<int, vector<int>, std::greater<int>> right;
    MedianFinder() {
    }

    void addNum(int num) {
        if (left.size() == right.size())
        {
            if (left.empty() || num <= left.top())
                left.push(num);
            else
            {
                right.push(num);
                left.push(right.top());
                right.pop();
            }
        }
        else//left.size() = right.size() + 1
        {
            if (num <= left.top())
            {
                left.push(num);
                right.push(left.top());
                left.pop();
            }
            else
            {
                right.push(num);
            }
        }
    }

    double findMedian() {
        return (left.size() == right.size()) ? (left.top() + right.top()) / 2.0 : left.top();
    }
};