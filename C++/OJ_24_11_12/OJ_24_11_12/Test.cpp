//class Solution {
//public:
//    vector<vector<string>> ret;
//    vector<string> path;
//    bool CheckCol[10], CheckDig1[20], CheckDig2[20];
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//            path[i].append(n, '.');
//        dfs(n, 0);
//        return ret;
//    }
//
//    void dfs(int n, int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            if (CheckCol[i] == false && CheckDig1[pos - i + n] == false && CheckDig2[pos + i] == false)
//            {
//                CheckCol[i] = CheckDig1[pos - i + n] = CheckDig2[pos + i] = true;
//                path[pos][i] = 'Q';
//                dfs(n, pos + 1);
//                path[pos][i] = '.';
//                CheckCol[i] = CheckDig1[pos - i + n] = CheckDig2[pos + i] = false;
//            }
//        }
//    }
//};



class Solution {
public:
    int ret = 0;
    bool vis[16][16];
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    int m, n;
    int getMaximumGold(vector<vector<int>>& grid) {
        m = grid.size(), n = grid[0].size();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (grid[i][j])
                    ret = max(ret, dfs(grid, i, j));
            }
        }
        return ret;
    }

    int dfs(vector<vector<int>>& grid, int i, int j)
    {
        vis[i][j] = true;
        int ans = 0;
        for (int k = 0; k < 4; k++)
        {
            int x = dx[k] + i, y = dy[k] + j;
            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y])
            {
                ans = max(ans, dfs(grid, x, y));
            }
        }
        vis[i][j] = false;
        return ans + grid[i][j];
    }
};