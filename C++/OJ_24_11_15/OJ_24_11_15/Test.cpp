//class Solution {
//public:
//    vector<vector<string>> ret;
//    vector<string> path;
//    bool CheckCol[10], CheckDig1[20], CheckDig2[20];
//    vector<vector<string>> solveNQueens(int n) {
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//            path[i].append(n, '.');
//        dfs(n, 0);
//        return ret;
//    }
//
//    void dfs(int n, int row) // 开始第row行可能结果
//    {
//        if (row == n)
//        {
//            ret.push_back(path);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (!CheckCol[i] && !CheckDig1[i + row] && !CheckDig2[n + i - row])
//            {
//                CheckCol[i] = CheckDig1[i + row] = CheckDig2[n + i - row] = true;
//                path[row][i] = 'Q';
//                dfs(n, row + 1);
//                // 恢复现场
//                path[row][i] = '.';
//                CheckCol[i] = CheckDig1[i + row] = CheckDig2[n + i - row] = false;
//            }
//        }
//    }
//};



//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool block[3][3][10];
//    bool isValidSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    if (row[i][num] || col[j][num] || block[i / 3][j / 3][num]) return false;
//                    row[i][num] = col[j][num] = block[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        return true;
//    }
//};




//class Solution {
//public:
//    bool row[9][10];
//    bool col[9][10];
//    bool grid[3][3][10];
//    void solveSudoku(vector<vector<char>>& board) {
//        //保存初始数独信息
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//        dfs(board);
//    }
//
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int k = 1; k <= 9; k++)
//                    {
//                        if (!row[i][k] && !col[j][k] && !grid[i / 3][j / 3][k])
//                        {
//                            row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = true;
//                            board[i][j] = '0' + k;
//                            if (dfs(board) == false)
//                            {
//                                row[i][k] = col[j][k] = grid[i / 3][j / 3][k] = false;
//                                board[i][j] = '.';
//                                continue;
//                            }
//                            break;
//                        }
//
//                    }
//                    if (board[i][j] == '.')
//                        return false;
//                }
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    bool vis[7][7];
//    int m, n;
//    bool exist(vector<vector<char>>& board, string word) {
//        m = board.size(), n = board[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (board[i][j] == word[0])
//                {
//                    vis[i][j] = true;
//                    if (dfs(board, i, j, word, 1)) return true;
//                    vis[i][j] = false;
//                }
//            }
//        }
//        return false;
//    }
//
//
//    bool dfs(vector<vector<char>>& board, int i, int j, string& word, int pos)
//    {
//        if (pos == word.size()) return true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = dx[k] + i, y = dy[k] + j;
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && board[x][y] == word[pos])
//            {
//                vis[x][y] = true;
//                if (dfs(board, x, y, word, pos + 1)) return true;
//                vis[x][y] = false;
//            }
//        }
//        return false;
//    }
//};



//class Solution {
//public:
//    int ret = 0;
//    bool vis[16][16];
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int m, n;
//    int getMaximumGold(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j])
//                    ret = max(ret, dfs(grid, i, j));
//            }
//        }
//        return ret;
//    }
//
//    int dfs(vector<vector<int>>& grid, int i, int j)
//    {
//        vis[i][j] = true;
//        int ans = 0;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = dx[k] + i, y = dy[k] + j;
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y])
//            {
//                ans = max(ans, dfs(grid, x, y));
//            }
//        }
//        vis[i][j] = false;
//        return ans + grid[i][j];
//    }
//};




class Solution {
public:
    int ret = 0, m, n;
    bool vis[21][21];
    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
    int step, sx, sy;
    int uniquePathsIII(vector<vector<int>>& grid) {
        m = grid.size(), n = grid[0].size();

        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (grid[i][j] == 1) sx = i, sy = j, vis[i][j] = true;
                else if (grid[i][j] == 0)step++;
        dfs(grid, sx, sy, 0);
        return ret;
    }

    void dfs(vector<vector<int>>& grid, int i, int j, int num)
    {
        if (grid[i][j] == 2)
        {
            if (num == step) ret++;
            return;
        }

        for (int k = 0; k < 4; k++)
        {
            int x = dx[k] + i, y = dy[k] + j;
            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y] != -1)
            {
                vis[x][y] = true;
                dfs(grid, x, y, num + 1);
                vis[x][y] = false;
            }
        }
    }
};