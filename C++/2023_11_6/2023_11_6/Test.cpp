//class Solution {
//public:
//    bool isletter(char ch)
//    {
//        if (ch >= 'a' && ch <= 'z')
//            return true;
//
//        if (ch >= 'A' && ch <= 'Z')
//            return true;
//
//        return false;
//    }
//
//    string reverseOnlyLetters(string s) {
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            while (left < right && !isletter(s[left]))
//            {
//                left++;
//            }
//            while (right > left && !isletter(s[right]))
//            {
//                right--;
//            }
//            swap(s[left], s[right]);
//            left++;
//            right--;
//        }
//        return s;
//    }
//};


//int main() {
//    string line;
//    getline(cin, line);
//    size_t reti = line.rfind(' ');
//    if (reti != string::npos)
//    {
//        cout << line.size() - reti - 1 << endl;
//    }
//    else
//    {
//        cout << line.size() << endl;
//    }
//}



//class Solution {
//public:
//    bool isnumberchar(char ch)
//    {
//        if (ch >= '0' && ch <= '9')
//            return true;
//        if (ch >= 'a' && ch <= 'z')
//            return true;
//        return false;
//    }
//    bool isPalindrome(string s) {
//        if (s.empty())
//        {
//            return true;
//        }
//
//        int len = s.size();
//        for (int i = 0; i < len; i++)
//        {
//            s[i] = tolower(s[i]);
//        }
//
//        int left = 0, right = len - 1;
//        while (left < right)
//        {
//            while (left < right && !isnumberchar(s[left]))
//                left++;
//            while (right > left && !isnumberchar(s[right]))
//                right--;
//            if (s[left] != s[right])
//                return false;
//            left++;
//            right--;
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    int firstUniqChar(string s) {
//        int count[256] = { 0 };
//        int size = s.size();
//        for (int i = 0; i < size; i++)
//        {
//            count[s[i]]++;
//        }
//        for (int i = 0; i < size; i++)
//        {
//            if (count[s[i]] == 1)
//                return i;
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    void reverseString(vector<char>& s) {
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            swap(s[left], s[right]);
//            left++;
//            right--;
//        }
//    }
//};

//class Solution {
//public:
//
//    int StrToInt(string str) {
//        int len = str.size();
//        if (len == 0)
//            return 0;
//
//        const char* pch = str.c_str();
//        if (pch == nullptr)
//            return 0;
//
//        int flag = 1;
//        int i = 0;
//        if (str[i] == '+')
//            i++;
//        else if (str[i] == '-')
//        {
//            flag = -1;
//            i++;
//        }
//
//        long long num = 0;
//        for (; i < len; i++)
//        {
//            if (str[i] >= '0' && str[i] <= '9')
//            {
//                num = num * 10 + str[i] - '0';
//                if ((flag > 0 && num > 0x7fffffff) || (flag < 0 && num>0x80000000))
//                    return 0;
//            }
//            else
//            {
//                return 0;
//            }
//        }
//        return flag * num;
//    }
//};


//#include <limits.h>

