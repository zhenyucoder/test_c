//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        if (n < 2) return n;
//        int ret = 1, prev = nums[0];
//        for (int i = 1; i < n - 1; i++)
//        {
//            if (nums[i + 1] == nums[i])
//                continue;
//            if ((prev < nums[i] && nums[i] > nums[i + 1]) || (prev > nums[i] && nums[i] < nums[i + 1]))
//                ret++;
//            prev = nums[i];
//        }
//        return prev == nums[n - 1] ? ret : ret + 1;
//    }



//class Solution {
//public:
//    int left = 0, right = 0;
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size(), ret = 0;
//        if (n < 2) return n;
//        for (int i = 0; i < nums.size() - 1; i++)
//        {
//            right = nums[i + 1] - nums[i];
//            if (right == 0)
//                continue;
//            if (left * right <= 0)
//                ret++;
//            left = right;
//        }
//        return ret + 1;
//    }
//};



//class Solution
//{
//public:
//	int lengthOfLIS(vector<int>& nums)
//	{
//		int n = nums.size();
//		vector<int> ret;
//		ret.push_back(nums[0]);
//		for (int i = 1; i < n; i++)
//		{
//			if (nums[i] > ret.back()) 
//			{
//				ret.push_back(nums[i]);
//			}
//			else
//			{
//				
//				int left = 0, right = ret.size() - 1;
//				while (left < right)
//				{
//					int mid = (left + right) >> 1;
//					if (ret[mid] < nums[i]) left = mid + 1;
//					else right = mid;
//				}
//				ret[left] = nums[i]; 
//			}
//		}
//		return ret.size();
//	}
//};



class Solution
{
public:
	bool increasingTriplet(vector<int>& nums)
	{
		int a = nums[0], b = INT_MAX;
		for (int i = 1; i < nums.size(); i++)
		{
			if (nums[i] > b) return true;
			else if (nums[i] > a) b = nums[i];
			else a = nums[i];
		}
		return false;
	}
};