//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root) {
//        if (root == nullptr) return root;
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (root->left == nullptr && root->right == nullptr && root->val == 0)
//        {
//            delete root;
//            root = nullptr;
//        }
//        return root;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        vector<int> diff(n);
//        for (int i = 0; i < n; i++)
//            diff[i] = gas[i] - cost[i];
//
//        for (int i = 0; i < n; i++)
//        {
//            if (diff[i] >= 0)
//            {
//                int target = i == 0 ? n - 1 : i - 1;
//                int sum = diff[i], begin = i;
//                while (sum >= 0 && begin != target)
//                {
//                    if (++begin == n) begin = 0;
//                    sum += diff[begin];
//                }
//                if (sum >= 0) return i;
//            }
//        }
//        return -1;
//    }
//};



class Solution {
public:
    int maxArea(vector<int>& height) {
        int ret = 0, left = 0, right = height.size() - 1;
        while (left < right)
        {
            int _height = min(height[left], height[right]);
            ret = max(ret, _height * (right - left));
            if (height[left] > height[right])
                right--;
            else
                left++;
        }
        return ret;
    }
};