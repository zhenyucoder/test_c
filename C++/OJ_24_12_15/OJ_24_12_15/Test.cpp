//class Solution {
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            ret.push_back(q.front()->val);
//            int sz = q.size();
//            while (sz--)
//            {
//                TreeNode* top = q.front();
//                q.pop();
//                if (top->right)
//                    q.push(top->right);
//                if (top->left)
//                    q.push(top->left);
//            }
//        }
//        return ret;
//    }
//};



class Solution {
public:
    vector<vector<int>> multiSearch(string big, vector<string>& smalls) {
        vector<vector<int>> ret;
        int pos = 0;
        for (auto str : smalls)
        {
            vector<int> ans;
            if (str.empty())
            {
                ret.push_back(ans);
                continue;
            }
            int pos = 0;
            while (true)
            {
                pos = big.find(str, pos);
                if (pos == std::string::npos) break;
                else
                {
                    ans.push_back(pos);
                    pos += 1;
                }
            }
            ret.push_back(ans);
        }
        return ret;
    }
};