//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int sum = 0, len = INT_MAX;
//        for (int left = 0, right = 0; right < nums.size(); right++)
//        {
//            sum += nums[right];
//            while (left <= right && sum >= target)
//            {
//                //更新结果
//                len = min(len, right - left + 1);
//                //出窗口 
//                sum -= nums[left++];
//            }
//        }
//        return len == INT_MAX ? 0 : len;
//    }
//};


//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[128] = { 0 };
//        int len = INT_MIN, n = s.size();
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            hash[s[right]]++;//进窗口
//            while (hash[s[right]] > 1)
//            {//出窗口
//                hash[s[left++]]--;
//            }
//            //跟新结果
//            len = max(len, right - left + 1);
//        }
//        return len == INT_MIN ? 0 : len;
//    }
//};



//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int ret = INT_MIN;
//        for (int left = 0, right = 0, zero = 0; right < nums.size(); right++)
//        {
//            if (nums[right] == 0)
//                zero++;
//            while (zero > k)
//            {//出窗口
//                if (nums[left++] == 0)
//                    zero--;
//            }
//            //更新结果
//            ret = max(ret, right - left + 1);
//        }
//        return ret == INT_MIN ? 0 : ret;
//    }
//};


//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int sum = 0, n = nums.size();
//        for (auto num : nums)
//            sum += num;
//        //查找最大连续和为target的子数组长度
//        int target = sum - x, len = INT_MIN;
//        sum = 0;
//        if (target < 0)
//            return -1;
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            sum += nums[right];//进窗口
//            while (sum > target)
//            {//出窗口
//                sum -= nums[left++];
//            }
//
//            //更新结果
//            if (sum == target)
//            {
//                len = max(len, right - left + 1);
//            }
//        }
//        return len == INT_MIN ? -1 : n - len;
//    }
//};

//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        unordered_map<int, int> hash;
//        int n = fruits.size(), ret = 0;
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            //进窗口
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {//出窗口
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)
//                    hash.erase(fruits[left]);
//                left++;
//            }
//            //更新结果
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    bool Check(int hash1[], int hash2[])
//    {
//        for (int i = 0; i < 26; i++)
//        {
//            if (hash1[i] != hash2[i])
//                return false;
//        }
//        return true;
//    }
//
//    vector<int> findAnagrams(string s, string p) {
//        int hash1[26], hash2[26] = { 0 };
//        for (auto ch : p)
//        {
//            hash2[ch - 'a']++;
//        }
//
//        vector<int> ret;
//        for (int left = 0, right = 0; right < s.size(); right++)
//        {
//            hash1[s[right] - 'a']++;
//            if (right - left + 1 > p.size())//出窗口
//            {
//                hash1[s[left++] - 'a']--;
//            }
//            if (Check(hash1, hash2))
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int hash1[26] = { 0 }, hash2[26] = { 0 };
//        for (auto ch : p)
//            hash2[ch - 'a']++;
//        vector<int> ret;
//        int n1 = s.size(), n2 = p.size();
//        //count 统计有效字母个数
//        for (int left = 0, right = 0, count = 0; right < n1; right++)
//        {
//            char in = s[right];
//            if (++hash1[in - 'a'] <= hash2[in - 'a'])//进窗口 + 维护count
//                count++;
//            if (right - left + 1 > n2)//出窗口
//            {
//                char out = s[left++];
//                if (hash1[out - 'a']-- <= hash2[out - 'a'])
//                    count--;
//            }
//            //更新结果
//            if (count == n2)
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};



