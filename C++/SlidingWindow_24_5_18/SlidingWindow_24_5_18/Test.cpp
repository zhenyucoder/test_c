//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int sum = 0, len = INT_MAX;
//        for (int left = 0, right = 0, n = nums.size(); right < n; right++)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left++];
//            }
//        }
//        return len == INT_MAX ? 0 : len;
//    }
//};



/*
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, int> hash;
        int ret = 0;
        for (int left = 0, right = 0, n = s.size(); right < n; right++)
        {
            hash[s[right]]++;
            while (hash[s[right]] > 1)
            {
                hash[s[left++]]--;
            }
            ret = max(ret, right - left + 1);
        }
        return ret;
    }
}*/;




/*class Solution {
public:
    int longestOnes(vector<int>& nums, int k) {
        int ret = 0;
        for (int left = 0, right = 0, n = nums.size(), zero = 0; right < n; right++)
        {
            if (nums[right] == 0)
                zero++;
            while (zero > k)
            {
                if (nums[left++] == 0)
                {
                    zero--;
                    break;
                }
            }
            ret = max(ret, right - left + 1);
        }
        return ret;
    }
}*/;





//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int sum = 0, n = nums.size();
//        for (auto x : nums)
//            sum += x;
//        int target = sum - x;
//        if (target < 0) return -1;
//        int maxlen = INT_MIN;
//        for (int left = 0, right = 0, tmp = 0; right < n; right++)
//        {
//            tmp += nums[right];
//            while (tmp > target)
//            {
//                tmp -= nums[left++];
//            }
//            if (tmp == target)
//                maxlen = max(maxlen, right - left + 1);
//        }
//        return maxlen == INT_MIN ? -1 : n - maxlen;
//    }
//};



//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        unordered_map<int, int> hash;
//        int ret = 0;
//        for (int left = 0, right = 0; right < fruits.size(); right++)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)
//                    hash.erase(fruits[left]);
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        vector<int> ret;
//        int arr2[26] = { 0 };
//        for (auto ch : p)
//            arr2[ch - 'a']++;
//        int n1 = s.size(), n2 = p.size();
//        int arr1[26] = { 0 };
//        for (int left = 0, right = 0, count = 0; right < n1; right++)
//        {
//            char in = s[right] - 'a';
//            arr1[in]++;
//            if (arr1[in] <= arr2[in])
//                count++;
//            if (right - left + 1 > n2)
//            {
//                char out = s[left++] - 'a';
//                arr1[out]--;
//                if (arr2[out] > 0 && arr1[out] < arr2[out])
//                    count--;
//            }
//            if (count == n2)
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        unordered_map<string, int> hash2;
//        for (auto& str : words)
//            hash2[str]++;
//
//        vector<int> ret;
//        int len = words[0].size(), n = words.size();
//        for (int i = 0; i < len; i++)
//        {
//            unordered_map<string, int> hash1;
//            for (int left = i, right = i, count = 0; right + len <= s.size(); right += len)
//            {
//                string in = s.substr(right, len);
//                hash1[in]++;
//                if (hash2.count(in) && hash1[in] <= hash2[in])
//                    count++;
//                if (right - left + 1 > n * len)
//                {
//                    string out = s.substr(left, len);
//                    hash1[out]--;
//                    if (hash2.count(out) && hash1[out] < hash2[out])
//                        count--;
//                    left += len;
//                }
//                if (count == n)
//                    ret.push_back(left);
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    string minWindow(string s, string t) {
//        int begin = 0, len = INT_MAX;
//        unordered_map<char, int> hash2;
//        for (auto ch : t)
//            hash2[ch]++;
//
//        int n = s.size(), m = t.size();
//        if (n < m) return "";
//        unordered_map<char, int> hash1;
//        for (int left = 0, right = 0, count = 0; right < n; right++)
//        {
//            char in = s[right];
//            hash1[in]++;
//            if (hash2.count(in) && hash1[in] <= hash2[in])
//                count++;
//            while (count == m)
//            {
//                if (right - left + 1 < len)
//                    begin = left, len = right - left + 1;
//                char out = s[left++];
//                hash1[out]--;
//                if (hash2.count(out) && hash1[out] < hash2[out])
//                    count--;
//            }
//        }
//        return len == INT_MAX ? "" : s.substr(begin, len);
//    }
//};





