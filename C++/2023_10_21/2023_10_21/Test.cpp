#include <iostream>
using namespace std;

//int main()
//{
//	//int* p1 = (int*)malloc(sizeof(int));
//	//new不需要强转,自动计算大小
//	int* p1 = new int;
//
//	//int* p2 = (int*)malloc(sizeof(int) * 10);
//	int* p2 = new int[10];
//
//	delete p1;
//	delete[] p2;
//
//	//额外支持初始化
//	int* p3 = new int(1);
//	int* p4 = new int[10]{ 1,2,3 };
//	int* p5 = new int[10]{};
//
//	delete p3;
//	delete[] p4;
//	delete[] p5;
//	return 0;
//}



//class A
//{
//public:
//	A(int a = 0)
//		: _a(a)
//	{
//		cout << "A():" << this << endl;
//	}
//
//	~A()
//	{
//		cout << "~A():" << this << endl;
//	}
//
//private:
//	int _a;
//};
//
//int main()
//{
//	//malloc没有办法很好的支持自定义类型动态申请空间的初始化-->new
//	A* p1 = (A*)malloc(sizeof(A));
//	//p1->A(1); 创建对象时，自动调用，不可显示调用
//
//	//new = malloc + 构造函数 （申请空间+ 初始化）
//	A* p2 = new A;
//	A* p3 = new A(1);
//
//	//delete = 析构函数 + free
//	delete p2;
//	delete p3;
//
//	A* p4 = new A[10];
//	delete[] p4;
//
//	A a1(1);
//	A a2(2);
//	A* p5 = new A[10]{ a1,a2 };
//	delete[] p5;
//
//	//匿名对象
//	A* p6 = new A[10]{ A(1), A(2) };
//	delete[] p6;
//
//	//隐式类型转换
//	A* p7 = new A[10]{ 1,2 };
//	delete[] p7;
//	return 0;
//}



typedef char DataType;
class Stack
{
public:
	Stack(size_t capacity = 4)
	{
		cout << "Stack()" << endl;

		_array = new DataType[capacity];

		_capacity = capacity;
		_size = 0;
	}

	void Push(DataType data)
	{
		// CheckCapacity();
		_array[_size] = data;
		_size++;
	}

	~Stack()
	{
		cout << "~Stack()" << endl;

		delete[] _array;
		_array = nullptr;
		_size = _capacity = 0;
	}
private:
	DataType* _array;
	int _capacity;
	int _size;
};
//
//Stack* func()
//{
//	int n;
//	cin >> n;
//	Stack* pst = new Stack(n);
//
//	return pst;
//}
//
//int main()
//{
//	Stack* ptr = func();
//	ptr->Push(1);
//	ptr->Push(2);
//
//	delete ptr;
//	return 0;
//}


void func()
{
	char* ptr = new char[0x7fffffff];
	cout << (void*)ptr << endl;
}

int main()
{
	try
	{
		//char* ptr = new char[0x7fffffff];
		///*char* ptr = new char[100000000]*/;
		///*cout << ptr << endl;*///char* cout不会按指针打印，会按照字符串打印。同时由于编码等问题，
		//cout << (void*)ptr << endl;

		func();
		cout << "hello C++" << endl;
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}

	return 0;
}


//class A
//{
//public:
//	A(int a = 1)
//		:_a(a)
//	{
//	}
//
//	~A()
//	{
//		cout << "~A" << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	/*Stack* p1 = new Stack(3);
//	delete p1;*/
//
//	//Stack* ptr = (Stack*)operator new(sizeof(Stack));
//	//operator delete(ptr);
//
//	//Stack* ptr2 = new Stack;
//	////delete ptr2;
//	//free(ptr2);//没有调用析构不会报错，只是内存泄漏
//
//	int* p1 = new int[10];
//	//delete p1;
//	//free(p1);
//	delete[] p1;
//
//	A* p2 = new A[10];
//	//delete[] p2;
//	//free(p2);
//	delete p2;
//	return 0;
//}


