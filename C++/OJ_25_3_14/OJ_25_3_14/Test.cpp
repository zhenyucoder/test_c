//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n + 1), w(n + 1);
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//
//    // ques1
//    vector<int> dp(V + 1);
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = V; j >= 1; j--)
//        {
//            if (j >= v[i])
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[V] << endl;
//
//    // ques2
//    vector<int> dp1(V + 1, -0x3f3f3f3f);
//    dp1[0] = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = V; j >= 1; j--)
//        {
//            if (v[i] <= j)
//            {
//                dp1[j] = max(dp1[j], dp1[j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << ((dp1[V] < 0) ? 0 : dp1[V]) << endl;
//    return 0;
//}




//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1010;
//int arr[N][N];
//int n;
//
//void setRow()
//{
//    for (int i = 1; i <= n / 2; i++)
//    {
//        for (int j = 1; i <= n; j++)
//            swap(arr[i][j], arr[n - i][j]);
//    }
//}
//
//void setCol()
//{
//    for (int j = 1; j <= n / 2; j++)
//    {
//        for (int i = 1; i <= n; i++)
//            swap(arr[i][j], arr[i][n - j]);
//    }
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= n; j++)
//            cin >> arr[i][j];
//    // 操作步数统计
//    int q; cin >> q;
//    int row = 0, col = 0, tmp = 0;
//    for (int i = 0; i < q; i++)
//    {
//        cin >> tmp;
//        if (tmp == 1) row++, col++;
//        else row++;
//    }
//
//    // 处理
//    row %= 2, col %= 2;
//    if (row) setRow();
//    if (col) setCol();
//
//    // 结果输出
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= n; j++)
//        {
//            std::cout << i << " ";
//        }
//        std::cout << std::endl;
//    }
//    return 0;
//}




#include <iostream>
using namespace std;

const int N = 1e5 + 10;
int dp[N];
int main()
{
    int n; cin >> n;
    int first = 0, second = 0;
    for (int i = 1; i <= n; i++)
    {
        int x; cin >> x;
        if (x >= first)
            second = first, first = x;
        else if (x > second)
            second = x;
        dp[i] = second;
    }

    int q; cin >> q;
    for (int i = 0; i < q; i++)
    {
        int x; cin >> x;
        cout << dp[x] << endl;
    }
    return 0;
}