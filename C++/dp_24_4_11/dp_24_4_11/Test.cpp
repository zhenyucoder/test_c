//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        int ret = 0;
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                if (dp[i][j])
//                    ret++;
//            }
//        return ret;
//    }
//};




//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int begin = 0, end = 0, len = 0;//最长回文串的开始和结束下标, 长度
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));//每个字串是否为回文子串信息
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//                if (j - i + 1 > len && dp[i][j])
//                {
//                    begin = i, end = j, len = j - i + 1;
//                }
//            }
//        return s.substr(begin, len);
//    }
//};


//class Solution {
//public:
//    bool checkPartitioning(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));//保存所有字串是否为回文子串信息
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
//
//        for (int i = 1; i < n - 1; ++i)
//            for (int j = i; j < n - 1; j++)
//                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1])
//                    return true;
//        return false;
//    }
//};




//class Solution {
//public:
//    int minCut(string s) {
//        int n = s.size();
//        vector<vector<bool>> isPal(n, vector<bool>(n));//保存字串回文信息
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//                if (s[i] == s[j])
//                    isPal[i][j] = i + 1 < j ? isPal[i + 1][j - 1] : true;
//
//        vector<int> dp(n, 0x3f3f3f3f);
//        for (int i = 0; i < n; i++)
//        {
//            if (isPal[0][i])
//            {
//                dp[i] = 0;
//            }
//            else
//            {
//                for (int j = 1; j <= i; j++)
//                {
//                    if (isPal[j][i])
//                        dp[i] = min(dp[i], dp[j - 1] + 1);
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};



//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i == j ? 1 : dp[i + 1][j - 1] + 2;
//                else
//                    dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        return dp[0][n - 1];
//    }
//};



//class Solution {
//public:
//    int minInsertions(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n, INT_MAX));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : 0;
//                else
//                    dp[i][j] = min(dp[i + 1][j], dp[i][j - 1]) + 1;
//            }
//        }
//        return dp[0][n - 1];
//    }
//};