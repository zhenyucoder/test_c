//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<Node*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int sz = q.size();
//            vector<int> cur;
//            for (int i = 0; i < sz; i++)
//            {
//                Node* top = q.front();
//                q.pop();
//
//                cur.push_back(top->val);
//                for (auto node : top->children)
//                {
//                    q.push(node);
//                }
//            }
//            ret.push_back(cur);
//        }
//        return ret;
//    }
//};




class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        vector<int> ret;
        if (root == nullptr) return ret;
        queue<TreeNode*> q;
        q.push(root);
        while (q.size())
        {
            ret.push_back(q.front()->val);
            int sz = q.size();
            while (sz--)
            {
                TreeNode* top = q.front();
                q.pop();
                if (top->right)
                    q.push(top->right);
                if (top->left)
                    q.push(top->left);
            }
        }
        return ret;
    }
};