#include <iostream>
#include <string>
using namespace std;


void test_string1()
{
	string s1;
	string s2("hello");

	cout << s2 << endl;

	cin >> s1;
	cout << s1 << endl;
	cin >> s2;
	cout << s2 << endl;


	string ret = s1 + s2;
	cout << ret << endl;

	s2 = s2 + "what is C++?";
	cout << s2 << endl;
	s2 = s2 + '1';
	cout << s2 << endl;
}

void test_string2()
{
	string s1("helloc C++!! -->");
	string s2 = "hello C++";

	//遍历string

	//1. for循环
	/*for (int i = 0; i < s1.size(); i++)
	{
		cout << s1.at(i) << " ";
	}
	cout << endl;

	for (int i = 0; i < s1.size(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;*/

	//2.迭代器
	//类似于指针
	//string::iterator it = s1.begin();
	//while (it != s1.end())
	//{
	//	cout << *it << " ";
	//	it++;
	//}
	//cout << endl;
	
	//3. 基于范围for
	for (auto ch : s1)
	{
		cout << ch << " ";
	}
	cout << endl;

	for (auto& ch : s1)
	{
		cout << ++ch << " ";
	}
	cout << endl;
}

void test_string3()
{
	string s1("hello C++!! -->");
	//string::reverse_iterator it = s1.rbegin();
	//范围for本质还是通过迭代器来实现的,编译时编译器替换为迭代器
	auto it = s1.rbegin();
	while (it != s1.rend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

}

void func(const string& s1)
{
	//string::const_iterator it = s1.begin();
	auto it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
	
	//string::const_reverse_iterator rit = s1.rbegin();
	auto rit = s1.rbegin();
	while (rit != s1.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;
}

void test_string4()
{
	string s1("hello zhenyuxxxxxxxxxxxxxx");
	func(s1);

	string s2(s1);
	cout << s2 << endl;

	string s3(s1, 5);
	string s4(s1, 6, 10);
	cout << "s3: " << s3 << endl;
	cout << "s4: " << s4 << endl;

	string s5("hello worldxxxxxxxxx", 12);
	cout << "s5:" << s5 << endl;

	string s6(10,'x');
	cout << "s6:" << s6 << endl;

	string s7(++s5.begin(), --s5.end());
	cout << "s7:" << s7 << endl;

	s7 = s1;
	cout << s7 << endl;
	s7 = "1234567890";
	cout << s7 << endl;
	s7 = 'x';
	cout << s7 << endl;

}

void test_string5()
{
	string s1("hello world");
	cout << s1.length() << endl;
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;

	s1.clear();
	s1 += "张三";
	cout << s1.length() << endl;
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
} 

void test_string6()
{
	string s;
	s.reserve(100);

	size_t old = s.capacity();
	cout << "初始" << s.capacity() << endl;

	for (int i = 0; i < 100; i++)
	{
		s.push_back('a');
		if (old != s.capacity())
		{
			cout << "扩容" << s.capacity() << endl;
			old = s.capacity();//vs --  1.5 ; g++ -- 2 
		}

	}
	s.reserve(10);
	cout << s.capacity() << endl;
}

void test_string7()
{
	string s1("hello world");
	cout << s1.size() << endl;
	cout << s1.length() << endl;
	cout << s1.capacity() << endl;

	s1.resize(20, 'x');
	cout << s1 << endl;
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;

	s1.resize(5);
	cout << s1 << endl;
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;

	s1.resize(10);
	cout << s1 << endl;
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
}

void test_string8()
{
	string s;
	s.push_back('?');

	string ss("hello world");
	s.append(ss);
	cout << s << endl;

	s.append(ss, 6, 4);
	cout << s << endl;

	s.append("xxxxxxxxx");
	cout << s << endl;


	s.append(10, '0');
	cout << s << endl;

	s += '#';
	s += "hello";
	s += ss;
	cout << s << endl;

	string ret1 = ss + '#';
	string ret2 = ss + "hello";
	cout << ret1 << endl;
	cout << ret2 << endl;

}

void test_string9()
{
	std::string str("xxxxxxx");
	std::string base = "The quick brown fox jumps over a lazy dog.";

	str.assign(base);
	cout << str << endl;

	str.assign(base, 5, 10);
	std::cout << str << '\n';
}

void test_string10()
{
	// insert/erase/repalce能不用就尽量不用，因为他们都涉及挪动数据，效率不高
	std::string str("hello world");
	str.insert(0, 1, 'x');
	str.insert(str.begin(), 'x');
	cout << str << endl;

	str.erase(5);
	cout << str << endl;

	std::string s1("hello world");
	s1.replace(5, 3, "%%20");
	cout << s1 << endl;

	// 空格替换为20%
	std::string s2("The quick brown fox jumps over a lazy dog.");
	string s3;
	for (auto ch : s2)
	{
		if (ch != ' ')
		{
			s3 += ch;
		}
		else
		{
			s3 += "20%";
		}
	}

	//s2 = s3;
	//s2.assign(s3);
	printf("s2：%p\n", s2.c_str());
	printf("s3：%p\n", s3.c_str());

	swap(s2, s3);
	//s2.swap(s3);

	printf("s2：%p\n", s2.c_str());
	printf("s3：%p\n", s3.c_str());

	cout << s2 << endl;
}

int main()
{
	test_string10();
	return 0;
}