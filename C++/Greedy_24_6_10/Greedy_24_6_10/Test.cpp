//class Solution {
//public:
//    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size();
//        vector<int> index(n), ret(n);
//        for (int i = 0; i < n; i++)//��ʼ��
//            index[i] = i;
//
//		  // ����
//        sort(nums1.begin(), nums1.end());
//        sort(index.begin(), index.end(), [&](int i, int j)
//            {
//                return nums2[i] < nums2[j];
//            });
//			
//			//�������
//        int left = 0, right = n - 1;
//        for (auto x : nums1)
//        {
//            if (x > nums2[index[left]])
//                ret[index[left++]] = x;
//            else
//                ret[index[right--]] = x;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int longestPalindrome(string s) {
//        int ret = 0, n = s.size();
//        unordered_map<char, int> hash;
//        for (auto ch : s)
//            hash[ch]++;
//        for (auto [ch, num] : hash)
//            ret += num / 2 * 2;
//        return ret == n ? ret : ret + 1;
//    }
//};




//class Solution {
//public:
//    vector<int> diStringMatch(string s) {
//        int left = 0, right = s.size();
//        vector<int> ret(right + 1);
//        int i = 0;
//        for (auto ch : s)
//            if (ch == 'I')
//                ret[i++] = left++;
//            else
//                ret[i++] = right--;
//        ret[i] = left;
//        return ret;
//    }
//};




//class Solution {
//public:
//    int findContentChildren(vector<int>& g, vector<int>& s) {
//        sort(g.begin(), g.end());
//        sort(s.begin(), s.end());
//
//        int ret = 0;
//        for (int i = 0, j = 0, n1 = g.size(), n2 = s.size(); i < n1 && j < n2;)
//        {
//            int x = g[i];
//            while (j < n2 && x > s[j])
//                j++;
//            if (j < n2)
//                ret++;
//            i++, j++;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    string optimalDivision(vector<int>& nums) {
//        string ret;
//        int n = nums.size();
//        if (n == 1)
//            ret = to_string(nums[0]);
//        else if (n == 2)
//        {
//            ret += to_string(nums[0]) + '/' + to_string(nums[1]);
//        }
//        else
//        {
//            for (int i = 0; i < n; i++)
//            {
//                if (i == 1)
//                    ret += '(';
//                ret += to_string(nums[i]) + '/';
//            }
//            ret[ret.size() - 1] = ')';
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i < n; i++)
//            for (int j = i - 1; j >= 0; j--)
//                if (j + nums[j] >= i)
//                    dp[i] = min(dp[i], dp[j] + 1);
//        return dp[n - 1];
//    }
//};





//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int n = nums.size(), ret = 0;
//        if (n == 1) return 0;
//        for (int left = 0, right = 0; right < n;)
//        {
//            int maxpos = 0; ret++;
//            while (left <= right)
//            {
//                maxpos = max(maxpos, left + nums[left]);
//                left++;
//                if (maxpos >= n - 1)
//                    return ret;
//            }
//            right = maxpos;
//        }
//        return -1;
//    }
//};




//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        int n = nums.size(), left = 0, right = 0;
//        while (left <= right)
//        {
//            if (right >= n - 1) return true;
//            int maxpos = 0;
//            for (int i = left; i <= right; i++)
//                maxpos = max(maxpos, nums[i] + i);
//            left = right + 1;
//            right = maxpos;
//        }
//        return false;
//    }
//};





//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int n = gas.size();
//        vector<int> diff(n);
//        for (int i = 0; i < n; i++)
//            diff[i] = gas[i] - cost[i];
//
//        for (int i = 0; i < n; i++)
//        {
//            if (diff[i] >= 0)
//            {
//                int target = i == 0 ? n - 1 : i - 1;
//                int sum = diff[i], begin = i;
//                while (sum >= 0 && begin != target)
//                {
//                    if (++begin == n) begin = 0;
//                    sum += diff[begin];
//                }
//                if (sum >= 0) return i;
//            }
//        }
//        return -1;
//    }
//};



