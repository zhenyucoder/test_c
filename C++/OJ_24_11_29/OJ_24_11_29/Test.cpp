//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int hash[256] = { 0 };
//        int maxlen = 0;
//        for (int left = 0, right = 0, n = s.size(); right < n; right++)
//        {
//            // 进窗口
//            char in = s[right];
//            hash[in]++;
//            while (hash[in] > 1) // 判断 出窗口
//            {
//                char out = s[left++];
//                hash[out]--;
//            }
//            // 更新结果
//            maxlen = max(maxlen, right - left + 1);
//        }
//        return maxlen;
//    }
//};



//class Solution {
//public:
//    int trap(vector<int>& height) {
//        int ret = 0, left = 0, right = height.size() - 1;
//        int leftMax = height[0], rightMax = height[right];
//        while (left < right)
//        {
//            leftMax = max(leftMax, height[left]);
//            rightMax = max(rightMax, height[right]);
//            if (height[left] < height[right])
//                ret += leftMax - height[left], left++;
//            else
//                ret += rightMax - height[right], right--;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        int cur = 0, dest = -1, n = nums.size();
//        while (cur < n)
//        {
//            if (nums[cur] != 0)
//                swap(nums[++dest], nums[cur]);
//            cur++;
//        }
//    }
//};



//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        int dest = -1, cur = 0, n = arr.size();
//        while (cur < n)
//        {
//            if (arr[cur])
//                dest++;
//            else
//                dest += 2;
//            if (dest >= n - 1)
//                break;
//            cur++;
//        }
//
//        //边界处理
//        if (dest == n)
//        {
//            arr[--dest] = 0;
//            dest--;
//            cur--;
//        }
//
//        for (; cur >= 0; cur--)
//        {
//            if (arr[cur])
//            {
//                arr[dest--] = arr[cur];
//            }
//            else
//            {
//                arr[dest--] = 0;
//                arr[dest--] = 0;
//            }
//        }
//    }
//};



