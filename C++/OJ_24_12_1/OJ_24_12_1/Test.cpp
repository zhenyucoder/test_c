//class Solution {
//public:
//    ListNode* sortInList(ListNode* head) {
//        typedef pair<int, ListNode*> PIL;
//        vector<PIL> nodes;
//        ListNode* cur = head;
//        while (cur)
//        {
//            nodes.push_back(make_pair(cur->val, cur));
//            cur = cur->next;
//        }
//
//        sort(nodes.begin(), nodes.end(), [](PIL& node1, PIL& node2) {
//            return node1.first < node2.first;
//            });
//
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        for (auto& node : nodes)
//        {
//            prev->next = node.second;
//            prev = prev->next;
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    ListNode* EntryNodeOfLoop(ListNode* pHead) {
//        if (pHead == nullptr) return nullptr;
//        ListNode* slow = pHead, * fast = pHead->next;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//            if (slow == fast)   break;
//        }
//        if (fast == nullptr || fast->next == nullptr) return nullptr;
//
//        // 存在换，查找入口
//        ListNode* cur1 = pHead, * cur2 = slow->next;
//        while (cur1 != cur2)
//        {
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return cur1;
//    }
//};



//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int dest = 0;
//        for (int i = 1; i < nums.size(); i++)
//        {
//            if (nums[i] != nums[dest])
//                nums[++dest] = nums[i];
//        }
//        return dest + 1;
//    }
//};


//class Solution {
//public:
//    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
//        int cur1 = m - 1, cur2 = n - 1, cur = m + n - 1;
//        while (cur1 >= 0 && cur2 >= 0)
//        {
//            if (nums1[cur1] > nums2[cur2])
//                nums1[cur--] = nums1[cur1--];
//            else
//                nums1[cur--] = nums2[cur2--];
//        }
//        while (cur2 >= 0)
//            nums1[cur--] = nums2[cur2--];
//        return;
//    }
//};



//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* newhead = new ListNode(0), * prev = newhead, * cur = head;
//        newhead->next = head;
//        while (cur)
//        {
//            if (cur->val == val)
//                cur = cur->next;
//            else
//            {
//                prev->next = cur;
//                prev = cur;
//                cur = cur->next;
//            }
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        if (head == nullptr) return nullptr;
//        head->next = removeElements(head->next, val);
//        return head->val == val ? head->next : head;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//};




//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newhead = new ListNode(), * cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            // 头插
//            cur->next = newhead->next;
//            newhead->next = cur;
//
//            cur = next;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};




//class Solution {
//public:
//    ListNode* middleNode(ListNode* head) {
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        return slow;
//    }
//};



//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr || list2 == nullptr)
//            return list1 == nullptr ? list2 : list1;
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list2->next, list1);
//            return list2;
//        }
//    }
//};




//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        ListNode* newnode = new ListNode(), * prev = newnode;
//        while (list1 && list2)
//        {
//            if (list1->val < list2->val)
//            {
//                prev->next = list1;
//                prev = prev->next;
//                list1 = list1->next;
//            }
//            else
//            {
//                prev->next = list2;
//                prev = prev->next;
//                list2 = list2->next;
//            }
//        }
//        prev->next = (list1 == nullptr) ? list2 : list1;
//        prev = newnode->next;
//        delete newnode;
//        return prev;
//    }
//};




//class Partition {
//public:
//    ListNode* partition(ListNode* pHead, int x) {
//        ListNode* newhead1 = new ListNode(0), * prev1 = newhead1; // 所有小于x节点
//        ListNode* newhead2 = new ListNode(0), * prev2 = newhead2; // 所有>=x节点
//        ListNode* cur = pHead;
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                prev1->next = cur;
//                prev1 = prev1->next;
//            }
//            else
//            {
//                prev2->next = cur;
//                prev2 = prev2->next;
//            }
//            cur = cur->next;
//        }
//        prev2->next = nullptr;
//        prev1->next = newhead2->next;
//        prev1 = newhead1->next;
//        delete newhead1;
//        delete newhead2;
//        return prev1;
//    }
//};




//class PalindromeList {
//public:
//    ListNode* ReverseList(ListNode* list)
//    {
//        if (list == nullptr || list->next == nullptr) return list;
//        ListNode* newhead = ReverseList(list->next);
//        list->next->next = list;
//        list->next = nullptr;
//        return newhead;
//    }
//
//    bool chkPalindrome(ListNode* list) {
//        ListNode* fast = list, * slow = list;
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//
//        ListNode* cur2 = ReverseList(slow);
//        ListNode* cur1 = list;
//        while (cur1 && cur2)
//        {
//            if (cur1->val != cur2->val) return false;
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        // 1. 将链表中的所有原节点深拷贝，并连接到被赋值节点之后
//        Node* cur = head;
//        while (cur)
//        {
//            Node* next = cur->next;
//            Node* node = new Node(cur->val);
//
//            // 链接
//            cur->next = node;
//            node->next = next;
//
//            cur = next;
//        }
//
//        // 2. 处理深拷贝出的节点的random指针
//        cur = head;
//        while (cur)
//        {
//            if (cur->random)
//            {
//                cur->next->random = cur->random->next;
//            }
//            cur = cur->next->next;
//        }
//
//        // 提取深拷贝节点， 并还原原链表
//        Node* newhead = new Node(0), * prev = newhead;
//        cur = head;
//        while (cur)
//        {
//            Node* next = cur->next->next;
//            Node* Cnode = cur->next;
//
//            cur->next = next;
//            cur = next;
//            prev->next = Cnode;
//            prev = Cnode;
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return nullptr;
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (fast == slow) break;
//        }
//        if (fast == nullptr || fast->next == nullptr) return nullptr;
//        ListNode* cur1 = head, * cur2 = slow;
//        while (cur1 != cur2)
//        {
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return cur1;
//    }
//};



class Solution {
public:
    int NodeSize(ListNode* head)
    {
        int ret = 0;
        while (head)
        {
            ret++;
            head = head->next;
        }
        return ret;
    }
    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
        ListNode* longList = headA, * shortList = headB;
        int s1 = NodeSize(headA), s2 = NodeSize(headB);
        int gap = s1 - s2;
        if (gap < 0)
        {
            gap = -gap;
            longList = headB, shortList = headA;
        }

        while (gap--)
        {
            longList = longList->next;
        }

        while (longList && shortList)
        {
            if (longList == shortList) break;
            longList = longList->next;
            shortList = shortList->next;
        }
        return longList;
    }
};