//class Solution
//{
//public:
//	int longestPalindrome(string s)
//	{
//		int hash[127] = { 0 };
//		for (char ch : s) hash[ch]++;
//		int ret = 0;
//		for (int x : hash)
//		{
//			ret += x / 2 * 2;
//		}
//		return ret < s.size() ? ret + 1 : ret;
//	}
//};


//class Solution
//{
//public:
//	vector<int> diStringMatch(string s)
//	{
//		int left = 0, right = s.size(); 
//		vector<int> ret;
//		for (auto ch : s)
//		{
//			if (ch == 'I') ret.push_back(left++);
//			else ret.push_back(right--);
//		}
//		ret.push_back(left); 
//		return ret;
//	}
//};



class Solution
{
public:
	int findContentChildren(vector<int>& g, vector<int>& s)
	{
		sort(g.begin(), g.end());
		sort(s.begin(), s.end());
		int ret = 0, n = s.size();
		for (int i = 0, j = 0; i < g.size() && j < n; i++, j++)
		{
			while (j < n && s[j] < g[i]) j++; 
			if (j < n) ret++;
		}
		return ret;
	}
};