//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        for (int cur = 0, dest = -1; cur < nums.size(); cur++)
//        {
//            if (nums[cur] != 0)
//                swap(nums[++dest], nums[cur]);
//        }
//    }
//};


//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        vector<int> tmp(arr.begin(), arr.end());
//        int n = tmp.size();
//        for (int cur1 = 0, cur2 = 0; cur1 < n && cur2 < n; cur1++, cur2++)
//        {
//            arr[cur1] = tmp[cur2];
//            if (tmp[cur2] == 0 && ++cur1 < n)
//                arr[cur1] = 0;
//        }
//    }
//}; 

class Solution {
public:
    void duplicateZeros(vector<int>& arr) {
        int dest = -1, cur = 0, n = arr.size();
        while (cur < n)
        {
            if (arr[cur])
                dest++;
            else
                dest += 2;
            if (dest >= n - 1)
                break;
            cur++;
        }

        //�߽紦��
        if (dest == n)
        {
            arr[--dest] = 0;
            dest--;
            cur--;
        }

        for (; cur >= 0; cur--)
        {
            if (arr[cur])
            {
                arr[dest--] = arr[cur];
            }
            else
            {
                arr[dest--] = 0;
                arr[dest--] = 0;
            }
        }
    }
};