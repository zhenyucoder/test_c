﻿//#include <iostream>
//using namespace std;
//const int N = 110;
//int n, m;
//int arr[N][N];
//int dp[N][N];
//int dx[4] = { 0, 0, 1, -1 };
//int dy[4] = { 1, -1, 0, 0 };
//int dfs(int i, int j)
//{
//	if (dp[i][j]) return dp[i][j];
//	int len = 1;
//	for (int k = 0; k < 4; k++)
//	{
//		int x = i + dx[k], y = j + dy[k];
//		if (x >= 1 && x <= n && y >= 1 && y <= m && arr[x][y] < arr[i][j])
//		{
//			len = max(len, 1 + dfs(x, y));
//		}
//	}
//	dp[i][j] = len;
//	return len;
//}
//int main()
//{
//	cin >> n >> m;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= m; j++)
//		{
//			cin >> arr[i][j];
//		}
//	}
//	int ret = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= m; j++)
//		{
//			ret = max(ret, dfs(i, j));
//		}
//	}
//	cout << ret << endl;
//	return 0;
//}





//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;
//const int N = 1e5 + 10;
//pair<int, string> sp[N];
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 0; i < n; i++)
//	{
//		cin >> sp[i].second >> sp[i].first;
//	}
//	sort(sp, sp + n);
//	for (int i = 0; i < n; i++)
//	{
//		cout << sp[i].second << endl;
//	}
//	return 0;
//}




//#include <iostream>
//using namespace std; 
//const int N = 1010;int n;
//int g[N][N];
//void setRow() // ⾏对称
//{
//	for (int i = 0; i < n / 2; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			swap(g[i][j], g[n - 1 - i][j]);
//		}
//	}
//}
//void setCol() // 列对称
//{
//	for (int j = 0; j < n / 2; j++)
//	{
//		for (int i = 0; i < n; i++)
//		{
//			swap(g[i][j], g[i][n - 1 - j]);
//		}
//	}
//}
//int main()
//{
//	cin >> n;
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			cin >> g[i][j];
//		}
//	}
//
//	int q, x;
//	cin >> q;
//
//	int row = 0, col = 0;
//	while (q--)
//	{
//		cin >> x;
//		if (x == 1) row++, col++;
//		else row++;
//	}
//
//	row %= 2; col %= 2;
//	if (row) setRow();
//	if (col) setCol();
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			cout << g[i][j] << " ";
//		}
//		cout << endl;
//	}
//
//	return 0;
//}



#include <iostream>
#include <cstring>
using namespace std;
typedef long long LL;
const int N = 1010;
int n, k;
LL a[N];
LL dp[N][N];
int main()
{
	cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> a[i];
	memset(dp, -0x3f, sizeof dp);
	dp[0][0] = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - a[i] % k + k) % k] +
				a[i]);
		}
	}
	if (dp[n][0] <= 0) cout << -1 << endl;
	else cout << dp[n][0] << endl;
	return 0;
}