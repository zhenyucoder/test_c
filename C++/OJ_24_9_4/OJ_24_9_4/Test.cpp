//#include <iostream>
//using namespace std;
//
//int GetGCD(int a, int b)
//{
//    if (b == 0) return a;
//    else return GetGCD(b, a % b);
//}
//
//int main()
//{
//    int n, a;
//    while (cin >> n >> a)
//    {
//        int ans = a, temp;
//        for (int i = 0; i < n; i++)
//        {
//            cin >> temp;
//            if (ans >= temp) ans += temp;
//            else ans += GetGCD(ans, temp);
//        }
//        std::cout << ans << endl;
//    }
//    return 0;
//}




//int maxValue(vector<vector<int> >& grid) {
//    int m = grid.size(), n = grid[0].size();
//    int dp[m + 1][n + 1];
//    memset(dp, 0, sizeof(dp));
//    // vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
//
//    for (int i = 1; i <= m; i++)
//    {
//        for (int j = 1; j <= n; j++)
//        {
//            dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//        }
//    }
//    return dp[m][n];
//}




//int t, n;
//string s;
//bool vis[110][26];
//bool check(int left, int right)
//{
//	for (int i = 0; i < 26; i++)
//	{
//		if (vis[left][i] && vis[right][i]) return true;
//	}
//	return false;
//}
//int main()
//{
//	cin >> t;
//	while (t--)
//	{
//		memset(vis, 0, sizeof vis);
//		cin >> n;
//		for (int i = 0; i < n; i++)
//		{
//			cin >> s;
//			for (auto ch : s)
//			{
//				vis[i][ch - 'a'] = true;
//			}
//		}
//
//		int left = 0, right = n - 1;
//		while (left < right)
//		{
//			if (!check(left, right)) break;
//			left++; right--;
//		}
//		if (left < right) cout << "No" << endl;
//		else cout << "Yes" << endl;
//	}
//
//	return 0;
//}



typedef long long LL;
LL a, h, b, k, ret = 0;
int main()
{
	cin >> a >> h >> b >> k;

	LL ret = 0;
	LL n = min(h / b, k / a);
	ret += n * (a + b);

	h -= n * b;
	k -= n * a;

	if (h > 0 && k > 0)
	{
		h -= b;
		k -= a;
		ret += a + b;
	}

	if (h > 0 || k > 0)
	{
		ret += 10 * (h > 0 ? a : b);
	}

	cout << ret << endl;

	return 0;
}