//class Solution
//{
//	vector<int> path;
//	vector<vector<int>> ret;
//	bool check[9];
//public:
//	vector<vector<int>> permuteUnique(vector<int>& nums)
//	{
//		sort(nums.begin(), nums.end());
//		dfs(nums, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos)
//	{
//		if (pos == nums.size())
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (int i = 0; i < nums.size(); i++)
//		{
//			if (check[i] == false && (i == 0 || nums[i] != nums[i - 1] ||
//				check[i - 1] != false))
//			{
//				path.push_back(nums[i]);
//				check[i] = true;
//				dfs(nums, pos + 1);
//				path.pop_back(); 
//				check[i] = false;
//			}
//
//		}
//	}
//};




//class Solution
//{
//	string path;
//	vector<string> ret;
//public:
//	vector<string> letterCasePermutation(string s)
//	{
//		dfs(s, 0);
//		return ret;
//	}
//	void dfs(string& s, int pos)
//	{
//		if (pos == s.length())
//		{
//			ret.push_back(path);
//			return;
//		}
//		char ch = s[pos];
//		path.push_back(ch);
//		dfs(s, pos + 1);
//		path.pop_back(); 
//		if (ch < '0' || ch > '9')
//		{
//			char tmp = change(ch);
//			path.push_back(tmp);
//			dfs(s, pos + 1);
//			path.pop_back(); 
//		}
//	}
//	char change(char ch)
//	{
//		if (ch >= 'a' && ch <= 'z') ch -= 32;
//		else ch += 32;
//		return ch;
//	}
//};




class Solution
{
	bool check[16];
	int ret;
public:
	int countArrangement(int n)
	{
		dfs(1, n);
		return ret;
	}
	void dfs(int pos, int n)
	{
		if (pos == n + 1)
		{
			ret++;
			return;
		}
		for (int i = 1; i <= n; i++)
		{
			if (!check[i] && (pos % i == 0 || i % pos == 0))
			{
				check[i] = true;
				dfs(pos + 1, n);
				check[i] = false; /
			}
		}
	}
};