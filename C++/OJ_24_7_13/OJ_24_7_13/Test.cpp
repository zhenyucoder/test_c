//class Solution
//{
//	vector<int> path;
//	vector<vector<int>> ret;
//	int n, k;
//public:
//	vector<vector<int>> combine(int _n, int _k)
//	{
//		n = _n; k = _k;
//		dfs(1);
//		return ret;
//	}
//	void dfs(int start)
//	{
//		if (path.size() == k)
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (int i = start; i <= n; i++)
//		{
//			path.push_back(i);
//			dfs(i + 1);
//			path.pop_back();
//		}
//	}
//};




//class Solution
//{
//	int aim;
//	vector<int> path;
//	vector<vector<int>> ret;
//public:
//	vector<vector<int>> combinationSum(vector<int>& nums, int target)
//	{
//		aim = target;
//		dfs(nums, 0, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos, int sum)
//	{
//		if (sum == aim)
//		{
//			ret.push_back(path);
//			return;
//		}
//		if (sum > aim || pos == nums.size()) return;
//		for (int k = 0; k * nums[pos] + sum <= aim; k++)
//		{
//			if (k) path.push_back(nums[pos]);
//			dfs(nums, pos + 1, sum + k * nums[pos]);
//		}
//		for (int k = 1; k * nums[pos] + sum <= aim; k++)
//		{
//			path.pop_back();
//		}
//	}
//};



//class Solution
//{
//public:
//	string optimalDivision(vector<int>& nums)
//	{
//		int n = nums.size();
//		if (n == 1)
//		{
//			return to_string(nums[0]);
//		}
//		if (n == 2)
//		{
//			return to_string(nums[0]) + "/" + to_string(nums[1]);
//		}
//		string ret = to_string(nums[0]) + "/(" + to_string(nums[1]);
//		for (int i = 2; i < n; i++)
//		{
//			ret += "/" + to_string(nums[i]);
//		}
//		ret += ")";
//		return ret;
//	}
//};




class Solution
{
public:
	int jump(vector<int>& nums)
	{
		int left = 0, right = 0, maxPos = 0, ret = 0, n = nums.size();
		while (left <= right) 
		{
			if (maxPos >= n - 1) 
			{
				return ret;
			}
			for (int i = left; i <= right; i++)
			{
				maxPos = max(maxPos, nums[i] + i);
			}
			left = right + 1;
			right = maxPos;
			ret++;
		}
		return -1;
	}
};