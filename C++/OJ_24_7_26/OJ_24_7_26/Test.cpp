//class Solution {
//public:
//    vector<vector<int>> ret;
//    bool check[7];
//    int n;
//    vector<vector<int>> permute(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, {});
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, vector<int> path)
//    {
//        if (path.size() == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (check[i] == false)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, path);
//                check[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    int n;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        // pos ��ѡ
//        dfs(nums, pos + 1);
//        // pos ѡ
//        path.push_back(nums[pos]);
//        dfs(nums, pos + 1);
//        path.pop_back();
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int n;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        ret.push_back(path);
//        if (pos >= n) return;
//        for (int i = pos; i < n; i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1);
//            path.pop_back();
//        }
//    }
//};





//class Solution {
//public:
//    int ret = 0, n = 0;
//    int subsetXORSum(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int Prev, int pos)
//    {
//        ret += Prev;
//        for (int i = pos; i < n; i++)
//        {
//            dfs(nums, Prev ^ nums[i], i + 1);
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    bool check[8];
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        dfs(nums);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        unordered_map<int, bool> hash;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && hash.count(nums[i]) == false)
//            {
//                hash[nums[i]] = true;
//                check[i] = true;
//                path.push_back(nums[i]);
//                dfs(nums);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};




class Solution {
public:
    vector<string> ret;
    string path;
    string arr[10] = { "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
    vector<string> letterCombinations(string digits) {
        if (digits.size() == 0)
            return ret;
        dfs(digits, 0);
        return ret;
    }

    void dfs(string digits, int pos)
    {
        if (pos >= digits.size())
        {
            ret.push_back(path);
            return;
        }

        string str = arr[digits[pos] - '2'];
        for (int i = 0; i < str.size(); i++)
        {
            path.push_back(str[i]);
            dfs(digits, pos + 1);
            path.pop_back();
        }
    }
};