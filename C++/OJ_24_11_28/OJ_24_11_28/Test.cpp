#include <iostream>
#include <queue>
#include <vector>

// priority_queue 默认less，降序 => '<' 大根堆
// greater --> 升序 => '>' 小根堆
// priority_queue第三个参数要传入的是一个类
// 对于sort这一类的算法函数，带自定义第三个参数的，人家传的是一个对象

//class Comp
//{
//public:
//	bool operator()(int x, int y)
//	{
//		return x < y;
//	}
//};
//
//int main()
//{
//	int a[] = { 1, 2, 45, 232 ,45 ,232 ,5, 223 };
//	std::priority_queue<int, std::vector<int>, Comp> heap;
//	for (auto x : a)
//		heap.push(x);
//	while (heap.size())
//	{
//		int top = heap.top();
//		heap.pop();
//		std::cout << top << " ";
//	}
//	return 0;
//}



//class Solution {
//public:
//    int lastStoneWeight(vector<int>& stones) {
//        priority_queue<int> heap;
//        for (auto x : stones)
//            heap.push(x);
//        while (heap.size() > 1)
//        {
//            int first = heap.top(); heap.pop();
//            int sec = heap.top(); heap.pop();
//            if (first > sec)
//                heap.push(first - sec);
//        }
//        return heap.empty() ? 0 : heap.top();
//    }
//};



//class KthLargest {
//public:
//    KthLargest(int k, vector<int>& nums) {
//        _k = k;
//        for (auto x : nums)
//        {
//            heap.push(x);
//            if (heap.size() > _k) heap.pop();
//        }
//    }
//
//    int add(int val) {
//        heap.push(val);
//        if (heap.size() > _k) heap.pop();
//        return heap.top();
//    }
//    priority_queue<int, vector<int>, std::greater<int>> heap;
//    int _k;
//};



//class KthLargest {
//public:
//    KthLargest(int k, vector<int>& nums) {
//        srand(time(nullptr));
//        _k = k;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            arr[i] = nums[i];
//            _size++;
//        }
//    }
//
//    int add(int val) {
//        arr[_size++] = val;
//        // 快排
//        return QSort(0, _size - 1, _k);
//    }
//
//    int QSort(int l, int r, int k)
//    {
//        if (l > r) return NULL;
//        int key = GetRandom();
//        int left = -1, right = _size, i = 0;
//        while (i < right)
//        {
//            if (arr[i] > key)
//                swap(arr[i], arr[--right]);
//            else if (arr[i] == key)
//                i++;
//            else
//                swap(arr[i++], arr[++left]);
//        }
//        // [l, left] [left + 1, right - 1] [right, r]
//        int a = r - right + 1, b = right - 1 - left;
//        if (a >= k)
//            return QSort(right, r, k);
//        else if (a + b >= k)
//            return key;
//        else
//            return QSort(l, left, k - a - b);
//    }
//
//    int GetRandom()
//    {
//        int pos = rand() % _size;
//        return arr[pos];
//    }
//
//    int arr[10010];
//    int _size;
//    int _k;
//};





//class Solution {
//public:
//    typedef pair<string, int> PSI;
//    class Cmp
//    {
//    public:
//        bool operator()(PSI& psi1, PSI& psi2)
//        {
//            if (psi1.second == psi2.second)
//                return psi1.first < psi2.first;
//            return psi1.second > psi2.second;
//        }
//    };
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        unordered_map<string, int> hash;
//        for (auto& str : words)
//            hash[str]++;
//        priority_queue<PSI, vector<PSI>, Cmp> pq;
//        for (auto& psi : hash)
//        {
//            pq.push(psi);
//            if (pq.size() > k)
//                pq.pop();
//        }
//        vector<string> ret(k);
//        for (int i = k - 1; i >= 0; i--)
//        {
//            ret[i] = pq.top().first;
//            pq.pop();
//        }
//        return ret;
//    }
//};



class MedianFinder {
public:
    priority_queue<int> left;
    priority_queue<int, vector<int>, std::greater<int>> right;
    MedianFinder() {
    }

    void addNum(int num) {
        if (left.size() == right.size())
        {
            if (left.empty() || num <= left.top())
                left.push(num);
            else
            {
                right.push(num);
                left.push(right.top());
                right.pop();
            }
        }
        else//left.size() = right.size() + 1
        {
            if (num <= left.top())
            {
                left.push(num);
                right.push(left.top());
                left.pop();
            }
            else
            {
                right.push(num);
            }
        }
    }

    double findMedian() {
        return (left.size() == right.size()) ? (left.top() + right.top()) / 2.0 : left.top();
    }
};