//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (k == 0 || head == nullptr || head->next == nullptr) return head;
//
//        // 将链表首尾连接，并统计链表长度
//        ListNode* cur = head;
//        int len = 1;
//        while (cur->next)
//        {
//            cur = cur->next;
//            len++;
//        }
//        int gap = len - k % len;
//        if (gap == len) return head; // 不需要旋转
//        cur->next = head;
//
//        while (gap--)// 在目标链表头节点的前一个节点停下
//            cur = cur->next;
//        ListNode* newhead = cur->next;
//        cur->next = nullptr;
//        return newhead;
//    }
//};



class Solution {
public:
    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
        _hanota(A, B, C, A.size());
    }

    void _hanota(vector<int>& A, vector<int>& B, vector<int>& C, int size)
    {
        if (size == 0) return;
        _hanota(A, C, B, size - 1);
        C.push_back(A.back());
        A.pop_back();
        _hanota(B, A, C, size - 1);
    }
};