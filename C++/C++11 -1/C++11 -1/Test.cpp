#include <iostream>
using namespace std;
#include <string>
#include <vector>
#include <list>
#include <map>
#include <initializer_list>

//struct Point
//{
//	int x;
//	int y;
//	int* ptr;
//};
//
//int main()
//{
//	int a = 1;
//	a = { 2 };
//	int c{ 3 };//初始化时，范围扩大
//
//	Point p1{ 1, 3, &a };
//	int arr[5]{ 1, 2, 3, 4 };
//	int* ptr = new int[5]{ 1, 2, 3, 5, 6 };
//	return 0;
//}



//
//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	Date d1(2024, 4, 9);
//	Date d2 = { 2023, 4, 1 };//本质上调用构造函数 + 拷贝构造  ---> 编译器优化为直接构造
//	Date d3{ 2023, 4, 1 };
//	string s = { 'a', 'c', 'e', 'x' };
//	cout << s << endl;
//	vector<int> v{ 1, 2, 4, 6, 7, 8 };
//
//
//	initializer_list<int> initlist{ 1, 2 ,3 ,4 ,5 };
//	for (auto x : initlist)
//		cout << x << " ";
//	cout << endl;
//
//	vector<int> v1(initlist), v2;
//	for(auto x : v1)
//		cout << x << " ";
//	cout << endl;
//
//	v2 = initlist;
//	for (auto x : v1)
//		cout << x << " ";
//	cout << endl;
//
//
//	map<string, string> m1 = { {"sort", "排序"}, {"get", "获得"}, pair<string, string>("dfs", "深度遍历") };
//	for (auto& p : m1)
//	{
//		cout << p.first << ":" << p.second << endl;
//	}
//	return 0;
//}




//namespace vector_addinitializer_list
//{
//	template<class T>
//	class vector
//	{
//		typedef  T* iterator;
//	public:
//		vector<T>(initializer_list<T> l)
//		{
//			_start = new T[l.size()];
//			_end = _endofstorage = _start + l.size();
//
//			iterator ptrv = _start;
//			typename initializer_list<T>::iterator ptrl = l.begin();
//			while (ptrl != l.end())
//				*ptrv++ = *ptrl++;
//		}
//
//		vector<T> operator=(initializer_list<T> l)
//		{
//			vector<T> tmp(l);
//			swap(_start, tmp._start);
//			swap(_end, tmp._end);
//			swap(_endofstorage, tmp._endofstorage);
//		}
//
//		iterator begin()
//		{
//			return _start;
//		}
//
//		iterator end()
//		{
//			return _end;
//		}
//	private:
//		iterator _start;
//		iterator _end;
//		iterator _endofstorage;
//	};
//}
//
//int main()
//{
//	initializer_list<int> l = { 1, 2 ,3,5 };
//	vector_addinitializer_list::vector<int> v(l);
//	vector_addinitializer_list::vector<int> v2{ 1, 5 ,6, 8,3 };
//	auto p = v2.begin();
//	while (p != v2.end())
//		cout << *p++ << "->";
//	cout << endl;
//
//	v = v2;
//	p = v.begin();
//	while (p != v.end())
//		cout << *p++ << "->";
//	cout << endl;
//}




//int main()
//{
//	int i = 2;
//	double d = 10.3;
//	cout << typeid(i).name() << endl;
//	cout << typeid(d).name() << endl;
//	//typeid(i).name() c;
//	decltype(d) f = i * d;
//	return 0;
//}


int sub(int x, int y)
{
	return y - x;
}

int main()
{
	int x = 1, y = 2;
	int&& c = 3;
	const int& w = 3;//const左值引用可以引用右值

	int&& sum = x + y;
	int&& s = sub(x, y);
	int&& r = move(x);
	return 0;
}