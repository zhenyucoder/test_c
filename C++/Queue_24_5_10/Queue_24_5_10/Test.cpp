//class Solution {
//public:
//    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
//        stack<int> st;
//        int popi = 0;
//        for (int i = 0; i < pushed.size(); i++)
//        {
//            st.push(pushed[i]);
//            while (st.size() && popped[popi] == st.top())
//                st.pop(), popi++;
//        }
//        return st.empty();
//    }
//};



//class Solution {
//public:
//    string decodeString(string s) {
//        vector<string> Strst; Strst.push_back("");
//        vector<int> Numst;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i] > '0' && s[i] <= '9')
//            {
//                int tmp = s[i] - '0';
//                while (i + 1 < s.size() && s[i + 1] >= '0' && s[i + 1] <= '9')
//                {
//                    i++;
//                    tmp = 10 * tmp + (s[i] - '0');
//                }
//                Numst.push_back(tmp);
//                cout << tmp << endl;
//            }
//            else if (s[i] == '[')
//            {
//                string tmp;
//                while (i + 1 < s.size() && s[i + 1] >= 'a' && s[i + 1] <= 'z')
//                    tmp += s[++i];
//                Strst.push_back(tmp);
//            }
//            else if (s[i] == ']')
//            {
//                int count = Numst.back(); Numst.pop_back();
//                int n = Strst.size(); string tmp = Strst[--n]; Strst.pop_back();
//                while (count--)
//                    Strst[n - 1] += tmp;
//            }
//            else
//            {
//                int pos = Strst.size();
//                Strst[pos - 1] += s[i];
//            }
//        }
//        return Strst[0];
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<Node*> q; q.push(root);
//        while (!q.empty())
//        {
//            vector<int> tmp;
//            int levelSize = q.size();
//            while (levelSize--)
//            {
//                Node* t = q.front();
//                q.pop();
//                tmp.push_back(t->val);
//                for (auto child : t->children)
//                    q.push(child);
//            }
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q; q.push(root);
//        bool flag = false;//表示是否遍历时需要逆序
//        while (q.size())
//        {
//            vector<int> cur;//当前层遍历结果
//            for (int levelsize = q.size(); levelsize > 0; levelsize--)
//            {
//                TreeNode* t = q.front();
//                q.pop();
//                cur.push_back(t->val);
//                //cout << flag <<"->";
//                if (t->left) q.push(t->left);
//                if (t->right) q.push(t->right);
//
//            }
//            if (flag)
//                reverse(cur.begin(), cur.end());
//            flag = !flag;
//            ret.push_back(cur);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        if (root == nullptr) return 0;
//        unsigned int ret = 1;
//        queue<pair<TreeNode*, unsigned int>> q; q.push({ root, 1 });
//        while (q.size())
//        {
//            ret = max(ret, q.back().second - q.front().second + 1);
//            unsigned int levelsize = q.size();
//            for (unsigned int i = 1; i <= levelsize; i++)
//            {
//                pair<TreeNode*, unsigned int> t = q.front(); q.pop();
//                if (t.first->left)
//                    q.push({ t.first->left, t.second * 2 });
//                if (t.first->right)
//                    q.push({ t.first->right, t.second * 2 + 1 });
//            }
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        vector<pair<TreeNode*, unsigned int>> queue;
//        unsigned int ret = 0;
//        queue.push_back({ root, 1 });
//        while (queue.size())
//        {
//            //更新结果
//            auto& [x1, y1] = queue[0];
//            auto& [x2, y2] = queue.back();
//            ret = max(ret, y2 - y1 + 1);
//            //跟新下一层
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            for (auto& [x, y] : queue)
//            {
//                if (x->left)
//                    tmp.push_back({ x->left, y * 2 });
//                if (x->right)
//                    tmp.push_back({ x->right, y * 2 + 1 });
//            }
//            queue = tmp;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root) {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q; q.push(root);
//        while (q.size())
//        {
//            int levelmax = q.front()->val;//当前层最大值
//            for (int levelsize = q.size(); levelsize > 0; levelsize--)
//            {
//                TreeNode* t = q.front(); q.pop();
//                levelmax = max(levelmax, t->val);
//                if (t->left)
//                    q.push(t->left);
//                if (t->right)
//                    q.push(t->right);
//            }
//            ret.push_back(levelmax);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    bool isValid(string s) {
//        if (s.size() % 2) return false;
//        stack<char> st;
//        for (auto ch : s)
//        {
//            if (ch == '(' || ch == '{' || ch == '[')
//            {
//                st.push(ch);
//            }
//            else
//            {
//                cout << st.size() << "->" << ch << endl;
//                if (st.size() && ((ch == ')' && st.top() == '(') ||
//                    (ch == ']' && st.top() == '[') || (ch == '}' && st.top() == '{')))
//                    st.pop();
//                else
//                    return false;
//            }
//        }
//        return st.empty();
//    }
//};




//class MyQueue {
//public:
//    stack<int> pushst;
//    stack<int> popst;
//    MyQueue() {
//
//    }
//
//    void push(int x) {
//        pushst.push(x);
//    }
//
//    int pop() {
//        int ret = 0;
//        if (popst.size())
//        {
//            ret = popst.top();
//            popst.pop();
//        }
//        else
//        {//将pushst中元素全部转移添加popst
//            while (pushst.size())
//            {
//                popst.push(pushst.top());
//                pushst.pop();
//            }
//            //将元素弹出
//            ret = popst.top();
//            popst.pop();
//        }
//        return ret;
//    }
//
//    int peek() {
//        if (popst.size() == 0)
//        {//将pushst中元素全部转移添加popst
//            while (pushst.size())
//            {
//                popst.push(pushst.top());
//                pushst.pop();
//            }
//        }
//        return popst.top();
//    }
//
//    bool empty() {
//        return popst.empty() && pushst.empty();
//    }
//};




//class MyStack {
//public:
//    queue<int> q1;
//    queue<int> q2;
//    MyStack() {
//    }
//
//    void push(int x) {
//        if (q1.empty())
//            q2.push(x);
//        else
//            q1.push(x);
//    }
//
//    int pop() {
//        int ret = 0;
//        if (q1.empty())
//        {//将q2前n-1个元素导入q2
//            while (q2.size() != 1)
//            {
//                q1.push(q2.front());
//                q2.pop();
//            }
//            ret = q2.front();
//            q2.pop();//删除目标元素
//        }
//        else
//        {
//            //将q1前n-1个元素导入q2
//            while (q1.size() != 1)
//            {
//                q2.push(q1.front());
//                q1.pop();
//            }
//            ret = q1.front();
//            q1.pop();//删除目标元素
//        }
//        return ret;
//    }
//
//    int top() {
//        return q1.empty() ? q2.back() : q1.back();
//    }
//
//    bool empty() {
//        return q1.empty() && q2.empty();
//    }
//};




//class Solution {
//public:
//    int calculate(string s) {
//        stack<long long> st;
//        char op = '+';//保存数字前的操作符
//        for (long long i = 0; i < s.size(); )
//        {
//            if (s[i] == ' ') i++;
//            else if (s[i] >= '0' && s[i] <= '9')//提取数字，运算
//            {
//                long long tmp = 0;
//                while (i < s.size() && s[i] >= '0' && s[i] <= '9')//提取数字
//                    tmp = tmp * 10 + (s[i++] - '0');
//                //运算
//                if (op == '+') st.push(tmp);
//                else if (op == '-') st.push(-tmp);
//                else if (op == '*')
//                    st.top() *= tmp;
//                else
//                    st.top() /= tmp;
//            }
//            else
//                op = s[i++];
//        }
//
//        //累加和
//        long long ret = 0;
//        while (!st.empty())
//        {
//            ret += st.top();
//            st.pop();
//        }
//        return ret;
//    }
//};




