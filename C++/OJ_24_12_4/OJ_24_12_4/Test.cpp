//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> ret;
//        int n = nums.size() - 1;
//        for (int i = n; i >= 2;)
//        {
//            int left = 0, right = i - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] + nums[i] > 0)
//                    right--;
//                else if (nums[left] + nums[right] + nums[i] < 0)
//                    left++;
//                else
//                {
//                    ret.push_back({ nums[left], nums[right], nums[i] });
//                    //处理相同元素
//                    left++, right--;
//                    while (left < right && nums[left - 1] == nums[left])
//                        left++;
//                    while (left < right && nums[right + 1] == nums[right])
//                        right++;
//                }
//            }
//            //处理相同元素
//            i--;
//            while (i >= 2 && nums[i + 1] == nums[i])
//                i--;
//        }
//        return ret;
//    }
//};




class Solution {
public:
    int compareVersion(string version1, string version2) {
        int n1 = version1.size(), n2 = version2.size();
        int cur1 = 0, cur2 = 0;
        while (cur1 < n1 || cur2 < n2)
        {
            long long val1 = 0;
            while (cur1 < n1 && version1[cur1] != '.') {
                val1 = val1 * 10 + version1[cur1] - '0';
                cur1++;
            }
            cur1++;
            long long val2 = 0;
            while (cur2 < n2&& version2[cur2] != '.') {
                val2 = val2 * 10 + version2[cur2] - '0';
                cur2++;
            }
            cur2++;
            if (val1 != val2) return val1 > val2 ? 1 : -1;
        }
        return 0;
    }
};