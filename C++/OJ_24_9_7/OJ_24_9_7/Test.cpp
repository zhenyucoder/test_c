//#include <iostream>
//#include <string>
//#include <cmath>
//using namespace std;
//
//bool Check(int x)
//{
//    if (x < 2) return false;
//    for (int i = 2; i <= sqrt(x); i++)
//    {
//        if (x % i == 0) return false;
//    }
//    return true;
//}
//
//int main()
//{
//    int maxn = 0, minn = 110;
//    int hash[26] = { 0 };
//    string str; cin >> str;
//    for (auto ch : str)
//    {
//        ++hash[ch - 'a'];
//    }
//
//    for (int i = 0; i < 26; i++)
//    {
//        if (hash[i])
//        {
//            maxn = max(hash[i], maxn);
//            minn = min(hash[i], minn);
//        }
//    }
//    bool ans = Check(maxn - minn);
//    if (ans)
//    {
//        cout << "Lucky Word" << endl;
//        cout << maxn - minn << endl;
//    }
//    else
//    {
//        cout << "No Answer" << endl;
//        cout << 0 << endl;
//    }
//    return 0;
//}



//bool hostschedule(vector<vector<int> >& schedule) {
//    sort(schedule.begin(), schedule.end());
//    for (int i = 1; i < schedule.size(); i++)
//    {
//        if (schedule[i][0] < schedule[i - 1][1]) return false;
//    }
//    return true;
//}



//int nums[510];
//int n, target;
//
//bool Check()
//{
//    for (int left = 0, right = 0, sum = 0; right < n; right++)
//    {
//        sum += nums[right];
//        while (sum > target)
//        {
//            sum -= nums[left++];
//        }
//        if (sum == target) return true;
//    }
//    return false;
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> nums[i];
//        target += nums[i];
//    }
//
//    if (target % 2)
//    {
//        cout << "false" << endl;
//    }
//    else
//    {
//        target /= 2;
//        bool ret = Check();
//        if (ret) cout << "true" << endl;
//        else cout << "false" << endl;
//    }
//    return 0;
//}



//string s;
//int main()
//{
//	cin >> s;
//	int ret = -1; 
//	int n = s.size();
//
//	for (int i = 0; i < n; i++)
//	{
//		if (i + 1 < n && s[i] == s[i + 1]) 
//		{
//			ret = 2;
//			break;
//		}
//		if (i + 2 < n && s[i] == s[i + 2])
//		{
//			ret = 3;
//		}
//	}
//
//	cout << ret << endl;
//
//	return 0;
//}



//const int N = 2e5 + 10;
//int n;
//int arr[N];
//int f[N], g[N];
//int main()
//{
//	cin >> n;
//	for (int i = 1; i <= n; i++) cin >> arr[i];
//	for (int i = 1; i <= n; i++)
//	{
//		f[i] = g[i - 1] + arr[i];
//		g[i] = max(f[i - 1], g[i - 1]);
//	}
//	cout << max(f[n], g[n]) << endl;
//	return 0;
//}


#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1e6 + 10;
int n, p;
int arr[N];
int main()
{
	cin >> n >> p;
	for (int i = 0; i < n; i++) cin >> arr[i];
	sort(arr, arr + n);

	int ret = 0, left = 0, right = 0;
	p *= 2;

	while (right < n)
	{
		while (arr[right] - arr[left] > p)
		{
			left++;
		}
		ret = max(ret, right - left + 1);
		right++;
	}

	cout << ret << endl;

	return 0;
}