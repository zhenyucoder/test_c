//class Solution {
//public:
//    int brokenCalc(int startValue, int target) {
//        int ret = 0;
//        while (startValue != target)
//        {
//            if (target < startValue)
//                target++;
//            else
//            {
//                if (target % 2 == 0)
//                    target /= 2;
//                else
//                    target++;
//            }
//            ret++;
//        }
//        return ret;
//    }
//};




class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        sort(intervals.begin(), intervals.end());
        int n = intervals.size();
        vector<vector<int>> ret;
        for (int i = 0; i < n;)
        {
            int left = intervals[i][0], right = intervals[i][1];
            while (i < n && right >= intervals[i][0])
                right = max(right, intervals[i][1]), i++;
            ret.push_back({ left, right });
        }
        return ret;
    }
};