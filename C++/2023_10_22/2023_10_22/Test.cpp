#include <iostream>
using namespace std;

//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}
//void Swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}


//模板
//template<class T>
//template<typename T>
//void Swap(T& left, T& right)
//{
//	T temp = left;
//	left = right;
//	right = temp;
//}
//
//int main()
//{
//	int x = 10, y = 30;
//	Swap(x, y);
//
//	int d1 = 10.123, d2 = 123.12;
//	Swap(d1, d2);
//	return 0;
//}



//template<class T>
//class Stack
//{
//public:
//	Stack(size_t capacity = 4)
//	{
//		cout << "Stack()" << endl;
//
//		_array = new T[capacity];
//
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	void Push(const T& data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//
//		delete[] _array;
//		_array = nullptr;
//		_size = _capacity = 0;
//	}
//private:
//	T* _array;
//	int _capacity;
//	int _size;
//};
//
//
//int main()
//{
//	Stack<int> st1; 
//	Stack<double> st2; 
//
//	st1.Push(1);
//	st1.Push(2);
//
//
//	st2.Push(1.2);
//	st2.Push(1.2);
//	return 0;
//}



//template <typename T>
//T Add(const T& x,const T& y)
//{
//	return x + y;
//}
//
//int main()
//{
//	cout << Add(1, 2) << endl;
//	cout << Add(1.2, 3.5) << endl;
//
//	cout << Add((double)1, 3.5) << endl;
//	cout << Add(1, (int)3.5) << endl;
//
//	cout << Add<int>(1, 2.3) << endl;
//	cout << Add<double>(1, 2.3) << endl;
//	return 0;
//}



int Add(const int x, const int y)
{
	return x + y;
}

template <typename T>
T Add(const T& x, const T& y)
{
	return x + y;
}

int main()
{
	Add(1, 2);//显示和通用同时存在时，走显示的
	return 0;
}


template<class T>
class Vector
{
public:
	Vector(size_t capacity = 10)
		: _pData(new T[capacity])
		, _size(0)
		, _capacity(capacity)
	{}

	// 使用析构函数演示：在类中声明，在类外定义。
	~Vector();

	void PushBack(const T& data)；
		void PopBack()；
		// ...

		size_t Size() { return _size; }

	T& operator[](size_t pos)
	{
		assert(pos < _size);
		return _pData[pos];
	}

private:
	T* _pData;
	size_t _size;
	size_t _capacity;
};

template <typename T>
Vector<T>::~Vector()
{
	delete[] _pData;
	_pData = nullptr;
}

int main()
{
	//Vector<int> v;

    vector<int> v;
    v.push_back(1);
	return 0;
}