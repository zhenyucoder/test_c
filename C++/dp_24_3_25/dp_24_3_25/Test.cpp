//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = -prices[0];//第1天买人股票
//        //填表
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};



//class Solution {
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        //初始化
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee);
//        }
//        return dp[n - 1][1];
//    }
//};


//class Solution {
//public:
//    const int KNF = 0x3f3f3f3f;
//    int maxProfit(int k, vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(k + 1, -KNF));
//        auto g = f;
//        //初始化
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//            for (int j = 0; j <= k; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1)
//                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//
//        int ret = -KNF;
//        for (int j = 0; j <= k; j++)
//            ret = max(ret, g[n - 1][j]);
//        return ret;
//    }
//};



//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        //数组和
//        int sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//        }
//        int n = nums.size();
//        vector<int> f(n + 1);//普通数组最大连续子数组和
//        auto g = f;//普通数组最小连续子数组和
//        int fmax = INT_MIN, gmin = INT_MAX;//分别统计f中最大值，g中最小值
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(nums[i - 1], f[i - 1] + nums[i - 1]);
//            fmax = max(fmax, f[i]);
//            cout << f[i] << ":" << fmax << " ";
//            g[i] = min(nums[i - 1], g[i - 1] + nums[i - 1]);
//            gmin = min(gmin, g[i]);
//            cout << g[i] << ":" << gmin << endl;
//        }
//        cout << " " << fmax << " " << gmin << endl;
//        cout << " " << fmax << " " << sum - gmin << endl;
//        return sum == gmin ? fmax : max(fmax, sum - gmin);//判断数组元素是否全为负数
//    }
//};



//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1, 1);//连续子数组最大乘积
//        auto g = f;//连续子数组最小乘积
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = nums[i - 1] < 0 ? max(nums[i - 1], nums[i - 1] * g[i - 1]) : max(nums[i - 1], nums[i - 1] * f[i - 1]);
//            g[i] = nums[i - 1] > 0 ? min(nums[i - 1], nums[i - 1] * g[i - 1]) : min(nums[i - 1], nums[i - 1] * f[i - 1]);
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};

class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int n = nums.size();
        //f[i]:以i为结尾，乘积为正的最长子数组长度；g[i]：以i为结尾，乘积为负的最长子数组长度
        vector<int> f(n + 1), g(n + 1);
        int ret = INT_MIN;
        for (int i = 1; i <= n; i++)
        {
            if (nums[i - 1] > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
            }
            else if (nums[i - 1] < 0)
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            ret = max(ret, f[i]);
        }
        return ret;
    }
};