//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int m, n;
//    cin >> n >> m;
//    int ans = n, count = m - 1;
//    while (count--)
//    {
//        ans *= (n - 1);
//        ans %= 109;
//    }
//    cout << ans << endl;
//    return 0;
//}



//
//#include <iostream>
//#include <string>
//#include <queue>
//#include <cstring>
//using namespace std;
//int dx[4] = { 1, -1, 0, 0 };
//int dy[4] = { 0, 0, 1, -1 };
//
//const int N = 1010;
//int n, m, x1, y1, x2, y2;
//char arr[N][N];
//int dist[N][N];
//
//int bfs()
//{
//    if (arr[x2][y2] == '*') return -1;
//    memset(dist, -1, sizeof(dist));
//    queue<pair<int, int>> q;
//    dist[x1][y1] = 0;
//    q.push({ x1, y1 });
//    while (!q.empty())
//    {
//        auto [a, b] = q.front();
//        q.pop();
//        for (int k = 0; k < 4; k++)
//        {
//            int x = a + dx[k], y = b + dy[k];
//            if (x > 0 && x <= n && y > 0 && y <= m
//                && arr[x][y] == '.' && dist[x][y] == -1)
//            {
//                dist[x][y] = dist[a][b] + 1;
//                q.push({ x, y });
//                if (x == x2 && y == y2) return dist[x][y];
//            }
//        }
//    }
//    return -1;
//}
//
//int main()
//{
//    cin >> n >> m >> x1 >> y1 >> x2 >> y2;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            cin >> arr[i][j];
//
//    cout << bfs() << endl;
//    return 0;
//}




class Solution
{
public:
	int minmumNumberOfHost(int n, vector<vector<int> >& startEnd)
	{
		sort(startEnd.begin(), startEnd.end());
		priority_queue<int, vector<int>, greater<int>> heap; 
		heap.push(startEnd[0][1]); 
		for (int i = 1; i < n; i++)
		{
			int a = startEnd[i][0], b = startEnd[i][1];
			if (a >= heap.top())
			{
				heap.pop();
				heap.push(b);
			}
			else 
			{
				heap.push(b); 
			}
		}
		return heap.size();
	}
};