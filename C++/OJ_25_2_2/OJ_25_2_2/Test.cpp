//class Solution {
//public:
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* prev = head, * cur = head->next;
//        int prevVal = head->val;
//
//        while (cur)
//        {
//            if (cur->val == prevVal) // 去重
//            {
//                ListNode* next = cur->next;
//                delete cur;
//                cur = next;
//                continue;
//            }
//
//            prevVal = cur->val;
//            ListNode* next = cur->next;
//
//            prev->next = cur;
//            prev = prev->next;
//            cur = next;
//        }
//        prev->next = nullptr;
//        return head;
//    }
//};



//class Solution {
//public:
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//
//        ListNode* cur = head, * newhead = new ListNode(0), * prev = newhead;
//        while (cur)
//        {
//            if (cur->next && cur->val == cur->next->val)
//            {
//                while (cur->next && cur->val == cur->next->val)
//                    cur = cur->next;
//                cur = cur->next;
//                continue;
//            }
//            prev->next = cur;
//            prev = prev->next;
//            cur = cur->next;
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};


//class Solution {
//public:
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = new ListNode(0);
//        newhead->next = head;
//
//        ListNode* cur = newhead;
//        while (cur->next && cur->next->next)
//        {
//            if (cur->next->val == cur->next->next->val)
//            {
//                int x = cur->next->val;
//                while (cur->next && cur->next->val == x)
//                {
//                    ListNode* tmp = cur->next;
//                    cur->next = cur->next->next;
//                    delete tmp;
//                }
//            }
//            else
//            {
//                cur = cur->next;
//            }
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};



class Solution {
public:
    ListNode* reverse(ListNode* head) {
        //前序节点
        ListNode* newhead = new ListNode(0), * cur = head;
        while (cur)
        {
            ListNode* next = cur->next;
            cur->next = newhead->next;
            newhead->next = cur;
            cur = next;
        }
        cur = newhead->next;
        delete newhead;
        return cur;
    }
    bool isPail(ListNode* head) {
        if (head == nullptr || head->next == nullptr) return true;
        ListNode* slow = head, * fast = head;
        while (fast && fast->next)
        {
            slow = slow->next;
            fast = fast->next->next;
        }


        ListNode* cur1 = head, * cur2 = reverse(slow->next);
        slow->next = nullptr;
        while (cur1 && cur2)
        {
            if (cur1->val != cur2->val) return false;
            cur1 = cur1->next;
            cur2 = cur2->next;
        }
        return true;
    }
};