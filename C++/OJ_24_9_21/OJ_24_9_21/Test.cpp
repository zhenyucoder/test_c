﻿//#include <iostream>
//using namespace std;
//int n, m;
//bool choose[11]; // 标记路径中选了哪些数
//int sum; // 标记选了的数的和
//void dfs(int x)
//{
//	if (sum == m)
//	{
//		for (int i = 1; i <= n; i++)
//		{
//			if (choose[i]) cout << i << " ";
//		}
//		cout << endl;
//		return;
//	}
//	if (sum > m || x > n) return;
//	// 选
//	sum += x;
//	choose[x] = true;
//	dfs(x + 1);
//	sum -= x;
//	choose[x] = false;
//	// 不选
//	dfs(x + 1);
//}
//int main()
//{
//	cin >> n >> m;
//	dfs(1);
//	return 0;
//}




//#include <iostream>
//#include <string>
//using namespace std;
//const int N = 1010;
//string a, b;
//int dp[N][N];
//int main()
//{
//	cin >> a >> b;
//	int n = a.size(), m = b.size();
//	for (int j = 0; j <= m; j++) dp[0][j] = j;
//	for (int i = 0; i <= n; i++) dp[i][0] = i;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= m; j++)
//		{
//			if (a[i - 1] == b[j - 1]) dp[i][j] = dp[i - 1][j - 1];
//			else dp[i][j] = min(min(dp[i - 1][j], dp[i][j - 1]), dp[i - 1][j -
//				1]) + 1;
//		}
//	}
//	cout << dp[n][m] << endl;
//	return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//int main()
//{
//	string s;
//	cin >> s;
//	bool hash[10] = { 0 };
//	for (int i = s.size() - 1; i >= 0; i--)
//	{
//		int x = s[i] - '0';
//		if (!hash[x])
//		{
//			cout << x;
//			hash[x] = true;
//		}
//	}
//
//	return 0;
//}




//typedef long long LL;
//int n;
//int main()
//{
//	cin >> n;
//	priority_queue<LL, vector<LL>, greater<LL>> heap;
//	while (n--)
//	{
//		LL x;
//		cin >> x;
//		heap.push(x);
//	}
//	// 构建最优⼆叉树 / 构建哈夫曼树
//	LL ret = 0;
//	while (heap.size() > 1)
//	{
//		LL t1 = heap.top(); heap.pop();
//		LL t2 = heap.top(); heap.pop();
//		heap.push(t1 + t2);
//		ret += t1 + t2;
//	}
//	cout << ret << endl;
//	return 0;
//}



typedef long long LL;
const int N = 1e5 + 10;
int n;
char s[N];
LL f[26];
LL g[26];
int main()
{
	cin >> n >> s;

	LL ret = 0;
	for (int i = 0; i < n; i++)
	{
		int x = s[i] - 'a';
		ret += f[x];
		f[x] = f[x] + i - g[x];
		g[x] = g[x] + 1;
	}
	cout << ret << endl;
	return 0;
}