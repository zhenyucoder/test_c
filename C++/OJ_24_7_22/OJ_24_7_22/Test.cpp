//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        const int MOD = 1e9 + 7;
//        int m = s.size(), n = t.size();
//        s = ' ' + s, t = ' ' + t;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 0; i <= m; i++)
//            dp[i][0] = 1;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (s[i] == t[j])
//                    dp[i][j] = (dp[i - 1][j - 1] + dp[i - 1][j]) % MOD;
//                else
//                    dp[i][j] = dp[i - 1][j];
//            }
//        }
//        return dp[m][n];
//    }
//};



//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        s = ' ' + s, p = ' ' + p;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        dp[0][0] = true;
//        for (int j = 1; j <= n; j++)
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (p[j] == '?')
//                    dp[i][j] = dp[i - 1][j - 1];
//                else if (p[j] == '*')
//                    dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
//                else
//                    dp[i][j] = (s[i] == p[j]) && dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};





//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        s = ' ' + s, p = ' ' + p;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        dp[0][0] = true;
//        for (int j = 2; j <= n; j += 2)
//            if (p[j] == '*') dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                if (p[j] == '*')
//                    dp[i][j] = dp[i][j - 2] || (p[j - 1] == '.' || s[i] == p[j - 1]) && dp[i - 1][j];
//                else
//                    dp[i][j] = (p[j] == '.' || s[i] == p[j]) && dp[i - 1][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};




//
//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size(), n = s2.size();
//        if (m + n != s3.size()) return false;
//        s1 = ' ' + s1, s2 = ' ' + s2, s3 = ' ' + s3;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        dp[0][0] = true;
//        for (int i = 1; i <= m; i++)
//            if (s1[i] == s3[i]) dp[i][0] = true;
//            else break;
//        for (int j = 1; j <= n; j++)
//            if (s2[j] == s3[j]) dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = (s1[i] == s3[i + j] && dp[i - 1][j]) || (s2[j] == s3[i + j] && dp[i][j - 1]);
//        return dp[m][n];
//    }
//};





//class Solution {
//public:
//    int minimumDeleteSum(string s1, string s2) {
//        int m = s1.size(), n = s2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//                if (s1[i - 1] == s2[j - 1])
//                    dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i - 1]);
//            }
//        }
//        int sum = 0;
//        for (auto x : s1)
//            sum += x;
//        for (auto x : s2)
//            sum += x;
//        return sum - dp[m][n] * 2;
//    }
//};




class Solution {
public:
    int findLength(vector<int>& nums1, vector<int>& nums2) {
        int m = nums1.size(), n = nums2.size(), ret = 0;
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = nums1[i - 1] == nums2[j - 1] ? dp[i - 1][j - 1] + 1 : 0;
                ret = max(ret, dp[i][j]);
            }
        return ret;
    }
};