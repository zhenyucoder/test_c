//class Solution {
//public:
//    int aimL = INT_MAX, amiR = INT_MIN, aim = 0;
//    vector<int> searchRange(vector<int>& nums, int target) {
//        aim = target;
//        _searchRange(nums, 0, nums.size() - 1);
//        // if(aimL == INT_MAX) return {-1, -1};
//        // else return {aimL, amiR};
//        return (aimL == INT_MAX) ? vector<int>({ -1, -1 }) : vector<int>({ aimL, amiR });
//    }
//
//    void _searchRange(vector<int>& nums, int l, int r)
//    {
//        if (l > r) return;
//        int mid = (l + r) >> 1;
//        if (nums[mid] > aim)
//            _searchRange(nums, l, mid - 1);
//        else if (nums[mid] < aim)
//            _searchRange(nums, mid + 1, r);
//        else
//        {
//            aimL = min(aimL, mid);
//            amiR = max(amiR, mid);
//            _searchRange(nums, l, mid - 1);
//            _searchRange(nums, mid + 1, r);
//        }
//    }
//};





//class Solution {
//public:
//    int lengthOfLastWord(string s) {
//        int index = s.size() - 1;
//        while (index >= 0 && s[index] == ' ') index--;
//        int len = 0;
//        while (index >= 0 && s[index] != ' ') len++, index--;
//        return len;
//    }
//};



//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        if (nums.size() == 0) return { -1, -1 };
//        //1. 查找目标区间最左值
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != target) return { -1, -1 };
//
//        // 存在目标区间
//        int aimL = left;
//
//        // 2. 查找目标区间最右值
//        left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else left = mid;
//        }
//        return { aimL, left };
//    }
//};




//class Solution {
//public:
//    int mySqrt(int x) {
//        long long left = 0, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            long long ans = mid * mid;
//            if (ans > x) right = mid - 1;
//            else left = mid;
//        }
//        return left;
//    }
//};




//class Solution {
//public:
//    bool validPalindrome(string s) {
//        for (int left = 0, right = s.size() - 1; left < right; left++, right--)
//        {
//            if (s[left] != s[right])
//                return Pailndroe(s, left, right - 1) || Pailndroe(s, left + 1, right);
//        }
//        return true;
//    }
//    bool Pailndroe(const string& s, int left, int right)
//    {
//        while (left < right)
//        {
//            if (s[left] != s[right]) return false;
//            left++, right--;
//        }
//        return true;
//    }
//};