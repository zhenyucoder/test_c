//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));//创建dp表
//        //初始化
//        dp[1][0] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int m = frame.size(), n = frame[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]) + frame[i - 1][j - 1];
//        return dp[m][n];
//    }
//};

//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int m = dungeon.size(), n = dungeon[0].size();
//        //创建dp表
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
//        //初始化
//        dp[m][n - 1] = dp[m - 1][n] = 1;
//        //填表
//        for (int i = m - 1; i >= 0; i--)
//            for (int j = n - 1; j >= 0; j--)
//            {
//                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
//                dp[i][j] = max(1, dp[i][j]);
//            }
//        return dp[0][0];
//    }
//};



//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        //创建dp表
//        vector<int> f(n + 1);//f[i]表示在[1, i],并且i号房屋偷的最高金额
//        vector<int> g = f;//g[i]表示在[1, i],并且i号房屋不偷的最高金额
//        //填表
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};



//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        return max(_rob(nums, 2, n), _rob(nums, 3, n - 1) + nums[0]);
//    }
//
//    int _rob(vector<int>& nums, int begin, int end)
//    {
//        int n = nums.size();
//        vector<int> f(n + 1);//f[i]表示[begin, i]号房屋中，i号房屋偷，偷窃到的最高金额
//        vector<int> g = f;//g[i]表示[begin, i]号房屋中，i号房屋不偷，偷窃到的最高金额
//        for (int i = begin; i <= end; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[end], g[end]);
//    }
//};


