#pragma once

namespace achieveList
{
	//节点
	template<class T>
	struct List_node
	{
		T _data;
		List_node<T>* _prev;
		List_node<T>* _next;

		List_node(const T& x = T())
			:_data(x)
			,_prev(nullptr)
			,_next(nullptr)
		{}
	};

	//迭代器封装
	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef List_node<T> Node;
		typedef __list_iterator<T,Ref, Ptr> self;
		Node* _node;

		__list_iterator(Node* node)
			:_node(node)
		{}

		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}

		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		self operator--(int)
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const self& s)
		{
			return _node != s._node;
		}

		bool operator==(const self& s)
		{
			return _node == s._node;
		}
	};

	//template<class T>
	//struct __list_const_iterator
	//{
	//	typedef List_node<T> Node;
	//	typedef  __list_const_iterator<T> self;
	//	Node* _node;

	//	__list_const_iterator(Node* node)
	//		:_node(node)
	//	{}

	//	self& operator++()
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}

	//	self& operator--()
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}

	//	self operator++(int)
	//	{
	//		self tmp(*this);
	//		_node = _node->_next;
	//		return tmp;
	//	}

	//	self operator--(int)
	//	{
	//		self tmp(*this);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	const T& operator*()
	//	{
	//		return _node->_data;
	//	}

	//	const T* operator->()
	//	{
	//		return &_node->_data;
	//	}

	//	bool operator!=(const self& s)
	//	{
	//		return _node != s._node;
	//	}

	//	bool operator==(const self& s)
	//	{
	//		return _node == s._node;
	//	}
	//};

	//list模拟实现
	template<class T>
	class List
	{
		typedef List_node<T> Node;
	public:
		typedef __list_iterator<T, T&, T*> iterator;
		typedef __list_iterator<T, const T&, const T*> const_iterator;
		//typedef __list_const_iterator<T> const_iterator;

		const_iterator begin()const
		{
			//return iterator(_head->_next);
			return _head->_next;
		}

		const_iterator end()const
		{
			return _head;

		}

		iterator begin()
		{
			//return iterator(_head->_next);
			return _head->_next;
		}

		iterator end()
		{
			return _head;
		}

		//初始化
		void empty_Init()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;

			_size = 0;
		}

		//无参构造
		List()
		{
			empty_Init();
		}

		//拷贝构造
		List(const List<T>& It)
		{
			empty_Init();
			for (auto e : It)
			{
				push_back(e);
			}
		}

		void swap(const List<T>& It)
		{
			std::swap(_head, It._head);
		}

		//赋值重载
		List<T>& operator=(const List<T> It)
		{
			swap(It);
			return *this;
		}

		//析构函数
		~List()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		/*void push_back(const T& x)
		{
			Node* tail = _head->_prev;
			Node* newnode = new Node(x);

			tail->_next = newnode;
			newnode->_prev = tail;

			_head->_prev = newnode;
			newnode->_next = _head;
		}*/
		void push_back(const T& x)
		{
			insert(end(), x);
		}

		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		void pop_back()
		{
			erase(--end());
		}

		void pop_front()
		{
			erase(begin());
		}

		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* newnode = new Node(x);

			prev->_next = newnode;
			newnode->_prev = prev;

			newnode->_next = cur;
			cur->_prev = newnode;
			_size++;
			return newnode;
		}

		iterator erase(iterator pos)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;
			delete cur;
			prev->_next = next;
			next->_prev = prev;

			_size--;
			return next;
		}

		int size()
		{
			return _size;
		}
	private:
		Node* _head;
		int _size;
	};

	void test_list1()
	{
		List<int> It;
		It.push_back(1);
		It.push_back(2);
		It.push_back(3);
		It.push_back(4);
		It.push_back(5);

		for (auto e : It)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_list2()
	{
		List<int> It;
		It.push_back(1);
		It.push_back(2);
		It.push_back(3);
		It.push_back(4);
		It.push_back(5);

		for (auto e : It)
		{
			cout << e << " ";
		}
		cout << endl;

		It.push_back(0);
		It.pop_front();

		for (auto e : It)
		{
			cout << e << " ";
		}
		cout << endl;

		List<int> It1 = It;

		for (auto e : It)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	struct AA
	{
		AA(int a1 = 0, int a2 = 0)
			:_a1(a1)
			, _a2(a2)
		{}

		int _a1;
		int _a2;
	};
	void test_list3()
	{
		List<AA> lt;
		lt.push_back(AA(1, 2));
		lt.push_back(AA(3, 4));
		lt.push_back(AA(4, 5));
		lt.push_back(AA(5, 6));
		lt.push_back(AA(6, 7));
		
		List<AA>::iterator it = lt.begin();
		while (it != lt.end())
		{
			cout << it->_a1 << " " << it->_a2 << endl;
			++it;
		}
		cout << endl;
	}


	//void print_list(const List<int>& lt)
	//{
	//	List<int>::const_iterator it = lt.begin();
	//	while (it != lt.end())
	//	{
	//		cout << *it << " ";
	//		it++;
	//	}
	//	cout << endl;
	//}

	//template<class T>
	//void print_list(const List<T>& lt)
	//{
	///*	List<T>::const_iterator it = lt.begin();
	//	while (it != lt.end())
	//	{
	//		cout << *it << " ";
	//		it++;
	//	}
	//	cout << endl;*/

	//	for (auto e : lt)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//}

	template<typename Container>
	void print_list(const Container& con)
	{
		typename Container::const_iterator it = con.begin();
		while (it != con.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}

	void test_list4()
	{
		List<int> Lt;
		Lt.push_back(1);
		Lt.push_back(2);
		Lt.push_back(3);
		Lt.push_back(4);
		Lt.push_back(5);

		print_list(Lt);

		List<string> it;
		it.push_back("11111111111111111");
		it.push_back("11111111111111111");
		it.push_back("11111111111111111");
		it.push_back("11111111111111111");
		it.push_back("11111111111111111");
		print_list(it);

		vector<string> v;
		v.push_back("222222222222222222222");
		v.push_back("222222222222222222222");
		v.push_back("222222222222222222222");
		v.push_back("222222222222222222222");
		print_list(v);
	}
}