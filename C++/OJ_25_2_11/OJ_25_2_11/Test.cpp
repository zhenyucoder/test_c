//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i <= nums.size(); i++)
//            ret ^= i;
//        for (auto x : nums)
//            ret ^= x;
//        return ret;
//    }
//}; 


class MinStack {
public:
    MinStack() {

    }

    void push(int val) {
        st.push(val);
        if (minst.empty() || minst.top() >= val)
            minst.push(val);
    }

    void pop() {
        if (minst.top() == st.top())
            minst.pop();
        st.pop();
    }

    int top() {
        return st.top();
    }

    int getMin() {
        return minst.top();
    }
    stack<int> st;
    stack<int> minst;
};