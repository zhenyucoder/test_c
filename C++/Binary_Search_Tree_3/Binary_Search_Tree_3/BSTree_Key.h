#pragma once

#include <iostream>
using namespace std;


//key模型
namespace Key
{
	template<class K>
	struct BSTreeNode
	{
		K _key;
		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;

		//节点构造函数
		BSTreeNode(const K& key)
			:_key(key)
			, _right(nullptr)
			, _left(nullptr)
		{}
	};


	template<class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		//插入数据
		bool insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				parent = cur;
				if (cur->_key > key)
					cur = cur->_left;
				else if (cur->_key < key)
					cur = cur->_right;
				else
					return false;
			}

			//链接插入数据
			Node* node = new Node(key);
			if (parent->_key > key)
				parent->_left = node;
			else
				parent->_right = node;

			return true;
		}

		//删除数据
		bool Erase(const K& key)
		{
			if (_root == nullptr)
				return false;

			Node* cur = _root;
			Node* parent = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{//删除数据

					//1.待删除节点左子树为空
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (parent->_key > cur->_key)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}

						delete cur;
					}
					else if (cur->_right == nullptr)//2.待删除节点右子树为空
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (parent->_key > cur->_key)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}

						delete cur;
					}
					else//3.待删除节点左右子树都不为空
					{
						//查找右子树最小值节点
						Node* parent = cur;
						Node* subLeft = cur->_right;
						while (subLeft->_left)
						{
							parent = subLeft;
							subLeft = subLeft->_left;
						}

						//交换元素
						swap(subLeft->_key, cur->_key);

						//删除subLeft节点
						if (parent->_key > subLeft->_key)
						{
							parent->_left = subLeft->_left;
						}
						else
						{
							parent->_right = subLeft->_left;
						}

						delete subLeft;
					}
					return true;
				}
			}
			return false;
		}

		//查找数据
		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key == key)
					return true;
				else if (cur->_key > key)
					cur = cur->_left;
				else
					cur = cur->_right;
			}
			return false;
		}

		//中序遍历
		void Inorder()
		{
			_Inorder(_root);
			cout << endl;
		}
		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;
			_Inorder(root->_left);
			cout << root->_key << " ";
			_Inorder(root->_right);
		}

		//查找:递归版
		bool FindR(const K& key)
		{
			return _FindR(_root, key);
		}

		//插入:递归版
		bool InsertR(const K& key)
		{
			return _InsertR(_root, key);
		}

		//删除:递归版
		bool EraseR(const K& key)
		{
			_EraseR(_root, key);
		}

		~BSTree()
		{
			Destory(_root);
		}

		//默认构造函数
		BSTree()
			:_root(nullptr)
		{}

		//拷贝构造函数
		BSTree(const BSTree<K>& t)
		{
			_root = Copy(t._root);
		}

		//复制重载
		BSTree<K>& operator=(const BSTree<K> t)
		{
			swap(_root, t._root);
			return *this;
		}
	private:
		Node* Copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;
			Node* newRoot = new Node(root->_key);
			newRoot->_left = Copy(root->_left);
			newRoot->_right = Copy(root->_right);
			return newRoot;
		}

		void Destory(Node*& root)
		{
			if (root == nullptr)
				return;
			Destory(root->_right);
			Destory(root->_left);
			delete root;
			root = nullptr;
		}

		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr)
				return false;

			if (root->_key > key)
			{
				return _EraseR(root->_left, key);
			}
			else if (root->_left < key)
			{
				return _EraseR(root->_right, key);
			}
			else
			{//准备删除数据
				if (root->_left == nullptr)
				{
					Node* del = root;
					root = root->_right;
					delete del;
					return true;
				}
				else if (root->_right == nullptr)
				{
					Node* del = root;
					root = root->_left;
					delete del;
					return true;
				}
				else
				{
					Node* subleft = root->_right;
					while (subleft->_left)
					{
						subleft = subleft->_left;
					}

					swap(root->_key, subleft->_key);
					//转化为递归子问题
					return EraseR(root->_right, key);
				}
			}
		}

		bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}

			if (root->_key == key)
			{
				return false;
			}
			else if (root->_key > key)
			{
				return _InsertR(root->_left, key);
			}
			else
			{
				return _InsertR(root->_right, key);
			}

		}

		bool _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
				return false;

			if (root->_key == key)
				return true;
			else if (root->_key > key)
				return _FindR(root->_left, key);
			else
				return _FindR(root->_right, key);
		}

		Node* _root = nullptr;
	};
}