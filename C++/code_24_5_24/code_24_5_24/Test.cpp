//int main()
//{
//	int n; 
//	cin >> n;
//	vector<int> dp(n + 1, 1);
//	for (int i = 1; i <= n; i++)
//		for (int j = i - 1; j > 0; j--)
//			dp[i] += dp[j];
//	cout << dp[n] << endl;
//	return 0;
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	cout << (1 << (n - 1) << endl;
//	return 0;
//}


//char ch[26] = { 0 };
//int main()
//{
//	string str;
//	cin >> str;
//  char ch[26] = { 0 };
//	int ret = 0, n = str.size();
//	for (int left = 0, right = 0, kinds = 0; right < n; right++)
//	{
//		char in = str[right];
//		if (ch[in - 'a']++ == 0)
//			kinds++;
//		while (kinds > 2)
//		{
//			char out = str[left++];
//			if (--ch[out - 'a'] == 0)
//				kinds--;
//		}
//		ret = max(ret, right - left + 1);
//	}
//	cout << ret << endl;
//	return 0;
//}




//vector<string> ret;
//string path;
//int n;
//bool vis[11] = { 0 };
//string s;
//
//void dfs(int pos)
//{
//	if (pos == n)
//	{
//		ret.push_back(path);
//		return;
//	}
//
//	for (int i = 0; i < n; i++)
//	{
//		if (i > 0 && s[i] == s[i - 1] && !vis[i - 1])
//			continue;
//		path.push_back(s[i]);
//		vis[i] = true;
//		dfs(pos + 1);
//		//�ָ��ֳ�
//		path.pop_back();
//		vis[i] = false;
//	}
//}
//
//vector<string> Permutation(string str)
//{
//	n = str.size();
//	sort(str.begin(), str.end());
//	s = str;
//	dfs(0);
//	return ret;
//}



//int main()
//{
//	string str;
//	int count = 0, sum = 0, n = str.size();
//	for (int i = 0; i < n - 1; i++)
//		if (str[i] >= '0' && str[i] <= '9')
//		{
//			sum += (str[i] - '0') * count;
//			count++;
//		}
//	sum %= 11;
//	if (sum == str[n - 1] - '0' || (sum == 10 && str[n - 1] == 'X'))
//		cout << "right" << endl;
//	else
//	{
//		str[n - 1] = sum == 10 ? 'X' : sum + '0';
//		cout << str << endl;
//	}
//	return 0;
//}




class solution
{
	int m, n;
	int memo[1001][1001];
	int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
public:
	int dfs(vector<vector<int>>& matrix, int i, int j)
	{
		if (memo[i][j] != -1)
			return memo[i][j];
		int len = 1;
		for (int k = 0; k < 4; k++) 
		{
			int x = i + dx[k], y = j + dy[k];
			if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j])
				len = max(len, 1 + dfs(matrix, x, y));
		}
		memo[i][j] = len;
		return len;
	}

	int solve(vector<vector<int>>& matrix)
	{
		int ret = 0;
		m = matrix.size(), n = matrix[0].size();
		memset(memo, -1, sizeof memo);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				ret = max(ret, dfs(matrix, i, j));
			}
		}
		return ret;
	}
};