//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> ldp(n), rdp(n); // 前缀积（左右两个方向）
//        ldp[0] = rdp[n - 1] = 1;
//        for (int i = 1; i < n; i++)
//            ldp[i] = ldp[i - 1] * nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            rdp[i] = rdp[i + 1] * nums[i + 1];
//        vector<int> ret(n);
//        for (int i = 0; i < n; i++)
//            ret[i] = ldp[i] * rdp[i];
//        return ret;
//    }
//};



//class Solution {
//public:
//    void rotate(vector<int>& nums, int k) {
//        k %= nums.size();
//        if (k == 0) return;
//        reverse(nums.begin(), nums.end());
//        reverse(nums.begin(), nums.begin() + k);
//        reverse(nums.begin() + k, nums.end());
//        return;
//    }
//};




class Solution {
public:
    int gcd(int a, int b) 
    {
        while (b != 0) 
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    void rotate(vector<int>& nums, int k) 
    {
        if (nums.empty()) 
        {
            return;
        }
        int n = nums.size();
        k = k % n;
        int count = gcd(k, n);
        for (int start = 0; start < count; ++start) 
        {
            int current = start;
            int prev = nums[start];
            do 
            {
                int next = (current + k) % n;
                swap(nums[next], prev);
                current = next;
            } while (start != current);
        }
    }
};