//class Solution {
//public:
//    class Comp
//    {
//    public:
//        bool operator()(ListNode* l1, ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        priority_queue<ListNode*, vector<ListNode*>, Comp> pq;
//        for (auto& list : lists)
//            if (list) pq.push(list);
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        while (!pq.empty())
//        {
//            ListNode* tmp = pq.top();
//            pq.pop();
//
//            prev->next = tmp;
//            prev = prev->next;
//
//            if (tmp->next)
//                pq.push(tmp->next);
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



class Solution {
public:
    int ListLen(ListNode* head)
    {
        ListNode* cur = head;
        int len = 0;
        while (cur)
        {
            len++;
            cur = cur->next;
        }
        return len;
    }
    ListNode* reverseKGroup(ListNode* head, int k) {
        ListNode* newhead = new ListNode, * prev = newhead;
        ListNode* cur = head;
        int gap = ListLen(head) / k;
        std::cout << gap;
        for (int i = 0; i < gap; i++) // ��תk��
        {
            ListNode* end = cur;
            int count = k;
            while (count--)
            {
                ListNode* next = cur->next;
                cur->next = prev->next;
                prev->next = cur;
                cur = next;
            }
            prev = end;
        }
        prev->next = cur;

        prev = newhead->next;
        delete newhead;
        return prev;
    }
};