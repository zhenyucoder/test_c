//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 }, dy[4] = { 1, -1, 0, 0 };
//    int m, n;
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        m = image.size(), n = image[0].size();
//        if (image[sr][sc] == color)
//            return image;
//        dfs(image, sr, sc, image[sr][sc], color);
//        return image;
//    }
//
//    void dfs(vector<vector<int>>& image, int i, int j, int target, int color)
//    {
//        image[i][j] = color;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == target)
//            {
//                dfs(image, x, y, target, color);
//            }
//        }
//    }
//};




//class Solution {
//public:
//    int m, n;
//    bool vis[301][301];
//    int dx[4] = { 0, 0, 1, -1 }, dy[4] = { 1, -1, 0, 0 };
//    int numIslands(vector<vector<char>>& grid) {
//        int ret = 0;
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (grid[i][j] == '1' && vis[i][j] == false)
//                {
//                    dfs(grid, i, j);
//                    ret++;
//                }
//        return ret;
//    }
//
//    void dfs(vector<vector<char>>& grid, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && vis[x][y] == false)
//                dfs(grid, x, y);
//        }
//    }
//};




//class Solution {
//public:
//    int m, n, ret = 0;
//    bool vis[51][51];
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (grid[i][j] == 1 && !vis[i][j])
//                    ret = max(ret, dfs(grid, i, j));
//        return ret;
//    }
//
//    int dfs(vector<vector<int>>& grid, int i, int j)
//    {
//        int sum = 1;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 && !vis[x][y])
//                sum += dfs(grid, x, y);
//        }
//        return sum;
//    }
//};





//class Solution {
//public:
//    int m, n;
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    void solve(vector<vector<char>>& board) {
//        m = board.size(), n = board[0].size();
//        //处理标记矩阵边界
//        for (int i = 0; i < m; i++)
//        {
//            if (board[i][0] == 'O')
//                dfs(board, i, 0);
//            if (board[i][n - 1] == 'O')
//                dfs(board, i, n - 1);
//        }
//
//        for (int j = 0; j < n; j++)
//        {
//            if (board[0][j] == 'O')
//                dfs(board, 0, j);
//            if (board[m - 1][j] == 'O')
//                dfs(board, m - 1, j);
//        }
//
//
//        //还原边界， 替换有效区域字符
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (board[i][j] == '.')
//                    board[i][j] = 'O';
//                else if (board[i][j] == 'O')
//                    board[i][j] = 'X';
//    }
//
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        board[i][j] = '.';
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'O')
//                dfs(board, x, y);
//        }
//    }
//};



//class Solution {
//public:
//    int m, n;
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
//        m = heights.size(), n = heights[0].size();
//        vector<vector<bool>> pac(m, vector<bool>(n)), atl(m, vector<bool>(n));
//        for (int j = 0; j < n; j++)
//        {
//            dfs(heights, 0, j, pac);
//            dfs(heights, m - 1, j, atl);
//        }
//        for (int i = 0; i < m; i++)
//        {
//            dfs(heights, i, 0, pac);
//            dfs(heights, i, n - 1, atl);
//        }
//
//        vector<vector<int>> ret;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (pac[i][j] && atl[i][j])
//                    ret.push_back({ i, j });
//        return ret;
//    }
//
//    void dfs(vector<vector<int>>& heights, int i, int j, vector<vector<bool>>& vis)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && heights[i][j] <= heights[x][y])
//                dfs(heights, x, y, vis);
//        }
//    }
//};



//class Solution {
//	int m, n, k, ret;
//	int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//	bool vis[101][101];
//public:
//	int movingCount(int _m, int _n, int _k)
//	{
//		m = _m, n = _n, k = _k;
//		dfs(0, 0);
//		return ret;
//	}
//
//	void dfs(int i, int j)
//	{
//		ret++, vis[i][j] = true;
//		for (int k = 0; k < 4; k++)
//		{
//			int x = i + dx[k], y = j + dy[k];
//			if (x >= 0 && x < m && y >= 0 && y < n && !vis[i][j] && check(i, j))
//				dfs(x, y);
//		}
//	}
//
//	bool check(int x, int y)
//	{
//		int bitsum = 0;
//		while (x)
//		{
//			bitsum += x % 10;
//			x /= 10;
//		}
//		while (y)
//		{
//			bitsum += y % 10;
//			y /= 10;
//		}
//		return bitsum < k;
//	}
//};




class Solution {
public:
    int m, n;
    int dx[8] = { 1, -1, 0, 0, 1, 1, -1, -1 }, dy[8] = { 0, 0, 1, -1, -1, 1, -1, 1 };
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        m = board.size(), n = board[0].size();
        int i = click[0], j = click[1];
        if (board[i][j] == 'M')
        {
            board[i][j] = 'X';
            return board;
        }
        dfs(board, i, j);
        return board;
    }

    void dfs(vector<vector<char>>& board, int i, int j)
    {
        board[i][j] = 'B';
        int count = 0;//计算周围地雷的个数
        for (int k = 0; k < 8; k++)
        {
            int x = i + dx[k], y = j + dy[k];
            if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'M')
                count++;
        }

        if (count)
        {
            board[i][j] = '0' + count;
            return;
        }
        else
            for (int k = 0; k < 8; k++)
            {
                int x = i + dx[k], y = j + dy[k];
                if (x >= 0 && x < m && y >= 0 && y < n && board[x][y] == 'E')
                    dfs(board, x, y);
            }

    }
};