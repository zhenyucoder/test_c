//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        int five = 0, ten = 0;
//        for (auto x : bills)
//        {
//            if (x == 5)
//                five++;
//            else if (x == 10)
//            {
//                if (five == 0) return false;
//                five--;
//                ten++;
//            }
//            else
//            {
//                if (ten > 0 && five > 0)
//                    ten--, five--;
//                else if (five >= 3)
//                    five -= 3;
//                else
//                    return false;
//            }
//        }
//        return true;
//    }
//};




//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        priority_queue<double> pq;
//        double sum = 0, aim = 0;
//        for (auto x : nums)
//        {
//            pq.push(x);
//            sum += x;
//        }
//        sum /= 2.0;
//        int count = 0;
//        while (aim < sum)
//        {
//            double tmp = pq.top() / 2.0;
//            pq.pop();
//            aim += tmp;
//            count++;
//            pq.push(tmp);
//        }
//        return count;
//    }
//};



//class Solution {
//public:
//    string largestNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end(), [](int num1, int num2) {
//            return to_string(num1) + to_string(num2) > to_string(num2) + to_string(num1);
//            });
//        string ret;
//        for (auto x : nums)
//            ret += to_string(x);
//        return (ret[0] == '0') ? "0" : ret;
//    }
//};




class Solution {
public:
    int left = 0;
    int wiggleMaxLength(vector<int>& nums) {
        int len = 0, n = nums.size();
        if (n < 2) return n;
        for (int i = 0; i < n - 1; i++)
        {
            int right = nums[i + 1] - nums[i];
            if (right == 0)
                continue;
            if (right * left <= 0)
                len++;
            left = right;
        }
        return len + 1;
    }
};