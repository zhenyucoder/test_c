//class Solution {
//public:
//    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
//        int pos = m + n - 1, pos1 = m - 1, pos2 = n - 1;
//        while (pos1 >= 0 && pos2 >= 0)
//        {
//            if (nums1[pos1] > nums2[pos2])
//                nums1[pos--] = nums1[pos1--];
//            else
//                nums1[pos--] = nums2[pos2--];
//        }
//        if (pos2 >= 0)
//        {
//            for (int i = 0; i <= pos2; i++)
//                nums1[i] = nums2[i];
//        }
//    }
//};


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        if (n <= 1) return n;
//        int pos = 1;
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] != nums[pos - 1])
//                nums[pos++] = nums[i];
//        }
//        return pos;
//    }
//};



//class Solution {
//public:
//    int removeElement(vector<int>& nums, int val) {
//        int pos = 0, n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] != val)
//                nums[pos++] = nums[i];
//        }
//        return pos;
//    }
//};




//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* newhead = new ListNode(), * prev = newhead;
//        while (head)
//        {
//            if (head->val != val)
//            {
//                prev->next = head;
//                prev = prev->next;
//            }
//            head = head->next;
//        }
//        prev->next = nullptr;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};





//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newhead = new ListNode();
//        while (head)
//        {
//            ListNode* temp = head;
//            head = head->next;
//
//            // 头插
//            ListNode* next = newhead->next;
//            newhead->next = temp;
//            temp->next = next;
//        }
//        ListNode* ret = newhead->next;
//        delete newhead;
//        return ret;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* cur = head, * next = cur->next;
//        ListNode* newhead = reverseList(next);
//        cur->next = nullptr;
//        next->next = cur;
//        return newhead;
//    }
//};



//class Solution {
//public:
//    ListNode* middleNode(ListNode* head) {
//        ListNode* slow = head, * fast = head;
//        while (fast != nullptr && fast->next != nullptr)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        return slow;
//    }
//};



//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newhead = new ListNode(), * prev = newhead;
//        while (list1 && list2)
//        {
//            if (list1->val < list2->val)
//                prev->next = list1, prev = prev->next, list1 = list1->next;
//            else
//                prev->next = list2, prev = prev->next, list2 = list2->next;
//        }
//        if (list1) prev->next = list1;
//        if (list2) prev->next = list2;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//
//        if (list1->val < list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list2->next, list1);
//            return list2;
//        }
//    }
//};




//class Partition {
//public:
//    ListNode* partition(ListNode* pHead, int x) {
//        ListNode* lesshead = new ListNode(0), * lessprev = lesshead;
//        ListNode* greaterhead = new ListNode(0), * greaterprev = greaterhead;
//        while (pHead)
//        {
//            if (pHead->val < x)
//                lessprev->next = pHead, lessprev = lessprev->next;
//            else
//                greaterprev->next = pHead, greaterprev = greaterprev->next;
//            pHead = pHead->next;
//        }
//        lessprev->next = greaterhead->next;
//        greaterprev->next = nullptr;
//
//        lessprev = lesshead->next;
//        delete lesshead;
//        delete greaterhead;
//        return lessprev;
//    }
//};




//class PalindromeList {
//public:
//    ListNode* Reverse(ListNode* head)
//    {
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        while (head)
//        {
//            ListNode* tmp = head;
//            head = head->next;
//
//            tmp->next = newhead->next;
//            newhead->next = tmp;
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//    bool chkPalindrome(ListNode* A) {
//        ListNode* slow = A, * fast = A;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//
//        ListNode* cur1 = A, * cur2 = Reverse(slow);
//        while (cur1 && cur2)
//        {
//            if (cur1->val != cur2->val)
//                return false;
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return true;
//    }
//};




//class Solution {
//public:
//    int NodeSize(ListNode* head)
//    {
//        int ret = 0;
//        while (head)
//        {
//            ret++;
//            head = head->next;
//        }
//        return ret;
//    }
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        ListNode* longList = headA, * shortList = headB;
//        int s1 = NodeSize(headA), s2 = NodeSize(headB);
//        int gap = s1 - s2;
//        if (gap < 0)
//        {
//            gap = -gap;
//            longList = headB, shortList = headA;
//        }
//
//        while (gap--)
//        {
//            longList = longList->next;
//        }
//
//        while (longList && shortList)
//        {
//            if (longList == shortList) break;
//            longList = longList->next;
//            shortList = shortList->next;
//        }
//        return longList;
//    }
//};




//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
        //ListNode* fast = head, * slow = head;
        //while (fast && fast->next)
        //{
        //    fast = fast->next->next;
        //    slow = slow->next;
        //    if (fast == slow) return true;
        //}
//        return false;
//    }
//};



//class Solution {
//public:
//    int Size(ListNode* head, ListNode* meet)
//    {
//        ListNode* cur = head;
//        int ret = 0;
//        while (cur != meet)
//        {
//            cur = cur->next;
//            ret++;
//        }
//        return ret;
//    }
//    ListNode* detectCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return nullptr;
//        // 1 判断是否成环，以及相遇点
//        ListNode* fast = head, * slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (fast == slow) break;
//        }
//        if (fast == nullptr || fast->next == nullptr) return nullptr;
//
//        // 2 成环， 相遇点标记成为"两条链"
//        ListNode* longHead = head, * shortHead = fast->next, * meet = fast;
//        // 3 判断两链表相交点
//        int LS = Size(longHead, meet), SS = Size(shortHead, meet), gap = LS - SS;
//        if (gap < 0)
//        {
//            gap = -gap;
//            longHead = shortHead;
//            shortHead = head;
//        }
//        // 3.1 较长链表先走gap步
//        while (gap--)
//        {
//            longHead = longHead->next;
//        }
//
//        // 3.2 同时走
//        while (longHead && shortHead)
//        {
//            if (longHead == shortHead)
//                break;
//            longHead = longHead->next;
//            shortHead = shortHead->next;
//        }
//        return longHead;
//    }
//};




class Solution {
public:
    Node* copyRandomList(Node* head) {
        if (head == nullptr) return nullptr;
        //将原节点深拷贝连接到被复制节点后
        Node* cur = head;
        while (cur)
        {
            Node* next = cur->next;//记录下一个节点
            Node* copynode = new Node(cur->val);
            cur->next = copynode;
            copynode->next = next;
            cur = next;
        }
        //更新深拷贝节点random指向
        cur = head;
        while (cur)
        {
            Node* copynode = cur->next;
            Node* next = copynode->next;
            //跟新深拷贝节点random指向
            if (cur->random)
                copynode->random = cur->random->next;

            cur = next;
        }

        //恢复链表
        Node* newhead = new Node(0), * prev = newhead;
        cur = head;
        while (cur)
        {
            Node* next = cur->next->next;

            prev->next = cur->next;
            prev = prev->next;

            cur->next = next;
            cur = next;
        }
        prev = newhead->next;
        delete newhead;
        return prev;
    }
};
