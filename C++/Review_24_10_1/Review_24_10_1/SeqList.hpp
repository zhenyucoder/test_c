#pragma once 
#include <iostream>
#include <assert.h>

template <class T>
class SeqList
{
public:
	SeqList()
		:_arr((T*)malloc(4 * sizeof(T))), _size(0), _capacity(4)
	{ }
	~SeqList()
	{
		free(_arr);
		_arr = nullptr;
	}

	void CheckCapacity()
	{
		if (_size == _capacity)
		{
			T* tmp= (T*)realloc(_arr, sizeof(T) * _capacity * 2);
			if (tmp == nullptr)
			{
				perror("�ڲ�����ʧ��");
				exit(-1);
			}
			_arr = tmp;
			_capacity *= 2;
		}
	}

	void Print()
	{
		for (int i = 0; i < _size; i++)
		{
			std::cout << _arr[i] << " ";
		}
		std::cout << std::endl;
	}

	int Find(T x)
	{
		for (int i = 0; i < _size; i++)
		{
			if (_arr[i] == x)
				return i;
		}
		return -1;
	}
public:
	void Insert(int pos, T x)
	{
		CheckCapacity();
		assert(pos >= 0 && pos <= _size); // ����

		for (int i = _size - 1; i >= pos; i--)
			_arr[i + 1] = _arr[i];
		_arr[pos] = x;
		_size++;
	}

	void Erase(int pos)
	{
		assert(pos >= 0 && pos < _size);
		for (int i = pos; i < _size; i++)
		{
			_arr[i] = _arr[i + 1];
		}
		_size--;
	}

	void Pushback(T x)
	{
		Insert(_size, x);
	}

	void Popback()
	{
		Erase(_size - 1);
	}

	void Pushfront(T x)
	{
		Insert(0, x);
	}

	void Popfront()
	{
		Erase(0);
	}
private:
	T* _arr;
	int _size;
	int _capacity;
};


