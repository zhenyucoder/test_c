#include <iostream>
#include <memory>
#include <string>
#include <map>
using namespace std;
#include "SmartPtr.h"

int div()
{
	int a, b;
	cin >> a >> b;
	if (b == 0)
		throw invalid_argument("除0错误");
	return a / b;
}

void func()
{
	/*int* p1 = new int;
	int* p2 = new int(2);*/

	SmartPtr<int> sp1 = new int(10);
	SmartPtr<int> sp2 = new int(2);

	*sp2 = 1000;
	cout << *sp2 << endl;

	SmartPtr<pair<string, int>> sp = new pair<string, int>("xxxxxxx", 1);
	sp->first += 'y';
	cout << sp->first << endl;

	cout << *sp1 << endl;
	cout << div() << endl;
}


void test_smart_ptr1()
{
	try
	{
		func();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}
}

void test_smart_ptr2()
{
	SmartPtr<int> sp1 = new int(1);
	//不允许深拷贝，原因在于智能指针不是容器，资源不是属于指针的。指针只是用于管理资源。
	//所以当一个指针拷贝另一个指针时，本质上还是管理同一块资源
	SmartPtr<int> sp2 = sp1;
}


void test_auto_ptr1()
{
	CPP98achieve::auto_ptr<int> ap1 = new int(1);
	CPP98achieve::auto_ptr<int> ap2 = ap1;//C++98给出的办法是转移管理权 

	(*ap2)++;
	//(*ap1)++;//err转移管理权 ,对象悬空
}


void test_unique_ptr()
{
	CPP11_up_achieve::unique_ptr<int> up = new int(10);
	CPP11_up_achieve::unique_ptr<int> up1;
	//up1 = up;

}



void test_shared_ptr()
{
	Cpp11_Shared_ptr::shared_ptr<int> sp = new int(10);
	Cpp11_Shared_ptr::shared_ptr<int> sp3 = new int(11);
	sp = sp3;
	Cpp11_Shared_ptr::shared_ptr<int> sp1(sp3);

	Cpp11_Shared_ptr::shared_ptr<int> sp2;
	sp2 = sp1;

	cout << sp.use_count() << endl;
}



struct ListNode
{
	ListNode()
	{}

	~ListNode()
	{
		cout << "~ListNode" << endl;
	}

	int _data;
	Cpp11_Shared_ptr::weak_ptr<ListNode> _prev;
	Cpp11_Shared_ptr::weak_ptr<ListNode> _next;
};

void test_shared_ptr1()//循环引用,shared_ptr缺陷
{
	Cpp11_Shared_ptr::weak_ptr<ListNode> n1(new ListNode);
	Cpp11_Shared_ptr::weak_ptr<ListNode> n2(new ListNode);//不支持RAII

	n1->_next = n2;
	n2->_prev = n1;
}

void test_shared_ptr2()
{
	//shared_ptr<ListNode> sp(new ListNode[10], [](ListNode* ptr) {delete[] ptr; });
	Cpp11_Shared_ptr::shared_ptr<ListNode> sp(new ListNode[10], [](ListNode* ptr) {delete[] ptr; });
}

int main()
{	
	test_shared_ptr2();
	return 0;
}


