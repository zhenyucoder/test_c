//class Solution {
//public:
//    int memo[31];
//    int fib(int n) {
//        //初始化
//        memset(memo, -1, sizeof memo);
//        return dfs(n);
//    }
//
//    int dfs(int n)
//    {
//        //开始前，查找备忘录中是否已经存在
//        if (memo[n] != -1)//剪枝
//            return memo[n];
//        if (n == 0 || n == 1)
//        {//返回之前将结果放到备忘录
//            memo[n] = n;
//            return n;
//        }
//
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//};



//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> memo(m + 1, vector<int>(n + 1));
//        return dfs(memo, m, n);
//    }
//
//    int dfs(vector<vector<int>>& memo, int m, int n)
//    {
//        if (memo[m][n] != 0)//剪枝
//            return memo[m][n];
//        if (m == 0 || n == 0) return 0;
//        if (m == 1 && n == 1)
//        {
//            memo[m][n] = 1;
//            return memo[m][n];
//        }
//
//        memo[m][n] = dfs(memo, m - 1, n) + dfs(memo, m, n - 1);
//        return memo[m][n];
//    }
//};



//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < nums.size(); i++)
//            ret = max(ret, dfs(nums, i));
//        return ret;
//    }
//
//    int dfs(vector<int>& nums, int pos)
//    {
//        int ret = 1;
//        for (int i = pos + 1; i < nums.size(); i++)
//        {
//            if (nums[i] > nums[pos])
//                ret = max(ret, dfs(nums, i) + 1);
//        }
//        return ret;
//    }
//};


//优化
//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> memo(n);
//        int ret = 0;
//        for (int i = 0; i < n; i++)
//            ret = max(ret, dfs(nums, i, memo));
//        return ret;
//    }
//
//    int dfs(vector<int>& nums, int pos, vector<int>& memo)
//    {
//        if (memo[pos] != 0)//剪枝
//            return memo[pos];
//
//        int ret = 1;
//        for (int i = pos + 1; i < nums.size(); i++)
//        {
//            if (nums[i] > nums[pos])
//                ret = max(ret, dfs(nums, i, memo) + 1);
//        }
//        memo[pos] = ret;
//        return memo[pos];
//    }
//};



//class Solution {
//public:
//    int m, n;
//    int longestIncreasingPath(vector<vector<int>>& matrix) {
//        m = matrix.size(), n = matrix[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                ret = max(ret, dfs(matrix, i, j));
//        return ret;
//    }
//
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int dfs(vector<vector<int>>& matrix, int i, int j)
//    {
//        int ret = 1;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j])
//                ret = max(ret, dfs(matrix, x, y) + 1);
//        }
//        return ret;
//    }
//};

//优化
//class Solution {
//public:
//    int m, n;
//    int memo[201][201];//备忘录
//    int longestIncreasingPath(vector<vector<int>>& matrix) {
//        m = matrix.size(), n = matrix[0].size();
//        int ret = 0;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                ret = max(ret, dfs(matrix, i, j));
//        return ret;
//    }
//
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int dfs(vector<vector<int>>& matrix, int i, int j)
//    {
//        if (memo[i][j] != 0)//剪枝
//            return memo[i][j];
//        int ret = 1;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && matrix[x][y] > matrix[i][j])
//                ret = max(ret, dfs(matrix, x, y) + 1);
//        }
//        memo[i][j] = ret;
//        return memo[i][j];
//    }
//};



