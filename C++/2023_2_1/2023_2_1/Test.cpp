#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <utility>
using namespace std;


void test_set1()
{	
	set<int> s;
	s.insert(1);
	s.insert(7);
	s.insert(4);
	s.insert(7);
	s.insert(10);
	s.insert(2);
	s.insert(45);

	set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto x : s)
	{
		cout << x << " ";
	}
	cout << endl;

	pair<set<int>::iterator, bool> pr = s.insert(7);
	it = pr.first;
	cout << pr.second << endl;
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	vector<int> v;
	for (int i = 0; i < 10; i++)
	{
		v.push_back(10 * i);
	}
	s.insert(v.begin()+2, v.end());
	for (auto x : s)
	{
		cout << x << " ";
	}
	cout << endl;

	it = s.find(93);
	if (it != s.end())
	{
		cout << "Ԫ���Ѿ�����" << endl;
	}
	else
	{
		cout << "Ԫ�ز����ڣ�ͬʱ�Բ���Ԫ��" << endl;
		s.insert(93);
	}
	for (auto x : s)
	{
		cout << x << " ";
	}
	cout << endl;

	cout << s.erase(100) << endl;
	s.erase(s.begin(), s.end());
	for (auto x : s)
	{
		cout << x << " ";
	}
	cout << endl;

	size_t x = s.count(15);
	if (x == 1)
		cout << "x����" << endl;
	else
		cout << "x������" << endl;

}

void test_set2()
{
	set<int> s;
	s.insert(1);
	s.insert(7);
	s.insert(4);
	s.insert(7);
	s.insert(10);
	s.insert(2);
	s.insert(45);

	for (auto x : s)
	{
		cout << x << " ";
	}
	cout << endl;

	set<int>::iterator itlow, itup;
	itlow = s.lower_bound(2); //>=val
	itup = s.upper_bound(45); //>val
	while (itlow != itup)
	{
		cout << *itlow << " ";
		++itlow;
	}
	cout << endl;

	pair<set<int>::iterator, set<int>::iterator> p = s.equal_range(8);
	cout << *p.first << endl; // >=val
	cout << *p.second << endl; // >val
}


void test_multiset1()
{
	multiset<int> muls;
	for (int i = 1; i < 10; i++)
	{
		if(i >2 && i<6)
			muls.insert(10 * i);
		muls.insert(10 * i);
	}
	
	for (auto x : muls)
	{
		cout << x << " ";
	}
	cout << endl;

	multiset<int>::iterator p = muls.find(50);
	if (p != muls.end())
	{
		pair<multiset<int>::iterator, multiset<int>::iterator> pr = muls.equal_range(50);
		multiset<int>::iterator it = pr.first;
		cout << *pr.first << ":" << *pr.second << endl;
		muls.erase(pr.first, pr.second);
	}
	for (auto x : muls)
	{
		cout << x << " ";
	}
	cout << endl;
}


void test_map1()
{
	map<string, string> dict;
	dict.insert(make_pair("insert", "����"));
	dict.insert(make_pair("left", "���"));
	dict.insert(pair<string, string>("right", "�ұ�"));
	dict.insert(pair < string, string>("sort", "����"));
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first<<":"<<it->second<<endl;
		cout << dict[it->first] << endl;
		++it;
	}
	dict.erase("insert");
	for (auto x : dict)
	{
		cout << x.first << ":" << x.second << endl;
	}
	cout << endl;

	it = dict.find("right");
	if (it != dict.end())
	{
		cout << "���ݴ��ڣ���ɾ��" << endl;
		dict.erase(it);
	}
	for (auto x : dict)
	{
		cout << x.first << ":" << x.second << endl;
	}
	cout << endl;

	
}

void test_map2()
{
	map<string, string> dict;
	dict.insert(make_pair("insert", "����"));
	dict.insert(make_pair("left", "���"));
	dict.insert(pair<string, string>("right", "�ұ�"));
	dict.insert(pair < string, string>("sort", "����"));

	auto it = dict.lower_bound("left");
	while (it != dict.upper_bound("right"))
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;

	pair<map<string, string>::iterator, map<string, string>::iterator> p = dict.equal_range("insert");
	it = p.first;
	while (it != p.second)
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test_multimap()
{
	multimap<string, string> dict;
	dict.insert(make_pair("insert", "����"));
	dict.insert(make_pair("left", "���"));
	dict.insert(pair<string, string>("right", "�ұ�"));
	dict.insert(pair < string, string>("sort", "����")); 
	dict.insert(make_pair("insert", "����"));
	dict.insert(make_pair("left", "���"));
	dict.insert(pair<string, string>("right", "�ұ�"));
	dict.insert(pair < string, string>("sort", "����"));

	pair<multimap<string, string>::iterator, multimap<string, string>::iterator> p = dict.equal_range("insert");
	auto it = p.first;
	while (it != p.second)
	{
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;

	dict.erase(p.first, p.second);
	for (auto x : dict)
	{
		cout << x.first << ":" << x.second << endl;
	}
	cout << endl;
}
int main()
{
	//test_map2();
	test_multimap();
	return 0;
}