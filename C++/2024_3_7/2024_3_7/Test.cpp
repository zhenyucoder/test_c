//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int>  dp(n, 1);
//        int ret = 1;
//
//        for (int i = 1; i < n; i++)
//        {
//            int x = nums[i];
//            int dp_max = 0;//查找[1, i -1]中nums比x大的所有数据中的最大dp值
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < x)
//                    dp_max = max(dp_max, dp[j]);
//            }
//            dp[i] += dp_max;
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n, 1), g(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                    f[i] = max(f[i], g[j] + 1);
//                else if (nums[j] > nums[i])
//                    g[i] = max(g[i], f[j] + 1);
//            }
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int findNumberOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> len(n, 1), count(n, 1);
//
//        int retlen = 1, retcount = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                {
//                    if (len[j] + 1 == len[i])
//                        count[i] += count[j];
//                    else if (len[j] + 1 > len[i])
//                        len[i] = len[j] + 1, count[i] = count[j];
//                }
//            }
//            //更新返回值
//            if (len[i] == retlen)
//                retcount += count[i];
//            else if (len[i] > retlen)
//                retlen = len[i], retcount = count[i];
//        }
//        return retcount;
//    }
//};


//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size(), ret = 1;
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (pairs[j][1] < pairs[i][0])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        unordered_map<int, int> hash;
//        //初始化
//        hash[arr[0]] = 1;
//        int ret = 0;
//        for (int i = 1; i < arr.size(); i++)
//        {
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            ret = max(ret, hash[arr[i]]);
//        }
//        return ret;
//    
//};


