#pragma once


namespace achieve_vector
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}

		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//3种构造
		vector()
		{}

		vector(size_t  n, const T& value = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		//防止定义vector<int>这种类型走迭代区间的构造函数
		vector(int n, const T& value = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		template<class InputIterator>//使用模板是为了，当数据类型匹配时就可以使用
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		//拷贝构造
		//vector(const vector& x) //库中实现模式, 直接使用类名。但C++模板中，类型不是类名
		vector(const vector<T>& v)
		{
			reserve(v.capacity());
			for (auto& e : v)
			{
				push_back(e);
			}
		}

		//赋值重载
		vector<T>& operator= (vector<T> tmp)
		{
			swap(tmp);
			return *this;
		}
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstroage, v._endofstroage);
		}


		~vector()
		{
			delete[] _start;
			_start = _finish = _endofstroage = nullptr;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		size_t capacity()const
		{
			return _endofstroage - _start;
		}

		size_t size()const
		{
			return _finish - _start;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t sz = size();//记录_finish 和 _start 的偏移量
				T* tmp = new T[n];
				if (_start)
				{
					//memcpy(tmp, _start, sizeof(T) * sz);
					for (size_t i = 0; i < sz; i++)
					{
						tmp[i] = _start[i];
					}
					delete[] _start;
				}

				_start = tmp;
				_finish = _start + sz;//不能用size()代替sz，否则会导致迭代器失效
				_endofstroage = _start + n;
			}
		}


		//const会延长匿名对象的生命周期, 匿名对象具有常性
		//模板出来后，对类进行了升级，内置类型也有构造函数
		//void resize(size_t n, T val = T())
		void resize(size_t n, const T& val = T())
		{
			if (n < size())
			{
				_finish = _start + n;
			}
			else
			{
				reserve(n);
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}

		const T& operator[](size_t pos)const
		{
			assert(pos < _finish);
			return _start[pos];
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		void push_back(const T& x)
		{
			//if (_finish == _endofstroage)
			//{
			//	reserve(capacity() == 0 ? 4 : capacity() * 2);
			//}

			////插入数据
			//*_finish = x;
			//_finish++;

			insert(_finish, x);
		}

		void pop_back()
		{
			erase(--end());
		}


		void insert(iterator pos, const T& x)
		{
			assert(pos >= _start);
			assert(pos <= _finish);
			if (_finish == _endofstroage)
			{
				size_t len = pos - _start;
				reserve(capacity() == 0 ? 4 : capacity() * 2);
				pos = _start + len;
			}

			//挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}

			//插入数据
			*pos = x;
			_finish++;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			iterator it = pos + 1;
			while (it < _finish)
			{
				*(it - 1) = *it;
				++it;
			}

			--_finish;

			return pos;
		}
	private:
		iterator _start = nullptr; // 指向数据块的开始
		iterator _finish = nullptr; // 指向有效数据的尾
		iterator _endofstroage = nullptr;// 指向存储容量的尾
	};

	void testvector1()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);

		for (size_t i = 0; i < v.size(); i++)
		{
			cout << v[i] << " ";
		}
		cout << endl;

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			it++;
		}
		cout << endl;
	}

	void testvector2()
	{
		vector<char*> v1;
		v1.resize(6);

		vector<string> v2;
		v2.resize(6);

		vector<string> v3;
		v3.resize(6, "xxx");
		for (auto v : v3)
		{
			cout << v << " ";
		}
		cout << endl;
	}

	void testvector3()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		//v.push_back(4);
		
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		v.insert(v.begin() + 2, 0);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int>::iterator it = v.begin()+2;
		v.insert(it, 0);//迭代器失效，扩容
		//insert中将it重置了，但关键在于传值。如果发生扩容，迭代器就失效
	/*	for (; it < v.end(); it++)//err
		{
			cout << *it << " ";
		}
		cout << endl;*/
		
	}

	void testvector4()
	{
		//std::vector<int> v;
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);
		auto it = v.begin();
		//迭代器失效
		while (it < v.end())
		{
			if (*it % 2 == 0)
			{
				v.erase(it);
			}
				it++;
		}

		/*while (it < v.end())
		{
			if (*it % 2 == 0)
			{
				it = v.erase(it);
			}
			else
			{
				it++;
			}
		}*/

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void testvector5()
	{
		vector<string> v;
		v.push_back("11111111111111");
		v.push_back("11111111111111");
		v.push_back("11111111111111");
		v.push_back("11111111111111");
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		v.push_back("11111111111111");
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void testvector6()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v1(v);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v2;
		v2 = v1;
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void testvector7()
	{
		vector<int> v1(5, 9);
		vector<string> v2(5, "xxxxxx");

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);

		vector<int> v3(v.begin() + 1, v.end() - 2);
		for (auto e : v3)
		{
			cout << e << " ";
		}
		cout << endl;
	}
};
