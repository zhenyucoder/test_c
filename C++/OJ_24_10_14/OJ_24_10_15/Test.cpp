//#include <iostream>
//#include <cstring>
//using namespace std;
//
//int n, V;
//int v[1010] = { 0 }, w[1010] = { 0 };
//int dp[1001] = { 0 };
//int main()
//{
//    cin >> n >> V;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//
//    // ques1
//    for (int i = 1; i <= n; i++)
//        for (int j = v[i]; j <= V; j++)
//            dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//    cout << dp[V] << endl;
//
//    /// ques2
//    memset(dp, 0, sizeof dp);
//    for (int i = 1; i <= V; i++)
//        dp[i] = -1;
//    for (int i = 1; i <= n; i++)
//        for (int j = v[i]; j <= V; j++)
//            if (dp[j - v[i]] != -1)
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//    cout << ((dp[V] == -1) ? 0 : dp[V]) << endl;
//    return 0;
//}



//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = coins[i - 1]; j <= amount; j++)
//            {
//                dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//            }
//        }
//        return (dp[amount] >= 0x3f3f3f3f) ? -1 : dp[amount];
//    }
//};



//class Solution {
//public:
//    int numSquares(int n) {
//        int x = sqrt(n);
//        vector<int> dp(n + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= x; i++)
//        {
//            for (int j = i * i; j <= n; j++)
//            {
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//            }
//        }
//        return dp[n];
//    }
//};




//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= strs.size(); i++)
//        {
//            // 统计i下标对应单词01个数
//            int zero = 0, one = 0;
//            for (auto ch : strs[i - 1])
//                if (ch == '0') zero++;
//                else one++;
//            // 填表
//            for (int j = m; j >= zero; j--)
//            {
//                for (int k = n; k >= one; k--)
//                {
//                    dp[j][k] = max(dp[j][k], dp[j - zero][k - one] + 1);
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};



