//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    bool check[7];//检查对应下标数据是否被使用过
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                permute(nums);
//                //回溯，恢复原始状态
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};

//
//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    bool check[8];
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        dfs(nums);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        unordered_map<int, bool> hash;//判断当前循环是否使用过相同的数
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && hash.count(nums[i]) == false)
//            {
//                hash[nums[i]] = true;
//                check[i] = true;
//                path.push_back(nums[i]);
//                dfs(nums);
//                //恢复现场
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        ret.push_back(path);
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos >= nums.size())
//            return;
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            ret.push_back(path);
//            dfs(nums, i + 1);
//            //回溯，恢复现场
//            path.pop_back();
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        //选pos位置元素
//        path.push_back(nums[pos]);
//        dfs(nums, pos + 1);
//        //pos位置元素不选
//        path.pop_back();
//        dfs(nums, pos + 1);
//    }
//};




//class Solution {
//public:
//    vector<string> ret;
//    string path;
//    string str[10] = { "abc", "def", "ghi", "jkl","mno", "pqrs", "tuv", "wxyz" };
//    vector<string> letterCombinations(string digits) {
//        if (digits.size() == 0)
//            return ret;
//        dfs(digits, 0);
//        return ret;
//    }
//
//    void dfs(string& digits, int pos)
//    {
//        if (digits.size() == pos)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        int n = str[digits[pos] - '2'].size();
//        for (int i = 0; i < n; i++)
//        {
//            path.push_back(str[digits[pos] - '2'][i]);
//            dfs(digits, pos + 1);
//            //恢复现场
//            path.pop_back();
//        }
//    }
//};


