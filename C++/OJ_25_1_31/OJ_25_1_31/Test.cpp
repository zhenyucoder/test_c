//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        return dfs(head);
//    }
//    ListNode* dfs(ListNode* head)
//    {
//        if (head->next == nullptr) return head;
//        ListNode* newhead = dfs(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//};




//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head) {
//        ListNode* newhead = new ListNode(0);
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* curnode = cur;
//            cur = cur->next;
//
//            // 头插
//            curnode->next = newhead->next;
//            newhead->next = curnode;
//        }
//        ListNode* prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseBetween(ListNode* head, int m, int n) {
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        ListNode* cur = head;
//        for (int i = 1; i < m; i++)
//        {
//            ListNode* node = cur;
//            cur = cur->next;
//
//            prev->next = node;
//            prev = prev->next;
//        }
//
//        prev->next = nullptr;
//        ListNode* next = cur;
//        // 带翻转区间
//        for (int i = m; i <= n; i++)
//        {// 头插
//            ListNode* node = cur;
//            cur = cur->next;
//
//            node->next = prev->next;
//            prev->next = node;
//        }
//
//        // 连接剩余链表节点
//        next->next = cur;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    ListNode* Merge(ListNode* pHead1, ListNode* pHead2) {
//        if (pHead1 == nullptr && pHead2 == nullptr) return pHead1;
//        if (pHead1 == nullptr || pHead2 == nullptr)
//            return pHead1 ? pHead1 : pHead2;
//        if (pHead1->val < pHead2->val)
//        {
//            pHead1->next = Merge(pHead1->next, pHead2);
//            return pHead1;
//        }
//        else
//        {
//            pHead2->next = Merge(pHead2->next, pHead1);
//            return pHead2;
//        }
//        return nullptr;
//    }
//};




//class Solution {
//public:
//    ListNode* Merge(ListNode* pHead1, ListNode* pHead2) {
//        if (pHead1 == nullptr && pHead2 == nullptr) return pHead1;
//        if (pHead1 == nullptr || pHead2 == nullptr)
//            return pHead1 ? pHead1 : pHead2;
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        ListNode* cur1 = pHead1, * cur2 = pHead2;
//        while (cur1 && cur2)
//        {
//            if (cur1->val < cur2->val)
//            {
//                prev->next = cur1;
//                prev = prev->next;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                prev->next = cur2;
//                prev = prev->next;
//                cur2 = cur2->next;
//            }
//        }
//        if (cur1) prev->next = cur1;
//        if (cur2) prev->next = cur2;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};





//class Solution {
//public:
//    class Cmp
//    {
//    public:
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        priority_queue<ListNode*, vector<ListNode*>, Cmp> head;
//        for (auto& list : lists)
//            if (list) head.push(list);
//
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        while (head.size())
//        {
//            ListNode* top = head.top();
//            head.pop();
//
//            prev->next = top;
//            prev = prev->next;
//
//            if (top->next)
//                head.push(top->next);
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return false;
//        ListNode* slow = head, * fast = head->next->next;
//        while (fast && fast->next)
//        {
//            if (slow == fast) return true;
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        return false;
//    }
//};



//class Solution {
//public:
//    ListNode* EntryNodeOfLoop(ListNode* pHead) {
//        // 是否有环
//        if (pHead == nullptr || pHead->next == nullptr) return nullptr;
//        ListNode* slow = pHead, * fast = pHead;
//        bool flag = false; // 标记是否有环
//        while (fast && fast->next)
//        {
//            slow = slow->next;
//            fast = fast->next->next;
//            if (slow == fast)
//            {// 有环，相遇
//                flag = true;
//                break;
//            }
//        }
//        if (flag == false) return nullptr;
//
//        ListNode* meet = slow; // 相遇点
//        ListNode* cur1 = pHead, * cur2 = meet;
//        while (cur1 != cur2)
//        {
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return cur1;
//    }
//};



//class Solution {
//public:
//    int ListLen(ListNode* pHead)
//    {
//        ListNode* cur = pHead;
//        int len = 0;
//        while (cur)
//        {
//            cur = cur->next;
//            len++;
//        }
//        return len;
//    }
//    ListNode* FindKthToTail(ListNode* pHead, int k) {
//        int len = ListLen(pHead);
//        if (len < k) return nullptr;
//
//        ListNode* cur = pHead;
//        for (int i = 0; i < len - k; i++)
//        {
//            cur = cur->next;
//        }
//        return cur;
//    }
//};




class Solution {
public:
	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
		ListNode* cur1 = pHead1, * cur2 = pHead2;
		while (cur1 != cur2)
		{
			cur1 = cur1 == nullptr ? pHead2 : cur1->next;
			cur2 = cur2 == nullptr ? pHead1 : cur2->next;
		}
		return cur1;
	}
};