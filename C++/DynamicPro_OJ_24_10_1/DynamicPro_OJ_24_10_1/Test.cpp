//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        bool dp[1010][1010] = { 0 };
//        for (int i = n - 1; i >= 0; i--)
//            for (int j = i; j < n; j++)
//                if (s[i] == s[j])
//                    dp[i][j] = (j - i + 1 <= 2) ? true : dp[i + 1][j - 1];
//
//        int pos = 0, len = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                if (dp[i][j] && (j - i + 1) > len)
//                {
//                    len = j - i + 1;
//                    pos = i;
//                }
//            }
//        }
//        return s.substr(pos, len);
//    }
//};



//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int pos = 0, len = 1, n = s.size();
//        for (int i = 1; i < n; i++)
//        {
//            // 1. 回文数为奇数个元素
//            int left = i - 1, right = i + 1;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] != s[right])
//                    break;
//                else
//                    left--, right++;
//            }
//            // 更新结果
//            if (right - left - 1 > len)
//            {
//                pos = left + 1;
//                len = right - left - 1;
//            }
//
//            // 2. 回文数为偶数个元素
//            left = i - 1, right = i;
//            while (left >= 0 && right < n)
//            {
//                if (s[left] != s[right])
//                    break;
//                else
//                    left--, right++;
//            }
//            if (right - left - 1 > len)
//            {
//                pos = left + 1;
//                len = right - left - 1;
//            }
//        }
//        return s.substr(pos, len);
//    }
//};



//class Solution {
//public:
//    int countSubstrings(string s) {
//        bool dp[1010][1010] = { 0 };
//        int n = s.size();
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = (j - i + 1 <= 2) ? true : dp[i + 1][j - 1];
//            }
//        }
//
//        int ret = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (dp[i][j])
//                    ret++;
//            }
//        }
//        return ret;
//    }
//};



