//class Solution {
//public:
//    int firstUniqChar(string s) {
//        unordered_map<char, int> hash;
//        // 将重复字符对应value设为INT_MAX，只出现一次的字符对应value设为下标
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (hash.count(s[i])) hash[s[i]] = INT_MAX;
//            else hash[s[i]] = i;
//        }
//        int ret = INT_MAX;
//        for (auto& [a, b] : hash)
//            ret = min(ret, b);
//        return (ret == INT_MAX) ? -1 : ret;
//    }
//};




class Solution {
public:
    int search(vector<int>& nums, int target) {
        int left = 0, right = nums.size() - 1;
        while (left <= right)
        {
            int mid = (left + right) >> 1;
            if (nums[mid] > target) right = mid - 1;
            else if (nums[mid] < target) left = mid + 1;
            else return mid;
        }
        return -1;
    }
};