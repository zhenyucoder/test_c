﻿//#include <iostream>
//using namespace std;
//const int N = 510;
//char g[N][N];
//int dp[N][N];
//int m, n;
//int main()
//{
//	cin >> m >> n;
//	for (int i = 1; i <= m; i++)
//	{
//		for (int j = 1; j <= n; j++)
//		{
//			cin >> g[i][j];
//		}
//	}
//	for (int i = 1; i <= m; i++)
//	{
//		for (int j = 1; j <= n; j++)
//		{
//			int t = 0;
//			if (g[i][j] == 'l') t = 4;
//			else if (g[i][j] == 'o') t = 3;
//			else if (g[i][j] == 'v') t = 2;
//			else if (g[i][j] == 'e') t = 1;
//			dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + t;
//		}
//	}
//	cout << dp[m][n] << endl;
//	return 0;
//}




//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;
//
//
//int main()
//{
//    long long num;
//    cin >> num;
//    string ret; int pos = 0;
//    while (num)
//    {
//        ret += char(num % 10 + '0'); num /= 10, pos++;
//        if (pos == 3)
//            pos = 0, ret += ',';
//    }
//    if (ret.back() == ',') ret.pop_back();
//    reverse(ret.begin(), ret.end());
//    cout << ret << endl;
//    return 0;
//}



//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n; cin >> n;
//    int a = 1, b = 2, c = 0;
//    for (int i = 3; i <= n; i++)
//    {
//        c = a + b;
//        a = b, b = c;
//    }
//    if (n < 3) cout << n << endl;
//    else cout << c << endl;
//    return 0;
//}



//class Solution {
//public:
//    bool IsContinuous(vector<int>& numbers) {
//        sort(numbers.begin(), numbers.end());
//        int zero = 0, n = numbers.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (numbers[i] == 0) zero++;
//            else if (i > 0 && numbers[i - 1] != 0)
//            {
//                int gap = numbers[i] - numbers[i - 1];
//                if (gap == 0) return false;
//                else if (gap != 1)
//                {
//                    zero -= gap - 1;
//                    if (zero < 0) return false;
//                }
//            }
//        }
//        return true;
//    }
//}



//class Solution {
//public:
//    bool hash[20] = { 0 };
//    bool IsContinuous(vector<int>& numbers) {
//        int maxval = 0, minval = 20;
//        for (auto num : numbers) {
//            if (num) {
//                if (hash[num]) return false;
//                hash[num] = true;
//                maxval = max(maxval, num);
//                minval = min(minval, num);
//            }
//        }
//        return (maxval - minval) <= 4;
//    }
//};



class Solution
{
public:
	int getLongestPalindrome(string s)
	{
		int ret = 1, n = s.size();
		for (int i = 1; i < n; i++)
		{
			// 当⻓度是奇数的时候
			int left = i - 1, right = i + 1;
			while (left >= 0 && right < n && s[left] == s[right])
			{
				left--;
				right++;
			}
			ret = max(ret, right - left - 1);
			// 当⻓度是偶数的时候
			left = i - 1, right = i;
			while (left >= 0 && right < n && s[left] == s[right])
			{
				left--;
				right++;
			}
			ret = max(ret, right - left - 1);
		}
		return ret;
	}
};