//class Solution {
//public:
//    ListNode* ReverseList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = ReverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return;
//        ListNode* slow = head, * fast = head, * prev = nullptr;
//        while (fast && fast->next)
//        {
//            prev = slow;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        prev->next = nullptr;
//
//        ListNode* cur1 = head;
//        ListNode* cur2 = ReverseList(slow);
//        ListNode* newhead = new ListNode();
//        prev = newhead;
//        while (cur1 && cur2)
//        {
//            ListNode* next1 = cur1->next, * next2 = cur2->next;
//            // 连接
//            prev->next = cur1;
//            cur1->next = cur2;
//
//            // 更新节点
//            prev = cur2;
//            cur1 = next1;
//            cur2 = next2;
//        }
//
//        prev = newhead->next;
//        delete newhead;
//    }
//};



class Solution {
public:
    int hash[26] = { 0 };
    char dismantlingAction(string arr) {
        for (auto ch : arr)
            hash[ch - 'a']++;
        for (auto ch : arr)
            if (hash[ch - 'a'] == 1) return ch;
        return ' ';
    }
};