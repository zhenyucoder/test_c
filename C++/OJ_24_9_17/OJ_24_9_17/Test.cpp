﻿#include <iostream>
#include <string>
using namespace std;


//#include <functional>
//int Sub(int a, int b)
//{
//	return a - b;
//}
//
//
//class Plus
//{
//public:
//	static int Plusi(int x, int y)
//	{
//		return x + y;
//	}
//
//	double Plusd(double x, double y)
//	{
//		return x + y;
//	}
//};
//int main()
//{
//	Plus ps;
//
//	std::function<double(Plus*, double, double)> func1 = &Plus::Plusd;
//	std::cout << func1(&ps, 10.1, 20.2) << std::endl;
//
//	std::function<double(double, double)> func2 = std::bind(&Plus::Plusd, &ps, std::placeholders::_1, std::placeholders::_2);
//	std::cout << func2(10.1, 20.2) << std::endl;
//
//	std::function<double(double)> func3 = std::bind(&Plus::Plusd, &ps, std::placeholders::_1, 1.1);
//	std::cout << func3(10.1) << std::endl;
//
//	auto func4 = std::bind(&Plus::Plusi, 10, 10);
//	std::cout << func4() << std::endl;
//
//	std::function<double(double, double)> func5 = std::bind(&Plus::Plusd, Plus(), std::placeholders::_1, std::placeholders::_2);
//	std::cout << func5(1.1, 2.2) << std::endl;
//}





double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	return (double)a / (double)b;
}

void Func()
{
	int* array = new int[10];
	try {
		int len, time;
		cin >> len >> time;
		cout << Division(len, time) << endl;
	}
	catch (...)
	{
		cout << "delete []" << array << endl;
		delete[] array;
		throw;
	}
	// ...
	cout << "delete []" << array << endl;
	delete[] array;
}

int main()
{
	try
	{
		Func();
	}
	catch (const char* errmsg)
	{
		cout << errmsg << endl;
	}
	return 0;
}