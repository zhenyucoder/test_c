//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size(), ret = 1;
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (nums[j] < nums[i])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size(), ret = 1;
//        vector<int> f(n, 1), g(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (nums[j] < nums[i])
//                    f[i] = max(f[i], g[j] + 1);
//                if (nums[j] > nums[i])
//                    g[i] = max(g[i], f[j] + 1);
//            }
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int findLongestChain(vector<vector<int>>& pairs) {
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size(), ret = 1;
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (pairs[i][0] > pairs[j][1])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        int n = arr.size(), ret = 1;
//        vector<int> dp(n, 1);
//        unordered_map<int, int> hash; hash[arr[0]]++;
//        for (int i = 1; i < n; i++)
//        {
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            ret = max(ret, hash[arr[i]]);
//        }
//        return ret;
//    }
//};






class Solution {
public:
    int lenLongestFibSubseq(vector<int>& arr) {
        int n = arr.size();
        unordered_map<int, int> hash;
        for (int i = 0; i < n; i++)
            hash[arr[i]] = i;

        vector<vector<int>> dp(n, vector<int>(n, 2));
        int ret = 2;
        for (int j = 2; j < n; j++)
        {
            for (int i = 1; i < j; i++)
            {
                int x = arr[j] - arr[i];
                if (x < arr[i] && hash.count(x))
                {
                    dp[i][j] = dp[hash[x]][i] + 1;
                    ret = max(ret, dp[i][j]);
                }
            }
        }
        return ret == 2 ? 0 : ret;
    }
};