//class Solution
//{
//	vector<int> ret;
//	vector<int> index; 
//	int tmpNums[500010];
//	int tmpIndex[500010];
//public:
//	vector<int> countSmaller(vector<int>& nums)
//	{
//		int n = nums.size();
//		ret.resize(n);
//		index.resize(n);
//		for (int i = 0; i < n; i++)
//			index[i] = i;
//		mergeSort(nums, 0, n - 1);
//		return ret;
//	}
//
//	void mergeSort(vector<int>& nums, int left, int right)
//	{
//		if (left >= right) return;
//		int mid = (left + right) >> 1;
//		mergeSort(nums, left, mid);
//		mergeSort(nums, mid + 1, right);
//		int cur1 = left, cur2 = mid + 1, i = 0;
//		while (cur1 <= mid && cur2 <= right)
//		{
//			if (nums[cur1] <= nums[cur2])
//			{
//				tmpNums[i] = nums[cur2];
//				tmpIndex[i++] = index[cur2++];
//			}
//			else
//			{
//				ret[index[cur1]] += right - cur2 + 1; 
//				tmpNums[i] = nums[cur1];
//				tmpIndex[i++] = index[cur1++];
//			}
//		}
//		while (cur1 <= mid)
//		{
//			tmpNums[i] = nums[cur1];
//			tmpIndex[i++] = index[cur1++];
//		}
//		while (cur2 <= right)
//		{
//			tmpNums[i] = nums[cur2];
//			tmpIndex[i++] = index[cur2++];
//		}
//		for (int j = left; j <= right; j++)
//		{
//			nums[j] = tmpNums[j - left];
//			index[j] = tmpIndex[j - left];
//		}
//	}
//};





//int memo[31]; //����¼
//int dp[31];
//int fib(int n)
//{
//	
//	dp[0] = 0; dp[1] = 1;
//	for (int i = 2; i <= n; i++)
//		dp[i] = dp[i - 1] + dp[i - 2];
//	return dp[n];
//}
//
//int dfs(int n)
//{
//	if (memo[n] != -1)
//	{
//		return memo[n]; 
//	}
//	if (n == 0 || n == 1)
//	{
//		memo[n] = n; 
//		return n;
//	}
//	memo[n] = dfs(n - 1) + dfs(n - 2); 
//	return memo[n];
//}



//class Solution
//{
//	int ret, aim;
//public:
//	int findTargetSumWays(vector<int>& nums, int target)
//	{
//		aim = target;
//		dfs(nums, 0, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos, int path)
//	{
//		if (pos == nums.size())
//		{
//			if (path == aim) ret++;
//			return;
//		}
//		dfs(nums, pos + 1, path + nums[pos]);
//		dfs(nums, pos + 1, path - nums[pos]);
//	}
//};



class Solution
{
	vector<int> path;
	vector<vector<int>> ret;
	int n, k;
public:
	vector<vector<int>> combine(int _n, int _k)
	{
		n = _n; k = _k;
		dfs(1);
		return ret;
	}
	void dfs(int start)
	{
		if (path.size() == k)
		{
			ret.push_back(path);
			return;
		}
		for (int i = start; i <= n; i++)
		{
			path.push_back(i);
			dfs(i + 1);
			path.pop_back(); 
		}
	}
};