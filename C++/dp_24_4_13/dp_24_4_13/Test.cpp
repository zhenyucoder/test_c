//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int n1 = text1.size(), n2 = text2.size();
//        text1 = " " + text1, text2 = " " + text2;//将text1、text2下标整体加1
//        vector<vector<int>> dp(n1 + 1, vector<int>(n2 + 1));
//        for (int i = 1; i <= n1; i++)
//            for (int j = 1; j <= n2; j++)
//                if (text1[i] == text2[j])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//        return dp[n1][n2];
//    }
//};



//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int m = nums1.size(), n = nums2.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//        return dp[m][n];
//    }
//};




//class Solution {
//public:
//    const int DOM = pwo(10, 9) + 7;
//    int numDistinct(string s, string t) {
//        int m = s.size(), n = t.size();
//        s = " " + s, t = " " + t;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //初始化
//        for (int i = 0; i <= m; i++)
//            dp[i][0] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = s[i] == t[j] ? (dp[i - 1][j - 1] + dp[i - 1][j]) % DOM : dp[i - 1][j];
//        return dp[m][n];
//    }
//}




class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        s = " " + s, p = '*' + p;
        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
        //初始化
        for (int i = 0; i <= n; i++)
        {
            if (p[i] == '*') dp[0][i] = true;
            else break;
        }

        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++)
                if (p[j] == '?')
                    dp[i][j] = dp[i - 1][j - 1];
                else if (p[j] == '*')
                    dp[i][j] = dp[i][j - 1] || dp[i - 1][j];
                else
                    dp[i][j] = s[i] == p[j] ? dp[i - 1][j - 1] : false;
        return dp[m][n];
    }
};



