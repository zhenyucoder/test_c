//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(dp[i - 1] + nums[i - 1], nums[i - 1]);
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//
//        int fmax = INT_MIN, gmin = INT_MAX, sum = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(f[i - 1] + nums[i - 1], nums[i - 1]);
//            fmax = max(fmax, f[i]);
//            g[i] = min(g[i - 1] + nums[i - 1], nums[i - 1]);
//            gmin = min(gmin, g[i]);
//            sum += nums[i - 1];
//        }
//        return sum == gmin ? fmax : max(fmax, sum - gmin);
//    }
//};



//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        //初始化
//        f[0] = g[0] = 1;
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            int x = nums[i - 1], y = f[i - 1] * x, z = g[i - 1] * x;
//            f[i] = max(x, max(y, z));
//            g[i] = min(x, min(y, z));
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size();
//        //f[i]:以i为结尾，乘积为正的最长子数组长度；g[i]：以i为结尾，乘积为负的最长子数组长度
//        vector<int> f(n + 1), g(n + 1);
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};


class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size();
        int sum = 0;
        vector<int> dp(n);
        for (int i = 2; i < n; i++)
        {
            if ((nums[i] - nums[i - 1]) == (nums[i - 1] - nums[i - 2]))
            {
                dp[i] = dp[i - 1] + 1;
            }
            else
            {
                dp[i] = 0;
            }
            sum += dp[i];
        }
        return sum;
    }
};