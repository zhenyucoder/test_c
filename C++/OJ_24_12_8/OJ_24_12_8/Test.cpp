//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int first = 0, second = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (nums[i] > first)
//                second = first, first = nums[i];
//            else if (nums[i] > second)
//                second = nums[i];
//        }
//        return (first - 1) * (second - 1);
//    }
//};



class Solution {
public:
    int longestSubstring(const string& s, int k) {
        int cnts[26] = { 0 }, n = s.size(), rets = 0;

        for (auto& ch : s) cnts[ch - 'a']++;

        for (int i = 0; i < n; i++) {
            while (i < n && cnts[s[i] - 'a'] < k) i++;

            int start = i, len = 0;
            while (i < n && cnts[s[i] - 'a'] >= k) {
                i++; len++;
            }
            if (start == 0 && len == n) return n;

            if (len >= k) {
                rets = max(rets, longestSubstring(s.substr(start, len), k));
            }
        }

        return rets;
    }
};