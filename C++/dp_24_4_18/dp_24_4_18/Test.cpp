//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int numSquares(int n) {
//        int x = sqrt(n);
//        vector<vector<int>> dp(x + 1, vector<int>(n + 1));
//        for (int j = 1; j <= n; j++) dp[0][j] = INF;
//        for (int i = 1; i <= x; i++)
//            for (int j = 1; j <= n; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= i * i)
//                    dp[i][j] = min(dp[i][j], dp[i][j - i * i] + 1);
//            }
//        return dp[x][n];
//    }
//};

//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int numSquares(int n) {
//        int x = sqrt(n);
//        vector<int> dp(n + 1, INF);
//        dp[0] = 0;
//        for (int i = 1; i <= x; i++)
//            for (int j = i * i; j <= n; j++)
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        for (int i = 1; i <= n; i++) dp[i][0] = 1;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= amount; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= coins[i - 1])
//                    dp[i][j] += dp[i][j - coins[i - 1]];
//            }
//        return dp[n][amount];
//    }
//};

//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//            for (int j = coins[i - 1]; j <= amount; j++)
//            {
//                dp[j] += dp[j - coins[i - 1]];
//            }
//        return dp[amount];
//    }
//};

//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1));
//        for (int j = 1; j <= amount; j++)
//            dp[0][j] = INF;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= amount; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= coins[i - 1])
//                    dp[i][j] = min(dp[i][j], dp[i][j - coins[i - 1]] + 1);
//            }
//        return dp[n][amount] >= INF ? -1 : dp[n][amount];
//    }
//};

//class Solution {
//public:
//    const int INF = 0x3f3f3f3f;
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        for (int j = 1; j <= amount; j++)
//            dp[j] = INF;
//        for (int i = 1; i <= n; i++)
//            for (int j = coins[i - 1]; j <= amount; j++)
//                dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//        return dp[amount] >= INF ? -1 : dp[amount];
//    }
//};


//vector<int> knapsack(int v, int n, vector<vector<int> >& nums) {
//    vector<int> ret;
//    int vn = nums.size();
//    vector<vector<int>> dp(vn + 1, vector<int>(v + 1));
//    for (int i = 1; i <= vn; i++)
//        for (int j = 1; j <= v; j++)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= nums[i - 1][0])
//                dp[i][j] = max(dp[i][j],
//                    dp[i][j - nums[i - 1][0]] + nums[i - 1][1]);
//        }
//    ret.push_back(dp[vn][v]);
//    memset(dp, 0, sizeof dp);
//    for (int j = 1; j <= vn; j++)
//        dp[0][j] = -1;
//    for (int i = 1; i <= vn; i++)
//        for (int j = 1; j <= v; j++)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= nums[i - 1][0] && dp[i][j - nums[i - 1][0]] != -1)
//                dp[i][j] = max(dp[i][j],
//                    dp[i][j - nums[i - 1][0]] + nums[i - 1][1]);
//        }
//    ret.push_back(dp[vn][v] == -1 ? 0 : dp[vn][n]);
//    return ret;
//}

//vector<int> knapsack(int v, int n, vector<vector<int> >& nums) {
//    vector<int> ret;
//    int vn = nums.size();
//    vector<int> dp(v + 1);
//    for (int i = 1; i <= vn; i++)
//        for (int j = nums[i - 1][0]; j <= v; j++)
//        {
//            dp[j] = max(dp[j], dp[j - nums[i - 1][0]] + nums[i - 1][1]);
//        }
//    ret.push_back(dp[v]);
//    memset(dp, 0, sizeof dp);
//    for (int j = 1; j <= vn; j++)
//        dp[0][j] = -1;
//    for (int i = 1; i <= vn; i++)
//        for (int j = = nums[i - 1][0]; j <= v; j++)
//        {
//            if (dp[j - nums[i - 1][0]] != -1)
//                dp[j] = max(dp[j],
//                    dp[j - nums[i - 1][0]] + nums[i - 1][1]);
//        }
//    ret.push_back(dp[v] == -1 ? 0 : dp[n]);
//    return ret;
//}

