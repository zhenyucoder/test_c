//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int n, V, v[N], w[N];
//int dp[N][N];
//
//
//int main()
//{
//	cin >> n >> V;
//	for (int i = 1; i <= n; i++)
//	{
//		cin >> v[i] >> w[i];
//	}
//
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= V; j++)
//		{
//			dp[i][j] = dp[i - 1][j];
//			if (v[i] <= j)
//				dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//		}
//	}
//	cout << dp[n][V] << endl;
//
//	memset(dp, 0, sizeof dp);
//	for (int j = 1; j <= V; j++)
//		dp[0][j] = -1;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= V; j++)
//		{
//			dp[i][j] = dp[i - 1][j];
//			if (j >= v[i] && dp[i - 1][j - v[i]] != -1)
//				dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//		}
//	}
//
//	cout << (dp[n][V] == -1 ? 0 : dp[n][V]) << endl;
//	return 0;
//}


// 滚动数组优化空间复杂度，顺带优化时间复杂度
//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int n, V, v[N], w[N];
//int dp[N];
//int main()
//{
//	cin >> n >> V;
//	for (int i = 1; i <= n; i++)
//	{
//		cin >> v[i] >> w[i];
//	}
//
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = V; j >= v[i]; j--)
//		{
//			if (v[i] <= j)
//				dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//		}
//	}
//	cout << dp[V] << endl;
//
//	memset(dp, 0, sizeof dp);
//	for (int j = 1; j <= V; j++)
//		dp[j] = -1;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = V; j >=v[i]; j--)
//		{
//			if (dp[j - v[i]] != -1)
//				dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//		}
//	}
//
//	cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//	return 0;
//}



//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        int aim = sum + target;
//        if (aim % 2 || aim < 0) return 0;
//        aim /= 2;
//
//        vector<int> dp(aim + 1);
//        int n = nums.size();
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = aim; j >= nums[i - 1]; j--)
//                dp[j] += dp[j - nums[i - 1]];
//        }
//        return dp[aim];
//    }
//};




//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int sum = 0;
//        for (auto x : stones)
//            sum += x;
//        int aim = sum / 2;
//        int n = stones.size();
//        vector<int> dp(aim + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = aim; j >= stones[i - 1]; j--)
//            {
//                dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - 2 * dp[aim];
//    }
//};






//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int n, V;
//int v[N], w[N];
//int dp[N];
//
//
//int main()
//{
//	cin >> n >> V;
//	for (int i = 1; i <= n; i++)
//		cin >> v[i] >> w[i];
//
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = v[i]; j <= V; j++)
//		{
//			dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//		}
//	}
//	cout << dp[V] << endl;
//
//	memset(dp, -1, sizeof dp);
//	dp[0] = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = v[i]; j <= V; j++)
//		{
//			if (dp[j - v[i]] != -1)
//				dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//		}
//	}
//	cout << (dp[V] == -1 ? 0 : dp[V]) << endl;
//	return 0;
//}





//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, 0);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = coins[i - 1]; j <= amount; j++)
//            {
//                if (dp[j - coins[i - 1]] != -1)
//                    dp[j] += dp[j - coins[i - 1]];
//            }
//        }
//        return dp[amount];
//    }
//};





//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= n; i++)
//            for (int j = 0; j <= amount; j++)
//                if (j >= coins[i - 1])
//                    dp[j] = min(dp[j], dp[j - coins[i - 1]] + 1);
//        return dp[amount] == 0x3f3f3f3f ? -1 : dp[amount];
//    }
//};





//class Solution {
//public:
//    int numSquares(int n) {
//        int x = sqrt(n);
//        vector<int> dp(n + 1, 0x3f3f3f3f);
//        dp[0] = 0;
//        for (int i = 1; i <= x; i++)
//        {
//            for (int j = i * i; j <= n; j++)
//            {
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//            }
//        }
//        return dp[n] >= 0x3f3f3f3f ? 0 : dp[n];
//    }
//};





//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        int len = strs.size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= len; i++)
//        {
//            int a = 0, b = 0;
//            for (auto ch : strs[i - 1])
//                if (ch == '0') a++;
//                else b++;
//            for (int j = m; j >= a; j--)
//                for (int k = n; k >= b; k--)
//                {
//                    dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
//                }
//        }
//        return dp[m][n];
//    }
//};





