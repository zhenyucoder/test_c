class Solution {
public:
    int majorityElement(vector<int>& nums)
    {
        int candidate = -1;
        int count = 0;

        for (int& num : nums) 
        {
            if (count == 0) 
            {
                candidate = num;
            }
            if (num == candidate) 
            {
                count++;
            }
            else {
                count--;
            }
        }
        count = 0;
        int length = nums.size();
        for (int& num : nums)
        {
            if (num == candidate) 
            {
                count++;
            }
        }
        return count * 2 > length ? candidate : -1;
    }
};
