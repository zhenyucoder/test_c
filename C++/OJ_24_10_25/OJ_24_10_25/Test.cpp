//class Solution
//{
//	bool vis[16][16];
//	int dx[4] = { 0, 0, 1, -1 };
//	int dy[4] = { 1, -1, 0, 0 };
//	int m, n;
//	int ret;
//public:
//	int getMaximumGold(vector<vector<int>>& g)
//	{
//		m = g.size(), n = g[0].size();
//		for (int i = 0; i < m; i++)
//			for (int j = 0; j < n; j++)
//			{
//				if (g[i][j])
//				{
//					vis[i][j] = true;
//					dfs(g, i, j, g[i][j]);
//					vis[i][j] = false;
//				}
//			}
//		return ret;
//	}
//	void dfs(vector<vector<int>>& g, int i, int j, int path)
//	{
//		ret = max(ret, path);
//		for (int k = 0; k < 4; k++)
//		{
//			int x = i + dx[k], y = j + dy[k];
//			if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && g[x][y])
//			{
//				vis[x][y] = true;
//				dfs(g, x, y, path + g[x][y]);
//				vis[x][y] = false;
//			}
//		}
//	}
//};




class Solution
{
	bool vis[21][21];
	int dx[4] = { 1, -1, 0, 0 };
	int dy[4] = { 0, 0, 1, -1 };
	int ret;
	int m, n, step;
public:
	int uniquePathsIII(vector<vector<int>>& grid)
	{
		m = grid.size(), n = grid[0].size();
		int bx = 0, by = 0;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				if (grid[i][j] == 0) step++;
				else if (grid[i][j] == 1)
				{
					bx = i;
					by = j;
				}
		step += 2;
		vis[bx][by] = true;
		dfs(grid, bx, by, 1);
		return ret;
	}
	void dfs(vector<vector<int>>& grid, int i, int j, int count)
	{
		if (grid[i][j] == 2)
		{
			if (count == step) // �ж��Ƿ�Ϸ�
				ret++;
			return;
		}
		for (int k = 0; k < 4; k++)
		{
			int x = i + dx[k], y = j + dy[k];
			if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y]
				!= -1)
			{
				vis[x][y] = true;
				dfs(grid, x, y, count + 1);
					vis[x][y] = false;
			}
		}
	}
};