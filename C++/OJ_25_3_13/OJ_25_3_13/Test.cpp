//class Solution {
//public:
//    class Cmp
//    {
//    public:
//        bool operator()(const ListNode* list1, const ListNode* list2)
//        {
//            return list1->val > list2->val;
//        }
//    };
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        priority_queue<ListNode*, vector<ListNode*>, Cmp> pq;
//        for (auto& list : lists)
//            if (list)
//                pq.push(list);
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        while (!pq.empty())
//        {
//            auto top = pq.top();
//            pq.pop();
//            prev->next = top;
//            prev = prev->next;
//            if (top->next)
//                pq.push(top->next);
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};



//class Solution {
//public:
//    bool isSame(const string& s1, const string& s2)
//    {
//        for (int i = 0; i < s1.size(); i++)
//        {
//            if (s1[i] != s2[i]) return false;
//        }
//        return true;
//    }
//
//    bool solve(string A, string B) {
//        int n = A.size(), m = B.size();
//        if (n != m) return false;
//
//        for (int pos = 0; pos < n; pos++)
//        {
//            if (A[pos] == B[0])
//            {
//                string tmp = A.substr(pos);
//                if (pos > 0)
//                    tmp += A.substr(0, pos - 1);
//                if (isSame(tmp, B)) return true;
//            }
//        }
//        return false;
//    }
//};



#include <iostream>
#include <vector>
using namespace std;

int n, m;
int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
int arr[110][110] = { 0 };
long long mem[110][110] = { 0 };

int dfs(int i, int j)
{
    if (mem[i][j] != 0) return mem[i][j];
    int ret = 0;
    for (int k = 0; k < 4; k++)
    {
        int x = i + dx[k], y = j + dy[k];
        if (x >= 0 && x < n && y >= 0 && y < m && arr[x][y] < arr[i][j])
            ret = max(ret, dfs(x, y));
    }
     mem[i][j] = ret + arr[i][j];
     return mem[i][j];
}

int main()
{
    cin >> n >> m;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            cin >> arr[i][j];
    int ret = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            ret = max(ret, dfs(i, j));
        }
    }

    return ret;
}