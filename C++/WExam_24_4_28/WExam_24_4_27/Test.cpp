//int main() {
//    string str1, str2;
//    getline(cin, str1);
//    getline(cin, str2);
//
//    bool hash[300] = { false };
//    for (auto ch : str2)
//        hash[ch] = true;
//    for (auto ch : str1)
//        if (hash[ch] == false)
//            cout << ch;
//}



//class Solution {
//public:
//	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
//		ListNode* cur1 = pHead1, * cur2 = pHead2;
//		while (cur1 != cur2)
//		{
//			cur1 = cur1 == nullptr ? pHead2 : cur1->next;
//			cur2 = cur2 == nullptr ? pHead1 : cur2->next;
//		}
//		return cur1;
//	}
//};


int main()
{
	int n;
	string str;
	cin >> n >> str;

	long long s = 0, h = 0, y = 0;
	for (auto ch : str)
	{
		if (ch == 's')
			s++;
		else if (ch == 'h')
			h += s;
		else if (ch == 'y')
			y += h;
	}
	cout << y << endl;
	return 0;
}