#pragma once
#include <iostream>
using namespace std;

template <class T>
struct BSTNode
{
	BSTNode(const T& data = T())
		:_data(data), _left(nullptr), _right(nullptr)
	{}

	T _data;
	BSTNode<T>* _left;
	BSTNode<T>* _right;
};


template <class T>
class BST_K
{
public:
	BST_K()
		:_root(nullptr)
	{}

	~BST_K()
	{
		_Destory(_root);
	}

	BST_K(const BST_K<T>& bst)
	{
		_root = Copy(bst);
	}

	BST_K<T>& operator=(const BST_K<T> bst)
	{
		swap(_root, bst._root);
		return *this;
	}
public:
	void Insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new BSTNode<T>(data);
			return;
		}

		BSTNode<T>* cur = _root;
		BSTNode<T>* parent = nullptr;
		while (cur)
		{
			parent = cur;
			if (cur->_data > data) cur = cur->_left;
			else if (cur->_data < data) cur = cur->_right;
			else
			{
				cout << "该元素已经存在，插入失败！！！" << endl;
				return;
			}
		}
		cur = new BSTNode<int>(data);
		if (parent->_data > data) parent->_left = cur;
		else parent->_right = cur;
	}

	bool Erase(const T& data)
	{
		if (_root == nullptr) return false;
		BSTNode<T>* parent = nullptr;
		BSTNode<T>* cur = _root;
		while (cur)
		{
			if (cur->_data > data)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_data < data)
			{
				parent = cur;
				cur = cur->_right;
			}
			else // 查找到目标节点，开始删除
			{
				if (cur->_left == nullptr)
				{
					if (cur == _root) _root = cur->_right;
					else
					{
						if (parent->_data > data) parent->_left = cur->_right;
						else parent->_right = cur->_right;
					}
					delete cur;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root) _root = cur->_left;
					else
					{
						if (parent->_data > data) parent->_left = cur->_left;
						else parent->_right = cur->_left;
					}
					delete cur;
				}
				else
				{
					BSTNode<T>* subleft = cur->_right;
					BSTNode<T>* subparent = cur;
					while (subleft->_left)
					{
						subparent = subleft;
						subleft = subleft->_left;
					}
					swap(cur->_data, subleft->_data);

					if (subparent->_data > cur->_data) subparent->_left = nullptr;
					else subparent->_right = nullptr;
					delete subleft;
				}
				return true;
			}
		}
		return false;
	}


	BSTNode<T>* Find(const T& data)
	{
		BSTNode<T>* cur = _root;
		while (cur)
		{
			if (cur->_data > data) cur = cur->_left;
			else if (cur->_data < data) cur = cur->_right;
			else return cur;
		}
		return nullptr;
	}

	void Inorder()
	{
		_Inorder(_root);
		cout << endl;
	}
private:
	BSTNode<T>* _root;

	void _Destory(BSTNode<T>* root)
	{
		if (root == nullptr) return;
		_Destory(root->_left);
		_Destory(root->_right);
		delete root;
	}

	void _Inorder(BSTNode<T>* cur)
	{
		if (cur == nullptr) return;
		_Inorder(cur->_left);
		cout << cur->_data << " ";
		_Inorder(cur->_right);
	}

	BST_K<T>* Copy(BST_K<T>*& root)
	{
		if (root == nullptr) return nullptr;
		BST_K<T>* newroot = root;
		newroot->_left = Copy(root->_left);
		newroot->_right = Copy(root->_right);
		return newroot;
	}
};