#include <iostream>
using namespace std;
//#include "BST_K.hpp"
#include "BST_KV.hpp"
#include <string>
using namespace std;

void TestBSTree1()
{
	BST_KV<string, string> dict;
	dict.Insert("string", "字符串");
	dict.Insert("tree", "树");
	dict.Insert("left", "左边、剩余");
	dict.Insert("right", "右边");
	dict.Insert("sort", "排序");

	string str;
	while (cin >> str)
	{
		BSTNode<string, string>* ret = dict.Find(str);
		if (ret == nullptr)
		{
			cout << "单词拼写错误，词库中没有这个单词:" << str << endl;
		}
		else
		{
			cout << str << "中文翻译:" << ret->_value << endl;
		}
	}
}

void TestBSTree2()
{
	// 统计水果出现的次数
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
   "苹果", "香蕉", "苹果", "香蕉" };
	BST_KV<string, int> countTree;
	for (const auto& str : arr)
	{
		// 先查找水果在不在搜索树中
		// 1、不在，说明水果第一次出现，则插入<水果, 1>
		// 2、在，则查找到的节点中水果对应的次数++
		//BSTreeNode<string, int>* ret = countTree.Find(str);
		auto ret = countTree.Find(str);
		if (ret == NULL)
		{
			countTree.Insert(str, 1);
		}
		else
		{
			ret->_value++;
		}
	}
	countTree.Inorder();
}

int main()
{
	TestBSTree2();
	return 0;
}

//int main()
//{
//	BST_K<int>* bst = new BST_K<int>();
//	bst->Insert(50);
//	bst->Insert(1);
//	bst->Insert(10);
//	bst->Insert(200);
//	//bst->Insert(10);
//	bst->Insert(100);
//	bst->Insert(5);
//	bst->Insert(78);
//	bst->Insert(0);
//	bst->Inorder();
//
//	//cout << bst->Find(1)->_data << endl;
//
//	bst->Erase(5);
//	bst->Inorder();
//
//	bst->Erase(1000);
//	bst->Inorder();
//
//	bst->Erase(50);
//	bst->Inorder();
//
//	BST_K<int>* temp = bst;
//	temp->Inorder();
//
//	BST_K<int>* temp1 = new BST_K<int>();
//	temp1->Insert(32482);
//	temp1->Inorder();
//
//	temp1 = bst;
//	temp1->Inorder();
//	return 0;
//}