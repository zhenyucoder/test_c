#pragma once
#include <iostream>
using namespace std;

template <class K, class V>
struct BSTNode
{
	BSTNode(const K& key = _key(), const V& value = _value())
		:_key(key), _value(value), _left(nullptr), _right(nullptr)
	{}

	K _key;
	V _value;
	BSTNode<K, V>* _left;
	BSTNode<K, V>* _right;
};


template <class K, class V>
class BST_KV
{
public:
	BST_KV()
		:_root(nullptr)
	{}

	~BST_KV()
	{
		_Destory(_root);
	}

	BST_KV(const BST_KV<K, V>& bst)
	{
		_root = Copy(bst);
	}

	BST_KV<K, V>& operator=(BST_KV<K, V> bst)
	{
		swap(_root, bst._root);
		return *this;
	}
public:
	void Insert(const K& key, const V& value)
	{
		if (_root == nullptr)
		{
			_root = new BSTNode<K, V>(key, value);
			return;
		}

		BSTNode<K, V>* cur = _root;
		BSTNode<K, V>* parent = nullptr;
		while (cur)
		{
			parent = cur;
			if (cur->_key > key) cur = cur->_left;
			else if (cur->_key < key) cur = cur->_right;
			else
			{
				cout << "该元素已经存在，插入失败！！！" << endl;
				return;
			}
		}
		cur = new BSTNode<K, V>(key, value);
		if (parent->_key > key) parent->_left = cur;
		else parent->_right = cur;
	}

	bool Erase(const K& key)
	{
		if (_root == nullptr) return false;
		BSTNode<K, V>* parent = nullptr;
		BSTNode<K, V>* cur = _root;
		while (cur)
		{
			if (cur->_key > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_key < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else // 查找到目标节点，开始删除
			{
				if (cur->_left == nullptr)
				{
					if (cur == _root) _root = cur->_right;
					else
					{
						if (parent->_key > key) parent->_left = cur->_right;
						else parent->_right = cur->_right;
					}
					delete cur;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root) _root = cur->_left;
					else
					{
						if (parent->_data > data) parent->_left = cur->_left;
						else parent->_right = cur->_left;
					}
					delete cur;
				}
				else
				{
					BSTNode<K, V>* subleft = cur->_right;
					BSTNode<K, V>* subparent = cur;
					while (subleft->_left)
					{
						subparent = subleft;
						subleft = subleft->_left;
					}
					swap(cur->_key, subleft->_key);

					if (subparent->_key > cur->_key) subparent->_left = nullptr;
					else subparent->_right = nullptr;
					delete subleft;
				}
				return true;
			}
		}
		return false;
	}


	BSTNode<K, V>* Find(const K& key)
	{
		BSTNode<K, V>* cur = _root;
		while (cur)
		{
			if (cur->_key > key) cur = cur->_left;
			else if (cur->_key < key) cur = cur->_right;
			else return cur;
		}
		return nullptr;
	}

	void Inorder()
	{
		_Inorder(_root);
		cout << endl;
	}
private:
	BSTNode<K, V>* _root;

	void _Destory(BSTNode<K, V>* root)
	{
		if (root == nullptr) return;
		_Destory(root->_left);
		_Destory(root->_right);
		delete root;
	}

	void _Inorder(BSTNode<K, V>* cur)
	{
		if (cur == nullptr) return;
		_Inorder(cur->_left);
		cout << cur->_key << " " << cur->_value << endl;;
		_Inorder(cur->_right);
	}

	BST_KV<K, V>* Copy(BST_KV<K, V>*& root)
	{
		if (root == nullptr) return nullptr;
		BST_KV<K, V>* newroot = root;
		newroot->_left = Copy(root->_left);
		newroot->_right = Copy(root->_right);
		return newroot;
	}
};