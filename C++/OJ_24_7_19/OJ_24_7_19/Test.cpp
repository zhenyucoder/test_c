//class Solution {
//public:
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};




//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = g[i - 1] + nums[i - 1];
//            g[i] = max(g[i - 1], f[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};




//class Solution {
//public:
//    int _rob(vector<int>& nums, int begin, int num)
//    {
//        if (num <= 0) return 0;
//        vector<int> f(num + 1), g(num + 1);
//        for (int i = 1; i <= num; i++)
//        {
//            f[i] = g[i - 1] + nums[begin + i - 1];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[num], g[num]);
//    }
//
//    int rob(vector<int>& nums) {
//        return max(_rob(nums, 1, nums.size() - 1), _rob(nums, 2, nums.size() - 3) + nums[0]);
//    }
//};




//class Solution {
//public:
//    unordered_map<TreeNode*, int> f, g;
//    void dfs(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        dfs(root->left);
//        dfs(root->right);
//        f[root] = root->val + g[root->left] + g[root->right];
//        g[root] = max(f[root->left], g[root->left]) + max(f[root->right], g[root->right]);
//    }
//
//    int rob(TreeNode* root) {
//        dfs(root);
//        return max(f[root], g[root]);
//    }
//};



//class Solution {
//public:
//#define NUM 10000
//    int deleteAndEarn(vector<int>& nums) {
//        int arr[NUM + 1] = { 0 };
//        for (auto x : nums)
//            arr[x] += x;
//        vector<int> f(NUM + 1), g(NUM + 1);
//        for (int i = 1; i <= NUM; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[NUM], g[NUM]);
//    }
//};





//class Solution {
//public:
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};




//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        //初始化
//        dp[0][0] = -prices[0], dp[0][1] = dp[0][2] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};





//class Solution {
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<vector<int>> dp(n + 1, vector<int>(2));
//        //初始化
//        dp[0][0] = -prices[0] - fee;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i] - fee);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
//        }
//        return dp[n - 1][1];
//    }
//};




//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int prev = 0x3f3f3f3f, ret = 0;
//        for (auto x : prices)
//        {
//            ret = max(ret, x - prev);
//            prev = min(prev, x);
//        }
//        return ret;
//    }
//};





//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
//        }
//        return dp[n - 1][1];
//    }
//};




//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size(), ret = -0x3f3f3f3f;
//        vector<int> dp(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//
//        //查找和最小的子数组
//        int n = nums.size(), maxAns = -0x3f3f3f3f, minAns = 0x3f3f3f3f;
//        vector<int> g(n + 1), f(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(nums[i - 1], f[i - 1] + nums[i - 1]);
//            maxAns = max(maxAns, f[i]);
//
//            g[i] = min(nums[i - 1], g[i - 1] + nums[i - 1]);
//            minAns = min(minAns, g[i - 1]);
//        }
//        return max(maxAns, sum - minAns);
//    }
//};




//class Solution {
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size();
//        double ret = -0x3f3f3f3f3;
//        vector<double> f(n + 1, 1), g(n + 1, 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max((double)nums[i - 1], max(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]));
//            g[i] = min((double)nums[i - 1], min(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]));
//            // if(nums[i - 1] > 0)
//            // {
//            //     f[i] = max(nums[i - 1], f[i - 1] * nums[i - 1]);
//            //     g[i] = min(nums[i - 1], g[i - 1] * nums[i - 1]);
//            // }
//            // else
//            // {
//            //     f[i] = max(nums[i - 1], g[i - 1] * nums[i - 1]);
//            //     g[i] = min(nums[i - 1], f[i - 1] * nums[i - 1]);
//            // }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size(), ret = INT_MIN;
//        vector<int> f(n + 1), g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size(), ret = 0;
//        vector<int> dp(n);
//        for (int i = 2; i < n; i++)
//        {
//            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2])
//            {
//                dp[i] = dp[i - 1] + 1;
//            }
//            ret += dp[i];
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size(), ret = 1;
//        vector<int> f(n, 1), g(n, 1);
//        for (int i = 1; i < n; i++)
//        {
//            if (arr[i] > arr[i - 1])
//                f[i] = g[i - 1] + 1;
//            else if (arr[i] < arr[i - 1])
//                g[i] = f[i - 1] + 1;
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int findSubstringInWraproundString(string s) {
//        int n = s.size();
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//            if ((s[i] - s[i - 1] == 1) || (s[i - 1] == 'z' && s[i] == 'a'))
//                dp[i] += dp[i - 1];
//
//        //去重
//        int hash[26] = { 0 };
//        for (int i = 0; i < n; i++)
//            hash[s[i] - 'a'] = max(dp[i], hash[s[i] - 'a']);
//
//        //统计结果
//        int ret = 0;
//        for (auto x : hash)
//            ret += x;
//        return ret;
//    }
//};




//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        unordered_map<string, bool> hash;
//        for (auto& s : wordDict)
//            hash[s] = true;
//        int n = s.size();
//        vector<bool> dp(n + 1);
//        dp[0] = true;
//        for (int i = 1; i <= n; i++)
//            for (int j = i; j >= 1; j--)
//            {
//                if (dp[j - 1] && hash[s.substr(j - 1, i - j + 1)])
//                {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        return dp[n];
//    }
//};




class Solution {
public:
    const int INF = 0x3f3f3f3f;
    int maxProfit(vector<int>& prices) {
        int n = prices.size();
        vector<vector<int>> f(n, vector<int>(3, -INF));
        auto g = f;
        //初始化
        f[0][0] = -prices[0], g[0][0] = 0;
        for (int i = 1; i < n; i++)
            for (int j = 0; j <= 2; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if (j >= 1)
                    g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
            }
        //返回结果
        int ret = -INF;
        for (int j = 0; j <= 2; j++)
            ret = max(ret, g[n - 1][j]);
        return ret;
    }
};