#include <iostream>
using namepsace std;

//class Solution {
//public:
//    int StrToInt(string str) {
//        if (str.empty())//非法字符串
//            return 0;
//
//        int ret = 0;
//        int flag = 1;//判断str中正负数
//        //将字母中正负号处理
//        if (str[0] == '-')
//        {
//            flag = -1; str[0] = '0';
//        }
//        if (str[0] == '+')
//        {
//            str[0] = '0';
//        }
//
//        for (auto ch : str)
//        {
//            if (ch >= '0' && ch < '9')
//            {
//                ret = ret * 10 + (ch - '0');
//            }
//            else
//            {
//                return 0;
//            }
//        }
//        return ret * flag;
//    }
//};


int main() {
    int W = 0, H = 0;
    while (cin >> W >> H)
    {
        int ans = 0;
        //创建二维数组存储蛋糕
        vector<vector<int>> v;
        v.resize(W);
        for (auto& x : v)
        {
            x.resize(H, 1);
        }

        for (int i = 0; i < W; i++)
        {
            for (int j = 0; j < H; j++)
            {
                if (v[i][j] == 1)
                {
                    ans++;
                    if (i + 2 < W)//将纵坐标差距为2的有效位置蛋糕消除
                        v[i + 2][j] = 0;
                    if (j + 2 < H)//将横坐标差距为2的有效位置蛋糕消除
                        v[i][j + 2] = 0;
                }
                // cout<<v[i][j]<<" ";
            }
            //cout<<endl;
        }

        cout << ans << endl;
    }
}