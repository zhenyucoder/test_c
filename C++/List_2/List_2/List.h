#pragma once


namespace achieveList
{
	//List的节点类
	template<class T>
	struct ListNode
	{
		T _data;
		ListNode<T>* _prev;
		ListNode<T>* _next;

		ListNode(const T& val = T())
			:_data(val)
			,_prev(nullptr)
			,_next(nullptr)
		{ }
	};

	//List的迭代器类
	template<class T, class Ptr, class Ref>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ptr, Ref> Self;
		Node* _pNode;
	public:
		ListIterator(Node* pNode = nullptr) 
			:_pNode(pNode)
		{}

		ListIterator(const Self& lt)
			: _pNode(lt._pNode)
		{}

		Ref operator*()
		{
			return _pNode->_data;
		}

		Ptr operator->()
		{
			return &_pNode->_data;
		}
		
		Self& operator++()
		{
			_pNode = _pNode->_next;
			return *this;
		}

		Self& operator--()
		{
			_pNode = _pNode->_prev;
			return *this;
		}

		Self operator++(int)
		{
			Node* tmp(*this);
			_pNode = _pNode->_next;
			return tmp;
		}

		Self operator--(int)
		{
			Node* tmp(*this);
			_pNode = _pNode->_prev;
			return tmp;
		}

		bool operator!=(const Self& s)
		{
			return _pNode != s._pNode;
		}

		bool operator==(const Self& s)
		{
			return _pNode == s._pNode;
		}
	};

	//list类
	template<class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef ListIterator<T, T*, T&> iterator;
		typedef ListIterator< T, const  T*, const T&> const_iterator;


		////////////////////////////////////////////////////////////////////////////////////////////////
			// List的构造
		list()
		{
			CreateHead();
		}

		list(int n, const T& value = T())
		{
			CreateHead();
			for (int i = 0; i < n; i++)
			{
				push_back(value);
			}
		}

		template <class iterator>
		list(iterator first, iterator last)
		{
			CreateHead();
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		list(const list<T>& lt)
		{
			CreateHead();
		/*	for (auto e : lt)
			{
				push_back(e);
			}*/
			//// 用l中的元素构造临时的temp,然后与当前对象交换
			list<T> tmp(lt.begin(), lt.end());
			swap(tmp);
		}

		list<T>& operator=(const list<T> lt)
		{
			swap(lt);
			return *this;
		}

		~list()
		{
			clear();
			delete _pHead;
			_pHead = nullptr;
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
		// List Iterator
		iterator begin()
		{
			return _pHead->_next;
		}

		iterator end()
		{
			return _pHead;
		}

		const_iterator begin()const
		{
			return _pHead->_next;
		}

		const_iterator end()const
		{
			return _pHead;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		// List Capacity
		size_t size()const
		{
			return _size;
		}

		bool empty()const
		{
			return _size == 0;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		// List Access
		T& front()
		{
			assert(!empty());
			return _pHead->_data;
		}

		const T& front()const
		{
			assert(!empty());
			return _pHead->_data;
		}

		T& back()
		{
			assert(!empty());
			return _pHead->_prev->_data;
		}

		const T& back()const
		{
			assert(!empty());
			return _pHead->_prev->_data;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		// List Modify
		void push_back(const T& val)
		{
			insert(end(), val);
		}

		void pop_back()
		{
			erase(--end());
		}

		void push_front(const T& val)
		{
			insert(begin(), val);
		}

		void pop_front()
		{
			erase(begin());
		}

		// 在pos位置前插入值为val的节点
		iterator insert(iterator pos, const T& val)
		{
			Node* cur = pos._pNode;
			Node* prev = cur->_prev;
			Node* newnode = new Node(val);

			prev->_next = newnode;
			newnode->_prev = prev;

			newnode->_next = cur;
			cur->_prev = newnode;

			_size++;
			return newnode;
		}

		// 删除pos位置的节点，返回该节点的下一个位置
		iterator erase(iterator pos)
		{
			Node* cur = pos._pNode;
			Node* prev = cur->_prev;
			Node* next = cur->_next;

			prev->_next = next;
			next->_prev = prev;

			delete cur;
			_size--;

			return next;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
	
		void swap(list<T>& lt)
		{
			std::swap(_pHead, lt._pHead);
			std::swap(_size, lt._size);
		}
	private:
		void CreateHead()
		{
			_pHead = new Node;
			_pHead->_next = _pHead;
			_pHead->_prev = _pHead;
			_size = 0;
		}
		Node* _pHead;
		size_t _size;
	};


	void test_list1()
	{	
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);

		lt.push_front(0);
		lt.push_front(0);
		lt.push_front(0);

		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.pop_front();
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.push_back(10);
		lt.push_back(11);
		lt.push_back(12);
		lt.push_back(13);
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		lt.pop_back();
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		cout << lt.front() << endl;
		cout << lt.back() << endl;
	}

	void test_list2()
	{
		list<int> lt;
		lt.push_back(1);
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);
		for (auto e : lt)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int> lt1(4, 4);
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;

		list<int> lt2 = lt1;
		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	
}
