#include <iostream>
using namespace std;


//class A
//{
//public:
//	int _a;
//};
//
// class B : public A
////class B : virtual public A
//{
//public:
//	int _b;
//};
//
// class C : public A
////class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}



//class Solution
//{
//	vector<int> path;
//	vector<vector<int>> ret;
//	int n, k;
//public:
//	vector<vector<int>> combine(int _n, int _k)
//	{
//		n = _n; k = _k;
//		dfs(1);
//		return ret;
//	}
//	void dfs(int start)
//	{
//		if (path.size() == k)
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (int i = start; i <= n; i++)
//		{
//			path.push_back(i);
//			dfs(i + 1);
//			path.pop_back(); // 恢复现场
//		}
//	}
//};




//class Solution
//{
//	int ret, aim;
//public:
//	int findTargetSumWays(vector<int>& nums, int target)
//	{
//		aim = target;
//		dfs(nums, 0, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos, int path)
//	{
//		if (pos == nums.size())
//		{
//			if (path == aim) ret++;
//			return;
//		}
//		// 加法
//		dfs(nums, pos + 1, path + nums[pos]);
//		// 减法
//		dfs(nums, pos + 1, path - nums[pos]);
//	}
//};




//class Solution
//{
//	bool row[9][10], col[9][10], grid[3][3][10];
//public:
//	void solveSudoku(vector<vector<char>>& board)
//	{
//		// 初始化
//		for (int i = 0; i < 9; i++)
//		{
//			for (int j = 0; j < 9; j++)
//			{
//				if (board[i][j] != '.')
//				{
//					int num = board[i][j] - '0';
//					row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//				}
//			}
//		}
//		dfs(board);
//	}
//	bool dfs(vector<vector<char>>& board)
//	{
//		for (int i = 0; i < 9; i++)
//		{
//			for (int j = 0; j < 9; j++)
//			{
//				if (board[i][j] == '.')
//				{
//					// 填数
//					for (int num = 1; num <= 9; num++)
//					{
//						if (!row[i][num] && !col[j][num] && !grid[i / 3][j / 3]
//							[num])
//						{
//							board[i][j] = '0' + num;
//							row[i][num] = col[j][num] = grid[i / 3][j / 3]
//								[num] = true;
//							if (dfs(board) == true) return true; 
//							// 恢复现场
//							board[i][j] = '.';
//							row[i][num] = col[j][num] = grid[i / 3][j / 3]
//								[num] = false;
//						}
//					}
//					return false; 
//				}
//			}
//		}
//		return true; 
//	}
//};

class Solution
{
	bool vis[21][21];
	int dx[4] = { 1, -1, 0, 0 };
	int dy[4] = { 0, 0, 1, -1 };
	int ret;
	int m, n, step;
public:
	int uniquePathsIII(vector<vector<int>>& grid)
	{
		m = grid.size(), n = grid[0].size();
		int bx = 0, by = 0;
		for (int i = 0; i < m; i++)
			for (int j = 0; j < n; j++)
				if (grid[i][j] == 0) step++;
				else if (grid[i][j] == 1)
				{
					bx = i;
					by = j;
				}
		step += 2;
		vis[bx][by] = true;
		dfs(grid, bx, by, 1);
		return ret;
	}
	void dfs(vector<vector<int>>& grid, int i, int j, int count)
	{
		if (grid[i][j] == 2)
		{
			if (count == step) // 判断是否合法
				ret++;
			return;
		}
		for (int k = 0; k < 4; k++)
		{
			int x = i + dx[k], y = j + dy[k];
			if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && grid[x][y]
				!= -1)
			{
				vis[x][y] = true;
				dfs(grid, x, y, count + 1);
				vis[x][y] = false;
			}
		}
	}
};