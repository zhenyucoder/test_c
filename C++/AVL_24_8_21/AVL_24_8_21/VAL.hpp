#pragma once

#include <iostream>
#include <cassert>

template <class K, class V>
struct AVLTreeNode
{
	AVLTreeNode(const std::pair<K, V> &kv)
		:_kv(kv), _left(nullptr), _right(nullptr), _parent(nullptr), _bf(0)
	{}
	~AVLTreeNode()
	{}
	std::pair<K, V> _kv;
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	int _bf; // 平衡因子
};



template <class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	void Insert(const std::pair<K, V> &kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return;
			}
		}

		//开始插入节点
		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_left = cur;
			cur->_parent = parent;
		}

		// 更新平衡因子，调整不符合AVL数的节点
		while (parent)
		{
			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}

			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == -1 || parent->_bf == 1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == -2 || parent->_bf == 2)
			{//不符合AVL特定，旋转调整
				if (parent->_bf == 2 && cur->_bf == 1)
					RotateL(parent);
				else if (parent->_bf == -2 && cur->_bf == -1)
					RotateR(parent);
				else if (parent->_bf == 2 && cur->_bf == -1)
					RotateRL(parent);
				else if (parent->_bf == -2 && cur->_bf == 1)
					RotateLR(parent);

				// 旋转让子树平衡
				// 降低子树的高度，恢复到插入前
				break;
			}
			else
			{
				assert(false);
			}
		}
	}

	void RotateL(Node *parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* PParent = parent->_parent;

		parent->_right = subRL;
		if(subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		parent->_parent = subR;
			
		if (parent == _root)
		{
			_root = subR; 
			subR->_parent = nullptr;
		}
		else
		{
			if (PParent->_left == parent)
			{
				PParent->_left = subR;
			}
			else
			{
				PParent->_right = subR;
			}
			subR->_parent = PParent;
		}
		parent->_bf = subR->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* PParent = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (PParent->_left == parent)
			{
				PParent->_left = subL;
			}
			else
			{
				PParent->_right = subL;
			}
			subL->_parent = PParent;
		}

		parent->_bf = subL->_bf = 0;
	}

	void RotateRL(Node* parent)
	{
		Node *subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateR(parent->_right);
		RotateL(parent);

		if (bf == 0)
		{
			parent->_bf = subR->_bf = subRL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			subR->_bf = 0;
			subRL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			subR->_bf = 1;
			subRL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(parent->_left);
		RotateR(parent);

		if (bf == 0)
		{
			parent->_bf = subL->_bf = subLR->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subL->_bf = 0;
			subLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	bool IsBalance()
	{
		return _IsBalance(_root);
	}

	void Inorder()
	{
		_Inorder(_root);
	}


private:
	int _Height(Node* root)
	{
		if (root == nullptr) return 0;
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}

	bool _IsBalance(Node* root)
	{
		if (root == nullptr) return true;
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		if (rightHeight - leftHeight != root->_bf)
		{
			std::cout << root->_kv.first << "负载因子异常" << std::endl;
			return false;
		}
		return abs(rightHeight - leftHeight) < 2 && _IsBalance(root->_left) && _IsBalance(root->_left);
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr) return;
		_Inorder(root->_left);
		std::cout << root->_kv.first << " : " << root->_kv.second << std::endl;
		_Inorder(root->_right);
	}
private:
	Node* _root = nullptr;
};