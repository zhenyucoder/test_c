﻿//class Solution
//{
//public:
//	int largestSumAfterKNegations(vector<int>& nums, int k)
//	{
//		int m = 0, minElem = INT_MAX, n = nums.size();
//		for (auto x : nums)
//		{
//			if (x < 0) m++;
//			minElem = min(minElem, abs(x)); 
//		}
//		int ret = 0;
//		if (m > k)
//		{
//			sort(nums.begin(), nums.end());
//			for (int i = 0; i < k; i++) // 前 k ⼩个负数，变成正数
//			{
//				ret += -nums[i];
//			}
//			for (int i = k; i < n; i++)
//			{
//				 ret += nums[i];
//			}
//		}
//		else
//		{
//			// 把所有的负数变成正数
//			for (auto x : nums) ret += abs(x);
//			if ((k - m) % 2) // 判断是否处理最⼩的正数
//			{
//				ret -= minElem * 2;
//			}
//		}
//		return ret;
//	}
//};


//class Solution
//{
//public:
//	vector<string> sortPeople(vector<string>& names, vector<int>& heights)
//	{
//		int n = names.size();
//		vector<int> index(n);
//		for (int i = 0; i < n; i++) index[i] = i;
//		//对下标进⾏排序
//		sort(index.begin(), index.end(), [&](int i, int j)
//			{
//				return heights[i] > heights[j];
//			});
//		//提取结果
//		vector<string> ret;
//		for (int i : index)
//		{
//			ret.push_back(names[i]);
//		}
//		return ret;
//	}
//};



class Solution
{
public:
	vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2)
	{
		int n = nums1.size();
		//排序
		sort(nums1.begin(), nums1.end());
		vector<int> index2(n);
		for (int i = 0; i < n; i++) index2[i] = i;
		sort(index2.begin(), index2.end(), [&](int i, int j)
			{
				return nums2[i] < nums2[j];
			});
		//⽥忌赛⻢
		vector<int> ret(n);
		int left = 0, right = n - 1;
		for (auto x : nums1)
		{
			if (x > nums2[index2[left]]) ret[index2[left++]] = x;
			else ret[index2[right--]] = x;
		}
		return ret;
	}
};