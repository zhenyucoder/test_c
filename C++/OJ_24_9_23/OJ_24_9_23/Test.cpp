//class Solution
//{
//	struct cmp
//	{
//		bool operator()(ListNode* l1, ListNode* l2)
//		{
//			return l1->val > l2->val;
//		}
//	};
//public:
//	ListNode* mergeKLists(vector<ListNode*>& lists)
//	{
//		priority_queue<ListNode*, vector<ListNode*>, cmp> heap; 
//		for (auto head : lists)
//		{
//			if (head != nullptr)
//			{
//				heap.push(head);
//			}
//		}
//		ListNode* ret = new ListNode(0);
//		ListNode* prev = ret;
//		while (heap.size())
//		{
//			ListNode* t = heap.top();
//			heap.pop();
//			prev = prev->next = t;
//			if (t->next != nullptr)
//			{
//				heap.push(t->next);
//			}
//		}
//		return ret->next;
//	}
//};



//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;
//const int N = 1e5 + 10;
//pair<int, string> sp[N];
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 0; i < n; i++)
//	{
//		cin >> sp[i].second >> sp[i].first;
//	}
//	sort(sp, sp + n);
//	for (int i = 0; i < n; i++)
//	{
//		cout << sp[i].second << endl;
//	}
//	return 0;
//}




#include <iostream>
#include <cstring>
using namespace std;
typedef long long LL;
const int N = 1010;
int n, k;
LL a[N];
LL dp[N][N];
int main()
{
	cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> a[i];
	memset(dp, -0x3f, sizeof dp);
	dp[0][0] = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 0; j < k; j++)
		{
			dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - a[i] % k + k) % k] +
				a[i]);
		}
	}
	if (dp[n][0] <= 0) cout << -1 << endl;
	else cout << dp[n][0] << endl;
	return 0;
}