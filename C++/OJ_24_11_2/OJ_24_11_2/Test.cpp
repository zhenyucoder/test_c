//typedef long long LL;
//LL dp[100010] = { 0 };
//LL n, q, tmp;
//
//int main()
//{
//    cin >> n >> q;
//    // 前缀和统计
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> tmp;
//        dp[i] = dp[i - 1] + tmp;
//    }
//
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << (dp[r] - dp[l - 1]) << endl;
//    }
//    return 0;
//}




//typedef long long LL;
//LL n, m, q, tmp;
//LL dp[1010][1010];
//
//int main()
//{
//    cin >> n >> m >> q;
//    // 统计二维前缀和
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> tmp;
//            dp[i][j] = tmp + dp[i][j - 1] + dp[i - 1][j] - dp[i - 1][j - 1];
//        }
//    }
//
//    LL x1, x2, y1, y2;
//    while (q--)
//    {
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << (dp[x2][y2] - dp[x2][y1 - 1] -
//            dp[x1 - 1][y2] + dp[x1 - 1][y1 - 1]) << endl;
//    }
//    return 0;
//}





//class Solution {
//public:
//    int dpl[10010] = { 0 }, dpr[10010] = { 0 };
//    int pivotIndex(vector<int>& nums) {
//        // 分别从左往右、从右往左统计前缀和
//        int n = nums.size();
//        for (int i = 1; i <= n; i++)
//            dpl[i] = dpl[i - 1] + nums[i - 1];
//        for (int i = n; i > 0; i--)
//            dpr[i] = dpr[i + 1] + nums[i - 1];
//        // 寻找中心目标
//        for (int i = 1; i <= n; i++)
//        {
//            if (dpl[i] == dpr[i])
//                return i - 1;
//        }
//        return -1;
//    }
//};




//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> lprod(n + 1), rprod(n + 1);
//        lprod[0] = 1, rprod[n - 1] = 1;
//        for (int i = 1; i < n; i++)
//            lprod[i] = lprod[i - 1] * nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            rprod[i] = rprod[i + 1] * nums[i + 1];
//        vector<int> ret(n);
//        for (int i = 0; i < n; i++)
//            ret[i] = lprod[i] * rprod[i];
//        return ret;
//    }
//};




//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        for (int i = 1; i <= n; i++)
//            dp[i] = dp[i - 1] + nums[i - 1];
//        int ret = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = i - 1; j >= 0; j--)
//            {
//                if (dp[i] - dp[j] == k)
//                    ret++;
//            }
//        }
//        return ret;
//    }
//};




