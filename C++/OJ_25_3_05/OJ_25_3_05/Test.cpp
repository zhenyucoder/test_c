//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        if (head == nullptr) return nullptr;
//        Node* cur = head;
//        while (cur) // 复制节点
//        {
//            Node* next = cur->next;
//            Node* copy = new Node(cur->val);
//            cur->next = copy;
//            copy->next = next;
//            cur = next;
//        }
//
//        // random处理
//        cur = head;
//        while (cur)
//        {
//            if (cur->random)
//                cur->next->random = cur->random->next;
//            cur = cur->next->next;
//        }
//
//        // 恢复链表，提取复制节点
//        Node* newhead = new Node(0), * prev = newhead;
//        cur = head;
//        while (cur)
//        {
//            Node* next = cur->next->next;
//            prev->next = cur->next;
//            prev = prev->next;
//            cur->next = next;
//            cur = cur->next;
//        }
//        prev->next = nullptr;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        vector<int> dp(n + 1);
//        dp[0] = dp[1] = 1;
//        for (int i = 2; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[n];
//    }
//};


