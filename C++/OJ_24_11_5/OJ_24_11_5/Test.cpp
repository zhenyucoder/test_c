//class Solution {
//public:
//    int hammingWeight(int n) {
//        int ret = 0;
//        while (n)
//        {
//            ret++;
//            n &= (n - 1); // 除去最右侧的1
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<int> countBits(int n) {
//        vector<int> ret(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            int ans = 0, x = i;
//            while (x)
//            {
//                ans++;
//                x &= (x - 1);
//            }
//            ret[i] = ans;
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int target = x ^ y;
//        //计算target中1的个数
//        int count = 0;
//        while (target)
//        {
//            target &= (target - 1);
//            count++;
//        }
//        return count;
//    }
//};



//class Solution {
//public:
//    bool isUnique(string astr) {
//        int hash[128] = { 0 };
//        for (auto ch : astr)
//            if (++hash[ch - 'a'] > 1) return false;
//        return true;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto x : nums)
//        {
//            ret ^= x;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int sum = 0;
//        for (auto num : nums)
//        {
//            sum ^= num;
//        }
//
//        int lsb = (sum == INT_MIN ? sum : sum & (-sum));
//        int x1 = 0, x2 = 0;
//        for (auto num : nums)
//        {
//            if (num & lsb)
//                x1 ^= num;
//            else
//                x2 ^= num;
//        }
//        return { x1, x2 };
//    }


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i <= nums.size(); i++)
//            ret ^= i;
//        for (auto x : nums)
//            ret ^= x;
//        return ret;
//    }
//};



