//class Solution {
//public:
//    int dp[100010] = { 0 };
//    int pos = 0;
//    int LIS(vector<int>& a) {
//        for (auto x : a)
//        {
//            if (pos == 0 || x > dp[pos])
//            {
//                dp[++pos] = x;
//            }
//            else
//            {
//                int left = 1, right = pos;
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2; // ?
//                    if (dp[mid] >= x)
//                        right = mid;
//                    else
//                        left = mid + 1;
//                }
//                dp[left] = x;
//            }
//        }
//        return pos;
//    }
//};


//#include <iostream>
//#include <queue>
//
//using namespace std;
//
//int main()
//{
//    int n, k, x;
//    cin >> n >> k;
//    priority_queue<int, vector<int>, greater<int>> pq;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> x;
//        pq.push(x);
//    }
//
//    int ret = 0;
//    while (k >= pq.top())
//    {
//        ret++;
//        int top = pq.top();
//        pq.pop();
//        k -= top;
//        pq.push(2 * top);
//    }
//    cout << ret << endl;
//    return 0;
//}



//#include <iostream>
//using namespace std;
//
//const int N = 20, M = N * 9;
//
//int n, sum;
//int arr[N];
//bool dp[M];
//
//bool func()
//{
//    if (sum % 2 == 1) return false;
//    sum /= 2;
//    dp[0] = true;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = sum; j >= arr[i]; j--)
//        {
//            dp[j] = dp[j] || dp[j - arr[i]];
//        }
//    }
//    return dp[sum];
//}
//
//int main()
//{
//    long long x;
//    cin >> x;
//    while (x)
//    {
//        int temp = x % 10;
//        x /= 10;
//        sum += temp;
//        arr[++n] = temp;
//    }
//    cout << (func() ? "Yes" : "No") << endl;
//    return 0;
//}


#include <iostream>
using namespace std;
typedef long long LL;
LL t;
LL n, a, b;
LL fun()
{
	if (n <= 2) return min(a, b);
	LL ret = 0;
	if (a * 3 < b * 2) 
	{
		ret += n / 2 * a;
		n %= 2;
		if (n) ret += min(min(a, b), b - a);
	}
	else 
	{
		ret += n / 3 * b;
		n %= 3;
		if (n == 1) ret += min(min(a, b), 2 * a - b);
		if (n == 2) ret += min(min(a, b), 3 * a - b);
	}
	return ret;
}
int main()
{
	cin >> t;
	while (t--)
	{
		cin >> n >> a >> b;
		cout << fun() << endl;
	}

	return 0;
}