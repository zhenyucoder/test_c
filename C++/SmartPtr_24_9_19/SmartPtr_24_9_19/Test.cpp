#include <iostream>
#include <string>
#include <memory>
#include <mutex>

using namespace std;
//
//template<class T>
//class SmartPtr
//{
//public:
//	SmartPtr(T* ptr = nullptr)
//		:_ptr(ptr)
//	{}
//	~SmartPtr()
//	{
//		cout << "~SmartPtr()" << endl;
//		delete _ptr;
//	}
//public:
//	T* operator->()
//	{
//		return _ptr;
//	}
//	T& operator*()
//	{
//		return *_ptr;
//	}
//private:
//	T* _ptr;
//	int a = 20;
//};
//
//struct Date
//{
//	int _year;
//	int _month;
//	int _day;
//};
//
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	SmartPtr<int> p1(new int(10));
//	cout << *p1 << endl;
//	*p1 = 20;
//	cout << *p1 << endl;
//
//	SmartPtr<Date> d1(new Date{ 2024, 9, 19 });
//	cout << d1->_year << ":" << d1->_month << ":" << d1->_day << endl;
//
//	cout << div() << endl;
//
//	// 拷贝后会导致资源重复释放，程序崩溃
//	//SmartPtr<int> p2;
//	//p2 = p1;
//}
//
//int main()
//{
//	try {
//		Func();
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}



namespace Own
{
	// 坑，管理权转移，如果使用着不熟悉。。。
	template <class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr = nullptr) :_ptr(ptr)
		{}
		~auto_ptr()
		{
			if(_ptr)
				delete _ptr;
			cout << "~auto_ptr()" << endl;
		}
	public:
		auto_ptr(auto_ptr& p)
			:_ptr(p._ptr)
		{
			p._ptr = nullptr;
		}
		auto_ptr<T>& operator=(auto_ptr<T>& ap)
		{
			if (this != &ap)
			{
				delete _ptr;
				_ptr = ap._ptr;
				ap._ptr = nullptr;
			}
			return *this;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};

	// 直接将拷贝有关成员函数禁掉
	template <class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr = nullptr) :_ptr(ptr)
		{}
		~unique_ptr()
		{
			delete _ptr;
			cout << "~unique_ptr()" << endl;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
	public: 
		// C++11之前实现方式： 只声明不实现 同时访问权限设置为私有
		unique_ptr(const unique_ptr<T>& up) = delete;
		unique_ptr<T>& operator=(const unique_ptr<T>& up) = delete;
	private:
		T* _ptr;
	};

	template <class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr = nullptr)
			: _ptr(ptr), _pRefcount(new int(1)), _pmtx(new mutex)
		{}
		~shared_ptr()
		{
			Release();
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* get() const
		{
			return _ptr;
		}
		int use_count()
		{
			return *_pRefcount;
		}
	public:
		void AddRef()
		{
			_pmtx->lock();
			++(*_pRefcount);
			_pmtx->unlock();
		}

		void Release()
		{
			_pmtx->lock();
			bool flag = false;
			if (--(*_pRefcount) == 0) // 调用复制重载时，可能会导致为空
			{
				cout << "delete: " << _ptr << endl;
				delete _ptr;
				delete _pRefcount;
				flag = true;
			}
			_pmtx -> unlock();
			if (flag)
				delete _pmtx;
		}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr), _pRefcount(sp._pRefcount), _pmtx(sp._pmtx)
		{
			AddRef();
		}

		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (_ptr != sp._ptr) // 不可直接this != &sp, 有可能间接指向
			{
				Release(); // 先将原本自身资源释放

				_ptr = sp._ptr;
				_pRefcount = sp._pRefcount;
				_pmtx = sp._pmtx;

				AddRef();
			}
			return *this;
		}
	private:
		T* _ptr;
		int* _pRefcount; // 设置为静态不行，所有该类对象只存在一个
		mutex* _pmtx;
	};


	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}
		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}
		weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			_ptr = sp.get();
			return *this;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};
}

struct ListNode
{
	int _data;
	Own::shared_ptr<ListNode> _prev;
	Own::shared_ptr<ListNode> _next;
	~ListNode()
	{
		cout << "~ListNode()" << endl;
	}
};

int main()
{
	Own::shared_ptr<ListNode> n1 = new ListNode;
	Own::shared_ptr<ListNode> n2 = new ListNode;

	n1->_next = n2;
	n2->_prev = n1;

	//Own::shared_ptr<int> sp(new int(10));
	//Own::shared_ptr<int> sp1 = sp;
	//Own::shared_ptr<int> sp2(new int(11));
	//sp2 = sp;
	
	//Own::unique_ptr<int> up(new int(10));
	////Own::unique_ptr<int> up1 = up;
	//Own::unique_ptr<int> up2;
	//up = up2;

	//Own::auto_ptr<int> ap(new int(10));
	//Own::auto_ptr<int> ap1 = ap;
	//Own::auto_ptr<int> ap2;
	//ap2 = ap1;
	return 0;
}




template <class T>
class shared_ptr
{
public:
	shared_ptr(T* ptr = nullptr) :_ptr(ptr), _pRefcount(new int(1)), _mtx(new mutex)
	{
	}

	~shared_ptr()
	{
		Release();
	}

	T* operator->()
	{
		return _ptr;
	}

	T& operator*()
	{
		retunr* _ptr;
	}
public:
	shared_ptr(const shared_ptr<T>& sp)
		:_ptr(sp->_ptr), _pRefcount(sp->_pRefcount), _mtx(sp->_mtx)
	{
		AddRef();
	}

	shared_ptr<T>& operator=(const shared_ptr<T>& sp)
	{
		if (_ptr != sp->_ptr)
		{
			Release();
			_ptr(sp->_ptr);
			_pRefcount(sp->_pRefcount);
			_mtx(sp->_mtx);
			AddRef();
		}
		return *this;
	}

public:
	void Release()
	{
		_mtx->lock();
		int flag = false;
		if (--(*_pRefcount) == 0)
		{
			flag = true;
			delete _ptr;
			delete _pRefcount;
		}
		_mtx->unlock();
		if (flag) delete _mtx;
	}
	void AddRef()
	{
		_mtx->lock();
		++(*_pRefcount);
		_mtx->unlock();
	}
private:
	T* _ptr;
	int* _pRefcount;
	mutex* _mtx; // 禁止拷贝
};