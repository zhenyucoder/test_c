//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        int n = nums.size();
//        for (int i = 1; i < n; i++)
//            for (int j = i - 1; j >= 0; j--)
//                if (nums[i] + nums[j] == target)
//                    return { i, j };
//        return { 0, 0 };
//    }
//};


//�Ż�1
//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        unordered_map<int, int> hash;
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            hash[nums[i]] = i;
//        }
//
//        for (int i = 0; i < n; i++)
//        {
//            int x = target - nums[i];
//            if (hash.count(x) && i != hash[x])
//                return { i, hash[x] };
//        }
//        return { 0, 0 };
//    }
//};

//�Ż�2
//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        unordered_map<int, int> hash;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            int x = target - nums[i];
//            if (hash.count(x))
//                return { hash[x], i };
//            hash[nums[i]] = i;
//        }
//        return { 0, 0 };
//    }
//};



//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        if (s1.size() != s2.size()) return false;
//        int hash[26] = { 0 };
//        for (auto ch : s1)
//            hash[ch - 'a']++;
//        for (auto ch : s2)
//        {
//            hash[ch - 'a']--;
//            if (hash[ch - 'a'] < 0) return false;
//        }
//        return true;
//    }
//};



//class Solution {
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            int x = nums[i];
//            if (hash.count(x) && i - hash[x] <= k)
//                return true;
//            hash[x] = i;
//        }
//        return false;
//    }
//};



class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> hash;
        for (auto& str : strs)
        {
            string tmp = str;
            sort(tmp.begin(), tmp.end());
            hash[tmp].push_back(str);
        }

        vector<vector<string>> ret;
        for (auto& [x, y] : hash)
            ret.push_back(y);
        return ret;
    }
};