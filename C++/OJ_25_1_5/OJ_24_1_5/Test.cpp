//class Solution {
//public:
//    int minimumTotal(vector<vector<int>>& triangle) {
//        int n = triangle.size();
//        if (n == 1) return triangle[0][0];
//        vector<int> dp(n + 1, INT_MAX);
//        dp[0] = triangle[0][0];
//
//        for (int i = 1; i < n; i++)
//        {
//            int end = triangle[i].size() - 1;
//            dp[end] = dp[end - 1] + triangle[i][end];
//            for (--end; end > 0; end--)
//            {
//                dp[end] = min(dp[end - 1], dp[end]) + triangle[i][end];
//            }
//            dp[0] += triangle[i][0];
//        }
//
//        int ret = INT_MAX;
//        for (int i = 0; i < triangle[n - 1].size(); i++)
//            ret = min(ret, dp[i]);
//        return ret;
//    }
//};





//class Solution {
//public:
//    int ret = INT_MAX;
//    int n = 0;
//    int minimumTotal(vector<vector<int>>& triangle) {
//        n = triangle.size();
//        if (n == 0) return 0;
//        dfs(triangle, 0, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<vector<int>>& triangle, int pos, int pathSum, int index)
//    {
//        if (pos == n)
//        {
//            ret = min(ret, pathSum);
//            return;
//        }
//
//        int curlen = triangle[pos].size();
//        if (index < curlen)
//            dfs(triangle, pos + 1, pathSum + triangle[pos][index], index);
//        if (index + 1 < curlen)
//            dfs(triangle, pos + 1, pathSum + triangle[pos][index + 1], index + 1);
//        return;
//    }
//};
//

//class Solution {
//public:
//    bool isAnagram(string s, string t) {
//        if (s.size() != t.size()) return false;
//        int hash1[26] = { 0 }, hash2[26] = { 0 };
//        for (auto ch : s)
//        {
//            hash1[ch - 'a']++;
//        }
//
//        for (auto ch : t)
//        {
//            if (++hash2[ch - 'a'] > hash1[ch - 'a']) return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n <= 2) return n;
//        vector<int> dp(n + 1);
//        dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= n; i++)
//        {
//            dp[i] = dp[i - 1] + dp[i - 2];
//        }
//        return dp[n];
//    }
//
//};


//class Solution {
//public:
//    int ListLen(ListNode* head)
//    {
//        ListNode* cur = head;
//        int len = 0;
//        while (cur)
//        {
//            cur = cur->next;
//            len++;
//        }
//        return len;
//    }
//
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (head == nullptr || k == 0) return head;
//        int len = ListLen(head);
//        ListNode* newhead = nullptr, * cur = head, * prev = nullptr;
//        int gap = len - k % len;
//        if (gap == len) return head;
//        for (int i = 0; i < gap; i++)
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//
//        // 查找到新链表的开头
//        newhead = cur;
//        prev->next = nullptr;
//        while (cur) // 查找到原链表结尾
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//
//        // 连接
//        std::cout << (newhead) << ": " << (prev) << endl;
//        prev->next = head;
//        return newhead;
//    }
//};

//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (k == 0 || head == nullptr || head->next == nullptr) return head;
//
//        // 将链表首尾连接，并统计链表长度
//        ListNode* cur = head;
//        int len = 1;
//        while (cur->next)
//        {
//            cur = cur->next;
//            len++;
//        }
//        int gap = len - k % len;
//        if (gap == len) return head; // 不需要旋转
//        cur->next = head;
//
//        while (gap--)// 在目标链表头节点的前一个节点停下
//            cur = cur->next;
//        ListNode* newhead = cur->next;
//        cur->next = nullptr;
//        return newhead;
//    }
//};






//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        // 拷贝节点，并连接到链表中
//        Node* cur = head;
//        while (cur)
//        {
//            Node* next = cur->next;
//            Node* copy = new Node(cur->val);
//
//            cur->next = copy;
//            copy->next = next;
//            cur = next;
//        }
//
//        // 初始化拷贝节点的random指针
//        cur = head;
//        while (cur)
//        {
//            if (cur->random)
//                cur->next->random = cur->random->next;
//            cur = cur->next->next;
//        }
//
//        // 恢复原链表和提取copy节点，构造新链表
//        Node* newhead = new Node(0), * prev = newhead;
//        cur = head;
//        while (cur)
//        {
//            Node* next = cur->next->next;
//
//            prev->next = cur->next;
//            prev = prev->next;
//
//            cur->next = next;
//            cur = next;
//        }
//        prev->next = nullptr;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Solution {
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        unordered_map<string, vector<string>> hash;
//        for (auto& str : strs)
//        {
//            string tmp = str;
//            sort(tmp.begin(), tmp.end());
//            if (hash.find(tmp) != hash.end())
//                hash[tmp].push_back(str);
//            else
//                hash.insert({ tmp, {str} });
//        }
//
//        vector<vector<string>> ret;
//        for (auto& [a, b] : hash)
//            ret.push_back(b);
//        return ret;
//    }
//};