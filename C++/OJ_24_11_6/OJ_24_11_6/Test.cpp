//class Solution
//{
//	int aim;
//	vector<int> path;
//	vector<vector<int>> ret;
//public:
//	vector<vector<int>> combinationSum(vector<int>& nums, int target)
//	{
//		aim = target;
//		dfs(nums, 0, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos, int sum)
//	{
//		if (sum == aim)
//		{
//			ret.push_back(path);
//			return;
//		}
//		if (sum > aim || pos == nums.size()) return;
//		for (int k = 0; k * nums[pos] + sum <= aim; k++)
//		{
//			if (k) path.push_back(nums[pos]);
//			dfs(nums, pos + 1, sum + k * nums[pos]);
//		}
//		for (int k = 1; k * nums[pos] + sum <= aim; k++)
//		{
//			path.pop_back();
//		}
//	}
//};




class Solution
{
	bool check[16];
	int ret;
public:
	int countArrangement(int n)
	{
		dfs(1, n);
		return ret;
	}
	void dfs(int pos, int n)
	{
		if (pos == n + 1)
		{
			ret++;
			return;
		}
		for (int i = 1; i <= n; i++)
		{
			if (!check[i] && (pos % i == 0 || i % pos == 0))
			{
				check[i] = true;
				dfs(pos + 1, n);
				check[i] = false; 
			}
		}
	}
};