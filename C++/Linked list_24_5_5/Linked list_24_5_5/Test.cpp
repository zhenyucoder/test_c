//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* newhead = new ListNode, * cur = newhead, * cur1 = l1, * cur2 = l2;
//        int tmp = 0;//进位
//        while (cur1 || cur2 || tmp)
//        {
//            int sum = tmp;
//            if (cur1)
//            {
//                sum += cur1->val;
//                cur1 = cur1->next;
//            }
//            if (cur2)
//            {
//                sum += cur2->val;
//                cur2 = cur2->next;
//            }
//
//            ListNode* node = new ListNode(sum % 10);
//            cur->next = node, cur = cur->next;
//            tmp = sum / 10;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};



//class Solution {
//public:
//    //翻转链表
//    ListNode* reverseList(ListNode* head)
//    {
//        ListNode* newhead = new ListNode(0), * cur = head;
//        while (cur)
//        {
//            ListNode* tmp = cur;
//            cur = cur->next;
//            //头插
//            tmp->next = newhead->next;
//            newhead->next = tmp;
//        }
//
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//
//    //链表相加
//    ListNode* _addInList(ListNode* head1, ListNode* head2)
//    {
//        ListNode* newhead = new ListNode(0), * cur = newhead, * cur1 = head1, * cur2 = head2;
//        int tmp = 0;//进位
//        while (cur1 || cur2 || tmp)
//        {
//            int sum = tmp;
//            if (cur1)
//            {
//                sum += cur1->val;
//                cur1 = cur1->next;
//            }
//            if (cur2)
//            {
//                sum += cur2->val;
//                cur2 = cur2->next;
//            }
//
//            ListNode* node = new ListNode(sum % 10);
//            cur->next = node;
//            cur = cur->next;
//            tmp = sum / 10;//更新进位
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//    ListNode* addInList(ListNode* head1, ListNode* head2) {
//        if (head1 == nullptr) return head2;
//        if (head2 == nullptr) return head1;
//        ListNode* cur1 = reverseList(head1);
//        ListNode* cur2 = reverseList(head2);
//        ListNode* newhead = _addInList(cur1, cur2);
//        return reverseList(newhead);
//    }
//};



//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* cur = head, * next = head->next, * nnext = next->next;
//        next->next = cur;
//        cur->next = swapPairs(nnext);
//        return next;
//    }
//};


//class Solution {
//public:
//    ListNode* reverse(ListNode* head)
//    {
//        ListNode* newhead = new ListNode, * cur = head;
//        while (cur)
//        {
//            ListNode* tmp = cur;
//            cur = cur->next;
//            //链接
//            tmp->next = newhead->next;
//            newhead->next = tmp;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//
//    void reorderList(ListNode* head1) {
//        if (head1 == nullptr || head1->next == nullptr) return;
//        //查找链表中间节点
//        ListNode* slow = head1, * fast = head1, * prev = nullptr;
//        while (fast && fast->next)
//        {
//            prev = slow;
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        prev->next = nullptr;
//        //逆序后半链表
//        ListNode* cur2 = reverse(slow);
//        //两链表相加
//        ListNode* cur1 = head1, * newhead = new ListNode, * cur = newhead;
//        while (cur1 && cur2)
//        {
//            ListNode* tmp1 = cur1, * tmp2 = cur2;
//            cur1 = cur1->next, cur2 = cur2->next;
//            //链接
//            cur->next = tmp1, cur = cur->next;
//            cur->next = tmp2, cur = cur->next;
//        }
//        delete newhead;
//    }
//};



//class Solution {
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        priority_queue < ListNode*, vector<ListNode*>, decltype([](ListNode* l1, ListNode* l2) { return l1->val > l2->val; }) > heap;
//
//        for (auto node : lists)
//            if (node)
//                heap.push(node);
//        while (!heap.empty())
//        {
//            ListNode* top = heap.top();
//            heap.pop();
//            //链接
//            prev->next = top;
//            prev = prev->next;
//            if (top->next)
//                heap.push(top->next);
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};

//class Solution {
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        return merge(lists, 0, lists.size() - 1);
//    }
//
//    ListNode* merge(vector<ListNode*>& lists, int left, int right)
//    {
//        if (left > right) return nullptr;
//        if (left == right) return lists[left];
//        int mid = left + (right - left) / 2;
//        ListNode* head1 = merge(lists, left, mid);
//        ListNode* head2 = merge(lists, mid + 1, right);
//        if (head1 == nullptr) return head2;
//        if (head2 == nullptr) return head1;
//        ListNode* newhead = new ListNode, * prev = newhead;
//        while (head1 && head2)
//        {
//            if (head1->val < head2->val)
//            {
//                prev->next = head1;
//                prev = head1;
//                head1 = head1->next;
//            }
//            else
//            {
//                prev->next = head2;
//                prev = head2;
//                head2 = head2->next;
//            }
//        }
//        if (head1) prev->next = head1;
//        if (head2) prev->next = head2;
//
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};


class Solution {
public:
    ListNode* reverseKGroup(ListNode* head, int k) {
        //逆序组数
        ListNode* cur = head;
        int n = 0;
        while (cur)
        {
            n++;
            cur = cur->next;
        }
        n /= k;
        //将n组节点逆序
        ListNode* newhead = new ListNode, * _newhead = newhead;
        cur = head;
        for (int i = 0; i < n; i++)
        {
            ListNode* tmp = cur;
            for (int j = 0; j < k; j++)
            {
                ListNode* next = cur->next;
                cur->next = _newhead->next;
                _newhead->next = cur;
                cur = next;
            }
            _newhead = tmp;
        }
        //链接剩余节点
        _newhead->next = cur;
        cur = newhead->next;
        delete newhead;
        return cur;
    }
};