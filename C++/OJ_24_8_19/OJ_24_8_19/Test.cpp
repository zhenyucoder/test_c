//#include <iostream>
//#include <cmath>
//using namespace std;
//
//int m, n, x, y;
//long long dp[25][25] = { 0 };
//int main()
//{
//    cin >> m >> n >> x >> y;
//    x += 1, y += 1;
//    dp[0][1] = 1;
//    for (int i = 1; i <= m + 1; i++)
//    {
//        for (int j = 1; j <= n + 1; j++)
//        {
//            if ((x != i && y != j && abs(x - i) + abs(y - j) == 3) || (i == x && j == y))
//                dp[i][j] = 0;
//            else
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        }
//    }
//    cout << dp[m + 1][n + 1] << endl;
//    return 0;
//}



//#include <iostream>
//using namespace std;
//long long n, m, a, b;
//int main()
//{
//	cin >> n >> m >> a >> b;
//
//	long long ret = 0;
//	for (long long x = 0; x <= min(n / 2, m); x++) 
//		long long y = min(n - x * 2, (m - x) / 2); 
//
//	cout << ret << endl;
//	return 0;
//}




//#include <iostream>
//using namespace std;
//const int N = 1e5 + 10;
//int n;
//int arr[N];
//int main()
//{
//	cin >> n;
//	for (int i = 0; i < n; i++) cin >> arr[i];
//	int ret = 0;
//	for (int i = 1; i < n; i++)
//		if (arr[i] > arr[i - 1])
//			ret += arr[i] - arr[i - 1];
//	cout << ret << endl;
//	return 0;
//}



//#include <iostream>
//#include <string>
//#include <algorithm>
//using namespace std;
//int main()
//{
//	string s;
//	getline(cin, s);
//	reverse(s.begin(), s.end());
//	int left = 0, n = s.size();
//	while (left < n)
//	{
//		int right = left;
//		while (right < n && s[right] != ' ') 
//		{
//			right++;
//		}
//		reverse(s.begin() + left, s.begin() + right);
//		while (right < n && s[right] == ' ') right++;
//		left = right;
//	}
//	cout << s << endl;
//	return 0;
//}



//#include <iostream>
//#include <string>
//using namespace std;
//int main()
//{
//	string s, t;
//	getline(cin, s);
//	getline(cin, t);
//	bool hash[300] = { 0 };
//	for (char ch : t) hash[ch] = true;
//	string ret;
//	for (auto ch : s)
//	{
//		if (!hash[ch])
//		{
//			ret += ch;
//		}
//	}
//	cout << ret << endl;
//	return 0;
//}



//	ListNode* FindFirstCommonNode(ListNode * pHead1, ListNode * pHead2)
//	{
//		ListNode* cur1 = pHead1, * cur2 = pHead2;
//		while (cur1 != cur2)
//		{
//			cur1 = cur1 != NULL ? cur1->next : pHead2;
//			cur2 = cur2 != NULL ? cur2->next : pHead1;
//		}
//		return cur1;
//	}



#include <iostream>
#include <string>
using namespace std;
int n;
string str;
int main()
{
	cin >> n >> str;

	long long s = 0, h = 0, y = 0;
	for (int i = 0; i < n; i++)
	{
		char ch = str[i];
		if (ch == 's') s++;
		else if (ch == 'h') h += s;
		else if (ch == 'y') y += h;
	}

	cout << y << endl;

	return 0;
}