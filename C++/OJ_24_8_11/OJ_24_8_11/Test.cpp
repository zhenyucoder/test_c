//#include <iostream>
//#include <string>
//using namespace std;
//
//
//int main()
//{
//    string str; cin >> str;
//    int begin = -1, maxlen = 0, n = str.size();
//    for (int i = 0; i < n; i++)
//    {
//        if (str[i] >= '1' && str[i] <= '9')
//        {
//            int len = 0, temp = i;
//            while (i < n && str[i] >= '1' && str[i] <= '9')
//            {
//                i++;
//            }
//            if (i - temp > maxlen)
//                begin = temp, maxlen = i - temp;
//        }
//    }
//    cout << str.substr(begin, maxlen) << endl;
//    return 0;
//}




//class Solution {
//public:
//    int m, n;
//    bool vis[401][401];
//    int dx[4] = { 1, -1, 0, 0 }, dy[4] = { 0, 0, 1, -1 };
//    int solve(vector<vector<char> >& grid) {
//        int ret = 0;
//        m = grid.size(), n = grid[0].size();
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (grid[i][j] == '1' && !vis[i][j])
//                    ret++, dfs(grid, i, j);
//            }
//        }
//        return ret;
//    }
//
//    void dfs(vector<vector<char> >& grid, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = dx[k] + i, y = dy[k] + j;
//            if (x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == '1' && !vis[x][y])
//            {
//                dfs(grid, x, y);
//            }
//        }
//    }
//};




//#include <iostream>
//#include <algorithm>
//using namespace std;
//int t;
//int arr[6];
//int main()
//{
//	cin >> t;
//	while (t--)
//	{
//		for (int i = 0; i < 6; i++) cin >> arr[i];
//		sort(arr, arr + 6);
//		if (arr[0] + arr[1] > arr[2] && arr[3] + arr[4] > arr[5] ||
//			arr[0] + arr[2] > arr[3] && arr[1] + arr[4] > arr[5] ||
//			arr[0] + arr[3] > arr[4] && arr[1] + arr[2] > arr[5] ||
//			arr[0] + arr[4] > arr[5] && arr[1] + arr[2] > arr[3])
//		{
//			cout << "Yes" << endl;
//		}
//		else cout << "No" << endl;
//	}
//
//	return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//bool vis[6];
//bool dfs(vector<int>& arr, int pos)
//{
//    if (pos == 3)
//    {
//        int arr1[3], arr2[3], step1 = 0, step2 = 0;
//
//        for (int i = 0; i < 6; i++)
//        {
//            if (vis[i])
//                arr1[step1++] = arr[i];
//            else
//                arr2[step2++] = arr[i];
//        }
//        return (arr1[1] + arr1[0] > arr1[2]) || (arr2[1] + arr2[0] > arr2[2]);
//    }
//
//    for (int i = 0; i < 6; i++)
//    {
//        if (!vis[i])
//        {
//            vis[i] = true;
//            if (dfs(arr, pos + 1)) return true;
//            vis[i] = false;
//        }
//    }
//    return false;
//}
//int main()
//{
//    vector<int> arr(6, 0);
//    int t; cin >> t;
//    while (t--)
//    {
//        for (int i = 0; i < 6; i++)
//            cin >> arr[i];
//        sort(arr.begin(), arr.end());
//        if (dfs(arr, 0))
//            cout << "Yes" << endl;
//        else
//            cout << "No" << endl;
//    }
//    return 0;
//}



class Solution {
public:
    int MLS(vector<int>& arr) {
        sort(arr.begin(), arr.end());
        int ret = 1, n = arr.size();
        int dp[100001];
        dp[0] = 1;
        for (int i = 1; i < n; i++)
        {
            int j = i - 1;
            if (arr[i] == arr[j])
                dp[i] = dp[j];
            else if (arr[i] - arr[j] == 1)
                dp[i] = dp[j] + 1;
            else
                dp[i] = 1;
            ret = max(ret, dp[i]);
        }
        return ret;
    }
};