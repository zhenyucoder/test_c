//select ifnull(
//    (select max(distinct salary) from Employee
//where salary < (select max(salary) from Employee)),
//    null)
//    as SecondHighestSalary;


//class Solution {
//public:
//    vector<int> grayCode(int n) 
//    {
//        vector<int> ret(1 << n);
//        for (int i = 0; i < ret.size(); i++)
//        {
//            ret[i] = (i >> 1) ^ i;
//        }
//        return ret;
//    }
//}


//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        vector<pair<TreeNode*, unsigned int>> queue;
//        unsigned int ret = 0;
//        queue.push_back({ root, 1 });
//        while (queue.size())
//        {
//            //更新结果
//            auto& [x1, y1] = queue[0];
//            auto& [x2, y2] = queue.back();
//            ret = max(ret, y2 - y1 + 1);
//            //跟新下一层
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            for (auto& [x, y] : queue)
//            {
//                if (x->left)
//                    tmp.push_back({ x->left, y * 2 });
//                if (x->right)
//                    tmp.push_back({ x->right, y * 2 + 1 });
//            }
//            queue = tmp;
//        }
//        return ret;
//    }
//};


SELECT
Department.NAME AS Department,
e1.NAME AS Employee,
e1.Salary AS Salary
FROM
Employee AS e1, Department
WHERE
e1.DepartmentId = Department.Id
AND 3 > (SELECT  count(DISTINCT e2.Salary)
	FROM	Employee AS e2
	WHERE	e1.Salary < e2.Salary 	AND e1.DepartmentId = e2.DepartmentId)
	ORDER BY Department.NAME, Salary DESC;