//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* newhead = new ListNode, * prev = newhead, * cur = head;
//        while (cur)
//        {
//            if (cur->val != val)
//            {
//                prev->next = cur;
//                prev = cur;
//            }
//            cur = cur->next;
//        }
//        prev->next = nullptr;
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newhead = new ListNode, * cur = head;
//        while (cur)
//        {
//            ListNode* tmp = cur;
//            cur = cur->next;
//            //头插
//            tmp->next = newhead->next;
//            newhead->next = tmp;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};



//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        return dfs(head);
//    }
//
//    ListNode* dfs(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* ret = dfs(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return ret;
//    }
//
//};




//class Solution {
//public:
//    ListNode* middleNode(ListNode* head) {
//        ListNode* fastNode = head, * slowNode = head;
//        while (fastNode && fastNode->next)
//        {
//            fastNode = fastNode->next->next;
//            slowNode = slowNode->next;
//        }
//        return slowNode;
//    }
//};





//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        return merge(list1, list2);
//    }
//
//    ListNode* merge(ListNode* head1, ListNode* head2)
//    {
//        if (head1 == nullptr) return head2;
//        if (head2 == nullptr) return head1;
//        if (head1->val < head2->val)
//        {
//            head1->next = merge(head1->next, head2);
//            return head1;
//        }
//        else
//        {
//            head2->next = merge(head2->next, head1);
//            return head2;
//        }
//    }
//};




//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newhead = new ListNode, * prev = newhead, * cur1 = list1, * cur2 = list2;
//        while (cur1 && cur2)
//        {
//            if (cur1->val < cur2->val)
//            {
//                prev->next = cur1;
//                prev = cur1;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                prev->next = cur2;
//                prev = cur2;
//                cur2 = cur2->next;
//            }
//        }
//        if (cur1) prev->next = cur1;
//        if (cur2) prev->next = cur2;
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//};




//class Partition {
//public:
//    ListNode* partition(ListNode* pHead, int x) {
//        ListNode* Snewhead = new ListNode(0), * Sprev = Snewhead;
//        ListNode* Bnewhead = new ListNode(0), * Bprev = Bnewhead;
//        ListNode* cur = pHead;
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                Sprev->next = cur;
//                Sprev = Sprev->next;
//            }
//            else
//            {
//                Bprev->next = cur;
//                Bprev = Bprev->next;
//            }
//            cur = cur->next;
//        }
//        //将两链表链接
//        Sprev->next = Bnewhead->next, Bprev->next = nullptr;
//        Sprev = Snewhead->next;
//        delete Snewhead;
//        delete Bnewhead;
//        return Sprev;
//    }
//};



//class PalindromeList {
//public:
//    ListNode* reverse(ListNode* head)
//    {
//        ListNode* newhead = new ListNode(0), * prev = newhead;
//        while (head)
//        {
//            ListNode* t = head;
//            head = head->next;
//            //头插
//            t->next = newhead->next;
//            newhead->next = t;
//        }
//        prev = newhead->next;
//        delete newhead;
//        return prev;
//    }
//
//    bool chkPalindrome(ListNode* A) {
//        ListNode* fast = A, * slow = A;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//
//            slow = slow->next;
//        }
//        ListNode* cur1 = A, * cur2 = reverse(slow);
//        while (cur1 && cur2)
//        {
//            if (cur1->val != cur2->val)
//                return false;
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return true;
//    }
//};




//class Solution {
//public:
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        ListNode* cur1 = headA, * cur2 = headB;
//        while (cur1 || cur2)
//        {
//            if (cur1 == cur2)
//                return cur1;
//            cur1 = cur1 == nullptr ? headB : cur1->next;
//            cur2 = cur2 == nullptr ? headA : cur2->next;
//        }
//        return nullptr;
//    }
//};



//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return false;
//        ListNode* slow = head, * fast = head->next;
//        while (fast && fast->next)
//        {
//            if (slow == fast)
//                return true;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        return false;
//    }
//};



//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return nullptr;
//        ListNode* slow = head, * fast = slow->next;
//        while (fast && fast->next)
//        {
//            if (fast == slow)
//                break;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        if (fast == nullptr || fast == nullptr) return nullptr;
//        //存在环
//        ListNode* cur1 = head, * cur2 = fast->next;
//        fast->next = nullptr, fast = cur2;//断开环形链表，并记录起始位置
//        while (cur1 || cur2)
//        {
//            if (cur1 == cur2)
//                break;
//            cur1 = cur1 == nullptr ? fast : cur1->next;
//            cur2 = cur2 == nullptr ? head : cur2->next;
//        }
//        return cur1;
//    }
//};




//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return nullptr;
//        ListNode* slow = head, * fast = slow->next;
//        while (fast && fast->next)
//        {
//            if (fast == slow)
//                break;
//            slow = slow->next;
//            fast = fast->next->next;
//        }
//        if (fast == nullptr || fast->next == nullptr) return nullptr;
//        //存在环
//        ListNode* cur1 = head, * cur2 = fast->next;
//        while (cur1 != cur2)
//        {
//            cur1 = cur1->next;
//            cur2 = cur2->next;
//        }
//        return cur1;
//    }
//};




class Solution {
public:
    Node* copyRandomList(Node* head) {
        if (head == nullptr) return nullptr;
        //将原节点深拷贝连接到被复制节点后
        Node* cur = head;
        while (cur)
        {
            Node* next = cur->next;//记录下一个节点
            Node* copynode = new Node(cur->val);
            cur->next = copynode;
            copynode->next = next;
            cur = next;
        }
        //更新深拷贝节点random指向
        cur = head;
        while (cur)
        {
            Node* copynode = cur->next;
            Node* next = copynode->next;
            //跟新深拷贝节点random指向
            if (cur->random)
                copynode->random = cur->random->next;

            cur = next;
        }

        //恢复链表
        Node* newhead = new Node(0), * prev = newhead;
        cur = head;
        while (cur)
        {
            Node* next = cur->next->next;

            prev->next = cur->next;
            prev = prev->next;

            cur->next = next;
            cur = next;
        }
        prev = newhead->next;
        delete newhead;
        return prev;
    }
};