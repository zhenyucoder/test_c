//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n, 1), g(n, 1);
//        int ret = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[i] > nums[j])
//                    f[i] = max(f[i], g[j] + 1);
//                else if (nums[i] < nums[j])
//                    g[i] = max(g[i], f[j] + 1);
//            }
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    const int DOM = pwo(10, 9) + 7;
//    int numDistinct(string s, string t) {
//        int m = s.size(), n = t.size();
//        s = " " + s, t = " " + t;
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //初始化
//        for (int i = 0; i <= m; i++)
//            dp[i][0] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = s[i] == t[j] ? (dp[i - 1][j - 1] + dp[i - 1][j]) % DOM : dp[i - 1][j];
//        return dp[m][n];
//    }
//};





//class Solution {
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        unordered_map<int, int> hash;
//        hash[nums[0]] = 0;
//        int n = nums.size(), ret = 2;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        for (int i = 1; i < n - 1; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                int k = 2 * nums[i] - nums[j];
//                if (hash.count(k))
//                {
//                    dp[i][j] = dp[hash[k]][i] + 1;
//                    ret = max(ret, dp[i][j]);
//                }
//            }
//            hash[nums[i]] = i;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int m = s.size(), n = p.size();
//        s = " " + s, p = " " + p;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        //初始化
//        dp[0][0] = true;
//        for (int i = 2; i <= n; i += 2)
//            if (p[i] == '*') dp[0][i] = true;
//            else break;
//        //填表
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                if (p[j] == '*')
//                    dp[i][j] = dp[i][j - 2] || (p[j - 1] == '.' || s[i] == p[j - 1]) && dp[i - 1][j];
//                else
//                    dp[i][j] = (p[j] == '.' || s[i] == p[j]) && dp[i - 1][j - 1];
//        return dp[m][n];
//    }
//};




class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        int sum = 0, n = nums.size();
        for (auto x : nums)
            sum += x;
        int aim = sum + target;
        if (aim % 2 || aim < 0) return 0;
        aim /= 2;

        vector<int> dp(aim + 1);
        dp[0] = 1;//初始化
        for (int i = 1; i <= n; i++)
            for (int j = aim; j >= 0; j--)
            {
                if (j - nums[i - 1] >= 0)
                    dp[j] += dp[j - nums[i - 1]];
            }
        return dp[aim];
    }
};