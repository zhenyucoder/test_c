//class Solution {
//public:
//    bool isUnique(string astr) {
//        unordered_map<char, int> hash;
//        for (auto ch : astr)
//            hash[ch]++;
//        for (auto& [a, b] : hash)
//        {
//            if (b != 1) return false;
//        }
//        return true;
//    }
//};



class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        unordered_map<string, function<int(int, int)>> opmap = {
            {"+", [](int x, int y) {return x + y; }},
            {"-", [](int x, int y) {return x - y; }},
            {"*", [](int x, int y) {return x * y; }},
            {"/", [](int x, int y) {return x / y; }}
        };

        stack<int> st;
        for (auto str : tokens)
        {
            if (opmap.find(str) != opmap.end())
            {
                int right = st.top(); st.pop();
                int left = st.top(); st.pop();
                st.push(opmap[str](left, right));
            }
            else
                st.push(stoi(str));
        }
        return st.top();
    }
};