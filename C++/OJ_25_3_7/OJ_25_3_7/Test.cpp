//class Solution {
//public:
//    vector<int> preorderTraversal(TreeNode* root) {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        stack<TreeNode*> st;
//        TreeNode* cur = root;
//        while (!st.empty() || cur)
//        {
//            while (cur)
//            {
//                ret.push_back(cur->val);
//                st.push(cur);
//                cur = cur->left;
//            }
//            cur = st.top()->right;
//            st.pop();
//        }
//        return ret;
//    }
//};




class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int prev = 0x3f3f3f3f, ret = 0;
        for (auto x : prices)
        {
            ret = max(ret, x - prev);
            prev = min(prev, x);
        }
        return ret;
    }
}