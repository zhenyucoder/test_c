//int main() {
//    string str;
//    getline(cin, str);
//    size_t i = str.rfind(' ');
//    cout << (str.size() - (i + 1)) << endl;
//}

//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int len = s.size();
//        int i = 0;
//        for (; i < len; i += 2 * k)
//        {
//            if (i + k < len)
//            {
//                reverse(s.begin() + i, s.begin() + i + k);
//            }
//            else
//                break;
//        }
//        if (i < len - 1)
//            reverse(s.begin() + i, s.end());
//        return s;
//    }
//};


#include <iostream>
#include <string>
using namespace std;

//class Solution {
//public:
//    string reverseWords(string& s) {
//        int len = s.size();
//        int start, end;
//        for (int i = 0; i < len; i = end+1)
//        {
//            start = i;
//            end = s.find(' ',start);
//            if (end == string::npos)
//                break;
//            reverse(s.begin() + start, s.begin() + end);
//        }
//        reverse(s.begin() + start, s.end());
//        return s;
//    }
//
//    void test()
//    {
//        string s("Let's take LeetCode contest");
//        reverseWords(s);
//        cout << s << endl;
//    }
//};
//
//
//int main()
//{
//
//    Solution s;
//    s.test();
//    return 0;
//}


//class Solution {
//public:
//    string addStrings(string num1, string num2) {
//        int end1 = num1.size() - 1, end2 = num2.size() - 1;
//        int value1, value2, value;
//        //进位
//        int next = 0;
//        string retadd;
//        while (end1 >= 0 || end2 >= 0)
//        {
//            int value1 = end1 >= 0 ? num1[end1] - '0' : 0;
//            int value2 = end2 >= 0 ? num2[end2] - '0' : 0;
//            int value = value1 + value2 + next;
//
//            next = value / 10;
//            retadd += value % 10 + '0';
//            end1--;
//            end2--;
//        }
//        if (next)
//            retadd += '1';
//        reverse(retadd.begin(), retadd.end());
//        return retadd;
//    }
//
//    void test()
//    {
//        string nums1 = "456", nums2 = "77";
//        cout << addStrings(nums1, nums2) << endl;
//    }
//};
//
//int main()
//{
//    Solution s;
//    s.test();
//    return 0;
//}


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        if (num1 == "0" || num2 == "0")
//            return "0";
//        string ans = "0";
//        int end1 = num1.size() - 1, end2 = num2.size() - 1;
//        for (int i = end2; i >= 0; i--)
//        {
//            string cur;
//            //处理nums2每位乘数0的个数
//            for (int j = end2; j > i; j--)
//            {
//                cur += '0';
//            }
//
//            //nums2中的乘数和nums1相乘
//            int y = num2[i] - '0';
//            int next = 0;//进位
//            for (int i = end1; i >= 0; i--)
//            {
//                int x = num1[i] - '0';
//                int product = x * y + next;
//                cur += product % 10 + '0';
//                next = product / 10;
//            }
//            //进一步处理进位问题
//            while (next)
//            {
//                cur += next % 10 + '0';
//                next /= 10;
//            }
//            reverse(cur.begin(), cur.end());
//            ans = addStrings(ans, cur);
//        }
//        return ans;
//    }
//
//    string addStrings(string& str1, string& str2)
//    {
//        int end1 = str1.size() - 1, end2 = str2.size() - 1;
//        string ret;
//        int next = 0;
//        while (end1 >= 0 || end2 >= 0)
//        {
//            int value1 = end1 >= 0 ? str1[end1] - '0' : 0;
//            int value2 = end2 >= 0 ? str2[end2] - '0' : 0;
//            int value = value1 + value2 + next;
//            ret += value % 10 + '0';
//            next = value / 10;
//            end1--;
//            end2--;
//        }
//        if (next)
//            ret += '1';
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};



class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1 == "0" || num2 == "0") 
        {
            return "0";
        }
        int m = num1.size(), n = num2.size();
        auto ansArr = vector<int>(m + n);
        for (int i = m - 1; i >= 0; i--) 
        {
            int x = num1.at(i) - '0';
            for (int j = n - 1; j >= 0; j--) 
            {
                int y = num2.at(j) - '0';
                ansArr[i + j + 1] += x * y;
            }
        }
        for (int i = m + n - 1; i > 0; i--) 
        {
            ansArr[i - 1] += ansArr[i] / 10;
            ansArr[i] %= 10;
        }
        int index = ansArr[0] == 0 ? 1 : 0;
        string ans;
        while (index < m + n) 
        {
            ans.push_back(ansArr[index]);
            index++;
        }
        for (auto& c : ans) 
        {
            c += '0';
        }
        return ans;
    }
};
