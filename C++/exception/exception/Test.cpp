#include <iostream>
#include <string>
#include <Windows.h>
using namespace std;

//class A
//{
//public:
//	A()
//	{
//		cout << "构造函数" << endl;
//	}
//
//	~A()
//	{
//		cout << "析构函数" << endl;
//	}
//};
//
//void Func1()
//{
//	throw "异常";
//}
//
//double Div(int x, int y)
//{
//	if (y == 0)
//	{
//		//throw "除0错误";//可以抛任意类型
//		string s("除0错误");
//		throw s;//抛临时对象，拷贝
//	}
//	else
//		return (double)x / y;
//}
//
//
//void Func()
//{
//	try
//	{
//		int x, y;
//		cin >> x >> y;
//		int ret = Div(x, y);
//		cout << ret << endl;
//		Func1();
//		A a;
//	}
//	catch (char* str)
//	{
//		cout << str << endl;
//	}
//}
//
//
//
//int main()
//{
//	while (1)
//	{
//		try
//		{
//			Func();;
//			cout << "12345678" << endl;
//		}
//		catch (string s)
//		{
//			cout << s << endl;
//		}
//		catch (...)
//		{
//			cout << "未知异常" << endl;
//		}
//	}
//	return 0;
//}





//class Exception
//{
//public:
//	Exception(const string& errmsg, int id)
//		:_errmsg(errmsg)
//		, _id(id)
//	{}
//	virtual string what() const
//	{
//		return _errmsg;
//	}
//protected:
//	string _errmsg;
//	int _id;
//};
//class SqlException : public Exception
//{
//public:
//	SqlException(const string& errmsg, int id, const string& sql)
//		:Exception(errmsg, id)
//		, _sql(sql)
//	{}
//	virtual string what() const
//	{
//		string str = "SqlException:";
//		str += _errmsg;
//		str += "->";
//		str += _sql;
//		return str;
//	}
//private:
//	const string _sql;
//};
//class CacheException : public Exception
//{
//public:
//	CacheException(const string& errmsg, int id)
//		:Exception(errmsg, id)
//	{}
//	virtual string what() const
//	{
//		string str = "CacheException:";
//		str += _errmsg;
//		return str;
//	}
//};
//class HttpServerException : public Exception
//{
//public:
//	HttpServerException(const string& errmsg, int id, const string& type)
//		:Exception(errmsg, id)
//		, _type(type)
//	{}
//	virtual string what() const
//	{
//		string str = "HttpServerException:";
//		str += _type;
//		str += ":";
//		str += _errmsg;
//		return str;
//	}
//private:
//	const string _type;
//};
//void SQLMgr()
//{
//	Sleep(1);
//	srand(time(0));
//	if (rand() % 7 == 0)
//	{
//		throw SqlException("权限不足", 100, "select * from name = '张三'");
//	}
//	//throw "xxxxxx";
//}
//void CacheMgr()
//{
//	Sleep(1);
//	srand(time(0));
//	if (rand() % 5 == 0)
//	{
//		throw CacheException("权限不足", 100);
//	}
//	else if (rand() % 6 == 0)
//	{
//		throw CacheException("数据不存在", 101);
//	}
//	SQLMgr();
//}
//void HttpServer()
//{
//	Sleep(1);
//	srand(time(0));
//	if (rand() % 3 == 0)
//	{
//		throw HttpServerException("请求资源不存在", 100, "get");
//	}
//	else if (rand() % 4 == 0)
//	{
//		throw HttpServerException("权限不足", 101, "post");
//	}
//	CacheMgr();
//}
//int main()
//{
//	while (1)
//	{
//		Sleep(1);
//		try {
//			HttpServer();
//		}
//		catch (const Exception& e) 
//		{
//			cout << e.what() << endl;
//		}
//		catch (...)
//		{
//			cout << "Unkown Exception" << endl;
//		}
//	}
//	return 0;
//}




//double Division(int a, int b) throw()//不是严格必须正确
double Division(int a, int b) noexcept//C++11新增关键字，表示不会抛异常，并且不会捕捉异常
{
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	return (double)a / (double)b;
}


void func() throw (std::bad_alloc)
{
	int* arr1 = new int[10];
	int* arr2 = new int[10];//如果arr2空间申请失败抛异常,arr1无法很好释放空间，导致内存泄漏
	try 
	{
		int len, time;
		cin >> len >> time;
		Division(len, time);
	}
	catch (...)
	{
		cout << "delete []" << endl;
		delete arr1;
		delete arr2;
		throw;//补到什么抛什么
	}

	cout << "delete []" << endl;
	delete arr1;
	delete arr2;
}

int main()
{
	while (1)
	{
		try
		{
			func();
		}
		catch (const char* errmsg)
		{
			cout << errmsg << endl;
		}
	}
	return 0;
}



