#include <iostream>
using namespace std;

//int main()
//{
//	/*printf("%s->%5.4s->%-10.3s", "zjutjxwzy20220503", "zjutjxwzy20220503", "zjutjxwzy20220503");
//	cout << endl;
//	cout << sizeof(unsigned) << endl;*/
//	struct A
//	{
//		short a : 15;
//	};
//
//	struct A1
//	{
//		short a : 11;
//		char b : 1;
//	};
//	cout << sizeof(A) << endl;
//	cout << sizeof(A1) << endl;
//	return 0;
//}



//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//int main() {
//	int n;
//	while (cin >> n)
//	{
//		long long sum = 0;
//		vector<int> a;
//		a.resize(3 * n);
//		for (long long i = 0; i < (3 * n); i++)
//		{
//			cin >> a[i];
//		}
//		sort(a.begin(), a.end());
//		for (int i = n; i < 3 * n; i += 2)
//		{
//			sum += a[i];
//		}
//		cout << sum << endl;
//	}
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//int main() {
//    string str1, str2;
//    getline(cin, str1);
//    getline(cin, str2);
//
//    int hash[256] = { 0 };
//    for (int i = 0; i < str2.size(); i++)
//    {
//        hash[str2[i]]++;
//    }
//
//    string ret;
//    for (int i = 0; i < str1.size(); i++)
//    {
//        if (hash[str1[i]] == 0)
//            ret.push_back(str1[i]);
//    }
//    cout << ret << endl;
//    return 0;
//}


//int main()
//{
//	int i = 1;
//	char* pi = (char*)&i;
//	if (*pi == 1)
//		cout << "小端" << endl;
//	else
//		cout << "大端"<<endl;
//	return 0;
//}


//#include <iostream>
//#include <math.h>
//using namespace std;
//
//int main() {
//    int n;
//    while (cin >> n)
//    {
//        long long count = 1, base = 10;
//        for (int i = 1; i <= n; i++)
//        {
//            long long pow_n = i * i;
//            if (i == base) base *= 10;
//            if (pow_n % base == i) count++;
//            // int flag =1;
//            // for(int pow = i*i, temp=i; temp>0; )
//            // {
//            //     if(pow%10 == temp%10)
//            //     {
//            //         pow /= 10;
//            //         temp /= 10;
//            //     }
//            //     else 
//            //     {
//            //         flag = 0;
//            //         break;
//            //     }
//            // }
//            // if(flag) count++;
//        }
//        cout << count << endl;
//    }
//    return 0;
//}


#include <iostream>
#include <math.h>
using namespace std;

// bool isPrime(int x)//判断是否为质数
// {
//     if(x<=1) return false;
//     for(int i=2; i<=sqrt(x); i++)
//     {
//         if(x % i == 0)
//             return false;
//     }
//     return true;
// }

//int main() {
//    int n = 0;
//    while (cin >> n)
//    {
//        int count = 0;
//        for (int i = 2; i < n; i++)
//        {
//            int flag = 1;//用于判断是否为质数
//            for (int j = 2; j <= sqrt(i); j++)
//            {
//                if (i % j == 0)
//                {
//                    flag = 0;
//                    break;
//                }
//            }
//            if (flag) count++;
//            // if(isPrime(i)) count++;
//        }
//        cout << count << endl;
//    }
//}


//class Solution {
//public:
//    int FirstNotRepeatingChar(string str) {
//        int hash[128] = { 0 };
//        //利用哈希映射，统计字符出现次数
//        for (auto e : str)
//        {
//            hash[e]++;
//        }
//        //判断是否存在只出现一次字符
//        for (int i = 0; i < str.size(); i++)
//        {
//            if (hash[str[i]] == 1)
//                return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool isUnique(string astr) {
//        int hash[128] = { 0 };
//        for (auto ch : astr)
//        {
//            hash[ch]++;
//        }
//        for (auto ch : astr)
//        {
//            if (hash[ch] != 1)
//                return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        if (s1.size() != s2.size())
//            return false;
//        int hash1[128] = { 0 }, hash2[182] = { 0 };
//        //存入哈希表
//        for (int i = 0; i < s1.size(); i++)
//        {
//            hash1[s1[i]]++, hash2[s2[i]]++;
//        }
//        //判断哈希表中所有数据是否相等
//        for (int i = 0; i < 128; i++)
//        {
//            if (hash1[i] != hash2[i])
//                return false;
//        }
//        return true;
//    }
//};

class Solution {
public:
    bool canPermutePalindrome(string s) {
        int hash[256] = { 0 };
        for (auto ch : s)
        {
            hash[ch]++;
        }
        int count = 0;//统计出现基数次的个数
        for (int i = 0; i < 256; i++)
        {
            if (hash[i] % 2 == 1)
                count++;
        }
        return count == 1 || count == 0;
    }
};