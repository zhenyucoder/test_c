#include <iostream>
#include <map>
using namespace std;


//禁止拷贝的类
//class CopyBan
//{
//	// .....
//private:// C++98设置为私有，之声明不是实现
//	CopyBan(const CopyBan& cp1);
//	CopyBan& operator=(const CopyBan& cp1);
//};
//
////C++11新增关键字delete
//class CopyBan
//{
//	// .....
//	CopyBan(const CopyBan& cp1) = delete;
//	CopyBan& operator=(const CopyBan& cp1) = delete;
//};



//只能在堆上创建对象
//class HwapOnly//方案1：将析构函数私有化
//{
//public:
//	/*static void Destory(HwapOnly* ptr)
//	{
//		delete ptr;
//	}*/
//
//	void Destroy()
//	{
//		delete this;
//	}
//private:
//	~HwapOnly()
//	{
//		cout << "~HwapOnly()" << endl;
//	}
//};
//int main()
//{
//	HwapOnly* hp = new HwapOnly;
//	//HwapOnly ho(*hp);//析构函数私有，不允许在栈上创建对象
//	return 0;
//}

//class HwapOnly//方案2：将构造函数私有化
//{
//public:
//	static HwapOnly* createobj()
//	{
//		return new HwapOnly;
//	}
//	
//	HwapOnly(const HwapOnly&) = delete;
//private:
//	HwapOnly()
//	{
//		cout << "HwapOnly()" << endl;
//	}
//};
//int main()
//{	
//	HwapOnly* hp = HwapOnly::createobj();
//	//HwapOnly ho(*hp);
//	return 0;
//}



//只能在栈上创建对象
//class StackOnly
//{
//public:
//	static StackOnly CreateObj()
//	{
//		StackOnly st;
//		return st;
//	}
//
//	void* operator new(size_t size) = delete;
//private:
//	StackOnly()
//	{
//		cout << "StackOnly()" << endl;
//	}
//};
//
//int main()
//{
//	StackOnly s = StackOnly::CreateObj();
//	return 0;
//}



//class A
//{
//public:
//	virtual void f() {}
//	int _a = 1;
//};
//
//class B : public A
//{
//public:
//	int _b = 2;
//};
//
//
//void func(A* pa)
//{
//	//B* ptr = (B*)pa;//不安全
//	B* ptr = dynamic_cast<B*>(pa);
//	if (ptr)
//	{
//		ptr->_a++;
//		ptr->_b++;
//		cout << ptr->_a << ":" << ptr->_b << endl;
//	}
//	else
//	{
//		cout << "转换失败" << endl;
//	}
//}
//int main()
//{
//	A a;
//	B b;
//	cout << "B b -> func(&a):" << endl;
//	func(&a);//转换失败
//	cout << endl;
//
//	cout << "A a -> func(&b):" << endl;
//	func(&b);
//	return 0;
//	return 0;
//}



class A
{
public:
	static A* GetInstance()
	{
		return &_inst;
	}

	void add(const string& s1, const string& s2)
	{
		_dict[s1] = s2;
	}

	void print()
	{
		for (auto& kv : _dict)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}
	A(const A&) = delete;
	A& operator=(const A&) = delete;
private:
	A()
	{
		cout << "A()" << endl;
	}

	map<string, string> _dict;
	int _n;
	static A _inst;
};


//A A::_inst;
//
//int main()
//{
//	A::GetInstance()->add("sort", "排序");
//	A::GetInstance()->add("left", "左边");
//	A::GetInstance()->add("right", "右边");
//	A::GetInstance()->print();
//
//	return 0;
//}


class B
{
public:
	static B* GetInstance()
	{
		if (_inst == nullptr)
		{
			_inst = new B;
		}
		return _inst;
	}

	void add(const string& s1, const string& s2)
	{
		_dict[s1] = s2;
	}

	void print()
	{
		for (auto& kv : _dict)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}
	B(const B&) = delete;
	B& operator=(const B&) = delete;
private:
	B()
	{
		cout << "A()" << endl;
	}

	map<string, string> _dict;
	int _n;
	static B* _inst;
};

B* B::_inst = nullptr;

