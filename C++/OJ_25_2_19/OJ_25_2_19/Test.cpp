//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newhead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newhead;
//    }
//};




//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newhead = new ListNode(), * cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = newhead->next; // ͷ��
//            newhead->next = cur;
//            cur = next;
//        }
//        cur = newhead->next;
//        delete newhead;
//        return cur;
//    }
//};





class Solution {
public:
    vector<vector<int>> ret;
    vector<int> path;
    int target;
    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        if (root == nullptr) return ret;
        target = targetSum;
        dfs(root, 0);
        return ret;
    }
    void dfs(TreeNode* root, int prevSum)
    {
        path.push_back(root->val);
        prevSum += root->val;
        if (root->left == nullptr && root->right == nullptr)
        {
            if (prevSum == target)
                ret.push_back(path);
            path.pop_back();
            return;
        }
        if (root->left) dfs(root->left, prevSum);
        if (root->right) dfs(root->right, prevSum);
        path.pop_back();
    }
};