//class StringFormat
//{
//public:
//	string formatString(string A, int n, vector<char> arg, int m)
//	{
//		int j = 0;
//		string ret;
//		for (int i = 0; i < n; i++)
//		{
//			if (A[i] == '%')
//			{
//				if (i + 1 < n && A[i + 1] == 's')
//				{
//					ret += arg[j++];
//					i++;
//				}
//				else
//				{
//					ret += A[i];
//				}
//			}
//			else
//			{
//				ret += A[i];
//			}
//		}
//
//		while (j < m)
//		{
//			ret += arg[j++];
//		}
//		return ret;
//	}
//};



//#include <iostream>
//#include <cmath>
//#include <vector>
//using namespace std;
//int a, b;
//bool isprim(int n)
//{
//	if (n < 2) return false;
//	for (int i = 2; i <= sqrt(n); i++)
//	{
//		if (n % i == 0) return false;
//	}
//	return true;
//}
//int check(int n) 
//{
//	vector<int> num;
//	while (n)
//	{
//		num.push_back(n % 10);
//		n /= 10;
//	}
//	for (int i = 0; i < num.size(); i++)
//	{
//		for (int j = 0; j < num.size(); j++) 
//		{
//			if (i != j && num[i] != 0)
//			{
//				if (isprim(num[i] * 10 + num[j]))
//				{
//					return 1;
//				}
//			}
//		}
//	}
//	return 0;
//}
//int main()
//{
//	cin >> a >> b;
//	int ret = 0;
//	for (int i = max(a, 10); i <= b; i++)
//	{
//		ret += check(i);
//	}
//	cout << ret << endl;
//	return 0;
//}



//#include <iostream>
//#include <string>
//using namespace std;
//string s;
//int x;
//int main()
//{
//	cin >> s >> x;
//	int begin = -1;
//	int maxCount = 0;
//	int left = 0, right = 0, n = s.size();
//	while (right < n)
//	{
//		if (s[right] == 'C' || s[right] == 'G') count++;
//		while (right - left + 1 > x)
//		{
//			if (s[left] == 'C' || s[left] == 'G') count--;
//			left++;
//		}
//		if (right - left + 1 == x)
//		{
//			if (count > maxCount)
//			{
//				begin = left;
//				maxCount = count;
//			}
//		}
//		right++;
//	}
//	cout << s.substr(begin, x) << endl;
//	return 0;
//}



//#include <iostream>
//#include <string>
//using namespace std;
//string s;
//int main()
//{
//	cin >> s;
//	for (int i = 0; i < s.size(); i++)
//	{
//		if (s[i] % 2 == 0) s[i] = '0';
//		else s[i] = '1';
//	}
//	cout << stoi(s) << endl; 
//}


//#include <iostream>
//using namespace std;
//const int N = 1e6 + 10;
//typedef long long LL;
//LL n, m;
//LL row[N], col[N];
//int main()
//{
//	scanf("%ld %ld", &n, &m);
//	LL arr[n][m];
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			scanf("%ld", &arr[i][j]);
//			row[i] += arr[i][j];
//			col[j] += arr[i][j];
//		}
//	}
//
//	for (int i = 0; i < n; i++)
//	{
//		for (int j = 0; j < m; j++)
//		{
//			printf("%ld ", row[i] + col[j] - arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}

#include <iostream>
using namespace std;
typedef long long LL;
const int N = 1e5 + 10;
LL n, k;
LL h[N], s[N];
int main()
{
	cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> h[i];
	for (int i = 1; i <= n; i++) cin >> s[i];

	LL left = 0, right = 0;
	LL hSum = 0, sSum = 0, hMax = 0, sMin = 0, begin = 0;

	while (right <= n)
	{
		hSum += h[right];
		sSum += s[right];
		while (right - left + 1 > k)
		{
			hSum -= h[left];
			sSum -= s[left];
			left++;
		}
		if (right - left + 1 == k)
		{
			if (hSum > hMax)
			{
				begin = left;
				hMax = hSum;
				sMin = sSum;
			}
			else if (hSum == hMax && sSum < sMin)
			{
				begin = left;
				hMax = hSum;
				sMin = sSum;
			}
		}
		right++;
	}

	cout << begin << endl;

	return 0;
}