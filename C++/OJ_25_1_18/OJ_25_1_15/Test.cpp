#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <unordered_map>

using namespace std;

// 对于sort， 排升序<, 排降序>
// 对于堆而言， 建大堆<， 建小堆>

//class Comp
//{
//public:
//	bool operator()(const int a, const int b)
//	{
//		return a < b;
//	}
//};
//
//int main()
//{
//	vector<int> v = { 45, 232 ,121 ,1212 ,45, 2121, 1 };
//	//sort(v.begin(), v.end(), [](const int a, const int b) {
//	//	return a > b;
//	//});
//
//	priority_queue<int, vector<int>, Comp> heap(v.begin(), v.end());
//
//	while (!heap.empty())
//	{
//		std::cout << heap.top() << " ";
//		heap.pop();
//	}
//	return 0;
//}




//class Solution {
//public:
//    typedef pair<int, int> PII;
//    class Comp
//    {
//    public:
//        bool operator()(const PII& kv1, const PII& kv2)
//        {
//            return kv1.second > kv2.second;
//        }
//    };
//    vector<int> topKFrequent(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        for (auto num : nums)
//            hash[num]++;
//        priority_queue<PII, vector<PII>, Comp> heap;
//        for (auto& kv : hash)
//        {
//            heap.push(kv);
//            if (heap.size() > k)
//                heap.pop();
//        }
//        vector<int> ret;
//        while (!heap.empty())
//        {
//            auto& [a, b] = heap.top();
//            ret.push_back(a);
//            heap.pop();
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    using PII = pair<int, int>;
//    vector<int> topKFrequent(vector<int>& nums, int k) {
//        if (nums.size() <= k) return nums;
//        unordered_map<int, int> hash;
//        for (auto num : nums)
//            hash[num]++;
//        vector<PII> values;
//        for (auto& kv : hash)
//            values.emplace_back(kv);
//        srand(time(nullptr));
//        Qsort(values, 0, values.size() - 1, k);
//        vector<int> ret;
//        for (int i = 0; i < k; i++)
//        {
//            ret.push_back(values[i].first);
//        }
//        return ret;
//    }
//
//    void Qsort(vector<PII>& values, int l, int r, int k)
//    {
//        if (l >= r) return;
//        int key = GetRandom(values, l, r);
//        int left = l - 1, right = r + 1, cur = l;
//        // [l, left] [left + 1, right - 1] [right, r]
//        while (cur < right)
//        {
//            if (values[cur].second < key)
//                swap(values[cur], values[--right]);
//            else if (values[cur].second > key)
//                swap(values[cur++], values[++left]);
//            else
//                cur++;
//        }
//        int a = left - l + 1, b = right - left - 1;
//        if (a > k)
//            Qsort(values, l, left, k);
//        else if (a + b < k)
//            Qsort(values, right, r, k - a - b);
//        return;
//    }
//
//    int GetRandom(vector<PII>& values, int l, int r)
//    {
//        int pos = l + rand() % (r - l + 1);
//        return values[pos].second;
//    }
//};




//class Solution {
//public:
//    vector<int> inorderTNode;
//    void Inorder(TreeNode* root)
//    {
//        if (root->left)
//            Inorder(root->left);
//        inorderTNode.push_back(root->val);
//        if (root->right)
//            Inorder(root->right);
//    }
//
//    TreeNode* buildAVL(int left, int right)
//    {
//        if (left > right) return nullptr;
//        int mid = (left + right) >> 1;
//        TreeNode* root = new TreeNode(inorderTNode[mid]);
//        if (mid - 1 >= left)
//            root->left = buildAVL(left, mid - 1);
//        if (mid + 1 <= right)
//            root->right = buildAVL(mid + 1, right);
//        return root;
//
//    }
//    TreeNode* balanceBST(TreeNode* root) {
//        Inorder(root);
//        return buildAVL(0, inorderTNode.size() - 1);
//    }
//};




//class Solution {
//public:
//    void wiggleSort(vector<int>& nums) {
//        for (int i = 1; i < nums.size(); i++)
//        {
//            if ((i % 2) && (nums[i] < nums[i - 1]))
//                swap(nums[i], nums[i - 1]);
//            else if ((i % 2 == 0) && (nums[i] > nums[i - 1]))
//                swap(nums[i], nums[i - 1]);
//        }
//    }
//};




//class Solution {
//public:
//    int PrevSum = 0, target = 0;
//    vector<int> path;
//    vector<vector<int>> ret;
//    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
//        if (root == nullptr) return ret;
//        target = targetSum;
//        dfs(root);
//        return ret;
//    }
//
//    void dfs(TreeNode* root)
//    {
//        PrevSum += root->val;
//        path.push_back(root->val);
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            if (PrevSum == target)
//                ret.push_back(path);
//            PrevSum -= root->val;
//            path.pop_back();
//            return;
//        }
//        if (root->left)
//            dfs(root->left);
//        if (root->right)
//            dfs(root->right);
//        PrevSum -= root->val;
//        path.pop_back();
//        return;
//    }
//};





//class Solution {
//public:
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        return dfs(nums, 0, nums.size() - 1);
//    }
//
//    TreeNode* dfs(vector<int>& nums, int left, int right)
//    {
//        if (left == right)
//            return new TreeNode(nums[left]);
//        int mid = (right + left) >> 1;
//        TreeNode* root = new TreeNode(nums[mid]);
//        if (mid - 1 >= left)
//            root->left = dfs(nums, left, mid - 1);
//        if (mid + 1 <= right)
//            root->right = dfs(nums, mid + 1, right);
//        return root;
//    }
//};




//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        unordered_map<int, int> hash;
//        hash[0] = 1;
//        int ret = 0, sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//            if (hash.count(sum - k) != 0)
//                ret += hash[sum - k];
//            hash[sum]++;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    bool check[20] = { 0 };
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, pos + 1);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};



//
//class Solution {
//public:
//    bool Row[10], Col[10], Dig1[20], Dig2[20];
//    int ret = 0;
//    int totalNQueens(int n) {
//        dfs(0, n);
//        return ret;
//    }
//
//    void dfs(int i, int n)
//    {
//        if (n == i)
//        {
//            ret++;
//            return;
//        }
//
//        for (int j = 0; j < n; j++)
//        {
//            if (!Row[i] && !Col[j] && !Dig1[i + j] && !Dig2[i - j + n])
//            {
//                Row[i] = Col[j] = Dig1[i + j] = Dig2[i - j + n] = true;
//                dfs(i + 1, n);
//                Row[i] = Col[j] = Dig1[i + j] = Dig2[i - j + n] = false;
//            }
//        }
//        return;
//    }
//};



//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            int total = 0;
//            for (auto x : nums)
//                total += ((x >> i) & 1);
//            if (total % 3)
//                ret |= (1 << i);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<Node*> q;
//        q.push(root);
//        while (!q.empty())
//        {
//            int levelsize = q.size();
//            vector<int> cur;
//            for (int i = 0; i < levelsize; i++)
//            {
//                Node* top = q.front();
//                q.pop();
//                cur.push_back(top->val);
//                for (auto& node : top->children)
//                    q.push(node);
//            }
//            ret.push_back(cur);
//        }
//        return ret;
//    }
//};



class Solution {
public:
    char dismantlingAction(string arr) {
        unordered_map<char, int> hash;
        for (auto ch : arr)
            hash[ch]++;
        for (auto ch : arr)
            if (hash[ch] == 1) return ch;
        return ' ';
    }
};