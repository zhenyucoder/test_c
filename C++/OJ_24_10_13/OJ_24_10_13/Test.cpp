//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int m = s1.size(), n = s2.size();
//        if (m + n != s3.size()) return false;
//
//        s1 = ' ' + s1, s2 = ' ' + s2, s3 = ' ' + s3;
//        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1));
//        // ��ʼ��
//        dp[0][0] = true; 
//        for (int j = 1; j <= n; j++)
//            if (s2[j] == s3[j]) dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= m; i++)
//            if (s1[i] == s3[i]) dp[i][0] = true;
//            else break;
//     
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = (s1[i] == s3[i + j] && dp[i - 1][j]) || (s2[j] == s3[i + j] && dp[i][j - 1]);
//        return dp[m][n];
//    }
//};


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n + 1), w(n + 1);
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//
//    // ques1
//    vector<vector<int>> dp(n + 1, vector<int>(V + 1));
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= V; j++)
//        {
//            dp[i][j] = dp[i - 1][j];
//            if (j >= v[i])
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[n][V] << endl;
//
//    // ques2
//    vector<vector<int>> dp1(n + 1, vector<int>(V + 1, -0x3f3f3f3f));
//    for (int i = 0; i <= n; i++) dp1[i][0] = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= V; j++)
//        {
//            dp1[i][j] = dp1[i - 1][j];
//            if (v[i] <= j)
//            {
//                dp1[i][j] = max(dp1[i][j], dp1[i - 1][j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << ((dp1[n][V] < 0) ? 0 : dp1[n][V]) << endl;
//    return 0;
//}




//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, V;
//    cin >> n >> V;
//    vector<int> v(n + 1), w(n + 1);
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//
//    // ques1
//    vector<int> dp(V + 1);
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = V; j >= 1; j--)
//        {
//            if (j >= v[i])
//                dp[j] = max(dp[j], dp[j - v[i]] + w[i]);
//        }
//    }
//    cout << dp[V] << endl;
//
//    // ques2
//    vector<int> dp1(V + 1, -0x3f3f3f3f);
//    dp1[0] = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = V; j >= 1; j--)
//        {
//            if (v[i] <= j)
//            {
//                dp1[j] = max(dp1[j], dp1[j - v[i]] + w[i]);
//            }
//        }
//    }
//    cout << ((dp1[V] < 0) ? 0 : dp1[V]) << endl;
//    return 0;
//}




//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int target = 0, n = nums.size();
//        for (int x : nums)
//            target += x;
//        if (target % 2) return false;
//        target /= 2;
//
//        vector<bool> dp(target + 1);
//        dp[0] = true;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = target; j >= 1; j--)
//            {
//                if (j >= nums[i - 1])
//                    dp[j] = dp[j] || dp[j - nums[i - 1]];
//            }
//        }
//        return dp[target];
//    }
//};





//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int aim = 0, n = nums.size();
//        for (auto x : nums) aim += x;
//        aim += target;
//        if (aim < 0 || aim % 2) return 0;
//        aim /= 2;
//
//        vector<int> dp(aim + 1, 0);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = aim; j >= 0; j--)
//            {
//                if (j >= nums[i - 1])
//                {
//                    dp[j] += dp[j - nums[i - 1]];
//                }
//            }
//        }
//        return dp[aim];
//    }
//};




class Solution {
public:
    int lastStoneWeightII(vector<int>& stones) {
        int sum = 0;
        for (auto x : stones)
            sum += x;
        int aim = sum / 2;
        int n = stones.size();
        vector<int> dp(aim + 1);
        for (int i = 1; i <= n; i++)
        {
            for (int j = aim; j >= stones[i - 1]; j--)
            {
                dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
            }
        }
        return sum - 2 * dp[aim];
    }
};