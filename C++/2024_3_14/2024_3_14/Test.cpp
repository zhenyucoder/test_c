//class Solution
//{
//public:
//	bool evaluateTree(TreeNode* root)
//	{
//		if (root->left == nullptr) return root->val == 0 ? false : true;
//		bool left = evaluateTree(root->left);
//		bool right = evaluateTree(root->right);
//		return root->val == 2 ? left | right : left & right;
//	}
//};


//class Solution
//{
//public:
//	int sumNumbers(TreeNode* root)
//	{
//		return dfs(root, 0);
//	}
//	int dfs(TreeNode* root, int presum)
//	{
//		presum = presum * 10 + root->val;
//		if (root->left == nullptr && root->right == nullptr)
//			return presum;
//		int ret = 0;
//		if (root->left) ret += dfs(root->left, presum);
//		if (root->right) ret += dfs(root->right, presum);
//			return ret;
//	}
//};



//class Solution
//{
//public:
//	TreeNode* pruneTree(TreeNode* root)
//	{
//		if (root == nullptr) return nullptr;
//		root->left = pruneTree(root->left);
//		root->right = pruneTree(root->right);
//		if (root->left == nullptr && root->right == nullptr && root->val == 0)
//		{
//			delete root;
//			root = nullptr;
//		}
//		return root;
//	}
//};




//class Solution
//{
//	long prev = LONG_MIN;
//public:
//	 bool isValidBST(TreeNode* root)
//	 {
//		if (root == nullptr)
//			return true;
//		bool left = isValidBST(root->left);
//		 // ��֦
//		 if (left == false) 
//			 return false;
//		 bool cur = false;
//		if (root->val > prev)
//			 cur = true;
//		// ��֦
//		if (cur == false) 
//			return false;
//		 prev = root->val;
//		 bool right = isValidBST(root->right);
//		 return left && right && cur;
//	 }
//};



class Solution
{
	int count;
	int ret;
public:
	int kthSmallest(TreeNode* root, int k)
	{
		count = k;
		dfs(root);
		return ret;
	}
	void dfs(TreeNode* root)
	{
		if (root == nullptr || count == 0) return;
		dfs(root->left);
		count--;
		if (count == 0) ret = root->val;
		dfs(root->right);
	}
};