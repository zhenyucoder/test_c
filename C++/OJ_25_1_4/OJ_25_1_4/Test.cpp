//class Solution {
//public:
//    bool isAnagram(string s, string t)
//    {
//        if (s.length() != t.length())
//        {
//            return false;
//        }
//        vector<int> table(26, 0);
//        for (auto& ch : s)
//        {
//            table[ch - 'a']++;
//        }
//        for (auto& ch : t)
//        {
//            table[ch - 'a']--;
//            if (table[ch - 'a'] < 0)
//            {
//                return false;
//            }
//        }
//        return true;
//    }
//};

class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> hash;
        for (auto& str : strs)
        {
            string tmp = str;
            sort(tmp.begin(), tmp.end());
            hash[tmp].push_back(str);
        }

        vector<vector<string>> ret;
        for (auto& [x, y] : hash)
            ret.push_back(y);
        return ret;
    }
};