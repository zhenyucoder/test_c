//class Solution
//{
//public:
//	int maxProfit(vector<int>& p)
//	{
//		int ret = 0, n = p.size();
//		for (int i = 0; i < n; i++)
//		{
//			int j = i;
//			while (j + 1 < n && p[j + 1] > p[j]) j++;
//			ret += p[j] - p[i];
//			i = j;
//		}
//		return ret;
//	}
//};




//class Solution
//{
//public:
//	int maxProfit(vector<int>& prices)
//	{
//		int ret = 0;
//		for (int i = 1; i < prices.size(); i++)
//		{
//			if (prices[i] > prices[i - 1])
//				ret += prices[i] - prices[i - 1];
//		}
//		return ret;
//	}
//};




//class Solution
//{
//public:
//	int longestPalindrome(string s)
//	{
//		int hash[127] = { 0 };
//		for (char ch : s) hash[ch]++;
//		int ret = 0;
//		for (int x : hash)
//		{
//			ret += x / 2 * 2;
//		}
//		return ret < s.size() ? ret + 1 : ret;
//	}
//};




class Solution
{
public:
	bool canJump(vector<int>& nums)
	{
		int left = 0, right = 0, maxPos = 0, n = nums.size();
		while (left <= right)
		{
			if (maxPos >= n - 1)
			{
				return true;
			}
			for (int i = left; i <= right; i++)
			{
				maxPos = max(maxPos, nums[i] + i);
			}
			left = right + 1;
			right = maxPos;
		}
		return false;
	}
};