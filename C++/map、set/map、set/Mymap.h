#pragma once
#include "RBTree.h"


namespace Imitate
{
	template <class K, class V>
	class map
	{
	public:
		struct setkeyofT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, mapkeyofT>::iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, mapkeyofT>::const_iterator const_iterator;
		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		pair<iterator, bool> Insert(V& kv)
		{
			return _t.Insert(kv);
		}

		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert(make_pair(key, V()));
			return ret.first->second;
		}
	private:
		RBTree<K, pair<K, V>, mapkeyofT> _t;
	};
}
