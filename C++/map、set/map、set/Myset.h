#pragma once
#include "RBTree.h"

namespace Imitate
{
	template<class K>
	class set
	{
	public:
		struct SetkeyofT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename RBTree<K, K, SetkeyofT>::const_iterator iterator;
		typedef typename RBTree<K, K, SetkeyofT>::const_iterator const_iterator;
		iterator begin() const
		{
			return _t.begin();
		}

		iterator end() const
		{
			return _t.end();
		}

		pair<iterator, bool> Insert(K& key)
		{
			return _t.Insert(key);
		}
	private:
		RBTree<K, K, SetkeyofT> _t;
	};
}