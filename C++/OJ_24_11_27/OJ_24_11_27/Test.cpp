//class Solution {
//public:
//    vector<int> sortArray(vector<int>& nums) {
//        merge(nums, 0, nums.size() - 1);
//        return nums;
//    }
//
//    void merge(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//        int mid = left + (right - left + 1) / 2;
//
//        merge(nums, left, mid - 1);
//        merge(nums, mid, right);
//
//        vector<int> ret(right - left + 1);
//        int cur1 = left, cur2 = mid, i = 0;
//        while (cur1 < mid && cur2 <= right)
//            ret[i++] = (nums[cur1] < nums[cur2]) ? nums[cur1++] : nums[cur2++];
//        while (cur1 < mid)
//            ret[i++] = nums[cur1++];
//        while (cur2 <= right)
//            ret[i++] = nums[cur2++];
//
//        for (int i = left; i <= right; i++)
//            nums[i] = ret[i - left];
//    }
//};




//class Solution {
//public:
//    int tmp[50001];
//    int reversePairs(vector<int>& record) {
//        return mergesort(record, 0, record.size() - 1);
//    }
//
//    int mergesort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//        int ret = 0;
//        int mid = (left + right) >> 1;
//        //分别统计[left, mid][mid+1, right]各自区间的逆序对
//        ret += mergesort(record, left, mid);
//        ret += mergesort(record, mid + 1, right);
//        //统计[left, mid][mid+1, right]区间各取一数构成的逆序对
//        int cur1 = left, cur2 = mid + 1, i = left;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] > record[cur2])
//            {
//                ret += right - cur2 + 1;
//                tmp[i++] = record[cur1++];
//            }
//            else
//            {
//                tmp[i++] = record[cur2++];
//            }
//        }
//
//        //将[left, mid][mid+1, right]中剩余元素排序
//        while (cur1 <= mid)
//            tmp[i++] = record[cur1++];
//        while (cur2 <= right)
//            tmp[i++] = record[cur2++];
//        //将tmp中排好的数据拷回record
//        for (i = left; i <= right; i++)
//            record[i] = tmp[i];
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int ret = INT_MAX, sum = 0;
//        for (int left = 0, right = 0, n = nums.size(); right < n; right++)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                ret = min(ret, right - left + 1);
//                sum -= nums[left++];
//            }
//        }
//        return ret == INT_MAX ? 0 : ret;
//    }
//};




//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int ret = 0;
//        for (int left = 0, right = 0, zero = 0; right < nums.size(); right++)
//        {
//            if (nums[right] == 0)
//                zero++;
//            while (zero > k)
//            {
//                if (nums[left++] == 0)
//                    zero--;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//
//        int target = sum - x, ans = 0; // 求和为target的最大连续子数组
//        if (target == 0) return nums.size();
//        if (target < 0) return -1;
//        for (int left = 0, right = 0, sum = 0; right < nums.size(); right++)
//        {
//            sum += nums[right];
//            while (sum > target)
//                sum -= nums[left++];
//            if (sum == target)
//                ans = max(ans, right - left + 1);
//        }
//        return ans == 0 ? -1 : nums.size() - ans;
//    }
//};



//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        int ret = 0;
//        unordered_map<int, int> hash;
//        for (int left = 0, right = 0; right < fruits.size(); right++)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                if (--hash[fruits[left]] == 0)
//                    hash.erase(fruits[left]);
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p) {
//        unordered_map<int, int> hash1, hash2;
//        for (auto ch : p)
//            hash2[ch]++;
//
//        vector<int> ret;
//        for (int left = 0, right = 0, num = 0; right < s.size(); right++)
//        {
//            char in = s[right];
//            if (++hash1[in] <= hash2[in]) num++;
//            if (right - left + 1 > p.size())
//            {
//                char out = s[left++];
//                if (--hash1[out] < hash2[out])
//                    num--;
//            }
//            if (right - left + 1 == p.size() && num == p.size())
//                ret.push_back(left);
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        vector<int> ret;
//        unordered_map<string, int>hash2;
//        for (auto& str : words)
//            hash2[str]++;
//        int n = words.size(), len = words[0].size();
//        for (int i = 0; i < len; i++)
//        {
//            unordered_map<string, int> hash1;
//            for (int left = i, right = i, count = 0; right + len <= s.size(); right += len)
//            {
//                string in = s.substr(right, len);
//                hash1[in]++;
//                if (hash2[in] && hash1[in] <= hash2[in]) count++;
//                if (right - left + 1 > n * len)
//                {
//                    string out = s.substr(left, len);
//                    hash1[out]--;
//                    if (hash2[out] && hash1[out] < hash2[out])
//                        count--;
//                    left += len;
//                }
//                if (count == n)
//                    ret.push_back(left);
//            }
//        }
//        return ret;
//    }
//};



class Solution {
public:
    string minWindow(string s, string t) {
        unordered_map<char, int> hash2, hash1;
        for (auto ch : t)
            hash2[ch]++;
        int pos = 0, len = INT_MAX;
        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
        {
            char in = s[right];
            hash1[in]++;
            if (hash2[in] && hash1[in] <= hash2[in]) count++;
            while (count >= t.size())
            {
                if (right - left + 1 < len)
                    pos = left, len = right - left + 1;
                char out = s[left++];
                hash1[out]--;
                if (hash2[out] && hash1[out] < hash2[out])
                    count--;
            }
        }
        return len == INT_MAX ? "" : s.substr(pos, len);
    }
};