#include <iostream>
#include <vector>
using namespace std;

//int main()
//{
//	vector<int> v;
//	v.push_back(1);
//	v.push_back(2);
//	v.push_back(3);
//	v.push_back(4);
//	vector<int> v1;
//	v1.resize(4);
//	for (int i = 0; i < 4; i++)
//		v1[i] = i;
//	return 0;
//}


//class Solution {
//public:
//    vector<int>& _postorderTraversal(TreeNode*& root, vector<int>& v)
//    {
//        if (root == nullptr)
//            return v;
//        _postorderTraversal(root->left, v);
//        _postorderTraversal(root->right, v);
//        v.push_back(root->val);
//        return v;
//    }
//
//    vector<int> postorderTraversal(TreeNode* root) {
//        vector<int> v;
//        return _postorderTraversal(root, v);
//    }
//};


//class Solution {
//public:
//    vector<int> postorderTraversal(TreeNode* root) {
//        vector<int> v;
//        if (root == nullptr)
//            return v;
//
//        stack<TreeNode*> st;
//        TreeNode* cur = root, * prev = nullptr;
//        while (cur)//最左序列节点插入
//        {
//            st.push(cur);
//            if (cur->left)
//                cur = cur->left;
//            else
//                break;
//        }
//
//        while (!st.empty())
//        {
//            cur = st.top();
//            if (cur->right == prev || cur->right == nullptr)//右子树为空，或右子树已经遍历过
//            {
//                v.push_back(cur->val);
//                prev = cur;
//                st.pop();
//            }
//            else
//            {
//                cur = cur->right;
//                while (cur)
//                {
//                    st.push(cur);
//                    cur = cur->left;
//                }
//            }
//        }
//        return v;
//    }
//};


//class Solution {
//public:
//    vector<int>& _inorderTraversal(TreeNode*& root, vector<int>& v)
//    {
//        if (root == nullptr)
//            return v;
//        _inorderTraversal(root->left, v);
//        v.push_back(root->val);
//        _inorderTraversal(root->right, v);
//        return v;
//    }
//    vector<int> inorderTraversal(TreeNode* root) {
//        vector<int> v;
//        return _inorderTraversal(root, v);
//    }
//};



//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        vector<int> v;
//        if (root == nullptr)
//            return v;
//
//        TreeNode* cur = root;
//        stack<TreeNode*> st;
//        while (root)//将整个二叉树最左节点全部入栈
//        {
//            st.push(root);
//            if (root->left)
//                root = root->left;
//            else
//                break;
//        }
//
//        while (!st.empty())
//        {
//            cur = st.top(), st.pop();
//            v.push_back(cur->val);
//            if (cur->right)//节点右子树不为空，右子树说有最左节点入栈
//            {
//                cur = cur->right;
//                while (cur)
//                {
//                    st.push(cur);
//                    cur = cur->left;
//                }
//            }
//        }
//        return v;
//    }
//};


//class Solution {
//public:
//    vector<int>& _preorderTraversal(TreeNode*& root, vector<int>& v)
//    {
//        if (root == nullptr)
//            return v;
//        v.push_back(root->val);
//        _preorderTraversal(root->left, v);
//        _preorderTraversal(root->right, v);
//        return v;
//    }
//    vector<int> preorderTraversal(TreeNode* root) {
//        vector<int> v;
//        if (root == nullptr)
//            return v;
//        return _preorderTraversal(root, v);
//    }
//};


//class Solution {
//public:
//    vector<int> preorderTraversal(TreeNode* root) {
//        vector<int> v;
//        if (root == nullptr)
//            return v;
//
//        TreeNode* cur = root;
//        stack<TreeNode*> st;
//        while (cur)//将二叉树最左节点全部入栈
//        {
//            v.push_back(cur->val);
//            st.push(cur);
//            if (cur->left)
//                cur = cur->left;
//            else
//                break;
//        }
//
//        while (!st.empty())
//        {
//            cur = st.top();
//            st.pop();
//            if (cur->right)//将节点右子树最左节点全部入栈
//            {
//                cur = cur->right;
//                while (cur)
//                {
//                    v.push_back(cur->val);
//                    st.push(cur);
//                    if (cur->left)
//                        cur = cur->left;
//                    else
//                        break;
//                }
//            }
//        }
//        return v;
//    }
//};

//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& inorder, vector<int>& postorder, int& posi, int inbegin, int inend)
//    {
//        if (inbegin > inend)
//            return nullptr;
//
//        int rooti = inbegin;
//        while (rooti <= inend)
//        {
//            if (inorder[rooti] == postorder[posi])
//                break;
//            ++rooti;
//        }
//        //中序[inbegin, rooti-1] rooti [rooti+1, inend]
//
//        TreeNode* root = new TreeNode(postorder[posi--]);//后序反向遍历确定根节点
//        //中序分割确定左右子树
//        root->right = _buildTree(inorder, postorder, posi, rooti + 1, inend);
//        root->left = _buildTree(inorder, postorder, posi, inbegin, rooti - 1);
//        return root;
//    }
//
//    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
//        int i = postorder.size() - 1;
//        return _buildTree(inorder, postorder, i, 0, inorder.size() - 1);
//    }
//};


//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorder, int& prei, int inbegin, int inend)
//    {
//        if (inbegin > inend)
//            return nullptr;
//        //查找根节点在中序下标
//        int rooti = inbegin;
//        while (inbegin <= inend)
//        {
//            if (inorder[rooti] == preorder[prei])
//                break;
//            ++rooti;
//        }
//        //中序分割为:[inbegin, rooti-1] rooti [rooti+1, inend]
//        TreeNode* root = new TreeNode(preorder[prei++]);
//        root->left = _buildTree(preorder, inorder, prei, inbegin, rooti - 1);
//        root->right = _buildTree(preorder, inorder, prei, rooti + 1, inend);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        int i = 0;
//        return _buildTree(preorder, inorder, i, 0, inorder.size() - 1);
//    }
//};


class Solution {
public:
	void InorderConvert(TreeNode* cur, TreeNode*& prev)
	{
		if (cur == nullptr)
			return;
		InorderConvert(cur->left, prev);
		cur->left = prev;
		if (prev)
			prev->right = cur;
		prev = cur;
		InorderConvert(cur->right, prev);
	}

	TreeNode* Convert(TreeNode* pRootOfTree) {
		TreeNode* root = pRootOfTree, * prev = nullptr;
		InorderConvert(root, prev);
		TreeNode* head = root;
		while (head && head->left)
		{
			head = head->left;
		}
		return head;
	}
};