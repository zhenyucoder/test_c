//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        //初始化
//        dp[0][1] = 1;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int m = obstacleGrid.size(), n = obstacleGrid[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        dp[0][1] = 1;//初始化
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//            {
//                if (obstacleGrid[i - 1][j - 1] == 1)
//                    dp[i][j] = 0;
//                else
//                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        return dp[m][n];
//    }
//};

//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int m = frame.size(), n = frame[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + frame[i - 1][j - 1];
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int m = matrix.size(), n = matrix[0].size();
//        vector<vector<int>> dp(m + 1, vector<int>(n + 2, INT_MAX));
//        // dp[0].resize(n+2, 0);//将第一行数据处理为0
//        for (int i = 0; i < n + 2; i++)
//            dp[0][i] = 0;
//        for (int i = 1; i <= m; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(min(dp[i - 1][j - 1], dp[i - 1][j]), dp[i - 1][j + 1]) + matrix[i - 1][j - 1];
//        int ret_min = INT_MAX;
//        for (auto num : dp[m])
//            if (num < ret_min)
//                ret_min = num;
//        return ret_min;
//    }
//};


#include <iostream>
#include <vector>
using namespace std;

int main()
{
	int m = 1, n = 1;
	vector<vector<int>> dp(m + 1, vector<int>(n + 2, INT_MAX));
	dp[0].resize(n+2, 0);//将第一行数据处理为0
	//        for (int i = 0; i < n + 2; i++)
	//            dp[0][i] = 0;
	return 0;
}