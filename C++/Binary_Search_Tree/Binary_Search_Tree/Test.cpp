#include "Binary_Search_Tree.h"

int main()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	BSTree<int> st;
	for (auto e : a)
	{
		st.Insert(e);
	}
	st.Find(1);
	st.Inorder();

	st.Erase(14);
	st.Inorder();

	st.Erase(6);
	st.Inorder();

	st.Erase(8);
	st.Inorder();

	st.FindR(10);
	st.InsertR(100);
	st.EraseR(4);
	return 0;
}