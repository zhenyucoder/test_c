//class Solution {
//public:
//    bool evaluateTree(TreeNode* root) {
//        if (root->left == nullptr && root->right == nullptr)
//            return root->val;
//        bool left = evaluateTree(root->left);
//        bool right = evaluateTree(root->right);
//        return root->val == 2 ? left | right : left & right;
//    }
//};


//class Solution {
//public:
//    int sumNumbers(TreeNode* root) {
//        return dfs(root, 0);
//    }
//
//    int dfs(TreeNode* root, int presum)
//    {
//        presum = presum * 10 + root->val;
//        if (root->left == nullptr && root->right == nullptr)
//            return presum;
//        int sum = 0;
//        if (root->left)
//            sum += dfs(root->left, presum);
//        if (root->right)
//            sum += dfs(root->right, presum);
//        return sum;
//    }
//};




//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (root->left == nullptr && root->right == nullptr && root->val == 0)
//        {
//            delete root;
//            root = nullptr;
//        }
//        return root;
//    }
//};


//class Solution {
//public:
//    long long prev = LLONG_MIN;
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr)
//            return true;
//        bool left = isValidBST(root->left);
//        if (left == false) return false;
//        bool cur = false;
//        if (root->val > prev)
//            cur = true;
//        if (cur == false) return false;
//        prev = root->val;
//
//        bool right = isValidBST(root->right);
//        return left && cur && right;
//    }
//};

//class Solution {
//public:
//    int ret = 0, count = 0;
//    int kthSmallest(TreeNode* root, int k) {
//        count = k;
//        dfs(root);
//        return ret;
//    }
//
//    void dfs(TreeNode* root)
//    {
//        if (count == 0) return;
//        if (root->left)
//            dfs(root->left);
//        if (count-- == 1) ret = root->val;
//        if (count == 0) return;
//        if (root->right)
//            dfs(root->right);
//    }
//};


//class Solution {
//public:
//    vector<string> ret;
//    vector<string> binaryTreePaths(TreeNode* root) {
//        string path;
//        dfs(root, path);
//        return ret;
//    }
//    void dfs(TreeNode* root, string path)
//    {
//        path += to_string(root->val);
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            ret.push_back(path);
//        }
//
//        path += "->";
//        if (root->left)
//            dfs(root->left, path);
//        if (root->right)
//            dfs(root->right, path);
//    }
//};



//class Solution {
//public:
//    bool check[7];
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums);
//                //回复现场
//                check[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int i)
//    {
//        if (i == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        path.push_back(nums[i]);
//        dfs(nums, i + 1);
//        //恢复现场
//        path.pop_back();
//        dfs(nums, i + 1);
//    }
//};


class Solution {
public:
    vector<vector<int>> ret;
    vector<int> path;
    vector<vector<int>> subsets(vector<int>& nums) {
        ret.push_back(path);
        dfs(nums, 0);
        return ret;
    }

    void dfs(vector<int>& nums, int pos)
    {
        for (int i = pos; i < nums.size(); i++)
        {
            path.push_back(nums[i]);
            ret.push_back(path);
            dfs(nums, i + 1);
            //恢复现场
            path.pop_back();
        }
    }
};