//class Solution
//{
//	string hash[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs",
//   "tuv", "wxyz" };
//	string path;
//	vector<string> ret;
//public:
//	vector<string> letterCombinations(string digits)
//	{
//		if (digits.size() == 0) return ret;
//		dfs(digits, 0);
//		return ret;
//	}
//	void dfs(string& digits, int pos)
//	{
//		if (pos == digits.size())
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (auto ch : hash[digits[pos] - '0'])
//		{
//			path.push_back(ch);
//			dfs(digits, pos + 1);
//			path.pop_back(); // 恢复现场
//		}
//	}
//};


//class Solution
//{
//	int left, right, n;
//	string path;
//	vector<string> ret;
//public:
//	vector<string> generateParenthesis(int _n)
//	{
//		n = _n;
//		dfs();
//		return ret;
//	}
//	void dfs()
//	{
//		if (right == n)
//		{
//			ret.push_back(path);
//			return;
//		}
//		if (left < n) // 添加左括号
//		{
//			path.push_back('('); left++;
//			dfs();
//			path.pop_back(); left--; // 恢复现场
//		}
//		if (right < left) // 添加右括号
//		{
//			path.push_back(')'); right++;
//			dfs();
//			path.pop_back(); right--; // 恢复现场
//		}
//	}
//};


//class Solution
//{
//	vector<int> path;
//	vector<vector<int>> ret;
//	int n, k;
//public:
//	vector<vector<int>> combine(int _n, int _k)
//	{
//		n = _n; k = _k;
//		dfs(1);
//		return ret;
//	}
//	void dfs(int start)
//	{
//		if (path.size() == k)
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (int i = start; i <= n; i++)
//		{
//			path.push_back(i);
//				dfs(i + 1);
//			path.pop_back(); // 恢复现场
//		}
//	}
//};


//class Solution
//{
//	int ret, aim;
//public:
//	int findTargetSumWays(vector<int>& nums, int target)
//	{
//		aim = target;
//		dfs(nums, 0, 0);
//		return ret;
//	}
//	void dfs(vector<int>& nums, int pos, int path)
//	{
//		if (pos == nums.size())
//		{
//			if (path == aim) ret++;
//			return;
//		}
//		// 加法
//		dfs(nums, pos + 1, path + nums[pos]);
//		// 减法
//		dfs(nums, pos + 1, path - nums[pos]);
//	}
//};


class Solution
{
	int aim;
	vector<int> path;
	vector<vector<int>> ret;
public:
	vector<vector<int>> combinationSum(vector<int>& nums, int target)
	{
		aim = target;
		dfs(nums, 0, 0);
		return ret;
	}
	void dfs(vector<int>& nums, int pos, int sum)
	{
		if (sum == aim)
		{
			ret.push_back(path);
			return;
		}
		if (sum > aim || pos == nums.size()) return;
		// 枚举个数
		for (int k = 0; k * nums[pos] + sum <= aim; k++)
		{
			if (k) path.push_back(nums[pos]);
			dfs(nums, pos + 1, sum + k * nums[pos]);
		}
		// 恢复现场
		for (int k = 1; k * nums[pos] + sum <= aim; k++)
		{
			path.pop_back();
		}
	}
};