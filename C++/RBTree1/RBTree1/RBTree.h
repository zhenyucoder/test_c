#pragma once

enum Color
{
	RED,
	BLACK
};

template<class K, class V>
struct RBTreeNode
{
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	Color _col;

	//默认构造
	RBTreeNode(const pair<K, V> kv)
		:_left(nullptr)
		,_right(nullptr)
		, _parent(nullptr)
		,_col(RED)
		,_kv(kv)
	{}
};


template<class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;//根节点为黑色
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)//查找待查入位置
		{
			if (cur->_kv.first > kv.first)//左子树查找
			{
				parent = cur;
				cur = cur->_left;
			}
			else if(cur->_kv.first < kv.first)//右子树查找
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{//查找到相同元素，插入失败
				return false;
			}
		}

		//查找到待插入位置，准备插入数据
		//链接插入数据
		cur = new Node(kv);
		if (parent->_kv.first > kv.first)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		//将不是红黑树的二叉树进行调整
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent;
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)
				{//变色
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;
				}
				else 
				{
					if (cur == parent->_left)
					{
						RotateR(grandfather);
						grandfather->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						RotateL(parent);
						RotateR(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			else//grandfather->_right == parent
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)
				{//变色
					uncle->_col = parent->_col = BLACK;
					grandfather->_col = RED;

					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_right)
					{
						RotateL(grandfather);
						grandfather->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						RotateR(parent);
						RotateL(grandfather);
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			_root->_col= BLACK;
		}
		return true;
	}


	//中序遍历
	void Inorder()
	{
		_Inorder(_root);
		cout << endl;
	}

	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout << root->_kv.first <<" ";
		_Inorder(root->_right);
	}

	void RotateL(Node* parent)//左单旋
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* grandfather = parent->_parent;

		parent->_right = subRL;
		if(subRL)
			subRL->_parent = parent;

		parent->_parent = subR;
		subR->_left = parent;

		if (parent == _root)
		{
			_root = subR;
		}
		else
		{
			if (grandfather->_left == parent)
			{
				grandfather->_left = subR;
				subR->_parent = grandfather;
			}
			else
			{
				grandfather->_right = subR;
				subR->_parent = grandfather;
			}
		}
		return;
	}

	void RotateR(Node* parent)//右单旋
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* grandfather = parent->_parent;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
		}
		else
		{
			if (grandfather->_left == parent)
			{
				grandfather->_left = subL;
				subL->_parent = grandfather;
			}
			else
			{
				grandfather->_right = subL;
				subL->_parent = grandfather;
			}
		}
		return;
	}

	bool Check(Node* root, int blacknum, const int RefVal)
	{
		if (root == nullptr)
		{
			//cout << blacknum << endl;
			if (blacknum != RefVal)
				cout << "存在黑节点数量不相同的路径" << endl;
			return true;
		}

		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << "存在连续红节点的路径" << endl;
			return false;
		}

		if (root->_col == BLACK)
		{
			blacknum++;
		}

		return Check(root->_left, blacknum, RefVal) && Check(root->_right, blacknum, RefVal);
	}

	//判断是否为红黑树
	bool Isblance()
	{
		if (_root == nullptr)
			return true;
		if (_root->_col == RED)
			return false;

		int RefVal = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				RefVal++;
			cur = cur->_left;
		}
		return Check(_root,0, RefVal);
	}
	private:
		Node* _root = nullptr;
};