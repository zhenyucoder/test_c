#include <iostream>
#include <vector>
#include <utility>
using namespace std;
#include "RBTree.h"
#include <time.h>
//int main()
//{
//	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
//	RBTree<int, int> t;
//	for (auto e : a)
//	{
//		cout << e << endl;
//		t.Insert(make_pair(e, e));
//	}
//	
//	t.Inorder();
//	t.Isblance();
//	return 0;
//}


int main()
{
	const int N = 10000000;
	srand((size_t)time(0));
	vector<int> v;
	v.reserve(N);
	for (int i = 0; i < N; i++)
	{
		v.push_back(i + rand());
	}

	RBTree<int, int>  rb;
	for (auto x : v)
	{
		rb.Insert(make_pair(x, x));
	}
	cout << rb.Isblance() << endl;
	return 0;
}