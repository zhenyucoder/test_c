//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 1;
//int dp[N];
//
//int main()
//{
//    int n; cin >> n;
//    int ret = -0x3f3f3f3f, temp;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> temp;
//        dp[i] = max(temp, temp + dp[i - 1]);
//        ret = max(ret, dp[i]);
//    }
//    cout << ret << endl;
//    return 0;
//}




//#include <iostream>
//#include <string>
//using namespace std;
//
//string str;
//int n;
//
//
//int func()
//{
//    bool flag = true;
//    for (int i = 1; i < n; i++)
//    {
//        if (str[i] != str[i - 1])
//        {
//            flag = false;
//            break;
//        }
//    }
//
//    if (flag) return 1;
//
//    for (int left = 0, right = n - 1; left < right; left++, right--)
//    {
//        if (str[left] != str[right])
//        {
//            flag = true;
//            break;
//        }
//    }
//
//    if (flag) return n;
//    else return n - 1;
//}
//int main()
//{
//    cin >> str;
//    n = str.size();
//    cout << func() << endl;
//    return 0;
//}




//#include <iostream>
//#include <set>
//using namespace std;
//
//int n, m, temp;
//int main()
//{
//    set<int> s;
//    cin >> n >> m;
//    for (int base = n + m; base > 0; base--)
//    {
//        cin >> temp;
//        s.insert(temp);
//    }
//
//    for (auto x : s)
//        cout << x << " ";
//    return 0;
//}



//#include <iostream>
//#include <string>
//using namespace std;
//int dp[1010][1010];
//int main()
//{
//	string s;
//	cin >> s;
//	int n = s.size();
//	for (int i = n - 1; i >= 0; i--)
//	{
//		dp[i][i] = 1;
//		for (int j = i + 1; j < n; j++)
//		{
//			if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1] + 2;
//			else dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//		}
//	}
//	cout << dp[0][n - 1] << endl;
//	return 0;
//}




//#include <iostream>
//using namespace std;
//int n;
//int main()
//{
//	cin >> n;
//
//	int left = 1, right = n;
//
//	while (left <= right)
//	{
//		cout << left << " ";
//		left++;
//		if (left <= right)
//		{
//			cout << right << " ";
//			right--;
//		}
//	}
//	return 0;
//}


#include <iostream>
#include <string>
using namespace std;
string a, b;
int main()
{
	cin >> a >> b;
	int m = a.size(), n = b.size();
	int ret = m;
	for (int i = 0; i <= n - m; i++) 
	{
		int tmp = 0;
		for (int j = 0; j < m; j++)
		{
			if (a[j] != b[i + j])
			{
				tmp++;
			}
		}
		ret = min(tmp, ret);
	}
	cout << ret << endl;
	return 0;
}