//class Solution
//{
//public:
//	int minNumberOfFrogs(string croakOfFrogs)
//	{
//		string t = "croak";
//		int n = t.size();
//		vector<int> hash(n);
//		unordered_map<char, int> index; 
//		for (int i = 0; i < n; i++)
//			index[t[i]] = i;
//
//		for (auto ch : croakOfFrogs)
//		{
//			if (ch == 'c')
//			{
//				if (hash[n - 1] != 0) hash[n - 1]--;
//				hash[0]++;
//			}
//			else
//			{
//				int i = index[ch];
//				if (hash[i - 1] == 0) return -1;
//				hash[i - 1]--; hash[i]++;
//			}
//		}
//		for (int i = 0; i < n - 1; i++)
//			if (hash[i] != 0)
//				return -1;
//		return hash[n - 1];
//	}
//};



//class Solution
//{
//public:
//	vector<int> sortArray(vector<int>& nums)
//	{
//		srand(time(NULL)); 
//		qsort(nums, 0, nums.size() - 1);
//		return nums;
//	}
//	void qsort(vector<int>& nums, int l, int r)
//	{
//		if (l >= r) return;
//		int key = getRandom(nums, l, r);
//		int i = l, left = l - 1, right = r + 1;
//		while (i < right)
//		{
//			if (nums[i] < key) swap(nums[++left], nums[i++]);
//			else if (nums[i] == key) i++;
//			else swap(nums[--right], nums[i]);
//		}
//		qsort(nums, l, left);
//		qsort(nums, right, r);
//	}
//	int getRandom(vector<int>& nums, int left, int right)
//	{
//		int r = rand();
//		return nums[r % (right - left + 1) + left];
//	}
//};


class Solution
{
	vector<int> tmp;
public:
	vector<int> sortArray(vector<int>& nums)
	{
		tmp.resize(nums.size());
		mergeSort(nums, 0, nums.size() - 1);
		return nums;
	}
	void mergeSort(vector<int>& nums, int left, int right)
	{
		if (left >= right) return;
		int mid = (left + right) >> 1;
		mergeSort(nums, left, mid);
		mergeSort(nums, mid + 1, right);
		int cur1 = left, cur2 = mid + 1, i = 0;
		while (cur1 <= mid && cur2 <= right)
			tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur1++] : nums[cur2++]
			while (cur1 <= mid) tmp[i++] = nums[cur1++];
		while (cur2 <= right) tmp[i++] = nums[cur2++];
		// 4. ��ԭ
		for (int i = left; i <= right; i++)
			nums[i] = tmp[i - left];
	}
};