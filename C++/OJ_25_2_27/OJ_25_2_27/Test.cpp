//class Solution {
//public:
//    vector<string> findRelativeRanks(vector<int>& score) {
//        int sz = score.size();
//        vector<int> hash(sz);
//        for (int i = 0; i < sz; i++)
//            hash[i] = i;
//        sort(hash.begin(), hash.end(), [&score](const int& x, const int& y) {
//            return score[x] > score[y];
//            });
//        vector<string> ret(sz);
//        for (int i = 0; i < sz; i++)
//        {
//            if (i == 0) ret[hash[i]] = "Gold Medal";
//            else if (i == 1) ret[hash[i]] = "Silver Medal";
//            else if (i == 2) ret[hash[i]] = "Bronze Medal";
//            else ret[hash[i]] = to_string(i + 1);
//        }
//        return ret;
//    }
//};



class Solution {
public:
    string removeKdigits(string num, int k) {
        vector<char> st;
        for (auto ch : num)
        {
            while (!st.empty() && ch < st.back() && k)
            {
                st.pop_back();
                --k;
            }
            st.push_back(ch);
        }
        while (k--)
            st.pop_back();
        int pos = 0;
        while (pos < st.size() && st[pos] == '0') pos++; // ��ȥǰ��0
        string ret(st.begin() + pos, st.end());
        return ret.empty() ? "0" : ret;
    }
};