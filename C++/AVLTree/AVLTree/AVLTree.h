#pragma once
#include <assert.h>
#include <stdlib.h>

template<class T>
struct AVLTreeNode
{
	AVLTreeNode<T>* _left;
	AVLTreeNode<T>* _right;
	AVLTreeNode<T>* _parent;//父节点，用于维护平衡因子
	T _data;//存储数据类型
	int _bf;//平衡因子，维护平衡树的手段之一

	AVLTreeNode(const T& data = T())//构造函数
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _bf(0)
	{}
};

template<class T>
class AVLTree
{
	typedef AVLTreeNode<T> Node;
public:

	AVLTree()//构造函数
		: _root(nullptr)
	{}

	bool Insert(const T& data)
	{
		if (_root == nullptr)//插入前为空树
		{
			_root = new Node(data);
			return true;
		}

		//插入前树非空，查找插入位置
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_data > data)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_data < data)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;//树中存在与待插入元素相等的值
			}
		}

		//链接数据
		cur = new Node(data);
		if (parent->_data > data)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_right = cur;
			cur->_parent = parent;
		}


		//更新平衡因子，同时通过旋转将不符合要求的子树通过旋转调整为AVL树
		while (parent)
		{
			//更新平很因子
			if (parent->_left == cur)
				parent->_bf--;
			else
				parent->_bf++;

			//判断插入节点后是否符合AVL树树，同时调整其他平衡因子
			if (parent->_bf == 0)//插入成功
				return true;
			else if (parent->_bf == 1 || parent->_bf == -1)//插入节点树高度变化，需要向上调整平衡因子
			{
				cur = parent;
				parent = cur->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)//插入节点后不符合AVL树要求，需要通过旋转来调整
			{
				if (parent->_bf == 2 && cur->_bf == 1)//左单旋
					RotateL(parent);
				else if (parent->_bf == -2 && cur->_bf == -1)//右单旋
					RotateR(parent);
				else if (parent->_bf == 2 && cur->_bf == -1)
					RotateRL(parent);
				else if (parent->_bf == -2 && cur->_bf == 1)
					RotateLR(parent);
				else
					assert(false);
				break;
			}
			else//其他情况，正常情况下不可能出现
			{
				assert(false);
			}
		}
		return true;
	}


	// AVL树的验证
	bool IsAVLTree()
	{
		return _IsAVLTree(_root);
	}

	//中序遍历
	void Inorder()
	{
		_Inorder(_root);
		cout << endl;
	}

private:
	bool _IsAVLTree(Node* root)//AVL树判断
	{
		if (root == nullptr)
			return true;
		
		int LeftHeight = TreeHeight(root->_left);
		int RightHeight = TreeHeight(root->_right);

		if (abs(RightHeight - LeftHeight) > 1)
			return false;
		return _IsAVLTree(root->_left) && _IsAVLTree(root->_right);
	}

	size_t TreeHeight(Node* root)//树的高度
	{
		if (root == nullptr)
			return 0;
		size_t LeftHeight = TreeHeight(root->_left);
		size_t RightHeight = TreeHeight(root->_right);
		return LeftHeight > RightHeight ? LeftHeight + 1 : RightHeight + 1;
	}
	//左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* Parentparent = parent->_parent;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;   
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (Parentparent->_left == parent)
				Parentparent->_left = subR;
			else
				Parentparent->_right = subR;

			subR->_parent = Parentparent;
		}

		//平衡因子更新
		subR->_bf = parent->_bf = 0;
	}

	//右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* Parentparent = parent->_parent;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (Parentparent->_left == parent)
				Parentparent->_left = subL;
			else
				Parentparent->_right = subL;
			subL->_parent = Parentparent;
		}
			
		//更新平衡因子
		parent->_bf = subL->_bf = 0;
	}

	// 左右双旋
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateR(subR);
		RotateL(parent);

		if (bf == 0)
			parent->_bf = subR->_bf = subRL->_bf = 0;
		else if (bf == 1)
		{
			parent->_bf = -1;
			subRL->_bf = 0;
			subR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			subRL->_bf = 0;
			subR->_bf = 1;
		}
		else
		{
			assert(false);
		}
	}

	// 右左双旋
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(subL);
		RotateR(parent);

		if (bf == 0)
		{
			parent->_bf = subL->_bf = subLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subLR->_bf = 0;
			subL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
			subLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	//中序遍历
	void _Inorder(Node* root)
	{
		if (root == nullptr)
			return;
		_Inorder(root->_left);
		cout << root->_data << "->";
		_Inorder(root->_right);
	}
public:
	Node* _root;
};