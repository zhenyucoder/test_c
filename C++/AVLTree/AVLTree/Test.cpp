#include <iostream>
#include <vector>
#include <time.h>
using namespace std;
#include "AVLTree.h"

//int main()
//{
//	int a[] = {16, 3, 7, 11, 9, 26, 18, 14, 15};
//	AVLTree<int> avl;
//	for (auto x : a)
//	{
//		avl.Insert(x);
//	}
//	avl.Inorder();
//	bool bl = avl.IsAVLTree();
//	cout << bl << endl;
//	//{4, 2, 6, 1, 3, 5, 15, 7, 16, 14}
//	int a1[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
//	AVLTree<int> av2;
//	for (auto x : a)
//	{
//		av2.Insert(x);
//	}
//	av2.Inorder();
//	cout << av2.IsAVLTree() << endl;
//	return 0;
//}


int main()
{
	const int N = 30;
	AVLTree<int> avl;
	//srand((size_t)time(0));
	vector<int> v;
	v.reserve(N + 1);
	for (int i = 0; i < N; i++)
	{
		v.push_back(rand() + i);
	}
	for (auto x : v)
	{
		/*if (x == 5455)
		{
			int i = 0;
		}*/
		avl.Insert(x);
	}
	avl.Inorder();
	cout << avl.IsAVLTree() << endl;
	return 0;
}