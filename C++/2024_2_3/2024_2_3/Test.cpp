#include <iostream>
using namespace std;

//class Solution {
//public:
//    int waysToStep(int n) {
//        if (n == 1) return 1;
//        else if (n == 2) return 2;
//        else if (n == 3) return 4;
//
//        vector<long long> dp(n + 1);
//        dp[1] = 1; dp[2] = 2; dp[3] = 4;
//        for (int i = 4; i <= n; i++)
//        {
//            dp[i] = (dp[i - 1] + dp[i - 2] + dp[i - 3]) % 1000000007;
//        }
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int waysToStep(int n) {
//        if (n == 1) return 1;
//        else if (n == 2) return 2;
//        else if (n == 3) return 4;
//
//        long long a = 1, b = 2, c = 4, d = 0;//滚动数组优化空间复杂度
//        for (int i = 4; i <= n; i++)
//        {
//            d = (a + b + c) % 1000000007;
//            a = b; b = c; c = d;
//        }
//        return d;
//    }
//};

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        for (int i = 2; i <= n; i++)
//        {
//            dp[i] = min(dp[i - 2] + cost[i - 2], dp[i - 1] + cost[i - 1]);
//        }
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        if (n == 0 || n == 1) return 0;
//        vector<int> dp(n);
//        dp[n - 1] = cost[n - 1]; dp[n - 2] = cost[n - 2];
//        for (int i = n - 3; i >= 0; --i)
//        {
//            dp[i] = min(dp[i + 1] + cost[i], dp[i + 2] + cost[i]);
//        }
//        return min(dp[0], dp[1]);
//    }
//};


//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0) return 0;
//        if (n <= 2)  return 1;
//        int a = 0, b = 0, c = 1, d = 1;
//        for (int i = 3; i <= n; ++i) {
//            d = a + b + c;
//            a = b, b = c, c = d;
//        }
//        return d;
//    }
//};


class Solution {
public:
    int numDecodings(string s) {
        int n = s.size();
        vector<int> dp(n + 1);
        dp[0] = 1;  
        for (int i = 1; i <= n; i++) 
        {
            if (dp[i - 1] != '0')
                dp[i] = dp[i - 1];         //单独解码s[i - 1]
            if (i >= 2) 
            {
                int t = (s[i - 2] - '0') * 10 + s[i - 1] - '0';
                if (t >= 10 && t <= 26) 
                    dp[i] += dp[i - 2]; //将s[i - 2] 和 s[i - 1]组合解码
            }
        }
        return dp[n];
    }
};
