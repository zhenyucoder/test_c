//class Solution {
//public:
//    long long prev = LLONG_MIN;
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        auto left = isValidBST(root->left);
//        if (!left) return false;
//
//        bool cur = true;
//        if (root->val <= prev)
//            cur = false;
//        prev = root->val;
//        if (!cur) return false;
//
//        auto right = isValidBST(root->right);
//        return left && cur && right;
//    }
//};



//class Solution {
//public:
//    int count = 0, ret = 0;
//    int kthSmallest(TreeNode* root, int k) {
//        count = k;
//        dfs(root);
//        return ret;
//    }
//
//    void dfs(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        dfs(root->left);
//        if (--count == 0)
//        {
//            ret = root->val;
//            return;
//        }
//        dfs(root->right);
//    }
//};



//class Solution {
//public:
//    vector<string> ret;
//    vector<string> binaryTreePaths(TreeNode* root) {
//        dfs(root, "");
//        return ret;
//    }
//    void dfs(TreeNode* root, string path)
//    {
//        path += to_string(root->val);
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        if (root->left)
//        {
//            dfs(root->left, path + "->");
//        }
//        if (root->right)
//            dfs(root->right, path + "->");
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> ret;
//    bool check[10] = { 0 };
//    vector<vector<int>> permute(vector<int>& nums) {
//        vector<int> path;
//        dfs(nums, path, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, vector<int> path, int pos)
//    {
//        if (pos == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false)
//            {
//                check[i] = true;
//                path.push_back(nums[i]);
//                dfs(nums, path, pos + 1);
//                // 回溯
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        vector<int> path;
//        dfs(nums, path, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, vector<int> path, int pos)
//    {
//        if (pos == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        path.push_back(nums[pos]);
//        dfs(nums, path, pos + 1);
//        // 回溯
//        path.pop_back();
//        dfs(nums, path, pos + 1);
//    }
//};



//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<vector<int>> subsets(vector<int>& nums) {
//        vector<int> path;
//        ret.push_back(path);
//        dfs(nums, path, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, vector<int>& path, int pos)
//    {
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            ret.push_back(path);
//            dfs(nums, path, i + 1);
//            // 恢复现场
//            path.pop_back();
//        }
//    }
//};




//class Solution {
//public:
//    int ret = 0;
//    int subsetXORSum(vector<int>& nums) {
//        dfs(nums, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int prev, int pos)
//    {
//        if (pos == nums.size()) return;
//        for (int i = pos; i < nums.size(); i++)
//        {
//            prev ^= nums[i];
//            ret += prev;
//            dfs(nums, prev, i + 1);
//            // 回溯，恢复现场
//            prev ^= nums[i];
//        }
//    }
//};



//class Solution {
//public:
//    int ret = 0;
//    int subsetXORSum(vector<int>& nums) {
//        dfs(nums, 0, 0);
//        return ret;
//    }
//
//    void dfs(vector<int>& nums, int prev, int pos)
//    {
//        if (nums.size() == pos)
//        {
//            ret += prev;
//            return;
//        }
//
//        prev ^= nums[pos];
//        dfs(nums, prev, pos + 1);
//        // 恢复现场
//        prev ^= nums[pos];
//        dfs(nums, prev, pos + 1);
//    }
//};




