//class Solution {
//public:
//    bool canFinish(int n, vector<vector<int>>& prerequisites) {
//        unordered_map<int, vector<int>> edges;
//        vector<int> in(n);
//
//        //建图
//        for (auto v : prerequisites)
//        {
//            int a = v[0], b = v[1];
//            edges[b].push_back(a);
//            in[a]++;
//        }
//
//        queue<int> q;
//        for (int i = 0; i < n; i++)
//            if (in[i] == 0)
//                q.push(i);
//
//        //拓扑排序
//        while (q.size())
//        {
//            int t = q.front(); q.pop();
//            for (auto x : edges[t])
//            {
//                in[x]--;
//                if (in[x] == 0)
//                    q.push(x);
//            }
//        }
//
//        //检查是否全部符合拓扑排序
//        for (int i = 0; i < n; i++)
//            if (in[i])
//                return false;
//        return true;
//    }
//};




//class Solution {
//public:
//    vector<int> findOrder(int n, vector<vector<int>>& prerequisites) {
//        unordered_map<int, vector<int>> edges;
//        vector<int> in(n);
//
//        //建图
//        for (auto& v : prerequisites)
//        {
//            int a = v[0], b = v[1];
//            edges[b].push_back(a);
//            in[a]++;
//        }
//
//        queue<int> q;
//        //将入度为0的顶点加入队列
//        for (int i = 0; i < n; i++)
//            if (in[i] == 0)
//                q.push(i);
//
//        //拓扑排序
//        vector<int> ret;
//        while (q.size())
//        {
//            int t = q.front(); q.pop();
//            ret.push_back(t);
//
//            for (auto x : edges[t])
//            {
//                in[x]--;
//                if (in[x] == 0)
//                    q.push(x);
//            }
//        }
//
//        if (ret.size() == n)
//            return ret;
//        else
//            return {};
//    }
//};





