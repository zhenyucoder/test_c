//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        if (n < 3)
//            return 0;
//        int ret = 0;
//        vector<int> dp(n);
//        for (int i = 2; i < n; i++)
//        {
//            if (nums[i] - nums[i - 1] != nums[i - 1] - nums[i - 2])
//                dp[i] = 0;
//            else
//                dp[i] = dp[i - 1] + 1;
//            ret += dp[i];
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        int ret = 0;
//        for (int i = 2; i < n; i++)
//        {
//            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2])
//                dp[i] = dp[i - 1] + 1;
//            else
//                dp[i] = 0;
//            ret += dp[i];//累加所有结果
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size();
//        vector<int> f(n, 1), g(n, 1);
//        int ret = 0;
//        for (int i = 1; i < n; i++)
//        {
//            if (arr[i] > arr[i - 1])
//                f[i] = g[i - 1] + 1;
//            else if (arr[i] < arr[i - 1])
//                g[i] = f[i - 1] + 1;
//            ret = max(ret, max(f[i], g[i]));
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            ret = max(ret, dp[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        //数组和
//        int sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//        }
//        int n = nums.size();
//        vector<int> f(n + 1);//普通数组最大连续子数组和
//        auto g = f;//普通数组最小连续子数组和
//        int fmax = INT_MIN, gmin = INT_MAX;//分别统计f中最大值，g中最小值
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(nums[i - 1], f[i - 1] + nums[i - 1]);
//            fmax = max(fmax, f[i]);
//
//            g[i] = min(nums[i - 1], g[i - 1] + nums[i - 1]);
//            gmin = min(gmin, g[i]);
//        }
//        return sum == gmin ? fmax : max(fmax, sum - gmin);//判断数组元素是否全为负数
//    }
//};

//class Solution{
//public:
//    int maxProduct(vector<int>&nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);//f[i]表示以i为结尾的子数组的最大乘积
//        auto g = f;//g[i]表示以i为结尾的子数组的最小乘积
//        int ret = INT_MIN;
//        f[0] = 1, g[0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(nums[i - 1], max(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]));
//            g[i] = min(nums[i - 1], min(g[i - 1] * nums[i - 1], f[i - 1] * nums[i - 1]));
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);//f[i]: 以i为结尾的，乘积为正的最长子数组长度
//        auto g = f;//g[i]: 以i为结尾的，乘积为负的最长子数组长度
//        int ret = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            ret = max(ret, f[i]);
//        }
//        return ret;
//    }
//};



