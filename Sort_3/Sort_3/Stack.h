#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>


typedef int STDateType;
typedef struct Stack
{
	STDateType* a;
	int top;//ջ��
	int capacity;//����
}ST;


void STInit(ST* ps);
void STDestroy(ST* ps);
void STPush(ST* ps, STDateType x);
void STPop(ST* ps);

STDateType STTop(ST* ps);
int STSize(ST* ps);
bool STEmpty(ST* ps);
