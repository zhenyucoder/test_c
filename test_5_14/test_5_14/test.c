#include <stdio.h>
//int main()
//{
//	int a = 2;
//	int b = 3;
//	printf("a=%d b=%d\n", a, b);
//	a =a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d b=%d\n\n", a, b);
//	return 0;
//}
//int main()
//{
//	int arr1[10];
//	int count = 10;
//	int arr2[count];//常量表达式才可以
//	//VS2019 VS2022 这样的IDE 不支持C99 中的变长数组
//
//	//C99 标准之前 数组的大小只能是常量表达式
//	//C99 标准中引入了：变长数组的概念，使得数组在创建的时候可以使用变量，但是这样的数组不能初始化
//
//	return 0;
//}

//int main()
//{
//	//int arr1[10] = {1,2,3,4,5,6,7,8,9,10};//完全初始化
//	//int arr2[10] = { 1,2,3 };//不完全初始化，剩余的元素默认都是0
//	//int arr3[10] = { 0 };//不完全初始化，剩余的元素默认都是0
//	//int arr4[] = { 0 };//省略数组的大小，数组必须初始化，数组的大小是根据初始化的内容来确定
//	//int arr5[] = { 1,2,3 };
//	//int arr6[];//err
//
//	char arr1[] = "abc";
//	char arr2[] = {'a', 'b', 'c'};
//	char arr3[] = { 'a', 98, 'c' };
//
//
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//              0 1 2 3 4 5 6 7 8 9 
//	//printf("%d\n", arr[5]);//[] 下标引用操作符
//	//printf("%d\n", arr[0]);//[] 下标引用操作符
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);//10
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}

//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//              0 1 2 3 4 5 6 7 8 9 
//	//printf("%d\n", arr[5]);//[] 下标引用操作符
//	//printf("%d\n", arr[0]);//[] 下标引用操作符
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);//10
//	for (i = 0; i < sz; i+=2)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}



//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//              0 1 2 3 4 5 6 7 8 9 
//	//printf("%d\n", arr[5]);//[] 下标引用操作符
//	//printf("%d\n", arr[0]);//[] 下标引用操作符
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);//10
//	for (i = sz-1; i >= 0; i--)
//	{
//		printf("%d ", arr[i]);
//	}
//
// 
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//              0 1 2 3 4 5 6 7 8 9 
//	//printf("%d\n", arr[5]);//[] 下标引用操作符
//	//printf("%d\n", arr[0]);//[] 下标引用操作符
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);//10
//	for (i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}
//

//
//int main()
//{
//	int arr[10] = {0};//10 * 4
//	printf("%d\n", sizeof(arr));//40 - 计算的是数组的总大小，单位是字节
//	printf("%d\n", sizeof(arr[0]));//4
//	int sz = sizeof(arr) / sizeof(arr[0]);//计算数组元素个数的方法
//	printf("%d\n", sz);
//
//	return 0;
//}
//


//%p  --  是用来打印地址的
//int main()
//{
//	int arr[10] = { 1,2,3,4,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("&arr[%d] = %p\n", i, &arr[i]);
//	}
//
//	return 0;
//}


//int main()
//{
//	//数组的创建
//	int arr[4][5];
//	char ch[3][8];
//
//	return 0;
//}
//

//
//int main()
//{
//	//数组的初始化
//	int arr[4][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7},{5,6,7,8,9} };
//	int arr2[4][5] = { {1,2,3 }, {2, 3, 4 }, {3, 4, 5, 6, 7}, {5, 6, 7, 8, 9} };
//	//二维数组即使初始化的了
//	//行是可以省略的，但是列是不能省略的
//	int arr3[][5] = { {1,2,3 }, {2, 3, 4 }, {3, 4, 5, 6, 7}, {5, 6, 7, 8, 9} };
//
//	return 0;
//}
//

//int main()
//{
//	int arr[4][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7},{5,6,7,8,9} };
//	//printf("%d\n", arr[2][3]);
//	int i = 0;
//	//行号
//	for (i = 0; i < 4; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);//0 1 2 3 4
//		}
//		printf("\n");
//	}
//	return 0;
//}
//



int main()
{
	int arr[4][5] = { 0 };
	int i = 0;
	//行号
	for (i = 0; i < 4; i++)
	{
		int j = 0;
		for (j = 0; j < 5; j++)
		{
			printf("&arr[%d][%d] = %p\n",i,j, &arr[i][j]);
		}
	}
	return 0;
}


int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int i = 0;
	//0~10
	//越界访问
	//
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}