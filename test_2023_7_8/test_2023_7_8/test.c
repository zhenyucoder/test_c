#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>




//暴力解法
//int main()
//{
//	//需要查找的数字
//	int target = 0;
//	scanf("%d", &target);
//	int flag = 1;
//	int matrix[3][4] = { 1,2,3,4,2,5,6,8,10,13,14,19 };
//	for (int row = 0; row < 3; row++)
//	{
//		for (int col = 0; col < 4; col++)
//		{
//			if (target == matrix[row][col])
//			{
//				printf("%d在矩阵中存在\n", target);
//				flag = 0;
//				break;
//			}
//		}
//	}
//	if (flag)
//	{
//		printf("%d在矩阵中不存在\n", target);
//	}
//	return 0;
//}
//


//优化
//二分查找
//首先将要查找的数字和右上角的矩阵元素比较，如果查找数字更大，则该行矩阵元素较小排除该行
// 如果查找数子更小，则该列所有矩阵元素都大于查早数字，跳过该列
//int SearchMatrix(int matrix[][4], int target, int row, int col)
//int SearchMatrix(int (*matrix)[4], int target, int rows, int cols)
//{
//	int row = 0;
//	int col = cols - 1;
//	while (row < rows && col >= 0)
//	{
//		if (matrix[row][col] == target)
//			return 1;
//		else if (matrix[row][col] < target)
//			row++; //排除当前行
//		else
//			col--; //排除当前列
//
//	}
//	return 0;
//}
//
//int main()
//{
//	int matrix[3][4] = { {1,2,3,4},{2,5,6,8},{10,13,14,19} };
//	int target = 0;
//	scanf("%d", &target);
//
//	int result = SearchMatrix(matrix, target, 3, 4);
//
//	if (result)
//		printf("目标数字存在\n");
//	else
//		printf("目标数字不存在\n");
//
//	return 0;
//}






//#include <string.h>
////左旋函数定义
//void Rotate_String(char* str, int left, int right)
//{
//	int tmp = 0;
//	while (left < right)
//	{
//		tmp = str[left];
//		str[left++] = str[right];
//		str[right--] = tmp;
//	}
//}
//
//
//void Left_RotateString(char str[], int k)
//{
//	int sz = strlen(str);
//	k %= sz;
//	//无需旋转
//	if (k == 0)
//		return;
//
//	//左旋k个字符串定义
//	Rotate_String(str, 0, sz - 1);
//	Rotate_String(str, 0, sz-k-1);
//	Rotate_String(str, sz-k, sz - 1);
//}
//
//
//int main()
//{
//	char str[] = "ABCDE";
//	int k = 3;
//	Left_RotateString(str, k);
//	printf("左旋后字符串:%s\n", str);
//	return 0;
//}




//void reverseString(char* s, int sSize) {
//    int left = 0, right = sSize - 1;
//    while (left < right)
//    {
//        int temp = s[left];
//        s[left++] = s[right];
//        s[right--] = temp;
//    }
//}




//int reverse(int x) {
//    int rev = 0;
//    while (x != 0) {
//        if (rev < INT_MIN / 10 || rev > INT_MAX / 10) {
//            return 0;
//        }
//        int digit = x % 10;
//        x /= 10;
//        rev = rev * 10 + digit;
//    }
//    return rev;
//}




//#include <string.h>
//int isRotation(char* s1, char* s2)
//{
//    int len1 = strlen(s1);
//    int len2 = strlen(s2);
//
//    // 检查长度是否相等
//    if (len1 != len2) 
//    {
//        return 0;
//    }
//
//    // 拼接s1与s1，判断s2是否为拼接后的字符串的子串
//    char* concat = strcat(s1, s1);
//    if (strstr(concat, s2) != NULL)
//    {
//        return 1;
//    }
//    else 
//    {
//        return 0;
//    }
//}
//
//int main()
//{
//    char s1[] = "AABCD";
//    char s2[] = "BCDAA";
//    printf("%d\n", isRotation(s1, s2)); // 输出1
//
//    char s3[] = "abcd";
//    char s4[] = "ACBD";
//    printf("%d\n", isRotation(s3, s4)); // 输出0
//
//    return 0;
//}


