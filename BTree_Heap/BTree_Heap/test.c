#include "Heap.h"



TestHeap()
{
	Heap ph;
	HeapInit(&ph);
	int a[] = { 1,4,2,8,10,13,5 };
	HeapCreate(&ph, a, sizeof(a) / sizeof(int));
	HeapPrint(&ph);
	HeapPush(&ph, 20);
	HeapPrint(&ph);

	HeapPush(&ph, 21);
	HeapPrint(&ph);

	printf("%d\n", HeapTop(&ph));
	printf("%d\n", HeapSize(&ph));
	printf("%d\n", HeapEmpty(&ph));
	HeapDestroy(&ph);
}


int main()
{
	TestHeap();
	return 0;
}