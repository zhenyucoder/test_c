#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int HpDataType;
typedef struct Heap
{
	HpDataType* a;
	int size;
	int capacity;
}Heap;

//堆的初始化
void HeapInit(Heap* php);
// 堆的构建
void HeapCreate(Heap * php, HpDataType* a, int n);
//堆的销毁
void HeapDestroy(Heap* php);
//堆的插入
void HeapPush(Heap* php, HpDataType x);
//堆的删除
void HeapPop(Heap* php);
//堆顶数据
HpDataType HeapTop(Heap* php);
//堆的数据个数
int HeapSize(Heap* php);
//堆判空
bool HeapEmpty(Heap* php);

//打印
void HeapPrint(Heap* php);
