#include "Heap.h"

void Swap(HpDataType* p1, HpDataType* p2)
{
	HpDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustDown(HpDataType* a, int size, int parent)
{
	assert(a);

	int child = parent * 2 + 1;

	while (child<size)
	{
		//右节点存在且更大，孩子节点+1
		if (child + 1 < size && a[child] < a[child + 1])
		{
			child += 1;
		}

		//孩子节点更大，和父节点交换
		if (a[parent] < a[child])
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


void AdjustUp(HpDataType* a, int size, int child)
{
	assert(a);

	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void HeapInit(Heap* php)
{
	php->a = NULL;
	php->size = 0;
	php->capacity = 0;
}

// 堆的构建
void HeapCreate(Heap* php, HpDataType* a, int n)
{
	assert(php && a);

	php->a = (HpDataType*)malloc(n * sizeof(HpDataType));
	if (php->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	php->capacity = n;
	php->size = n;

	for (int i = 0; i < n; i++)
	{
		php->a[i] = a[i];
	}

	for (int i = (n - 1) / 2; i >= 0; i--)
	{
		AdjustDown(php->a, php->size, i);
	}
}



//堆的销毁
void HeapDestroy(Heap* php)
{
	assert(php);
	free(php->a);
	php->a = NULL;
	php->size = php->capacity = 0;
}


//堆的插入
void HeapPush(Heap* php, HpDataType x)
{
	assert(php);

	//检查容量
	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HpDataType* tmp = (HpDataType*)realloc(php->a, newcapacity * sizeof(HpDataType));
		if (tmp== NULL)
		{
			perror("malloc fail");
			exit(-1);
		}

		php->a = tmp;
		php->capacity = newcapacity;
	}

	//插入数据
	php->a[php->size] = x;
	php->size++;

	//向上调整
	AdjustUp(php->a, php->size, php->size - 1);
}

//堆的删除
void HeapPop(Heap* php)
{
	assert(php);

	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;
	AdjustDown(php->a, php->size, 0);
}



//打印
void HeapPrint(Heap* php)
{
	assert(php);

	for (int i = 0; i < php->size; i++)
	{
		printf("%d ", php->a[i]);
	}
	printf("\n");
}


//堆顶数据
HpDataType HeapTop(Heap* php)
{
	assert(php);
	return php->a[0];
}

//堆的数据个数
int HeapSize(Heap* php)
{
	assert(php);
	return php->size;
}

//堆判空
bool HeapEmpty(Heap* php)
{
	assert(php);
	return php->size == 0;
}
