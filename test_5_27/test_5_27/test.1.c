#include <stdio.h>


//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = 10 - i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//
//	printf("%p\n", &i);
//	printf("%p\n", &arr[10]);
//
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//	}
//	for (i = 0; i < 12; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



//函数调用堆栈

//void test2()
//{
//	printf("test\n");
//}
//void test1()
//{
//	test2();
//}
//void test()
//{
//	test1();
//}
//int main()
//{
//	test();
//	return 0;
//}



//模拟实现strcpy函数功能
// #include <assert.h>
//void My_strcpy(char* dest, char* src)
//{
//	//指针不希望为空指针(断言)
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;//‘\0'拷贝
//}

//const修饰指针的时候
//当const放在*的左边时，限制的时指针指向的内容，不能通过指针变量改变指针所指向的内容，但是指针变量本身是可以改变的
//当const放在*的右边时，限制的时指针变量本身，不能改变指针变量，但指针变量所指向的内容可以通过指针来的改变

//指针scr中指针所指向的内容不希望被改变（const修饰）
//char * My_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	//指针不希望为空指针(断言)
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*dest++ = *src++)
//		;
//	return ret;
//}




//模拟实现strlen函数
//#include <assert.h>
//int My_strlen(const char* str)
//{
//	assert(*str != NULL);
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//size_t 是专门为sizeof 设计的一个类型
//size_t unsigned int /unsigned long
//#include <assert.h>
//size_t My_strlen(const char* str)
//{
//	assert(*str != NULL);
//	size_t count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abc";
//	size_t len = My_strlen(arr);
//	printf("%zd\n", len);
//	return 0;
//}



int 