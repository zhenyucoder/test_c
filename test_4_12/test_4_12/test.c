#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int input = 0;
//	printf("你要好好学习吗?(1/0)");
//	scanf("%d", &input);
//	if (input == 1)
//	{
//		printf("坚持，你会有好offer\n");
//	}
//	else
//	{
//		printf("卖红薯\n");
//	}
//	return 0;
//}
////坚持写代码，当代码行数大于20000时，输出好offer。
//#include <stdio.h>
//int main()
//{
//	printf("坚持写代码:>\n");
//	int line = 0;
//	while(line <= 20000)
//	{
//		line++;
//		printf("我要坚持写代码\n");
//	}
//	if (line >= 20000)
//	{
//		printf("好offer\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 }; //定义一个整形数组，最多放10个元素
//	int arr1[10] = { 1,2,3 }; //定义一个数组，但只初始化前3个元素，其余7个元素为0
//	int arr2[10];      //这种写法也可以，但数组中的值为随机值
//	int arr3[] = { 1,2,3,4,5,6 }; //这样也可以，[]中的值根据{}中初始化的值确定
//	int arr4[];   //这种写法是错误的
//	 //一般情况下数组[]中如果有字符等，只能为常量
//	//但C99标准支持以下写法
//	int = 10;
//	int arr[n];//C99标准中  变长数组，允许数组在创建的时候，数组大小用变量指定，但是这种数组不能初始化
//	return 0;
//}
// 
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };//10个元素，则下表是从0-9
//	printf("%d\n", arr[6]);//打印下标为6对应的元素，即7
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", i);//将arr中的元素依次打印
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	//‘/’为取余操作符
//	//如果要打印小数，则操作符两端运算数字至少有一个是小数
//	printf("%d\n", 8 / 3);//结果为2
//	printf("%.2f\n", 8.0 / 3);//结果为2.67
//}

//#include <stdio.h>
//int main()
//{
//	//‘%'为取余操作符
//	//'%'两端操作对象必须为整数
//	printf("%d\n", 8 % 3); //结果为2
//	printf("%d\n", 8 % 2); //结果为0
//	return 0;
//}

#include <stdio.h>
int main(void)			//一个简单C程序
{
	int num;			//定义一个名为num的变量
	num = 14;			//为num赋一个值
	printf("What is your favorite day in April ? "); //使用printf()函数
	printf("14\n");
	printf("My favorite day is %d\n", num);
	return 0;
}