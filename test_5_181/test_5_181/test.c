#include <stdio.h>
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	i = a++ || ++b || d++;
//
//	printf("a=%d b=%d c=%d d=%d\n", a, b, c, d);
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = (a > b, a = b + 10, a, b = a + 1);
//	printf("c=%d\n", c);
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };//数组的起始是有下标的，下标从0开始
//	//			    0 1 2 3 4 5 6 7 8 8 9
//	printf("%d\n", arr[2]);// [] 下标引用操作符，arr和2是两个操作数
//	printf("%d\n", 2[arr]);
//	return 0;
//}

//#include <string.h>
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int len = strlen("abc");//( )函数调用操作符
//	//（）的操作数是：strlrn 和 "abc
//	printf("%d\n", len);
//
//	int c = Add(3, 5);//( ) 函数调用操作符
//	//（）的操作数是：Add 和3 5
//	//对于函数调用操作符来说，至少有一个操作数
//
//	printf("%d\n", c);
//	return 0;
//}

//定义一个struct Book类型
//struct Book
//{
//	char name[30];
//	char author[20];
//	float price;
//};
//
//int main()
//{
//	struct Book b= { "C primer Plus","Stephen Prate",47.8f };//创建一个变量b
//	//结构体.成员名，答应相关信息
//	printf("%s %s %.1f\n", b.name, b.author, b.price);
//	return 0;
//}

//struct Book
//{
//	char name[30];
//	char author[20];
//	float price;
//};
////定义过程
//void print(struct Book* b)
//{
//	//结构指针->成员名，访问成员
//	printf("%s %s %.1f\n", b->name, b->author, b->price);
//}
//
//int main()
//{
//	struct Book b= { "C primer Plus","Stephen Prate",47.8f };//创建一个变量b
//	//封装print()函数，打印信息
//	print(&b);
//	return 0;
//}


//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;
//	b = ++c, c++, ++a, a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}

//写一个函数返回参数二进制中 1 的个数。
//比如： 15    00001111    4 个 1
//
//int number_of_1(unsigned int m)
//{
//	int count = 0;
//	while (m)
//	{
//		if (m % 2 == 1)
//			count++;
//		m /= 2;
//	}
//	return count;
//}


//int number_of_1(int m)
//{
//	int count = 0;
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((m >> i) & 1) == 1)
//			count++;
//	}
//	return count;
//}
//
//int number_of_1(int m)
//{
//	int count = 0;
//	while (m)
//	{
//		m = m & (m - 1);
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	//-1
//	//10000000000000000000000000000001
//	//11111111111111111111111111111110
//	//11111111111111111111111111111111--> 32
//	int n = 0;
//	scanf("%d", &n);//15
//	int ret = number_of_1(n);
//	printf("%d\n", ret);//4
//	return 0;
//}
//

//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	//30 28 26 ... 4 2 0
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	printf("\n");
//	for (i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	printf("\n");
//
//	return 0;
//}
//

//
//两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//输入例子 :
//1999 2299
//输出例子 : 7
//

int number_of_1(int m)
{
	int count = 0;
	while (m)
	{
		m = m & (m - 1);
		count++;
	}
	return count;
}

int main()
{
	int m = 0;
	int n = 0;
	scanf("%d %d", &m, &n);
	//异或 - 相同为0，相异为1
	int ret = number_of_1(m ^ n);
	printf("%d\n", ret);

	return 0;
}
