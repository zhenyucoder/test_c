#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,3,4,5 };
//    short* p = (short*)arr;
//    int i = 0;
//    for (i = 0; i < 4; i++)
//    {
//        *(p + i) = 0;
//    }
//
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}

//int main()
//{
//	unsigned long pulArray[] = { 6,7,8,9,10 };
//	unsigned long* pulPtr;
//	pulPtr = pulArray;
//	*(pulPtr + 3) += 3;
//	printf("%d,%d\n", *pulPtr, *(pulPtr + 3));
//	return 0;
//}

//void My_arr(int arr[10], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	My_arr(arr, sz);
//	return 0;
//}

//#include <string.h>
//
//int main()
//{
//    char  arr[20] = { 0 };
//    scanf("%s", arr);
//
//    int tmp = strlen(arr);
//    int i = 0;
//    for (i = tmp - 1; i >= 0; i--)
//    {
//        printf("%c", arr[i]);
//    }
//    return 0;
//}

//#include <math.h>
//int My_n(int i)
//{
//	int count = 0;
//	if (i < 9)
//		return 1;
//	else
//		return 1 + My_n(i / 10);
//}
//
//int My_sum(int i,int ret)
//{
//	int sum = 0;
//	for (int x = ret; x >= 0; x--)
//	{
//		sum = sum + pow(i %= 10, ret);
//	}
//	return sum;
//}
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		int ret=My_n(i);
//		int tmp = My_sum(i,ret);
//		if (tmp == i)
//			printf("%d ", i);
//	}
//	return 0;
//}

//int main()
//{
//	int m = 10;
//	//cosnt 可以修饰指针
//	int n  = 100;
//	const int * p = &m;
//	//*p = 0;//err
//	p = &n; //ok
//
//	printf("%d\n", m);
//
//	return 0;
//}

//
//int main()
//{
//	int m = 10;
//	//cosnt 可以修饰指针
//	int n = 100;
//	int * const p = &m;
//	*p = 0;//ok
//	p = &n; //err
//
//	printf("%d\n", m);
//
//	return 0;
//}
//

//模拟实现一个strlen函数
//assert
//const

//size_t 是专门为sizeof 设计的一个类型
//size_t  unsigned int / unsigned long 
//>=0
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	size_t count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abc";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//
//	return 0;
//}
//
//%u 无符号整数的
//
//

//
//int Add(int x, int y)
//{
//	return x - y;
//}
//int main()
//{
//	int ret = Add(2, 3);
//	printf("%d\n", ret);
//	return 0;
//}
//

#include <stdio.h>

//int i;//全局变量，如果不初始化，默认是0
////
////静态变量和全局变量，如果不初始化，默认是0
////
//int main()
//{
//    i--;//-1
//    // int  size_t
//    //size_t size_t
//    //-1     4
//    //
//    if (i > sizeof(i))//-1 > 4
//    {
//        printf(">\n");
//    }
//    else
//    {
//        printf("<\n");
//    }
//
//    return 0;
//}




int main()
{
    int n = 0;
    int m = 0;
    scanf("%d %d", &n, &m);

    //变长数组
    int arr1[1000];
    int arr2[1000];
    int i = 0;
    //第一行数据
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr1[i]);
    }
    //第二行数据
    for (i = 0; i < m; i++)
    {
        scanf("%d", &arr2[i]);
    }
    //合并
    int arr3[2000];
    i = 0;
    int j = 0;
    int k = 0;
    while (i < n && j < m)
    {
        if (arr1[i] < arr2[j])
        {
            arr3[k++] = arr1[i++];
        }
        else
        {
            arr3[k++] = arr2[j++];
        }
    }
    if (i == n)
    {
        //arr1遍历完了，需要将arr2中剩余的元素全部放在arr3中
        while (j < m)
        {
            arr3[k++] = arr2[j++];
        }
    }
    else
    {
        //arr2遍历完了，需要将arr1中剩余的元素全部放在arr3中
        while (i < n)
        {
            arr3[k++] = arr1[i++];
        }
    }

    //输出
    for (i = 0; i < n + m; i++)
    {
        printf("%d ", arr3[i]);
    }

    return 0;
}