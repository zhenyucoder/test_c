#include "BinaryTree.h"
#include "Queue.h"

BTNode* CreatBinaryTree()
{
	BTNode* n1 = BuyNode(1);
	BTNode* n2 = BuyNode(2);
	BTNode* n3 = BuyNode(3);
	BTNode* n4 = BuyNode(4);
	BTNode* n5 = BuyNode(5);
	BTNode* n6 = BuyNode(6);

	n1->left = n2;
	n1->right = n3;
	n2->left = n4;
	n2->right = n5;
	n3->left = n6;
	return n1;
}

void TestBT1(BTNode* root)
{
	BinaryTreePrevOrder(root);
	printf("\n");
	BinaryTreeInOrder(root);
	printf("\n");

	BinaryTreePostOrder(root);
	printf("\n");

}

void TestBT2(BTNode* root)
{
	printf("%d\n", BinaryTreeSize(root));
	printf("%d\n", BinaryTreeLevelKSize(root,2));
	printf("%d\n", BinaryTreeLevelKSize(root,3));
	printf("%d\n", BinaryTreeHeight(root));
	printf("%d\n", BinaryTreeFind(root, 5)->data);
	LevelOrder(root);
}
void TestBT3(BTNode* root)
{
	printf("%d\n", BinaryTreeComplete(root));
}
int main()
{
	BTNode* root=CreatBinaryTree();
	//TestBT1(root);
	//TestBT2(root);
	TestBT3(root);
	return 0;
}