#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>


typedef int QDateType;
typedef struct QueueNode
{
	struct QueueNode* next;
	QDateType date;
}QNode;

typedef struct Queue//不传二级指针有三种方式：带哨兵位、有返回值、结构体封装
{
	QNode* head;
	QNode* tail;
	int size;
}Que;

void QueueInit(Que* pq);
void QueueDestory(Que* pq);
void QueuePush(Que* pq, QDateType x);
void QueuePop(Que* pq);
QDateType QueueFront(Que* pq);
QDateType QueueBack(Que* pq);
bool QueueEmpty(Que* pq);
int QueueSize(Que* pq);