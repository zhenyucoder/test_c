#include <stdio.h>
//int main()
//{
//	int n = 9;
//	float* Pfloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*Pfloat的值为：%f\n", *Pfloat);
//
//	*Pfloat = 9.0;
//	printf("n的值为:%d\n", n);
//	printf("*Pfloat的值为：%f\n", *Pfloat);
//	return 0;
//}


//int main()
//{
//	int x = 0X100;
//
//	printf("dec= %d, octal= %o, hex=%x\n",x,x,x);
//	printf("dec= %#d, octal= %#o, hex=%#X\n",x,x,x);
//	return 0;
//}



//int hammingWeight(uint32_t n) 
//{
//    int ret = 0;
//    for (int i = 0; i < 32; i++) 
//    {
//        if (n & (1u << i)) {
//            ret++;
//        }
//    }
//    return ret;
//}




//
//int hammingWeight(uint32_t n) 
//{
//    int ret = 0;
//    while (n) 
//    {
//        n &= n - 1;
//        ret++;
//    }
//    return ret;
//}


//
//
//class Solution 
//{
//public:
//    int hammingDistance(int x, int y) 
//    {
//        int ret = x ^ y;     
//        int ans = 0;      
//        while (ret)
//        {       
//            ret &= (ret - 1);  
//            ans++;
//        }
//        return ans;
//    }
//};
//



//uint32_t reverseBits(uint32_t n) 
//{
//    uint32_t rev = 0;
//    for (int i = 0; i < 32 && n > 0; ++i) 
//    {
//        rev |= (n & 1) << (31 - i);
//        n >>= 1;
//    }
//    return rev;
//}
uint32_t reverseBits(uint32_t n)
{
    uint32_t ans = 0;
    for (int i = 0; i < 32 && n>0; i++) 
    {
        ans |= ((n & 1) << (31 - i));
        n >>= 1;
    }
    return ans;
}
