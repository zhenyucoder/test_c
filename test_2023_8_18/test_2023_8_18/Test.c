#include "Heap.h"


int main()
{
	HP hp;
	HPInit(&hp);
	int a[] = { 65,70,100,32,50,60 };
	for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
	{
		HPPush(&hp, a[i]);
	}

	while (!HPEmpty(&hp))
	{
		int top = HPTop(&hp);
		printf("%d\n", top);
		HPPop(&hp);
	}

	HPDestory(&hp);
	return 0;
}