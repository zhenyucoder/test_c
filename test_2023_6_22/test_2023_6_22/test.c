#include <limits.h>
#include <float.h>
#include <stdio.h>


//int main(void)
//{
//	int n = 10;
//	float* pfloat = (float*)&n;
//
//	printf("n的值为:%d\n", n);
//	printf("*pfloat的值为:%f\n", *pfloat);
//
//	*pfloat = 10.0;
//	printf("n的值为:%d\n", n);
//	printf("*pfloat的值为:%f\n", *pfloat);
//	return 0;
//}
// 



//bool isPrime(int x) 
//{
//    for (int i = 2; i * i <= x; ++i) 
//    {
//        if (x % i == 0)
//        {
//            return false;
//        }
//    }
//    return true;
//}
//
//int countPrimes(int n) 
//{
//    int ans = 0;
//    for (int i = 2; i < n; ++i) 
//    {
//        ans += isPrime(i);
//    }
//    return ans;
//}

//int countPrimes(int n) 
//{
//    if (n < 2)
//    {
//        return 0;
//    }
//    int isPrime[n];
//    memset(isPrime, 0, sizeof(isPrime));
//    int ans = 0;
//
//    for (int i = 2; i < n; ++i) 
//    {
//        if (!isPrime[i]) 
//        {
//            ans += 1;
//            if ((long long)i * i < n) 
//            {
//                for (int j = i * i; j < n; j += i) 
//                {
//                    isPrime[j] = 1;
//                }
//            }
//        }
//    }
//    return ans;
//}

//int countPrimes(int n) 
//{
//    if (n < 2) 
//    {
//        return 0;
//    }
//    int isPrime[n];
//    int primes[n], primesSize = 0;
//    memset(isPrime, 0, sizeof(isPrime));
//    for (int i = 2; i < n; ++i) 
//    {
//        if (!isPrime[i]) 
//        {
//            primes[primesSize++] = i;
//        }
//        for (int j = 0; j < primesSize && i * primes[j] < n; ++j) 
//        {
//            isPrime[i * primes[j]] = 1;
//            if (i % primes[j] == 0) 
//            {
//                break;
//            }
//        }
//    }
//    return primesSize;
//}






public static int romanToInt(String s)
{
	int res = 0;
	char[] arr = s.toCharArray();
	for (int i = arr.length - 1; i >= 0; i--) 
	{
		char c = arr[i];
		switch (c) 
		{
		case 'I':
			res += (res >= 5 ? -1 : 1);
			break;
		case 'V':
			res += 5;
			break;
		case 'X':
			res += 10 * (res >= 50 ? -1 : 1);
			break;
		case 'L':
			res += 50;
			break;
		case 'C':
			res += 100 * (res >= 500 ? -1 : 1);
			break;
		case 'D':
			res += 500;
			break;
		case 'M':
			res += 1000;
			break;
		}
	}
	return res;
}