//int BinaaryTreeSize(struct TreeNode* root)
//{
//    if (root == NULL)
//        return 0;
//    return 1 + BinaaryTreeSize(root->left) + BinaaryTreeSize(root->right);
//}
//
//void _inorderTraversal(struct TreeNode* root, int* arr, int* pi)
//{
//    if (root == NULL)
//        return;
//    if (root->left)
//    {
//        _inorderTraversal(root->left, arr, pi);
//    }
//    arr[(*pi)++] = root->val;
//    if (root->right)
//    {
//        _inorderTraversal(root->right, arr, pi);
//    }
//}
//
//int* inorderTraversal(struct TreeNode* root, int* returnSize) {
//    *returnSize = BinaaryTreeSize(root);
//    int* r_array = (int*)malloc(sizeof(int) * *returnSize);
//    int i = 0;
//    _inorderTraversal(root, r_array, &i);
//    return r_array;
//}



int BinaaryTreeSize(struct TreeNode* root)
{
    return root == NULL ? 0 : 1 + BinaaryTreeSize(root->left) + BinaaryTreeSize(root->right);
}
//����
int* inorderTraversal(struct TreeNode* root, int* returnSize) {
    *returnSize = BinaaryTreeSize(root);
    int* r_array = malloc(*returnSize * sizeof(int));
    struct TreeNode** stack = (struct TreeNode**)malloc(*returnSize * sizeof(struct TreeNode*));
    int top = 0;
    int i = 0;
    while (root != NULL || top > 0)
    {
        while (root != NULL)
        {
            stack[top++] = root;
            root = root->left;
        }
        root = stack[--top];
        r_array[i++] = root->val;
        root = root->right;
    }

    return r_array;
}