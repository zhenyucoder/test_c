#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//	const char* pstr = "hello world\n";
//	printf("%s\n", pstr);
//	return 0;
//}



//int main()
//{
//	char str1[] = "hello world";
//	char str2[] = "hello world";
//	const char *str3 = "hello world";
//	const char *str4 = "hello world";
//
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//
//	if (str3 == str4)
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and atr4 are not same\n");
//	return 0;
//}

//int* p1[10];  //整型指针的数组
//char *arr2[4];//一维字符指针的数组
//char** arr3[5]; //二级字符指针的数组



//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);
//	printf("%p\n", &arr);
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	printf("arr=%p\n", arr);
//	printf("&arr=%p\n", &arr);
//
//	printf("arr+1=%p\n", arr + 1);
//	printf("&arr+1=%p\n", &arr + 1);
//	return 0;
//}



////void print_arr(int arr[3][5], int row, int col)
//void print_arr(int (*arr)[5], int row, int col)
//{
//	for (int i = 0; i < row; i++)
//	{
//		for (int j = 0; j < col; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{6,7,8,9,10},{11,12,13} };
//	print_arr(arr, 3, 5);
//	//数组名arr，表示首元素地址
//	//但二维数组的首元素是二维数组的第一行
//	//所以这里传递的arr，其实相当于第一行的地址，是一维数组的地址，可以使用一维数组来接受
//	return 0;
//}






//模拟实现qsort

//排序整数
//void print(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//int cmp_int(const void* p1, const void* p2)
//{
//	return *((int*)p1) - *((int*)p2);
//}
//void test1()
//{
//	int arr[10] = { 10,6,2,97,3,56,34,57,45,24 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr, sz);
//}

//浮点数
//void print(float arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%f ", arr[i]);
//	}
//}
//int cmp_int(const void* p1, const void* p2)
//{
//	return *((float*)p2) - *((float*)p1);
//}
//void test2()
//{
//	float arr[10] = { 10.0, 6.3, 2, 97.1, 3.4, 56.12, 34.00, 57, 45.0, 24.11 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print(arr, sz);
//}


//结构体
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//int cmp_Stu_by_age(const void* p1, const void* p2)
//{
//	return((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//void test3()
//{
//	struct Stu arr[] = { {"zhangsan",23},{"lisi",58},{"wangwu",34} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_Stu_by_age);
//}

//int cmp_Stu_by_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//
//void test3()
//{
//	struct Stu arr[] = { {"zhangsan",23},{"lisi",58},{"wangwu",34} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_Stu_by_name);
//}
//
//int main()
//{
//	//test1();
//	//test2();
//	test3();
//	return 0;
//}



//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d %d", *(a + 1), *(ptr - 1));
//	return 0;
//}




//struct Test//20
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p;
//
//int main()
//{
//	printf("%p\n", p + 0x1);
//	printf("%p\n", (unsigned long)p + 0x1);
//	printf("%p\n", (unsigned int*)p + 0x1);
//	return 0;
//}





//int main()
//{
//	int a[3][2] = { (0,1),(2,3),(4,5) };
//	int* p;
//	p = a;
//	printf("%d\n", p[0]);
//	return 0;
//}




//int main()
//{
//	int aa[2][5] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d\n", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}




//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	//                                           -4
//	//10000000 00000000 00000000 00000100
//	//11111111 11111111 11111111 11111011
//	//11111111 11111111 11111111 11111100
//	//f  f   ff ff fc
//	return 0;
//}



void Swap(char* buf1, char* buf2, int size)
{
	int i = 0;
	int tmp = 0;
	for (i = 0; i < size; i++)
	{
		tmp = *buf1;
		*buf1++ = *buf2;
		*buf2++ = tmp;
	}
}

int cmp(const void* p1, const void* p2)
{
	return *(int*)p1 - *(int*)p2;
}

void bubble_sort(int *base, int num, int size, int (*cmp)(const void*, const void*))
{
	//趟数
	for (int i = 9; i < num - 1; i++)
	{
		//每趟内部比较对数
		for (int j = 0; j < num - 1 - i; j++)
		{
			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)
			{
				//交换
				Swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}

int main()
{
	int arr[] = { 34,23,12,43,54,76,24,87,45,23 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}