#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//    int a = 1, b = 1, c = 1;
//
//    while (scanf("%d %d %d", &a, &b, &c) != EOF)
//    {
//        if (a + b > c && a + c > b && b + c > a)
//        {
//            if (a == b && b == c)
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if (a == b || b == c || a == c)
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else
//            {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else
//        {
//            printf("Not a triangle!\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//    float a = 0.0;
//    float b = 0.0;
//    float c = 0.0;
//    while (scanf("%f %f %f", &a, &b, &c) == 3)
//    {
//        if (a == 0)
//        {
//            printf("Not quadratic equation\n");
//        }
//        else
//        {
//            float deta = b * b - 4 * a * c;
//            if (deta >= 0)
//            {
//                float result1 = (-b + sqrt(deta)) / (2 * a);
//                float result2 = (-b - sqrt(deta)) / (2 * a);
//                if (deta > 0)
//                {
//                    printf("x1=%.2f;x2=%.2f\n", result2, result1);
//                }
//                else
//                {
//                    if (result1 == 0)
//                    {
//                        printf("x1=x2=0.00\n");
//                    }
//                    else
//                    {
//                        printf("x1=x2=%.2f\n", result1);
//                    }
//                }
//            }
//            else
//            {
//                float shibu = (-b) / (2.0 * a);
//                float xubu = (sqrt(-deta)) / (2.0 * a);
//                if (xubu < 0)
//                {
//                    xubu = -xubu;
//                    printf("x1=%.2f-%.2fi;x2=%.2f+%.2fi\n", shibu, xubu, shibu, xubu);
//                }
//                else
//                {
//                    printf("x1=%.2f-%.2fi;x2=%.2f+%.2fi\n", shibu, xubu, shibu, xubu);
//                }
//            }
//        }
//    }
//    return 0;
//}

//int main()
//{
//    double a, b;
//    char ch;
//    while (scanf("%lf %c %lf", &a, &ch, &b) != EOF)
//    {
//        if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
//        {
//            if (ch == '+')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a + b);
//            else if (ch == '-')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a - b);
//            else if (ch == '*')
//                printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a * b);
//            else
//            {
//                if (b == 0.0)
//                    printf("Wrong!Division by zero!\n");
//                else
//                    printf("%.4lf%c%.4lf=%.4lf\n", a, ch, b, a / b);
//            }
//        }
//        else
//            printf("Invalid operation!\n");
//    }
//    return 0;
//}

//int main() 
//{
//    int x;
//    while (scanf("%d", &x) != EOF) 
//    {
//        for (int i = 1; i <= x; i++) 
//        {
//            for (int j = 1; j <= x; j++) 
//            {
//                if (j == x) printf("*\n");
//                else printf("* ");
//            }
//        }
//    }
//}

//int main() 
//{
//    int a;
//    while (scanf("%d", &a) != EOF) 
//    {
//        int i, k;
//        for (k = a; k > 0; k--) 
//        {
//            for (i = k; i > 0; i--) 
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//int main() 
//{
//    int a;
//    while (scanf("%d", &a) != EOF) 
//    {
//        int i, k;
//        for (k = 0; k < a; k++) 
//        {
//            for (i = 0; i < (a - k - 1); i++) 
//            {
//                printf("  ");
//            }
//            for (i = 0; i <= k; i++) 
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}

//int main()
//{
//    int a;
//    while (~scanf("%d", &a))
//    {
//        getchar();
//        switch (a)
//        {
//        case 200:printf("OK\n"); break;
//        case 202:printf("Accepted\n"); break;
//        case 400:printf("Bad Request\n"); break;
//        case 403:printf("Forbidden\n"); break;
//        case 404:printf("Not Found\n"); break;
//        case 500:printf("Internal Server Error\n"); break;
//        case 502:printf("Bad Gateway\n");
//        }
//    }
//    return 0;
//}

int main() 
{
    int* arr;
    int n = 0;
    scanf("%d", &n);
    arr = (int*)malloc(sizeof(int) * (n + 1));
    int number = 0;
    for (int i = 0; i < n; i++) 
    {
        scanf("%d", &arr[i]);
    }
    scanf("%d", &number);
    int j = n;
    for (; arr[j - 1] > number; j--) 
    {
        arr[j] = arr[j - 1];
    }
    arr[j] = number;
    for (int i = 0; i < n + 1; i++) 
    {
        printf("%d ", arr[i]);
    }
    return 0;
}