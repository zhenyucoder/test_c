#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int x = 0;
//	int y = 0;
//	int tmp = y;
//	scanf("%d %d", &x, &y);
//	while (tmp = x % y)
//	{
//		x = y;
//		y = tmp;
//	}
//	printf("%d\n", y);
//	return 0;
//}





//最小公倍数
//暴力解法
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int m = x > y ? x : y;
//	while (1)
//	{
//		if (m % x == 0 && m % y == 0)
//			break;
//		m++;
//	}
//	printf("%d\n", m);
//	return 0;
//}

//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//
//	int i = 1;
//	while (x * i % y)
//	{
//		i++;
//	}
//	printf("%d\n", x * i);
//	return 0;
//}


//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int q = x * y;
//	int tmp = 0;
//	while (tmp = x % y)
//	{
//		x = y;
//		y = tmp;
//	}
//	printf("%d %d", y, q / y);
//	return 0;
//}




#include <string.h>

void reverse(char* left, char* right)
{
    while (left < right)
    {
        char tmp = *left;
        *left = *right;
        *right = tmp;
        left++;
        right--;
    }
}

//函数逆序字符串
int main() {
    char arr[101];
    //scanf("%s", arr);
    gets(arr);//读取一个字符串，即使中间有空格
    //scanf("%[^\n]s", arr);
    int len = strlen(arr);
    reverse(arr, arr + len - 1);
    //逆序每个单词
    char* start = arr;
    char* cur = arr;

    while (*cur) {
        while (*cur != ' ' && *cur != '\0')
        {
            cur++;
        }
        reverse(start, cur - 1);
        start = cur + 1;
        if (*cur == ' ')
            cur++;
    }
    printf("%s\n", arr);
    return 0;
}
