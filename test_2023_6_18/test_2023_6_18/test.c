//int strStr(char* haystack, char* needle) 
//{
//    int i = 0, j = 0;
//    if (strlen(needle) > strlen(haystack))
//    {
//        return -1;
//    }
//    else
//    {
//        for (i = 0; i <= strlen(haystack) - strlen(needle); ++i)
//        {
//            for (j = 0; j < strlen(needle); ++j)
//            {
//                if (haystack[i + j] != needle[j])
//                {
//                    break;
//                }
//            }
//            if (j == strlen(needle))
//            {
//                return i;
//            }
//        }
//    }
//    return -1;
//}





//char* countHelper(char* s, int n)
//{
//    if (n == 1)
//        return s;
//    else
//    {
//        int count;
//        char ch[10000];
//        char* p = ch;
//       
//        while (*s != '\0')
//        {
//            count = 1;
//            while (*s == *(s + 1))
//            {
//                count++;
//                s++;
//            }
//            *p++ = (char)(count + '0');
//            *p++ = *s++;
//        }
//        return countHelper(ch, n - 1);
//    }
//}
//
//char* countAndSay(int n)
//{
//    return countHelper("1", n);
//}



char* longestCommonPrefix(char** strs, int strsSize) 
{
    int len = 0;
    int i = 0;
    int j = 0;
    len = strlen(strs[0]);
    for (i = 1; i < strsSize; ++i)
    {
        if (strlen(strs[i]) < len)
        {
            len = strlen(strs[i]);
        }
    }
    for (j = 0; j < len; ++j)
    {
        for (i = 1; i < strsSize; ++i)
        {
            if (strs[i][j] != strs[i - 1][j])
            {
                break;
            }
        }
        if (i != strsSize)
        {
            break;
        }
    }
    strs[0][j] = '\0';
    return strs[0];
}