#include <stdio.h>



//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    char arr[20][20] = { 0 };
//    while (~scanf("%d", &n))
//    {
//        //初始化为空格
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                arr[i][j] = ' ';
//            }
//        }
//
//        //主对角线
//        for (i = 0; i < n; i++)
//        {
//            arr[i][i] = '*';
//            arr[i][n - i - 1] = '*';
//        }
//
//        //输出
//        for (i = 0; i < n; i++)
//        {
//            for (j = 0; j < n; j++)
//            {
//                printf("%c", arr[i][j]);
//            }
//            printf("\n");
//        }
//
//    }
//
//    return 0;
//}


//int main() 
//{
//    int n = 0;
//    while (~scanf("%d", &n) )
//    {
//        int i = 0;
//        for (i = 0; i < n; i++)
//        {
//            int j = 0;
//            for (j = 0; j < n - i - 1; j++)
//            {
//                printf("  ");
//            }
//            for (; j < n; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}




//int main()
//{
//    char str[8001];
//    char t;
//    gets(str);
//    int len = strlen(str);
//    long countC = 0, countCH = 0, count = 0;
//    
//    for (int i = 0; i < len; i++)
//    {
//        if (str[i] == 'C')
//        {
//            countC++;
//        }
//        else if (str[i] == 'H')//用countCH来表示当前遇到的H可以跟之前遇到C进行countC种组合，由此得到所有的H情况
//        {
//            countCH = countCH + countC;
//        }
//        else if(str[i] == 'N')
//        {
//            count = count + countCH;//用count来表示当前遇到的N可以跟前面遇到的CH组成的countCH种组合，每个N全部加起来就是全部的CHN情况
//        }
//    }
//
//
//    printf("%ld", count);
//    return 0;
//}


int main()
{
    int n = 0, tmp = 0, max = 0;
    scanf("%d", &n);
    int arr[1000] = { 0 };   

    for (int i = 0; i < n; ++i)
    {
        scanf("%d ", &tmp);           
        arr[tmp] = tmp;
        if (tmp > max)                   
            max = tmp;
    }

    for (int i = 1; i <= max; ++i)
    {
        if (arr[i])
            printf("%d ", arr[i]);
    }
    return 0;
}