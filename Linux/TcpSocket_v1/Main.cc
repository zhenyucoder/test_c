#include <iostream>
#include <cstdlib>
#include <string>

#include "TcpServer.hpp"

void Usage(const std::string& proc)
{
    std::cout << "Usage:\n\t" << proc << "  serveroprt" << std::endl;
}

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(-1);
    }

    uint16_t serverport = std::stoi(argv[1]);
    TcpServer tsvr(serverport);
    tsvr.Init();
    tsvr.Start();
    return 0;
}

