#include <iostream>
#include <queue>
#include <mutex>
#include "LockGuard.hpp"

const int defaultsize = 5;

template <class T>
class BlockQueue
{
private:
    bool Empty()
    {
        return _q.size() == 0;
    }
    bool IsFull()
    {
        return _q.size() == _capacity;
    }

public:
    BlockQueue(int cap = defaultsize) : _capacity(cap)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_p_cond, nullptr);
        pthread_cond_init(&_c_cond, nullptr);
    }
    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_p_cond);
        pthread_cond_destroy(&_c_cond);
    }

    void Push(const T &data)
    {
        // pthread_mutex_lock(&_mutex);
        // while提高健壮性, 多进程中，多进程被唤醒后在pthread_cond_wait出竞争锁资源
        // 当第一个线程跑完，此时数据全部被消费了。但一旦阻塞在pthread_cond_wait出的线程竞争到锁资源后，就会向后跑，出错
        {
            LockGuard lock(&_mutex);
            while (IsFull())
            {
                pthread_cond_wait(&_p_cond, &_mutex);
            }
            _q.push(data);
            // 直接唤醒
            pthread_cond_signal(&_c_cond);
        }
        // pthread_mutex_unlock(&_mutex);
    }

    void Pop(T *data)
    {
        // pthread_mutex_lock(&_mutex);
        {
            LockGuard lock(&_mutex);
            while (Empty())
            {
                pthread_cond_wait(&_c_cond, &_mutex);
            }
            *data = _q.front();
            _q.pop();
            pthread_cond_signal(&_p_cond);
        }
        // pthread_mutex_unlock(&_mutex);
    }

private:
    std::queue<T> _q;
    int _capacity;
    pthread_mutex_t _mutex;
    pthread_cond_t _p_cond;
    pthread_cond_t _c_cond;
};