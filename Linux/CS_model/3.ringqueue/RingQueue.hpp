#include <iostream>
#include <pthread.h>
#include "LockGuard.hpp"
#include <vector>
#include <semaphore.h>

const int defaultsize = 5;
template <class T>
class RingQueue
{
private:
    void P(sem_t &sem)
    {
        sem_wait(&sem);
    }
    void V(sem_t &sem)
    {
        sem_post(&sem);
    }

public:
    RingQueue(int cap = defaultsize)
        : _q(cap), _cap(cap), _c_step(0), _p_step(0)
    {
        sem_init(&_data_sem, 0, 0);
        sem_init(&_space_sem, 0, cap);
        pthread_mutex_init(&_p_mutex, nullptr);
        pthread_mutex_init(&_c_mutex, nullptr);
    }

    void Push(const T &in)
    {
        P(_space_sem);
        {
            LockGuard lock(&_p_mutex);
            _q[_p_step] = in;
            _p_step++;
            _p_step %= _cap;
        }
        V(_data_sem);
    }

    void Pop(T *out)
    {
        P(_data_sem);
        {
            LockGuard lock(&_c_mutex);
            *out = _q[_c_step];
            _c_step++;
            _c_step %= _cap;
        }
        V(_space_sem);
    }

    ~RingQueue()
    {
        sem_destroy(&_space_sem);
        sem_destroy(&_data_sem);
        pthread_mutex_destroy(&_c_mutex);
        pthread_mutex_destroy(&_p_mutex);
    }

private:
    std::vector<T> _q;
    int _cap;
    int _c_step; // 消费者脚步
    int _p_step; // 生产者脚步

    sem_t _space_sem; // 给生产者使用
    sem_t _data_sem;  // 给消费者使用

    pthread_mutex_t _p_mutex;
    pthread_mutex_t _c_mutex;
};