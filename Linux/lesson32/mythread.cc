#include <iostream>
#include <pthread.h>
#include <unistd.h>

// int gal_val = 100;

void* ThreadRoution(void* arg)
{
    std::cout << "I am new thread" << std::endl;
    const char* threadname = (const char*)arg;
    std::cout << threadname << std::endl;
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, nullptr, ThreadRoution, (void*)"thread-1");
    std::cout << tid << std::endl;
    std::cout << "I am main thread" << std::endl;
    sleep(1);
    // pthread_join(tid, nullptr);
    return 0;
}


// void *ThreadRoution(void *arg)
// {
//     std::string threadname = (const char *)arg;

//     int cnt = 5;
//     while (cnt)
//     {
//         std::cout << "thread: " << threadname << ", cnt:" << cnt << std::endl;
//         if (--cnt == 0)
//         {
//             // pthread_exit((void*)pthread_self());
//             pthread_cancel(pthread_self());
//         }
//         sleep(1);
//     }
// }

// int main()
// {
//     pthread_t tid;
//     pthread_create(&tid, nullptr, ThreadRoution, (void *)"thread-1");

//     sleep(2);
//     // pthread_cancel(tid);
//     // pthread_detach(tid);

//     pthread_join(tid, nullptr);
//     std::cout << "join success !" << std::endl;
//     return 0;
// }

// void *ThreadRoution(void *arg)
// {
//     // std::string threadname = (const char *)arg;

//     // int cnt = 10;
//     // while (cnt--)
//     // {
//     //     gal_val--;
//     //     // std::cout << "I am a new thread: " << threadname << ", cnt: " << cnt-- << std::endl;
//     //     sleep(1);
//     // }

//     sleep(3);
//     std::cout << "new thread attempt to join main thread!" << std::endl;
//     pthread_t tid = (pthread_t)arg;
//     std::cout << "new thread get tid: " << tid << std::endl;
//     pthread_join(tid, nullptr);
//     std::cout << "new thread join success ? " << std::endl;
// }

// int main()
// {
//     pthread_t tid;
//     // pthread_create(&tid, nullptr, ThreadRoution, (void *)"thread-1");
//     pthread_create(&tid, nullptr, ThreadRoution, (void *)pthread_self());

//     // std::cout << "tid: " << tid << std::endl;
//     // std::cout << pthread_self() << std::endl;
//
//     // while(true)
//     // {
//     //     std::cout << "I am main thread" << gal_val  <<std::endl;
//     //     sleep(1);
//     // }
//
//     std::cout << "I am main thread" << std::endl;
//     std::cout << "main thread tid" << pthread_self() << std::endl;
//     int cnt = 5;
//     while(cnt)
//     {
//         std::cout << cnt-- <<std::endl;
//         sleep(1);
//     }
//     // pthread_join(tid, nullptr);
//     // std::cout << "等待线程成功" << std::endl;
//     return 0;
// }