#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <cstdlib>
#include <cstring>
#include <string>

void Usage(const std::string& proc)
{
    std::cout << "\n输入格式错误" << std::endl;
    std::cout << "Usage: " << proc << " -signumber processid" << std::endl;
}

void handler(int signo)
{
    std::cout << "获取信号:" << signo << std::endl;
    sleep(1);
    // exit(0);
}

int cnt = 0;

// ./test -n pid
int main(int argc, char* argv[])
{
    signal(11, handler);
    // int a = 10;
    // a /= 0;
    int *p = nullptr;
    *p = 10;

    // while(true)
    // {
    //     signal(2, handler);

    //     std::cout << getpid() <<": running .... " << ++cnt << std::endl;
    //     sleep(1);
    //     // if(cnt == 4) 
    //     //     abort();
    //         // raise(11);
    //     //     kill(getpid(), 2);
    // }


    // if(argc != 3)
    // {
    //     Usage(argv[0]);
    //     exit(0);
    // }

    // int signumber = std::stoi(argv[1] + 1);
    // pid_t processid = std::stoi(argv[2]);
    // kill(processid, signumber);
    return 0;
}
