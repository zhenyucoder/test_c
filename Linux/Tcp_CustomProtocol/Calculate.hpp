#pragma once
#include <iostream>
#include <string>
#include "Protocal.hpp"

namespace CalCulateNS
{
    enum
    {
        Success = 0,
        DivZeroErr,
        ModZeroErr,
        UnKnowOper
    };
    // 业务处理
    class Calculate
    {
    public:
        Calculate()
        {
        }
        ~Calculate()
        {
        }
        std::shared_ptr<Protocol::Response> Cal(std::shared_ptr<Protocol::Request> req)
        {
            std::shared_ptr<Protocol::Response> resp = factory->BuildResponse();
            resp->SetCode(Success);
            switch (req->GetOp())
            {
            case '+':
                resp->SetResult(req->GetX() + req->GetY());
                break;
            case '-':
                resp->SetResult(req->GetX() - req->GetY());
                break;
            case '*':
                resp->SetResult(req->GetX() * req->GetY());
                break;
            case '/':
            {
                if (req->GetY() == 0)
                {
                    resp->SetCode(DivZeroErr);
                }
                else
                {
                    resp->SetResult(req->GetX() / req->GetY());
                }
            }
            break;
            case '%':
            {
                if (req->GetY() == 0)
                {
                    resp->SetCode(ModZeroErr);
                }
                else
                {
                    resp->SetResult(req->GetX() % req->GetY());
                }
            }
            break;
            default:
                resp->SetCode(UnKnowOper);
                break;
            }
            return resp;
        }

    private:
        std::unique_ptr<Protocol::Factory> factory;
    };
}