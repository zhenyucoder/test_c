#include <iostream>
#include <memory>
#include <string>
#include "Socket.hpp"
#include "TcpServer.hpp"
#include "Protocal.hpp"
#include "Calculate.hpp"

using namespace Protocol;
using namespace CalCulateNS;

// std::string HeadlerRequest(std::string &request, bool *error_code)
// {
//     *error_code = true;
//     Calculate calculte;
//     std::unique_ptr<Factory> factory = std::make_unique<Factory>();
//     auto req = factory->BuildRequest();

//     std::cout << "开始处理请求" << std::endl;
//     std::string inbuffer;
//     if (Decode(request, &inbuffer))
//     { // 如果读取到了，一定是完整报文
//         std::cout << "请求为: " << inbuffer << std::endl;
//         if (req->deserialize(inbuffer))
//         {
//             std::cout << "请求格式错误" << std::endl;
//             // 反序列化失败，不符合协议格式要求
//             *error_code = false;
//             return std::string();
//         }
//         auto resp = calculte.Cal(req); // 处理请求
//         std::string send_string;
//         resp->Serialize(&send_string);
//         std::string resp_send = Encode(send_string);
//         std::cout << "响应为resp: " << resp_send << std::endl;
//         return resp_send;
//     }
//     else
//     {
//         std::cout << "不是完整报文" << std::endl;
//         return std::string();
//     }
// }

std::string HandlerRequest(std::string &inbufferstream, bool *error_code)
{
    *error_code = true;
    // 0. 计算机对象
    Calculate calculte;

    // 1. 构建响应对象
    std::unique_ptr<Factory> factory = std::make_unique<Factory>();
    auto req = factory->BuildRequest();

    // 2. 分析字节流，看是否有一个完整的报文
    std::string total_resp_string;
    std::string message;
    while (Decode(inbufferstream, &message))
    {
        // 3.完整的报文，反序列化！
        if (!req->Deserialize(message))
        {
            std::cout << "Deserialize error" << std::endl;
            *error_code = false;
            return std::string();
        }
        // 4. 业务处理
        auto resp = calculte.Cal(req);
        // 5. 序列化response
        std::string send_string;
        resp->Serialize(&send_string); // "result code"
        // 6. 构建完成的字符串级别的响应报文
        send_string = Encode(send_string);
        // 7. 发送
        total_resp_string += send_string;
    }
    return total_resp_string;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "Usage:\n\t" << argv[0] << "localport" << std::endl;
    }
    uint16_t localport = std::stoi(argv[1]);
    std::unique_ptr<TcpServer> svr(new TcpServer(localport, HandlerRequest));
    svr->Loop();
    return 0;
}

// int main(int argc, char *argv[])
// {
//     uint16_t localport = std::stoi(argv[1]);
//     std::unique_ptr<TcpSokcet> svr = std::make_unique<TcpSokcet>();
//     svr->BUlidListenSockfdMethod(localport);

//     while (true)
//     {
//         std::string cilentip;
//         uint16_t cilentport;
//         Socket *newSock = svr->AcceptSocket(&cilentip, &cilentport);
//         std::string in;
//         newSock->Recv(&in, 1024);
//         std::cout << "Cilent say: " << in << std::endl;
//         newSock->Send(in);
//         newSock->ColseSockfd();
//     }
//     svr->ColseSockfd();
//     return 0;
// }
