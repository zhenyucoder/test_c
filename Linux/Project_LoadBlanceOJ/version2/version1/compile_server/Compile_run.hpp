#pragma once
#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>

#include "Compiler.hpp"
#include "Runner.hpp"
#include "../comm/log.hpp"
#include "../comm/util.hpp"

using namespace ns_compiler;
using namespace ns_runner;
using namespace ns_util;
using namespace ns_log;

namespace ns_compiler_and_run
{
    class CompileAnddRun
    {
    public:
        static void RemoveTempFile(const std::string &file_name)
        {
            // 清理文件的个数是不确定的，但是有哪些我们是知道的
            std::string _src = PathUtil::Src(file_name);
            if (FileUtil::IsFilesExist(_src))
                unlink(_src.c_str());
            std::string _compiler_error = PathUtil::CompileError(file_name);
            if (FileUtil::IsFilesExist(_compiler_error))
                unlink(_compiler_error.c_str());
            std::string _execute = PathUtil::Exe(file_name);
            if (FileUtil::IsFilesExist(_execute))
                unlink(_execute.c_str());
            std::string _stdin = PathUtil::Stdin(file_name);
            if (FileUtil::IsFilesExist(_stdin))
                unlink(_stdin.c_str());
            std::string _stdout = PathUtil::Stdout(file_name);
            if (FileUtil::IsFilesExist(_stdout))
                unlink(_stdout.c_str());
            std::string _stderr = PathUtil::Stderr(file_name);
            if (FileUtil::IsFilesExist(_stderr))
                unlink(_stderr.c_str());
        }

        static std::string CodeToDesc(int code, const std::string &file_name)
        {
            std::string desc;
            switch (code)
            {
            case -1:
                desc = "提交代码为空";
                break;
            case -2:
                desc = "未知错误";
                break;
            case -3: // 编译失败
                FileUtil::ReadFile(PathUtil::CompileError(file_name), &desc, true);
                break;
            case SIGABRT: // 6
                desc = "内存超出限制";
                break;
            case SIGXCPU: // 24
                desc = "时间超出限制";
                break;
            case SIGFPE: // 8
                desc = "浮点数溢出";
                break;
            default:
                desc = "未知: " + std::to_string(code);
            }
            return desc;
        }

        static void Start(const std::string &in_json, std::string *out_json)
        {
            /***************************************
             * in_json(输入):
             *         code: 用户提交的代码
             *         input: 用户提交的自己测试用例，本项目不做处理
             *         cpu_limit: 时间要求
             *         mem_limit: 空间要求
             * out_json(输出):
             *         status: 状态码
             *         result: 请求结果
             *        选填:
             *         stdout: 程序运行的结果
             *         stderr: 程序运行报错原因
             *************************************/

            Json::Value in_value;
            Json::Reader reader;
            reader.parse(in_json, in_value);

            std::string code = in_value["code"].asCString();
            std::string input = in_value["input"].asCString();
            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            std::string file_name; // 唯一文件名
            int run_result = 0;
            int status_code = 0;
            Json::Value out_value;

            if (code.size() == 0)
            {
                // 代码为空
                status_code = -1;
                goto END;
            }

            file_name = FileUtil::UniqueFileName(); // 形成唯一文件名，防止处理多请求时，文件名冲突（毫秒级时间戳 + 原子性递增唯一值）
            if (!FileUtil::WriteFile(PathUtil::Src(file_name), code))
            {
                // 写入失败
                status_code = -2; // 内部错误，一律设为-2
                goto END;
            }

            if (!Compiler::Compile(file_name))
            {
                // 编译失败
                status_code = -3;
                goto END;
            }

            run_result = Runner::Run(file_name, cpu_limit, mem_limit);
            if (run_result < 0)
            {
                // 运行失败，内部错误
                status_code = -2; // 内部错误，一律设为-2
            }
            else if (run_result > 0)
            {
                // 程序运行崩溃了
                status_code = run_result; // 运行代码时返回的是信号
            }
            else
            {
                // 运行成功
                status_code = 0;
            }
        END:
            out_value["status"] = status_code;
            out_value["result"] = CodeToDesc(status_code, file_name);
            if (status_code == 0)
            {
                // 整个过程全部成功
                std::string _stdout;
                _stdout = FileUtil::ReadFile(PathUtil::Stdout(file_name), &_stdout, true);
                out_value["stdout"] = _stdout;

                std::string _stderr;
                _stderr = FileUtil::ReadFile(PathUtil::Stdout(file_name), &_stderr, true);
                out_value["stderr"] = _stderr;
            }

            Json::StyledWriter writer; // Json::FastWriter writer
            *out_json = writer.write(out_value);
            RemoveTempFile(file_name);
        }
    };
}
