#include "Compiler.hpp"
#include "Runner.hpp"

using namespace ns_compiler;
using namespace ns_runner;

int main()
{
    if (!Compiler::Compile("code"))
        return 0;
    int n = Runner::Run("code", 3, 1024 * 1024);
    std::cout << n << std::endl;
    return 0;
}