#pragma once 
#include <iostream>
#include <string>

#include "../comm/util.hpp"

namespace ns_log
{
    using namespace ns_util;
    enum
    {
        INFO,
        DEBUG,
        WARNING,
        ERROR,
        FATAL
    };

    inline std::ostream& Log(const std::string& level, std::string filename, int line)
    {
        //日志等级
        std::string message;
        message += "[";
        message += level;
        message += "]";

        //相关文件信息
        message += "[";
        message += filename;
        message += "]";

        message += "[";
        message += std::to_string(line);
        message += "]";

        // 日志时间戳
        message += "[";
        message += TimeUtil::GetTimeStamp();
        message += "]";
        std::cout << message;
        return std::cout;
    }

    #define LOG(level) Log(#level, __FILE__, __LINE__)
};