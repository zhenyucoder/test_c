#pragma once
#include <iostream>
#include <string>
#include <atomic>
#include <sys/stat.h>
#include <sys/time.h>
#include <fstream>

namespace ns_util
{
    class TimeUtil
    {
    public:
        static std::string GetTimeStamp()
        {
            struct timeval _time;
            gettimeofday(&_time, nullptr);
            return std::to_string(_time.tv_sec);
        }
        // 获得毫秒时间戳
        static std::string GetTimeMs()
        {
            struct timeval _time;
            gettimeofday(&_time, nullptr);
            return std::to_string(_time.tv_sec * 1000 + _time.tv_usec / 1000);
        }
    };

    const std::string temp_path = "./temp/";
    class PathUtil
    {
    public:
        static std::string AddSuffix(const std::string &filename, const std::string suffix)
        {
            std::string pathname = temp_path;
            pathname += filename;
            pathname += suffix;
            return pathname;
        }

    public:
        // 编译时需要有的临时文件
        // 构建源文件路径 + 后缀名
        static std::string Src(const std::string &filename)
        {
            return AddSuffix(filename, ".cc");
        }
        // 构建可执行文件路径 + 后缀名
        static std::string Exe(const std::string &filename)
        {
            return AddSuffix(filename, ".exe");
        }
        // 构建编译报错文件路径 + 后缀名
        static std::string CompileError(const std::string &filename)
        {
            return AddSuffix(filename, ".compile_error");
        }

        // 运行时需要的临时文件
        static std::string Stdin(const std::string &filename)
        {
            return AddSuffix(filename, ".stdin");
        }

        static std::string Stdout(const std::string &filename)
        {
            return AddSuffix(filename, ".stdout");
        }

        static std::string Stderr(const std::string &filename)
        {
            return AddSuffix(filename, ".stderr");
        }
    };

    class FileUtil
    {
    public:
        static bool IsFilesExist(const std::string &pathname)
        {
            struct stat st;
            if (stat(pathname.c_str(), &st) == 0) // 获取文件属性成功，文件存在；或直接粗暴读方式打开文件判断
                return true;
            return false;
        }

        static std::string UniqueFileName()
        {
            static std::atomic_uint id(0);
            id++;
            // 毫秒级时间戳+原子性递增唯一值: 来保证唯一性
            std::string ms = TimeUtil::GetTimeMs();
            std::string uniq_id = std::to_string(id);
            return ms + "_" + uniq_id;
        }

        static bool WriteFile(const std::string &target, const std::string &content)
        {
            std::ofstream out(target);
            if (!out.is_open())
            {
                return false;
            }
            out.write(content.c_str(), content.size());
            out.close();
            return true;
        }

        static bool ReadFile(const std::string &target, std::string *content, bool keep)
        {
            (*content).clear();

            std::ifstream in(target);
            if (!in.is_open())
            {
                return false;
            }
            std::string line;
            // getline:不保存行分割符,有些时候需要保留\n,
            // getline内部重载了强制类型转化
            while (std::getline(in, line))
            {
                (*content) += line;
                (*content) += (keep ? "\n" : "");
            }
            in.close();
            return true;
        }
    };
};