#include <iostream>
#include "UdpServer.hpp"
#include "Comm.hpp"

void Usage(const std::string& proc)
{
    // std::cout << "Usage: \n\t" << proc << " udpserver_ip udpserver_port" << std::endl;
    std::cout << "Usage: \n\t" << proc << " udpserver_port" << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(Usage_Err);
    }
    UdpServer udpserver(std::stoi(argv[1]));
    udpserver.Init();
    udpserver.Start();
    return 0;
}