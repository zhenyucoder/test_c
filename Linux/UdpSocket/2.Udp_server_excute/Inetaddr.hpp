#pragma once
#include <string>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class InetAddr
{
public:
    InetAddr(struct sockaddr_in &addr)
        : _addr(addr)
    {
        _ip = inet_ntoa(addr.sin_addr);
        _port = ntohs(_addr.sin_port);
    }

    std::string Ip()
    {
        return _ip;
    }

    uint16_t Port()
    {
        return _port;
    }

    std::string DebugPrint()
    {
        std::string info;
        info += _ip;
        info += ":";
        info += std::to_string(_port);
        return info;
    }

private:
    std::string _ip;
    uint16_t _port;
    struct sockaddr_in _addr;
};