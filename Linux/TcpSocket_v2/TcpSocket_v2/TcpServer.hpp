#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "nocopy.hpp"
#include "Log.hpp"
#include "Comm.hpp"
#include "Inetaddr.hpp"

static const int defaultbacklog = 5;

class TcpServer;

class ThreadData
{
public:
    ThreadData(int sockfd, TcpServer *ptr, struct sockaddr_in &local)
        : _sockfd(sockfd), _svr_ptr(ptr), _peer(local)
    {
    }

    int SockFd()
    {
        return _sockfd;
    }

    TcpServer *GetServer()
    {
        return _svr_ptr;
    }

private:
    int _sockfd;
    TcpServer *_svr_ptr;

public:
    InetAddr _peer;
};

class TcpServer : public nocopy
{
public:
    TcpServer(const uint16_t port)
        : _port(port), _isrunning(false)
    {
    }

    void Init()
    {
        // 1.创建监听套接字，本质是打开文件，创建文件细节
        _listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (_listensock < 0)
        {
            lg.LogMessage(Fatal, "create socket err, errno code: %d, error string: %s", errno, strerror(errno));
            exit(Socket_Err);
        }
        lg.LogMessage(Debug, "cerate socket success, socket: %d", _listensock);

        // 2. 填充本地网络信息并和监听套接字绑定
        // 2.1 解决一些少量的bind失败的问题
        int opt = 1;
        setsockopt(_listensock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
        // 2.2 填充并绑定
        struct sockaddr_in local;
        memset(&local, 0, sizeof local);
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY;
        int n = bind(_listensock, CONV(&local), sizeof local);
        if (n < 0)
        {
            lg.LogMessage(Fatal, "bind socket err, errno: %d, error string: %s", errno, strerror(errno));
            exit(Bind_Err);
        }
        lg.LogMessage(Debug, "bind socket success, socket: %d", _listensock);

        // 3. 设置为监听状态
        if (listen(_listensock, defaultbacklog) != 0)
        {
            lg.LogMessage(Fatal, "listen socket err, errno: %d, error string: %s", errno, strerror(errno));
            exit(Listen_Err);
        }
        lg.LogMessage(Debug, "Listen socket success, socket: %d", _listensock);
    }

    static void *HandlerRequest(void *args)
    {
        pthread_detach(pthread_self());
        ThreadData *td = static_cast<ThreadData *>(args);
        td->GetServer()->Server(td->SockFd(), td->_peer);
        delete td;
        return nullptr;
    }

    void Start()
    {
        _isrunning = true;
        signal(SIGCHLD, SIG_IGN); // 在Linux环境中，如果对SIG_IGN进行忽略，子进程退出的时候，自动释放自己的资源
        while (_isrunning)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int sockfd = accept(_listensock, (struct sockaddr *)&peer, &len);
            if (sockfd < 0)
            {
                lg.LogMessage(Warning, "accept socket error, errno code: %d, error string: %s", errno, strerror(errno));
                continue;
            }
            lg.LogMessage(Debug, "accept success, get n new sockfd: %d", sockfd);

            /*****************************
             *  v1，最基础功能测试，单进程，一次只能接收一个请求
             * Server(sockfd);
             * close(sockfd);
             ******************************/

            // v2 多进程版
            // v2.1基于进程
            // pid_t id = fork();
            // if(id < 0)
            // {
            //     close(sockfd);
            //     continue;
            // }
            // else if(id == 0)
            // {//子进程
            //     close(_listensock);
            //     if(fork() > 0) exit(0);

            //     Server(sockfd);
            //     close(sockfd);
            //     exit(0);
            // }
            // else
            // {
            //     close(sockfd);
            //     waitpid(id, nullptr, 0);
            // }

            // v2.2基于信号
            // pid_t id = fork();
            // if (id < 0)
            // {
            //     close(sockfd);
            //     continue;
            // }
            // else if (id == 0)
            // { // 子进程
            //     close(_listensock);
            //     Server(sockfd);
            //     close(sockfd);
            //     exit(0);
            // }
            // else
            // {
            //     close(sockfd);
            // }

            // v3 --- 多线程
            // ThreadData *td = new ThreadData(sockfd, this, peer);
            // pthread_t tid;
            // pthread_create(&tid, nullptr, HandlerRequest, td);
        }
    }

    void Server(int socket, InetAddr& peer) // 面向连接，面向字节流。和文件或管道特性一样，所以对于tcp读写最本质的做法就是直接调用read、write
    {
        char buffer[1024];
        while (true)
        {
            ssize_t n = read(socket, buffer, sizeof(buffer) - 1);
            if (n > 0)
            {
                buffer[n] = 0;
                std::cout << "[" << peer.DebugPrint() << "]: " << buffer << std::endl;

                std::string info = "Server echo# ";
                info += buffer;
                write(socket, info.c_str(), info.size());
            }
            else if (n == 0)
            {
                lg.LogMessage(Info, "Cilent quit !!!");
                break;
            }
            else
            {
                lg.LogMessage(Error, "read error, errno code: %d, error string: %s", errno, strerror(errno));
            }
        }
    }

private:
    uint16_t _port;
    int _listensock;
    bool _isrunning;
};