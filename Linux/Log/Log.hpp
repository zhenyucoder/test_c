#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <unistd.h>
#include <cstdarg>
#include <fcntl.h>
#include <sys/stat.h>

enum
{
    Debug = 0,
    Info,
    Warning,
    Error,
    Fatal
};

enum
{
    Screen = 10,
    Onefile,
    Classfile
};

const int defaultStyle = Screen;
const std::string default_filename = "log.";
const std::string logdir = "log";

class Log
{
private:
    std::string LevelToDesc(int level)
    {
        switch (level)
        {
        case Debug:
            return "Debug";
        case Info:
            return "Info";
        case Warning:
            return "Warning";
        case Error:
            return "Error";
        case Fatal:
            return "Fatal";
        default:
            return "Unknown";
        }
    }
    std::string TimeStampExLocalTime()
    {
        time_t currtime = time(nullptr);
        struct tm *curr = localtime(&currtime);
        char buffer[128];
        snprintf(buffer, sizeof(buffer), "%d-%d-%d %d:%d:%d", curr->tm_year + 1900, curr->tm_mon + 1, curr->tm_mday,
                 curr->tm_hour, curr->tm_min, curr->tm_sec);
        return buffer;
    }

public:
    Log() : _style(defaultStyle), _filename(default_filename)
    {
        mkdir(logdir.c_str(), 0775);
    }

    void WriteLogToOneFile(const std::string &logname, const std::string &messageinfo)
    {
        umask(0);
        int fd = open(logname.c_str(), O_CREAT | O_WRONLY | O_APPEND, 0666);
        if (fd < 0)
            return;
        write(fd, messageinfo.c_str(), messageinfo.size());
        close(fd);
    }

    void WriteLogToClassFile(const std::string &levelstr, const std::string &messageinfo)
    {
        std::string logname = logdir;
        logname += "/";
        logname += _filename;
        logname += levelstr;
        WriteLogToOneFile(logname, messageinfo);
    }

    void WriteLog(const std::string &levelstr, const std::string &messageinfo)
    {
        switch (_style)
        {
        case Screen:
            std::cout << messageinfo;
            break;
        case Onefile:
            WriteLogToClassFile("all", messageinfo);
            break;
        case Classfile:
            WriteLogToClassFile(levelstr, messageinfo);
            break;
        default:
            WriteLogToClassFile("unknow", messageinfo);
            break;
        }
    }

    void LogMessage(int level, const char *format, ...)
    {
        char leftbuffer[1024];
        std::string levelstr = LevelToDesc(level);
        std::string currtime = TimeStampExLocalTime();
        std::string id = std::to_string(getpid());
        snprintf(leftbuffer, sizeof(leftbuffer), "[%s][%s][%s]", levelstr.c_str(), currtime.c_str(), id.c_str());

        char rightbuffer[1024];
        va_list ap;
        va_start(ap, format);
        vsnprintf(rightbuffer, sizeof(rightbuffer), format, ap);
        va_end(ap);

        std::string messageinfo = leftbuffer;
        messageinfo += rightbuffer;
        WriteLog(levelstr, messageinfo);
    }

    void Enable(int style)
    {
        _style = style;
    }

private:
    int _style;
    std::string _filename;
};

Log lg;

class Conf
{
public:
    Conf()
    {
        lg.Enable(Screen);
    }
    ~Conf()
    {
    }
};

Conf conf;
