#include "Log.hpp"

int main()
{
    Log log;
    log.Enable(Classfile);
    log.LogMessage(Debug, " %s\n", "this is test codeing");
    log.LogMessage(Info, " %s\n", "this is test codeing");
    log.LogMessage(Warning, " %s\n", "this is test codeing");
    log.LogMessage(Error, " %s\n", "this is test codeing");
    log.LogMessage(Fatal, " %s\n", "this is test codeing");
    return 0;
}