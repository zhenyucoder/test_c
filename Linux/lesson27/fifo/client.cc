#include <iostream>
#include <cstring>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>
#include "comm.chh"


int main()
{
    int wfd = open(FILENAME, O_WRONLY);
    if(wfd < 0)
    {
        std::cerr << "errno: " << errno << "errstring: " << strerror(errno) << std::endl;
        return 1;
    }
    std::cout << "open fifo success...write" << std::endl;

    std::string message;
    while(true)
    {
        std::cout << "Please Entry: ";
        getline(std::cin, message);
        ssize_t s = write(wfd, message.c_str(), message.size());
        if(s < 0)
        {
            std::cerr << "errno: " << errno << "errstring: " << strerror(errno) << std::endl;
            break;
        }
    }

    sleep(2);
    close(wfd);
    std::cout << "close fifo success" << std::endl;
    return 0;
}