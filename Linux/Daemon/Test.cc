#include "Daemon.hpp"
#include <unistd.h>

int main()
{
    // 变成守护进程
    Daemon(true, false);

    while(true)
    {
        sleep(1);
    }

    return 0;
}