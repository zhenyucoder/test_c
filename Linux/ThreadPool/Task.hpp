#include <iostream>

enum
{
    ok = 0,
    div_zero,
    mod_zero,
    unkonw
};

const std::string opers = "+-*/%)(&";

class Task
{
public:
    Task(int x = 0, int y = 0, char op = '+')
        : _x(x), _y(y), _op(op), _result(0), _code(ok)
    {
    }
    void Run()
    {
        switch (_op)
        {
        case '+':
            _result = _x + _y;
            break;
        case '-':
            _result = _x - _y;
            break;
        case '*':
            _result = _x * _y;
            break;
        case '/':
            if (_y == 0)
                _code = div_zero;
            else
                _result = _x / _y;
            break;
        case '%':
            if (_y == 0)
                _code = mod_zero;
            else
                _result = _x % _y;
            break;
        default:
            _code = unkonw;
            break;
        }
    }

    void operator()()
    {
        Run();
    }

    std::string PrintTask()
    {
        std::string s;
        s = std::to_string(_x);
        s += _op;
        s += std::to_string(_y);
        s += "=?";

        return s;
    }
    std::string PrintResult()
    {
        std::string s;
        s = std::to_string(_x);
        s += _op;
        s += std::to_string(_y);
        s += "=";
        s += std::to_string(_result);
        s += " [";
        s += std::to_string(_code);
        s += "]";

        return s;
    }

private:
    int _x;
    int _y;
    char _op;
    int _result;
    int _code; // 非0表示结果不可信
};