#include "ThreadPool.hpp"
#include "Task.hpp"
#include <cstdlib>
#include <ctime>
#include <memory>

int main()
{
    // std::unique_ptr<ThreadPool<Task>> tp(new ThreadPool<Task>());
    ThreadPool<Task>::GetInstance()->Start();
    // tp->Start();
    srand((uint64_t)time(nullptr) ^ getpid());

    while (true)
    {
        int x = rand() % 100 + 1;
        usleep(1000);
        int y = rand() % 100 + 1;
        usleep(1000);
        char op = opers[rand() % opers.size()];
        Task t(x, y, op);
        ThreadPool<Task>::GetInstance()->Push(t);
        sleep(1);
    }

    ThreadPool<Task>::GetInstance()->ThreadWait();
    return 0;
}