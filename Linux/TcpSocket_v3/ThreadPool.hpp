#pragma once
#include <iostream>
#include <queue>
#include <vector>
#include <pthread.h>
#include <string>
#include "LockGuard.hpp"
#include "Thread.hpp"
#include "Log.hpp"

const int default_threadnum = 5;
namespace ThreadNs
{
    class ThreadData
    {
    public:
        ThreadData(const std::string &threadname)
            : _threadname(threadname)
        {
        }
        ~ThreadData()
        {
        }

    public:
        std::string _threadname;
    };

    template <class T>
    class ThreadPool
    {
    private:
        ThreadPool(int thread_num = default_threadnum)
            : _thread_num(thread_num)
        {
            pthread_mutex_init(&_mutex, nullptr);
            pthread_cond_init(&_cond, nullptr);

            for (int i = 0; i < _thread_num; i++)
            {
                std::string threadname = "thread-";
                threadname += std::to_string(i + 1);

                ThreadData td(threadname);
                Thread<ThreadData> t(threadname,
                                     std::bind(&ThreadPool<T>::ThreadRun, this, std::placeholders::_1), td);
                _threads.push_back(t);
                // _threads.emplace_back(threadname,
                //                       std::bind(&ThreadPool<T>::ThreadRun, this, std::placeholders::_1),
                //                       td);
                lg.LogMessage(Info, "%s is created ...\n", threadname.c_str());
            }
        }

    public:
        static ThreadPool *GetInstall()
        {
            if (instance == nullptr)
            {
                LockGuard lockguard(&lock_sig);
                if (instance == nullptr)
                {
                    instance = new ThreadPool<T>();
                }
            }

            return instance;
        }

        void ThreadRun(ThreadData &td)
        {
            while (true)
            {
                // 获取任务
                T t;
                {
                    LockGuard lockguard(&_mutex);
                    while (_q.empty())
                    {
                        ThreadWait(td);
                        lg.LogMessage(Debug, "%s wakeup...\n", td._threadname.c_str());
                    }

                    t = _q.front();
                    _q.pop();
                }

                // 执行任务
                // std::cout << t << std::endl;
                t();

                // lg.LogMessage(Debug, "%s headler task %s done,  result is : %s\n",
                            //   td._threadname.c_str(), t.PrintTask().c_str(), t.PrintResult().c_str());
            }
        }

        void Start()
        {
            for (auto &thread : _threads)
            {
                thread.Start();
                lg.LogMessage(Debug, "%s is start...\n", thread.GetThreadName().c_str());
            }
        }

        void ThreadWait(ThreadData &td)
        {
            lg.LogMessage(Debug, "no task, %s is sleeping...\n", td._threadname.c_str());
            pthread_cond_wait(&_cond, &_mutex);
        }

        void ThreadWakeup()
        {
            pthread_cond_signal(&_cond);
        }

        void Push(T &in)
        {
            // lg.LogMessage(Debug, "other thread push a task, task is : %s\n", in.PrintTask().c_str());
            LockGuard lockguard(&_mutex);
            _q.push(in);
            ThreadWakeup();
        }

        ~ThreadPool()
        {
            pthread_mutex_destroy(&_mutex);
            pthread_cond_destroy(&_cond);
        }

    private:
        std::queue<T> _q;
        std::vector<Thread<ThreadData>> _threads;
        int _thread_num;
        pthread_mutex_t _mutex;
        pthread_cond_t _cond;

    public:
        // 单例模式添加
        static ThreadPool<T> *instance;
        static pthread_mutex_t lock_sig;
    };

    template <class T>
    ThreadPool<T> *ThreadPool<T>::instance = nullptr;

    template <class T>
    pthread_mutex_t ThreadPool<T>::lock_sig = PTHREAD_MUTEX_INITIALIZER;
}