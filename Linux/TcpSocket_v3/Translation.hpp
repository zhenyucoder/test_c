#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include "Log.hpp"

const std::string unknown = "unknown";
const std::string mydict = "./recource/dict.txt";
const std::string sep = " ";

class Translate
{
public:
    Translate()
    {
        LoadDict();
        Parse();
    }

    void LoadDict()
    {
        std::ifstream in(_dict_path);
        std::string line;
        while (std::getline(in, line))
        {
            lines.push_back(line);
        }
        in.close();
        lg.LogMessage(Debug, "Load dict txt success, path: %s\n", _dict_path.c_str());
    }

    void Parse()
    {
        for(auto &line : lines)
        {
            auto pos = line.find(sep);
            if(pos == std::string::npos) continue;
            else
            {
                std::string word = line.substr(0, pos);
                std::string chinese = line.substr(pos+sep.size());
                _dict.insert(std::make_pair(word, chinese));
            }
        }
        lg.LogMessage(Debug, "Parse dict txt success, path: %s\n", _dict_path.c_str());
    }

    void debug()
    {
        for(auto &elem : _dict)
        {
            std::cout << elem.first << " : " << elem.second << std::endl;
        }
    }

    std::string Excute(const std::string &word)
    {
        auto iter = _dict.find(word);
        if (iter == _dict.end())
            return unknown;
        else
            return _dict[word];
    }
    ~Translate()
    {
    }

private:
    std::string _dict_path;
    std::unordered_map<std::string, std::string> _dict;
    std::vector<std::string> lines;
};