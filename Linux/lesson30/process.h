#pragma once 
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define SIZE 101
#define MAXSIZE 100
#define STYLE '#'
#define STIME 1000*30

#define STYLE_BODY '='
#define STYLE_HEADER '>'

extern void Process_v1();
extern void Process_v2(int);
extern void Process_v3(double);


// typedef void (*callback_t)(int);
typedef void (*callback_t)(double);
