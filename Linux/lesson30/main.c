#include "process.h"
#define TARGET_SIZE 1024 * 1024//下载任务总大小
#define DOWNLOAD_SIZE 1024 * 10//模拟每次下载时，下载文件的大小

void DownLoad(callback_t cb)
{
    int target = TARGET_SIZE;
    int total = 0;
    int testcnt = 50;
    while(total <= target)
    {
        usleep(STIME);//模拟下载任务时所花费的时间
        total += DOWNLOAD_SIZE;
        double rate = total*100.0/target;
        // if(rate >= 50.0 && testcnt)
        // {
        //     total = target / 2;
        //     testcnt--;
        // }
        cb(rate);
    }
    if(total >= target)
    {
        cb(100.0);
    }
    printf("\n");
}

int main()
{
    DownLoad(Process_v3);
    return 0;
}