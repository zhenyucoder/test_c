#include "process.h"
const char *sstr= "|\\-/";



//version1
void Process_v1()
{
    int rate = 0;
    char bar[SIZE];
    memset(bar, 0, SIZE);
    int num = strlen(sstr);

    while(rate <= MAXSIZE)
    {
        printf("[%-100s][%d%%][%c]\r", bar, rate, sstr[rate % num]);
        fflush(stdout);
        usleep(STIME);
        bar[rate++] = STYLE;
    }
    printf("\n");
}


//version2
void Process_v2(int rate)
{
    static char bar[SIZE] = {0};
    int num = strlen(sstr);

    if(rate <= MAXSIZE)
    {
        printf("[%-100s][%d%%][%c]\r", bar, rate, sstr[rate % num]);
        fflush(stdout);
        bar[rate++] = STYLE;
    }
    else if(rate > MAXSIZE)
        memset(bar, 0, SIZE);
}


//version3
void Process_v3(double rate)
{
    static char bar[SIZE] = {0};
    int num = strlen(sstr);
    static int cnt = 0;
    if(rate <= MAXSIZE)
    {
        cnt++;
        cnt = (cnt >= 4 ? 0 : cnt);
        printf("[%-100s][%.1f%%][%c]\r", bar, rate, sstr[cnt]);
        fflush(stdout);
        if(rate < MAXSIZE)
        {
            bar[(int)rate] = STYLE_BODY;
            bar[(int)rate + 1] = STYLE_HEADER;
        }
        else
        {
            bar[(int)rate] = STYLE_BODY;
            printf("[%-100s][%.1f%%][%c]\r", bar, rate, sstr[cnt]);
        }
    }
}
