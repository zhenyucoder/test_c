#pragma once 

#define SIZE 4096
#define FLUSH_NONE 1
#define FLUSH_LINE (1<<1)
#define FLUSH_ALL  (1<<2)



typedef struct _myFILE
{
    char buffer[SIZE];//缓冲区
    int end;//简化有效空间范围，从0开始
    int flag;//标志位，刷新策略
    int fileno;
}myFILE;

extern myFILE *my_fopen(const char *path, const char *mode);
extern int my_fwrite(const char *s, int num, myFILE *stream);
extern int my_fflush(myFILE *stream);
extern int my_fclose(myFILE *fp);
