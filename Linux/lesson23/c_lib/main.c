#include "mystdio.h"
#include <string.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
    myFILE *fp = my_fopen("log.txt", "w");
    if(fp == NULL)
    {
        perror("my_fopen");
        return 1;
    }

   const char *s = "this is test for stdio lib\n";
   int cnt = 10;
   while(cnt--)
   {
        my_fwrite(s, strlen(s), fp);
        sleep(1);
   }
    my_fclose(fp);
    return 0;
}
