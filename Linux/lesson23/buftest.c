#include <stdio.h>
#include <unistd.h>
#include <string.h>


int main()
{
    fprintf(stdout, "C: hello fprintf\n");
    printf("C: hello printf\n");
    fputs("C: hello fputs\n", stdout);
    const char* buf = "system call: hello write\n";
    write(1, buf, strlen(buf));
    fork();
    return 0;
}


