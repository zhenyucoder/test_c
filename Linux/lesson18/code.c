 #include <stdio.h>
 #include <stdlib.h>
 #include <unistd.h>

#define N 10
typedef void (*callback_bt)();

void createsubProcess(int n, callback_bt bt)
{
    int i = 0;
    for(; i < n; i++)
    {
        sleep(1);
        pid_t id = fork();
        if(id == 0)
        {
            //child
            printf("create chilld process sucess: %d\n", i);
            bt();
            exit(0);
        }
    }
}

void worker() 
{
    int cnt = 10;
    for(; cnt >= 0; cnt--)
    {
        printf("I am child process: pid:%d Ppid%d cnt:%d\n", getpid(), getppid(), cnt);
        sleep(1);
    }
}

int main()
{
    createsubProcess(N, worker);
    sleep(100);
    return 0;
}

////int global_val = 100;    
//int main()    
//{    
//    pid_t id = fork();    
//    if(id == 0)    
//    {    
//       //child    
//       int cnt = 3;                                                                                                                                      
//       while(1)    
//       {    
//           printf("child Pid:%d Ppid:%d g_val:%d &g_val:%p\n", getpid(), getppid(), global_val, &global_val);    
//           if(--:cnt == 0)    
//           {    
//               global_val = 200;    
//               printf("child change g_val 100 -> 200\n");    
//           }    
//           sleep(1);    
//       }    
//    }    
//    else if(id > 0)    
//    {    
//        //pather          
//       while(1)    
//       {    
//           printf("father Pid:%d Ppid:%d g_val:%d &g_val:%p\n", getpid(), getppid(), global_val, &global_val);    
//           sleep(1);    
//       }    
//    }    
//    return 0; 
//}



// int un_global_val;
// int Init_global_val = 100;
// int main(int argc, char *argv[], char * env[])
// {
// 	printf("code addr: %p\n", main); //代码区
// 	
//     const char *str = "hello Linux";//字符常量区
//     printf("read only char add: %p\n", str);
//     printf("Init global value add: %p\n", &Init_global_val);//全局初始区
//     printf("uninit global value add: %p\n", &un_global_val);//全局未初始区
// 
//     char* heap1 = (char*)malloc(100);
//     char* heap2 = (char*)malloc(100);
//     char* heap3 = (char*)malloc(100);
//     char* heap4 = (char*)malloc(100);
//     
//     //堆及地址增长方向
//     printf("heap1 add: %p\n", heap1);
//     printf("heap2 add: %p\n", heap2);
//     printf("heap3 add: %p\n", heap3);
//     printf("heap4 add: %p\n", heap4);
//     //堆及地址增长方向
//     printf("stack1 add: %p\n", &heap1);                                                                                                                
//     printf("stack2 add: %p\n", &heap2);
//     printf("stack3 add: %p\n", &heap3);
//     printf("stack4 add: %p\n", &heap4);
//     
//     int i = 0;//命令行参数
//     for(; argv[i]; i++)
//     {
//         printf("argv[%d]: %p\n",i, argv[i]);
//     }
//     
//     i = 0;//环境变量
//     for(; i < 2; i++)
//     {
//         printf("env[%d]: %p\n",i, env[i]);
//     }
//     return 0;
// }
