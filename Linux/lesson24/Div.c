#include "Div.h"

int Div(int x, int y, int* flag)
{
    *flag = 0;
    if(y == 0)
    {
        *flag = -1;
        return -1;
    }
    return x / y;
}
