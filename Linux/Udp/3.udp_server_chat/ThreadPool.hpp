#include <iostream>
#include "Thread.hpp"
#include "LockGuard.hpp"
#include "Log.hpp"
#include <queue>
#include <vector>
#include <pthread.h>
#include <functional>

namespace ThreadPoolNS
{
    const int default_thread_num = 5;

    class ThreadData
    {
    public:
        ThreadData(const std::string &name) : threadname(name)
        {
        }
        ~ThreadData()
        {
        }

    public:
        std::string threadname;
    };

    template <class T>
    class ThreadPool
    {
    private:
        ThreadPool(int thread_num = default_thread_num) : _thread_num(thread_num)
        {
            pthread_mutex_init(&_mutex, nullptr);
            pthread_cond_init(&_cond, nullptr);
            for (int i = 0; i < _thread_num; i++)
            {
                std::string threadname = "thread-";
                threadname += std::to_string(i + 1);
                ThreadData td(threadname);
                _threads.emplace_back(threadname, std::bind(&ThreadPool<T>::ThreadRun, this, std::placeholders::_1), td);
                lg.LogMessage(Info, "%s is created...\n", threadname.c_str());
            }
        }
        ThreadPool(const ThreadPool<T> &) = delete;
        ThreadPool<T> operator=(const ThreadPool<T> &) = delete;

    public:
        static ThreadPool<T> *GetInstance()
        {
            if (instance == nullptr) // 防止线程疯狂加锁、解锁检测install是否为空，浪费锁资源
            {
                LockGuard lockguard(&sigmutex);
                if (instance == nullptr)
                {
                    lg.LogMessage(Info, "创建单例成功...\n");
                    instance = new ThreadPool<T>();
                }
            }
            return instance;
        }
        void ThreadRun(ThreadData &td)
        {
            while (true)
            {
                T t;
                {
                    LockGuard lockguard(&_mutex);
                    while (_q.empty())
                    {
                        pthread_cond_wait(&_cond, &_mutex);
                        lg.LogMessage(Debug, "%s is wakeup\n", td.threadname.c_str());
                    }
                    t = _q.front();
                    _q.pop();
                }
                t();
                // lg.LogMessage(Debug, "%s handler task %s done, result is : %s\n",
                //               td.threadname.c_str(), t.PrintTask().c_str(), t.PrintResult().c_str());
            }
        }

        void Start()
        {
            for (auto &thread : _threads)
            {
                thread.Start();
                lg.LogMessage(Info, "%s is running...\n", thread.ThreadName().c_str());
            }
        }

        void Push(T &in)
        {
            // lg.LogMessage(Debug, "other thread push a task, task is : %s\n", in.PrintTask().c_str());
            LockGuard lockguard(&_mutex);
            _q.push(in);
            ThreadWakeup();
        }

        void ThreadWait()
        {
            for (auto &thread : _threads)
            {
                thread.Join();
            }
        }

        ~ThreadPool()
        {
            pthread_mutex_destroy(&_mutex);
            pthread_cond_destroy(&_cond);
        }

        void ThreadWakeup()
        {
            pthread_cond_signal(&_cond);
        }

    private:
        std::queue<T> _q;
        std::vector<Thread<ThreadData>> _threads;
        int _thread_num;
        pthread_mutex_t _mutex;
        pthread_cond_t _cond;
        static ThreadPool<T> *instance;
        static pthread_mutex_t sigmutex;
    };

    template <class T>
    ThreadPool<T> *ThreadPool<T>::instance = nullptr;
    template <class T>
    pthread_mutex_t ThreadPool<T>::sigmutex = PTHREAD_MUTEX_INITIALIZER;
}