#include "UdpServer.hpp"
#include <iostream>
#include <memory>
#include <cstdio>

void Usage(const std::string proc)
{
    std::cout << "Usage:\n\t" << proc << " serverip" << std::endl;
}

// std::string OnMessageDefault(std::string request)
// {
//     return request + "[haha, got you!!]";
// }

// std::string ExcuteCommand(std::string command)
// {
//     std::cout << "get a command: " << command << std::endl;
//     FILE *fp = popen(command.c_str(), "r");
//     if (fp == nullptr)
//     {
//         return "execute error, reason is unknown";
//     }
//     std::string response;
//     char buffer[1024];
//     while (true)
//     {
//         char *s = fgets(buffer, sizeof(buffer), fp);
//         if (s == nullptr)
//             break;
//         else
//             response += buffer;
//     }
//     pclose(fp);
//     return response;
// }

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(Usage_Err);
    }
    uint16_t port = std::stoi(argv[1]);
    std::unique_ptr<UdpServer> usvr = std::make_unique<UdpServer>(port);
    usvr->Init();
    usvr->Start();
    return 0;
}