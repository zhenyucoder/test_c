#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <vector>
#include <pthread.h>

#include "Comm.hpp"
#include "Log.hpp"
#include "InetAddr.hpp"
#include "nocopy.hpp"
#include "Thread.hpp"
#include "LockGuard.hpp"
#include "ThreadPool.hpp"

const uint16_t defaultPort = 8888;
const int defautbuffersize = 1024;
using task_t = std::function<void()>;

class UdpServer : public nocopy
{
public:
    UdpServer(uint16_t port = defaultPort) : _port(port)
    {
        pthread_mutex_init(&_mutex, nullptr);
    }

    void Init()
    {
        // 1.创建文件细节，相当于打开网卡文件
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            lg.LogMessage(Fatal, "socket err: %d, %s\n", errno, strerror(errno));
            exit(Socket_Err);
        }
        lg.LogMessage(Info, "create socket success, sockfd: %d\n", _sockfd);

        // 2. 文件绑定网络相关信息
        struct sockaddr_in local;
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        // local.sin_addr = inet_addr(serverip); // 不推荐
        local.sin_addr.s_addr = INADDR_ANY;
        socklen_t len = sizeof(local);

        // 2.1 将填充好的字段设进内核
        int n = ::bind(_sockfd, (struct sockaddr *)&local, len);
        if (n < 0)
        {
            lg.LogMessage(Fatal, "bind socket err: %d, %s\n", errno, strerror(errno));
            exit(Bind_Err);
        }
        lg.LogMessage(Info, "bind socket success...\n");

        // 启动线程池
        ThreadPoolNS::ThreadPool<task_t>::GetInstance()->Start();
    }

    void AddOnlineUser(InetAddr addr)
    {
        LockGuard lockguard(&_mutex);
        for (auto &user : _online_user)
        {
            if (user == addr)
                return;
        }
        _online_user.push_back(addr);
        lg.LogMessage(Debug, "%s:%d is add to onlineuser list...\n", addr.Ip().c_str(), addr.Port());
    }

    void Route(int sockfd, std::string message)
    {
        LockGuard lockguard(&_mutex);
        for (auto &user : _online_user)
        {
            sendto(sockfd, message.c_str(), message.size(), 0, (sockaddr *)&user.GetAddr(), sizeof(user.GetAddr()));
            lg.LogMessage(Debug, "server send message to %s:%d, message: %s\n", user.Ip().c_str(), user.Port(), message.c_str());
        }
    }

    void Start()
    {
        char buffer[defautbuffersize];
        for (;;)
        {
            struct sockaddr_in peer; // 远端
            socklen_t peerlen = sizeof(peer);
            // 以阻塞方式收消息，并获取客户端peer信息
            ssize_t n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)(&peer), &peerlen);
            if (n > 0)
            {
                InetAddr addr(peer);
                AddOnlineUser(peer); // 将客户添加到用户在线列表

                buffer[n] = 0;
                std::string message = "[";
                message += addr.Ip();
                message += ":";
                message += std::to_string(addr.Port());
                message += "] ";
                message += buffer;

                task_t t = std::bind(&UdpServer::Route, this, _sockfd, message);
                ThreadPoolNS::ThreadPool<task_t>::GetInstance()->Push(t);
            }
        }
    }
    ~UdpServer()
    {
        pthread_mutex_destroy(&_mutex);
    }

private:
    uint16_t _port;
    int _sockfd;
    std::vector<InetAddr> _online_user;
    pthread_mutex_t _mutex;
};