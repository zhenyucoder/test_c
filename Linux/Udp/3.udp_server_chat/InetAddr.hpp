#pragma once
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>

class InetAddr
{
public:
    InetAddr(struct sockaddr_in &addr) : _addr(addr)
    {
        _port = ntohs(addr.sin_port);
        _ip = inet_ntoa(addr.sin_addr);
    }

    std::string Ip()
    {
        return _ip;
    }
    uint16_t Port()
    {
        return _port;
    }

    std::string PrintDebug()
    {
        std::string info = _ip;
        info += ":";
        info += std::to_string(_port);
        return info;
    }
    bool operator==(InetAddr addr)
    {
        return addr.Ip() == _ip && addr.Port() == _port;
    }

    const struct sockaddr_in &GetAddr()
    {
        return _addr;
    }
    ~InetAddr()
    {
    }

private:
    struct sockaddr_in _addr;
    std::string _ip;
    uint16_t _port;
};