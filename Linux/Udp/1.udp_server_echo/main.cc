#include "UdpServer.hpp"
#include <iostream>
#include <memory>

void Usage(const std::string proc)
{
    std::cout << "Usage:\n\t" << proc << "serverip" << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(Usage_Err);
    }
    uint16_t port = std::stoi(argv[1]);
    std::unique_ptr<UdpServer> usvr = std::make_unique<UdpServer>(port);
    usvr->Init();
    usvr->Start();
    return 0;
}