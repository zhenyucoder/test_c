#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>
#include <cstdlib>
#include <cstring>
#include "Comm.hpp"
#include "Log.hpp"
#include "InetAddr.hpp"
#include "nocopy.hpp"

const uint16_t defaultPort = 8888;
const int defautbuffersize = 1024;

class UdpServer : public nocopy
{
public:
    UdpServer(uint16_t port = defaultPort) : _port(port)
    {
    }

    void Init()
    {
        // 1.创建文件细节，相当于打开网卡文件
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            lg.LogMessage(Fatal, "socket err: %d, %s\n", errno, strerror(errno));
            exit(Socket_Err);
        }
        lg.LogMessage(Info, "create socket success, sockfd: %d\n", _sockfd);

        // 2. 文件绑定网络相关信息
        struct sockaddr_in local;
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        // local.sin_addr = inet_addr(serverip); // 不推荐
        local.sin_addr.s_addr = INADDR_ANY;
        socklen_t len = sizeof(local);

        // 2.1 将填充好的字段设进内核
        int n = bind(_sockfd, (struct sockaddr *)&local, len);
        if (n < 0)
        {
            lg.LogMessage(Fatal, "bind socket err: %d, %s\n", errno, strerror(errno));
            exit(Bind_Err);
        }
        lg.LogMessage(Info, "bind socket success...\n");
    }

    void Start()
    {
        char buffer[defautbuffersize];
        for (;;)
        {
            struct sockaddr_in peer; // 远端
            socklen_t peerlen = sizeof(peer);
            // 以阻塞方式收消息，并获取客户端peer信息
            ssize_t n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)(&peer), &peerlen);
            if (n > 0)
            {
                InetAddr addr(peer);

                buffer[n] = 0;
                std::cout << "[" << addr.PrintDebug() << "]# " << buffer << std::endl;
                std::string response = buffer;
                response += "[haha, got you!!!]";
                int m = sendto(_sockfd, response.c_str(), response.size(), 0, (struct sockaddr *)&peer, peerlen); // 向peer发消息
                if (m < 0)
                    break;
            }
            else
                break;
        }
    }
    ~UdpServer()
    {
    }

private:
    uint16_t _port;
    int _sockfd;
};