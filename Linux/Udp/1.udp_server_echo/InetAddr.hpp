#pragma once
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string>

class InetAddr
{
public:
    InetAddr(struct sockaddr_in &addr)
    {
        _ip = ntohs(addr.sin_port);
        _ip = inet_ntoa(addr.sin_addr);
    }

    std::string Ip()
    {
        return _ip;
    }
    uint16_t Port()
    {
        return _port;
    }

    std::string PrintDebug()
    {
        std::string info = _ip;
        info += ":";
        info += std::to_string(_port);
        return info;
    }

    ~InetAddr()
    {
    }

private:
    struct sockaddr_in _addr;
    std::string _ip;
    uint16_t _port;
};