#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

//const char* argv[] = {
//    "ls",
//    "-l"
//};

 
int main()
{
    char* const myenv[] = {
        "MYVAL1=1111111111111111111",
        "MYVAL2=1111111111111111111",
        "MYVAL3=1111111111111111111",
        "MYVAL4=1111111111111111111",
        NULL
    };
    //char*  env_val = "MYVAL5=444444444444";
    //putenv(env_val);
    pid_t id = fork();
    if(id == 0)
    {
        printf("pid:%d, exec command begin!\n", getpid());
        execle("./mytest", "test", "-1", "-a", NULL, myenv);
        //execl("./mytest", "mytest", NULL);
        //execv("/usr/bin/ls", argv);
        //execlp("pwd", "pwd", NULL);
        //execl("/usr/bin/ls", "ls", "-a", "-l", NULL);
        //execl("/usr/bin/top", "top", NULL);
        printf("pid:%d, exec command end!\n", getpid());
        exit(1);//替换失败
    }
    else{
        //parent
        int status = 0;
        pid_t rid = waitpid(id, &status, 0);        
        if(WIFEXITED(status))
        {
            printf("wait sucess, pid:%d, rid:%d, exit code:%d\n",getpid(), rid, WEXITSTATUS(status));
        }
    }
    return 0;
}
