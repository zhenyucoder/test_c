#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <atomic>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <boost/algorithm/string.hpp>

namespace ns_util
{
    class TimeUtil
    {
    public:
        static std::string GetTimeStamp()
        {
            struct timeval tv;
            gettimeofday(&tv, nullptr);
            return std::to_string(tv.tv_sec);
        }

        static std::string GetTimeMs() // 毫秒级时间戳
        {
            struct timeval tv;
            gettimeofday(&tv, nullptr);
            return std::to_string(tv.tv_sec * 1000 + tv.tv_usec / 1000);
        }
    };

    const std::string temp = "./temp/";
    class PathUtil
    {
        static std::string AddSuffix(const std::string &filename, const std::string &suffix)
        {
            std::string pathname = temp;
            pathname += filename;
            pathname += suffix;
            return pathname;
        }

    public:
        // 构建源文件路径 + 后缀的完整文件名
        // 编译时需要有的临时文件
        static std::string Src(const std::string &filename)
        {
            return AddSuffix(filename, ".cpp");
        }
        static std::string Exe(const std::string &filename)
        {
            return AddSuffix(filename, ".exe");
        }
        static std::string CompileError(const std::string &filename)
        {
            return AddSuffix(filename, ".compile_error");
        }

        // 构建运行时现需要的临时文件，输入、输出、错误
        static std::string Stdin(const std::string &filename)
        {
            return AddSuffix(filename, ".stdin");
        }
        static std::string Stdout(const std::string &filename)
        {
            return AddSuffix(filename, ".stdout");
        }
        static std::string Stderr(const std::string &filename)
        {
            return AddSuffix(filename, ".stderr");
        }
    };

    class FileUtil
    {
    public:
        static bool IsFileExists(const std::string &pathname)
        {
            struct stat st;
            if (stat(pathname.c_str(), &st) == 0)
                return true;
            return false;
        }

        static std::string UniqFileName()
        {
            // 毫秒级时间戳 + 原子性唯一递增值
            static std::atomic_uint id(0);
            id++;
            // 毫秒级时间戳+原子性递增唯一值: 来保证唯一性
            std::string ms = TimeUtil::GetTimeMs();
            std::string uniq_id = std::to_string(id);
            return ms + "_" + uniq_id;
        }

        static bool WriteFile(const std::string &targetFile, const std::string &content)
        {
            std::ofstream out(targetFile);
            if (!out.is_open())
                return false;

            out.write(content.c_str(), content.size());
            out.close();
            return true;
        }

        static bool ReadFile(const std::string &targetFile, std::string *content, bool keep = false)
        {
            std::ifstream in(targetFile);
            if (!in.is_open())
            {
                return false;
            }

            std::string line;
            while (std::getline(in, line))
            {
                *content += line;
                *content += (keep ? "\n" : "");
            }

            in.close();
            return false;
        }
    };

    class StringUtil
    {
    public:
        static void SplitString(std::string str, std::vector<std::string> *target, std::string Sep)
        {
            // boost::is_any_of, 任何出现在Sep中的字符都被当做分割符
            boost::split(*target, str, boost::is_any_of(Sep), boost::algorithm::token_compress_on);
        }
    };
}