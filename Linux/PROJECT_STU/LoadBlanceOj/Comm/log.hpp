#pragma once
#include <iostream>
#include <string>
#include "../Comm/util.hpp"

namespace ns_log
{
    using namespace ns_util;

    enum
    {
        INFO,
        GEBUG,
        WARNINR,
        ERROR,
        FATLE
    };

    inline std::ostream &Log(const std::string &level, const std::string &filename, int line)
    {
        std::string message;

        // 添加日志等级
        message += "[";
        message += level;
        message += "]";

        // 报错文件名
        message += "[";
        message += filename;
        message += "]";

        // 报错行信息
        message += "[";
        message += line;
        message += "]";

        // 日志时间戳
        message += "[";
        message += TimeUtil::GetTimeStamp();
        message += "]";

        std::cout << message;
        return std::cout;
    }

#define LOG(level) Log(#level, __FILE__, __LINE__)
}