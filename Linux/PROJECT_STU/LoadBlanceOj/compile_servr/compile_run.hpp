#pragma once
#include "compiler.hpp"
#include "runner.hpp"
#include "../Comm/log.hpp"
#include "../Comm/util.hpp"

#include <signal.h>
#include <unistd.h>
#include <jsoncpp/json/json.h>

namespace ns_compile_and_run
{
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_compiler;
    using namespace ns_runner;

    class CompileAndRun
    {
    public:
        static std::string CodeToDesc(int code, const std::string &filename)
        {
            std::string desc;
            switch (code)
            {
            case 0:
                desc = "编译运行成功";
                break;
            case -1:
                desc = "用户提交的代码为空";
                break;
            case -2:
                desc = "未知错误"; // 内部错误
                break;
            case -3:
                FileUtil::ReadFile(PathUtil::CompileError(filename), &desc, true);
                break;
            case 6:
                desc = "内存超出限制";
                break;
            case 24:
                desc = "时间超出限制";
            case SIGFPE:
                desc = "浮点数异常";
            default:
                desc = desc = "未知: " + std::to_string(code);
            }
            return desc;
        }

        static void RemoveTempFile(const std::string &filename)
        {
            std::string _src = PathUtil::Src(filename);
            if (FileUtil::IsFileExists(_src))
                unlink(_src.c_str());

            std::string _execute = PathUtil::Exe(filename);
            if (FileUtil::IsFileExists(_execute))
                unlink(_execute.c_str());

            std::string _compile_error = PathUtil::CompileError(filename);
            if (FileUtil::IsFileExists(_compile_error))
                unlink(_compile_error.c_str());

            std::string _stdin = PathUtil::Stdin(filename);
            if (FileUtil::IsFileExists(_stdin))
                unlink(_stdin.c_str());

            std::string _stdout = PathUtil::Stdout(filename);
            if (FileUtil::IsFileExists(_stdout))
                unlink(_stdout.c_str());

            std::string _stderr = PathUtil::Stderr(filename);
            if (FileUtil::IsFileExists(_stderr))
                unlink(_stderr.c_str());
        }

    public:
        /***************************************
         * 输入:
         * code： 用户提交的代码
         * input: 用户给自己提交的代码对应的输入，不做处理
         * cpu_limit: 时间要求
         * mem_limit: 空间要求
         *
         * 输出:
         * 必填
         * status: 状态码
         * reason: 请求结果,错误原因
         * 选填：
         * stdout: 我的程序运行完的结果
         * stderr: 我的程序运行完的错误结果
         * ************************************/
        static void Start(const std::string &in_json, std::string *out_json) // 客户端传入json式的数据，返回json式结果
        {
            Json::Value in_value; // Value Json中间类，可以填充kv值
            Json::Reader reader;
            reader.parse(in_json, in_value);

            std::string code = in_value["code"].asCString();
            std::string input = in_value["input"].asCString();
            int cpu_limit = in_value["cpu_limit"].asInt();
            int mem_limit = in_value["mem_limit"].asInt();

            int status_code = 0;
            int run_result = 0;
            Json::Value out_value;
            std::string filename;

            if (code.size() == 0)
            {
                status_code = -1; // 代码为空
                goto END;
            }

            // 形成唯一文件名，并将code内容写入到temp路径下的临界源文件中
            filename = FileUtil::UniqFileName();
            // 形成临时src文件
            if (!FileUtil::WriteFile(PathUtil::Src(filename), code))
            {
                status_code = -2; // 内部错误
                goto END;
            }

            if (!Compiler::Compile(filename))
            {
                status_code = -3; // 代码编译的时候发生了错误
                goto END;
            }

            run_result = Runner::Run(filename, cpu_limit, mem_limit);
            if (run_result < 0)
            {
                status_code = -2; // 内部错误
            }
            else
            {
                status_code = run_result; // 包含运行成功和崩溃
            }
        END:
            out_value["status"] = status_code;
            out_value["reason"] = CodeToDesc(status_code, filename);
            if (status_code == 0)
            { // 整个过程成功
                std::string _stdout;
                FileUtil::ReadFile(PathUtil::Stdout(filename), &_stdout, true);
                out_value["stdout"] = _stdout;

                std::string _stderr;
                FileUtil::ReadFile(PathUtil::Stderr(filename), &_stderr, true);
                out_value["stderr"] = _stderr;
            }

            // 序列化
            Json::StyledWriter writer; // Json::FastWriter
            *out_json = writer.write(out_value);

            // 清理临时文件
            RemoveTempFile(filename);
        }
    };
}