#include <iostream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <ctemplate/template.h>

int main()
{
    std::string in_html = "./test.html";
    std::string value = "hello world";

    // 形成数据字典
    ctemplate::TemplateDictionary root("test");
    root.SetValue("key", value);

    // 获取被渲染网页对象
    ctemplate::Template *tp1 = ctemplate::Template::GetTemplate(in_html, ctemplate::DO_NOT_STRIP);

    // 开始渲染
    std::string out_html;
    tp1->Expand(&out_html, &root);

    std::cout << "带渲染的html为: " << std::endl;
    std::cout << out_html << std::endl;

    // std::vector<std::string> tokens;
    // std::string str = "duh jie iehfu hu#jdg uh#gy";
    // std::string Sep = " #";
    // boost::split(tokens, str, boost::is_any_of(Sep), boost::algorithm::token_compress_on);

    // for (auto &iter : tokens)
    // {
    //     std::cout << iter << std::endl;
    // }
    return 0;
}