#pragma once
#include "../Comm/log.hpp";
#include "../Comm/util.hpp";

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <cassert>
#include <cstdlib>

namespace ns_model
{
    using namespace std;
    using namespace ns_log;
    using namespace ns_util;

    struct Question
    {
        string number;      // 题目编号
        string title;       // 标题
        string star;        // 难度等级
        int cpu_limit;      // 时间限制
        int mem_limit;      // 空间限制
        std::string desc;   // 题目描述
        std::string header; // 预设代码
        std::string tail;   // 测试用例
    };

    const string questions_list = "./questions/questions_list";
    const string question_path = "./questions/";

    class Model
    {
    public:
        Model()
        {
            assert(LoadQuestionsList(questions_list));
        }
        ~Model()
        {
        }

    public:
        bool LoadQuestionsList(const std::string &questions_list)
        {
            ifstream in(questions_list);
            if (!in.is_open())
            {
                LOG(FATLE) << "加载题库失败，检查是否存在题库文件" << "\n";
                return false;
            }

            string line;
            while (getline(in, line))
            {
                vector<string> tokens;
                StringUtil::SplitString(line, &tokens, " ");

                if (tokens.size() != 5)
                {
                    LOG(WARNINR) << "加载部分题目失败，请检查文件格式" << "\n";
                    continue;
                }

                struct Question ques;
                ques.number = tokens[0];
                ques.title = tokens[1];
                ques.star = tokens[2];
                ques.cpu_limit = atoi(tokens[3].c_str());
                ques.mem_limit = atoi(tokens[4].c_str());

                string path = question_path;
                path += ques.number;
                path += "/";

                FileUtil::ReadFile(path + "desc", &ques.desc, true);
                FileUtil::ReadFile(path + "header.hpp", &ques.header, true);
                FileUtil::ReadFile(path + "tail.hpp", &ques.tail, true);

                _questions.insert({ques.number, ques});
            }

            LOG(INFO) << "加载题库成功！！" << "\n";
            in.close();
            return true;
        }

        bool GetAllQuestions(vector<struct Question> *out)
        {
            if (_questions.size() == 0)
            {
                LOG(ERROR) << "用户获取题库失败" << "\n";
                return false;
            }

            for (auto &[a, b] : _questions)
            {
                (*out).push_back(b);
            }
            return true;
        }

        bool GetOneQuestion(const std::string &number, struct Question *out)
        {
            auto iter = _questions.find(number);
            if (iter == _questions.end())
            {
                LOG(ERROR) << "用户获取题目失败, 题目编号: " << number << "\n";
                return false;
            }

            *out = iter->second;
            return true;
        }

    private:
        unordered_map<string, struct Question> _questions;
    };
}