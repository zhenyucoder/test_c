#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <ctemplate/template.h>

namespace ns_view
{
    using namespace std;
    const std::string template_path = "./template_html/";
    class View
    {
    public:
        View()
        {
        }
        ~View()
        {
        }

    public:
        void AllExpandHtml(const vector<struct Question> &questions, std::string *html)
        {
            std::string src_html = template_path + "all_questions.html";

            ctemplate::TemplateDictionary root("all_questions");
            for (const auto &q : questions)
            {
                ctemplate::TemplateDictionary *sub = root.AddSectionDictionary("question_list");
                sub->SetValue("number", q.number);
                sub->SetValue("title", q.title);
                sub->SetValue("star", q.star);
            }

            ctemplate::Template *tp = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            tp->Expand(html, &root);
        }
        void OneExpandHtml(const struct Question &q, std::string *html)
        {
            std::string src_html = template_path + "one_question.html";

            ctemplate::TemplateDictionary root("one_question");
            root.SetValue("number", q.number);
            root.SetValue("title", q.title);
            root.SetValue("star", q.star);
            root.SetValue("desc", q.desc);
            root.SetValue("pre_code", q.header);

            ctemplate::Template *tp = ctemplate::Template::GetTemplate(src_html, ctemplate::DO_NOT_STRIP);

            tp->Expand(html, &root);
        }
    };
}