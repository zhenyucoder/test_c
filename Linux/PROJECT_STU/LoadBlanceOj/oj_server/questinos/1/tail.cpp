#ifndef COMPILER_ONLINE
#include "header.cpp"
#endif

void test1(int x)
{
    bool ret = Solution().isPalindrome(x);
    if (ret)
        std::cout << "Test1 ok!" << std::endl;
    else
        std::cout << "Test1 failed! input: 121, output expected true, actual false" << std::endl;
}

void test2(int x)
{
    bool ret = Solution().isPalindrome(x);
    if (ret)
        std::cout << "Test2 failed! input: -10, output expected false, actual true" << std::endl;
    else
        std::cout << "Test2 ok!" << std::endl;
}

int main()
{
    test1(121);
    test2(-10);
    return 0;
}