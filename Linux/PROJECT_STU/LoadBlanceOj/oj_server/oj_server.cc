#include <iostream>
#include "../Comm/httplib.h"
#include "control.hpp"

using namespace httplib;
using namespace ns_control;

int main()
{
    // 用户请求的服务器路由功能，根据用户的不同情况，呈现不同的功能
    Server svr;
    Control ctrl;
    svr.Get("/all_questions", [&ctrl](const Request &req, Response &resp)
            {
                std::string html;
                ctrl.AllQuestion(&html);
                resp.set_content(html, "text/html; charset=utf8");
                // resp.set_content("这是所有题目的列表", "text/plain; charset=utf8");
            });

    svr.Get(R"(/question/(\d+))", [&ctrl](const Request &req, Response &resp)
            {
                std::string html;
                std::string number = req.matches[1];
                ctrl.OneQuestion(number, &html);
                resp.set_content(html, "text/html; charset=utf8");
                // resp.set_content("这是指定一道题：" + number, "text/plain; charset=utf8");
            });

    svr.Get(R"(/judge/(\d+))", [](const Request &req, Response &resp)
            {
        std::string number = req.matches[1];
        resp.set_content("指定题目的判题: " + number, "text/plain; charset=utf8"); });

    svr.set_base_dir("./wwwroot");
    svr.listen("0.0.0.0", 8888);
    return 0;
}