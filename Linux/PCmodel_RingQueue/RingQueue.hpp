#pragma once
#include <vector>
#include <string>
#include <pthread.h>
#include <semaphore.h>
#include "LockGuard.hpp"

const int defaultSize = 5;

template <class T>
class RingQueue
{
    void P(sem_t &sem)
    {
        sem_wait(&sem);
    }

    void V(sem_t &sem)
    {
        sem_post(&sem);
    }

public:
    RingQueue(int size = defaultSize)
        : _size(size), _ringqueue(size), _p_step(0), _c_step(0)
    {
        sem_init(&_data_sem, 0, 0);
        sem_init(&_space_sem, 0, size);
        pthread_mutex_init(&_mutex_p, nullptr);
        pthread_mutex_init(&_mutex_c, nullptr);
    }

    void Push(T &in)
    {
        P(_space_sem);
        {
            LockGuard lock(&_mutex_p);
            // pthread_mutex_lock(&_mutex_p);
            _ringqueue[_p_step] = in;
            _p_step++;
            _p_step %= _size;
            // pthread_mutex_unlock(&_mutex_p);
        }
        V(_data_sem);
    }

    void Pop(T *out)
    {
        P(_data_sem);
        {
            LockGuard lock(&_mutex_c);
            //  pthread_mutex_lock(&_mutex_c);
            *out = _ringqueue[_c_step];
            _c_step++;
            _c_step %= _size;
            // pthread_mutex_unlock(&_mutex_c);
        }
        V(_space_sem);
    }

    ~RingQueue()
    {
        sem_destroy(&_data_sem);
        sem_destroy(&_space_sem);

        pthread_mutex_destroy(&_mutex_c);
        pthread_mutex_destroy(&_mutex_p);
    }

private:
    std::vector<T> _ringqueue;
    int _size;

    int _p_step; // 生产者
    int _c_step; // 消费者

    sem_t _data_sem;  // 消费者使用
    sem_t _space_sem; // 生产者使用

    pthread_mutex_t _mutex_p; // 生产者使用
    pthread_mutex_t _mutex_c; // 消费者使用
};