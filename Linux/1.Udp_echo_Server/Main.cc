#include <iostream>
#include <string>
#include <memory>
#include "UdpServer.hpp"
#include "Comm.hpp"


void Usage(std::string proc)
{
    // std::cout << "Usage:\n\t" <<  proc << " server_ip server_port" << std::endl;
    std::cout << "Usage:\n\t" <<  proc << "server_port" << std::endl;
}

// ./UdpServer server_ip server_port
int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(Usage_err);
    }

    // std::string ip = argv[1];
    uint16_t port = std::stoi(argv[1]);
    // UdpService usvr(ip,port);
    UdpService* usvr = new UdpService(port);
    // std::cout << usvr->_ip << " " << usvr->_port << std::endl;
    usvr->Init();
    usvr->Start();
    delete usvr;
    // std::unique_ptr<UdpService> usvr = std::make_unique<UdpService>(ip, port);
    return 0;
}