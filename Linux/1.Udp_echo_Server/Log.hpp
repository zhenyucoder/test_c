#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <stdarg.h>
#include <ctime>
#include <unistd.h>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
enum
{
    Debug = 0,
    Info,
    Warning,
    Error,
    Fatal
};

enum
{
    Screen = 10,
    OneFile,
    ClassFile
};

const int default_style = Screen;
const std::string default_filename = "testlog.";
const std::string logdir = "log";

std::string LevalToString(int leval)
{
    switch (leval)
    {
    case Debug:
        return "Debug";
    case Info:
        return "Info";
    case Warning:
        return "Warning";
    case Error:
        return "Error";
    case Fatal:
        return "Fatal";
    default:
        return "Unknown";
    }
}

std::string TimeStampExLocalTime()
{
    time_t currtime = time(nullptr);
    struct tm *curr = localtime(&currtime);
    char buffer[128];
    int n = snprintf(buffer, sizeof(buffer) - 1, "%d-%d-%d %d:%d:%d",
                     curr->tm_year + 1900, curr->tm_mon + 1, curr->tm_mday,
                     curr->tm_hour, curr->tm_min, curr->tm_sec);
    return buffer;
}

class Log
{
public:
    Log(int style = default_style, const std::string &filename = default_filename)
        : _style(style), _filename(filename)
    {
        mkdir(logdir.c_str(), 0775);
    }

    void Enable(int sty)
    {
        _style = sty;
    }

    void WriteLogToOneFile(const std::string &logname, const std::string &message)
    {
        umask(0);
        int fd = open(logname.c_str(), O_CREAT | O_WRONLY | O_APPEND, 0666);
        if (fd < 0)
            return;
        write(fd, message.c_str(), message.size());
        close(fd);

        // std::ofstream out(logname);
        // if (!out.is_open())
        //     return;
        // out.write(message.c_str(), message.size());
        // out.close();
    }

    void WriteLogToClassFile(const std::string &levalstr, const std::string &loginfo)
    {
        std::string logname = logdir;
        logname += "/";
        logname += _filename;
        logname += levalstr;
        WriteLogToOneFile(logname, loginfo);
    }

    void WriteLog(const std::string &levalstr, const std::string &loginfo)
    {
        switch (_style)
        {
        case Screen:
            std::cout << loginfo << std::endl;
            break;
        case OneFile:
            WriteLogToClassFile("all", loginfo);
            break;
        case ClassFile:
            WriteLogToClassFile(levalstr, loginfo);
            break;
        }
    }

    void LogMessage(int leval, const char *format, ...)
    {
        char leftbuffer[1024];
        std::string levalstr = LevalToString(leval);
        std::string currtime = TimeStampExLocalTime();
        std::string idstr = std::to_string(getpid());
        snprintf(leftbuffer, sizeof(leftbuffer), "[%s][%s][%s]", levalstr.c_str(), currtime.c_str(), idstr.c_str());

        // 获取内容
        char rightbuffer[1024];
        va_list args; // char* / void*
        va_start(args, format);
        vsnprintf(rightbuffer, sizeof(rightbuffer), format, args);
        va_end(args);

        // printf("%s %s\n", leftbuffer, rightbuffer);
        std::string loginfo = leftbuffer;
        loginfo += rightbuffer;
        WriteLog(levalstr, loginfo);
    }

    ~Log()
    {
    }

private:
    int _style;
    std::string _filename;
};


Log lg;

class Conf
{
public:
    Conf()
    {
        lg.Enable(Screen);
    }
    ~Conf()
    {}
};

Conf conf;