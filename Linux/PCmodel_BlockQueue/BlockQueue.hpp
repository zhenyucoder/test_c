#pragma once
#include <iostream>
#include <queue>
#include <pthread.h>
#include "LockGuard.hpp"
// #define defaultSize 5
const int defaultSize = 5;

template <class T>
class BlockQueue
{
public:
    BlockQueue(int cap = defaultSize)
        : _capacity(cap)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_p_cond, nullptr);
        pthread_cond_init(&_c_cond, nullptr);
    }

    bool Full()
    {
        return _q.size() == _capacity;
    }

    void Push(T &in)
    {
        {
            LockGuard lock(&_mutex);
            // pthread_mutex_lock(&_mutex);
            while (Full())
            {
                pthread_cond_wait(&_p_cond, &_mutex);
            }
            _q.push(in);
            pthread_cond_signal(&_c_cond); // 通知消费者消费
            // pthread_mutex_unlock(&_mutex);
        }
    }

    bool Empty()
    {
        return _q.size() == 0;
    }

    void Pop(T *out)
    {
        {
            LockGuard lock(&_mutex);
            // pthread_mutex_lock(&_mutex);
            while (Empty())
            {
                pthread_cond_wait(&_c_cond, &_mutex);
            }
            *out = _q.front();
            _q.pop();
            pthread_cond_signal(&_p_cond); // 通知生成者生产
            // pthread_mutex_unlock(&_mutex);
        }
    }

    ~BlockQueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_p_cond);
        pthread_cond_destroy(&_c_cond);
    }

private:
    std::queue<T> _q;
    int _capacity;
    pthread_mutex_t _mutex;
    pthread_cond_t _p_cond; // 生产者使用
    pthread_cond_t _c_cond; // 消费者使用
};