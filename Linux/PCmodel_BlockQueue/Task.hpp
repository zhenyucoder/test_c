#pragma once
#include <iostream>
#include <string>

const int defaultResult = 0;
enum
{
    ok = 0,
    div_zero,
    mod_zero,
    unknow
};

class Task
{
public:
    Task()
    {}
    Task(int data_x, int data_y, char op)
        : _data_x(data_x), _data_y(data_y), _op(op), _result(ok), _code(defaultResult)
    {
    }

    void Run()
    {
        switch (_op)
        {
        case '+':
            _result = _data_x + _data_y;
            break;
        case '-':
            _result = _data_x - _data_y;
            break;
        case '*':
            _result = _data_x * _data_y;
            break;
        case '/':
        {
            if (_data_y == 0)
                _code = div_zero;
            else
                _result = _data_x + _data_y;
            break;
        }
        case '%':
        {
            if (_data_y == 0)
                _code = mod_zero;
            else
                _result = _data_x + _data_y;
            break;
        }
        default:
            _code = unknow;
        }
    }

    void operator()()
    {
        Run();
    }

    std::string PrintTask()
    {
        std::string s;
        s += std::to_string(_data_x);
        s += _op;
        s += std::to_string(_data_y);
        s += "=?";
        return s;
    }

    std::string PrintResult()
    {
        std::string s;
        s += std::to_string(_data_x);
        s += _op;
        s += std::to_string(_data_y);
        s += "=";
        s += std::to_string(_result);
        s += ", [";
        s += std::to_string(_code);
        s += "]";
        return s;
    }

    ~Task()
    {
    }

private:
    int _data_x;
    int _data_y;
    char _op;

    int _result;
    int _code; // 错误码为0，结果可行，否则不可信
};