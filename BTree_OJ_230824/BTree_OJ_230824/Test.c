//bool isUnivalTree(struct TreeNode* root) {
//    if (root == NULL)//为空，成立
//        return true;
//
//    //父子节点值不相等
//    if ((root->left && root->val != root->left->val)
//        || (root->right && root->val != root->right->val))
//        return false;
//
//    return isUnivalTree(root->left) && isUnivalTree(root->right);
//}


//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (p == NULL && q == NULL)
//        return true;
//
//    //只有一个节点存在
//    if (p == NULL || q == NULL)
//        return false;
//
//    if (p->val != q->val)
//        return false;
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}



//bool _isSymmetric(struct TreeNode* leftNode, struct TreeNode* rightNode)
//{
//    if (leftNode == NULL && rightNode == NULL)
//        return true;
//    //只有一个节点存在
//    if (leftNode == NULL || rightNode == NULL)
//        return false;
//    if (leftNode->val != rightNode->val)
//        return false;
//
//    return _isSymmetric(leftNode->left, rightNode->right)
//        && _isSymmetric(leftNode->right, rightNode->left);
//}
//bool isSymmetric(struct TreeNode* root) {
//    return _isSymmetric(root->left, root->right);
//}


//int BTreeSize(struct TreeNode* root)
//{
//    return root == NULL ? 0 : 1 + BTreeSize(root->left) + BTreeSize(root->right);
//}
//void _preorder(struct TreeNode* root, int* arr, int* pi)
//{
//    if (root == NULL)
//    {
//        return;
//    }
//    arr[(*pi)++] = root->val;
//    _preorder(root->left, arr, pi);
//    _preorder(root->right, arr, pi);
//}
//
//int* preorderTraversal(struct TreeNode* root, int* returnSize) {
//    *returnSize = BTreeSize(root);
//    int* arr = (int*)malloc(*returnSize * sizeof(int));
//    int i = 0;
//    _preorder(root, arr, &i);
//    return arr;
//}



//int BTreeSize(struct TreeNode* root)
//{
//    return root == NULL ? 0 : 1 + BTreeSize(root->left) + BTreeSize(root->right);
//}
//void _inorderT(struct TreeNode* root, int* arr, int* pi)
//{
//    if (root == NULL)
//        return;
//    _inorderT(root->left, arr, pi);
//    arr[(*pi)++] = root->val;
//    _inorderT(root->right, arr, pi);
//}
//
//int* inorderTraversal(struct TreeNode* root, int* returnSize) {
//    *returnSize = BTreeSize(root);
//    int* arr = (int*)malloc(*returnSize * sizeof(int));
//    int i = 0;
//    _inorderT(root, arr, &i);
//    return arr;
//}



bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
    if (p == NULL && q == NULL)
        return true;

    //只有一个节点存在
    if (p == NULL || q == NULL)
        return false;

    if (p->val != q->val)
        return false;
    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
}

bool isSubtree(struct TreeNode* root, struct TreeNode* subRoot) {
    if (root == NULL)
    {
        return false;
    }

    if (root->val == subRoot->val)
    {
        bool ret = isSameTree(root, subRoot);
        if (ret == true)
            return true;
    }

    return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
}



 
