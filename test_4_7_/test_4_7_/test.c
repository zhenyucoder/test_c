#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	printf("hell0\n");
//	return 0;
//}

//这种写法古老，不推荐
//void main()
//{
//}
//int main(void)
//{
//	printf("haha\n");
//}
//int main(int argc, char* argv[])
//{
//	printf("haha\n");
//	return 0;
//}

//sizeof 是一个操作符，计算机类型/变量所占内存空间的大小
//int main()
//{
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	return 0;
//}

//int main()
//{
//	printf("%d %d\n", 10, 30);
//	return 0;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int num = 0;
//	scanf("%d%d", &num1, &num2);
//	num = num1 + num2;
//	printf("%d\n", num);
//	return 0;
//}

//%c-字符、%d-整型、%s-字符串、%f-float、%p-地址的打印
//int main()
//{
//	char ch1,ch2;
//	scanf("%c%s", &ch1, &ch2);
//	printf("%c\n%s\n", ch1,ch2);
//	return 0;
//}

//int a = 100;
//int main()
//{
//	{
//		//int a = 100;
//	}
//	printf("%d\n", a);
//	return 0;
//}
 
//int a = 2023;
//void test()
//{
//	printf("2--->%d\n", a);
//}
//int main()
//{
//	printf("1--->%d\n", a);
//	test();
//	return 0;
//}

//extern int g_val;
//int main()
//{
//	printf("%d\n", g_val);
//	return 0;
//}

////int a = 100;
//int main()
//{
//	int a = 2023;
//	printf("%d\n", a);
//	int a = 20;
//	printf("%d\n", a);
//	return 0;
//}

//int main()
//{
//	/*const int a = 3;
//	printf("%d\n", a);
//	a = 2;
//	printf("%d\n", a);*/
//	const int n = 10;
//	int arr1[n];
//	int arr2[10];
//	return 0;
//}

//#define SIZE 10
//#define MAX 20
//#define CH 'w'
//int main()
//{
//	printf("%c", CH);
//	int arr[SIZE] = { 0 };
//	int a;
//	a=MAX;
//	return 0;
//}

