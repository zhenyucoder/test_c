#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//	int x, y, z;
//	int a = 0;
//	scanf("%d %d %d", &x, &y, &z);
//	if (x < y)
//	{
//		a = y, y = x, x = a;
//	}
//	if (x < z)
//	{
//		a = z, z = x, x = a;
//	}
//	if (y < z)
//	{
//		a = z, z = y, y = a;
//	}
//	printf("%d %d %d\n", x, y, z);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a = 7;
//    int b = 8;
//    int r = 10;
//    int x = 0;
//
//    scanf("%d %d\n%d\n%d", &a, &b, &r, &x);
//    printf("%d\n%d\n%d\n", a * b, (int)(3.14 * r * r), x * x);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int n, i;
//    scanf("%d", &n);
//    int arr[1001] = { 0 };
//    int temp = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d ", &temp);
//        if (temp >= 1 && temp <= 1000)
//            arr[temp] = temp;
//    }
//    for (i = 0; i <= 1000; i++)
//        if (arr[i] != 0)
//            printf("%d ", arr[i]);
//    return 0;
//}

//#include <stdio.h>
//#include <math.h>
//int main()
//{
//    int n = 0;
//    int count = 0;
//    for (n = 100; n < 1000; n++)
//    {
//        int i = 2;
//
//        for (i = 2; i <= sqrt(n); i++)
//        {
//            if (n % i == 0)
//            {
//                break;
//            }
//        }
//        if (i > sqrt(n))
//        {
//            count++;
//        }
//    }
//    printf("%d\n", count);
//    return 0;
//}

#include <stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);

    int ret = 0;
    int i = 0;
    while (n)
    {
        int w = n % 10;
        if (0 == w % 2)
        {
            w = 0;
        }
        else
        {
            w = 1;
        }
        ret += w * pow(10, i++);
        n /= 10;
    }

    printf("%d\n", ret);

    return 0;
}
