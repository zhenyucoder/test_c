#include <stdio.h>
#include <stdlib.h>



//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//开辟成功
//	int i = 0;
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}


//int main()
//{
//	int* p = (int*)calloc(LLONG_MAX, sizeof(int));
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}



//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	
//	//初始化1~10
//	for (int i = 0; i < 10; i++)
//	{
//		p[i] = i + 1;
//	}
//
//	//增加一些空间
//	int* ptr = realloc(p, 80);
//	if (ptr == NULL)
//	{
//		perror("remalloc");
//		return 1;
//	}
//	else
//	{
//		p = ptr;
//		ptr = NULL;
//	}
//
//	//打印数据
//	for (int i = 0; i < 20; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//	return 0;
//}




//void test()
//{
//	int* p = (int*)malloc(40);
//	*p = 20;//如果p是空指针，会有问题。开辟完后判断
//	free(p);
//}

//void test()//err
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return ;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		*p = i;
//		p++;
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//}
//int main()
//{
//	test();
//	return 0;
//}





//struct S
//{
//	int n;
//	int arr[];
//	//int arr[0];
//};
//
//int main()
//{
//	//printf("%d\n", sizeof(struct S));
//	struct S* ps = (struct S*)malloc(sizeof(struct S) + 40);
//	if (ps == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	ps->n = 100;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i] = i + 1;
//	}
//
//	//空间不够，需要增容
//	struct S* ptr = realloc(ps, sizeof(struct S) + 60);
//	if (ptr == NULL)
//	{
//		perror("remalloc");
//		return 1;
//	}
//	ps = ptr;
//	ps->n = 15;
//	for (i = 0; i < 15; i++)
//	{
//		printf("%d\n", ps->arr[i]);
//	}
//	//释放
//	free(ps);
//	ps = NULL;
//	return 0;
//}





struct S
{
	int n;
	int* arr;
};

int main()
{
	struct S* ps = (struct S*)malloc(sizeof(struct S));
	if (ps == NULL)
	{
		perror("malloc");
		return 1;
	}

	ps->n = 100;
	ps->arr = (int*)malloc(40);
	if (ps->arr == NULL)
	{
		perror("malloc->arr");
		return 1;
	}
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		ps->arr[i] = i + 1;
	}

	//调整
	int* ptr = realloc(ps->arr, 60);
	if (ptr != NULL)
	{
		ps->arr = ptr;
	}
	else
	{
		perror("realloc");
		return 1;
	}

	//打印
	for (i = 0; i < 15; i++)
	{
		printf("%d\n", ps->arr[i]);
	}

	//释放
	free(ps);
	ps = NULL;
	return 0;
}