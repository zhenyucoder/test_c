#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//struct Stu
//{
//	char name[20];
//	char sex[5];
//	int age;
//}s2,s3;//全局变量

//struct Stu s2;
//typedef struct Stu
//{
//	char name[20];
//	char sex[5];
//	int age;
//}Stu;
//
//int main()
//{
//	//struct Stu s1;//局部变量
//	Stu s2;
//	return 0;
//}


//结构的成员可以是其他结构体
//struct S
//{
//	int a;
//	char arr[5];
//	int* p;
//}s1 = {100,"hehe",NULL};
//struct B
//{
//	char ch[10];
//	struct S s;
//	double d;
//};
//
//int main()
//{
//	struct S s3 = { .p=NULL,.a=17,.arr="bit"};
//	printf("%d %p %s\n", s3.a, s3.p, s3.arr);
//	struct B sb = { .d = 3.14,.ch = "hello",.s = {.p = NULL,.arr = "bit",.a = 20} };
//	printf("%s %d %s %p %.2lf", sb.ch, sb.s.a, sb.s.arr,sb.s.p, sb.d);
//	return 0;
//}

//#include <string.h>
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void set_stu(struct Stu* ps)
//{
//	//(*ps).age = 20;
//	ps->age = 20;
//	strcpy(ps->name, "张三");
//}
//
//void print_stu(struct Stu s)
//{
//	printf("%d %s\n", s.age, s.name);
//}
//
//int main()
//{
//	struct Stu  s= { 0 };
//	//初始化结构体
//	set_stu(&s);
//	print_stu(s);
//	return 0;
//}

//int number_of_1(int m)
//{
//	int count = 0;
//	while (m)
//	{
//		m = m & (m - 1);
//		count++;
//	}
//	return count++;
//}
//
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//
//	int ret = number_of_1(x ^ y);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	int i = 0;
//	scanf("%d", &n);
//	for (i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	printf("\n");
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (n >> i) & 1);
//	}
//	return 0;
//}

//方法一
//int number_of_1(unsigned int n)
//{
//	int count = 0;
//	while (n)
//	{
//		if (n % 2 == 1)
//			count++;
//		n /= 2;
//	}
//	return count;
//}
//方法二
//int number_of_1(int n)
//{
//	int count = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) == 1)
//			count++;
//	}
//	return count;
//}
//终极解法
int number_of_1(int n)
{
	int count = 0;
	while (n)
	{
		n = n & (n - 1);
		count++;
	}
	return count;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	int ret = number_of_1(n);
	printf("%d\n", ret);
	return 0;
}