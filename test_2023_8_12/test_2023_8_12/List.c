#include "List.h"

LTNode* BuyLTNode(LTDataType x)
{
	LTNode* newnode = (LTNode*)malloc(sizeof(LTNode));
	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	newnode->data = x;
	newnode->prev = NULL;
	newnode->next = NULL;
}


LTNode* LTInit()
{
	LTNode* phead = BuyLTNode(0);
	phead->next = phead;
	phead->prev = NULL;
}

void LTPrint(LTNode* phead)
{
	assert(phead);


	LTNode* cur = phead->next;
	printf("phead<=>");
	while (cur != phead)
	{
		printf("%d<==>", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

void LTPushBack(LTNode* phead, LTDataType x)
{
	assert(phead);

	LTInsert(phead, x);
}


void LTPopBack(LTNode* phead)
{
	assert(phead);
	assert(phead != phead->next);

	LTErase(phead->prev);
}


void LTPushFront(LTNode* phead, LTDataType x)
{
	assert(phead);

	LTInsert(phead->next, x);
}


void LTPopFront(LTNode* phead)
{
	assert(phead);
	assert(phead != phead->next);
	LTErase(phead->next);
}


int LTSize(LTNode* phead)
{
	LTNode* cur = phead->next;
	int count = 0;
	while (cur != phead)
	{
		count++;
		cur = cur->next;
	}
	return count;
}


LTNode* LTFind(LTNode* phead, LTDataType x)
{
	assert(phead);

	LTNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}