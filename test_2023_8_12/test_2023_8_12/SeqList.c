#include "SeqList.h"


void SLInit(SL* ps)
{
	assert(ps);

	ps->a = (SLDataType*)malloc(5*sizeof(SLDataType));
	if (ps->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	ps->capacity = 5;
	ps->size = 0;
}

void SLDestory(SL* ps)
{
	assert(ps);

	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SLPrint(SL* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SLCheakCapacity(SL* ps)
{
	assert(ps);

	if (ps->size == ps->capacity)
	{
		SLDataType* tmp = (SLDataType*)realloc( ps->a,ps->capacity * sizeof(SLDataType)*2);
		if (ps->a == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity *= 2;
	}
}


void SLPushBack(SL* ps, SLDataType x)
{
	assert(ps);
	SLCheakCapacity(ps);

	/*ps->a[ps->size] = x;
	ps->size++;*/
	SLInsert(ps, ps->size, x);
}

void SLPopBack(SL* ps)
{
	assert(ps);
	assert(ps->size > 0);
	ps->size--;
}

void SLPushFront(SL* ps, SLDataType x)
{
	assert(ps);
	SLCheakCapacity(ps);

	//for (int end = ps->size; end > 0; end--)
	//{
	//	ps->a[end] = ps->a[end-1];
	//}
	//ps->a[0] = x;
	//ps->size++;
	SLInsert(ps, 0, x);
}

void SLPopFront(SL* ps)
{
	assert(ps);

	/*for (int begin = 1; begin < ps->size; begin++)
	{
		ps->a[begin - 1] = ps->a[begin];
	}
	ps->size--;*/
	SLErase(ps, 0);
}

void SLInsert(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	SLCheakCapacity(ps);

	for (int end = ps->size-1; end >= pos; end--)
	{
		ps->a[end + 1] = ps->a[end];
	}
	ps->a[pos] = x;

	ps->size++;
}


void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);

	for (int begin = pos + 1; begin < ps->size; begin++)
	{
		ps->a[begin - 1] = ps->a[begin];
	}
	ps->size--;
}


int SLFind(SL* ps, SLDataType x)//�����±�
{
	assert(ps);
	for (int i = 0; i < ps->a[ps->size]; i++)
	{
		if (ps->a[i] == x)
			return i;
	}
	return -1;
}

void SLModify(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);

	ps->a[pos] = x;
}