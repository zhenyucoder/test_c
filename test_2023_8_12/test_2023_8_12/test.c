#include "SeqList.h"
#include "SList.h"
#include "List.h"


void SeqListTset()
{
	SL s;
	SLInit(&s);
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushBack(&s, 6);
	SLPrint(&s);
	SLPopBack(&s);
	SLPrint(&s);
	SLPushFront(&s, 100);
	SLPushFront(&s, 200);
	SLPushFront(&s, 300);
	SLPushFront(&s, 400);
	SLPushFront(&s, 500);
	SLPushFront(&s, 600);
	SLPushFront(&s, 700);
	SLPrint(&s);
	SLPopFront(&s);
	SLPopFront(&s);
	SLPrint(&s);

}

SeqListTset2()
{
	SL s;
	SLInit(&s);
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushBack(&s, 6);
	SLPrint(&s);

	/*SLInsert(&s, 1, 2000);
	SLInsert(&s, 2, 3000);
	SLPrint(&s);*/
	
	SLErase(&s, 1);
	SLPrint(&s);
	printf("%d\n", SLFind(&s, 5));
	SLModify(&s, 2, 3000);
	SLPrint(&s);

}


TestSList()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist);
	SLTPopBack(&plist);
	SLTPrint(plist);

	SLTPushFront(&plist, 1000);
	SLTPushFront(&plist, 2000);
	SLTPushFront(&plist, 3000);
	SLTPushFront(&plist, 4000);
	SLTPrint(plist);

	SLTPopFront(&plist);
	SLTPopFront(&plist);
	SLTPrint(plist);

	printf("%d\n", SLTFind(plist, 1000)->data);

	
}


TestList()
{
	LTNode* plist = LTInit();
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);

	LTPrint(plist);
	LTPopBack(plist);
	LTPopBack(plist);
	LTPrint(plist);

	LTPushFront(plist, 1000);
	LTPushFront(plist, 2000);
	LTPushFront(plist, 3000);
	LTPushFront(plist, 4000);
	LTPrint(plist);

	LTPopFront(plist);
	LTPopFront(plist);
	LTPopFront(plist);
	LTPrint(plist);
	
	printf("%d\n", LTSize(plist));
}

int main()
{
	//SeqListTset2();
	//TestSList();
	TestList();
	return 0;
}