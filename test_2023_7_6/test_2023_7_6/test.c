#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int n = 0; 
//	scanf("%d", &n);
//	int arr[50];
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	//判断
//	int flag1 = 0;
//	int flag2 = 0;
//	for (int i = 0; i < n - 1; i++)
//	{
//		if (arr[i] < arr[i + 1])
//			flag1 = 1;
//		else if (arr[i] > arr[i + 1])
//			flag2 = 1;
//	}
//	if (flag1 + flag2 == 2)
//		printf("unsorted\n");
//	else
//		printf("sorted\n");
//	return 0;
//}

//优化
//int main()
//{
//	int n = 0;
//	int flag1 = 0;
//	int flag2 = 0;
//	scanf("%d", &n);
//	int arr[50];
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//		if (i >= 1)
//		{
//			if (arr[i] > arr[i - 1])
//				flag1 = 1;
//			else if (arr[i] < arr[i - 1])
//				flag2 = 1;
//		}
//	}
//	//判断
//	if (flag2 + flag1 == 2)
//		printf("unsorted\n");
//	else
//		printf("sorted\n");
//	return 0;
//}





//int get_days_of_month(int y, int m)
//{
//	int d = 0;
//	switch (m)
//	{
//	case 1:
//	case 3:
//	case 5:
//	case 7:
//	case 8:
//	case 10:
//	case 12:
//		d = 31;
//		break;
//	default :
//		d = 30;
//	case 2:
//	{
//		d = 28;
//		if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
//		{
//			d++;
//		}
//		
//	}
//	return d;
//	}
//}

//优化
//int get_days_of_month(int y, int m)
//{
//	int days[] = {0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	int d = days[m];
//	if ((y % 100 != 0 && y % 4 == 0) || (y % 400 == 0))
//		d++;
//	return d;
//}
//int main()
//{
//	int year = 0;
//	int month = 0;
//	while (~scanf("%d %d", &year, &month))
//	{
//		int x = get_days_of_month(year, month);
//		printf("%d\n", x);
//	}
//	return 0;
//}




//int main()
//{
//	int arr[] = { 1,2,3,5,6 };
//	short* p = (short*)arr;
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		*(p + i) = 0;
//	}
//
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]); 
//	}
//	return 0;
//}




//int main()
//{
//	float arr[] = { 3.14f, 86.4f, 2.3f };
//	float* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%f ", *(p++));
//	}
//	return 0;
//}






//使用临时数组
//void rotate(int** matrix, int matrixSize, int* matrixColSize) {
//    int matrix_new[matrixSize][matrixSize];
//    //使用临时数组存储矩阵
//    for (int i = 0; i < matrixSize; i++)
//    {
//        for (int j = 0; j < matrixSize; j++)
//        {
//            matrix_new[i][j] = matrix[i][j];
//        }
//    }
//    //旋转矩阵
//    for (int i = 0; i < matrixSize; i++)
//    {
//        for (int j = 0; j < matrixSize; j++)
//        {
//            matrix[j][matrixSize - i - 1] = matrix_new[i][j];
//        }
//    }
//}

//不使用临时数组，直接反转
//void rotate(int** matrix, int matrixSize, int* matrixColSize) {
//    //先上下交换
//    for (int i = 0; i < matrixSize / 2; i++)
//    {
//        for (int j = 0; j < matrixSize; j++)
//        {
//            int tmp = matrix[i][j];
//            matrix[i][j] = matrix[matrixSize - 1 - i][j];
//            matrix[matrixSize - 1 - i][j] = tmp;
//        }
//    }
//    //对角线交换
//    for (int i = 0; i < matrixSize; i++)
//    {
//        for (int j = i + 1; j < matrixSize; j++)
//        {
//            int tmp = matrix[i][j];
//            matrix[i][j] = matrix[j][i];
//            matrix[j][i] = tmp;
//        }
//    }
//}

//直接交换
void rotate(int** matrix, int matrixSize, int* matrixColSize) {
    for (int i = 0; i < matrixSize / 2; i++)
    {
        for (int j = i; j < matrixSize - 1 - i; j++)
        {
            //交换后，当前位置的列是后一个位置的行
            //当前位置的行是前一个位置的列
            //(i,j) -> (j,n-i-1) -> (n-i-1,n-j-1) -> (n-j-1,i)
            int tmp = matrix[i][j];
            matrix[i][j] = matrix[matrixSize - j - 1][i];
            matrix[matrixSize - j - 1][i] = matrix[matrixSize - i - 1][matrixSize - j - 1];
            matrix[matrixSize - i - 1][matrixSize - j - 1] = matrix[j][matrixSize - i - 1];
            matrix[j][matrixSize - i - 1] = tmp;
        }
    }
}