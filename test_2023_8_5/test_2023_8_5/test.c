//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    if (list1 == NULL)
//        return list2;
//    if (list2 == NULL)
//        return list1;
//    struct ListNode* head = NULL, * tail = NULL;
//    //带一个哨兵位，方便尾插
//    head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//    while (list1 && list2)
//    {
//        //尾插
//        if (list1->val < list2->val)
//        {
//            tail->next = list1;
//            tail = tail->next;
//            list1 = list1->next;
//        }
//        else
//        {
//            tail->next = list2;
//            tail = tail->next;
//            list2 = list2->next;
//        }
//    }
//    if (list1)
//        tail->next = list1;
//    if (list2)
//        tail->next = list2;
//    struct ListNode* del = head;
//    head = head->next;
//    free(del);
//    return head;
//}



//
//#include <algorithm>
//class Partition {
//public:
//    ListNode* partition(ListNode* pHead, int x) {
//        ListNode* lesshead, * lesstail, * greaterhead, * greatertail;
//        lesshead = lesstail = (ListNode*)malloc(sizeof(ListNode));
//        greaterhead = greatertail = (ListNode*)malloc(sizeof(ListNode));
//
//        ListNode* cur = pHead;
//        while (cur)
//        {
//            if (cur->val < x)
//            {
//                lesstail->next = cur;
//                lesstail = lesstail->next;
//            }
//            else
//            {
//                greatertail->next = cur;
//                greatertail = greatertail->next;
//            }
//            cur = cur->next;
//        }
//
//        //链接
//        lesstail->next = greaterhead->next;
//        //置空，防止死循环
//        greatertail->next = NULL;
//
//        ListNode* head = lesshead;
//        head = head->next;
//        free(lesshead);
//        free(greaterhead);
//        return head;
//    }
//};




//class PalindromeList {
//public:
//    struct ListNode* MiddleNode(struct ListNode* head)
//    {
//        struct ListNode* fast = head, * slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        return slow;
//    }
//
//    struct ListNode* reverseList(struct ListNode* head)
//    {
//        struct ListNode* cur = head;
//        struct ListNode* newnode = NULL;
//        while (cur)
//        {
//            struct ListNode* next = cur->next;
//            //头插
//            cur->next = newnode;
//            newnode = cur;
//
//            cur = next;
//        }
//        return newnode;
//    }
//
//    bool chkPalindrome(ListNode* head) {
//        struct ListNode* mid = MiddleNode(head);
//        struct ListNode* r_mid = reverseList(head);
//
//        while (head && r_mid)
//        {
//            if (head->val != r_mid->val)
//                return false;
//            head = head->next;
//            r_mid = r_mid->next;
//        }
//        return true;
//    }
//};




//struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
//    struct ListNode* curA = headA, * curB = headB;
//    int lenA = 1, lenB = 1;
//    while (curA->next)
//    {
//        curA = curA->next;
//        lenA++;
//    }
//    while (curB->next)
//    {
//        curB = curB->next;
//        lenB++;
//    }
//
//    //不相交
//    if (curA != curB)
//    {
//        return NULL;
//    }
//
//    int gap = abs(lenA - lenB);
//    struct ListNode* LongList = headA, * ShortList = headB;
//    if (lenA < lenB)
//    {
//        LongList = headB;
//        ShortList = headA;
//    }
//
//    //长的先走差距步
//    while (gap--)
//    {
//        LongList = LongList->next;
//    }
//
//    //同时走，求相交节点
//    while (LongList != ShortList)
//    {
//        LongList = LongList->next;
//        ShortList = ShortList->next;
//    }
//    return LongList;
//}




//bool hasCycle(struct ListNode* head) {
//    struct ListNode* fast = head, * slow = head;
//    while (fast && fast->next)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//
//        if (fast == slow)
//            return true;
//    }
//    return false;
//}




//struct ListNode* detectCycle(struct ListNode* head) {
//    struct ListNode* fast, * slow;
//    fast = slow = head;
//    while (fast && fast->next)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//
//        if (fast == slow)//相遇
//        {
//            struct ListNode* meet = fast;
//            while (meet != head)
//            {
//                meet = meet->next;
//                head = head->next;
//            }
//            return meet;//入环第一个节点
//        }
//    }
//
//    return NULL;//不带环
//}




struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {
    struct ListNode* curA = headA, * curB = headB;
    int lenA = 1, lenB = 1;
    while (curA->next)
    {
        curA = curA->next;
        lenA++;
    }
    while (curB->next)
    {
        curB = curB->next;
        lenB++;
    }

    //不相交
    if (curA != curB)
    {
        return NULL;
    }

    int gap = abs(lenA - lenB);
    struct ListNode* LongList = headA, * ShortList = headB;
    if (lenA < lenB)
    {
        LongList = headB;
        ShortList = headA;
    }

    //长的先走差距步
    while (gap--)
    {
        LongList = LongList->next;
    }

    //同时走，求相交节点
    while (LongList != ShortList)
    {
        LongList = LongList->next;
        ShortList = ShortList->next;
    }
    return LongList;
}

struct ListNode* detectCycle(struct ListNode* head) {
    struct ListNode* fast, * slow;
    fast = slow = head;
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;

        if (fast == slow)//相遇
        {
            struct ListNode* meet = fast;
            struct ListNode* newhead = meet->next;
            meet->next = NULL;
            return getIntersectionNode(head, newhead);
        }
    }

    return NULL;//不带环
}