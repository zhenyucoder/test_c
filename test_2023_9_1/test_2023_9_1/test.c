#include <stdio.h>
#include <stdlib.h>

//int* masterMind(char* solution, char* guess, int* returnSize) {
//    *returnSize = 2;
//    int* ret = (int*)malloc(*returnSize * sizeof(int));
//    int s_arr[26] = { 0 };
//    int g_arr[26] = { 0 };
//    for (int i = 0; i < 4; i++)
//    {
//        if (solution[i] == guess[i])
//            ret[0] += 1;
//        else
//        {
//            s_arr[solution[i] - 'A'] += 1;
//            g_arr[guess[i] - 'A'] += 1;
//        }
//    }
//    for (int i = 0; i < 26; i++)
//    {
//        ret[1] += s_arr[i] > g_arr[i] ? g_arr[i] : s_arr[i];
//    }
//    return ret;
//}
//
//int main()
//{
//    char solution[] = "RGRB";
//    char guess[] = "BBBY";
//    int i = 0;
//    masterMind(solution, guess, &i);
//    return 0;
//}


//int main()
//{
//	long n, k;
//	while (~scanf("%ld %ld", &n, &k)) 
//	{
//		if (k == 0) 
//		{
//			printf("%ld\n", n * n);//任意数对的取模结果都是大于等于0的
//			continue;
//		} long count = 0;
//		for (long y = k + 1; y <= n; y++) 
//		{
//			count += ((n / y) * (y - k)) + ((n % y < k) ? 0 : (n % y - k + 1));
//		}
//		printf("%ld\n", count);
//	}
//	return 0;
//}


//int main()
//{
//	char str[101];
//	while (scanf("%s", str) > 0) 
//	{
//		int n;
//		scanf("%d", &n);
//		str[n] = '\0';
//		printf("%s\n", str);
//	} return 0;


int findPeakElement(int* nums, int numsLen) 
{
	
	if (numsLen == 1 || nums[0] > nums[1]) return 0;
	if (nums[numsLen - 1] > nums[numsLen - 2]) return numsLen - 1;
	int left = 0, right = numsLen - 1, mid;
	while (left < right) 
	{
		mid = left + (right - left) / 2;
		if (nums[mid] < nums[mid + 1])
			left = mid + 1;
		else 
			right = mid;
	} return left;
}
