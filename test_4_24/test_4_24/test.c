#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
//int main()
//{
//	char arr1[] = { "hello bit" };
//	char arr2[20] = { "xxxxxxxxxxxxxxx" };
//	strcpy( arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//int main()
//{
//	char arr[] = "hello world";
//	memset(arr, 'x', 4);
//	printf("%s\n", arr);
//	return 0;
//}

//int main()
//{
//	int len = strlen("abcdef");
//	printf("%d\n", len);
//	return 0;
//}

//int get_max(int x, int y)
//{
//	return (x > y) ? x : y;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int m = get_max(a, b);
//	printf("max=%d", m);
//	return 0;
//}

//void Swap(int* pa, int* pb)
//{
//	int tmp = *pa;
//	*pa = *pb;
//	*pb = tmp;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	Swap(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}

//判断是否为素数，如果是返回1，否则返回0
//#include <math.h>
//int is_prime(int i)
//{
//	int j = 0;
//	for (j = 3; j < sqrt(i); j += 2)
//	{
//		if (i % j == 0)
//			return 0;
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (is_prime(i) == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}

//判断是否为闰年，如果成立返回1，否则返回0
//int is_leap_year(int y)
//{
//	return((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0));
//}
//int main()
//{
//	int y = 0;
//	int count = 0;
//	for (y = 1000; y <= 2000; y++)
//	{
//		if (is_leap_year(y) == 1)
//		{
//			printf("%d ", y);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}

//写一个函数一个有序数组实现二分查找
int binary_search(int arr[], int k, int sz)
{
	int left = 0;
	int right = sz-1;
	while (left<=right)
	{
		int mid = left + (right - left) / 2;
		if (arr[mid] > k)
		{
			right = mid - 1;
		}
		else if (arr[mid] < k)
		{
			left = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int k = 7;
	int sz = sizeof(arr) / sizeof(arr[0]);
	int m=binary_search(arr, k, sz);
	if (m !=-1)
		printf("找到了，下标是%d\n", m);
	else
		printf("找不到了\n");
	return 0;
}