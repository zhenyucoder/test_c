#define _CRT_SECURE_NO_WARNINGS
#include "game.h"


void menu()
{
	printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
	printf("xxxxxx     1. play     xxxxxx\n");
	printf("xxxxxx     0. exit     xxxxxx\n");
	printf("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
}

void game()
{
	char mine[ROWS][COLS];//存放布置好的雷
	char show[ROWS][COLS];// 存放排查出雷的信息
	//初始化棋盘
	// mine数组最开始全为0；show数组开始全为*
	InitBoard(mine, ROWS, COLS,'0');
	InitBoard(show, ROWS, COLS,'*');
	//打印棋盘
	//DisplayBoard(mine, ROW, COL);
	DisplayBoard(show, ROW, COL);
	//1. 布雷
	SetMine(mine, ROW, COL);
	DisplayBoard(mine, ROW, COL);

	//2. 排查类
	FindMine(mine, show, ROW, COL);
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>\n");
		scanf("%d",&input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏");
			break;
		default:
			printf("选择错误，请重新选择\n");
		}
	} while (input);

	return 0;
}