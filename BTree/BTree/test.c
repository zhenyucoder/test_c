#include <stdio.h>
#include "Queue.h"

typedef int BTDataType;
typedef struct BinaryTreeNode
{
    BTDataType data;
    struct BinaryTreeNode* left;
    struct BinaryTreeNode* right;
}BTNode;

BTNode* BuyNode(BTDataType x)
{
    BTNode* Node = (BTNode*)malloc(sizeof(BTNode));
    if (Node == NULL)
    {
        perror("malloc fail");
        exit(-1);
    }
    Node->data = x;
    Node->left = NULL;
    Node->right = NULL;
    return Node;
}

//先序创建二叉树，中序遍历
//BTNode* createrroot(char* a, int* pi)
//{
//    if (a[*pi] == '#')
//    {
//        (*pi)++;
//        return NULL;
//    }
//
//    BTNode* root = BuyNode(a[*pi]);
//    (*pi)++;
//
//    root->left = createrroot(a, pi);
//    root->right = createrroot(a, pi);
//    return root;
//}
//
//void InOrder(BTNode* root)
//{
//    if (root == NULL)
//        return;
//    InOrder(root->left);
//    printf("%c ", root->data);
//    InOrder(root->right);
//}
//
//int main() {
//    char a[100];
//    while (~scanf("%s", a))
//    {
//        int i = 0;
//        BTNode* root = createrroot(a, &i);
//        InOrder(root);
//        printf("\n");
//    }
//    return 0;
//}


// 判断二叉树是否是完全二叉树
//int BinaryTreeComplete(BTNode* root)
//{
//    Que q;
//    QueueInit(&q);
//
//    if (root)
//        QueuePush(&q, root);
//
//    while (!QueueEmpty(&q))
//    {
//        BTNode* front = QueueFront(&q);
//        QueuePop(&q);
//
//        //遇空就跳过
//        if (front==NULL)
//        {
//            break;
//        }
//
//        QueuePush(&q, root->left);
//        QueuePush(&q, root->right);
//    }
//
//    //检查后续节点是否有非空
//    //有非空就不是完全二叉树
//    while (!QueueEmpty(&q))
//    {
//        BTNode* front = QueueFront(&q);
//        QueuePop(&q);
//        if (front)
//        {
//            QueueDestroy(&q);
//            return false;
//        }
//    }
//    QueueDestroy(&q);
//    return true;
//}


void BinaryTreeDestory(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeDestory(root->left);
	BinaryTreeDestory(root->right);
	free(root);
}