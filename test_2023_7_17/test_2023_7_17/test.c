#include <stdio.h>
#include <stddef.h>
//struct stu
//{
//	int age;
//	char name[20];
//}x,*p;
//
//struct 
//{
//	int age;
//	char name[20];
//}x;
//
//struct
//{
//	int age;
//	char name[20];
//}*p;
//int main()
//{
//	p = &x;
//	return 0;
//}



//结构体自引用
//struct Node//err
//{
//	int date;
//	struct Node next;//sizeof(struct Node)不确定
//};


//struct Node
//{
//	int date;//数据域
//	struct Node* next;//指针域
//};


//err typedef要定义一个完整结构体
//typedef struct 
//{
//	int date;
//	Node* next;
//} Node;

//typedef struct Node
//{
//	int date;
//	struct Node* next;
//} Node;
//int main()
//{
//	Node s = { 0 };
//	return 0;
//}




//struct SN
//{
//	char c;
//	int i;
//}sn1 = { 'q',20 }, sn2 = {.i=23, .c='w'};//全局变量
//
//struct S
//{
//	double d;
//	struct SN s;
//	char arr[10];
//};
//int main()
//{
//	//printf("%d %c\n", sn1.i, sn2.c);
//	struct S s = { .arr = {1,2,3}, .d = 3.14, .s = {'e',20}};//初始换时arr不用带数字
//	printf("%lf %c %d\n", s.d, s.s.c, s.arr[1]);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", s.arr[i]);
//	}
//	return 0;
//}




//struct s1
//{
//	char c1;
//	int i;
//	char c2;
//};
//
//struct s2
//{
//	int i ;
//	char c1;
//	char c2;
//};
//
//int main()
//{
//	/*printf("%d\n", sizeof(struct s1));
//	printf("%d\n", sizeof(struct s2));*/
//	//offsetof(); 可以计算结构体成员相较于结构体起始位置的偏移量
//	printf("%d\n", offsetof(struct s1, c1));
//	printf("%d\n", offsetof(struct s1, i));
//	printf("%d\n", offsetof(struct s1, c2));
//	return 0;
//}




//struct s3
//{
//	double d;
//	char c;
//	int i;
//};
//
//struct s4
//{
//	char c1;
//	struct s3 s;
//	double d;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct s3));
//	printf("%d\n", sizeof(struct s4));
//	return 0;
//}




//#pragma pack(1)
//
//struct S
//{
//	char c1;
//	int a;
//	char c2;
//};
//
//#pragma pack()
//struct S2
//{
//	char c1;
//	int a;
//	char c2;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct S));
//	printf("%d\n", sizeof(struct S2));
//	return 0;
//}




//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//	return 0;
//}





//enum Color
//{
//	RED,
//	GREEN=3,
//	BLUE
//};
//
//int main()
//{
//	/*enum Color c= GREEN;
//	printf("%d\n", c);
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);*/
//
//	enum Color cc = 3;
//	return 0;
//}




//union Un
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	union Un un = { 0 };
//	un.i = x11223344;
//	un.c = 0x55;
//	//printf("%d\n", sizeof(un));
//
//	/*printf("%p\n", &un);
//	printf("%p\n", &un.c);
//	printf("%p\n", &un.i);*/
//	return 0;
//}



//int cheak_sys()
//{
//	union
//	{
//		int i;
//		char c;
//	}un = {.c = 'q', .i=20};
//	return un.c;
//}
//
//int main()
//{
//	int ret = cheak_sys();
//	if (ret)
//		printf("小端\n");
//	else
//		printf("大端\n");
//
//	return 0;
//}





//union Un1
//{
//	char c[5];
//	int i;
//};
//
//union Un2
//{
//	short c[7];
//	int i;
//};
//
//
//int main()
//{
//	printf("%d\n", sizeof(union Un1));
//	printf("%d\n", sizeof(union Un2));
//	return 0;
//}





struct S
{
	char a : 3;
	char b : 4;
	char c : 5;
	char d : 4;
};


int main()
{
	struct S s = { 0 };
	s.a = 10;
	s.b = 12;
	s.c = 3;
	s.d = 4;
	// 01100010 00000110 00000100
	//     62     03         04
	printf("%d\n", sizeof(s));
	return 0;

}