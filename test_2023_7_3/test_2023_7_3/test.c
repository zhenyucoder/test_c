//int removeDuplicates(int* nums, int numsSize) {
//    if (numsSize == 0)
//    {
//        return 0;
//    }
//    int slow = 0;
//    int fast = 1;
//    while (fast < numsSize)
//    {
//        if (nums[fast] != nums[fast - 1])
//            nums[++slow] = nums[fast];
//        fast++;
//    }
//    return ++slow;
//}






//贪心算法
//#include <stdio.h>
//int maxProfit(int* prices, int pricesSize) {
//    int total = 0;
//    int index = 0;
//    while (index < pricesSize)
//    {
//        //如果股票一直下跌就一直找，直到找到股票上涨为止
//        while (index < pricesSize - 1 && prices[index] >= prices[index + 1])
//            index++;
//        int min = prices[index];
//        //如果股票一直上涨就一直找，直到找到股票下跌为止
//        while (index < pricesSize - 1 && prices[index] <= prices[index + 1])
//            index++;
//        //和累加
//        total += prices[index] - min;
//        index++;
//    }
//    return total;
//}
//
//int main()
//{
//    int prices[] = { 1,5,3,7,8,10 };
//    int pricesSize = sizeof(prices) / sizeof(prices[0]);
//    int sum = 0;
//    sum=maxProfit(prices, pricesSize);
//    printf("%d\n", sum);
//    return 0;
//}

//优化
// //int maxProfit(int* prices, int pricesSize) {
//    if (pricesSize == 0)
//        return 0;
//    int sum = 0;
//    for (int i = 1; i < pricesSize; i++)
//    {
//        sum += fmax(0, prices[i] - prices[i - 1]);
//    }
//    return sum;
//}


//动态规划
//int maxProfit(int* prices, int pricesSize) {
//    int dp[pricesSize][2];
//    //base case
//    //第一天没有购买股票
//    dp[0][0] = 0;
//    //第一天购买股票
//    dp[0][1] = -prices[0];
//
//    for (int i = 1; i < pricesSize; i++)
//    {
//        //交易完后手里没有股票
//        dp[i][0] = fmax(dp[i - 1][0], dp[i - 1][1] + prices[i]);
//        //交易完后手里还有股票
//        dp[i][1] = fmax(dp[i - 1][1], dp[i - 1][0] - prices[i]);
//    }
//    return dp[pricesSize - 1][0];
//}

//int maxProfit(int* prices, int pricesSize) {
//    //由于玩家每天持有股票的情况的情况只和前一天有关，因此可以使用两个变量来代替上面的二维数组
//    // base case
//    // 第一天没有购买股票
//    int nohold = 0;
//    // 第一天购买股票
//    int hold = -prices[0];
//
//    for (int i = 1; i < pricesSize; i++)
//    {
//        //交易完后手里没有股票
//        nohold = fmax(nohold, hold + prices[i]);
//        // 交易完后手里还有股票
//        hold = fmax(hold, nohold - prices[i]);
//    }
//    return nohold;
//}




//使用临时数组
//void rotate(int* nums, int numsSize, int k) {
//    int tmp[numsSize];
//    //将原数组的值放到临时数组中
//    for (int i = 0; i < numsSize; i++)
//    {
//        tmp[i] = nums[i];
//    }
//    //将临时数组向右移动k个位置后，在放到原数组中
//    for (int i = 0; i < numsSize; i++)
//    {
//        nums[(i + k) % numsSize] = tmp[i];
//    }
//}

//多次反转
//void reverse(int* nums, int start, int end)
//{
//    int tmp = 0;
//    while (start < end)
//    {
//        tmp = nums[start];
//        nums[start] = nums[end];
//        nums[end] = tmp;
//        start++;
//        end--;
//    }
//}
//void rotate(int* nums, int numsSize, int k) {
//    //逆序数组
//    reverse(nums, 0, numsSize - 1);
//    k %= numsSize;
//    reverse(nums, 0, k - 1);
//    reverse(nums, k, numsSize - 1);
//}

