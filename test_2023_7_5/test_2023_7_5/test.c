//void moveZeroes(int* nums, int numsSize) {
//    int index = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (nums[i] != 0)
//        {
//            nums[index++] = nums[i];
//        }
//    }
//    for (; index < numsSize; index++)
//    {
//        nums[index] = 0;
//    }
//}

//void moveZeroes(int* nums, int numsSize) {
//    int index = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (nums[i] != 0)
//        {
//            int tmp = nums[index];
//            nums[index++] = nums[i];
//            nums[i] = tmp;
//        }
//    }
//}





//暴力解法
//int* twoSum(int* nums, int numsSize, int target, int* returnSize) {
//    for (int i = 0; i < numsSize; i++)
//    {
//        for (int j = 1 + i; j < numsSize; j++)
//        {
//            if (nums[i] + nums[j] == target)
//            {
//                int* ret = malloc(sizeof(int) * 2);
//                ret[0] = i;
//                ret[1] = j;
//                *returnSize = 2;
//                return ret;
//            }
//        }
//    }
//    *returnSize = 0;
//    return NULL;
//}





//bool isValidSudoku(char** board, int boardSize, int* boardColSize) {
//    int* flag = (int*)malloc(sizeof(int) * 10);
//    //行判断
//    for (int i = 0; i < 9; i++)
//    {
//        for (int k = 0; k <= 9; k++)
//            flag[k] = 0;
//        for (int j = 0; j < 9; j++)
//        {
//            if (board[i][j] != '.')
//            {
//                flag[board[i][j] - '0']++;
//                if (flag[board[i][j] - '0'] == 2)
//                    return false;
//            }
//        }
//    }
//    //列判断
//    for (int i = 0; i < 9; i++)
//    {
//        for (int k = 0; k <= 9; k++)
//            flag[k] = 0;
//        for (int j = 0; j < 9; j++)
//        {
//            if (board[j][i] != '.')
//            {
//                flag[board[j][i] - '0']++;
//                if (flag[board[j][i] - '0'] == 2)
//                    return false;
//            }
//        }
//    }
//    //3*3宫格判断
//    int count = 0;
//    for (int i = 0; i < 7; i += 3)
//    {
//        for (int k = 0; k <= 9; k++)
//            flag[k] = 0;
//        //判断每一列的三宫格
//        for (int j = 0; j < 9; j++)
//        {
//            //每一列中的每个三宫格判断完后，数组清零
//            if (count % 3 == 0)
//            {
//                for (int k = 0; k <= 9; k++)
//                    flag[k] = 0;
//            }
//            //判断每个三宫格
//            if (board[i][j] != '.')
//            {
//                flag[board[i][j] - '0']++;
//                if (flag[board[i][j] - '0'] == 2)
//                    return false;
//            }
//            if (board[i + 1][j] != '.')
//            {
//                flag[board[i + 1][j] - '0']++;
//                if (flag[board[i + 1][j] - '0'] == 2)
//                    return false;
//            }
//            if (board[i + 2][j] != '.')
//            {
//                flag[board[i + 2][j] - '0']++;
//                if (flag[board[i + 2][j] - '0'] == 2)
//                    return false;
//            }
//            //三宫格每执行完一行后count++;
//            count++;
//        }
//    }
//    return true;
//}

