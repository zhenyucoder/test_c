#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int a = 10;
//	printf("a=%d\n", a); 
//	return 0;
//}
//
//#include <stdio.h>
//
//int main()
//{
//    long long a, b;
//    scanf("%lld %lld", &a, &b);
//    //求最小公倍数
//    long long m = a > b ? a : b;//假设a 和 b的较大值是最小公倍数
//    while (1)
//    {
//        if (m % a == 0 && m % b == 0)
//        {
//            break;
//        }
//        m++;
//    }
//    printf("%lld\n", m);
//
//    return 0;
//}







//
//int main()
//{
//    long long a, b;
//    scanf("%lld %lld", &a, &b);
//    //求最小公倍数
//    int i = 1;
//    while (a * i % b)
//    {
//        i++;
//    }
//    printf("%lld\n", a * i);
//
//    return 0;
//}






//
//int main() 
// {
//    char arr[101];
//    //scanf("%s", arr);
//    //gets(arr);
//    //fgets(arr, 100, stdin);
//    printf("%s\n", arr);
//    return 0;
//}




//void reverse(char* left, char* right)
//{
//    while (left < right)
//    {
//        char tmp = *left;
//        *left = *right;
//        *right = tmp;
//        left++;
//        right--;
//    }
//}






//int main() 
//{
//    char arr[101];
//    //scanf("%s", arr);
//    gets(arr);/
//    //scanf("%[^\n]s", arr);
//    int len = strlen(arr);
//    reverse(arr, arr + len - 1);
//    //逆序每个单词
//    char* start = arr;
//    char* cur = arr;
//
//    while (*cur) {
//        while (*cur != ' ' && *cur != '\0')
//        {
//            cur++;
//        }
//        reverse(start, cur - 1);
//        start = cur + 1;
//        if (*cur == ' ')
//            cur++;
//    }
//    printf("%s\n", arr);
//    return 0;
//}




//int rob(int* nums, int numsSize)
//{
//    if (numsSize == 1)
//    {
//        return nums[0];
//    }
//
//    int dp[numsSize];
//    dp[0] = nums[0];
//    dp[1] = fmax(nums[0], nums[1]);
//
//    for (int i = 2; i < numsSize; i++)
//    {
//        dp[i] = fmax(dp[i - 1], dp[i - 2] + nums[i]);
//    }
//
//    return dp[numsSize - 1];
//}




char** fizzBuzz(int n, int* returnSize)
{
    char** answer = (char**)malloc(sizeof(char*) * n);
    for (int i = 1; i <= n; i++)
    {
        answer[i - 1] = (char*)malloc(sizeof(char) * 9);
        if (i % 3 == 0 && i % 5 == 0)
        {
            strcpy(answer[i - 1], "FizzBuzz");
        }
        else if (i % 3 == 0)
        {
            strcpy(answer[i - 1], "Fizz");
        }
        else if (i % 5 == 0)
        {
            strcpy(answer[i - 1], "Buzz");
        }
        else
        {
            sprintf(answer[i - 1], "%d", i);
        }
    }
    *returnSize = n;
    return answer;
