#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//    printf("Practice makes perfect!");
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    printf("v   v\n");
//    printf(" v v\n");
//    printf("  v\n");
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    printf("The size of short is %d bytes.\n", sizeof(short));
//    printf("The size of int is %d bytes.\n", sizeof(int));
//    printf("The size of long is %d bytes.\n", sizeof(long));
//    printf("The size of long long is %d bytes.\n", sizeof(long long));
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int a = 1234;
//    printf("%#o ", a);
//    printf("%#X\n", a);
//    return 0;
//}

//#include <stdio.h>
//int main(void)
//{
//	int num;
//	num = 2023;
//	printf("I")
//	return 0;
//}
//

#include <stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	/*if (a && b)
//	{
//		printf("hehe\n");
//	}*/
//	if (a || b)
//	{
//		printf("haha\n");
//	}
//
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//	int m = (a > b ? a : b);
//	printf("%d\n", m);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int d = (a += 2, b = 3, c = a + b, c -= 4);
//	printf("%d\n", d);
//	return 0;
//}

//下表引用[]
//int main()
//{
//	char arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	               //0 1 2 3 4 5 6 7 8 9
//	printf("%d\n", arr[5]);
//	return 0;
//}

//函数调用（）
//int Add(int x, int y)//C语言语法，不是函数调用
//{
//	return x + y;
//}
//int main()
//{
//	int z = Add(3, 5);
//	printf("%d%", z);//（）函数调用，调用Add函数
//	return 0;
//}

//typedef unsigned int u_int;
//typedef unsigned long long u_ll;
//typedef struct Node
//{
//	int date;
//	struct Node* next;
//}Node;

//void test(void)
//{
//	static int a = 5;
//	a++;
//	printf("%d ", a);
//}
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test();
//		i++;
//	}
//	return 0;
//}

//extern int g_val;
//int main()
//{
//	printf("%d\n", g_val);
//	return 0;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d%d", &a, &b);
//	int s = Add(a, b);
//	printf("%d\n", s);
//	return 0;
//}

//int main()
//{
//	//仅仅是建议作用
//	//建议放在寄存器中，但最终是不是真的放在寄存器中，取决于编译器
//	register int num = 4;
//	return 0;
//}

//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[20];
//};
struct Stu 
{
	char name[ 20];
	char hobby;
	char time;
};

int main()
{ 
	struct  Stu s = { "小黑子", "篮球", "两年半" };
	printf("%s %s %s", s.name, s.hobby, s.time);
	return 0;
}