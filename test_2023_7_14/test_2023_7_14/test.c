#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>



//void bubblr_sort(int arr[], int sz)
//{
//	//排序趟数
//	for (int i = 0; i < sz - 1; i++)
//	{
//		int tmp = 0;
//		//每趟比较对数
//		for (int j = 0; j < sz - 1 - i; j++)
//		{
//			//假设升序，交换元素
//			if (arr[i] > arr[i + 1])
//			{
//				tmp = arr[i];
//				arr[i] = arr[i + 1];
//				arr[i + 1] = tmp;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr[10] = { 10,9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz);
//	return 0;
//}
//void qsort(void* base, //指向排序的第一个元素
//		   size_t num, //排序元素的个数
//		   size_t size,//一个元素的大小，单位为字节
//		   int (*cmp)(const void*, const void*)//函数指针类型 — 这个函数指针指向的函数，能够比较base指向数组的两个元素
//);

//int cmp_int(const void* p1, const void* p2)
//{
//	return (*(int*)p1 - *(int*)p2);
//}
//
//void Swap(char* buf1, char* buf2, int size)//交换arr[i]和arr[i+1]
//{
//	char tmp = 0;
//	while (size--)
//	{
//		tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_sort(void* base, int num, int size, int (*cmp)(const void*, const void*))
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < num; i++)
//	{
//		//一趟比较对数
//		//假设升序
//		for (int j = 0; j < num - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)//比较两个元素，需要将arr[j]和arr[j+1]的地址传给cmp
//			{
//				//交换
//				Swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
//			}
//		}
//	}
//}
//
//
////测试bubble_sort排序整型数据
//void test1()
//{
//	int arr[] = { 4,2,5,8,3,8,34,23,1,54 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr,sz, sizeof(arr[0]), cmp_int);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//
//int main()
//{
//	test1();
//	return 0;
//}






//#include <string.h>
//
//void Swap(char* buf1, char* buf2, int size)//交换arr[i]和arr[i+1]
//{
//	char tmp = 0;
//	while (size--)
//	{
//		tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_sort(void* base, int num, int size, int (*cmp)(const void*, const void*))
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < num; i++)
//	{
//		//一趟比较对数
//		//假设升序
//		for (int j = 0; j < num - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)//比较两个元素，需要将arr[j]和arr[j+1]的地址传给cmp
//			{
//				//交换
//				Swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
//			}
//		}
//	}
//}
//
//
////测试bubble_sort排序结构体数据
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//int cmp_stu_by_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name ,((struct Stu*)p2)->name);
//}
//
//void test2()
//{
//	struct Stu arr[] = { {"zahngsan",20},{"lisi",12},{"wangwu",43} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_name);
//}
//
//
//int main()
//{
//	test2();
//	return 0;
//}




//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3, c + 2, c + 1,c };
//	char*** cpp = cp;
//
//	printf("%s\n", **++cpp);//POINT
//	printf("%s\n", *-- * ++cpp + 3);//ER
//	printf("%s\n", *cpp[-2] + 3);//ST
//	printf("%s\n", cpp[-1][-1] + 1);//EW
//	return 0;
//}


#include <string.h>
//int main()
//{
//	//stelen返回size_t类型，结果始终>=0
//	if (strlen("abc") - strlen("abcdef") > 0)
//	{
//		printf("大于\n");
//	}
//	else
//	{
//		printf("小于\n");
//	}
//	return 0;
//}


//模拟实现strlen()函数
//实现方法一
//size_t My_strlen(const char* str)
//{
//	size_t count = 0;
//	while (*str++)
//	{
//		count++;
//	}
//	return count;
//}

//方法二：双指针
//size_t My_strlen(const char* str)
//{
//	char* p1 = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str - p1;
//}

//方法三：递归
//size_t My_strlen(const char* str)
//{
//	int count = 0;
//	if (*str == '\0')
//		return 0;
//	else
//		return 1 + My_strlen(++str);
//}
//int main()
//{
//	size_t sz = My_strlen("abc");
//	printf("%u\n", sz);
//	return 0;
//}




