#define _CRT_SECURE_NO_WARNINGS

#include "SeqList.h"


void SLInit(SL* ps)
{
	assert(ps);
	ps->a = (SLDateType*)malloc(4 * sizeof(SLDateType));
	if (ps->a == NULL)
	{
		perror("malloc");
		exit(-1);
	}
	//开辟成功
	ps->capacity = 4;
	ps->size = 0;
}


void SLDestory(SL* ps)
{
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

void SLPrint(SL* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SLCheakCapacity(SL* ps)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		SLDateType* tmp = (SLDateType*)realloc(ps->a, ps->capacity * sizeof(SLDateType) * 2);
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		ps->a = tmp;
		ps->capacity *= 2;
	}
}


void SLPushBack(SL* ps, SLDateType x)
{
	assert(ps);
	/*SLCheakCapacity(ps);
	ps->a[ps->size] = x;
	ps->size++;*/

	SLInsert(ps, ps->size, x);
}


void SLPopBack(SL* ps)
{
	assert(ps);
	assert(ps->size >= 0);
	/*ps->size--;*/
	SLErase(ps, 0);
}


void SLPushFront(SL* ps, SLDateType x)
{
	assert(ps);
	SLCheakCapacity(ps);

	//移动数据
	/*int i = ps->size-1;
	while (i >= 0)
	{
		ps->a[i + 1] = ps->a[i];
		i--;
	}
	ps->a[0] = x;
	ps->size++;*/
	SLInsert(ps, 0, x);
}


void SLPopFront(SL* ps)
{
	assert(ps);
	assert(ps->size >= 0);
	/*for (int begin = 1; begin < ps->size; begin++)
	{
		ps->a[begin - 1] = ps->a[begin];
	}
	ps->size--;*/
	SLErase(ps, 0);
}


int SLFind(SL* ps, SLDateType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		if (x == ps->a[i])
			return i;
	}
	return -1;
}


void SLInsert(SL* ps, int pos, SLDateType x)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	SLCheakCapacity(ps);

	int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[pos] = x;
	ps->size++;

}



void SLErase(SL* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);

	int begin = pos+1;
	while (begin < ps->size)
	{
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;
}



void SLModify(SL* ps, int pos, SLDateType x)
{
	assert(ps);
	assert(pos >= 0 && pos < ps->size);
	ps->a[pos] = x;
}
