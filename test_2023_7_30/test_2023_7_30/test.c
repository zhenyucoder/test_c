#define _CRT_SECURE_NO_WARNINGS
#include "SeqList.h"

//TestSeqList1()
//{
//	SL s;
//	SLInit(&s);
//	SLPushBack(&s, 1);
//	SLPushBack(&s, 2);
//	SLPushBack(&s, 3);
//	SLPushBack(&s, 4);
//	SLPushBack(&s, 5);
//	SLPushBack(&s, 6);
//	SLPushBack(&s, 7);
//	SLPushBack(&s, 8);
//	SLPrint(&s);
//	SLPopBack(&s);
//	SLPopBack(&s);
//	SLPopBack(&s);
//	SLPopBack(&s);
//	SLPrint(&s);
//	
//	SLDestory(&s);
//}


//TestSeqList2()
//{
//	SL s;
//	SLInit(&s);
//	SLPushBack(&s, 1);
//	SLPushBack(&s, 2);
//	SLPushBack(&s, 3);
//	SLPushBack(&s, 4);
//	SLPrint(&s);
//	SLPushFront(&s, 10);
//	SLPushFront(&s, 11);
//	SLPushFront(&s, 12);
//	SLPrint(&s);
//
//	SLPopFront(&s);
//	SLPopFront(&s);
//	SLPrint(&s);
//	SLInsert(&s, 2, 110);
//	SLInsert(&s, 1, 130);
//	SLPrint(&s);
//	SLErase(&s, 1);
//	SLErase(&s, 1);
//	SLPrint(&s);
//	SLModify(&s, 1, 10000);
//	SLPrint(&s);
//	SLDestory(&s);
//}

enum
{
	OXIT,
	SLPUSHFRONT,
	SLPOPFRONT,
	SLPUSHBACK,
	SLPOPBACK,
	SLINSERT,
	SLERASE,
	ELMODIFY
};
void menu()
{
	printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
	printf("XXXXX  1.头插      2.头删  XXXXXX\n");
	printf("XXXXX  3.尾插      4.尾删  XXXXXX\n");
	printf("XXXXX  5.添加      6.删除  XXXXXX\n");
	printf("XXXXX  7.修改      0.退出  XXXXXX\n");
	printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");
}
int main()
{
	//TestSeqList2();
	SL s1;
	SLInit(&s1);
	int option = 0;
	int x = 0;
	int y = 0;
	do
	{
		menu();
		printf("请选择:>\n");
		scanf("%d", &option);
		switch (option)
		{
		case OXIT:
			printf("程序退出\n");
			break;
		case SLPUSHFRONT:
			printf("请输入需要头插的数据:>");
			scanf("%d", x);
			SLPushFront(&s1, x);
			break;
		case SLPOPFRONT:
			printf("请输入需要头删的数据:>");
			scanf("%d", x);
			SLPopFront(&s1, x);
			break;
		case SLPUSHBACK:
			printf("请输入需要尾插的数据:>");
			scanf("%d", x);
			SLPushBack(&s1, x);
			break;
		case SLPOPBACK:
			printf("请输入需要尾删的数据:>");
			scanf("%d", x);
			SLPopBack(&s1, x);
			break;
		case SLINSERT:
			printf("请依次输入需要添加数据的下标,数据; 以空格空开:>");
			scanf("%d%d", x,y);
			SLInsert(&s1, x,y);
			break;
		case SLERASE:
			printf("请依次输入需要删除数据的下标,数据; 以空格空开:>");
			scanf("%d%d", x, y);
			SLErase(&s1, x, y);
			break;
		default:
			printf("选择错误,请重新选择:>\n");
			break;
		}
	} while (option);

	SLDestory(&s1);
	return 0;
}