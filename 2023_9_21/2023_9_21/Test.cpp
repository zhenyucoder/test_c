#include <iostream>
//#include <stdio.h>

//using namespace std;
using std::cout;
using std::endl;


//C++函数重载，函数名修饰导致名字不同，可以区分
//void func(int x, double y);//?func@@YAXHN@Z
////{
////	cout << "void func(int x, double y)" << endl;
////}
//
//void func(double y, int x);//?func@@YAXNH@Z
////{
////	cout << "void func(double y, int x)" << endl;
////}
//
//int main()
//{
//	func(1, 1.1);
//	func(1.1, 1);
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int& b = a;
//	a++;
//	b++;
//	cout << "a=" << a << endl;
//	cout << "b=" << b << endl;
//	cout << &a << endl;
//	cout << &b << endl;
//	return 0;
//}




//struct TreeNode {
//	    int val;
//	    struct TreeNode *left;
//	    struct TreeNode *right;
//	};
//	 	
//int TreeSize(struct TreeNode* root)
//{
//	if (root == NULL)
//	    return 0;
//	else
//	    return TreeSize(root->left) + TreeSize(root->right) + 1;
//}
//	
//
//void _preorderTraversal(struct TreeNode* root, int* a, int& ri)
//{
//	if (root == NULL)
//	    return;
//	
//	printf("[%d] %d ", ri, root->val);
//	a[ri] = root->val;
//	++ri;
//	_preorderTraversal(root->left, a, ri);
//	_preorderTraversal(root->right, a, ri);
//}
//
//int* preorderTraversal(struct TreeNode* root, int& returnSize) {
//    int size = TreeSize(root);
//    int* a = (int*)malloc(sizeof(int) * size);
//    int i = 0;
//    _preorderTraversal(root, a, i);
//
//    returnSize = size;
//
//    return a;
//}
//
//void Swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//int main()
//{
//
//	int size;
//	preorderTraversal(nullptr, size);	
//
//	int a = 10;
//	int b = 20;
//	Swap(a, b);
//	cout << "a=" << a << endl;
//	cout << "b=" << b << endl;
//	return 0;
//}


//typedef struct ListNode
//{
//	int data;
//	struct ListNode* next;
//}ListNode,*PListNode;
//
////单链表尾插CPP玩法
////void PushBack(ListNode*& ps, int x)
//void PushBack(PListNode& phead, int x)
//{
//	ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
//	if (phead == NULL)
//	{
//		phead = newnode;
//	}
//	else
//	{
//		//.....
//	}
//}
//
//
//int main()
//{
//	ListNode* plist = NULL;
//	PushBack(plist, 1);
//	PushBack(plist, 2);
//	PushBack(plist, 3);
//	return 0;
//}



//int main()
//{
//	//int& a = 10;//在定义时必须初始化
//	 
//	int a = 10;
//	int& b = a;
//	int& c = a;
//	int& d = b;
//
//	int x = 1;
//	b = x;//赋值，引用一旦引用了某一个实体，就不可再引用其他实体,不可以改变其指向
//	return 0;
//}


//int& count()
//{
//	int n = 10;
//	n++;
//	return n;
//}
//
//int& Add(int x, int y)
//{
//	int a[1000];
//	int c = x + y;
//	return c;
//}
//
//int main()
//{
//	//int& ret = count();
//	int& ret = Add(3, 4);
//	cout << "ret=" << ret << endl;
//	cout << "ret=" << ret << endl;
//
//	return 0;
//}


// 传引用传参（任何时候都可以用）
// 1、提高效率
// 2、输出型参数（形参的修改，影响的实参）
//#include <time.h>
//struct A { int a[10000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}


// 传引用返回（出了函数作用域对象还在才可以用）
// 1、提高效率
// 2、修改返回对象
// 值返回
//A a;//全局变量，可以使用应用返回
//A TestFunc1() { return a; }
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestReturnByRefOrValue();
//	//TestRefAndValue();
//	return 0;
//}


//#include <assert.h>
//typedef struct SeqNode
//{
//	int a[10];
//	int size;
//}SeqNode;
//
//int& SLAT(SeqNode& ps, int i)
//{
//	assert(i < ps.size);
//	return ps.a[i];
//}
//
//int main()
//{
//	SeqNode s;
//	s.size = 3;
//	int& ret = SLAT(s, 2);
//	SLAT(s, 2) = 12;
//	cout << SLAT(s, 2) << endl;
//	return 0;
//}

int func()
{
	int a = 0;
	return a;
}
int main()
{
	const int& ret = func();//const引用后，会延长临时变量的生命周期
	const int a = 10;
	//int& b = a;//权限扩大
	int b = a;//赋值，只有设计引用时，才会涉及权限问题

	const int& b = a;//平移
	int c = 12;
	const int& d = c;//缩小

	int i = 10;
	double d = i;
	const double d = i;//C/C++中，类型转化会将原值存放到一个临时变量中，而临时变量具有常性
	return 0;
}