#include <stdio.h>
#include <stdlib.h>


////第一种声明方式
//struct S
//{
//	int i;
//	int arr[];//柔性数组，前面至少有一个成员
//};
//
////第二种声明方式
//struct S
//{
//	int i;
//	int arr[0];//柔性数组
//};


//typedef struct S
//{
//	int i;
//	int arr[];
//}type_s;
//
//int main()
//{
//	int i = 0;
//	type_s* p = (type_s*)malloc(sizeof(type_s) + 100 * sizeof(int));
//	//业务处理
//	p->i = 100;
//	for (i = 0; i < 100; i++)
//	{
//		p->arr[i] = i + 1;
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}



//typedef struct S
//{
//	int i;
//	int* p_a;
//}type_s;
//
//
//int main()
//{
//	type_s* p = (type_s*)malloc(sizeof(type_s));
//	if (p == NULL)
//	{
//		perror("malloc->type_s");
//		return 1;
//	}
//	p->i = 100;
//	int* ptr = realloc(p->p_a, 4 * sizeof(int));
//	if (ptr == NULL)
//	{
//		perror("malloc->p_s");
//		return 1;
//	}
//	else
//	{
//		p->p_a = ptr;
//	}
//	//业务处理
//	for (int i = 0; i < 100; i++)
//	{
//		p->p_a[i] = i + 1;
//	}
//
//	//释放
//	free(p->p_a);
//	p->p_a = NULL;
//	free(p);
//	p = NULL;
//	return 0;
//}



//int main()
//{
//	int* p = (int*)calloc(10, sizeof(int));
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d:%p\n", p[i],&p[i]);
//	}
//	
//	return 0;
//}


//long long Fib(int N)
//{
//	if (N < 3)
//	{
//		return 1;
//	}
//	return Fib(N - 1) + Fib(N - 2);
//}


//void Func1(int N)
//{
//	int count = 0;
//	for (int i = 0; i < N; i++)
//	{
//		for (int j = 0; j < N; j++)
//		{
//			++count;
//		}
//	}
//
//	for (int k = 0; k < 2 * N; k++)
//	{
//		++count;
//	}
//
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//	return 0;
//}



//union Un
//{
//	int i;
//	char c;
//};
//
//int main()
//{
//	union Un u = { 0 };
//	printf("%p\n", &u);
//	printf("%p\n", &u.i);
//	printf("%p\n", &u.c);
//	return 0;
//}


//int cheak_sys()
//{
//	union Un
//	{
//		int i;
//		char c;
//	}un = { .i = 1 };
//	return un.c;
//}
//
//int main()
//{
//	int ret = cheak_sys();
//	if (ret == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}


union Un1
{
	char c[5];
	int i;
};
union Un2
{
	short c[7];
	int i;
}; 

int main()
{
	printf("%d\n", sizeof(union Un1));
	printf("%d\n", sizeof(union Un2));
	return 0;
}