


//bool isUnivalTree(struct TreeNode* root) {
//    if (!root) 
//    {
//        return true;
//    }
//    if (root->left) 
//    {
//        if (root->val != root->left->val || !isUnivalTree(root->left)) 
//        {
//            return false;
//        }
//    }
//    if (root->right) 
//    {
//        if (root->val != root->right->val || !isUnivalTree(root->right)) 
//        {
//            return false;
//        }
//    }
//    return true;
//}





//bool isSameTree(struct TreeNode* p, struct TreeNode* q){
//    if (p == NULL && q == NULL) 
//    {
//        return true;
//    }
//    else if (p == NULL || q == NULL) 
//    {
//        return false;
//    }
//    else if (p->val != q->val) 
//    {
//
//        return false;
//    }
//    else {
//        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//    }
//}

//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (p == NULL && q == NULL) 
//    {
//        return true;
//    }
//    else if (p == NULL || q == NULL) 
//    {
//        return false;
//    }
//
//    struct TreeNode** que1 = (struct TreeNode**)malloc(sizeof(struct TreeNode*));
//    struct TreeNode** que2 = (struct TreeNode**)malloc(sizeof(struct TreeNode*));
//    int queleft1 = 0, queright1 = 0;
//    int queleft2 = 0, queright2 = 0;
//    que1[queright1++] = p;
//    que2[queright2++] = q;
//
//    while (queleft1 < queright1 && queleft2 < queright2) 
//    {
//        struct TreeNode* node1 = que1[queleft1++];
//        struct TreeNode* node2 = que2[queleft2++];
//        if (node1->val != node2->val) 
//        {
//            return false;
//        }
//        struct TreeNode* left1 = node1->left;
//        struct TreeNode* right1 = node1->right;
//        struct TreeNode* left2 = node2->left;
//        struct TreeNode* right2 = node2->right;
//        if ((left1 == NULL) ^ (left2 == NULL)) 
//        {
//            return false;
//        }
//        if ((right1 == NULL) ^ (right2 == NULL)) 
//        {
//            return false;
//        }
//        if (left1 != NULL) 
//        {
//            queright1++;
//            que1 = realloc(que1, sizeof(struct TreeNode*) * queright1);
//            que1[queright1 - 1] = left1;
//        }
//        if (right1 != NULL) 
//        {
//            queright1++;
//            que1 = realloc(que1, sizeof(struct TreeNode*) * queright1);
//            que1[queright1 - 1] = right1;
//        }
//        if (left2 != NULL) 
//        {
//            queright2++;
//            que2 = realloc(que2, sizeof(struct TreeNode*) * queright2);
//            que2[queright2 - 1] = left2;
//        }
//        if (right2 != NULL) 
//        {
//            queright2++;
//            que2 = realloc(que2, sizeof(struct TreeNode*) * queright2);
//            que2[queright2 - 1] = right2;
//        }
//    }
//    return queleft1 == queright1 && queleft2 == queright2;
//}





//bool _isSymmetric(struct TreeNode* Leftroot, struct TreeNode* Rightroot) {
//
//    if (Leftroot == NULL && Rightroot == NULL) 
//    {
//        return true;
//    }
//    
//    if (Leftroot == NULL || Rightroot == NULL) 
//    {
//        return false;
//    }
//
//    if (Leftroot->val != Rightroot->val) 
//    {
//        return false;
//    }
//
//    return _isSymmetric(Leftroot->left, Rightroot->right)
//        && _isSymmetric(Leftroot->right, Rightroot->left);
//}
//
//bool isSymmetric(struct TreeNode* root) 
//{
//    return _isSymmetric(root->left, root->right);
//}



//void preorder(struct TreeNode* root, int* res, int* resSize) 
//{
//    if (root == NULL) 
//    {
//        return;
//    }
//    res[(*resSize)++] = root->val;
//    preorder(root->left, res, resSize);
//    preorder(root->right, res, resSize);
//}
//
//int* preorderTraversal(struct TreeNode* root, int* returnSize) {
//
//    int* res = malloc(sizeof(int) * 2000);
//    *returnSize = 0;
//    preorder(root, res, returnSize);
//    return res;
//}



void inorder(struct TreeNode* root, int* res, int* resSize) {
    if (!root) 
    {
        return;
    }
    inorder(root->left, res, resSize);
    res[(*resSize)++] = root->val;
    inorder(root->right, res, resSize);
}

int* inorderTraversal(struct TreeNode* root, int* returnSize) 
{
    int* res = malloc(sizeof(int) * 501);
    *returnSize = 0;
    inorder(root, res, returnSize);
    return res;
}

