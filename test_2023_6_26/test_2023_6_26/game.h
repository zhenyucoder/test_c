#pragma once
#define Row 6
#define Col 6
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//初始化函数
void InitBoard(char board[Row][Col], int row, int col);
//打印函数
void DisplayBoard(char board[Row][Col], int row, int col);
//玩家下棋
void PlayerMove(char board[Row][Col], int row, int col);
//电脑下棋
void ComputerMove(char board[Row][Col], int row, int col);



//判断输赢
//玩家赢 - ‘O'
//电脑赢 - 'X'
//平局 - ‘Q'
//继续 - 'C'
char IsWin(char board[Row][Col], int row, int col);
