#define _CRT_SECURE_NO_WARNINGS
#include "game.h"


//棋盘初始化
void InitBoard(char board[Row][Col], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}



void DisplayBoard(char board[Row][Col], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		//1.打印数据
		int j = 0;
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)
				printf("|");
		}
		printf("\n");
		//2.打印分割线
		if (i < row - 1)
		{
			int j = 0;
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
		}
		printf("\n");
	}
}





//玩家下棋
void PlayerMove(char board[Row][Col], int row, int col)
{
	int x = 0;
	int y = 0;
	printf("玩家下棋，请输入下棋坐标\n");
	while (1)
	{
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			//坐标正确
			//判断坐标是否被占用
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = 'O';
				break;
			}
			else
			{
				printf("坐标被占用，不能落子，请重新输入\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入坐标\n");
		}
	}
}




//电脑下棋
void ComputerMove(char board[Row][Col], int row, int col)
{
	int x = 0;
	int y = 0;

	printf("电脑下棋:>\n");
	while (1)
	{
		x = rand() % row;
		y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = 'X';
			break;
		}
	}
}

//判断棋盘是否下子已满
int Is_full(char board[Row][Col], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
				return 0;
		}
	}
	return 1;
}


//判断输赢 — 三子棋
//char IsWin(char board[Row][Col], int row, int col)
//{
//	//赢
//	//行
//	int i = 0;
//	for (i = 0; i < row; i++)
//	{
//		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
//			return board[i][0];
//	}
//
//	//列
//	for (i = 0; i < col; i++)
//	{
//		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != ' ')
//			return board[0][i];
//	}
//
//	//对角线
//	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != ' ')
//		return board[0][0];
//	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
//		return board[1][1];
//
//
//	//平局
//	if (Is_full(board, Row, Col) == 1)
//		return 'Q';
//	return 'C';
//}





//判断输赢 - N子棋
char IsWin(char board[Row][Col], int row, int col)
{
	//赢
	//行
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		int count = 0;
		//如果该行成立后，遍历访问数组后count的值变为col-1
		for (j = 0; j < col - 1; j++)
		{
			if (board[i][j] == board[i][j + 1] && board[i][j] != ' ' )
				count++;
		}
		if (count == col - 1)
			return board[i][j];
	}

	//列
	for (i = 0; i < col; i++)
	{
		int j = 0;
		int count = 0;
		//如果该列成立后，遍历访问数组后count的值变为col-1
		for (j = 0; j < row - 1; j++)
		{
			if (board[j][i] == board[j+1][i] && board[j][i] != ' ')
				count++;
		}
		if (count == row - 1)
			return board[j][i];
	}

	//对角线
	for (i = 0; i < row-1; i++)
	{
		//如果对角线成立，这遍历访问后，i的值变为row-2
		if (board[i][i] == board[i + 1][i + 1] && board[i][i] != ' ')
		{
			if (i == row - 2)
				return board[i][i];
		}
		else
			break;
	}

	for (i = 0; i < row - 1; i++)
	{
		if (board[i][row - 1 - i] == board[i + 1][row - 2 - i] && board[i][row - 1] != ' ')
		{
			if (i = row - 2)
				return board[i][row - 1 - i];
		}
		else
			break;
	}


	//平局
	if (Is_full(board, Row, Col) == 1)
		return 'Q';
	//继续
	return 'C';
}


