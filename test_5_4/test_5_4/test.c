#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

//int main()
//{
//	char arr1[] = "hello bit";
//	char arr2[20] = "xxxxxxxxxxxxxxx";
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//int main()
//{
//	char arr[] = "hello bit";
//	memset(arr, 'x', 6);
//	printf("%s", arr);
//	return 0;
//}

//int get_max(int x, int y)
//{
//	return (x > y ? x : y);
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int m = get_max(a, b);
//	printf("%d\n", m);
//	return 0;
//}

//#include <math.h>
//
//int is_prime(int x)
//{
//	int j = 0;
//	for (j = 2; j <= sqrt(x); j++)
//	{
//		if (x % j == 0)
//			return 0;
//	}
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i); 
//
//	if (is_prime(i) == 1)
//	{
//		printf("%d是素数", i);
//	}
//	else
//	{
//		printf("%d不是", i);
//	}
//
//	return 0;
//}

//int is_leap_year(int x)
//{
//	if (((x % 4 == 0) && (x % 100 != 0)) || (x % 400 == 0))
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	int count = 0;
//
//	for (n = 1000; n <= 2000; n++)
//	{
//		if (is_leap_year(n) == 1)
//		{
//			printf("%d ", n);
//			count++;
//		}
//	}
//
//	printf("\ncount=%d", count);
//	return 0;
//}0

//函数实现有序数组二分查找
int binary_search(char arr[], int k, int sz)
{
	int left = 0;
	int right = sz - 1;
	
	while (left <= right)
	{
		int mid = (left + right) / 2;

		if (arr[mid] > k)
		{
			right=mid -1;
		}
		else if(arr[mid]<k)
		{
			left = mid + 1;
		}
		else
		{
			return mid;
		}

	}
	return -1;
}
int main()
{
	char arr[] = { 2,3,4,5,6,8,9,12,13,14,17,18,19,20 };
	int k = 13;
	int sz = sizeof(arr) / sizeof(arr[0]);
	//printf("%d", sz);
	int ret = binary_search(arr, k, sz);

	if (ret == -1)
	{
		printf("找不到\n");
	}
	else
	{
		printf("找到了，下标是:%d", ret);
	}

	return 0;
}