#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <assert.h>




//int main()
//{
//	char arr1[20] = "xxxxxxxxxxxxxxx";
//	char arr2[] = "hello boy";
//	strcpy(arr1+2, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//模拟实现strcpy()函数
//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		/**dest++ = *src++;*/
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello world";
//	char arr2[] = "xxxxx";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}





//int main()
//{
//	char arr1[20] = "123dferg";
//	char arr2[] = "hello";
//	strcat(arr1, arr2);
//	//strcat(arr1, ar1);//不可追加自身
//	return 0;
//}

//模拟实现strcmp()函数
//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	//找到dest'\0'的位置
//	while (*dest)
//	{
//		dest++;
//	}
//	//追加src
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}




//int main()
//{
//	printf("%d\n", strcmp("bbcdef", "abcdefs"));
//	return 0;
//}

//模拟实现strcmp()函数
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	int ret = my_strcmp("bbq", "bbw");
//	printf("%d\n", ret);
//	return 0;
//}




//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "xxx";
//	strncpy(arr1, arr2, 5);
//	return 0;
//}


//int main()
//{
//	char arr1[20] = "abcdef\0yyyyyyyy";
//	char arr2[] = "xxx";
//	strncat(arr1, arr2, 5);
//	return 0;
//}

//int main()
//{
//	char arr1[20] = "abcdef\0yyyyyyyy";
//	char arr2[] = "abcdef";
//	strncmp(arr1, arr2, 5);
//	return 0;
//}




//模拟实现strstr函数
//char* my_strstr(char *str1, char* str2)
//{
//	char* cp = str1;
//	char* s1 = cp;
//	char* s2 = str2;
//
//	if (*str2 == '\0')
//		return str1;
//	while (*cp)
//	{
//		//开始匹配
//		s1 = cp;
//		s2 = str2;
//		while (*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//			return cp;
//		
//		cp++;
//	}
//	return NULL;
//}
//
//
//
//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//
//	char* ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//		printf("%s\n", ret);
//	else
//		printf("找不到\n");
//
//	return 0;
//}





//int main()
//{
//	char arr[] = "zyuwang@yeah.net@666#777";
//	char copy[30];
//	strcpy(copy, arr);
//
//	char srp[] = "@.#";
//	char* ret = NULL;
//
//	for (ret = strtok(copy, srp); ret != NULL; ret = strtok(NULL, srp))
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}




//
//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d:%s\n", i, strerror(i));
//	}
//	return 0;
//}



//int main()
//{
//	int arr[20] = { 0 };
//	gets(arr);
//	char* p= arr;
//	while (*p)
//	{
//		if (isupper(*p))
//			*p = tolower(*p);
//		p++;
//	}
//	printf("%s\n",arr);
//	return 0;
//}



//size_t my_strlen(const char* str)
//{
//  assert(str);
//	size_t count = 0;
//	while (*str++)
//	{
//		count++;
//	}
//	return count;
//}

//指针实现
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	char* start = str;
//	while (*str)
//	{
//		str++;
//	}
//	return str - start;
//}

//递归实现
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	if (*str == '\0')
//		return 0;
//	else
//	{
//		return 1 + my_strlen(str + 1);
//		count++;
//	}
//}
//
//
//int main()
//{
//	char arr[] = "asffefs";
//	size_t ret = my_strlen(arr);
//	printf("%u\n", ret);
//	return 0;
//}