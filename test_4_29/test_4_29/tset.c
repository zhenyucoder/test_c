#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 2;
//	printf("%d %d %d", a, 2, a - b);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int x = 100;
//	printf("dec=%d; octal=%o; hex=%x\n", x, x, x);
//	printf("dec=%d; octal=%#o; hex=%#x\n", x, x, x);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i = 2147483647;
//	unsigned int j = 4294967295;
//	printf("%d %d %d\n", i, i + 1, i + 2);
//	printf("%u %u %u\n", j, j + 1, j + 2);
//	return 0;
//}

//#include<stdio.h>
//
//int main()
//{
//    double price = 0.0;
//    int month = 0;
//    int day = 0;
//    int flag = 0;
//
//    scanf("%lf %d %d %d", &price, &month, &day, &flag);
//    if (month == 11 && day == 11)
//    {
//        price = price * 0.7 - 50.0 * flag;
//    }
//    else if (month == 12 && day == 12)
//    {
//        price = price * 0.8 - 50 * flag;
//    }
//    if (price < 0)
//    {
//        printf("0.00\n");
//    }
//    else
//    {
//        printf("%.2lf\n", price);
//    }
//
//    return 0;
//}

#include<stdio.h>
int main()
{
	int i, j;
	int k, h, n, z, s, m;
	int rowbegin, rowend, rankbegin, rankend;

	scanf("%d", &n);		//输入数值

	int X = 3 * (1 << n - 1);
	int Y = 5 * (1 << n - 1) + (1 << n - 1) - 1;

	char b[X][Y];			//定义一个刚好可以存放图形的二维数组


	for (i = 0; i < X; i++)			//将数组初始化，全为空格
	{
		for (j = 0; j < Y; j++)
		{

			b[i][j] = ' ';


		}

	}
	for (i = 0; i < 3; i++)			//将输入为 1 时的图形存放到数组内
	{
		for (j = 0; j < 5; j++)
		{
			if ((i == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0) || i == j)
			{
				b[i][j] = '*';
			}
			else
			{
				b[i][j] = ' ';
			}

		}

	}

	for (h = 0, z = 0, k = 0, s = 0, m = 0; s < n + n - 1; h++, z++, s++, k = 0)	//控制循环的次数
	{

		if (z > 1)
		{
			z = 0;
			h--;
			k = h;
			m = k;
		}

		rowbegin = (k > 0) * 3 * (1 << k - 1);		//控制行的开始数值
		rowend = 3 * (1 << m);					//控制行的结束数值

		rankbegin = (h > 0) * (5 * (1 << (h - 1)) + (1 << (h - 1))) - ((k > 0) << k - 1) * (3); //控制列的开始数值
		rankend = 5 * (1 << h) + (1 << h) - 1 - ((k > 0) << k - 1) * (3);					//控制列的结束数值

		for (i = rowbegin; i < rowend; i++)
		{
			for (j = rankbegin; j < rankend; j++)
			{
				b[i][j] = b[i - rowbegin][j - rankbegin];
			}
		}
	}


	for (i = X - 1; i >= 0; i--)				//输出树叶
	{
		for (j = Y - 1; j >= 0; j--)
		{
			printf("%c", b[i][j]);

		}

		putchar('\n');
	}

	for (i = 0; i < n; i++)				//输出树干
	{
		for (j = 0; j < Y; j++)
		{
			printf("%c", b[X - 1][j]);
		}

		putchar('\n');
	}

	return 0;
}