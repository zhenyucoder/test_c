#define _CRT_SECURE_NO_WARNINGS
#include "SList.h"



//void TestSList1()
//{
//	int n;
//	printf("请输入链表的长度：>\n");
//	scanf("%d", &n);
//	printf("请依次输入每个节点的值\n");
//	SLTNode* plist = NULL;
//
//	for (int i = 0; i < n; i++)
//	{
//		int value;
//		scanf("%d", &value);
//		SLTNode* newnode = BuySListNode(value);
//
//		//头插
//		newnode->next = plist;
//		plist = newnode;
//	}
//	SLTPrint(plist);
//
//	SLTPushBack(&plist,1000);
//	SLTPrint(plist);
//	SLTPushBack(&plist, 2000);
//	SLTPushBack(&plist, 3000);
//	SLTPushBack(&plist, 4000);
//	SLTPushBack(&plist, 5000);
//	SLTPrint(plist);
//}



void TestSList2()
{
	SLTNode* phead = NULL;
	SLTPushBack(&phead, 1000); 
	SLTPushBack(&phead, 2000); 
	SLTPushBack(&phead, 3000); 
	SLTPushBack(&phead, 4000); 
	SLTPushBack(&phead, 5000); 
	SLTPrint(phead);
	SLTPushFront(&phead, 1);
	SLTPushFront(&phead, 2);
	SLTPushFront(&phead, 3);
	SLTPushFront(&phead, 4);
	SLTPushFront(&phead, 5);
	SLTPrint(phead);
	SLTPopBack(&phead);
	SLTPrint(phead);
	SLTPopBack(&phead);
	SLTPopBack(&phead);
	SLTPopBack(&phead);
	SLTPrint(phead);
	SLTPopFront(&phead);
	SLTPopFront(&phead);
	SLTPopFront(&phead);
	SLTPopFront(&phead);
	SLTPopFront(&phead);
	SLTPopFront(&phead);
	//SLTPopFront(&phead);
	SLTPrint(phead);

}
int main()
{
	TestSList2();
	return 0;
}