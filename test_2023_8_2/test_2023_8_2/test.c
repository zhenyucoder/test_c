#define _CRT_SECURE_NO_WARNINGS
#include "SList.h"

//TestSList1()
//{
//	SLTNode* plist = NULL;
//	printf("请输入链表的长度:>\n");
//	int n;
//	scanf("%d", &n);
//	printf("请输入每个节点的值\n");
//	for (int i = 0; i < n; i++)
//	{
//		int value;
//		scanf("%d", &value);
//		SLTNode* newnode = BuySListNode(value);
//		
//		//头插
//		newnode->next = plist;
//		plist = newnode;
//	}
//	SLTPrint(plist);
//	STLPushBack(&plist, 1000);
//	SLTPrint(plist);
//
//}

//TestSList2()
//{
//	SLTNode* plist = NULL;
//	SLTPushBack(&plist,1000);
//	SLTPushBack(&plist, 2000);
//	SLTPushBack(&plist, 3000);
//	SLTPushBack(&plist, 4000);
//	SLTPrint(plist);
//	SLTPushFront(&plist, 1);
//	SLTPushFront(&plist, 2);
//	SLTPushFront(&plist, 3);
//	SLTPushFront(&plist, 4);
//	SLTPrint(plist);
//	SLTPopBack(&plist);
//	SLTPopBack(&plist);
//	SLTPrint(plist);
//	SLTPopBack(&plist);
//	SLTPopBack(&plist);
//	SLTPrint(plist);
//	SLTPopFront(&plist);
//	SLTPopFront(&plist);
//	SLTPopFront(&plist);
//	SLTPrint(plist);
//
//}


TestSList3()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1000);
	SLTPushBack(&plist, 2000);
	SLTPushBack(&plist, 3000);
	SLTPushBack(&plist, 4000);
	SLTPrint(plist);
	SLTNode* pos = SLTFind(plist, 3000);
	if (pos != NULL)
	{
		//SLTInsert(&plist, pos, 20);
		SLTErase(&plist,pos);
		pos = NULL;
	}
	SLTPrint(plist);
	
}

int main()
{
	TestSList3();
	return 0;
}
