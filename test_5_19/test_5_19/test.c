#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	char c1 = 5;
//	//00000000000000000000000000000101
//	//00000101 - c1 (截断)
//	char c2 = 127;
//	//00000000000000000000000001111111
//	//01111111 - c2
//	char c3 = c1 + c2;
//	//00000000000000000000000000000101
//	//00000000000000000000000001111111
//	//00000000000000000000000010000100
//	//10000100 - c3
//	//%d - 10进制的形式打印有符号的整数
//	//11111111111111111111111110000100 - 补码
//	//11111111111111111111111110000011
//	//10000000000000000000000001111100 --> -124
//	printf("%d\n", c3);
//	
//	return 0;
//}

//int main()
//{
//	char a = 0xb6;//10110110
//	short b = 0xb600;
//	int c = 0xb6000000;
//
//	if (a == 0xb6)
//		printf("a");
//	if (b == 0xb600)
//		printf("b");
//	if (c == 0xb6000000)
//		printf("c");
//	return 0;
//}


//int main()
//{
//	char c = 1;
//	printf("%u\n", sizeof(c));
//	printf("%u\n", sizeof(+c));
//	printf("%u\n", sizeof(-c));
//	return 0;
//}

//int fun()
//{
//	static int count = 1;
//	return ++count;
//}
//
//int main()
//{
//	int answer;
//	answer = fun() - fun() * fun();
//	printf("%d\n", answer);
//	return 0;
//}


//写一个函数返回参数二进制中 1 的个数
//版本1
//int number_of_1(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		if (n % 2 == 1)
//			count++;
//		n /= 2;
//	}
//	return count;
//}

//版本2
//int number_of_1(unsigned int n)
//{
//	int count = 0;
//	while (n)
//	{
//		if (n % 2 == 1)
//			count++;
//		n /= 2;
//	}
//	return count;
//}

//版本3
//int number_of_1(int n)
//{
//	int count = 0;
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if ((n >> i && 1) == 1)
//			count++;
//	}
//	return count;
//}

//版本4
//int number_of_1(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		n = n & (n - 1);
//		count++;
//	}
//	return count;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = number_of_1(n);
//	printf("%d\n", ret);
//	return 0;
//}
//


//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
int main()
{
	int n = 0;
	scanf("%d", &n);
	int i = 0;

	for (i = 30; i >= 0; i -= 2)
	{
		printf("%d ", (n >> i) && 1);
	}
	printf("\n");
	for (i = 31; i >= 1; i -= 2)
	{
		printf("%d ", (n >> i) && 1);
	}
	printf("\n");

	return 0;
}

