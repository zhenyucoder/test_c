#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	// vs支持该c99语法
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	do
//	{
//		if (5 == i)
//		{
//			continue;
//		}
//		printf("%d ", i);
//		i++;
//	} while (i <= 10);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	int ret = 1;
//	scanf("%d", &n);
//	for (int j = 1; j <= n; j++)
//	{
//		ret = ret * j;
//	}
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = 1;
//	int sum = 0;
//	int j = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		for (; j <= i; j++)
//		{
//			ret = ret * j;
//		}
//		sum = sum + ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//int main()
//{
//	int n = 0;
//	int ret = 1;
//	int sum = 0;
//	scanf("%d", &n);
//	for (int i = 1; i <= n; i++)
//	{
//		ret = ret * i;
//		sum = sum + ret;
//	}
//	printf("%d\n", sum);
//}

////二分查找一个有序数组
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	int flag = 0;   //是否找到的标志
//	while (left<=right)
//	{
//		int mid = (left + right) / 2;
//		if (arr[mid] == k)
//		{
//			printf("找到了，下标是%d", mid);
//			flag = 1;
//			break;
//		}
//		else if (arr[mid]>k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			left = mid + 1;
//		}
//	}
//	if (flag == 0)
//		printf("找不到了\n");
//	return 0;
//}

//#include <string.h>
//#include <windows.h>
//int main()
//{
//	char arr1[] = "welcome to Chain";
//	char arr2[] = "****************";
//	int left = 0;
//	int right = strlen(arr1) - 1;
//	while (left<=right)
//	{
//		arr2[left] = arr1[left];
//		arr2[right] = arr1[right];
//		left++;
//		right--;
//		printf("%s\n", arr2);
//		Sleep(1000);
//		system("cls");
//	}
//	printf("%s\n", arr2);
//}

////假设密码为123456,有三次机会
//int main()
//{
//	char password[20] = { 0 };
//	int i = 0;
//	int flag = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("请输入密码:>");
//		scanf("%s", password);
//		if (strcmp(password, "123456") == 0)//判断两个字符串是否相等不能用==，应该用strcmp函数，返回值为0
//		{
//			printf("登录成功\n");
//			flag = 1;
//			break;
//		}
//		else
//		{
//			printf("密码错误\n");
//		}
//	}
//	/*if (3 == i)
//	{
//		printf("退出程序\n");
//	}*/
//	if (flag == 0)
//		printf("退出程序\n");
//	return 0;
//}

//#include <time.h>
//#include <stdlib.h>
//void menu()
//{
//	printf("***********************\n");
//	printf("*** 1.play  0.exit  ***\n");
//	printf("***********************\n");
//}
//void game()
//{
//	int ret = rand() % 100 + 1;//随机生成1-100
//	//2.猜数字
//	int guess = 0;
//	while (1)
//	{
//		printf("请猜数字:>\n");
//		scanf("%d", &guess);
//		if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	srand((unsigned int)time(NULL));
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:>\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误\n");
//		}
//	} while (input);
//	return 0;
//}

//#include <stdlib.h>
//#include <string.h>
//int main()
//{
//	char input[20] = { 0 };
//	system("shutdown -s -t 60");
////again:
////	printf("你的电脑将在一分钟之内关机，如果输入：快停下来,就取消取消关机\n");
////	scanf("%s", &input);
////	if (strcmp(input, "快停下来") == 0)
////	{
////		system("shutdown -a");
////	}
////	else
////	{
////		goto again;
////	}
//	while (1)
//	{
//		printf("你的电脑将在一分钟之内关机，如果输入：快停下来,就取消取消关机\n");
//		scanf("%s", &input);
//		if (strcmp(input, "快停下来") == 0)
//		{
//			system("shutdown -a");
//			break;
//		}
//	}
//	return 0;
//}

//#include <math.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 200; i <= 300; i++)
//	{
//		int j = 0;
//		int flag = 1;
//		for (j = 3; j <= sqrt(i); j+=2)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\ncount=%d\n", count);
//	return 0;
//}

//求两个数最大公约数
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//	int k = (m > n) ? n : m;
//	while (1)
//	{
//		if (m % k == 0 && n % k == 0)
//		{
//			printf("%d\n", k);
//			break;
//		}
//		k--;
//	}
//	return 0;
//}

//辗转相除法
int main()
{
	int m = 0;
	int n = 0;
	scanf("%d %d", &m, &n);
	int k = 0;
	while (k = m % n)
	{
		m = n;
		n = k;
	}
	printf("%d\n", n);
	return 0;
}