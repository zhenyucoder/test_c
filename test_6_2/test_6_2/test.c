#include <stdio.h>

//void test1()
//{
//	printf("test2\n");
//}
//void test()
//{
//	test1();
//}
//int main()
//{
//	test();
//	return 0;
//}





//int main()
//{
//
//	int i = 0;
//	int arr[10] = { 0 };
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hehe\n");
//	}
//	return 0;
//}


//#include <assert.h>
//char* My_strcpy(char* dest, const char* src)
//{
//	//断言
//	//由于我们不希望两个指针变量为空指针
//	//所以我们可以断言两个指针变量不为空指针
//	//当程序编译时，如果其中之一出现空指针，程序会报错，并且快速准确找到错误位置
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;//  '\0'拷贝
//}
//
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = "xxxxxxxxxxxx";
//
//	//封装一个strcpy功能的函数My_strcpy函数
//	printf("%s\n", My_strcpy(arr2, arr1));
//	return 0;
//}

//代码1
void test1()
{
	int n = 10;
	int m = 20;
	int* p = &m;
	*p = 20;//ok?
	p = &m;//ok?
}
//代码2
void test2()
{
	int n = 10;
	int m = 20;
	const int* p = &m;
	*p = 20;//ok?
	p = &m;//ok?
}
//代码3
void test1()
{
	int n = 10;
	int m = 20;
	int* const  p = &m;
	*p = 20;//ok?
	p = &m;//ok?
}
int main()
{
	//测试无const代码
	test1();
	//测试const放在*的左边
	test2();
	//测试const放在*的右边
	test3();
	return 0;
}