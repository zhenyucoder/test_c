//int singleNumber(int* nums, int numsSize) 
// {
//    int res = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        res = res ^ nums[i];
//    }
//    return res;
//}



//
//int cmp(const void* _a, const void* _b) 
//{
//    int a = *(int*)_a, b = *(int*)_b;
//    return a - b;
//}
//
//
//int* intersect(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) 
//{
//    qsort(nums1, nums1Size, sizeof(int), cmp);
//    qsort(nums2, nums2Size, sizeof(int), cmp);
//    int* res = malloc(sizeof(int) * nums1Size);
//
//    int i = 0;
//    int left1 = 0, left2 = 0;
//    while (left1 < nums1Size && left2 < nums2Size)
//    {
//        if (nums1[left1] == nums2[left2])
//        {
//            res[i++] = nums1[left1];
//            left1++;
//            left2++;
//        }
//        else if ((nums1[left1] < nums2[left2]))
//        {
//            left1++;
//        }
//        else
//        {
//            left2++;
//        }
//    }
//    *returnSize = i;
//    return res;
//}





//int* plusOne(int* digits, int digitsSize, int* returnSize) 
//{
//    for (int i = digitsSize - 1; i >= 0; i--) 
//    {
//        if (digits[i] != 9) 
//        {  
//            digits[i]++;
//            break;
//        }
//        digits[i] = 0;  
//    }
//
//    if (digits[0] == 0) 
//    {  
//        int* temp;   
//        int Size = digitsSize + 1; 
//        int k = Size - 1;
//        temp = (int*)malloc(Size * sizeof(int));  
//
//        while ((k) != 0) 
//        {
//            temp[k] = 0;
//            k--;
//        }
//        temp[k] = 1;  
//        *returnSize = Size;  
//        return temp;
//    }
//    *returnSize = digitsSize;
//    return digits;
//}





//
//void swap(int* a, int* b) 
//{
//    int t = *a;
//    *a = *b, * b = t;
//}
//
//void moveZeroes(int* nums, int numsSize) 
//{
//    int left = 0, right = 0;
//    while (right < numsSize) 
//    {
//        if (nums[right]) 
//        {
//            swap(nums + left, nums + right);
//            left++;
//        }
//        right++;
//    }
//}



int* twoSum(int* nums, int numsSize, int target, int* returnSize) 
{
    for (int i = 0; i < numsSize; ++i) 
    {
        for (int j = i + 1; j < numsSize; ++j) 
        {
            if (nums[i] + nums[j] == target) 
            {
                int* ret = malloc(sizeof(int) * 2);
                ret[0] = i, ret[1] = j;
                *returnSize = 2;
                return ret;
            }
        }
    }
    *returnSize = 0;
    return NULL;
}

