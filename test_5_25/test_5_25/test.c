#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>



//int f_arr(int arr[2][3], int x, int y)
//{
//	int max = arr[0][0];
//	int i = 0, j = 0;
//	for (i = 0; i < x; i++)
//	{
//		for (j = 0; j < y; j++)
//		{
//			if (arr[i][j] > max)
//				max = arr[i][j];
//		}
//	}
//	return max;
//}
//int main()
//{
//	int arr[2][3] = {0};
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 2; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//		
//	}
//	int max=f_arr(arr, 2, 3);
//	printf("max=%d\n", max);
//	return 0;
//}



//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "hello world";
//	char arr3[] = "cuihua";
//
//	//指针数组
//	char* parr[] = { arr1,arr2,arr3 };
//	for (int i = 0; i < 3; i++)
//	{
//		printf("%s\n", parr[i]);
//	}
//	return 0;
//}


//
//#define NEL 5
//float values[NEL];
//float* vp;
//int main()
//{
//	for (vp = &values[NEL]; vp > &values[0];)
//	{
//		*--vp;
//	}
//	return 0;
//}



//int main()
//{
//	int a = 10;
//	printf("a=%d\n", a); 
//	return 0;
//}
//
//#include <stdio.h>
//
//int main()
//{
//    long long a, b;
//    scanf("%lld %lld", &a, &b);
// 
//    long long m = a > b ? a : b;//假设a 和 b的较大值是最小公倍数
//    while (1)
//    {
//        if (m % a == 0 && m % b == 0)
//        {
//            break;
//        }
//        m++;
//    }
//    printf("%lld\n", m);
//
//    return 0;
//}
//
//
//
//#include <stdio.h>
//
//int main()
//{
//    long long a, b;
//    scanf("%lld %lld", &a, &b);
//    //求最小公倍数
//    int i = 1;
//    while (a * i % b)
//    {
//        i++;
//    }
//    printf("%lld\n", a * i);
//
//    return 0;
//}


//#include <stdio.h>
//
//int main() {
//    char arr[101];
//    //scanf("%s", arr);
//    //gets(arr);
//    //fgets(arr, 100, stdin);
//    printf("%s\n", arr);
//    return 0;
//}




//void reverse(char* left, char* right)
//{
//    while (left < right)
//    {
//        char tmp = *left;
//        *left = *right;
//        *right = tmp;
//        left++;
//        right--;
//    }
//}





int main() {
    char arr[101];
    //scanf("%s", arr);
    gets(arr);//读取一个字符串，即使中间有空格
    //scanf("%[^\n]s", arr);
    int len = strlen(arr);
    reverse(arr, arr + len - 1);
    //逆序每个单词
    char* start = arr;
    char* cur = arr;

    while (*cur) {
        while (*cur != ' ' && *cur != '\0')
        {
            cur++;
        }
        reverse(start, cur - 1);
        start = cur + 1;
        if (*cur == ' ')
            cur++;
    }
    printf("%s\n", arr);
    return 0;
}