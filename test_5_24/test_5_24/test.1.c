#include <stdio.h>

//int main()
//{
//	int n = 10;
//	char* pc = (char*)&n; 
//	int* pi = &n; 
//
//	printf("%p\n", &n);
//	printf("%p\n", pc);
//	printf("%p\n", pc+1);
//	printf("%p\n", pi);
//	printf("%p\n", pi+1);
//	return 0;
//}


//int main()
//{
//	int a = 0x11223344;
//	int* pa = &a;
//	*pa = 0;
//
//	return 0;
//}



//int main()
//{
//	int* p;//局部变量指针未初始化，默认为随机值
//	*p = 20;
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i <= 11; i++)
//	{
//		//当指针指向的范围超出数组arr的范围时，p就是野指针
//		*(p++) = i;
//	}
//	return 0;
//}


//int* test()
//{
//	int a = 110;
//	return &a;
//}
//int mian()
//{
//	int* p = test();
//	printf("%d\n", *p);
//	return 0;
//}

//#define N_VALUE 5
//float values[N_VALUE];
//float* vp;
//int main()
//{
//	for (vp = &values[0]; vp < &values[N_VALUE];)
//	{
//		*vp++ = 1;//vp++,vp地址向后移动4字节
//	}
//	//输出
//	for (int i = 0; i < N_VALUE; i++)
//	{
//		printf("%d ", values[i]);
//	}
//	return 0;
//}



int main()
{
	int arr[10] = { 0 };
	printf("%d\n", &arr[9] - &arr[0]);
	return 0;
}