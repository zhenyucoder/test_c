#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main()
//{
//	short end = 200;
//	long big = 65537;
//	long long verybig = 12345678908642;
//
//	printf("end=%u and not %d\n", end, end);
//	printf("big=%ld and not %hd\n", big,big );
//	printf("verybig=%lld and not %ld\n", verybig,verybig );
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char ch;
//	scanf("%c", &ch);
//	printf("%d %c", ch, ch);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	float ibs = 32000.0;
//	double ibc = 2.14e9;
//	long double ibf = 5.32e-5;
//	printf("%f can be written %e\n", ibs, ibs);
//	printf("And it's %a in hexadecimal,powers of 2 notation\n", ibs);
//	printf("%f can be weitten %e\n", ibc, ibc);
//	printf("%Lf can be written %Le\n", ibf, ibf);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	float x = 0.1234E-10;
//	printf("%.4e", x/10);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int c = 1;//用于控制最后的尾巴（数柄）；
//    int n = 0;//层数
//    scanf("%d", &n);
//    int d = 3 * n;//a和d用于控制最开始的空白数；
//    for (int i = 1; i <= n; i++)
//    {
//        for (int a = d - 1; a > 0; a--)//
//        {
//            printf(" ");
//        }
//        for (int k = 1; k <= i; k++)//k代表一行打印多少个“*     ”
//        {
//            printf("*     ");//把第一层当作“*     ”来打印
//        }
//        printf("\n");
//        for (int a = d - 2; a > 0; a--)//同上
//        {
//            printf(" ");
//        }
//        for (int k = 1; k <= i; k++)
//        {
//            printf("* *   ");
//        }
//        printf("\n");
//        for (int a = d - 3; a > 0; a--)//同上
//        {
//            printf(" ");
//        }
//        for (int k = 1; k <= i; k++)
//        {
//            printf("* * * ");
//        }
//        printf("\n");
//        d = d - 3;
//    }
//    while (c <= n)
//    {
//        for (int i = 1; i <= (6 * n - 1) / 2; i++)
//        {
//            printf(" ");
//        }
//        printf("*\n");
//        c++;
//    }
//    return 0;
//}

#include <stdio.h>
//法一：无法通过，会出现算法复杂度过大的情况！--暴力求解不太行
/*
int main()
{
    long long n,m;
    scanf("%lld %lld",&n,&m);
    //求最大公约数
    long long max = n>m?m:n;//假设n和m的较小值为最大公约数
    while( 1 )
    {
        if( n%max==0 && m%max==0)
        {
            break;
        }
        max--;
    }//此时max里面记录的就是最大公约数
    //最小公倍数
    long long min = n>m?n:m; //假设n和m的较大值为最小公倍数
    while(1)
    {
        if( min%n==0 && min%m==0 )
        {
            break;
        }
        min++;
    }
    printf("%lld",min+max);
    return 0;
}
*/

//重新优化后的算法
int main()
{
    long long n, m;
    scanf("%lld %lld", &n, &m);
    //求最大公约数
    long long max = 0;
    long long min = 0;
    long long tmp = 0;
    //先将n和m进行保存，防止下面使用辗转相除的方法影响n和m的值
    long long a = n;
    long long b = m;

    //辗转相除法
    while (tmp = n % m)
    {
        n = m;
        m = tmp;
    }
    max = m;
    //最小公倍数=n*m/max
    min = a * b / max;
    printf("%lld", min + max);
    return 0;
}