#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//int rand = 1;
//int main()
//{
//	printf("%p\n", rand);
//	return 0;
//}
//
//namespace N1
//{
//	int rand = 10;
//
//	int Add(int left, int right)
//	{
//		return left + right;
//	}
//
//	struct Node
//	{
//		int* a;
//		int data;
//	};
//
//	namespace N2
//	{
//		int rand = 100;
//	}
//}

//全部展开，全部授权
//using namespace N1;
//部分展开
//using N1::Add;

//int main()
//{
//	//printf("%d\n", rand);
//	printf("%d\n", N1::rand);
//	printf("%d\n", N1::Add(1, 2));
//	printf("%d\n", Add(12, 4));
//	printf("%d\n", N1::N2::rand);
//
//	struct N1::Node node;
//	//struct Node node;
//
//	return 0;
//}



#include <iostream>

//using namespace std;
//
//#include "Queue.h"
//#include "Stack.h"
//
//int main()
//{
//	//std::cout << "hello world" << std::endl;
//
//	//c:控制台
//	cout << "count =" << 100 << endl;
//	cout << "count=" << 100 << "\n";
//	//cout << "count=" << 100 << 20;
//	N1::QueueInin();
//	N1::StackInit();
//
//	int i;
//    cin>>i;
//	return 0;
//}



//int main()
//{
//	int i;
//	double j;
//	std::cin >>i >> j;
//
//	std::cout << i << std::endl;
//	std::cout << j << std::endl;
//
//	std::cout << &i << std::endl;
//	std::cout << &j << std::endl;
//
//	//设计具体精度时，回归C
//	printf("%.1f\n", j);
//	return 0;
//}

using std::cout;
using std::endl;

////缺省参数
//void Func(int a = 10)
//{
//	cout << a << endl;
//}
//
////全缺省参数
//void Func1(int a = 10, int b = 20, int c = 30)
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl << endl;
//}
//
////半缺省参数
//void Func2(int a, int b = 10, int c = 20)
//{
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl << endl;
//}
//
//int main()
//{
//	/*Func1();
//	Func1(100);
//	Func1(100,200);
//	Func1(100,200,300);*/
//
//	Func2(1,2);
//	return 0;
//}


//函数类型不同
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
//
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}

//参数个数不同
//void Func()
//{
//	cout << "Func()" << endl;
//}
//
//void Func(int a)
//{
// cout << "Func(int a)" << endl;
//}

//返回值不同，不构成重载
//void Func()
//{
//	cout << "Func()" << endl;
//}
//
//int Func(int a)
//{
//	cout << "Func(int a)" << endl;
//	return a;
//}
//
////参数类型顺序不同
//void f(int a, char b)
//{
//	cout << "f(int a,char b)" << endl;
//}
//
//void f(char b, int a)
//{
//	cout << "f(char b, int a)" << endl;
//}
//
//int main()
//{
//	/*cout << Add(1, 2) << endl;
//	cout << Add(1.12, 12.1) << endl;*/
//
//	/*Func(1);
//	Func();*/
//
//	f(1, '1');
//	f('a', 1);
//	return 0;
//}


//构成重载
//void func(int a)
//{
//	cout << "void func(int a)" << endl;
//}
//
//void func(int a, int b)
//{
//	cout << "void func(int a)" << endl;
//}
//
//int main()
//{
//	//调用歧义
//	//fun(1);
//	func(1, 2);
//	return 0;
//}




