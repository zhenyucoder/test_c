//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        //判断查找结果是边界数据还是真正结果
//        return nums[left] >= target ? left : left + 1;
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        if (nums.size() == 0)
//            return { -1, -1 };
//        int begin = 0;//记录查找元素的第一个
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        //判断是否存在结果
//        if (nums[left] != target)
//            return { -1,-1 };
//        else
//            begin = left;
//
//        //一定存在结果，查找最后一个元素
//        left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > target)
//                right = mid - 1;
//            else
//                left = mid;
//        }
//        return { begin, left };
//    }
//};


//class Solution{
//public:
//    int mySqrt(int x) {
//       int left = 0, right = x;
//       while (left < right)
//       {
//            long long mid = left + (right - left) / 2;
//            if (mid * mid < x)
//                left = mid + 1;
//            else
//                right = mid;
//       }
//       //判断结果是否为整数，调整结果
//       return (long long)left * left == x ? left : left - 1;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int left = 0, right = arr.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid] > arr[mid - 1])
//                left = mid;
//            else
//                right = mid - 1;
//        }
//        return left;
//    }
//};



//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > nums[mid - 1])
//                left = mid;
//            else
//                right = mid - 1;
//        }
//        return left;
//    }
//};

//class Solution
//{
//public:
//	int findMin(vector<int>& nums)
//	{
//		int left = 0, right = nums.size() - 1;
//		int x = nums[right];
//		while (left < right)
//		{
//			int mid = left + (right - left) / 2;
//			if (nums[mid] > x) left = mid + 1;
//			else right = mid;
//		}
//		return nums[left];
//	}
//};

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0, n = nums.size();
//        for (int i = 0; i <= n; i++)
//        {
//            ret ^= i;
//        }
//
//        for (auto nu : nums)
//        {
//            ret ^= nu;
//        }
//        return ret;
//    }
//};
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] == mid)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return nums[left] == left ? left + 1 : left;
//    }
//};

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

//vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//    vector<int> ret;
//    int n1 = nums1.size(), n2 = nums2.size();
//    //将两数组排序，方便后续处理相同元素
//    sort(nums1.begin(), nums1.end()), sort(nums2.begin(), nums2.end());
//    for (int i = 0; i < n1;i++)
//    {
//        int target = nums1[i];
//        //nums2中二分查找target
//        int left = 0, right = n2 - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums2[mid] > target)
//                right = mid - 1;
//            else if (nums2[mid] < target)
//                left = mid + 1;
//            else
//            {
//                ret.push_back(target);
//                break;
//            }
//        }
//        //跳过重叠元素
//        while (i + 1 < n1 && nums1[i + 1] == target) i++;
//     }
//    return ret;
//}
//
//
//int main()
//{
//    vector<int> v1;
//    v1.push_back(1);
//    v1.push_back(2);
//    v1.push_back(2);
//    v1.push_back(1);
//    vector<int> v2;
//    v2.push_back(2);
//    v2.push_back(2);
//
//    intersection(v1, v2);
//    return 0;
//}
//
//class Solution {
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        vector<int> ret;
//        int n1 = nums1.size(), n2 = nums2.size();
//        sort(nums1.begin(), nums1.end()), sort(nums2.begin(), nums2.end());
//        int index1 = 0, index2 = 0;
//        while (index1 < n1 && index2 < n2)
//        {
//            int x1 = nums1[index1], x2 = nums2[index2];
//            if (x1 > x2)
//                index2++;
//            else if (x1 < x2)
//                index1++;
//            else
//            {
//                //保证元素唯一性
//                if (!ret.size() || ret.back() != x1)
//                    ret.push_back(x1);
//                index1++, index2++;
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
//        vector<int> ret;
//        sort(nums1.begin(), nums1.end()), sort(nums2.begin(), nums2.end());
//        int len1 = nums1.size(), len2 = nums2.size();
//        int index1 = 0, index2 = 0;
//        while (index1 < len1 && index2 < len2)
//        {
//            int num1 = nums1[index1], num2 = nums2[index2];
//            if (num1 > num2)
//                index2++;
//            else if (num1 < num2)
//                index1++;
//            else
//            {
//                ret.push_back(num1);
//                index1++, index2++;
//            }
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    bool isPerfectSquare(int num) {
//        int left = 0, right = num;
//        while (left < right)
//        {
//            long long mid = left + (right - left) / 2;
//            if (mid * mid < num)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return (long long)left * left == num;
//    }
//};

//class Solution {
//public:
//    bool isPerfectSquare(int num) {
//        double x0 = num;
//        while (1)
//        {
//            double x1 = (x0 + num / x0) / 2;
//            if (x0 - x1 < 1e-6)
//                break;
//            x0 = x1;
//        }
//        int x = (int)x0;
//        return x * x == num;
//    }
//};

//class Solution {
//public:
//    int guessNumber(int n) {
//        int left = 0, right = n;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (guess(mid) == -1)
//                right = mid - 1;
//            else if (guess(mid) == 1)
//                left = mid + 1;
//            else
//                return mid;
//        }
//        return -1;
//    }
//
//};


//class Solution {
//public:
//    int arrangeCoins(int n) {
//        int left = 1, right = n;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            long long total = (long long)(mid + 1) * mid / 2;
//            if (total > n)
//                right = mid - 1;
//            else
//                left = mid;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    char nextGreatestLetter(vector<char>& letters, char target) {
//        int left = 0, right = letters.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (letters[mid] <= target)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return letters[left] > target ? letters[left] : letters[0];
//    }
//};



