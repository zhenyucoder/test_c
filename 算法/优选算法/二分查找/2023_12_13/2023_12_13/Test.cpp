//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int left = 0, right = nums.size() - 1;
//        int x = nums[0];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] >= x)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        //特殊处理，当数据变换后单调递增时，上述所得结果无效
//        return nums[left] > nums[0] ? nums[0] : nums[left];
//    }
//};

//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int left = 0, right = records.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (records[mid] == mid)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return records[left] == left ? left + 1 : left;
//    }
//};


