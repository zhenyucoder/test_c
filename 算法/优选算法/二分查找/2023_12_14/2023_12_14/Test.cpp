//class Solution {
//public:
//    int findString(vector<string>& words, string s) {
//        int left = 0, right = words.size() - 1;
//        while (left <= right)
//        {
//            while (words[left] == "") left++;//删除左边空字符元素
//            while (words[right] == "") right--;//删除右边空字符元素
//            //由于如果mid的元素为空字符串，可以向右遍历查找第一个非空字符
//            // 但有可能查找到的数据比s大，这时我们需要将右边界right回到mid最开始位置并-1。
//            //但在此过程中我们一系列行为不断修改mid，为此这里定义一个temp变量来保持mid开始值
//            int mid = left + (right - left) / 2, temp = mid;
//            while (mid < right && words[mid] == "") mid++;
//            if (words[mid] > s)
//                right = temp - 1;
//            else if (words[mid] == s)
//                return mid;
//            else
//                left = mid + 1;
//        }
//        return -1;
//    }
//};



//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int left = 0, right = records.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (records[mid] <= mid)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return records[left] == left ? left + 1 : left;
//    }
//};


//class Solution{
//public:
//    int countTarget(vector<int>&scores, int target) {
//        if (scores.size() == 0)
//            return 0;
//        int begin = -1;//查找目标成绩出现的首个元素
//        int left = 0, right = scores.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (scores[mid] < target)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        //判断是否存在目标成绩
//        if (scores[left] == target)
//            begin = left;
//        else
//            return 0;
//
//        //查找目标数据最右边界
//        left = 0, right = scores.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (scores[mid] > target)
//                right = mid - 1;
//            else
//                left = mid;
//        }
//        return left - begin + 1;
//    }
//};


//class Solution {
//public:
//    int stockManagement(vector<int>& stock) {
//        int left = 0, right = stock.size() - 1;
//        while (left < right)
//        {
//            int x1 = stock[right];
//            int mid = left + (right - left) / 2;
//            if (stock[mid] > x1)
//                left = mid + 1;
//            else if (stock[mid] < x1)
//                right = mid;
//            else
//                right--;
//        }
//        return stock[left];
//    }
//};


//class Solution {
//public:
//    int mySqrt(int x) {
//        long long left = 0, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            if (mid * mid <= x)
//                left = mid;
//            else
//                right = mid - 1;
//        }
//        return left;
//    }
//};



//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int left = 0, right = arr.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid] > arr[mid - 1])
//                left = mid;
//            else
//                right = mid - 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target)
//                left = mid + 1;
//            else
//                right = mid;
//        }
//        return nums[left] < target ? left + 1 : left;
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& numbers, int target) {
//        int left = 0, right = numbers.size() - 1;
//        while (left < right)
//        {
//            int sum = numbers[left] + numbers[right];
//            if (sum > target)
//                right--;
//            else if (sum < target)
//                left++;
//            else
//                return { left, right };
//        }
//        return { -1, -1 };
//    }
//};

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

//int purchasePlans(vector<int>& nums, int target) {//2 3 5 5
//    int count = 0, n = nums.size();
//    sort(nums.begin(), nums.end());
//    for (int i = 0; i < n - 1; i++)
//    {
//        int _target = target - nums[i];
//        int left = i + 1, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > _target)
//                right = mid - 1;
//            else
//                left = mid;
//        }
//        //更新结果 
//        if (nums[left] <= _target)
//            count += (left - i);
//    }
//    return count;
//}
//
//int main()
//{
//    vector<int> v1;
//    v1.push_back(2);
//    v1.push_back(2);
//    v1.push_back(1);
//    v1.push_back(9);
//    cout << purchasePlans(v1, 10) << endl;
//
//    return 0;
//}



//class Solution {
//public:
//    int purchasePlans(vector<int>& nums, int target) {
//        long long count = 0, n = nums.size();
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < n - 1; i++)
//        {
//            int _target = target - nums[i];
//            int left = i + 1, right = n - 1;
//            if (nums[left] > _target) break;
//            while (left < right)
//            {
//                int mid = left + (right - left + 1) / 2;
//                if (nums[mid] > _target)
//                    right = mid - 1;
//                else
//                    left = mid;
//            }
//            //更新结果
//            count += (left - i);
//        }
//        return count % (1000000007);
//    }
//};



//class Solution {
//public:
//    int breakfastNumber(vector<int>& staple, vector<int>& drinks, int x) {
//        sort(staple.begin(), staple.end()), sort(drinks.begin(), drinks.end());
//        long long count = 0;
//        int len1 = staple.size(), len2 = drinks.size();
//        for (int i = 0; i < len1; i++)
//        {
//            if (staple[i] >= x) break;
//            int target = x - staple[i];
//            int left = 0, right = len2 - 1;
//            if (drinks[left] > target) break;
//            while (left < right)
//            {
//                int mid = left + (right - left + 1) / 2;
//                if (drinks[mid] > target)
//                    right = mid - 1;
//                else
//                    left = mid;
//            }
//            count += left + 1;
//        }
//        return count % (1000000007);
//    }
//};



//class Solution {
//public:
//    int getCommon(vector<int>& nums1, vector<int>& nums2) {
//        int len1 = nums1.size(), len2 = nums2.size();
//        int index1 = 0, index2 = 0;
//        while (index1 < len1 && index2 < len2)
//        {
//            if (nums1[index1] > nums2[index2])
//                index2++;
//            else if (nums1[index1] < nums2[index2])
//                index1++;
//            else
//                return nums1[index1];
//        }
//        return -1;
//    }
//};


