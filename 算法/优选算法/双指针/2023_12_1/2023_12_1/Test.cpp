//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        for (int cur = 0, dest = -1; cur < nums.size(); cur++)
//        {
//            if (nums[cur] != 0)
//                swap(nums[++dest], nums[cur]);
//        }
//    }
//};

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

//
//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        int dest = -1, cur = 0, n = arr.size();
//        while (cur < n)
//        {
//            if (arr[cur])
//                dest++;
//            else
//                dest += 2;
//            if (dest >= n - 1)
//                break;
//            cur++;
//        }
//
//        //边界处理
//        if (dest == n)
//        {
//            arr[--dest] = 0;
//            dest--;
//            cur--;
//        }
//
//        for (; cur >= 0; cur--)
//        {
//            if (arr[cur])
//            {
//                arr[dest--] = arr[cur];
//            }
//            else
//            {
//                arr[dest--] = 0;
//                arr[dest--] = 0;
//            }
//        }
//    }
//};


//class Solution {
//public:
//    int bitSum(int x)
//    {
//        int ret = 0;
//        while (x)
//        {
//            int r = x % 10;
//            ret += r * r;
//            x /= 10;
//        }
//        return ret;
//    }
//
//    bool isHappy(int n)
//    {
//        //寻找相遇点
//        int slow = n, fast = bitSum(n);
//        while (slow != fast)
//        {
//            slow = bitSum(slow);
//            fast = bitSum(bitSum(fast));
//        }
//        //判断
//        if (slow == 1)
//            return true;
//        else
//            return false;
//    }
//};


//class Solution {
//public:
//    int maxArea(vector<int>& height) {
//        int ret = 0;
//        int left = 0, right = height.size() - 1;
//        while (left < right)
//        {
//            int capacity = min(height[left], height[right]) * (right - left);
//            ret = max(ret, capacity);
//            if (height[left] < height[right])
//                left++;
//            else
//                right--;
//        }
//        return ret;
//    }
//};



//class Solution {
//public:
//    int triangleNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int count = 0;
//        for (int i = nums.size() - 1; i >= 2; i--)
//        {
//            int left = 0, right = i - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] > nums[i])
//                {
//                    //计算有效个数
//                    count += right - left;
//                    right--;
//                }
//                else
//                {
//                    left++;
//                }
//            }
//
//        }
//        return count;
//    }
//};



//class Solution {
//public:
//    vector<int> twoSum(vector<int>& price, int target) {
//        int left = 0, right = price.size() - 1;
//        while (left < right)
//        {
//            if (price[left] + price[right] > target)
//                right--;
//            else if (price[left] + price[right] < target)
//                left++;
//            else
//                return { price[left], price[right] };
//        }
//        return { -1, -1 };
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        vector<vector<int>>  ret;
//        sort(nums.begin(), nums.end());
//        for (int i = nums.size() - 1; i >= 2; i--)
//        {
//			  if(nums[i]<0)
//					break;
//            int left = 0, right = i - 1;
//            while (left < right)
//            {
//                if (nums[left] + nums[right] + nums[i] > 0)
//                    right--;
//                else if (nums[left] + nums[right] + nums[i] < 0)
//                    left++;
//                else
//                {
//                    //相等,记录数据；处理相同元素
//                    ret.push_back({ nums[left], nums[right], nums[i] });
//                    left++, right--;
//                    //处理相同元素
//                    while (left < right && nums[left - 1] == nums[left])
//                    {
//                        left++;
//                    }
//                    while (left < right && nums[right + 1] == nums[right])
//                    {
//                        right--;
//                    }
//                }
//                //处理相同元素，防止重叠
//                while (i >= 2 && nums[i] == nums[i - 1])
//                {
//                    i--;
//                }
//            }
//        }
//        return ret;
//    }
//};




//class Solution {
//public:
//    vector<vector<int>> fourSum(vector<int>& nums, int target) {
//        vector<vector<int>> ret;
//        sort(nums.begin(), nums.end());
//        for (int i = nums.size() - 1; i >= 3;)
//        {
//            for (int j = i - 1; j >= 2;)
//            {
//                long long _targrt = (long long)target - nums[i] - nums[j];
//                int left = 0, right = j - 1;
//                while (left < right)
//                {
//                    int sum = nums[left] + nums[right];
//                    if (sum > _targrt)
//                        right--;
//                    else if (sum < _targrt)
//                        left++;
//                    else
//                    {
//                        ret.push_back({ nums[left], nums[right], nums[j], nums[i] });
//                        left++, right--;
//                        //处理重复元素，防止答案重叠
//                        while (left < right && nums[left - 1] == nums[left])
//                            left++;
//                        while (left < right && nums[right + 1] == nums[right])
//                            right--;
//                    }
//                }
//                //处理重复元素，防止答案重叠
//                j--;
//                while (j >= 2 && nums[j + 1] == nums[j])
//                    j--;
//            }
//            //处理重复元素，防止答案重叠
//            i--;
//            while (i >= 3 && nums[i + 1] == nums[i])
//                i--;
//        }
//        return ret;
//    }
//};


