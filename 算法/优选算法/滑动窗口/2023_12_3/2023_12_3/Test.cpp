//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int len = INT_MAX, n = nums.size(), sum = 0;
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            sum += nums[right];//进窗口
//            while (sum >= target)
//            {
//                //更新结果
//                len = min(len, right - left + 1);
//                //出窗口
//                sum -= nums[left];
//                left++;
//            }
//        }
//
//        return len == INT_MAX ? 0 : len;
//    }
//};



//class Solution {
//public:
//    int lengthOfLongestSubstring(string s) {
//        int len = 0, n = s.size(), hash[128] = { 0 };
//        for (int left = 0, right = 0; right < n; right++)
//        {
//            //进窗口
//            hash[s[right]]++;
//            //判断
//            while (hash[s[right]] > 1)
//            {
//                hash[s[left++]]--;
//            }
//            //更新结果
//            len = max(len, right - left + 1);
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int ret = 0, n = nums.size();
//        for (int left = 0, right = 0, zero = 0; right < n; right++)
//        {
//            if (nums[right] == 0)
//                zero++;
//            //判断
//            if (zero > k)
//            {
//                while (zero > k)
//                {
//                    if (nums[left++] == 0)
//                        zero--;
//
//                }
//            }
//            //更新结果
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//        }
//        int target = sum - x;
//        if (target < 0)
//            return -1;
//
//        int ret = -1, n = nums.size();
//        for (int left = 0, right = 0, tmp = 0; right < n; right++)
//        {
//            tmp += nums[right];
//            while (tmp > target)
//            {
//                tmp -= nums[left++];
//            }
//            //更新结果
//            if (tmp == target)
//                ret = max(ret, right - left + 1);
//        }
//        return ret == -1 ? -1 : n - ret;
//    }
//};



//class Solution {
//public:
//    int totalFruit(vector<int>& fruits) {
//        int hash[100001] = { 0 };
//        int ret = 0, n = fruits.size();
//        for (int left = 0, right = 0, kinds = 0; right < n; right++)
//        {
//            if (hash[fruits[right]] == 0)
//                kinds++;
//            hash[fruits[right]]++;//进窗口
//            while (kinds > 2)//判断
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)
//                    kinds--;
//                left++;
//            }
//            //更新结果
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};


//class Solution
//{
//public:
//	string minWindow(string s, string t)
//	{
//		int hash1[128] = { 0 }; 
//		int kinds = 0; 
//		for (auto ch : t)
//			if (hash1[ch]++ == 0) kinds++;
//		int hash2[128] = { 0 }; // 统计窗?内每个字符的频次
//		int minlen = INT_MAX, begin = -1;
//		for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//		{
//			char in = s[right];
//			if (++hash2[in] == hash1[in]) count++; // 进窗? + 维护 count
//			while (count == kinds) 
//			{
//				if (right - left + 1 < minlen) // 更新结果
//				{
//					minlen = right - left + 1;
//					begin = left;
//				}
//				char out = s[left++];
//				if (hash2[out]-- == hash1[out]) count--; // 出窗? + 维护 count
//			}
//		}
//		if (begin == -1) return "";
//		else return s.substr(begin, minlen);
//	}
//};



//class Solution
//{
//public:
//	vector<int> findSubstring(string s, vector<string>& words)
//	{
//		vector<int> ret;
//		unordered_map<string, int> hash1; // 保存 words ??所有单词的频次
//		for (auto& s : words) hash1[s]++;
//		int len = words[0].size(), m = words.size();
//		for (int i = 0; i < len; i++) // 执? len 次
//		{
//			unordered_map<string, int> hash2; // 维护窗?内单词的频次
//			for (int left = i, right = i, count = 0; right + len <= s.size();
//				right += len)
//			{
//				// 进窗? + 维护 count
//				string in = s.substr(right, len);
//				hash2[in]++;
//				if (hash1.count(in) && hash2[in] <= hash1[in]) count++;
//				// 判断
//				if (right - left + 1 > len * m)
//				{
//					// 出窗? + 维护 count
//					string out = s.substr(left, len);
//					if (hash1.count(out) && hash2[out] <= hash1[out]) count--;
//					hash2[out]--;
//					left += len;
//				}
//				// 更新结果
//				if (count == m) ret.push_back(left);
//			}
//		}
//		return ret;
//	}
//};



class Solution
{
public:
	vector<int> findAnagrams(string s, string p)
	{
		vector<int> ret;
		int hash1[26] = { 0 }; // 统计字符串 p 中每个字符出现的个数
		for (auto ch : p) hash1[ch - 'a']++;
		int hash2[26] = { 0 }; // 统计窗???的每?个字符出现的个数
		int m = p.size();
		for (int left = 0, right = 0, count = 0; right < s.size(); right++)
		{
			char in = s[right];
			// 进窗? + 维护 count
			if (++hash2[in - 'a'] <= hash1[in - 'a']) count++;
			if (right - left + 1 > m) // 判断
			{
				char out = s[left++];
				// 出窗? + 维护 count
				if (hash2[out - 'a']-- <= hash1[out - 'a']) count--;
			}
			// 更新结果
			if (count == m) ret.push_back(left);
		}
		return ret;
	}
};