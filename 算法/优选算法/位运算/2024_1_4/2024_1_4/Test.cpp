//class Solution
//{
//	string hash[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs",
//   "tuv", "wxyz" };
//	string path;
//	vector<string> ret;
//public:
//	vector<string> letterCombinations(string digits)
//	{
//		if (digits.size() == 0) return ret;
//		dfs(digits, 0);
//		return ret;
//	}
//	void dfs(string& digits, int pos)
//	{
//		if (pos == digits.size())
//		{
//			ret.push_back(path);
//			return;
//		}
//		for (auto ch : hash[digits[pos] - '0'])
//		{
//			path.push_back(ch);
//			dfs(digits, pos + 1);
//			path.pop_back(); // 恢复现场
//		}
//	}
//};


//class Solution
//{
//	public int uniquePaths(int m, int n)
//	{
//		// 动态规划
//		int[][] dp = new int[m + 1][n + 1];
//		dp[1][1] = 1;
//		for (int i = 1; i <= m; i++)
//			for (int j = 1; j <= n; j++)
//			{
//				if (i == 1 && j == 1) continue;
//				dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//			}
//		return dp[m][n];
//	}
//	public int dfs(int i, int j, int[][] memo)
//	{
//		if (memo[i][j] != 0)
//		{
//			return memo[i][j];
//		}
//		if (i == 0 || j == 0) return 0;
//		if (i == 1 && j == 1)
//		{
//			memo[i][j] = 1;
//			return 1;
//		}
//		memo[i][j] = dfs(i - 1, j, memo) + dfs(i, j - 1, memo);
//		return memo[i][j];
//	}
//}



//class Solution
//{
//	int memo[201][201];
//public:
//	int getMoneyAmount(int n)
//	{
//		return dfs(1, n);
//	}
//	int dfs(int left, int right)
//	{
//		if (left >= right) return 0;
//		if (memo[left][right] != 0) return memo[left][right];
//		int ret = INT_MAX;
//		for (int head = left; head <= right; head++) // 选择头结点
//		{
//			int x = dfs(left, head - 1);
//			int y = dfs(head + 1, right);
//			ret = min(ret, head + max(x, y));
//		}
//		memo[left][right] = ret;
//		return ret;
//	}
//};

class Solution
{
	int path;
	int sum;
public:
	int subsetXORSum(vector<int>& nums)
	{
		dfs(nums, 0);
		return sum;
	}
	void dfs(vector<int>& nums, int pos)
	{
		sum += path;
		for (int i = pos; i < nums.size(); i++)
		{
			path ^= nums[i];
			dfs(nums, i + 1);
			path ^= nums[i]; 
		}
	}
};