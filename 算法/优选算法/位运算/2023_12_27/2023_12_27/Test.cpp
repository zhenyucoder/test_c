//class Solution {
//public:
//    int hammingWeight(uint32_t n) {
//        int count = 0;
//        while (n)
//        {
//            n &= (n - 1);
//            count++;
//        }
//        return count;
//    }
//};

//class Solution {
//public:
//    vector<int> countBits(int n) {
//        vector<int> ret;
//        for (int i = 0; i <= n; i++)
//        {
//            int count = 0, x = i;
//            while (x)
//            {
//                x &= (x - 1);
//                count++;
//            }
//            ret.push_back(count);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int target = x ^ y;
//        //计算target中1的个数
//        int count = 0;
//        while (target)
//        {
//            target &= (target - 1);
//            count++;
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto x : nums)
//        {
//            ret ^= x;
//        }
//        return ret;
//    }
//};


//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int sum = 0;
//        for (auto num : nums)
//        {
//            sum ^= num;
//        }
//
//        // int lsb = sum &(-sum);
//        int lsb = (sum == INT_MIN ? sum : sum & (-sum));
//        int x1 = 0, x2 = 0;
//        for (auto num : nums)
//        {
//            if (num & lsb)
//                x1 ^= num;
//            else
//                x2 ^= num;
//        }
//        return { x1, x2 };
//    }
//};

//class Solution {
//public:
//    bool isUnique(string astr) {
//        //利用割巢原来优化
//        if (astr.size() > 26)
//            return false;
//        int bitMap = 0;
//        for (auto ch : astr)
//        {
//            int i = ch - 'a';
//            //判断字符是否出现过
//            if (((bitMap >> i) & 1) == 1)
//                return false;
//            bitMap |= (1 << i);
//        }
//        return true;
//    }
//};

//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int ret = 0;
//        for (auto num : nums)
//        {
//            ret ^= num;
//        }
//
//        for (int i = 0; i <= nums.size(); i++)
//        {
//            ret ^= i;
//        }
//        return ret;
//    }
//};
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int sum = nums.size() * (nums.size() + 1) / 2;
//        for (auto num : nums)
//        {
//            sum -= num;
//        }
//        return sum;
//    }
//};
//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int n = nums.size();
//        int hash[10001] = { 0 };
//        for (auto num : nums)
//        {
//            hash[num] = 1;
//        }
//
//        for (int i = 0; i <= n; i++)
//        {
//            if (hash[i] == 0)
//                return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int getSum(int a, int b) {
//        while (b)
//        {
//            int x = a ^ b;
//            b = (a & b) << 1;
//            a = x;
//        }
//        return a;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int ret = 0;
//        for (int i = 0; i < 32; i++)//依次修改ret中的每一位
//        {
//            int sum = 0;
//            for (auto num : nums)//记录所有数字第i位所有和
//            {
//                if ((num >> i) & 1 == 1)
//                    sum++;
//            }
//            if (sum % 3 == 1)
//                ret |= (1 << i);
//        }
//        return ret;
//    }
//};


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> missingTwo(vector<int>& nums) {
        int ret = 0;
        int n = nums.size();
        for (int i = 1; i <= (n + 2); i++)
        {
            ret ^= i;
        }
        for (auto num : nums)
        {
            ret ^= num;
        }
        //目前所得ret为消失的两个数字为异或
        int type1 = 0, type2 = 0;//消失的两个数字
        int lowbit = ret & (-ret);//ret二进制第一个1出现位置
        for (int i = 1; i <= (n + 2); i++)
        {
            if ((i & lowbit) == 0)
                type1 ^= i;
            else
                type2 ^= i;
        }
        for (auto num : nums)
        {
            if ((num & lowbit) == 0)
                type1 ^= num;
            else
                type2 ^= num;
        }
        return { type1, type2 };
    }
};

int main()
{
    vector<int> v;
    v.push_back(2);
    v.push_back(3);
    missingTwo(v);
    return 0;
}