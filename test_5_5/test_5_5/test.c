#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int binary_search(int arr[], int k, int sz)
//{
//	int left = 0;
//	int right=sz - 1;
//	
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;
//
//		if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else
//		{
//			return mid;
//		}
//
//	}
//
//	return -1;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int ret = binary_search(arr, k,sz);
//
//	if (ret == -1)
//	{
//		printf("找不到了\n");
//	}
//	else
//	{
//		printf("找到了，下标是:%d", ret);
//	}
//
//	return 0;
//}

//void test(int* num)
//{
//	(*num)++;
//}
//
//int main()
//{
//	int num = 0;
//	test(&num);
//	printf("%d", num);
//	return 0;
//}

//int main()
//{
//	printf("hehe\n"); //死递归，栈溢出
//	main();
//	return 0;
//}

//void print(unsigned int num)
//{
//	if (num > 9)
//	{
//		print(num / 10);
//	}	
//		printf("%d ",num%10);
//}
//int main()
//{
//	unsigned int num = 0;
//	scanf("%d", &num);//输入1234，打印1 2 3 4（递归）
//	print(num);
//	return 0;
//}

//int Fac(int n)
//{
//	if (n <= 1)	//递归
//		return 1;
//	else
//		return (n * Fac(n - 1));
//}
//int Fac(int n)
//{
//	int i = 0;
//	int r = 1;
//
//	for (int i = 1; i <= n; i++)
//	{
//		r = r * i;
//	}
//
//	return r;
//}
//int main()		//打印n!
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fac(n);
//	printf("%d", ret);
//	return 0;
//}

//求第n个斐波那契数
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fib(n - 1) + Fib(n - 2);
//}

//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//	return 0;
//}

////递归
//int Fib(int n)
//{
//	if (n <= 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return Fib(n - 1) + Fib(n - 2);
//	}
//}

////非递归
//int Fib(n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//
//	while (n >= 3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//
//	return c;
//}
// 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//}

//int Fx(int n, int k)
//{
//	if (k <= 1)
//		return n;
//	else
//		return n * Fx(n, k - 1);
//}
//
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int ret = Fx(n, k);
//	printf("%d\n", ret);
//	return 0;
//}

//int DigitSum(int n)
//{
//	if (n < 9)
//		return n;
//	else
//		return n % 10 + DigitSum(n / 10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret=DigitSum(n);
//	printf("%d\n", ret);
//	return 0;
//}

//void ret(int n)
//{
//	if (n > 9)
//	{
//		ret(n / 10);
//	}
//		printf("%d ", n%10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	ret(n);
//	return 0;
//}

//int Fax(int n)
//{
//	if (n <= 1)
//		return 1;
//	else
//		return n * Fax(n - 1);
//}
//int Fax(int n)
//{
//	int c = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		c = c * i;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fax(n);
//	printf("%d", ret);
//	return 0;
//}
