#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int i;//全局变量，未初始化默认为0
//int main()
//{
//    //sizeof计算为无符号整数，i发生算术提升
//    i--;
//    if (i > sizeof(i))
//    {
//        printf(">\n");
//    }
//    else
//    {
//        printf("<\n");
//    }
//    return 0;
//}



//输入两个升序数组，合并为一个数组打印
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//
//	int arr1[1000];
//	int arr2[1000];
//	int i = 0;
//
//	//输入两个升序数组
//	//第一个
//	for (i = 0; i < m; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	//第二个
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//
//	//合并
//	int arr3[2000];
//	i = 0;
//	int j = 0;
//	int k = 0;
//	while (i < m && j < n)
//	{
//		if (arr1[i] > arr2[j])
//		{
//			arr3[k] = arr2[j];
//			k++;
//			j++;
//		}
//		else
//		{
//			arr3[k] = arr1[i];
//			k++;
//			i++;
//		}
//	}
//
//	if (i == m)
//	{
//		//arr1遍历完了，将arr2中剩下元素放到arr3中
//		while (j < n)
//		{
//			arr3[k] = arr2[j];
//			k++;
//			j++;
//		}
//	}
//	else
//	{
//		//arr2遍历完了，将arr1中剩下元素放到arr3中
//		while (i < m)
//		{
//			arr3[k] = arr2[i];
//			k++;
//			i++;
//		}
//	}
//
//	//输出
//	for (k = 0; k < m + n; k++)
//	{
//		printf("%d ", arr3[k]);
//	}
//	return 0;
//}

//不使用第三个数组，直接打印
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//
//	int arr1[1000];
//	int arr2[1000];
//	int i = 0;
//
//	//输入两个升序数组
//	//第一个
//	for (i = 0; i < m; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	//第二个
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//
//	i = 0;
//	int j = 0;
//	while (i < m && j < n)
//	{
//		if (arr1[i] > arr2[j])
//		{
//			printf("%d ", arr2[j++]);
//		}
//		else
//		{
//			printf("%d ", arr1[i++]);
//		}
//	}
//
//	if (i == m)
//	{
//		//arr1遍历完了
//		while (j < n)
//		{
//			printf("%d ", arr2[j++]);
//		}
//	}
//	else
//	{
//		//arr2遍历完了
//		while (i< m)
//		{
//			printf("%d ", arr1[i++]);
//		}
//	}
//
//
//	return 0;
//}



//int My_f(int i, int a)
//{
//	if (i == 0)
//		return a;
//	else
//		return a + 10 * My_f(i - 1, a);
//}
//
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int arr[5] = { 0 };
//	
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		arr[i] = My_f(i, a);
//	}
//	/*for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}*/
//
//	//求和
//	int sum = 0;
//	for (i = 0; i < 5; i++)
//	{
//		sum = sum + arr[i];
//	}
//
//	//输出
//	printf("sum=%d\n", sum);
//	return 0;
//}


//#include <math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10000; i++)
//	{
//		int count = 1;
//		int tmp = i;
//		int sum = 0;
//		//求i的位数
//		while (tmp / 10)
//		{
//			count++;
//			tmp = tmp / 10;
//		}
//
//		//求和
//		tmp = i;
//		while (tmp)
//		{
//			sum = sum + pow(tmp % 10, count);
//			tmp = tmp / 10;
//		}
//
//		//判断
//		if (i == sum)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//int main()
//{
//
//    char arr[10000];
//    gets(arr);
//    for (int i = strlen(arr) - 1; i >= 0; i--)
//    {
//        printf("%c", arr[i]);
//    }
//
//    return 0;
//}




//#include <math.h>
//int My_n(int i)
//{
//	int count = 0;
//	if (i < 10)
//		return 1;
//	else
//		return 1 + My_n(i / 10);
//}
//
//
//int My_sum(int i, int ret)
//{
//	int sum = 0;
//	while (i)
//	{
//		sum = sum + pow(i % 10, ret);
//		i = i / 10;
//	}
//	return sum;
//}
//
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		int ret = My_n(i);
//		int tmp = My_sum(i, ret);
//		if (tmp == i)
//			printf("%d ", i);
//	}
//	return 0;
//}


//int main()
//{
//	int money = 0;
//	int total = 0;
//	int empty = 0;
//
//	scanf("%d", &money);
//
//	if (money <= 0)
//	{
//		total = 0;
//	}
//	else
//	{
//		total = money * 2 - 1;
//	}
//	printf("total = %d\n", total);
//
//
//	return 0;
//}


//int main()
//{
//	int money = 0;
//	scanf("%d", &money);
//	int sum = money;
//	int ret = money;
//
//	while (ret / 2)
//	{
//		sum = sum + ret/2;
//		if (ret % 2 )
//		{
//			ret = ret / 2 + 1;
//		}
//		else
//		{
//			ret = ret / 2;
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}



