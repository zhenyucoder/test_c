#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int max = x > y ? x : y;
//	printf("%d\n", max);
//	return 0;
//}

//typedef unsigned long u_long;
//int main()
//{
//	u_long s_time= 6;
//	printf("大学生睡觉时间为%d小时\n", s_time);
//	return 0;
//}

//void test(void)
//{
//	static int z = 5;	//静态变量
//	printf("%d ", z);
//	z++;
//}
//int main()
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test();
//		i++;
//	}
//	return 0;
//}

//extern int g_val;
//int main()
//{
//	printf("%d\n", g_val);
//	return 0;
//}
//extern Add(int , int );
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int s = Add(x, y);
//	printf("%d\n", s);
//	return 0;
//}

//#define Add(x,y) ((x)+(y))
//int main()
//{
//	int a = 10;
//	int b = 0;
//	scanf("%d %d",&a,&b);
//	int c = Add(a, b);
//	printf("%d\n", c);
//	return 0;
//}

//struct stu
//{
//	char name[20];
//	int age;
//	char sex[5];
//};
//int main()
//{
//	struct stu s = { "张三",23,"女" };
//	printf("%s %d %s", s.name, s.age, s.sex);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a = 23;
//	int* pa = &a;
//	//printf("%d", pa);
//	*pa = 25;
//	int s = a;
//	printf("%d\n", s);
//
//	return 0;
//}

//int main()
//{
//	int a = 8;
//	int* p = &a;
//	printf("%d\n", sizeof(p));
//	printf("%d\n", sizeof(int *));
//	return 0;
//}

struct S
{
	char name[20];
	int age;
	float score;
};
void printf1(struct S t)
{
	printf("%s %d %f\n", t.name, t.age, t.score);
}
void printf2(struct S* ps)
{
	printf("%s %d %f\n", (*ps).name, (*ps).age, (*ps).score);
	printf("%s %d %f\n", ps->name,ps->age,ps->score);
}
int main()
{
	struct S s = { "zhangsan",23,85.f };
	printf1(s);
	printf2(&s);
	return 0;
}
