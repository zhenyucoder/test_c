﻿#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

//插入排序
void InsertSort(int* a, int n);
//希尔排序
void ShellSort(int* a, int n);
//选择排序
void SelectSort(int* a, int n);
//冒泡排序
void BubbleSort(int* a, int n);
//堆排序
void HeapSort(int* a, int n);
//快排
void QuickSort(int* a, int left, int right);