#include <stdio.h>

//int removeDuplicates(int* nums, int numsSize) {
//    int slow = 0, fast = 1;
//    while (fast < numsSize)
//    {
//        if (nums[fast] == nums[slow])
//        {
//            nums[++slow] = nums[fast++];
//            while (fast < numsSize)
//            {
//                if (nums[fast] == nums[slow])
//                {
//                    fast++;
//                }
//                else
//                {
//                    break;
//                }
//            }
//        }
//        else
//        {
//            nums[++slow] = nums[fast++];
//        }
//    }
//    return slow + 1;
//}
//代码优化
//int removeDuplicates(int* nums, int numsSize) {
//    if (numsSize <= 2)
//        return numsSize;
//    int slow = 2, fast = 2;
//    while (fast < numsSize)
//    {
//        if (nums[slow - 2] != nums[fast])
//        {
//            nums[slow] = nums[fast];
//            slow++;
//        }
//        ++fast;
//    }
//    return slow;
//}


//int compare(const void* p1, const void* p2)
//{
//    return *(int*)p1 - *(int*)p2;
//}
//
//int majorityElement(int* nums, int numsSize) {
//    qsort(nums, numsSize, sizeof(int), compare);
//    int mid = numsSize / 2;
//    return nums[mid];
//}

//void Swap(int* p1, int* p2)
//{
//    int tmp = *p1;
//    *p1 = *p2;
//    *p2 = tmp;
//}
//
//void reverse_nums(int* nums, int left, int right)
//{
//    while (left <= right)
//    {
//        Swap(&nums[left], &nums[right]);
//        left++;
//        right--;
//    }
//}
//
//void rotate(int* nums, int numsSize, int k) {
//    k %= numsSize;
//    reverse_nums(nums, 0, numsSize - 1);
//    reverse_nums(nums, 0, k - 1);
//    reverse_nums(nums, k, numsSize - 1);
//}

//2、使用额外的空间
// void rotate(int* nums, int numsSize, int k){
//     int newArr[numsSize];
//     for(int i=0; i<numsSize; i++)
//     {
//         newArr[(i+k)%numsSize]=nums[i];
//     }
//     //拷贝回去
//     for(int i=0; i<numsSize; i++)
//     {
//         nums[i]=newArr[i];
//     }
// }

//int maxProfit(int* prices, int pricesSize) {
//    int minprice = prices[0], maxprofit = 0;
//    for (int i = 0; i < pricesSize; i++)
//    {
//        maxprofit = fmax(maxprofit, prices[i] - minprice);
//        minprice = fmin(minprice, prices[i]);
//    }
//    return maxprofit;
//}

