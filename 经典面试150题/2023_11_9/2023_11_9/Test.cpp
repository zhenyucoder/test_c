//int maxSum;
//int maxGain(struct TreeNode* root)
//{
//    if (root == NULL)
//        return 0;
//
//    int leftGain = maxGain(root->left);
//    int rightGain = maxGain(root->right);
//    int left = fmax(root->val, root->val + leftGain);
//    int right = fmax(root->val, root->val + rightGain);
//    maxSum = fmax(maxSum, fmax(left + right - root->val, fmax(left, right)));
//    return fmax(left, right);
//
//}
//int maxPathSum(struct TreeNode* root) {
//    maxSum = INT_MIN;
//    maxGain(root);
//    return maxSum;
//}


//int countNodes(struct TreeNode* root) {
//    if (root == NULL)
//        return 0;
//    return 1 + countNodes(root->left) + countNodes(root->right);
//}

//struct TreeNode* lowestCommonAncestor(struct TreeNode* root, struct TreeNode* p, struct TreeNode* q) {
//    if (root == NULL || root == p || root == q)
//        return root;
//    struct TreeNode* left = lowestCommonAncestor(root->left, p, q);
//    struct TreeNode* right = lowestCommonAncestor(root->right, p, q);
//    if (left != NULL && right != NULL)
//        return root;
//    return left != NULL ? left : right;
//}

//int num;
//void preorderTraversal(struct TreeNode* root, struct TreeNode*** pl);
//void flatten(struct TreeNode* root) {
//    //二叉树展开为链表会破会二叉树结构
//    num = 0;
//    struct TreeNode** l = (struct TreeNode**)malloc(0);
//    preorderTraversal(root, &l);
//    for (int i = 1; i < num; i++)
//    {
//        struct TreeNode* prev = l[i - 1], * cur = l[i];
//        prev->left = NULL;
//        prev->right = cur;
//    }
//    free(l);
//}
//void preorderTraversal(struct TreeNode* root, struct TreeNode*** pl)
//{
//    if (root != NULL)
//    {
//        num++;
//        (*pl) = (struct TreeNode*)realloc((*pl), sizeof(struct TreeNode) * num);
//        (*pl)[num - 1] = root;
//        preorderTraversal(root->left, pl);
//        preorderTraversal(root->right, pl);
//    }
//
//}



//typedef struct {
//    int* res;
//    int size;
//    int idx;
//} BSTIterator;
//
//int getTreeSize(struct TreeNode* root) {
//    if (root == NULL) 
//    {
//        return 0;
//    }
//    return 1 + getTreeSize(root->left) + getTreeSize(root->right);
//}
//
//void inorder(int* ret, int* retSize, struct TreeNode* root) {
//    if (root == NULL) 
//    {
//        return;
//    }
//    inorder(ret, retSize, root->left);
//    ret[(*retSize)++] = root->val;
//    inorder(ret, retSize, root->right);
//}
//
//int* inorderTraversal(int* retSize, struct TreeNode* root) {
//    *retSize = 0;
//    int* ret = malloc(sizeof(int) * getTreeSize(root));
//    inorder(ret, retSize, root);
//    return ret;
//}
//
//BSTIterator* bSTIteratorCreate(struct TreeNode* root) {
//    BSTIterator* ret = malloc(sizeof(BSTIterator));
//    ret->res = inorderTraversal(&(ret->size), root);
//    ret->idx = 0;
//    return ret;
//}
//
//int bSTIteratorNext(BSTIterator* obj) {
//    return obj->res[(obj->idx)++];
//}
//
//bool bSTIteratorHasNext(BSTIterator* obj) {
//    return (obj->idx < obj->size);
//}
//
//void bSTIteratorFree(BSTIterator* obj) {
//    free(obj->res);
//    free(obj);
//}


//truct Node* connect(struct Node* root) {
//    if (!root)
//    {
//        return NULL;
//    }
//    struct Node* q[10001];
//    int left = 0, right = 0;
//    q[right++] = root;
//    while (left < right)
//    {
//        int n = right - left;
//        struct Node* last = NULL;
//        for (int i = 1; i <= n; ++i) 
//        {
//            struct Node* f = q[left++];
//            if (f->left) 
//            {
//                q[right++] = f->left;
//            }
//            if (f->right) 
//            {
//                q[right++] = f->right;
//            }
//            if (i != 1) 
//            {
//                last->next = f;
//            }
//            last = f;
//        }
//    }
//    return root;
//}


void handle(struct Node** last, struct Node** p, struct Node** nextStart) {
    if (*last)
    {
        (*last)->next = *p;
    }
    if (!(*nextStart))
    {
        *nextStart = *p;
    }
    *last = *p;
}

struct Node* connect(struct Node* root) {
    if (!root)
    {
        return NULL;
    }
    struct Node* start = root;
    while (start) 
    {
        struct Node* last = NULL, * nextStart = NULL;
        for (struct Node* p = start; p != NULL; p = p->next)
        {
            if (p->left)
            {
                handle(&last, &(p->left), &nextStart);
            }
            if (p->right) 
            {
                handle(&last, &(p->right), &nextStart);
            }
        }
        start = nextStart;
    }
    return root;
}
