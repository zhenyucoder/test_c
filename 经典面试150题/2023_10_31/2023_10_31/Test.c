//法一：排序
//int compare(const void* p1, const void* p2)
//{
//    return *(int*)p1 - *(int*)p2;
//}
//
//int hIndex(int* citations, int citationsSize) {
//    qsort(citations, citationsSize, sizeof(int), compare);
//    int h = 0, i = citationsSize - 1;
//    while (i >= 0 && citations[i] > h)
//    {
//        h++;
//        i--;
//    }
//    return 0;
//}

//bool hasCycle(struct ListNode* head) {
//    if (head == NULL)
//        return false;
//    struct ListNode* fast = head, * slow = head;
//    while (fast->next != NULL && fast->next->next != NULL)
//    {
//        fast = fast->next->next;
//        slow = slow->next;
//        if (fast == slow)
//            return true;
//    }
//    return false;
//}

//struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
//    struct ListNode* head = NULL, * tail = NULL;
//    int carry = 0;
//    while (l1 || l2)
//    {
//        int n1 = l1 ? l1->val : 0;
//        int n2 = l2 ? l2->val : 0;
//        int sum = n1 + n2 + carry;
//        if (head == NULL)
//        {
//            head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//            head->val = sum % 10;
//            head->next = NULL;
//        }
//        else
//        {
//            tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//            tail = tail->next;
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        carry = sum / 10;
//        // l1 l2 非空往后走
//        if (l1)
//            l1 = l1->next;
//        if (l2)
//            l2 = l2->next;
//    }
//    if (carry)
//    {
//        tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//        tail->next->val = carry;
//        tail->next->next = NULL;
//    }
//    return head;
//}


//struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
//    struct ListNode* head = NULL, * tail = NULL;
//    int carry = 0;
//    while (l1 && l2)
//    {
//        int sum = l1->val + l2->val + carry;
//        if (head == NULL)
//        {
//            head = tail = (struct ListNode*)malloc(sizeof(struct ListNode));
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        else
//        {
//            tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//            tail = tail->next;
//            tail->val = sum % 10;
//            tail->next = NULL;
//        }
//        carry = sum / 10;
//        l1 = l1->next;
//        l2 = l2->next;
//    }
//
//    struct ListNode* l3 = l1;
//    if (l1 == NULL)
//        l3 = l2;
//    while (l3)
//    {
//        int sum = l3->val + carry;
//        tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//        tail = tail->next;
//        tail->val = sum % 10;
//        tail->next = NULL;
//        carry = sum / 10;
//        l3 = l3->next;
//    }
//
//    if (carry)
//    {
//        tail->next = (struct ListNode*)malloc(sizeof(struct ListNode));
//        tail->next->val = carry;
//        tail->next->next = NULL;
//    }
//    return head;
//}


//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    //两链表中有一个为空
//    if (list1 == NULL)
//        return list2;
//    if (list2 == NULL)
//        return list1;
//
//    struct ListNode* newhead = NULL, * tail = NULL;
//    struct ListNode* cur1 = list1, * cur2 = list2;
//    while (cur1 && cur2)
//    {
//        struct ListNode* next1 = cur1->next;
//        struct ListNode* next2 = cur2->next;
//        if (cur1->val < cur2->val)
//        {
//            if (newhead == NULL)
//            {
//                newhead = tail = cur1;
//            }
//            else
//            {
//                tail->next = cur1;
//                tail = tail->next;
//            }
//            cur1 = next1;
//
//        }
//        else
//        {
//            if (newhead == NULL)
//            {
//                newhead = tail = cur2;
//                tail->next = NULL;
//            }
//            else
//            {
//                tail->next = cur2;
//                tail = tail->next;
//            }
//            cur2 = next2;
//        }
//    }
//    //处理后出现链表为空
//    if (cur1)
//        tail->next = cur1;
//    if (cur2)
//        tail->next = cur2;
//    return newhead;
//}

//递归
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    if (list1 == NULL)
//        return list2;
//    else if (list2 == NULL)
//        return list1;
//    else if (list1->val < list2->val)
//    {
//        list1->next = mergeTwoLists(list1->next, list2);
//        return list1;
//    }
//    else
//    {
//        list2->next = mergeTwoLists(list1, list2->next);
//        return list2;
//    }
//}


//struct Node* copyRandomList(struct Node* head) {
//    struct Node* cur = head;
//    while (cur)
//    {
//        struct Node* copy = (struct Node*)malloc(sizeof(struct Node));
//        copy->val = cur->val;
//        struct Node* next = cur->next;
//
//        //插入
//        cur->next = copy;
//        copy->next = next;
//
//        cur = next;
//    }
//
//    //拷贝random指针
//    cur = head;
//    while (cur)
//    {
//        struct Node* next = cur->next->next;
//        if (cur->random == NULL)
//            cur->next->random = NULL;
//        else
//            cur->next->random = cur->random->next;
//
//        cur = next;
//    }
//
//    //还原链表
//    cur = head;
//    struct Node* newhead = NULL, * tail = NULL;
//    while (cur)
//    {
//        struct Node* next = cur->next->next;
//        if (newhead == NULL)
//            newhead = tail = cur->next;
//        else
//        {
//            tail->next = cur->next;
//            tail = tail->next;
//        }
//
//        cur = next;
//    }
//    return newhead;
//}



//void reverseList(struct ListNode* head)
//{
//    struct ListNode* cur = head;
//    struct ListNode* prev = NULL;
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//        cur->next = prev;
//        prev = cur;
//        cur = next;
//    }
//}
//
//struct ListNode* reverseBetween(struct ListNode* head, int left, int right) {
//    //哨兵位
//    struct ListNode* phead = (struct ListNode*)malloc(sizeof(struct ListNode));
//    phead->next = head;
//
//    //待反转部分的前一个节点
//    struct ListNode* prev = phead;
//    for (int i = 0; i < left - 1; i++)
//    {
//        prev = prev->next;
//    }
//    //待反转部分的最后一个节点
//    struct ListNode* rightNode = prev;
//    for (int i = 0; i < right - left + 1; i++)
//    {
//        rightNode = rightNode->next;
//    }
//
//    //切断除子链，断开链接
//    struct ListNode* leftNode = prev->next;
//    struct ListNode* succ = rightNode->next;
//    prev->next = NULL;
//    rightNode->next = NULL;
//
//    //反转
//    reverseList(leftNode);
//
//    //反转部分链接回原链表
//    prev->next = rightNode;
//    leftNode->next = succ;
//
//    //返回，哨兵位销毁
//    struct ListNode* newhead = phead->next;
//    free(phead);
//    return newhead;
//}

//优化
//struct ListNode* reverseBetween(struct ListNode* head, int left, int right) {
//    //哨兵位
//    struct ListNode* phead = (struct ListNode*)malloc(sizeof(struct ListNode));
//    phead->next = head;
//
//    struct ListNode* prev = phead;
//    for (int i = 0; i < left - 1; i++)
//    {
//        prev = prev->next;
//    }
//    struct ListNode* cur = prev->next;
//    struct ListNode* next;
//    for (int i = 0; i < right - left; i++)
//    {
//        next = cur->next;
//        cur->next = next->next;
//        next->next = prev->next;
//        prev->next = next;
//    }
//
//    //处理哨兵位
//    struct ListNode* newhead = phead->next;
//    free(phead);
//    return newhead;
//}

