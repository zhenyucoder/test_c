//int getLength(struct ListNode* head)
//{
//    int length = 0;
//    struct ListNode* cur = head;
//    while (cur)
//    {
//        length++;
//        cur = cur->next;
//    }
//    return length;
//}
//
//struct ListNode* removeNthFromEnd(struct ListNode* head, int n) {
//    int length = getLength(head);
//    struct ListNode* dummy = (struct ListNode*)malloc(sizeof(struct ListNode));
//    dummy->next = head;
//
//    struct ListNode* cur = dummy;
//    //找到待删除节点的前一个节点
//    for (int i = 0; i < length - n; i++)
//    {
//        cur = cur->next;
//    }
//
//    //删除节点
//    cur->next = cur->next->next;
//
//    struct ListNode* newhead = dummy->next;
//    free(dummy);
//    return newhead;
//}

//struct ListNode* deleteDuplicates(struct ListNode* head) {
//    if (head == NULL)
//        return head;
//
//    struct ListNode* dummy = (struct ListNode*)malloc(sizeof(struct ListNode));
//    dummy->next = head;
//
//    struct ListNode* cur = dummy;
//    while (cur->next && cur->next->next)
//    {
//        if (cur->next->val == cur->next->next->val)
//        {
//            //删除重复元素
//            int x = cur->next->val;
//            while (cur->next && cur->next->val == x)
//            {
//                cur->next = cur->next->next;
//            }
//        }
//        else
//        {
//            cur = cur->next;
//        }
//    }
//
//    struct ListNode* newhead = dummy->next;
//    free(dummy);
//    return newhead;
//}


//环形链表
//#include <stdio.h>
//#include <stdlib.h>
//struct ListNode {
//    int val;
//    struct ListNode* next;
//};
//
//struct ListNode* BuyNode(int x)
//{
//    struct ListNode* node = (struct ListNode*)malloc(sizeof(struct ListNode));
//    node->val = x;
//    node->next = NULL;
//    return node;
//}
//
//struct ListNode* rotateRight(struct ListNode* head, int k) {
//    if (k == 0 || head == NULL || head->next == NULL)
//        return head;
//
//    //链表长度
//    int length = 1;
//    struct ListNode* iter = head;
//    while (iter->next)
//    {
//        length++;
//        iter = iter->next;
//    }
//
//    int movegap = k % length;
//    if (movegap == 0)
//        return head;
//
//    //链表首尾相连，环形链表
//    iter->next = head;
//
//    for (int i = 0; i < length - movegap - 1; i++)
//    {
//        head = head->next;
//    }
//    struct ListNode* newhead = head->next;
//    head->next = NULL;
//    return newhead;
//}
//
//
//
//
//int main()
//{
//    struct ListNode* n1 = BuyNode(0);
//    struct ListNode* n2 = BuyNode(1);
//    struct ListNode* n3 = BuyNode(2);
//    n1->next = n2;
//    n2->next = n3;
//    rotateRight(n1, 4);
//    return 0;
//}

//双指针
//struct ListNode* rotateRight(struct ListNode* head, int k) {
//    if (k == 0 || head == NULL || head->next == NULL)
//        return head;
//
//    struct ListNode* dummy = (struct ListNode*)malloc(sizeof(struct ListNode));
//    dummy->next = head;
//
//    int length = 1;
//    struct ListNode* iter = head;
//    while (iter->next)
//    {
//        length++;
//        iter = iter->next;
//    }
//    k %= length;
//    if (k == 0)
//        return head;
//
//    struct ListNode* slow = dummy, * fast = head;
//    while (k--)
//    {
//        fast = fast->next;
//    }
//
//    while (fast)
//    {
//        slow = slow->next;
//        fast = fast->next;
//    }
//
//    dummy->next = slow->next;
//    slow->next = NULL;
//    iter->next = head;
//    return dummy->next;
//}


//struct ListNode* partition(struct ListNode* head, int x) {
//    if (head == NULL || head->next == NULL)
//        return head;
//    struct ListNode* pheadless = (struct ListNode*)malloc(sizeof(struct ListNode));
//    struct ListNode* tailless = pheadless;
//    struct ListNode* pheadgreater = (struct ListNode*)malloc(sizeof(struct ListNode));
//    struct ListNode* tailgreater = pheadgreater;
//
//    struct ListNode* cur = head;
//    while (cur)
//    {
//        if (cur->val < x)
//        {
//            tailless->next = cur;
//            tailless = tailless->next;
//        }
//        else
//        {
//            tailgreater->next = cur;
//            tailgreater = tailgreater->next;
//        }
//        cur = cur->next;
//    }
//    //防止循环
//    tailgreater->next = NULL;
//
//    tailless->next = pheadgreater->next;
//    struct ListNode* newhead = pheadless->next;
//    free(pheadgreater);
//    free(pheadless);
//    return newhead;
//}

