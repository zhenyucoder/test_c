//#include <iostream>
//using namespace std;
//
// 
//
//static int Monthday[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//int main() {
//    int year1, month1, day1, year2, month2, day2;
//    //计算日期与0000 01 01 相差的天数
//    int GapDay(int year, int month, int day);
//    while (~scanf("%4d%2d%2d%4d%2d%2d", &year1, &month1, &day1, &year2, &month2, &day2))
//    {
//        int totalday1 = GapDay(year1, month1, day1);
//        int totalday2 = GapDay(year2, month2, day2);
//        cout << abs(totalday1 - totalday2) + 1 << endl;
//    }
//}
//
////计算日期与0000 00 00 相差的天数
//int GapDay(int year, int month, int day)
//{
//    // 年份相差的天数
//    int YearGapday = 365 * year + year / 400 + year / 4 - year / 100;
//    //月份相差的天数
//    int MonthGapday = 0;
//    for (int i = 1; i < month; i++)
//    {
//        MonthGapday += Monthday[i];
//    }
//    if (month > 2 && ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)))
//        MonthGapday++;
//    return YearGapday + MonthGapday + day;
//}


//int main() {
//    int year, day;
//    int MonthDay[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    while (cin >> year >> day)
//    {
//        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
//            MonthDay[2] = 29;
//        else
//            MonthDay[2] = 28;
//
//        for (int i = 1; i <= 12; i++)
//        {
//            if (day < MonthDay[i])
//            {
//                printf("%04d-%02d-%02d\n", year, i, day);
//                break;
//            }
//            else
//            {
//                day -= MonthDay[i];
//            }
//        }
//    }
//}


//int main() {
//    int days[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//    int m;
//    while (cin >> m)
//    {
//        for (int i = 0; i < m; i++)
//        {
//            int GapDay, year, month, day;
//            cin >> year >> month >> day >> GapDay;
//            day += GapDay;
//            while (day > days[month])
//            {
//                if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
//                    days[2] = 29;
//                else
//                    days[2] = 28;
//
//                day -= days[month];
//                month++;
//                if (month == 13)
//                {
//                    ++year;
//                    month = 1;
//                }
//            }
//            printf("%4d-%02d-%02d\n", year, month, day);
//        }
//    }
//}


//#define MAX_NUM_SIZE 10001
//
//typedef struct {
//    int key;
//    int val;
//    UT_hash_handle hh;
//} HashItem;
//
//bool findHash(const HashItem** obj, int key) {
//    HashItem* pEntry = NULL;
//    HASH_FIND_INT(*obj, &key, pEntry);
//    if (NULL != pEntry) 
//    {
//        return true;
//    }
//    return false;
//}
//
//int getHash(const HashItem** obj, int key) {
//    HashItem* pEntry = NULL;
//    HASH_FIND_INT(*obj, &key, pEntry);
//    if (NULL == pEntry) 
//    {
//        return -1;
//    }
//    return pEntry->val;
//}
//
//void insertHash(HashItem** obj, int key, int val) {
//    HashItem* pEntry = NULL;
//    HASH_FIND_INT(*obj, &key, pEntry);
//    if (NULL != pEntry) 
//    {
//        pEntry->val = val;
//    }
//    else 
//    {
//        pEntry = (HashItem*)malloc(sizeof(HashItem));
//        pEntry->key = key;
//        pEntry->val = val;
//        HASH_ADD_INT(*obj, key, pEntry);
//    }
//}
//
//bool removeHash(HashItem** obj, int key) {
//    HashItem* pEntry = NULL;
//    HASH_FIND_INT(*obj, &key, pEntry);
//    if (NULL != pEntry) 
//    {
//        HASH_DEL(*obj, pEntry);
//        free(pEntry);
//    }
//    return true;
//}
//
//void freeHash(HashItem** obj) {
//    HashItem* curr, * tmp;
//    HASH_ITER(hh, *obj, curr, tmp) 
//    {
//        HASH_DEL(*obj, curr);
//        free(curr);
//    }
//}
//
//typedef struct {
//    int* nums;
//    int numsSize;
//    HashItem* indices;
//} RandomizedSet;
//
//RandomizedSet* randomizedSetCreate() {
//    srand((unsigned)time(NULL));
//    RandomizedSet* obj = (RandomizedSet*)malloc(sizeof(RandomizedSet));
//    obj->nums = (int*)malloc(sizeof(int) * MAX_NUM_SIZE);
//    obj->numsSize = 0;
//    obj->indices = NULL;
//    return obj;
//}
//
//bool randomizedSetInsert(RandomizedSet* obj, int val) {
//    HashItem* pEntry = NULL;
//    if (findHash(&obj->indices, val)) 
//    {
//        return false;
//    }
//    int index = obj->numsSize;
//    obj->nums[obj->numsSize++] = val;
//    insertHash(&obj->indices, val, obj->numsSize - 1);
//    return true;
//}
//
//bool randomizedSetRemove(RandomizedSet* obj, int val) {
//    if (!findHash(&obj->indices, val)) 
//    {
//        return false;
//    }
//    int index = getHash(&obj->indices, val);
//    int last = obj->nums[obj->numsSize - 1];
//    obj->nums[index] = last;
//    insertHash(&obj->indices, last, index);
//    obj->numsSize--;
//    removeHash(&obj->indices, val);
//    return true;
//}
//
//int randomizedSetGetRandom(RandomizedSet* obj) {
//    int randomIndex = rand() % obj->numsSize;
//    return obj->nums[randomIndex];
//}
//
//void randomizedSetFree(RandomizedSet* obj) {
//    freeHash(&obj->indices);
//    free(obj->nums);
// 
//    free(obj);
//}

//int maxProfit(int* prices, int pricesSize) {
//    int ans = 0;
//    for (int i = 1; i < pricesSize; i++)
//    {
//        ans += fmax(0, prices[i] - prices[i - 1]);
//    }
//    return ans;
//}

bool canJump(int* nums, int numsSize) {
    int maxreach = 0;

    for (int i = 0; i < numsSize && maxreach < numsSize - 1; i++)
    {
        if (i > maxreach)
            return false;
        maxreach = fmax(maxreach, i + nums[i]);
    }
    return true;
}