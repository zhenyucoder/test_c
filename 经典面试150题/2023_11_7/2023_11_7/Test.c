//int maxDepth(struct TreeNode* root) {
//    if (root == NULL)
//        return 0;
//    int leftTreeLength = maxDepth(root->left);
//    int rightTreeLength = maxDepth(root->right);
//    return leftTreeLength > rightTreeLength ? leftTreeLength + 1 : rightTreeLength + 1;
//}


//bool isSameTree(struct TreeNode* p, struct TreeNode* q) {
//    if (p == NULL && q == NULL)
//        return true;
//    if (p == NULL || q == NULL)
//        return false;
//    if (p->val != q->val)
//        return false;
//    return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//}


//struct TreeNode* invertTree(struct TreeNode* root) {
//    if (root == NULL)
//        return NULL;
//    struct TreeNode* left = invertTree(root->left);
//    struct TreeNode* right = invertTree(root->right);
//    root->left = right;
//    root->right = left;
//    return root;
//}


//bool _isSymmetric(struct TreeNode* leftTree, struct TreeNode* rightTree)
//{
//    if (leftTree == NULL && rightTree == NULL)
//        return true;
//    if (leftTree == NULL || rightTree == NULL)
//        return false;
//    if (leftTree->val != rightTree->val)
//        return false;
//    return _isSymmetric(leftTree->left, rightTree->right)
//        && _isSymmetric(leftTree->right, rightTree->left);
//}
//bool isSymmetric(struct TreeNode* root) {
//    return _isSymmetric(root->left, root->right);
//}

// 创建节点
//struct TreeNode* BuyTreeNode(int x)
//{
//    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
//    node->val = x;
//    node->left = NULL;
//    node->right = NULL;
//    return node;
//}
//
//struct TreeNode* buildTree(int* preorder, int preorderSize, int* inorder, int inorderSize) {
//    if (preorderSize == 0 || inorderSize == 0)
//        return NULL;
//    //根节点
//    int value = preorder[0];
//    struct TreeNode* root = BuyTreeNode(value);
//    //中序中根节点下标，后续分割子树
//    int i = 0;
//    while (i < inorderSize && inorder[i] != value)
//    {
//        i++;
//    }
//    root->left = buildTree(preorder + 1, i, inorder, i);
//    root->right = buildTree(preorder + i + 1, preorderSize - i - 1, inorder + i + 1, inorderSize - i - 1);
//    return root;
//}


 //创建节点
struct TreeNode* BuyTreeNode(int x)
{
    struct TreeNode* node = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    node->val = x;
    node->left = NULL;
    node->right = NULL;
    return node;
}

struct TreeNode* buildTree(int* inorder, int inorderSize, int* postorder, int postorderSize) {
    if (inorderSize == 0 || postorderSize == 0)
        return NULL;
    int x = postorder[postorderSize - 1];
    struct TreeNode* root = BuyTreeNode(x);

    //查找中序中根节点下标，用于后续分割出子树
    int indix = 0;
    while (indix < inorderSize && x != inorder[indix])
    {
        indix++;
    }

    root->left = buildTree(inorder, indix, postorder, indix);
    root->right = buildTree(inorder + indix + 1, inorderSize - indix - 1,
        postorder + indix, postorderSize - indix - 1);
    return root;
}