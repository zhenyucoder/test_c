//void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
//    int end = m + n - 1, end1 = m - 1, end2 = n - 1;
//    while (end1 >= 0 && end2 >= 0)
//    {
//        if (nums1[end1] <= nums2[end2])
//        {
//            nums1[end--] = nums2[end2--];
//        }
//        else
//        {
//            nums1[end--] = nums1[end1--];
//        }
//    }
//    //nums2[]中还有数据
//    if (end2 >= 0)
//    {
//        while (end2 >= 0)
//        {
//            nums1[end--] = nums2[end2--];
//        }
//    }
//}

//int removeElement(int* nums, int numsSize, int val) {
//    int newnumsSize = 0;
//    int fast = 0, slow = 0;
//    while (fast < numsSize)
//    {
//        if (nums[fast] == val)
//        {
//            fast++;
//        }
//        else
//        {
//            nums[slow++] = nums[fast++];
//        }
//    }
//    return slow;
//}
// 
//双指针优化
//int removeElement(int* nums, int numsSize, int val) {
//    int left = 0, right = numsSize - 1;
//    while (left <= right)
//    {
//        if (nums[left] == val)
//        {
//            nums[left] = nums[right--];
//        }
//        else
//        {
//            left++;
//        }
//    }
//    return left;
//}

//int removeDuplicates(int* nums, int numsSize) {
//    int slow = 0, fast = 1;
//    for (; fast < numsSize; fast++)
//    {
//        if (nums[fast] != nums[slow])
//        {
//            nums[++slow] = nums[fast];
//        }
//    }
//    return slow + 1;
//}

int removeDuplicates(int* nums, int numsSize) {
    if (numsSize <= 2) 
    {
        return numsSize;
    }
    int slow = 2, fast = 2;
    while (fast < numsSize) 
    {
        if (nums[slow - 2] != nums[fast]) 
        {
            nums[slow] = nums[fast];
            ++slow;
        }
        ++fast;
    }
    return slow;
}

